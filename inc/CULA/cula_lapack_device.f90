! This file contains the device interface declarations for all supported Fortran
! compilers. These routines are for data allocated by the user through the CUDA
! runtime API, using functions such as cudaMalloc().
!
! This file does not declare complete dimensions for routines because many
! routines have variable dimensions which are impossible to declare fully.
! Please consult the API Reference document for full details of expected
! arguments.

module cula_lapack_device
    use ISO_C_BINDING
    implicit none

    ! culaDeviceSbdsqr(char uplo, int n, int ncvt, int nru, int ncc, culaFloat* d, culaFloat* e, culaFloat* vt, int ldvt, culaFloat* u, int ldu, culaFloat* c, int ldc);
    interface
        integer(C_INT) function cula_device_sbdsqr(uplo,n,ncvt,nru,ncc,d,e,vt,ldvt,u,ldu,c,ldc)&
                BIND(C,name="culaDeviceSbdsqr")
            use ISO_C_BINDING
            character(C_CHAR), value :: uplo
            integer(C_INT), value :: n
            integer(C_INT), value :: ncvt
            integer(C_INT), value :: nru
            integer(C_INT), value :: ncc
            type(C_PTR), value :: d
            type(C_PTR), value :: e
            type(C_PTR), value :: vt
            integer(C_INT), value :: ldvt
            type(C_PTR), value :: u
            integer(C_INT), value :: ldu
            type(C_PTR), value :: c
            integer(C_INT), value :: ldc
        end function
    end interface

    ! culaDeviceDbdsqr(char uplo, int n, int ncvt, int nru, int ncc, culaDouble* d, culaDouble* e, culaDouble* vt, int ldvt, culaDouble* u, int ldu, culaDouble* c, int ldc);
    interface
        integer(C_INT) function cula_device_dbdsqr(uplo,n,ncvt,nru,ncc,d,e,vt,ldvt,u,ldu,c,ldc)&
                BIND(C,name="culaDeviceDbdsqr")
            use ISO_C_BINDING
            character(C_CHAR), value :: uplo
            integer(C_INT), value :: n
            integer(C_INT), value :: ncvt
            integer(C_INT), value :: nru
            integer(C_INT), value :: ncc
            type(C_PTR), value :: d
            type(C_PTR), value :: e
            type(C_PTR), value :: vt
            integer(C_INT), value :: ldvt
            type(C_PTR), value :: u
            integer(C_INT), value :: ldu
            type(C_PTR), value :: c
            integer(C_INT), value :: ldc
        end function
    end interface

    ! culaDeviceCbdsqr(char uplo, int n, int ncvt, int nru, int ncc, culaFloat* d, culaFloat* e, culaFloatComplex* vt, int ldvt, culaFloatComplex* u, int ldu, culaFloatComplex* c, int ldc);
    interface
        integer(C_INT) function cula_device_cbdsqr(uplo,n,ncvt,nru,ncc,d,e,vt,ldvt,u,ldu,c,ldc)&
                BIND(C,name="culaDeviceCbdsqr")
            use ISO_C_BINDING
            character(C_CHAR), value :: uplo
            integer(C_INT), value :: n
            integer(C_INT), value :: ncvt
            integer(C_INT), value :: nru
            integer(C_INT), value :: ncc
            type(C_PTR), value :: d
            type(C_PTR), value :: e
            type(C_PTR), value :: vt
            integer(C_INT), value :: ldvt
            type(C_PTR), value :: u
            integer(C_INT), value :: ldu
            type(C_PTR), value :: c
            integer(C_INT), value :: ldc
        end function
    end interface

    ! culaDeviceZbdsqr(char uplo, int n, int ncvt, int nru, int ncc, culaDouble* d, culaDouble* e, culaDoubleComplex* vt, int ldvt, culaDoubleComplex* u, int ldu, culaDoubleComplex* c, int ldc);
    interface
        integer(C_INT) function cula_device_zbdsqr(uplo,n,ncvt,nru,ncc,d,e,vt,ldvt,u,ldu,c,ldc)&
                BIND(C,name="culaDeviceZbdsqr")
            use ISO_C_BINDING
            character(C_CHAR), value :: uplo
            integer(C_INT), value :: n
            integer(C_INT), value :: ncvt
            integer(C_INT), value :: nru
            integer(C_INT), value :: ncc
            type(C_PTR), value :: d
            type(C_PTR), value :: e
            type(C_PTR), value :: vt
            integer(C_INT), value :: ldvt
            type(C_PTR), value :: u
            integer(C_INT), value :: ldu
            type(C_PTR), value :: c
            integer(C_INT), value :: ldc
        end function
    end interface

    ! culaDeviceSgbtrf(int m, int n, int kl, int ku, culaFloat* a, int lda, culaInt* ipiv);
    interface
        integer(C_INT) function cula_device_sgbtrf(m,n,kl,ku,a,lda,ipiv)&
                BIND(C,name="culaDeviceSgbtrf")
            use ISO_C_BINDING
            integer(C_INT), value :: m
            integer(C_INT), value :: n
            integer(C_INT), value :: kl
            integer(C_INT), value :: ku
            type(C_PTR), value :: a
            integer(C_INT), value :: lda
            type(C_PTR), value :: ipiv
        end function
    end interface

    ! culaDeviceDgbtrf(int m, int n, int kl, int ku, culaDouble* a, int lda, culaInt* ipiv);
    interface
        integer(C_INT) function cula_device_dgbtrf(m,n,kl,ku,a,lda,ipiv)&
                BIND(C,name="culaDeviceDgbtrf")
            use ISO_C_BINDING
            integer(C_INT), value :: m
            integer(C_INT), value :: n
            integer(C_INT), value :: kl
            integer(C_INT), value :: ku
            type(C_PTR), value :: a
            integer(C_INT), value :: lda
            type(C_PTR), value :: ipiv
        end function
    end interface

    ! culaDeviceCgbtrf(int m, int n, int kl, int ku, culaFloatComplex* a, int lda, culaInt* ipiv);
    interface
        integer(C_INT) function cula_device_cgbtrf(m,n,kl,ku,a,lda,ipiv)&
                BIND(C,name="culaDeviceCgbtrf")
            use ISO_C_BINDING
            integer(C_INT), value :: m
            integer(C_INT), value :: n
            integer(C_INT), value :: kl
            integer(C_INT), value :: ku
            type(C_PTR), value :: a
            integer(C_INT), value :: lda
            type(C_PTR), value :: ipiv
        end function
    end interface

    ! culaDeviceZgbtrf(int m, int n, int kl, int ku, culaDoubleComplex* a, int lda, culaInt* ipiv);
    interface
        integer(C_INT) function cula_device_zgbtrf(m,n,kl,ku,a,lda,ipiv)&
                BIND(C,name="culaDeviceZgbtrf")
            use ISO_C_BINDING
            integer(C_INT), value :: m
            integer(C_INT), value :: n
            integer(C_INT), value :: kl
            integer(C_INT), value :: ku
            type(C_PTR), value :: a
            integer(C_INT), value :: lda
            type(C_PTR), value :: ipiv
        end function
    end interface

    ! culaDeviceSgbtrs(char trans, int n, int kl, int ku, int nrhs, culaFloat* ab, int ldab, culaInt* ipiv, culaFloat* b, int ldb);
    interface
        integer(C_INT) function cula_device_sgbtrs(trans,n,kl,ku,nrhs,ab,ldab,ipiv,b,ldb)&
                BIND(C,name="culaDeviceSgbtrs")
            use ISO_C_BINDING
            character(C_CHAR), value :: trans
            integer(C_INT), value :: n
            integer(C_INT), value :: kl
            integer(C_INT), value :: ku
            integer(C_INT), value :: nrhs
            type(C_PTR), value :: ab
            integer(C_INT), value :: ldab
            type(C_PTR), value :: ipiv
            type(C_PTR), value :: b
            integer(C_INT), value :: ldb
        end function
    end interface

    ! culaDeviceDgbtrs(char trans, int n, int kl, int ku, int nrhs, culaDouble* ab, int ldab, culaInt* ipiv, culaDouble* b, int ldb);
    interface
        integer(C_INT) function cula_device_dgbtrs(trans,n,kl,ku,nrhs,ab,ldab,ipiv,b,ldb)&
                BIND(C,name="culaDeviceDgbtrs")
            use ISO_C_BINDING
            character(C_CHAR), value :: trans
            integer(C_INT), value :: n
            integer(C_INT), value :: kl
            integer(C_INT), value :: ku
            integer(C_INT), value :: nrhs
            type(C_PTR), value :: ab
            integer(C_INT), value :: ldab
            type(C_PTR), value :: ipiv
            type(C_PTR), value :: b
            integer(C_INT), value :: ldb
        end function
    end interface

    ! culaDeviceCgbtrs(char trans, int n, int kl, int ku, int nrhs, culaFloatComplex* ab, int ldab, culaInt* ipiv, culaFloatComplex* b, int ldb);
    interface
        integer(C_INT) function cula_device_cgbtrs(trans,n,kl,ku,nrhs,ab,ldab,ipiv,b,ldb)&
                BIND(C,name="culaDeviceCgbtrs")
            use ISO_C_BINDING
            character(C_CHAR), value :: trans
            integer(C_INT), value :: n
            integer(C_INT), value :: kl
            integer(C_INT), value :: ku
            integer(C_INT), value :: nrhs
            type(C_PTR), value :: ab
            integer(C_INT), value :: ldab
            type(C_PTR), value :: ipiv
            type(C_PTR), value :: b
            integer(C_INT), value :: ldb
        end function
    end interface

    ! culaDeviceZgbtrs(char trans, int n, int kl, int ku, int nrhs, culaDoubleComplex* ab, int ldab, culaInt* ipiv, culaDoubleComplex* b, int ldb);
    interface
        integer(C_INT) function cula_device_zgbtrs(trans,n,kl,ku,nrhs,ab,ldab,ipiv,b,ldb)&
                BIND(C,name="culaDeviceZgbtrs")
            use ISO_C_BINDING
            character(C_CHAR), value :: trans
            integer(C_INT), value :: n
            integer(C_INT), value :: kl
            integer(C_INT), value :: ku
            integer(C_INT), value :: nrhs
            type(C_PTR), value :: ab
            integer(C_INT), value :: ldab
            type(C_PTR), value :: ipiv
            type(C_PTR), value :: b
            integer(C_INT), value :: ldb
        end function
    end interface

    ! culaDeviceSgebrd(int m, int n, culaFloat* a, int lda, culaFloat* d, culaFloat* e, culaFloat* tauq, culaFloat* taup);
    interface
        integer(C_INT) function cula_device_sgebrd(m,n,a,lda,d,e,tauq,taup)&
                BIND(C,name="culaDeviceSgebrd")
            use ISO_C_BINDING
            integer(C_INT), value :: m
            integer(C_INT), value :: n
            type(C_PTR), value :: a
            integer(C_INT), value :: lda
            type(C_PTR), value :: d
            type(C_PTR), value :: e
            type(C_PTR), value :: tauq
            type(C_PTR), value :: taup
        end function
    end interface

    ! culaDeviceDgebrd(int m, int n, culaDouble* a, int lda, culaDouble* d, culaDouble* e, culaDouble* tauq, culaDouble* taup);
    interface
        integer(C_INT) function cula_device_dgebrd(m,n,a,lda,d,e,tauq,taup)&
                BIND(C,name="culaDeviceDgebrd")
            use ISO_C_BINDING
            integer(C_INT), value :: m
            integer(C_INT), value :: n
            type(C_PTR), value :: a
            integer(C_INT), value :: lda
            type(C_PTR), value :: d
            type(C_PTR), value :: e
            type(C_PTR), value :: tauq
            type(C_PTR), value :: taup
        end function
    end interface

    ! culaDeviceCgebrd(int m, int n, culaFloatComplex* a, int lda, culaFloat* d, culaFloat* e, culaFloatComplex* tauq, culaFloatComplex* taup);
    interface
        integer(C_INT) function cula_device_cgebrd(m,n,a,lda,d,e,tauq,taup)&
                BIND(C,name="culaDeviceCgebrd")
            use ISO_C_BINDING
            integer(C_INT), value :: m
            integer(C_INT), value :: n
            type(C_PTR), value :: a
            integer(C_INT), value :: lda
            type(C_PTR), value :: d
            type(C_PTR), value :: e
            type(C_PTR), value :: tauq
            type(C_PTR), value :: taup
        end function
    end interface

    ! culaDeviceZgebrd(int m, int n, culaDoubleComplex* a, int lda, culaDouble* d, culaDouble* e, culaDoubleComplex* tauq, culaDoubleComplex* taup);
    interface
        integer(C_INT) function cula_device_zgebrd(m,n,a,lda,d,e,tauq,taup)&
                BIND(C,name="culaDeviceZgebrd")
            use ISO_C_BINDING
            integer(C_INT), value :: m
            integer(C_INT), value :: n
            type(C_PTR), value :: a
            integer(C_INT), value :: lda
            type(C_PTR), value :: d
            type(C_PTR), value :: e
            type(C_PTR), value :: tauq
            type(C_PTR), value :: taup
        end function
    end interface

    ! culaDeviceSgeev(char jobvl, char jobvr, int n, culaFloat* a, int lda, culaFloat* wr, culaFloat* wi, culaFloat* vl, int ldvl, culaFloat* vr, int ldvr);
    interface
        integer(C_INT) function cula_device_sgeev(jobvl,jobvr,n,a,lda,wr,wi,vl,ldvl,vr,ldvr)&
                BIND(C,name="culaDeviceSgeev")
            use ISO_C_BINDING
            character(C_CHAR), value :: jobvl
            character(C_CHAR), value :: jobvr
            integer(C_INT), value :: n
            type(C_PTR), value :: a
            integer(C_INT), value :: lda
            type(C_PTR), value :: wr
            type(C_PTR), value :: wi
            type(C_PTR), value :: vl
            integer(C_INT), value :: ldvl
            type(C_PTR), value :: vr
            integer(C_INT), value :: ldvr
        end function
    end interface

    ! culaDeviceDgeev(char jobvl, char jobvr, int n, culaDouble* a, int lda, culaDouble* wr, culaDouble* wi, culaDouble* vl, int ldvl, culaDouble* vr, int ldvr);
    interface
        integer(C_INT) function cula_device_dgeev(jobvl,jobvr,n,a,lda,wr,wi,vl,ldvl,vr,ldvr)&
                BIND(C,name="culaDeviceDgeev")
            use ISO_C_BINDING
            character(C_CHAR), value :: jobvl
            character(C_CHAR), value :: jobvr
            integer(C_INT), value :: n
            type(C_PTR), value :: a
            integer(C_INT), value :: lda
            type(C_PTR), value :: wr
            type(C_PTR), value :: wi
            type(C_PTR), value :: vl
            integer(C_INT), value :: ldvl
            type(C_PTR), value :: vr
            integer(C_INT), value :: ldvr
        end function
    end interface

    ! culaDeviceCgeev(char jobvl, char jobvr, int n, culaFloatComplex* a, int lda, culaFloatComplex* w, culaFloatComplex* vl, int ldvl, culaFloatComplex* vr, int ldvr);
    interface
        integer(C_INT) function cula_device_cgeev(jobvl,jobvr,n,a,lda,w,vl,ldvl,vr,ldvr)&
                BIND(C,name="culaDeviceCgeev")
            use ISO_C_BINDING
            character(C_CHAR), value :: jobvl
            character(C_CHAR), value :: jobvr
            integer(C_INT), value :: n
            type(C_PTR), value :: a
            integer(C_INT), value :: lda
            type(C_PTR), value :: w
            type(C_PTR), value :: vl
            integer(C_INT), value :: ldvl
            type(C_PTR), value :: vr
            integer(C_INT), value :: ldvr
        end function
    end interface

    ! culaDeviceZgeev(char jobvl, char jobvr, int n, culaDoubleComplex* a, int lda, culaDoubleComplex* w, culaDoubleComplex* vl, int ldvl, culaDoubleComplex* vr, int ldvr);
    interface
        integer(C_INT) function cula_device_zgeev(jobvl,jobvr,n,a,lda,w,vl,ldvl,vr,ldvr)&
                BIND(C,name="culaDeviceZgeev")
            use ISO_C_BINDING
            character(C_CHAR), value :: jobvl
            character(C_CHAR), value :: jobvr
            integer(C_INT), value :: n
            type(C_PTR), value :: a
            integer(C_INT), value :: lda
            type(C_PTR), value :: w
            type(C_PTR), value :: vl
            integer(C_INT), value :: ldvl
            type(C_PTR), value :: vr
            integer(C_INT), value :: ldvr
        end function
    end interface

    ! culaDeviceSgehrd(int n, int ilo, int ihi, culaFloat* a, int lda, culaFloat* tau);
    interface
        integer(C_INT) function cula_device_sgehrd(n,ilo,ihi,a,lda,tau)&
                BIND(C,name="culaDeviceSgehrd")
            use ISO_C_BINDING
            integer(C_INT), value :: n
            integer(C_INT), value :: ilo
            integer(C_INT), value :: ihi
            type(C_PTR), value :: a
            integer(C_INT), value :: lda
            type(C_PTR), value :: tau
        end function
    end interface

    ! culaDeviceDgehrd(int n, int ilo, int ihi, culaDouble* a, int lda, culaDouble* tau);
    interface
        integer(C_INT) function cula_device_dgehrd(n,ilo,ihi,a,lda,tau)&
                BIND(C,name="culaDeviceDgehrd")
            use ISO_C_BINDING
            integer(C_INT), value :: n
            integer(C_INT), value :: ilo
            integer(C_INT), value :: ihi
            type(C_PTR), value :: a
            integer(C_INT), value :: lda
            type(C_PTR), value :: tau
        end function
    end interface

    ! culaDeviceCgehrd(int n, int ilo, int ihi, culaFloatComplex* a, int lda, culaFloatComplex* tau);
    interface
        integer(C_INT) function cula_device_cgehrd(n,ilo,ihi,a,lda,tau)&
                BIND(C,name="culaDeviceCgehrd")
            use ISO_C_BINDING
            integer(C_INT), value :: n
            integer(C_INT), value :: ilo
            integer(C_INT), value :: ihi
            type(C_PTR), value :: a
            integer(C_INT), value :: lda
            type(C_PTR), value :: tau
        end function
    end interface

    ! culaDeviceZgehrd(int n, int ilo, int ihi, culaDoubleComplex* a, int lda, culaDoubleComplex* tau);
    interface
        integer(C_INT) function cula_device_zgehrd(n,ilo,ihi,a,lda,tau)&
                BIND(C,name="culaDeviceZgehrd")
            use ISO_C_BINDING
            integer(C_INT), value :: n
            integer(C_INT), value :: ilo
            integer(C_INT), value :: ihi
            type(C_PTR), value :: a
            integer(C_INT), value :: lda
            type(C_PTR), value :: tau
        end function
    end interface

    ! culaDeviceSgelqf(int m, int n, culaFloat* a, int lda, culaFloat* tau);
    interface
        integer(C_INT) function cula_device_sgelqf(m,n,a,lda,tau)&
                BIND(C,name="culaDeviceSgelqf")
            use ISO_C_BINDING
            integer(C_INT), value :: m
            integer(C_INT), value :: n
            type(C_PTR), value :: a
            integer(C_INT), value :: lda
            type(C_PTR), value :: tau
        end function
    end interface

    ! culaDeviceDgelqf(int m, int n, culaDouble* a, int lda, culaDouble* tau);
    interface
        integer(C_INT) function cula_device_dgelqf(m,n,a,lda,tau)&
                BIND(C,name="culaDeviceDgelqf")
            use ISO_C_BINDING
            integer(C_INT), value :: m
            integer(C_INT), value :: n
            type(C_PTR), value :: a
            integer(C_INT), value :: lda
            type(C_PTR), value :: tau
        end function
    end interface

    ! culaDeviceCgelqf(int m, int n, culaFloatComplex* a, int lda, culaFloatComplex* tau);
    interface
        integer(C_INT) function cula_device_cgelqf(m,n,a,lda,tau)&
                BIND(C,name="culaDeviceCgelqf")
            use ISO_C_BINDING
            integer(C_INT), value :: m
            integer(C_INT), value :: n
            type(C_PTR), value :: a
            integer(C_INT), value :: lda
            type(C_PTR), value :: tau
        end function
    end interface

    ! culaDeviceZgelqf(int m, int n, culaDoubleComplex* a, int lda, culaDoubleComplex* tau);
    interface
        integer(C_INT) function cula_device_zgelqf(m,n,a,lda,tau)&
                BIND(C,name="culaDeviceZgelqf")
            use ISO_C_BINDING
            integer(C_INT), value :: m
            integer(C_INT), value :: n
            type(C_PTR), value :: a
            integer(C_INT), value :: lda
            type(C_PTR), value :: tau
        end function
    end interface

    ! culaDeviceSgels(char trans, int m, int n, int nrhs, culaFloat* a, int lda, culaFloat* b, int ldb);
    interface
        integer(C_INT) function cula_device_sgels(trans,m,n,nrhs,a,lda,b,ldb)&
                BIND(C,name="culaDeviceSgels")
            use ISO_C_BINDING
            character(C_CHAR), value :: trans
            integer(C_INT), value :: m
            integer(C_INT), value :: n
            integer(C_INT), value :: nrhs
            type(C_PTR), value :: a
            integer(C_INT), value :: lda
            type(C_PTR), value :: b
            integer(C_INT), value :: ldb
        end function
    end interface

    ! culaDeviceDgels(char trans, int m, int n, int nrhs, culaDouble* a, int lda, culaDouble* b, int ldb);
    interface
        integer(C_INT) function cula_device_dgels(trans,m,n,nrhs,a,lda,b,ldb)&
                BIND(C,name="culaDeviceDgels")
            use ISO_C_BINDING
            character(C_CHAR), value :: trans
            integer(C_INT), value :: m
            integer(C_INT), value :: n
            integer(C_INT), value :: nrhs
            type(C_PTR), value :: a
            integer(C_INT), value :: lda
            type(C_PTR), value :: b
            integer(C_INT), value :: ldb
        end function
    end interface

    ! culaDeviceCgels(char trans, int m, int n, int nrhs, culaFloatComplex* a, int lda, culaFloatComplex* b, int ldb);
    interface
        integer(C_INT) function cula_device_cgels(trans,m,n,nrhs,a,lda,b,ldb)&
                BIND(C,name="culaDeviceCgels")
            use ISO_C_BINDING
            character(C_CHAR), value :: trans
            integer(C_INT), value :: m
            integer(C_INT), value :: n
            integer(C_INT), value :: nrhs
            type(C_PTR), value :: a
            integer(C_INT), value :: lda
            type(C_PTR), value :: b
            integer(C_INT), value :: ldb
        end function
    end interface

    ! culaDeviceZgels(char trans, int m, int n, int nrhs, culaDoubleComplex* a, int lda, culaDoubleComplex* b, int ldb);
    interface
        integer(C_INT) function cula_device_zgels(trans,m,n,nrhs,a,lda,b,ldb)&
                BIND(C,name="culaDeviceZgels")
            use ISO_C_BINDING
            character(C_CHAR), value :: trans
            integer(C_INT), value :: m
            integer(C_INT), value :: n
            integer(C_INT), value :: nrhs
            type(C_PTR), value :: a
            integer(C_INT), value :: lda
            type(C_PTR), value :: b
            integer(C_INT), value :: ldb
        end function
    end interface

    ! culaDeviceSgeqlf(int m, int n, culaFloat* a, int lda, culaFloat* tau);
    interface
        integer(C_INT) function cula_device_sgeqlf(m,n,a,lda,tau)&
                BIND(C,name="culaDeviceSgeqlf")
            use ISO_C_BINDING
            integer(C_INT), value :: m
            integer(C_INT), value :: n
            type(C_PTR), value :: a
            integer(C_INT), value :: lda
            type(C_PTR), value :: tau
        end function
    end interface

    ! culaDeviceDgeqlf(int m, int n, culaDouble* a, int lda, culaDouble* tau);
    interface
        integer(C_INT) function cula_device_dgeqlf(m,n,a,lda,tau)&
                BIND(C,name="culaDeviceDgeqlf")
            use ISO_C_BINDING
            integer(C_INT), value :: m
            integer(C_INT), value :: n
            type(C_PTR), value :: a
            integer(C_INT), value :: lda
            type(C_PTR), value :: tau
        end function
    end interface

    ! culaDeviceCgeqlf(int m, int n, culaFloatComplex* a, int lda, culaFloatComplex* tau);
    interface
        integer(C_INT) function cula_device_cgeqlf(m,n,a,lda,tau)&
                BIND(C,name="culaDeviceCgeqlf")
            use ISO_C_BINDING
            integer(C_INT), value :: m
            integer(C_INT), value :: n
            type(C_PTR), value :: a
            integer(C_INT), value :: lda
            type(C_PTR), value :: tau
        end function
    end interface

    ! culaDeviceZgeqlf(int m, int n, culaDoubleComplex* a, int lda, culaDoubleComplex* tau);
    interface
        integer(C_INT) function cula_device_zgeqlf(m,n,a,lda,tau)&
                BIND(C,name="culaDeviceZgeqlf")
            use ISO_C_BINDING
            integer(C_INT), value :: m
            integer(C_INT), value :: n
            type(C_PTR), value :: a
            integer(C_INT), value :: lda
            type(C_PTR), value :: tau
        end function
    end interface

    ! culaDeviceSgeqrf(int m, int n, culaFloat* a, int lda, culaFloat* tau);
    interface
        integer(C_INT) function cula_device_sgeqrf(m,n,a,lda,tau)&
                BIND(C,name="culaDeviceSgeqrf")
            use ISO_C_BINDING
            integer(C_INT), value :: m
            integer(C_INT), value :: n
            type (C_PTR), value :: a
            integer(C_INT), value :: lda
            type (C_PTR), value :: tau
        end function
    end interface

    ! culaDeviceDgeqrf(int m, int n, culaDouble* a, int lda, culaDouble* tau);
    interface
        integer(C_INT) function cula_device_dgeqrf(m,n,a,lda,tau)&
                BIND(C,name="culaDeviceDgeqrf")
            use ISO_C_BINDING
            integer(C_INT), value :: m
            integer(C_INT), value :: n
            type(C_PTR), value :: a
            integer(C_INT), value :: lda
            type(C_PTR), value :: tau
        end function
    end interface

    ! culaDeviceCgeqrf(int m, int n, culaFloatComplex* a, int lda, culaFloatComplex* tau);
    interface
        integer(C_INT) function cula_device_cgeqrf(m,n,a,lda,tau)&
                BIND(C,name="culaDeviceCgeqrf")
            use ISO_C_BINDING
            integer(C_INT), value :: m
            integer(C_INT), value :: n
            type(C_PTR), value :: a
            integer(C_INT), value :: lda
            type(C_PTR), value :: tau
        end function
    end interface

    ! culaDeviceZgeqrf(int m, int n, culaDoubleComplex* a, int lda, culaDoubleComplex* tau);
    interface
        integer(C_INT) function cula_device_zgeqrf(m,n,a,lda,tau)&
                BIND(C,name="culaDeviceZgeqrf")
            use ISO_C_BINDING
            integer(C_INT), value :: m
            integer(C_INT), value :: n
            type(C_PTR), value :: a
            integer(C_INT), value :: lda
            type(C_PTR), value :: tau
        end function
    end interface

    ! culaDeviceSgeqrfp(int m, int n, culaFloat* a, int lda, culaFloat* tau);
    interface
        integer(C_INT) function cula_device_sgeqrfp(m,n,a,lda,tau)&
                BIND(C,name="culaDeviceSgeqrfp")
            use ISO_C_BINDING
            integer(C_INT), value :: m
            integer(C_INT), value :: n
            type(C_PTR), value :: a
            integer(C_INT), value :: lda
            type(C_PTR), value :: tau
        end function
    end interface

    ! culaDeviceDgeqrfp(int m, int n, culaDouble* a, int lda, culaDouble* tau);
    interface
        integer(C_INT) function cula_device_dgeqrfp(m,n,a,lda,tau)&
                BIND(C,name="culaDeviceDgeqrfp")
            use ISO_C_BINDING
            integer(C_INT), value :: m
            integer(C_INT), value :: n
            type(C_PTR), value :: a
            integer(C_INT), value :: lda
            type(C_PTR), value :: tau
        end function
    end interface

    ! culaDeviceCgeqrfp(int m, int n, culaFloatComplex* a, int lda, culaFloatComplex* tau);
    interface
        integer(C_INT) function cula_device_cgeqrfp(m,n,a,lda,tau)&
                BIND(C,name="culaDeviceCgeqrfp")
            use ISO_C_BINDING
            integer(C_INT), value :: m
            integer(C_INT), value :: n
            type(C_PTR), value :: a
            integer(C_INT), value :: lda
            type(C_PTR), value :: tau
        end function
    end interface

    ! culaDeviceZgeqrfp(int m, int n, culaDoubleComplex* a, int lda, culaDoubleComplex* tau);
    interface
        integer(C_INT) function cula_device_zgeqrfp(m,n,a,lda,tau)&
                BIND(C,name="culaDeviceZgeqrfp")
            use ISO_C_BINDING
            integer(C_INT), value :: m
            integer(C_INT), value :: n
            type(C_PTR), value :: a
            integer(C_INT), value :: lda
            type(C_PTR), value :: tau
        end function
    end interface

    ! culaDeviceSgeqrs(int m, int n, int nrhs, culaFloat* a, int lda, culaFloat* tau, culaFloat* b, int ldb);
    interface
        integer(C_INT) function cula_device_sgeqrs(m,n,nrhs,a,lda,tau,b,ldb)&
                BIND(C,name="culaDeviceSgeqrs")
            use ISO_C_BINDING
            integer(C_INT), value :: m
            integer(C_INT), value :: n
            integer(C_INT), value :: nrhs
            type(C_PTR), value :: a
            integer(C_INT), value :: lda
            type(C_PTR), value :: tau
            type(C_PTR), value :: b
            integer(C_INT), value :: ldb
        end function
    end interface

    ! culaDeviceDgeqrs(int m, int n, int nrhs, culaDouble* a, int lda, culaDouble* tau, culaDouble* b, int ldb);
    interface
        integer(C_INT) function cula_device_dgeqrs(m,n,nrhs,a,lda,tau,b,ldb)&
                BIND(C,name="culaDeviceDgeqrs")
            use ISO_C_BINDING
            integer(C_INT), value :: m
            integer(C_INT), value :: n
            integer(C_INT), value :: nrhs
            type(C_PTR), value :: a
            integer(C_INT), value :: lda
            type(C_PTR), value :: tau
            type(C_PTR), value :: b
            integer(C_INT), value :: ldb
        end function
    end interface

    ! culaDeviceCgeqrs(int m, int n, int nrhs, culaFloatComplex* a, int lda, culaFloatComplex* tau, culaFloatComplex* b, int ldb);
    interface
        integer(C_INT) function cula_device_cgeqrs(m,n,nrhs,a,lda,tau,b,ldb)&
                BIND(C,name="culaDeviceCgeqrs")
            use ISO_C_BINDING
            integer(C_INT), value :: m
            integer(C_INT), value :: n
            integer(C_INT), value :: nrhs
            type(C_PTR), value :: a
            integer(C_INT), value :: lda
            type(C_PTR), value :: tau
            type(C_PTR), value :: b
            integer(C_INT), value :: ldb
        end function
    end interface

    ! culaDeviceZgeqrs(int m, int n, int nrhs, culaDoubleComplex* a, int lda, culaDoubleComplex* tau, culaDoubleComplex* b, int ldb);
    interface
        integer(C_INT) function cula_device_zgeqrs(m,n,nrhs,a,lda,tau,b,ldb)&
                BIND(C,name="culaDeviceZgeqrs")
            use ISO_C_BINDING
            integer(C_INT), value :: m
            integer(C_INT), value :: n
            integer(C_INT), value :: nrhs
            type(C_PTR), value :: a
            integer(C_INT), value :: lda
            type(C_PTR), value :: tau
            type(C_PTR), value :: b
            integer(C_INT), value :: ldb
        end function
    end interface

    ! culaDeviceSgerqf(int m, int n, culaFloat* a, int lda, culaFloat* tau);
    interface
        integer(C_INT) function cula_device_sgerqf(m,n,a,lda,tau)&
                BIND(C,name="culaDeviceSgerqf")
            use ISO_C_BINDING
            integer(C_INT), value :: m
            integer(C_INT), value :: n
            type(C_PTR), value :: a
            integer(C_INT), value :: lda
            type(C_PTR), value :: tau
        end function
    end interface

    ! culaDeviceDgerqf(int m, int n, culaDouble* a, int lda, culaDouble* tau);
    interface
        integer(C_INT) function cula_device_dgerqf(m,n,a,lda,tau)&
                BIND(C,name="culaDeviceDgerqf")
            use ISO_C_BINDING
            integer(C_INT), value :: m
            integer(C_INT), value :: n
            type(C_PTR), value :: a
            integer(C_INT), value :: lda
            type(C_PTR), value :: tau
        end function
    end interface

    ! culaDeviceCgerqf(int m, int n, culaFloatComplex* a, int lda, culaFloatComplex* tau);
    interface
        integer(C_INT) function cula_device_cgerqf(m,n,a,lda,tau)&
                BIND(C,name="culaDeviceCgerqf")
            use ISO_C_BINDING
            integer(C_INT), value :: m
            integer(C_INT), value :: n
            type(C_PTR), value :: a
            integer(C_INT), value :: lda
            type(C_PTR), value :: tau
        end function
    end interface

    ! culaDeviceZgerqf(int m, int n, culaDoubleComplex* a, int lda, culaDoubleComplex* tau);
    interface
        integer(C_INT) function cula_device_zgerqf(m,n,a,lda,tau)&
                BIND(C,name="culaDeviceZgerqf")
            use ISO_C_BINDING
            integer(C_INT), value :: m
            integer(C_INT), value :: n
            type(C_PTR), value :: a
            integer(C_INT), value :: lda
            type(C_PTR), value :: tau
        end function
    end interface

    ! culaDeviceSgesv(int n, int nrhs, culaFloat* a, int lda, culaInt* ipiv, culaFloat* b, int ldb);
    interface
        integer(C_INT) function cula_device_sgesv(n,nrhs,a,lda,ipiv,b,ldb)&
                BIND(C,name="culaDeviceSgesv")
            use ISO_C_BINDING
            integer(C_INT), value :: n
            integer(C_INT), value :: nrhs
            type(C_PTR), value :: a
            integer(C_INT), value :: lda
            type(C_PTR), value :: ipiv
            type(C_PTR), value :: b
            integer(C_INT), value :: ldb
        end function
    end interface

    ! culaDeviceDgesv(int n, int nrhs, culaDouble* a, int lda, culaInt* ipiv, culaDouble* b, int ldb);
    interface
        integer(C_INT) function cula_device_dgesv(n,nrhs,a,lda,ipiv,b,ldb)&
                BIND(C,name="culaDeviceDgesv")
            use ISO_C_BINDING
            integer(C_INT), value :: n
            integer(C_INT), value :: nrhs
            type(C_PTR), value :: a
            integer(C_INT), value :: lda
            type(C_PTR), value :: ipiv
            type(C_PTR), value :: b
            integer(C_INT), value :: ldb
        end function
    end interface

    ! culaDeviceCgesv(int n, int nrhs, culaFloatComplex* a, int lda, culaInt* ipiv, culaFloatComplex* b, int ldb);
    interface
        integer(C_INT) function cula_device_cgesv(n,nrhs,a,lda,ipiv,b,ldb)&
                BIND(C,name="culaDeviceCgesv")
            use ISO_C_BINDING
            integer(C_INT), value :: n
            integer(C_INT), value :: nrhs
            type(C_PTR), value :: a
            integer(C_INT), value :: lda
            type(C_PTR), value :: ipiv
            type(C_PTR), value :: b
            integer(C_INT), value :: ldb
        end function
    end interface

    ! culaDeviceZgesv(int n, int nrhs, culaDoubleComplex* a, int lda, culaInt* ipiv, culaDoubleComplex* b, int ldb);
    interface
        integer(C_INT) function cula_device_zgesv(n,nrhs,a,lda,ipiv,b,ldb)&
                BIND(C,name="culaDeviceZgesv")
            use ISO_C_BINDING
            integer(C_INT), value :: n
            integer(C_INT), value :: nrhs
            type(C_PTR), value :: a
            integer(C_INT), value :: lda
            type(C_PTR), value :: ipiv
            type(C_PTR), value :: b
            integer(C_INT), value :: ldb
        end function
    end interface

    ! culaDeviceDsgesv(int n, int nrhs, culaDouble* a, int lda, culaInt* ipiv, culaDouble* b, int ldb, culaDouble* x, int ldx, int* iter);
    interface
        integer(C_INT) function cula_device_dsgesv(n,nrhs,a,lda,ipiv,b,ldb,x,ldx,iter)&
                BIND(C,name="culaDeviceDsgesv")
            use ISO_C_BINDING
            integer(C_INT), value :: n
            integer(C_INT), value :: nrhs
            type(C_PTR), value :: a
            integer(C_INT), value :: lda
            type(C_PTR), value :: ipiv
            type(C_PTR), value :: b
            integer(C_INT), value :: ldb
            type(C_PTR), value :: x
            integer(C_INT), value :: ldx
            type(C_PTR), value :: iter
        end function
    end interface

    ! culaDeviceZcgesv(int n, int nrhs, culaDoubleComplex* a, int lda, culaInt* ipiv, culaDoubleComplex* b, int ldb, culaDoubleComplex* x, int ldx, int* iter);
    interface
        integer(C_INT) function cula_device_zcgesv(n,nrhs,a,lda,ipiv,b,ldb,x,ldx,iter)&
                BIND(C,name="culaDeviceZcgesv")
            use ISO_C_BINDING
            integer(C_INT), value :: n
            integer(C_INT), value :: nrhs
            type(C_PTR), value :: a
            integer(C_INT), value :: lda
            type(C_PTR), value :: ipiv
            type(C_PTR), value :: b
            integer(C_INT), value :: ldb
            type(C_PTR), value :: x
            integer(C_INT), value :: ldx
            type(C_PTR), value :: iter
        end function
    end interface

    ! culaDeviceSgesdd(char jobz, int m, int n, culaFloat* a, int lda, culaFloat* s, culaFloat* u, int ldu, culaFloat* vt, int ldvt);
    interface
        integer(C_INT) function cula_device_sgesdd(jobz,m,n,a,lda,s,u,ldu,vt,ldvt)&
                BIND(C,name="culaDeviceSgesdd")
            use ISO_C_BINDING
            character(C_CHAR), value :: jobz
            integer(C_INT), value :: m
            integer(C_INT), value :: n
            type(C_PTR), value :: a
            integer(C_INT), value :: lda
            type(C_PTR), value :: s
            type(C_PTR), value :: u
            integer(C_INT), value :: ldu
            type(C_PTR), value :: vt
            integer(C_INT), value :: ldvt
        end function
    end interface

    ! culaDeviceDgesdd(char jobz, int m, int n, culaDouble* a, int lda, culaDouble* s, culaDouble* u, int ldu, culaDouble* vt, int ldvt);
    interface
        integer(C_INT) function cula_device_dgesdd(jobz,m,n,a,lda,s,u,ldu,vt,ldvt)&
                BIND(C,name="culaDeviceDgesdd")
            use ISO_C_BINDING
            character(C_CHAR), value :: jobz
            integer(C_INT), value :: m
            integer(C_INT), value :: n
            type(C_PTR), value :: a
            integer(C_INT), value :: lda
            type(C_PTR), value :: s
            type(C_PTR), value :: u
            integer(C_INT), value :: ldu
            type(C_PTR), value :: vt
            integer(C_INT), value :: ldvt
        end function
    end interface

    ! culaDeviceCgesdd(char jobz, int m, int n, culaFloatComplex* a, int lda, culaFloat* s, culaFloatComplex* u, int ldu, culaFloatComplex* vt, int ldvt);
    interface
        integer(C_INT) function cula_device_cgesdd(jobz,m,n,a,lda,s,u,ldu,vt,ldvt)&
                BIND(C,name="culaDeviceCgesdd")
            use ISO_C_BINDING
            character(C_CHAR), value :: jobz
            integer(C_INT), value :: m
            integer(C_INT), value :: n
            type(C_PTR), value :: a
            integer(C_INT), value :: lda
            type(C_PTR), value :: s
            type(C_PTR), value :: u
            integer(C_INT), value :: ldu
            type(C_PTR), value :: vt
            integer(C_INT), value :: ldvt
        end function
    end interface

    ! culaDeviceZgesdd(char jobz, int m, int n, culaDoubleComplex* a, int lda, culaDouble* s, culaDoubleComplex* u, int ldu, culaDoubleComplex* vt, int ldvt);
    interface
        integer(C_INT) function cula_device_zgesdd(jobz,m,n,a,lda,s,u,ldu,vt,ldvt)&
                BIND(C,name="culaDeviceZgesdd")
            use ISO_C_BINDING
            character(C_CHAR), value :: jobz
            integer(C_INT), value :: m
            integer(C_INT), value :: n
            type(C_PTR), value :: a
            integer(C_INT), value :: lda
            type(C_PTR), value :: s
            type(C_PTR), value :: u
            integer(C_INT), value :: ldu
            type(C_PTR), value :: vt
            integer(C_INT), value :: ldvt
        end function
    end interface

    ! culaDeviceSgesvd(char jobu, char jobvt, int m, int n, culaFloat* a, int lda, culaFloat* s, culaFloat* u, int ldu, culaFloat* vt, int ldvt);
    interface
        integer(C_INT) function cula_device_sgesvd(jobu,jobvt,m,n,a,lda,s,u,ldu,vt,ldvt)&
                BIND(C,name="culaDeviceSgesvd")
            use ISO_C_BINDING
            character(C_CHAR), value :: jobu
            character(C_CHAR), value :: jobvt
            integer(C_INT), value :: m
            integer(C_INT), value :: n
            type(C_PTR), value :: a
            integer(C_INT), value :: lda
            type(C_PTR), value :: s
            type(C_PTR), value :: u
            integer(C_INT), value :: ldu
            type(C_PTR), value :: vt
            integer(C_INT), value :: ldvt
        end function
    end interface

    ! culaDeviceDgesvd(char jobu, char jobvt, int m, int n, culaDouble* a, int lda, culaDouble* s, culaDouble* u, int ldu, culaDouble* vt, int ldvt);
    interface
        integer(C_INT) function cula_device_dgesvd(jobu,jobvt,m,n,a,lda,s,u,ldu,vt,ldvt)&
                BIND(C,name="culaDeviceDgesvd")
            use ISO_C_BINDING
            character(C_CHAR), value :: jobu
            character(C_CHAR), value :: jobvt
            integer(C_INT), value :: m
            integer(C_INT), value :: n
            type(C_PTR), value :: a
            integer(C_INT), value :: lda
            type(C_PTR), value :: s
            type(C_PTR), value :: u
            integer(C_INT), value :: ldu
            type(C_PTR), value :: vt
            integer(C_INT), value :: ldvt
        end function
    end interface

    ! culaDeviceCgesvd(char jobu, char jobvt, int m, int n, culaFloatComplex* a, int lda, culaFloat* s, culaFloatComplex* u, int ldu, culaFloatComplex* vt, int ldvt);
    interface
        integer(C_INT) function cula_device_cgesvd(jobu,jobvt,m,n,a,lda,s,u,ldu,vt,ldvt)&
                BIND(C,name="culaDeviceCgesvd")
            use ISO_C_BINDING
            character(C_CHAR), value :: jobu
            character(C_CHAR), value :: jobvt
            integer(C_INT), value :: m
            integer(C_INT), value :: n
            type(C_PTR), value :: a
            integer(C_INT), value :: lda
            type(C_PTR), value :: s
            type(C_PTR), value :: u
            integer(C_INT), value :: ldu
            type(C_PTR), value :: vt
            integer(C_INT), value :: ldvt
        end function
    end interface

    ! culaDeviceZgesvd(char jobu, char jobvt, int m, int n, culaDoubleComplex* a, int lda, culaDouble* s, culaDoubleComplex* u, int ldu, culaDoubleComplex* vt, int ldvt);
    interface
        integer(C_INT) function cula_device_zgesvd(jobu,jobvt,m,n,a,lda,s,u,ldu,vt,ldvt)&
                BIND(C,name="culaDeviceZgesvd")
            use ISO_C_BINDING
            character(C_CHAR), value :: jobu
            character(C_CHAR), value :: jobvt
            integer(C_INT), value :: m
            integer(C_INT), value :: n
            type(C_PTR), value :: a
            integer(C_INT), value :: lda
            type(C_PTR), value :: s
            type(C_PTR), value :: u
            integer(C_INT), value :: ldu
            type(C_PTR), value :: vt
            integer(C_INT), value :: ldvt
        end function
    end interface

    ! culaDeviceSgetrf(int m, int n, culaFloat* a, int lda, culaInt* ipiv);
    interface
        integer(C_INT) function cula_device_sgetrf(m,n,a,lda,ipiv)&
                BIND(C,name="culaDeviceSgetrf")
            use ISO_C_BINDING
            integer(C_INT), value :: m
            integer(C_INT), value :: n
            type(C_PTR), value :: a
            integer(C_INT), value :: lda
            type(C_PTR), value :: ipiv
        end function
    end interface

    ! culaDeviceDgetrf(int m, int n, culaDouble* a, int lda, culaInt* ipiv);
    interface
        integer(C_INT) function cula_device_dgetrf(m,n,a,lda,ipiv)&
                BIND(C,name="culaDeviceDgetrf")
            use ISO_C_BINDING
            integer(C_INT), value :: m
            integer(C_INT), value :: n
            type(C_PTR), value :: a
            integer(C_INT), value :: lda
            type(C_PTR), value :: ipiv
        end function
    end interface

    ! culaDeviceCgetrf(int m, int n, culaFloatComplex* a, int lda, culaInt* ipiv);
    interface
        integer(C_INT) function cula_device_cgetrf(m,n,a,lda,ipiv)&
                BIND(C,name="culaDeviceCgetrf")
            use ISO_C_BINDING
            integer(C_INT), value :: m
            integer(C_INT), value :: n
            type(C_PTR), value :: a
            integer(C_INT), value :: lda
            type(C_PTR), value :: ipiv
        end function
    end interface

    ! culaDeviceZgetrf(int m, int n, culaDoubleComplex* a, int lda, culaInt* ipiv);
    interface
        integer(C_INT) function cula_device_zgetrf(m,n,a,lda,ipiv)&
                BIND(C,name="culaDeviceZgetrf")
            use ISO_C_BINDING
            integer(C_INT), value :: m
            integer(C_INT), value :: n
            type(C_PTR), value :: a
            integer(C_INT), value :: lda
            type(C_PTR), value :: ipiv
        end function
    end interface

    ! culaDeviceSgetri(int n, culaFloat* a, int lda, culaInt* ipiv);
    interface
        integer(C_INT) function cula_device_sgetri(n,a,lda,ipiv)&
                BIND(C,name="culaDeviceSgetri")
            use ISO_C_BINDING
            integer(C_INT), value :: n
            type(C_PTR), value :: a
            integer(C_INT), value :: lda
            type(C_PTR), value :: ipiv
        end function
    end interface

    ! culaDeviceDgetri(int n, culaDouble* a, int lda, culaInt* ipiv);
    interface
        integer(C_INT) function cula_device_dgetri(n,a,lda,ipiv)&
                BIND(C,name="culaDeviceDgetri")
            use ISO_C_BINDING
            integer(C_INT), value :: n
            type(C_PTR), value :: a
            integer(C_INT), value :: lda
            type(C_PTR), value :: ipiv
        end function
    end interface

    ! culaDeviceCgetri(int n, culaFloatComplex* a, int lda, culaInt* ipiv);
    interface
        integer(C_INT) function cula_device_cgetri(n,a,lda,ipiv)&
                BIND(C,name="culaDeviceCgetri")
            use ISO_C_BINDING
            integer(C_INT), value :: n
            type(C_PTR), value :: a
            integer(C_INT), value :: lda
            type(C_PTR), value :: ipiv
        end function
    end interface

    ! culaDeviceZgetri(int n, culaDoubleComplex* a, int lda, culaInt* ipiv);
    interface
        integer(C_INT) function cula_device_zgetri(n,a,lda,ipiv)&
                BIND(C,name="culaDeviceZgetri")
            use ISO_C_BINDING
            integer(C_INT), value :: n
            type(C_PTR), value :: a
            integer(C_INT), value :: lda
            type(C_PTR), value :: ipiv
        end function
    end interface

    ! culaDeviceSgetrs(char trans, int n, int nrhs, culaFloat* a, int lda, culaInt* ipiv, culaFloat* b, int ldb);
    interface
        integer(C_INT) function cula_device_sgetrs(trans,n,nrhs,a,lda,ipiv,b,ldb)&
                BIND(C,name="culaDeviceSgetrs")
            use ISO_C_BINDING
            character(C_CHAR), value :: trans
            integer(C_INT), value :: n
            integer(C_INT), value :: nrhs
            type(C_PTR), value :: a
            integer(C_INT), value :: lda
            type(C_PTR), value :: ipiv
            type(C_PTR), value :: b
            integer(C_INT), value :: ldb
        end function
    end interface

    ! culaDeviceDgetrs(char trans, int n, int nrhs, culaDouble* a, int lda, culaInt* ipiv, culaDouble* b, int ldb);
    interface
        integer(C_INT) function cula_device_dgetrs(trans,n,nrhs,a,lda,ipiv,b,ldb)&
                BIND(C,name="culaDeviceDgetrs")
            use ISO_C_BINDING
            character(C_CHAR), value :: trans
            integer(C_INT), value :: n
            integer(C_INT), value :: nrhs
            type(C_PTR), value :: a
            integer(C_INT), value :: lda
            type(C_PTR), value :: ipiv
            type(C_PTR), value :: b
            integer(C_INT), value :: ldb
        end function
    end interface

    ! culaDeviceCgetrs(char trans, int n, int nrhs, culaFloatComplex* a, int lda, culaInt* ipiv, culaFloatComplex* b, int ldb);
    interface
        integer(C_INT) function cula_device_cgetrs(trans,n,nrhs,a,lda,ipiv,b,ldb)&
                BIND(C,name="culaDeviceCgetrs")
            use ISO_C_BINDING
            character(C_CHAR), value :: trans
            integer(C_INT), value :: n
            integer(C_INT), value :: nrhs
            type(C_PTR), value :: a
            integer(C_INT), value :: lda
            type(C_PTR), value :: ipiv
            type(C_PTR), value :: b
            integer(C_INT), value :: ldb
        end function
    end interface

    ! culaDeviceZgetrs(char trans, int n, int nrhs, culaDoubleComplex* a, int lda, culaInt* ipiv, culaDoubleComplex* b, int ldb);
    interface
        integer(C_INT) function cula_device_zgetrs(trans,n,nrhs,a,lda,ipiv,b,ldb)&
                BIND(C,name="culaDeviceZgetrs")
            use ISO_C_BINDING
            character(C_CHAR), value :: trans
            integer(C_INT), value :: n
            integer(C_INT), value :: nrhs
            type(C_PTR), value :: a
            integer(C_INT), value :: lda
            type(C_PTR), value :: ipiv
            type(C_PTR), value :: b
            integer(C_INT), value :: ldb
        end function
    end interface

    ! culaDeviceSgglse(int m, int n, int p, culaFloat* a, int lda, culaFloat* b, int ldb, culaFloat* c, culaFloat* d, culaFloat* x);
    interface
        integer(C_INT) function cula_device_sgglse(m,n,p,a,lda,b,ldb,c,d,x)&
                BIND(C,name="culaDeviceSgglse")
            use ISO_C_BINDING
            integer(C_INT), value :: m
            integer(C_INT), value :: n
            integer(C_INT), value :: p
            type(C_PTR), value :: a
            integer(C_INT), value :: lda
            type(C_PTR), value :: b
            integer(C_INT), value :: ldb
            type(C_PTR), value :: c
            type(C_PTR), value :: d
            type(C_PTR), value :: x
        end function
    end interface

    ! culaDeviceDgglse(int m, int n, int p, culaDouble* a, int lda, culaDouble* b, int ldb, culaDouble* c, culaDouble* d, culaDouble* x);
    interface
        integer(C_INT) function cula_device_dgglse(m,n,p,a,lda,b,ldb,c,d,x)&
                BIND(C,name="culaDeviceDgglse")
            use ISO_C_BINDING
            integer(C_INT), value :: m
            integer(C_INT), value :: n
            integer(C_INT), value :: p
            type(C_PTR), value :: a
            integer(C_INT), value :: lda
            type(C_PTR), value :: b
            integer(C_INT), value :: ldb
            type(C_PTR), value :: c
            type(C_PTR), value :: d
            type(C_PTR), value :: x
        end function
    end interface

    ! culaDeviceCgglse(int m, int n, int p, culaFloatComplex* a, int lda, culaFloatComplex* b, int ldb, culaFloatComplex* c, culaFloatComplex* d, culaFloatComplex* x);
    interface
        integer(C_INT) function cula_device_cgglse(m,n,p,a,lda,b,ldb,c,d,x)&
                BIND(C,name="culaDeviceCgglse")
            use ISO_C_BINDING
            integer(C_INT), value :: m
            integer(C_INT), value :: n
            integer(C_INT), value :: p
            type(C_PTR), value :: a
            integer(C_INT), value :: lda
            type(C_PTR), value :: b
            integer(C_INT), value :: ldb
            type(C_PTR), value :: c
            type(C_PTR), value :: d
            type(C_PTR), value :: x
        end function
    end interface

    ! culaDeviceZgglse(int m, int n, int p, culaDoubleComplex* a, int lda, culaDoubleComplex* b, int ldb, culaDoubleComplex* c, culaDoubleComplex* d, culaDoubleComplex* x);
    interface
        integer(C_INT) function cula_device_zgglse(m,n,p,a,lda,b,ldb,c,d,x)&
                BIND(C,name="culaDeviceZgglse")
            use ISO_C_BINDING
            integer(C_INT), value :: m
            integer(C_INT), value :: n
            integer(C_INT), value :: p
            type(C_PTR), value :: a
            integer(C_INT), value :: lda
            type(C_PTR), value :: b
            integer(C_INT), value :: ldb
            type(C_PTR), value :: c
            type(C_PTR), value :: d
            type(C_PTR), value :: x
        end function
    end interface

    ! culaDeviceSggrqf(int m, int p, int n, culaFloat* a, int lda, culaFloat* taua, culaFloat* b, int ldb, culaFloat* taub);
    interface
        integer(C_INT) function cula_device_sggrqf(m,p,n,a,lda,taua,b,ldb,taub)&
                BIND(C,name="culaDeviceSggrqf")
            use ISO_C_BINDING
            integer(C_INT), value :: m
            integer(C_INT), value :: p
            integer(C_INT), value :: n
            type(C_PTR), value :: a
            integer(C_INT), value :: lda
            type(C_PTR), value :: taua
            type(C_PTR), value :: b
            integer(C_INT), value :: ldb
            type(C_PTR), value :: taub
        end function
    end interface

    ! culaDeviceDggrqf(int m, int p, int n, culaDouble* a, int lda, culaDouble* taua, culaDouble* b, int ldb, culaDouble* taub);
    interface
        integer(C_INT) function cula_device_dggrqf(m,p,n,a,lda,taua,b,ldb,taub)&
                BIND(C,name="culaDeviceDggrqf")
            use ISO_C_BINDING
            integer(C_INT), value :: m
            integer(C_INT), value :: p
            integer(C_INT), value :: n
            type(C_PTR), value :: a
            integer(C_INT), value :: lda
            type(C_PTR), value :: taua
            type(C_PTR), value :: b
            integer(C_INT), value :: ldb
            type(C_PTR), value :: taub
        end function
    end interface

    ! culaDeviceCggrqf(int m, int p, int n, culaFloatComplex* a, int lda, culaFloatComplex* taua, culaFloatComplex* b, int ldb, culaFloatComplex* taub);
    interface
        integer(C_INT) function cula_device_cggrqf(m,p,n,a,lda,taua,b,ldb,taub)&
                BIND(C,name="culaDeviceCggrqf")
            use ISO_C_BINDING
            integer(C_INT), value :: m
            integer(C_INT), value :: p
            integer(C_INT), value :: n
            type(C_PTR), value :: a
            integer(C_INT), value :: lda
            type(C_PTR), value :: taua
            type(C_PTR), value :: b
            integer(C_INT), value :: ldb
            type(C_PTR), value :: taub
        end function
    end interface

    ! culaDeviceZggrqf(int m, int p, int n, culaDoubleComplex* a, int lda, culaDoubleComplex* taua, culaDoubleComplex* b, int ldb, culaDoubleComplex* taub);
    interface
        integer(C_INT) function cula_device_zggrqf(m,p,n,a,lda,taua,b,ldb,taub)&
                BIND(C,name="culaDeviceZggrqf")
            use ISO_C_BINDING
            integer(C_INT), value :: m
            integer(C_INT), value :: p
            integer(C_INT), value :: n
            type(C_PTR), value :: a
            integer(C_INT), value :: lda
            type(C_PTR), value :: taua
            type(C_PTR), value :: b
            integer(C_INT), value :: ldb
            type(C_PTR), value :: taub
        end function
    end interface

    ! culaDeviceSlacpy(char uplo, int m, int n, culaFloat* a, int lda, culaFloat* b, int ldb);
    interface
        integer(C_INT) function cula_device_slacpy(uplo,m,n,a,lda,b,ldb)&
                BIND(C,name="culaDeviceSlacpy")
            use ISO_C_BINDING
            character(C_CHAR), value :: uplo
            integer(C_INT), value :: m
            integer(C_INT), value :: n
            type(C_PTR), value :: a
            integer(C_INT), value :: lda
            type(C_PTR), value :: b
            integer(C_INT), value :: ldb
        end function
    end interface

    ! culaDeviceDlacpy(char uplo, int m, int n, culaDouble* a, int lda, culaDouble* b, int ldb);
    interface
        integer(C_INT) function cula_device_dlacpy(uplo,m,n,a,lda,b,ldb)&
                BIND(C,name="culaDeviceDlacpy")
            use ISO_C_BINDING
            character(C_CHAR), value :: uplo
            integer(C_INT), value :: m
            integer(C_INT), value :: n
            type(C_PTR), value :: a
            integer(C_INT), value :: lda
            type(C_PTR), value :: b
            integer(C_INT), value :: ldb
        end function
    end interface

    ! culaDeviceClacpy(char uplo, int m, int n, culaFloatComplex* a, int lda, culaFloatComplex* b, int ldb);
    interface
        integer(C_INT) function cula_device_clacpy(uplo,m,n,a,lda,b,ldb)&
                BIND(C,name="culaDeviceClacpy")
            use ISO_C_BINDING
            character(C_CHAR), value :: uplo
            integer(C_INT), value :: m
            integer(C_INT), value :: n
            type(C_PTR), value :: a
            integer(C_INT), value :: lda
            type(C_PTR), value :: b
            integer(C_INT), value :: ldb
        end function
    end interface

    ! culaDeviceZlacpy(char uplo, int m, int n, culaDoubleComplex* a, int lda, culaDoubleComplex* b, int ldb);
    interface
        integer(C_INT) function cula_device_zlacpy(uplo,m,n,a,lda,b,ldb)&
                BIND(C,name="culaDeviceZlacpy")
            use ISO_C_BINDING
            character(C_CHAR), value :: uplo
            integer(C_INT), value :: m
            integer(C_INT), value :: n
            type(C_PTR), value :: a
            integer(C_INT), value :: lda
            type(C_PTR), value :: b
            integer(C_INT), value :: ldb
        end function
    end interface

    ! culaDeviceSlag2d(int m, int n, culaFloat* a, int lda, culaDouble* sa, int ldsa);
    interface
        integer(C_INT) function cula_device_slag2d(m,n,a,lda,sa,ldsa)&
                BIND(C,name="culaDeviceSlag2d")
            use ISO_C_BINDING
            integer(C_INT), value :: m
            integer(C_INT), value :: n
            type(C_PTR), value :: a
            integer(C_INT), value :: lda
            type(C_PTR), value :: sa
            integer(C_INT), value :: ldsa
        end function
    end interface

    ! culaDeviceDlag2s(int m, int n, culaDouble* a, int lda, culaFloat* sa, int ldsa);
    interface
        integer(C_INT) function cula_device_dlag2s(m,n,a,lda,sa,ldsa)&
                BIND(C,name="culaDeviceDlag2s")
            use ISO_C_BINDING
            integer(C_INT), value :: m
            integer(C_INT), value :: n
            type(C_PTR), value :: a
            integer(C_INT), value :: lda
            type(C_PTR), value :: sa
            integer(C_INT), value :: ldsa
        end function
    end interface

    ! culaDeviceClag2z(int m, int n, culaFloatComplex* a, int lda, culaDoubleComplex* sa, int ldsa);
    interface
        integer(C_INT) function cula_device_clag2z(m,n,a,lda,sa,ldsa)&
                BIND(C,name="culaDeviceClag2z")
            use ISO_C_BINDING
            integer(C_INT), value :: m
            integer(C_INT), value :: n
            type(C_PTR), value :: a
            integer(C_INT), value :: lda
            type(C_PTR), value :: sa
            integer(C_INT), value :: ldsa
        end function
    end interface

    ! culaDeviceZlag2c(int m, int n, culaDoubleComplex* a, int lda, culaFloatComplex* sa, int ldsa);
    interface
        integer(C_INT) function cula_device_zlag2c(m,n,a,lda,sa,ldsa)&
                BIND(C,name="culaDeviceZlag2c")
            use ISO_C_BINDING
            integer(C_INT), value :: m
            integer(C_INT), value :: n
            type(C_PTR), value :: a
            integer(C_INT), value :: lda
            type(C_PTR), value :: sa
            integer(C_INT), value :: ldsa
        end function
    end interface

    ! culaDeviceSlange(char norm, int m, int n, culaFloat* a, int lda, culaFloat* result);
    interface
        integer(C_INT) function cula_device_slange(norm,m,n,a,lda,result)&
                BIND(C,name="culaDeviceSlange")
            use ISO_C_BINDING
            character(C_CHAR), value :: norm
            integer(C_INT), value :: m
            integer(C_INT), value :: n
            type(C_PTR), value :: a
            integer(C_INT), value :: lda
            type(C_PTR), value :: result
        end function
    end interface

    ! culaDeviceDlange(char norm, int m, int n, culaDouble* a, int lda, culaDouble* result);
    interface
        integer(C_INT) function cula_device_dlange(norm,m,n,a,lda,result)&
                BIND(C,name="culaDeviceDlange")
            use ISO_C_BINDING
            character(C_CHAR), value :: norm
            integer(C_INT), value :: m
            integer(C_INT), value :: n
            type(C_PTR), value :: a
            integer(C_INT), value :: lda
            type(C_PTR), value :: result
        end function
    end interface

    ! culaDeviceClange(char norm, int m, int n, culaFloatComplex* a, int lda, culaFloat* result);
    interface
        integer(C_INT) function cula_device_clange(norm,m,n,a,lda,result)&
                BIND(C,name="culaDeviceClange")
            use ISO_C_BINDING
            character(C_CHAR), value :: norm
            integer(C_INT), value :: m
            integer(C_INT), value :: n
            type(C_PTR), value :: a
            integer(C_INT), value :: lda
            type(C_PTR), value :: result
        end function
    end interface

    ! culaDeviceZlange(char norm, int m, int n, culaDoubleComplex* a, int lda, culaDouble* result);
    interface
        integer(C_INT) function cula_device_zlange(norm,m,n,a,lda,result)&
                BIND(C,name="culaDeviceZlange")
            use ISO_C_BINDING
            character(C_CHAR), value :: norm
            integer(C_INT), value :: m
            integer(C_INT), value :: n
            type(C_PTR), value :: a
            integer(C_INT), value :: lda
            type(C_PTR), value :: result
        end function
    end interface

    ! culaDeviceSlanhe(char norm, char uplo, int n, culaFloat* a, int lda, culaFloat* result);
    interface
        integer(C_INT) function cula_device_slanhe(norm,uplo,n,a,lda,result)&
                BIND(C,name="culaDeviceSlanhe")
            use ISO_C_BINDING
            character(C_CHAR), value :: norm
            character(C_CHAR), value :: uplo
            integer(C_INT), value :: n
            type(C_PTR), value :: a
            integer(C_INT), value :: lda
            type(C_PTR), value :: result
        end function
    end interface

    ! culaDeviceDlanhe(char norm, char uplo, int n, culaDouble* a, int lda, culaDouble* result);
    interface
        integer(C_INT) function cula_device_dlanhe(norm,uplo,n,a,lda,result)&
                BIND(C,name="culaDeviceDlanhe")
            use ISO_C_BINDING
            character(C_CHAR), value :: norm
            character(C_CHAR), value :: uplo
            integer(C_INT), value :: n
            type(C_PTR), value :: a
            integer(C_INT), value :: lda
            type(C_PTR), value :: result
        end function
    end interface

    ! culaDeviceClanhe(char norm, char uplo, int n, culaFloatComplex* a, int lda, culaFloat* result);
    interface
        integer(C_INT) function cula_device_clanhe(norm,uplo,n,a,lda,result)&
                BIND(C,name="culaDeviceClanhe")
            use ISO_C_BINDING
            character(C_CHAR), value :: norm
            character(C_CHAR), value :: uplo
            integer(C_INT), value :: n
            type(C_PTR), value :: a
            integer(C_INT), value :: lda
            type(C_PTR), value :: result
        end function
    end interface

    ! culaDeviceZlanhe(char norm, char uplo, int n, culaDoubleComplex* a, int lda, culaDouble* result);
    interface
        integer(C_INT) function cula_device_zlanhe(norm,uplo,n,a,lda,result)&
                BIND(C,name="culaDeviceZlanhe")
            use ISO_C_BINDING
            character(C_CHAR), value :: norm
            character(C_CHAR), value :: uplo
            integer(C_INT), value :: n
            type(C_PTR), value :: a
            integer(C_INT), value :: lda
            type(C_PTR), value :: result
        end function
    end interface

    ! culaDeviceSlar2v(int n, culaFloat* x, culaFloat* y, culaFloat* z, int incx, culaFloat* c, culaFloat* s, int incc);
    interface
        integer(C_INT) function cula_device_slar2v(n,x,y,z,incx,c,s,incc)&
                BIND(C,name="culaDeviceSlar2v")
            use ISO_C_BINDING
            integer(C_INT), value :: n
            type(C_PTR), value :: x
            type(C_PTR), value :: y
            type(C_PTR), value :: z
            integer(C_INT), value :: incx
            type(C_PTR), value :: c
            type(C_PTR), value :: s
            integer(C_INT), value :: incc
        end function
    end interface

    ! culaDeviceDlar2v(int n, culaDouble* x, culaDouble* y, culaDouble* z, int incx, culaDouble* c, culaDouble* s, int incc);
    interface
        integer(C_INT) function cula_device_dlar2v(n,x,y,z,incx,c,s,incc)&
                BIND(C,name="culaDeviceDlar2v")
            use ISO_C_BINDING
            integer(C_INT), value :: n
            type(C_PTR), value :: x
            type(C_PTR), value :: y
            type(C_PTR), value :: z
            integer(C_INT), value :: incx
            type(C_PTR), value :: c
            type(C_PTR), value :: s
            integer(C_INT), value :: incc
        end function
    end interface

    ! culaDeviceClar2v(int n, culaFloatComplex* x, culaFloatComplex* y, culaFloatComplex* z, int incx, culaFloat* c, culaFloatComplex* s, int incc);
    interface
        integer(C_INT) function cula_device_clar2v(n,x,y,z,incx,c,s,incc)&
                BIND(C,name="culaDeviceClar2v")
            use ISO_C_BINDING
            integer(C_INT), value :: n
            type(C_PTR), value :: x
            type(C_PTR), value :: y
            type(C_PTR), value :: z
            integer(C_INT), value :: incx
            type(C_PTR), value :: c
            type(C_PTR), value :: s
            integer(C_INT), value :: incc
        end function
    end interface

    ! culaDeviceZlar2v(int n, culaDoubleComplex* x, culaDoubleComplex* y, culaDoubleComplex* z, int incx, culaDouble* c, culaDoubleComplex* s, int incc);
    interface
        integer(C_INT) function cula_device_zlar2v(n,x,y,z,incx,c,s,incc)&
                BIND(C,name="culaDeviceZlar2v")
            use ISO_C_BINDING
            integer(C_INT), value :: n
            type(C_PTR), value :: x
            type(C_PTR), value :: y
            type(C_PTR), value :: z
            integer(C_INT), value :: incx
            type(C_PTR), value :: c
            type(C_PTR), value :: s
            integer(C_INT), value :: incc
        end function
    end interface

    ! culaDeviceSlarfb(char side, char trans, char direct, char storev, int m, int n, int k, culaFloat* v, int ldv, culaFloat* t, int ldt, culaFloat* c, int ldc);
    interface
        integer(C_INT) function cula_device_slarfb(side,trans,direct,storev,m,n,k,v,ldv,t,ldt,c,ldc)&
                BIND(C,name="culaDeviceSlarfb")
            use ISO_C_BINDING
            character(C_CHAR), value :: side
            character(C_CHAR), value :: trans
            character(C_CHAR), value :: direct
            character(C_CHAR), value :: storev
            integer(C_INT), value :: m
            integer(C_INT), value :: n
            integer(C_INT), value :: k
            type(C_PTR), value :: v
            integer(C_INT), value :: ldv
            type(C_PTR), value :: t
            integer(C_INT), value :: ldt
            type(C_PTR), value :: c
            integer(C_INT), value :: ldc
        end function
    end interface

    ! culaDeviceDlarfb(char side, char trans, char direct, char storev, int m, int n, int k, culaDouble* v, int ldv, culaDouble* t, int ldt, culaDouble* c, int ldc);
    interface
        integer(C_INT) function cula_device_dlarfb(side,trans,direct,storev,m,n,k,v,ldv,t,ldt,c,ldc)&
                BIND(C,name="culaDeviceDlarfb")
            use ISO_C_BINDING
            character(C_CHAR), value :: side
            character(C_CHAR), value :: trans
            character(C_CHAR), value :: direct
            character(C_CHAR), value :: storev
            integer(C_INT), value :: m
            integer(C_INT), value :: n
            integer(C_INT), value :: k
            type(C_PTR), value :: v
            integer(C_INT), value :: ldv
            type(C_PTR), value :: t
            integer(C_INT), value :: ldt
            type(C_PTR), value :: c
            integer(C_INT), value :: ldc
        end function
    end interface

    ! culaDeviceClarfb(char side, char trans, char direct, char storev, int m, int n, int k, culaFloatComplex* v, int ldv, culaFloatComplex* t, int ldt, culaFloatComplex* c, int ldc);
    interface
        integer(C_INT) function cula_device_clarfb(side,trans,direct,storev,m,n,k,v,ldv,t,ldt,c,ldc)&
                BIND(C,name="culaDeviceClarfb")
            use ISO_C_BINDING
            character(C_CHAR), value :: side
            character(C_CHAR), value :: trans
            character(C_CHAR), value :: direct
            character(C_CHAR), value :: storev
            integer(C_INT), value :: m
            integer(C_INT), value :: n
            integer(C_INT), value :: k
            type(C_PTR), value :: v
            integer(C_INT), value :: ldv
            type(C_PTR), value :: t
            integer(C_INT), value :: ldt
            type(C_PTR), value :: c
            integer(C_INT), value :: ldc
        end function
    end interface

    ! culaDeviceZlarfb(char side, char trans, char direct, char storev, int m, int n, int k, culaDoubleComplex* v, int ldv, culaDoubleComplex* t, int ldt, culaDoubleComplex* c, int ldc);
    interface
        integer(C_INT) function cula_device_zlarfb(side,trans,direct,storev,m,n,k,v,ldv,t,ldt,c,ldc)&
                BIND(C,name="culaDeviceZlarfb")
            use ISO_C_BINDING
            character(C_CHAR), value :: side
            character(C_CHAR), value :: trans
            character(C_CHAR), value :: direct
            character(C_CHAR), value :: storev
            integer(C_INT), value :: m
            integer(C_INT), value :: n
            integer(C_INT), value :: k
            type(C_PTR), value :: v
            integer(C_INT), value :: ldv
            type(C_PTR), value :: t
            integer(C_INT), value :: ldt
            type(C_PTR), value :: c
            integer(C_INT), value :: ldc
        end function
    end interface

    ! culaDeviceSlarfg(int n, culaFloat* alpha, culaFloat* x, int incx, culaFloat* tau);
    interface
        integer(C_INT) function cula_device_slarfg(n,alpha,x,incx,tau)&
                BIND(C,name="culaDeviceSlarfg")
            use ISO_C_BINDING
            integer(C_INT), value :: n
            type(C_PTR), value :: alpha
            type(C_PTR), value :: x
            integer(C_INT), value :: incx
            type(C_PTR), value :: tau
        end function
    end interface

    ! culaDeviceDlarfg(int n, culaDouble* alpha, culaDouble* x, int incx, culaDouble* tau);
    interface
        integer(C_INT) function cula_device_dlarfg(n,alpha,x,incx,tau)&
                BIND(C,name="culaDeviceDlarfg")
            use ISO_C_BINDING
            integer(C_INT), value :: n
            type(C_PTR), value :: alpha
            type(C_PTR), value :: x
            integer(C_INT), value :: incx
            type(C_PTR), value :: tau
        end function
    end interface

    ! culaDeviceClarfg(int n, culaFloatComplex* alpha, culaFloatComplex* x, int incx, culaFloatComplex* tau);
    interface
        integer(C_INT) function cula_device_clarfg(n,alpha,x,incx,tau)&
                BIND(C,name="culaDeviceClarfg")
            use ISO_C_BINDING
            integer(C_INT), value :: n
            type(C_PTR), value :: alpha
            type(C_PTR), value :: x
            integer(C_INT), value :: incx
            type(C_PTR), value :: tau
        end function
    end interface

    ! culaDeviceZlarfg(int n, culaDoubleComplex* alpha, culaDoubleComplex* x, int incx, culaDoubleComplex* tau);
    interface
        integer(C_INT) function cula_device_zlarfg(n,alpha,x,incx,tau)&
                BIND(C,name="culaDeviceZlarfg")
            use ISO_C_BINDING
            integer(C_INT), value :: n
            type(C_PTR), value :: alpha
            type(C_PTR), value :: x
            integer(C_INT), value :: incx
            type(C_PTR), value :: tau
        end function
    end interface

    ! culaDeviceSlargv(int n, culaFloat* x, int incx, culaFloat* y, int incy, culaFloat* c, int incc);
    interface
        integer(C_INT) function cula_device_slargv(n,x,incx,y,incy,c,incc)&
                BIND(C,name="culaDeviceSlargv")
            use ISO_C_BINDING
            integer(C_INT), value :: n
            type(C_PTR), value :: x
            integer(C_INT), value :: incx
            type(C_PTR), value :: y
            integer(C_INT), value :: incy
            type(C_PTR), value :: c
            integer(C_INT), value :: incc
        end function
    end interface

    ! culaDeviceDlargv(int n, culaDouble* x, int incx, culaDouble* y, int incy, culaDouble* c, int incc);
    interface
        integer(C_INT) function cula_device_dlargv(n,x,incx,y,incy,c,incc)&
                BIND(C,name="culaDeviceDlargv")
            use ISO_C_BINDING
            integer(C_INT), value :: n
            type(C_PTR), value :: x
            integer(C_INT), value :: incx
            type(C_PTR), value :: y
            integer(C_INT), value :: incy
            type(C_PTR), value :: c
            integer(C_INT), value :: incc
        end function
    end interface

    ! culaDeviceClargv(int n, culaFloatComplex* x, int incx, culaFloatComplex* y, int incy, culaFloat* c, int incc);
    interface
        integer(C_INT) function cula_device_clargv(n,x,incx,y,incy,c,incc)&
                BIND(C,name="culaDeviceClargv")
            use ISO_C_BINDING
            integer(C_INT), value :: n
            type(C_PTR), value :: x
            integer(C_INT), value :: incx
            type(C_PTR), value :: y
            integer(C_INT), value :: incy
            type(C_PTR), value :: c
            integer(C_INT), value :: incc
        end function
    end interface

    ! culaDeviceZlargv(int n, culaDoubleComplex* x, int incx, culaDoubleComplex* y, int incy, culaDouble* c, int incc);
    interface
        integer(C_INT) function cula_device_zlargv(n,x,incx,y,incy,c,incc)&
                BIND(C,name="culaDeviceZlargv")
            use ISO_C_BINDING
            integer(C_INT), value :: n
            type(C_PTR), value :: x
            integer(C_INT), value :: incx
            type(C_PTR), value :: y
            integer(C_INT), value :: incy
            type(C_PTR), value :: c
            integer(C_INT), value :: incc
        end function
    end interface

    ! culaDeviceSlartv(int n, culaFloat* x, int incx, culaFloat* y, int incy, culaFloat* c, culaFloat* s, int incc);
    interface
        integer(C_INT) function cula_device_slartv(n,x,incx,y,incy,c,s,incc)&
                BIND(C,name="culaDeviceSlartv")
            use ISO_C_BINDING
            integer(C_INT), value :: n
            type(C_PTR), value :: x
            integer(C_INT), value :: incx
            type(C_PTR), value :: y
            integer(C_INT), value :: incy
            type(C_PTR), value :: c
            type(C_PTR), value :: s
            integer(C_INT), value :: incc
        end function
    end interface

    ! culaDeviceDlartv(int n, culaDouble* x, int incx, culaDouble* y, int incy, culaDouble* c, culaDouble* s, int incc);
    interface
        integer(C_INT) function cula_device_dlartv(n,x,incx,y,incy,c,s,incc)&
                BIND(C,name="culaDeviceDlartv")
            use ISO_C_BINDING
            integer(C_INT), value :: n
            type(C_PTR), value :: x
            integer(C_INT), value :: incx
            type(C_PTR), value :: y
            integer(C_INT), value :: incy
            type(C_PTR), value :: c
            type(C_PTR), value :: s
            integer(C_INT), value :: incc
        end function
    end interface

    ! culaDeviceClartv(int n, culaFloatComplex* x, int incx, culaFloatComplex* y, int incy, culaFloat* c, culaFloatComplex* s, int incc);
    interface
        integer(C_INT) function cula_device_clartv(n,x,incx,y,incy,c,s,incc)&
                BIND(C,name="culaDeviceClartv")
            use ISO_C_BINDING
            integer(C_INT), value :: n
            type(C_PTR), value :: x
            integer(C_INT), value :: incx
            type(C_PTR), value :: y
            integer(C_INT), value :: incy
            type(C_PTR), value :: c
            type(C_PTR), value :: s
            integer(C_INT), value :: incc
        end function
    end interface

    ! culaDeviceZlartv(int n, culaDoubleComplex* x, int incx, culaDoubleComplex* y, int incy, culaDouble* c, culaDoubleComplex* s, int incc);
    interface
        integer(C_INT) function cula_device_zlartv(n,x,incx,y,incy,c,s,incc)&
                BIND(C,name="culaDeviceZlartv")
            use ISO_C_BINDING
            integer(C_INT), value :: n
            type(C_PTR), value :: x
            integer(C_INT), value :: incx
            type(C_PTR), value :: y
            integer(C_INT), value :: incy
            type(C_PTR), value :: c
            type(C_PTR), value :: s
            integer(C_INT), value :: incc
        end function
    end interface

    ! culaDeviceSlascl(char type, int kl, int ku, culaFloat cfrom, culaFloat cto, int m, int n, culaFloat* a, int lda);
    interface
        integer(C_INT) function cula_device_slascl(type,kl,ku,cfrom,cto,m,n,a,lda)&
                BIND(C,name="culaDeviceSlascl")
            use ISO_C_BINDING
            character(C_CHAR), value :: type
            integer(C_INT), value :: kl
            integer(C_INT), value :: ku
            real(C_FLOAT) :: cfrom
            real(C_FLOAT) :: cto
            integer(C_INT), value :: m
            integer(C_INT), value :: n
            type(C_PTR), value :: a
            integer(C_INT), value :: lda
        end function
    end interface

    ! culaDeviceDlascl(char type, int kl, int ku, culaDouble cfrom, culaDouble cto, int m, int n, culaDouble* a, int lda);
    interface
        integer(C_INT) function cula_device_dlascl(type,kl,ku,cfrom,cto,m,n,a,lda)&
                BIND(C,name="culaDeviceDlascl")
            use ISO_C_BINDING
            character(C_CHAR), value :: type
            integer(C_INT), value :: kl
            integer(C_INT), value :: ku
            real(C_DOUBLE), value :: cfrom
            real(C_DOUBLE), value :: cto
            integer(C_INT), value :: m
            integer(C_INT), value :: n
            type(C_PTR), value :: a
            integer(C_INT), value :: lda
        end function
    end interface

    ! culaDeviceClascl(char type, int kl, int ku, culaFloat cfrom, culaFloat cto, int m, int n, culaFloatComplex* a, int lda);
    interface
        integer(C_INT) function cula_device_clascl(type,kl,ku,cfrom,cto,m,n,a,lda)&
                BIND(C,name="culaDeviceClascl")
            use ISO_C_BINDING
            character(C_CHAR), value :: type
            integer(C_INT), value :: kl
            integer(C_INT), value :: ku
            real(C_FLOAT) :: cfrom
            real(C_FLOAT) :: cto
            integer(C_INT), value :: m
            integer(C_INT), value :: n
            type(C_PTR), value :: a
            integer(C_INT), value :: lda
        end function
    end interface

    ! culaDeviceZlascl(char type, int kl, int ku, culaDouble cfrom, culaDouble cto, int m, int n, culaDoubleComplex* a, int lda);
    interface
        integer(C_INT) function cula_device_zlascl(type,kl,ku,cfrom,cto,m,n,a,lda)&
                BIND(C,name="culaDeviceZlascl")
            use ISO_C_BINDING
            character(C_CHAR), value :: type
            integer(C_INT), value :: kl
            integer(C_INT), value :: ku
            real(C_DOUBLE), value :: cfrom
            real(C_DOUBLE), value :: cto
            integer(C_INT), value :: m
            integer(C_INT), value :: n
            type(C_PTR), value :: a
            integer(C_INT), value :: lda
        end function
    end interface

    ! culaDeviceSlaset(char uplo, int m, int n, culaFloat alpha, culaFloat beta, culaFloat* a, int lda);
    interface
        integer(C_INT) function cula_device_slaset(uplo,m,n,alpha,beta,a,lda)&
                BIND(C,name="culaDeviceSlaset")
            use ISO_C_BINDING
            character(C_CHAR), value :: uplo
            integer(C_INT), value :: m
            integer(C_INT), value :: n
            real(C_FLOAT) :: alpha
            real(C_FLOAT) :: beta
            type(C_PTR), value :: a
            integer(C_INT), value :: lda
        end function
    end interface

    ! culaDeviceDlaset(char uplo, int m, int n, culaDouble alpha, culaDouble beta, culaDouble* a, int lda);
    interface
        integer(C_INT) function cula_device_dlaset(uplo,m,n,alpha,beta,a,lda)&
                BIND(C,name="culaDeviceDlaset")
            use ISO_C_BINDING
            character(C_CHAR), value :: uplo
            integer(C_INT), value :: m
            integer(C_INT), value :: n
            real(C_DOUBLE), value :: alpha
            real(C_DOUBLE), value :: beta
            type(C_PTR), value :: a
            integer(C_INT), value :: lda
        end function
    end interface

    ! culaDeviceClaset(char uplo, int m, int n, culaFloatComplex alpha, culaFloatComplex beta, culaFloatComplex* a, int lda);
    interface
        integer(C_INT) function cula_device_claset(uplo,m,n,alpha,beta,a,lda)&
                BIND(C,name="culaDeviceClaset")
            use ISO_C_BINDING
            character(C_CHAR), value :: uplo
            integer(C_INT), value :: m
            integer(C_INT), value :: n
            complex(C_FLOAT_COMPLEX), value :: alpha
            complex(C_FLOAT_COMPLEX), value :: beta
            type(C_PTR), value :: a
            integer(C_INT), value :: lda
        end function
    end interface

    ! culaDeviceZlaset(char uplo, int m, int n, culaDoubleComplex alpha, culaDoubleComplex beta, culaDoubleComplex* a, int lda);
    interface
        integer(C_INT) function cula_device_zlaset(uplo,m,n,alpha,beta,a,lda)&
                BIND(C,name="culaDeviceZlaset")
            use ISO_C_BINDING
            character(C_CHAR), value :: uplo
            integer(C_INT), value :: m
            integer(C_INT), value :: n
            complex(C_DOUBLE_COMPLEX), value :: alpha
            complex(C_DOUBLE_COMPLEX), value :: beta
            type(C_PTR), value :: a
            integer(C_INT), value :: lda
        end function
    end interface

    ! culaDeviceSlasr(char side, char pivot, char direct, int m, int n, culaFloat* c, culaFloat* s, culaFloat* a, int lda);
    interface
        integer(C_INT) function cula_device_slasr(side,pivot,direct,m,n,c,s,a,lda)&
                BIND(C,name="culaDeviceSlasr")
            use ISO_C_BINDING
            character(C_CHAR), value :: side
            character(C_CHAR), value :: pivot
            character(C_CHAR), value :: direct
            integer(C_INT), value :: m
            integer(C_INT), value :: n
            type(C_PTR), value :: c
            type(C_PTR), value :: s
            type(C_PTR), value :: a
            integer(C_INT), value :: lda
        end function
    end interface

    ! culaDeviceDlasr(char side, char pivot, char direct, int m, int n, culaDouble* c, culaDouble* s, culaDouble* a, int lda);
    interface
        integer(C_INT) function cula_device_dlasr(side,pivot,direct,m,n,c,s,a,lda)&
                BIND(C,name="culaDeviceDlasr")
            use ISO_C_BINDING
            character(C_CHAR), value :: side
            character(C_CHAR), value :: pivot
            character(C_CHAR), value :: direct
            integer(C_INT), value :: m
            integer(C_INT), value :: n
            type(C_PTR), value :: c
            type(C_PTR), value :: s
            type(C_PTR), value :: a
            integer(C_INT), value :: lda
        end function
    end interface

    ! culaDeviceClasr(char side, char pivot, char direct, int m, int n, culaFloat* c, culaFloat* s, culaFloatComplex* a, int lda);
    interface
        integer(C_INT) function cula_device_clasr(side,pivot,direct,m,n,c,s,a,lda)&
                BIND(C,name="culaDeviceClasr")
            use ISO_C_BINDING
            character(C_CHAR), value :: side
            character(C_CHAR), value :: pivot
            character(C_CHAR), value :: direct
            integer(C_INT), value :: m
            integer(C_INT), value :: n
            type(C_PTR), value :: c
            type(C_PTR), value :: s
            type(C_PTR), value :: a
            integer(C_INT), value :: lda
        end function
    end interface

    ! culaDeviceZlasr(char side, char pivot, char direct, int m, int n, culaDouble* c, culaDouble* s, culaDoubleComplex* a, int lda);
    interface
        integer(C_INT) function cula_device_zlasr(side,pivot,direct,m,n,c,s,a,lda)&
                BIND(C,name="culaDeviceZlasr")
            use ISO_C_BINDING
            character(C_CHAR), value :: side
            character(C_CHAR), value :: pivot
            character(C_CHAR), value :: direct
            integer(C_INT), value :: m
            integer(C_INT), value :: n
            type(C_PTR), value :: c
            type(C_PTR), value :: s
            type(C_PTR), value :: a
            integer(C_INT), value :: lda
        end function
    end interface

    ! culaDeviceSlaswp(int n, culaFloat* a, int lda, int k1, int k2, culaInt* ipiv, int incx);
    interface
        integer(C_INT) function cula_device_slaswp(n,a,lda,k1,k2,ipiv,incx)&
                BIND(C,name="culaDeviceSlaswp")
            use ISO_C_BINDING
            integer(C_INT), value :: n
            type(C_PTR), value :: a
            integer(C_INT), value :: lda
            integer(C_INT), value :: k1
            integer(C_INT), value :: k2
            type(C_PTR), value :: ipiv
            integer(C_INT), value :: incx
        end function
    end interface

    ! culaDeviceDlaswp(int n, culaDouble* a, int lda, int k1, int k2, culaInt* ipiv, int incx);
    interface
        integer(C_INT) function cula_device_dlaswp(n,a,lda,k1,k2,ipiv,incx)&
                BIND(C,name="culaDeviceDlaswp")
            use ISO_C_BINDING
            integer(C_INT), value :: n
            type(C_PTR), value :: a
            integer(C_INT), value :: lda
            integer(C_INT), value :: k1
            integer(C_INT), value :: k2
            type(C_PTR), value :: ipiv
            integer(C_INT), value :: incx
        end function
    end interface

    ! culaDeviceClaswp(int n, culaFloatComplex* a, int lda, int k1, int k2, culaInt* ipiv, int incx);
    interface
        integer(C_INT) function cula_device_claswp(n,a,lda,k1,k2,ipiv,incx)&
                BIND(C,name="culaDeviceClaswp")
            use ISO_C_BINDING
            integer(C_INT), value :: n
            type(C_PTR), value :: a
            integer(C_INT), value :: lda
            integer(C_INT), value :: k1
            integer(C_INT), value :: k2
            type(C_PTR), value :: ipiv
            integer(C_INT), value :: incx
        end function
    end interface

    ! culaDeviceZlaswp(int n, culaDoubleComplex* a, int lda, int k1, int k2, culaInt* ipiv, int incx);
    interface
        integer(C_INT) function cula_device_zlaswp(n,a,lda,k1,k2,ipiv,incx)&
                BIND(C,name="culaDeviceZlaswp")
            use ISO_C_BINDING
            integer(C_INT), value :: n
            type(C_PTR), value :: a
            integer(C_INT), value :: lda
            integer(C_INT), value :: k1
            integer(C_INT), value :: k2
            type(C_PTR), value :: ipiv
            integer(C_INT), value :: incx
        end function
    end interface

    ! culaDeviceSlaswpcol(int n, culaFloat* a, int lda, int k1, int k2, culaInt* ipiv, int incx);
    interface
        integer(C_INT) function cula_device_slaswpcol(n,a,lda,k1,k2,ipiv,incx)&
                BIND(C,name="culaDeviceSlaswpcol")
            use ISO_C_BINDING
            integer(C_INT), value :: n
            type(C_PTR), value :: a
            integer(C_INT), value :: lda
            integer(C_INT), value :: k1
            integer(C_INT), value :: k2
            type(C_PTR), value :: ipiv
            integer(C_INT), value :: incx
        end function
    end interface

    ! culaDeviceDlaswpcol(int n, culaDouble* a, int lda, int k1, int k2, culaInt* ipiv, int incx);
    interface
        integer(C_INT) function cula_device_dlaswpcol(n,a,lda,k1,k2,ipiv,incx)&
                BIND(C,name="culaDeviceDlaswpcol")
            use ISO_C_BINDING
            integer(C_INT), value :: n
            type(C_PTR), value :: a
            integer(C_INT), value :: lda
            integer(C_INT), value :: k1
            integer(C_INT), value :: k2
            type(C_PTR), value :: ipiv
            integer(C_INT), value :: incx
        end function
    end interface

    ! culaDeviceClaswpcol(int n, culaFloatComplex* a, int lda, int k1, int k2, culaInt* ipiv, int incx);
    interface
        integer(C_INT) function cula_device_claswpcol(n,a,lda,k1,k2,ipiv,incx)&
                BIND(C,name="culaDeviceClaswpcol")
            use ISO_C_BINDING
            integer(C_INT), value :: n
            type(C_PTR), value :: a
            integer(C_INT), value :: lda
            integer(C_INT), value :: k1
            integer(C_INT), value :: k2
            type(C_PTR), value :: ipiv
            integer(C_INT), value :: incx
        end function
    end interface

    ! culaDeviceZlaswpcol(int n, culaDoubleComplex* a, int lda, int k1, int k2, culaInt* ipiv, int incx);
    interface
        integer(C_INT) function cula_device_zlaswpcol(n,a,lda,k1,k2,ipiv,incx)&
                BIND(C,name="culaDeviceZlaswpcol")
            use ISO_C_BINDING
            integer(C_INT), value :: n
            type(C_PTR), value :: a
            integer(C_INT), value :: lda
            integer(C_INT), value :: k1
            integer(C_INT), value :: k2
            type(C_PTR), value :: ipiv
            integer(C_INT), value :: incx
        end function
    end interface

    ! culaDeviceSlat2d(char uplo, int n, culaFloat* a, int lda, culaDouble* sa, int ldsa);
    interface
        integer(C_INT) function cula_device_slat2d(uplo,n,a,lda,sa,ldsa)&
                BIND(C,name="culaDeviceSlat2d")
            use ISO_C_BINDING
            character(C_CHAR), value :: uplo
            integer(C_INT), value :: n
            type(C_PTR), value :: a
            integer(C_INT), value :: lda
            type(C_PTR), value :: sa
            integer(C_INT), value :: ldsa
        end function
    end interface

    ! culaDeviceDlat2s(char uplo, int n, culaDouble* a, int lda, culaFloat* sa, int ldsa);
    interface
        integer(C_INT) function cula_device_dlat2s(uplo,n,a,lda,sa,ldsa)&
                BIND(C,name="culaDeviceDlat2s")
            use ISO_C_BINDING
            character(C_CHAR), value :: uplo
            integer(C_INT), value :: n
            type(C_PTR), value :: a
            integer(C_INT), value :: lda
            type(C_PTR), value :: sa
            integer(C_INT), value :: ldsa
        end function
    end interface

    ! culaDeviceClat2z(char uplo, int n, culaFloatComplex* a, int lda, culaDoubleComplex* sa, int ldsa);
    interface
        integer(C_INT) function cula_device_clat2z(uplo,n,a,lda,sa,ldsa)&
                BIND(C,name="culaDeviceClat2z")
            use ISO_C_BINDING
            character(C_CHAR), value :: uplo
            integer(C_INT), value :: n
            type(C_PTR), value :: a
            integer(C_INT), value :: lda
            type(C_PTR), value :: sa
            integer(C_INT), value :: ldsa
        end function
    end interface

    ! culaDeviceZlat2c(char uplo, int n, culaDoubleComplex* a, int lda, culaFloatComplex* sa, int ldsa);
    interface
        integer(C_INT) function cula_device_zlat2c(uplo,n,a,lda,sa,ldsa)&
                BIND(C,name="culaDeviceZlat2c")
            use ISO_C_BINDING
            character(C_CHAR), value :: uplo
            integer(C_INT), value :: n
            type(C_PTR), value :: a
            integer(C_INT), value :: lda
            type(C_PTR), value :: sa
            integer(C_INT), value :: ldsa
        end function
    end interface

    ! culaDeviceSorgbr(char vect, int m, int n, int k, culaFloat* a, int lda, culaFloat* tau);
    interface
        integer(C_INT) function cula_device_sorgbr(vect,m,n,k,a,lda,tau)&
                BIND(C,name="culaDeviceSorgbr")
            use ISO_C_BINDING
            character(C_CHAR), value :: vect
            integer(C_INT), value :: m
            integer(C_INT), value :: n
            integer(C_INT), value :: k
            type(C_PTR), value :: a
            integer(C_INT), value :: lda
            type(C_PTR), value :: tau
        end function
    end interface

    ! culaDeviceDorgbr(char vect, int m, int n, int k, culaDouble* a, int lda, culaDouble* tau);
    interface
        integer(C_INT) function cula_device_dorgbr(vect,m,n,k,a,lda,tau)&
                BIND(C,name="culaDeviceDorgbr")
            use ISO_C_BINDING
            character(C_CHAR), value :: vect
            integer(C_INT), value :: m
            integer(C_INT), value :: n
            integer(C_INT), value :: k
            type(C_PTR), value :: a
            integer(C_INT), value :: lda
            type(C_PTR), value :: tau
        end function
    end interface

    ! culaDeviceCungbr(char vect, int m, int n, int k, culaFloatComplex* a, int lda, culaFloatComplex* tau);
    interface
        integer(C_INT) function cula_device_cungbr(vect,m,n,k,a,lda,tau)&
                BIND(C,name="culaDeviceCungbr")
            use ISO_C_BINDING
            character(C_CHAR), value :: vect
            integer(C_INT), value :: m
            integer(C_INT), value :: n
            integer(C_INT), value :: k
            type(C_PTR), value :: a
            integer(C_INT), value :: lda
            type(C_PTR), value :: tau
        end function
    end interface

    ! culaDeviceZungbr(char vect, int m, int n, int k, culaDoubleComplex* a, int lda, culaDoubleComplex* tau);
    interface
        integer(C_INT) function cula_device_zungbr(vect,m,n,k,a,lda,tau)&
                BIND(C,name="culaDeviceZungbr")
            use ISO_C_BINDING
            character(C_CHAR), value :: vect
            integer(C_INT), value :: m
            integer(C_INT), value :: n
            integer(C_INT), value :: k
            type(C_PTR), value :: a
            integer(C_INT), value :: lda
            type(C_PTR), value :: tau
        end function
    end interface

    ! culaDeviceSorghr(int n, int ilo, int ihi, culaFloat* a, int lda, culaFloat* tau);
    interface
        integer(C_INT) function cula_device_sorghr(n,ilo,ihi,a,lda,tau)&
                BIND(C,name="culaDeviceSorghr")
            use ISO_C_BINDING
            integer(C_INT), value :: n
            integer(C_INT), value :: ilo
            integer(C_INT), value :: ihi
            type(C_PTR), value :: a
            integer(C_INT), value :: lda
            type(C_PTR), value :: tau
        end function
    end interface

    ! culaDeviceDorghr(int n, int ilo, int ihi, culaDouble* a, int lda, culaDouble* tau);
    interface
        integer(C_INT) function cula_device_dorghr(n,ilo,ihi,a,lda,tau)&
                BIND(C,name="culaDeviceDorghr")
            use ISO_C_BINDING
            integer(C_INT), value :: n
            integer(C_INT), value :: ilo
            integer(C_INT), value :: ihi
            type(C_PTR), value :: a
            integer(C_INT), value :: lda
            type(C_PTR), value :: tau
        end function
    end interface

    ! culaDeviceCunghr(int n, int ilo, int ihi, culaFloatComplex* a, int lda, culaFloatComplex* tau);
    interface
        integer(C_INT) function cula_device_cunghr(n,ilo,ihi,a,lda,tau)&
                BIND(C,name="culaDeviceCunghr")
            use ISO_C_BINDING
            integer(C_INT), value :: n
            integer(C_INT), value :: ilo
            integer(C_INT), value :: ihi
            type(C_PTR), value :: a
            integer(C_INT), value :: lda
            type(C_PTR), value :: tau
        end function
    end interface

    ! culaDeviceZunghr(int n, int ilo, int ihi, culaDoubleComplex* a, int lda, culaDoubleComplex* tau);
    interface
        integer(C_INT) function cula_device_zunghr(n,ilo,ihi,a,lda,tau)&
                BIND(C,name="culaDeviceZunghr")
            use ISO_C_BINDING
            integer(C_INT), value :: n
            integer(C_INT), value :: ilo
            integer(C_INT), value :: ihi
            type(C_PTR), value :: a
            integer(C_INT), value :: lda
            type(C_PTR), value :: tau
        end function
    end interface

    ! culaDeviceSorglq(int m, int n, int k, culaFloat* a, int lda, culaFloat* tau);
    interface
        integer(C_INT) function cula_device_sorglq(m,n,k,a,lda,tau)&
                BIND(C,name="culaDeviceSorglq")
            use ISO_C_BINDING
            integer(C_INT), value :: m
            integer(C_INT), value :: n
            integer(C_INT), value :: k
            type(C_PTR), value :: a
            integer(C_INT), value :: lda
            type(C_PTR), value :: tau
        end function
    end interface

    ! culaDeviceDorglq(int m, int n, int k, culaDouble* a, int lda, culaDouble* tau);
    interface
        integer(C_INT) function cula_device_dorglq(m,n,k,a,lda,tau)&
                BIND(C,name="culaDeviceDorglq")
            use ISO_C_BINDING
            integer(C_INT), value :: m
            integer(C_INT), value :: n
            integer(C_INT), value :: k
            type(C_PTR), value :: a
            integer(C_INT), value :: lda
            type(C_PTR), value :: tau
        end function
    end interface

    ! culaDeviceCunglq(int m, int n, int k, culaFloatComplex* a, int lda, culaFloatComplex* tau);
    interface
        integer(C_INT) function cula_device_cunglq(m,n,k,a,lda,tau)&
                BIND(C,name="culaDeviceCunglq")
            use ISO_C_BINDING
            integer(C_INT), value :: m
            integer(C_INT), value :: n
            integer(C_INT), value :: k
            type(C_PTR), value :: a
            integer(C_INT), value :: lda
            type(C_PTR), value :: tau
        end function
    end interface

    ! culaDeviceZunglq(int m, int n, int k, culaDoubleComplex* a, int lda, culaDoubleComplex* tau);
    interface
        integer(C_INT) function cula_device_zunglq(m,n,k,a,lda,tau)&
                BIND(C,name="culaDeviceZunglq")
            use ISO_C_BINDING
            integer(C_INT), value :: m
            integer(C_INT), value :: n
            integer(C_INT), value :: k
            type(C_PTR), value :: a
            integer(C_INT), value :: lda
            type(C_PTR), value :: tau
        end function
    end interface

    ! culaDeviceSorgql(int m, int n, int k, culaFloat* a, int lda, culaFloat* tau);
    interface
        integer(C_INT) function cula_device_sorgql(m,n,k,a,lda,tau)&
                BIND(C,name="culaDeviceSorgql")
            use ISO_C_BINDING
            integer(C_INT), value :: m
            integer(C_INT), value :: n
            integer(C_INT), value :: k
            type(C_PTR), value :: a
            integer(C_INT), value :: lda
            type(C_PTR), value :: tau
        end function
    end interface

    ! culaDeviceDorgql(int m, int n, int k, culaDouble* a, int lda, culaDouble* tau);
    interface
        integer(C_INT) function cula_device_dorgql(m,n,k,a,lda,tau)&
                BIND(C,name="culaDeviceDorgql")
            use ISO_C_BINDING
            integer(C_INT), value :: m
            integer(C_INT), value :: n
            integer(C_INT), value :: k
            type(C_PTR), value :: a
            integer(C_INT), value :: lda
            type(C_PTR), value :: tau
        end function
    end interface

    ! culaDeviceCungql(int m, int n, int k, culaFloatComplex* a, int lda, culaFloatComplex* tau);
    interface
        integer(C_INT) function cula_device_cungql(m,n,k,a,lda,tau)&
                BIND(C,name="culaDeviceCungql")
            use ISO_C_BINDING
            integer(C_INT), value :: m
            integer(C_INT), value :: n
            integer(C_INT), value :: k
            type(C_PTR), value :: a
            integer(C_INT), value :: lda
            type(C_PTR), value :: tau
        end function
    end interface

    ! culaDeviceZungql(int m, int n, int k, culaDoubleComplex* a, int lda, culaDoubleComplex* tau);
    interface
        integer(C_INT) function cula_device_zungql(m,n,k,a,lda,tau)&
                BIND(C,name="culaDeviceZungql")
            use ISO_C_BINDING
            integer(C_INT), value :: m
            integer(C_INT), value :: n
            integer(C_INT), value :: k
            type(C_PTR), value :: a
            integer(C_INT), value :: lda
            type(C_PTR), value :: tau
        end function
    end interface

    ! culaDeviceSorgqr(int m, int n, int k, culaFloat* a, int lda, culaFloat* tau);
    interface
        integer(C_INT) function cula_device_sorgqr(m,n,k,a,lda,tau)&
                BIND(C,name="culaDeviceSorgqr")
            use ISO_C_BINDING
            integer(C_INT), value :: m
            integer(C_INT), value :: n
            integer(C_INT), value :: k
            type(C_PTR), value :: a
            integer(C_INT), value :: lda
            type(C_PTR), value :: tau
        end function
    end interface

    ! culaDeviceDorgqr(int m, int n, int k, culaDouble* a, int lda, culaDouble* tau);
    interface
        integer(C_INT) function cula_device_dorgqr(m,n,k,a,lda,tau)&
                BIND(C,name="culaDeviceDorgqr")
            use ISO_C_BINDING
            integer(C_INT), value :: m
            integer(C_INT), value :: n
            integer(C_INT), value :: k
            type(C_PTR), value :: a
            integer(C_INT), value :: lda
            type(C_PTR), value :: tau
        end function
    end interface

    ! culaDeviceCungqr(int m, int n, int k, culaFloatComplex* a, int lda, culaFloatComplex* tau);
    interface
        integer(C_INT) function cula_device_cungqr(m,n,k,a,lda,tau)&
                BIND(C,name="culaDeviceCungqr")
            use ISO_C_BINDING
            integer(C_INT), value :: m
            integer(C_INT), value :: n
            integer(C_INT), value :: k
            type(C_PTR), value :: a
            integer(C_INT), value :: lda
            type(C_PTR), value :: tau
        end function
    end interface

    ! culaDeviceZungqr(int m, int n, int k, culaDoubleComplex* a, int lda, culaDoubleComplex* tau);
    interface
        integer(C_INT) function cula_device_zungqr(m,n,k,a,lda,tau)&
                BIND(C,name="culaDeviceZungqr")
            use ISO_C_BINDING
            integer(C_INT), value :: m
            integer(C_INT), value :: n
            integer(C_INT), value :: k
            type(C_PTR), value :: a
            integer(C_INT), value :: lda
            type(C_PTR), value :: tau
        end function
    end interface

    ! culaDeviceSorgrq(int m, int n, int k, culaFloat* a, int lda, culaFloat* tau);
    interface
        integer(C_INT) function cula_device_sorgrq(m,n,k,a,lda,tau)&
                BIND(C,name="culaDeviceSorgrq")
            use ISO_C_BINDING
            integer(C_INT), value :: m
            integer(C_INT), value :: n
            integer(C_INT), value :: k
            type(C_PTR), value :: a
            integer(C_INT), value :: lda
            type(C_PTR), value :: tau
        end function
    end interface

    ! culaDeviceDorgrq(int m, int n, int k, culaDouble* a, int lda, culaDouble* tau);
    interface
        integer(C_INT) function cula_device_dorgrq(m,n,k,a,lda,tau)&
                BIND(C,name="culaDeviceDorgrq")
            use ISO_C_BINDING
            integer(C_INT), value :: m
            integer(C_INT), value :: n
            integer(C_INT), value :: k
            type(C_PTR), value :: a
            integer(C_INT), value :: lda
            type(C_PTR), value :: tau
        end function
    end interface

    ! culaDeviceCungrq(int m, int n, int k, culaFloatComplex* a, int lda, culaFloatComplex* tau);
    interface
        integer(C_INT) function cula_device_cungrq(m,n,k,a,lda,tau)&
                BIND(C,name="culaDeviceCungrq")
            use ISO_C_BINDING
            integer(C_INT), value :: m
            integer(C_INT), value :: n
            integer(C_INT), value :: k
            type(C_PTR), value :: a
            integer(C_INT), value :: lda
            type(C_PTR), value :: tau
        end function
    end interface

    ! culaDeviceZungrq(int m, int n, int k, culaDoubleComplex* a, int lda, culaDoubleComplex* tau);
    interface
        integer(C_INT) function cula_device_zungrq(m,n,k,a,lda,tau)&
                BIND(C,name="culaDeviceZungrq")
            use ISO_C_BINDING
            integer(C_INT), value :: m
            integer(C_INT), value :: n
            integer(C_INT), value :: k
            type(C_PTR), value :: a
            integer(C_INT), value :: lda
            type(C_PTR), value :: tau
        end function
    end interface

    ! culaDeviceSormlq(char side, char trans, int m, int n, int k, culaFloat* a, int lda, culaFloat* tau, culaFloat* c, int ldc);
    interface
        integer(C_INT) function cula_device_sormlq(side,trans,m,n,k,a,lda,tau,c,ldc)&
                BIND(C,name="culaDeviceSormlq")
            use ISO_C_BINDING
            character(C_CHAR), value :: side
            character(C_CHAR), value :: trans
            integer(C_INT), value :: m
            integer(C_INT), value :: n
            integer(C_INT), value :: k
            type(C_PTR), value :: a
            integer(C_INT), value :: lda
            type(C_PTR), value :: tau
            type(C_PTR), value :: c
            integer(C_INT), value :: ldc
        end function
    end interface

    ! culaDeviceDormlq(char side, char trans, int m, int n, int k, culaDouble* a, int lda, culaDouble* tau, culaDouble* c, int ldc);
    interface
        integer(C_INT) function cula_device_dormlq(side,trans,m,n,k,a,lda,tau,c,ldc)&
                BIND(C,name="culaDeviceDormlq")
            use ISO_C_BINDING
            character(C_CHAR), value :: side
            character(C_CHAR), value :: trans
            integer(C_INT), value :: m
            integer(C_INT), value :: n
            integer(C_INT), value :: k
            type(C_PTR), value :: a
            integer(C_INT), value :: lda
            type(C_PTR), value :: tau
            type(C_PTR), value :: c
            integer(C_INT), value :: ldc
        end function
    end interface

    ! culaDeviceCunmlq(char side, char trans, int m, int n, int k, culaFloatComplex* a, int lda, culaFloatComplex* tau, culaFloatComplex* c, int ldc);
    interface
        integer(C_INT) function cula_device_cunmlq(side,trans,m,n,k,a,lda,tau,c,ldc)&
                BIND(C,name="culaDeviceCunmlq")
            use ISO_C_BINDING
            character(C_CHAR), value :: side
            character(C_CHAR), value :: trans
            integer(C_INT), value :: m
            integer(C_INT), value :: n
            integer(C_INT), value :: k
            type(C_PTR), value :: a
            integer(C_INT), value :: lda
            type(C_PTR), value :: tau
            type(C_PTR), value :: c
            integer(C_INT), value :: ldc
        end function
    end interface

    ! culaDeviceZunmlq(char side, char trans, int m, int n, int k, culaDoubleComplex* a, int lda, culaDoubleComplex* tau, culaDoubleComplex* c, int ldc);
    interface
        integer(C_INT) function cula_device_zunmlq(side,trans,m,n,k,a,lda,tau,c,ldc)&
                BIND(C,name="culaDeviceZunmlq")
            use ISO_C_BINDING
            character(C_CHAR), value :: side
            character(C_CHAR), value :: trans
            integer(C_INT), value :: m
            integer(C_INT), value :: n
            integer(C_INT), value :: k
            type(C_PTR), value :: a
            integer(C_INT), value :: lda
            type(C_PTR), value :: tau
            type(C_PTR), value :: c
            integer(C_INT), value :: ldc
        end function
    end interface

    ! culaDeviceSormql(char side, char trans, int m, int n, int k, culaFloat* a, int lda, culaFloat* tau, culaFloat* c, int ldc);
    interface
        integer(C_INT) function cula_device_sormql(side,trans,m,n,k,a,lda,tau,c,ldc)&
                BIND(C,name="culaDeviceSormql")
            use ISO_C_BINDING
            character(C_CHAR), value :: side
            character(C_CHAR), value :: trans
            integer(C_INT), value :: m
            integer(C_INT), value :: n
            integer(C_INT), value :: k
            type(C_PTR), value :: a
            integer(C_INT), value :: lda
            type(C_PTR), value :: tau
            type(C_PTR), value :: c
            integer(C_INT), value :: ldc
        end function
    end interface

    ! culaDeviceDormql(char side, char trans, int m, int n, int k, culaDouble* a, int lda, culaDouble* tau, culaDouble* c, int ldc);
    interface
        integer(C_INT) function cula_device_dormql(side,trans,m,n,k,a,lda,tau,c,ldc)&
                BIND(C,name="culaDeviceDormql")
            use ISO_C_BINDING
            character(C_CHAR), value :: side
            character(C_CHAR), value :: trans
            integer(C_INT), value :: m
            integer(C_INT), value :: n
            integer(C_INT), value :: k
            type(C_PTR), value :: a
            integer(C_INT), value :: lda
            type(C_PTR), value :: tau
            type(C_PTR), value :: c
            integer(C_INT), value :: ldc
        end function
    end interface

    ! culaDeviceCunmql(char side, char trans, int m, int n, int k, culaFloatComplex* a, int lda, culaFloatComplex* tau, culaFloatComplex* c, int ldc);
    interface
        integer(C_INT) function cula_device_cunmql(side,trans,m,n,k,a,lda,tau,c,ldc)&
                BIND(C,name="culaDeviceCunmql")
            use ISO_C_BINDING
            character(C_CHAR), value :: side
            character(C_CHAR), value :: trans
            integer(C_INT), value :: m
            integer(C_INT), value :: n
            integer(C_INT), value :: k
            type(C_PTR), value :: a
            integer(C_INT), value :: lda
            type(C_PTR), value :: tau
            type(C_PTR), value :: c
            integer(C_INT), value :: ldc
        end function
    end interface

    ! culaDeviceZunmql(char side, char trans, int m, int n, int k, culaDoubleComplex* a, int lda, culaDoubleComplex* tau, culaDoubleComplex* c, int ldc);
    interface
        integer(C_INT) function cula_device_zunmql(side,trans,m,n,k,a,lda,tau,c,ldc)&
                BIND(C,name="culaDeviceZunmql")
            use ISO_C_BINDING
            character(C_CHAR), value :: side
            character(C_CHAR), value :: trans
            integer(C_INT), value :: m
            integer(C_INT), value :: n
            integer(C_INT), value :: k
            type(C_PTR), value :: a
            integer(C_INT), value :: lda
            type(C_PTR), value :: tau
            type(C_PTR), value :: c
            integer(C_INT), value :: ldc
        end function
    end interface

    ! culaDeviceSormqr(char side, char trans, int m, int n, int k, culaFloat* a, int lda, culaFloat* tau, culaFloat* c, int ldc);
    interface
        integer(C_INT) function cula_device_sormqr(side,trans,m,n,k,a,lda,tau,c,ldc)&
                BIND(C,name="culaDeviceSormqr")
            use ISO_C_BINDING
            character(C_CHAR), value :: side
            character(C_CHAR), value :: trans
            integer(C_INT), value :: m
            integer(C_INT), value :: n
            integer(C_INT), value :: k
            type(C_PTR), value :: a
            integer(C_INT), value :: lda
            type(C_PTR), value :: tau
            type(C_PTR), value :: c
            integer(C_INT), value :: ldc
        end function
    end interface

    ! culaDeviceDormqr(char side, char trans, int m, int n, int k, culaDouble* a, int lda, culaDouble* tau, culaDouble* c, int ldc);
    interface
        integer(C_INT) function cula_device_dormqr(side,trans,m,n,k,a,lda,tau,c,ldc)&
                BIND(C,name="culaDeviceDormqr")
            use ISO_C_BINDING
            character(C_CHAR), value :: side
            character(C_CHAR), value :: trans
            integer(C_INT), value :: m
            integer(C_INT), value :: n
            integer(C_INT), value :: k
            type(C_PTR), value :: a
            integer(C_INT), value :: lda
            type(C_PTR), value :: tau
            type(C_PTR), value :: c
            integer(C_INT), value :: ldc
        end function
    end interface

    ! culaDeviceCunmqr(char side, char trans, int m, int n, int k, culaFloatComplex* a, int lda, culaFloatComplex* tau, culaFloatComplex* c, int ldc);
    interface
        integer(C_INT) function cula_device_cunmqr(side,trans,m,n,k,a,lda,tau,c,ldc)&
                BIND(C,name="culaDeviceCunmqr")
            use ISO_C_BINDING
            character(C_CHAR), value :: side
            character(C_CHAR), value :: trans
            integer(C_INT), value :: m
            integer(C_INT), value :: n
            integer(C_INT), value :: k
            type(C_PTR), value :: a
            integer(C_INT), value :: lda
            type(C_PTR), value :: tau
            type(C_PTR), value :: c
            integer(C_INT), value :: ldc
        end function
    end interface

    ! culaDeviceZunmqr(char side, char trans, int m, int n, int k, culaDoubleComplex* a, int lda, culaDoubleComplex* tau, culaDoubleComplex* c, int ldc);
    interface
        integer(C_INT) function cula_device_zunmqr(side,trans,m,n,k,a,lda,tau,c,ldc)&
                BIND(C,name="culaDeviceZunmqr")
            use ISO_C_BINDING
            character(C_CHAR), value :: side
            character(C_CHAR), value :: trans
            integer(C_INT), value :: m
            integer(C_INT), value :: n
            integer(C_INT), value :: k
            type(C_PTR), value :: a
            integer(C_INT), value :: lda
            type(C_PTR), value :: tau
            type(C_PTR), value :: c
            integer(C_INT), value :: ldc
        end function
    end interface

    ! culaDeviceSormrq(char side, char trans, int m, int n, int k, culaFloat* a, int lda, culaFloat* tau, culaFloat* c, int ldc);
    interface
        integer(C_INT) function cula_device_sormrq(side,trans,m,n,k,a,lda,tau,c,ldc)&
                BIND(C,name="culaDeviceSormrq")
            use ISO_C_BINDING
            character(C_CHAR), value :: side
            character(C_CHAR), value :: trans
            integer(C_INT), value :: m
            integer(C_INT), value :: n
            integer(C_INT), value :: k
            type(C_PTR), value :: a
            integer(C_INT), value :: lda
            type(C_PTR), value :: tau
            type(C_PTR), value :: c
            integer(C_INT), value :: ldc
        end function
    end interface

    ! culaDeviceDormrq(char side, char trans, int m, int n, int k, culaDouble* a, int lda, culaDouble* tau, culaDouble* c, int ldc);
    interface
        integer(C_INT) function cula_device_dormrq(side,trans,m,n,k,a,lda,tau,c,ldc)&
                BIND(C,name="culaDeviceDormrq")
            use ISO_C_BINDING
            character(C_CHAR), value :: side
            character(C_CHAR), value :: trans
            integer(C_INT), value :: m
            integer(C_INT), value :: n
            integer(C_INT), value :: k
            type(C_PTR), value :: a
            integer(C_INT), value :: lda
            type(C_PTR), value :: tau
            type(C_PTR), value :: c
            integer(C_INT), value :: ldc
        end function
    end interface

    ! culaDeviceCunmrq(char side, char trans, int m, int n, int k, culaFloatComplex* a, int lda, culaFloatComplex* tau, culaFloatComplex* c, int ldc);
    interface
        integer(C_INT) function cula_device_cunmrq(side,trans,m,n,k,a,lda,tau,c,ldc)&
                BIND(C,name="culaDeviceCunmrq")
            use ISO_C_BINDING
            character(C_CHAR), value :: side
            character(C_CHAR), value :: trans
            integer(C_INT), value :: m
            integer(C_INT), value :: n
            integer(C_INT), value :: k
            type(C_PTR), value :: a
            integer(C_INT), value :: lda
            type(C_PTR), value :: tau
            type(C_PTR), value :: c
            integer(C_INT), value :: ldc
        end function
    end interface

    ! culaDeviceZunmrq(char side, char trans, int m, int n, int k, culaDoubleComplex* a, int lda, culaDoubleComplex* tau, culaDoubleComplex* c, int ldc);
    interface
        integer(C_INT) function cula_device_zunmrq(side,trans,m,n,k,a,lda,tau,c,ldc)&
                BIND(C,name="culaDeviceZunmrq")
            use ISO_C_BINDING
            character(C_CHAR), value :: side
            character(C_CHAR), value :: trans
            integer(C_INT), value :: m
            integer(C_INT), value :: n
            integer(C_INT), value :: k
            type(C_PTR), value :: a
            integer(C_INT), value :: lda
            type(C_PTR), value :: tau
            type(C_PTR), value :: c
            integer(C_INT), value :: ldc
        end function
    end interface

    ! culaDeviceSpbtrf(char uplo, int n, int kd, culaFloat* ab, int ldab);
    interface
        integer(C_INT) function cula_device_spbtrf(uplo,n,kd,ab,ldab)&
                BIND(C,name="culaDeviceSpbtrf")
            use ISO_C_BINDING
            character(C_CHAR), value :: uplo
            integer(C_INT), value :: n
            integer(C_INT), value :: kd
            type(C_PTR), value :: ab
            integer(C_INT), value :: ldab
        end function
    end interface

    ! culaDeviceDpbtrf(char uplo, int n, int kd, culaDouble* ab, int ldab);
    interface
        integer(C_INT) function cula_device_dpbtrf(uplo,n,kd,ab,ldab)&
                BIND(C,name="culaDeviceDpbtrf")
            use ISO_C_BINDING
            character(C_CHAR), value :: uplo
            integer(C_INT), value :: n
            integer(C_INT), value :: kd
            type(C_PTR), value :: ab
            integer(C_INT), value :: ldab
        end function
    end interface

    ! culaDeviceCpbtrf(char uplo, int n, int kd, culaFloatComplex* ab, int ldab);
    interface
        integer(C_INT) function cula_device_cpbtrf(uplo,n,kd,ab,ldab)&
                BIND(C,name="culaDeviceCpbtrf")
            use ISO_C_BINDING
            character(C_CHAR), value :: uplo
            integer(C_INT), value :: n
            integer(C_INT), value :: kd
            type(C_PTR), value :: ab
            integer(C_INT), value :: ldab
        end function
    end interface

    ! culaDeviceZpbtrf(char uplo, int n, int kd, culaDoubleComplex* ab, int ldab);
    interface
        integer(C_INT) function cula_device_zpbtrf(uplo,n,kd,ab,ldab)&
                BIND(C,name="culaDeviceZpbtrf")
            use ISO_C_BINDING
            character(C_CHAR), value :: uplo
            integer(C_INT), value :: n
            integer(C_INT), value :: kd
            type(C_PTR), value :: ab
            integer(C_INT), value :: ldab
        end function
    end interface

    ! culaDeviceSposv(char uplo, int n, int nrhs, culaFloat* a, int lda, culaFloat* b, int ldb);
    interface
        integer(C_INT) function cula_device_sposv(uplo,n,nrhs,a,lda,b,ldb)&
                BIND(C,name="culaDeviceSposv")
            use ISO_C_BINDING
            character(C_CHAR), value :: uplo
            integer(C_INT), value :: n
            integer(C_INT), value :: nrhs
            type(C_PTR), value :: a
            integer(C_INT), value :: lda
            type(C_PTR), value :: b
            integer(C_INT), value :: ldb
        end function
    end interface

    ! culaDeviceDposv(char uplo, int n, int nrhs, culaDouble* a, int lda, culaDouble* b, int ldb);
    interface
        integer(C_INT) function cula_device_dposv(uplo,n,nrhs,a,lda,b,ldb)&
                BIND(C,name="culaDeviceDposv")
            use ISO_C_BINDING
            character(C_CHAR), value :: uplo
            integer(C_INT), value :: n
            integer(C_INT), value :: nrhs
            type(C_PTR), value :: a
            integer(C_INT), value :: lda
            type(C_PTR), value :: b
            integer(C_INT), value :: ldb
        end function
    end interface

    ! culaDeviceCposv(char uplo, int n, int nrhs, culaFloatComplex* a, int lda, culaFloatComplex* b, int ldb);
    interface
        integer(C_INT) function cula_device_cposv(uplo,n,nrhs,a,lda,b,ldb)&
                BIND(C,name="culaDeviceCposv")
            use ISO_C_BINDING
            character(C_CHAR), value :: uplo
            integer(C_INT), value :: n
            integer(C_INT), value :: nrhs
            type(C_PTR), value :: a
            integer(C_INT), value :: lda
            type(C_PTR), value :: b
            integer(C_INT), value :: ldb
        end function
    end interface

    ! culaDeviceZposv(char uplo, int n, int nrhs, culaDoubleComplex* a, int lda, culaDoubleComplex* b, int ldb);
    interface
        integer(C_INT) function cula_device_zposv(uplo,n,nrhs,a,lda,b,ldb)&
                BIND(C,name="culaDeviceZposv")
            use ISO_C_BINDING
            character(C_CHAR), value :: uplo
            integer(C_INT), value :: n
            integer(C_INT), value :: nrhs
            type(C_PTR), value :: a
            integer(C_INT), value :: lda
            type(C_PTR), value :: b
            integer(C_INT), value :: ldb
        end function
    end interface

    ! culaDeviceDsposv(char uplo, int n, int nrhs, culaDouble* a, int lda, culaDouble* b, int ldb, culaDouble* x, int ldx, int* iter);
    interface
        integer(C_INT) function cula_device_dsposv(uplo,n,nrhs,a,lda,b,ldb,x,ldx,iter)&
                BIND(C,name="culaDeviceDsposv")
            use ISO_C_BINDING
            character(C_CHAR), value :: uplo
            integer(C_INT), value :: n
            integer(C_INT), value :: nrhs
            type(C_PTR), value :: a
            integer(C_INT), value :: lda
            type(C_PTR), value :: b
            integer(C_INT), value :: ldb
            type(C_PTR), value :: x
            integer(C_INT), value :: ldx
            type(C_PTR), value :: iter
        end function
    end interface

    ! culaDeviceZcposv(char uplo, int n, int nrhs, culaDoubleComplex* a, int lda, culaDoubleComplex* b, int ldb, culaDoubleComplex* x, int ldx, int* iter);
    interface
        integer(C_INT) function cula_device_zcposv(uplo,n,nrhs,a,lda,b,ldb,x,ldx,iter)&
                BIND(C,name="culaDeviceZcposv")
            use ISO_C_BINDING
            character(C_CHAR), value :: uplo
            integer(C_INT), value :: n
            integer(C_INT), value :: nrhs
            type(C_PTR), value :: a
            integer(C_INT), value :: lda
            type(C_PTR), value :: b
            integer(C_INT), value :: ldb
            type(C_PTR), value :: x
            integer(C_INT), value :: ldx
            type(C_PTR), value :: iter
        end function
    end interface

    ! culaDeviceSpotrf(char uplo, int n, culaFloat* a, int lda);
    interface
        integer(C_INT) function cula_device_spotrf(uplo,n,a,lda)&
                BIND(C,name="culaDeviceSpotrf")
            use ISO_C_BINDING
            character(C_CHAR), value :: uplo
            integer(C_INT), value :: n
            type(C_PTR), value :: a
            integer(C_INT), value :: lda
        end function
    end interface

    ! culaDeviceDpotrf(char uplo, int n, culaDouble* a, int lda);
    interface
        integer(C_INT) function cula_device_dpotrf(uplo,n,a,lda)&
                BIND(C,name="culaDeviceDpotrf")
            use ISO_C_BINDING
            character(C_CHAR), value :: uplo
            integer(C_INT), value :: n
            type(C_PTR), value :: a
            integer(C_INT), value :: lda
        end function
    end interface

    ! culaDeviceCpotrf(char uplo, int n, culaFloatComplex* a, int lda);
    interface
        integer(C_INT) function cula_device_cpotrf(uplo,n,a,lda)&
                BIND(C,name="culaDeviceCpotrf")
            use ISO_C_BINDING
            character(C_CHAR), value :: uplo
            integer(C_INT), value :: n
            type(C_PTR), value :: a
            integer(C_INT), value :: lda
        end function
    end interface

    ! culaDeviceZpotrf(char uplo, int n, culaDoubleComplex* a, int lda);
    interface
        integer(C_INT) function cula_device_zpotrf(uplo,n,a,lda)&
                BIND(C,name="culaDeviceZpotrf")
            use ISO_C_BINDING
            character(C_CHAR), value :: uplo
            integer(C_INT), value :: n
            type(C_PTR), value :: a
            integer(C_INT), value :: lda
        end function
    end interface

    ! culaDeviceSpotri(char uplo, int n, culaFloat* a, int lda);
    interface
        integer(C_INT) function cula_device_spotri(uplo,n,a,lda)&
                BIND(C,name="culaDeviceSpotri")
            use ISO_C_BINDING
            character(C_CHAR), value :: uplo
            integer(C_INT), value :: n
            type(C_PTR), value :: a
            integer(C_INT), value :: lda
        end function
    end interface

    ! culaDeviceDpotri(char uplo, int n, culaDouble* a, int lda);
    interface
        integer(C_INT) function cula_device_dpotri(uplo,n,a,lda)&
                BIND(C,name="culaDeviceDpotri")
            use ISO_C_BINDING
            character(C_CHAR), value :: uplo
            integer(C_INT), value :: n
            type(C_PTR), value :: a
            integer(C_INT), value :: lda
        end function
    end interface

    ! culaDeviceCpotri(char uplo, int n, culaFloatComplex* a, int lda);
    interface
        integer(C_INT) function cula_device_cpotri(uplo,n,a,lda)&
                BIND(C,name="culaDeviceCpotri")
            use ISO_C_BINDING
            character(C_CHAR), value :: uplo
            integer(C_INT), value :: n
            type(C_PTR), value :: a
            integer(C_INT), value :: lda
        end function
    end interface

    ! culaDeviceZpotri(char uplo, int n, culaDoubleComplex* a, int lda);
    interface
        integer(C_INT) function cula_device_zpotri(uplo,n,a,lda)&
                BIND(C,name="culaDeviceZpotri")
            use ISO_C_BINDING
            character(C_CHAR), value :: uplo
            integer(C_INT), value :: n
            type(C_PTR), value :: a
            integer(C_INT), value :: lda
        end function
    end interface

    ! culaDeviceSpotrs(char uplo, int n, int nrhs, culaFloat* a, int lda, culaFloat* b, int ldb);
    interface
        integer(C_INT) function cula_device_spotrs(uplo,n,nrhs,a,lda,b,ldb)&
                BIND(C,name="culaDeviceSpotrs")
            use ISO_C_BINDING
            character(C_CHAR), value :: uplo
            integer(C_INT), value :: n
            integer(C_INT), value :: nrhs
            type(C_PTR), value :: a
            integer(C_INT), value :: lda
            type(C_PTR), value :: b
            integer(C_INT), value :: ldb
        end function
    end interface

    ! culaDeviceDpotrs(char uplo, int n, int nrhs, culaDouble* a, int lda, culaDouble* b, int ldb);
    interface
        integer(C_INT) function cula_device_dpotrs(uplo,n,nrhs,a,lda,b,ldb)&
                BIND(C,name="culaDeviceDpotrs")
            use ISO_C_BINDING
            character(C_CHAR), value :: uplo
            integer(C_INT), value :: n
            integer(C_INT), value :: nrhs
            type(C_PTR), value :: a
            integer(C_INT), value :: lda
            type(C_PTR), value :: b
            integer(C_INT), value :: ldb
        end function
    end interface

    ! culaDeviceCpotrs(char uplo, int n, int nrhs, culaFloatComplex* a, int lda, culaFloatComplex* b, int ldb);
    interface
        integer(C_INT) function cula_device_cpotrs(uplo,n,nrhs,a,lda,b,ldb)&
                BIND(C,name="culaDeviceCpotrs")
            use ISO_C_BINDING
            character(C_CHAR), value :: uplo
            integer(C_INT), value :: n
            integer(C_INT), value :: nrhs
            type(C_PTR), value :: a
            integer(C_INT), value :: lda
            type(C_PTR), value :: b
            integer(C_INT), value :: ldb
        end function
    end interface

    ! culaDeviceZpotrs(char uplo, int n, int nrhs, culaDoubleComplex* a, int lda, culaDoubleComplex* b, int ldb);
    interface
        integer(C_INT) function cula_device_zpotrs(uplo,n,nrhs,a,lda,b,ldb)&
                BIND(C,name="culaDeviceZpotrs")
            use ISO_C_BINDING
            character(C_CHAR), value :: uplo
            integer(C_INT), value :: n
            integer(C_INT), value :: nrhs
            type(C_PTR), value :: a
            integer(C_INT), value :: lda
            type(C_PTR), value :: b
            integer(C_INT), value :: ldb
        end function
    end interface

    ! culaDeviceSpptrf(char uplo, int n, culaFloat* ap);
    interface
        integer(C_INT) function cula_device_spptrf(uplo,n,ap)&
                BIND(C,name="culaDeviceSpptrf")
            use ISO_C_BINDING
            character(C_CHAR), value :: uplo
            integer(C_INT), value :: n
            type(C_PTR), value :: ap
        end function
    end interface

    ! culaDeviceDpptrf(char uplo, int n, culaDouble* ap);
    interface
        integer(C_INT) function cula_device_dpptrf(uplo,n,ap)&
                BIND(C,name="culaDeviceDpptrf")
            use ISO_C_BINDING
            character(C_CHAR), value :: uplo
            integer(C_INT), value :: n
            type(C_PTR), value :: ap
        end function
    end interface

    ! culaDeviceCpptrf(char uplo, int n, culaFloatComplex* ap);
    interface
        integer(C_INT) function cula_device_cpptrf(uplo,n,ap)&
                BIND(C,name="culaDeviceCpptrf")
            use ISO_C_BINDING
            character(C_CHAR), value :: uplo
            integer(C_INT), value :: n
            type(C_PTR), value :: ap
        end function
    end interface

    ! culaDeviceZpptrf(char uplo, int n, culaDoubleComplex* ap);
    interface
        integer(C_INT) function cula_device_zpptrf(uplo,n,ap)&
                BIND(C,name="culaDeviceZpptrf")
            use ISO_C_BINDING
            character(C_CHAR), value :: uplo
            integer(C_INT), value :: n
            type(C_PTR), value :: ap
        end function
    end interface

    ! culaDeviceSstebz(char range, char order, int n, float vl, float vu, int il, int iu, float abstol, culaFloat* d, culaFloat* e, int* m, int* nsplit, culaFloat* w, culaInt* isplit, culaInt* iblock);
    interface
        integer(C_INT) function cula_device_sstebz(rang,order,n,vl,vu,il,iu,abstol,d,e,m,nsplit,w,isplit,iblock)&
                BIND(C,name="culaDeviceSstebz")
            use ISO_C_BINDING
            character(C_CHAR), value :: rang
            character(C_CHAR), value :: order
            integer(C_INT), value :: n
            real(C_FLOAT) :: vl
            real(C_FLOAT) :: vu
            integer(C_INT), value :: il
            integer(C_INT), value :: iu
            real(C_FLOAT) :: abstol
            type(C_PTR), value :: d
            type(C_PTR), value :: e
            type(C_PTR), value :: m
            type(C_PTR), value :: nsplit
            type(C_PTR), value :: w
            type(C_PTR), value :: isplit
            type(C_PTR), value :: iblock
        end function
    end interface

    ! culaDeviceDstebz(char range, char order, int n, double vl, double vu, int il, int iu, double abstol, culaDouble* d, culaDouble* e, int* m, int* nsplit, culaDouble* w, culaInt* isplit, culaInt* iblock);
    interface
        integer(C_INT) function cula_device_dstebz(rang,order,n,vl,vu,il,iu,abstol,d,e,m,nsplit,w,isplit,iblock)&
                BIND(C,name="culaDeviceDstebz")
            use ISO_C_BINDING
            character(C_CHAR), value :: rang
            character(C_CHAR), value :: order
            integer(C_INT), value :: n
            real(C_DOUBLE), value :: vl
            real(C_DOUBLE), value :: vu
            integer(C_INT), value :: il
            integer(C_INT), value :: iu
            real(C_DOUBLE), value :: abstol
            type(C_PTR), value :: d
            type(C_PTR), value :: e
            type(C_PTR), value :: m
            type(C_PTR), value :: nsplit
            type(C_PTR), value :: w
            type(C_PTR), value :: isplit
            type(C_PTR), value :: iblock
        end function
    end interface

    ! culaDeviceSsteqr(char compz, int n, culaFloat* d, culaFloat* e, culaFloat* z, int ldz);
    interface
        integer(C_INT) function cula_device_ssteqr(compz,n,d,e,z,ldz)&
                BIND(C,name="culaDeviceSsteqr")
            use ISO_C_BINDING
            character(C_CHAR), value :: compz
            integer(C_INT), value :: n
            type(C_PTR), value :: d
            type(C_PTR), value :: e
            type(C_PTR), value :: z
            integer(C_INT), value :: ldz
        end function
    end interface

    ! culaDeviceDsteqr(char compz, int n, culaDouble* d, culaDouble* e, culaDouble* z, int ldz);
    interface
        integer(C_INT) function cula_device_dsteqr(compz,n,d,e,z,ldz)&
                BIND(C,name="culaDeviceDsteqr")
            use ISO_C_BINDING
            character(C_CHAR), value :: compz
            integer(C_INT), value :: n
            type(C_PTR), value :: d
            type(C_PTR), value :: e
            type(C_PTR), value :: z
            integer(C_INT), value :: ldz
        end function
    end interface

    ! culaDeviceCsteqr(char compz, int n, culaFloat* d, culaFloat* e, culaFloatComplex* z, int ldz);
    interface
        integer(C_INT) function cula_device_csteqr(compz,n,d,e,z,ldz)&
                BIND(C,name="culaDeviceCsteqr")
            use ISO_C_BINDING
            character(C_CHAR), value :: compz
            integer(C_INT), value :: n
            type(C_PTR), value :: d
            type(C_PTR), value :: e
            type(C_PTR), value :: z
            integer(C_INT), value :: ldz
        end function
    end interface

    ! culaDeviceZsteqr(char compz, int n, culaDouble* d, culaDouble* e, culaDoubleComplex* z, int ldz);
    interface
        integer(C_INT) function cula_device_zsteqr(compz,n,d,e,z,ldz)&
                BIND(C,name="culaDeviceZsteqr")
            use ISO_C_BINDING
            character(C_CHAR), value :: compz
            integer(C_INT), value :: n
            type(C_PTR), value :: d
            type(C_PTR), value :: e
            type(C_PTR), value :: z
            integer(C_INT), value :: ldz
        end function
    end interface

    ! culaDeviceSsyev(char jobz, char uplo, int n, culaFloat* a, int lda, culaFloat* w);
    interface
        integer(C_INT) function cula_device_ssyev(jobz,uplo,n,a,lda,w)&
                BIND(C,name="culaDeviceSsyev")
            use ISO_C_BINDING
            character(C_CHAR), value :: jobz
            character(C_CHAR), value :: uplo
            integer(C_INT), value :: n
            type(C_PTR), value :: a
            integer(C_INT), value :: lda
            type(C_PTR), value :: w
        end function
    end interface

    ! culaDeviceDsyev(char jobz, char uplo, int n, culaDouble* a, int lda, culaDouble* w);
    interface
        integer(C_INT) function cula_device_dsyev(jobz,uplo,n,a,lda,w)&
                BIND(C,name="culaDeviceDsyev")
            use ISO_C_BINDING
            character(C_CHAR), value :: jobz
            character(C_CHAR), value :: uplo
            integer(C_INT), value :: n
            type(C_PTR), value :: a
            integer(C_INT), value :: lda
            type(C_PTR), value :: w
        end function
    end interface

    ! culaDeviceCheev(char jobz, char uplo, int n, culaFloatComplex* a, int lda, culaFloat* w);
    interface
        integer(C_INT) function cula_device_cheev(jobz,uplo,n,a,lda,w)&
                BIND(C,name="culaDeviceCheev")
            use ISO_C_BINDING
            character(C_CHAR), value :: jobz
            character(C_CHAR), value :: uplo
            integer(C_INT), value :: n
            type(C_PTR), value :: a
            integer(C_INT), value :: lda
            type(C_PTR), value :: w
        end function
    end interface

    ! culaDeviceZheev(char jobz, char uplo, int n, culaDoubleComplex* a, int lda, culaDouble* w);
    interface
        integer(C_INT) function cula_device_zheev(jobz,uplo,n,a,lda,w)&
                BIND(C,name="culaDeviceZheev")
            use ISO_C_BINDING
            character(C_CHAR), value :: jobz
            character(C_CHAR), value :: uplo
            integer(C_INT), value :: n
            type(C_PTR), value :: a
            integer(C_INT), value :: lda
            type(C_PTR), value :: w
        end function
    end interface

    ! culaDeviceSsyevx(char jobz, char range, char uplo, int n, culaFloat* a, int lda, culaFloat vl, culaFloat vu, int il, int iu, culaFloat abstol, culaInt* m, culaFloat* w, culaFloat* z, int ldz, culaInt* ifail);
    interface
        integer(C_INT) function cula_device_ssyevx(jobz,rang,uplo,n,a,lda,vl,vu,il,iu,abstol,m,w,z,ldz,ifail)&
                BIND(C,name="culaDeviceSsyevx")
            use ISO_C_BINDING
            character(C_CHAR), value :: jobz
            character(C_CHAR), value :: rang
            character(C_CHAR), value :: uplo
            integer(C_INT), value :: n
            type(C_PTR), value :: a
            integer(C_INT), value :: lda
            real(C_FLOAT) :: vl
            real(C_FLOAT) :: vu
            integer(C_INT), value :: il
            integer(C_INT), value :: iu
            real(C_FLOAT) :: abstol
            type(C_PTR), value :: m
            type(C_PTR), value :: w
            type(C_PTR), value :: z
            integer(C_INT), value :: ldz
            type(C_PTR), value :: ifail
        end function
    end interface

    ! culaDeviceDsyevx(char jobz, char range, char uplo, int n, culaDouble* a, int lda, culaDouble vl, culaDouble vu, int il, int iu, culaDouble abstol, culaInt* m, culaDouble* w, culaDouble* z, int ldz, culaInt* ifail);
    interface
        integer(C_INT) function cula_device_dsyevx(jobz,rang,uplo,n,a,lda,vl,vu,il,iu,abstol,m,w,z,ldz,ifail)&
                BIND(C,name="culaDeviceDsyevx")
            use ISO_C_BINDING
            character(C_CHAR), value :: jobz
            character(C_CHAR), value :: rang
            character(C_CHAR), value :: uplo
            integer(C_INT), value :: n
            type(C_PTR), value :: a
            integer(C_INT), value :: lda
            real(C_DOUBLE), value :: vl
            real(C_DOUBLE), value :: vu
            integer(C_INT), value :: il
            integer(C_INT), value :: iu
            real(C_DOUBLE), value :: abstol
            type(C_PTR), value :: m
            type(C_PTR), value :: w
            type(C_PTR), value :: z
            integer(C_INT), value :: ldz
            type(C_PTR), value :: ifail
        end function
    end interface

    ! culaDeviceCheevx(char jobz, char range, char uplo, int n, culaFloatComplex* a, int lda, culaFloat vl, culaFloat vu, int il, int iu, culaFloat abstol, culaInt* m, culaFloat* w, culaFloatComplex* z, int ldz, culaInt* ifail);
    interface
        integer(C_INT) function cula_device_cheevx(jobz,rang,uplo,n,a,lda,vl,vu,il,iu,abstol,m,w,z,ldz,ifail)&
                BIND(C,name="culaDeviceCheevx")
            use ISO_C_BINDING
            character(C_CHAR), value :: jobz
            character(C_CHAR), value :: rang
            character(C_CHAR), value :: uplo
            integer(C_INT), value :: n
            type(C_PTR), value :: a
            integer(C_INT), value :: lda
            real(C_FLOAT) :: vl
            real(C_FLOAT) :: vu
            integer(C_INT), value :: il
            integer(C_INT), value :: iu
            real(C_FLOAT) :: abstol
            type(C_PTR), value :: m
            type(C_PTR), value :: w
            type(C_PTR), value :: z
            integer(C_INT), value :: ldz
            type(C_PTR), value :: ifail
        end function
    end interface

    ! culaDeviceZheevx(char jobz, char range, char uplo, int n, culaDoubleComplex* a, int lda, culaDouble vl, culaDouble vu, int il, int iu, culaDouble abstol, culaInt* m, culaDouble* w, culaDoubleComplex* z, int ldz, culaInt* ifail);
    interface
        integer(C_INT) function cula_device_zheevx(jobz,rang,uplo,n,a,lda,vl,vu,il,iu,abstol,m,w,z,ldz,ifail)&
                BIND(C,name="culaDeviceZheevx")
            use ISO_C_BINDING
            character(C_CHAR), value :: jobz
            character(C_CHAR), value :: rang
            character(C_CHAR), value :: uplo
            integer(C_INT), value :: n
            type(C_PTR), value :: a
            integer(C_INT), value :: lda
            real(C_DOUBLE), value :: vl
            real(C_DOUBLE), value :: vu
            integer(C_INT), value :: il
            integer(C_INT), value :: iu
            real(C_DOUBLE), value :: abstol
            type(C_PTR), value :: m
            type(C_PTR), value :: w
            type(C_PTR), value :: z
            integer(C_INT), value :: ldz
            type(C_PTR), value :: ifail
        end function
    end interface

    ! culaDeviceSsyrdb(char jobz, char uplo, int n, int kd, culaFloat* a, int lda, culaFloat* d, culaFloat* e, culaFloat* tau, culaFloat* z, int ldz);
    interface
        integer(C_INT) function cula_device_ssyrdb(jobz,uplo,n,kd,a,lda,d,e,tau,z,ldz)&
                BIND(C,name="culaDeviceSsyrdb")
            use ISO_C_BINDING
            character(C_CHAR), value :: jobz
            character(C_CHAR), value :: uplo
            integer(C_INT), value :: n
            integer(C_INT), value :: kd
            type(C_PTR), value :: a
            integer(C_INT), value :: lda
            type(C_PTR), value :: d
            type(C_PTR), value :: e
            type(C_PTR), value :: tau
            type(C_PTR), value :: z
            integer(C_INT), value :: ldz
        end function
    end interface

    ! culaDeviceDsyrdb(char jobz, char uplo, int n, int kd, culaDouble* a, int lda, culaDouble* d, culaDouble* e, culaDouble* tau, culaDouble* z, int ldz);
    interface
        integer(C_INT) function cula_device_dsyrdb(jobz,uplo,n,kd,a,lda,d,e,tau,z,ldz)&
                BIND(C,name="culaDeviceDsyrdb")
            use ISO_C_BINDING
            character(C_CHAR), value :: jobz
            character(C_CHAR), value :: uplo
            integer(C_INT), value :: n
            integer(C_INT), value :: kd
            type(C_PTR), value :: a
            integer(C_INT), value :: lda
            type(C_PTR), value :: d
            type(C_PTR), value :: e
            type(C_PTR), value :: tau
            type(C_PTR), value :: z
            integer(C_INT), value :: ldz
        end function
    end interface

    ! culaDeviceCherdb(char jobz, char uplo, int n, int kd, culaFloatComplex* a, int lda, culaFloat* d, culaFloat* e, culaFloatComplex* tau, culaFloatComplex* z, int ldz);
    interface
        integer(C_INT) function cula_device_cherdb(jobz,uplo,n,kd,a,lda,d,e,tau,z,ldz)&
                BIND(C,name="culaDeviceCherdb")
            use ISO_C_BINDING
            character(C_CHAR), value :: jobz
            character(C_CHAR), value :: uplo
            integer(C_INT), value :: n
            integer(C_INT), value :: kd
            type(C_PTR), value :: a
            integer(C_INT), value :: lda
            type(C_PTR), value :: d
            type(C_PTR), value :: e
            type(C_PTR), value :: tau
            type(C_PTR), value :: z
            integer(C_INT), value :: ldz
        end function
    end interface

    ! culaDeviceZherdb(char jobz, char uplo, int n, int kd, culaDoubleComplex* a, int lda, culaDouble* d, culaDouble* e, culaDoubleComplex* tau, culaDoubleComplex* z, int ldz);
    interface
        integer(C_INT) function cula_device_zherdb(jobz,uplo,n,kd,a,lda,d,e,tau,z,ldz)&
                BIND(C,name="culaDeviceZherdb")
            use ISO_C_BINDING
            character(C_CHAR), value :: jobz
            character(C_CHAR), value :: uplo
            integer(C_INT), value :: n
            integer(C_INT), value :: kd
            type(C_PTR), value :: a
            integer(C_INT), value :: lda
            type(C_PTR), value :: d
            type(C_PTR), value :: e
            type(C_PTR), value :: tau
            type(C_PTR), value :: z
            integer(C_INT), value :: ldz
        end function
    end interface

    ! culaDeviceSsysv(char uplo, int n, int nrhs, culaFloat* a, int lda, culaInt* ipiv, culaFloat* b, int ldb);
    interface
        integer(C_INT) function cula_device_ssysv(uplo,n,nrhs,a,lda,ipiv,b,ldb)&
                BIND(C,name="culaDeviceSsysv")
            use ISO_C_BINDING
            character(C_CHAR), value :: uplo
            integer(C_INT), value :: n
            integer(C_INT), value :: nrhs
            type(C_PTR), value :: a
            integer(C_INT), value :: lda
            type(C_PTR), value :: ipiv
            type(C_PTR), value :: b
            integer(C_INT), value :: ldb
        end function
    end interface

    ! culaDeviceDsysv(char uplo, int n, int nrhs, culaDouble* a, int lda, culaInt* ipiv, culaDouble* b, int ldb);
    interface
        integer(C_INT) function cula_device_dsysv(uplo,n,nrhs,a,lda,ipiv,b,ldb)&
                BIND(C,name="culaDeviceDsysv")
            use ISO_C_BINDING
            character(C_CHAR), value :: uplo
            integer(C_INT), value :: n
            integer(C_INT), value :: nrhs
            type(C_PTR), value :: a
            integer(C_INT), value :: lda
            type(C_PTR), value :: ipiv
            type(C_PTR), value :: b
            integer(C_INT), value :: ldb
        end function
    end interface

    ! culaDeviceCsysv(char uplo, int n, int nrhs, culaFloatComplex* a, int lda, culaInt* ipiv, culaFloatComplex* b, int ldb);
    interface
        integer(C_INT) function cula_device_csysv(uplo,n,nrhs,a,lda,ipiv,b,ldb)&
                BIND(C,name="culaDeviceCsysv")
            use ISO_C_BINDING
            character(C_CHAR), value :: uplo
            integer(C_INT), value :: n
            integer(C_INT), value :: nrhs
            type(C_PTR), value :: a
            integer(C_INT), value :: lda
            type(C_PTR), value :: ipiv
            type(C_PTR), value :: b
            integer(C_INT), value :: ldb
        end function
    end interface

    ! culaDeviceZsysv(char uplo, int n, int nrhs, culaDoubleComplex* a, int lda, culaInt* ipiv, culaDoubleComplex* b, int ldb);
    interface
        integer(C_INT) function cula_device_zsysv(uplo,n,nrhs,a,lda,ipiv,b,ldb)&
                BIND(C,name="culaDeviceZsysv")
            use ISO_C_BINDING
            character(C_CHAR), value :: uplo
            integer(C_INT), value :: n
            integer(C_INT), value :: nrhs
            type(C_PTR), value :: a
            integer(C_INT), value :: lda
            type(C_PTR), value :: ipiv
            type(C_PTR), value :: b
            integer(C_INT), value :: ldb
        end function
    end interface

    ! culaDeviceChesv(char uplo, int n, int nrhs, culaFloatComplex* a, int lda, culaInt* ipiv, culaFloatComplex* b, int ldb);
    interface
        integer(C_INT) function cula_device_chesv(uplo,n,nrhs,a,lda,ipiv,b,ldb)&
                BIND(C,name="culaDeviceChesv")
            use ISO_C_BINDING
            character(C_CHAR), value :: uplo
            integer(C_INT), value :: n
            integer(C_INT), value :: nrhs
            type(C_PTR), value :: a
            integer(C_INT), value :: lda
            type(C_PTR), value :: ipiv
            type(C_PTR), value :: b
            integer(C_INT), value :: ldb
        end function
    end interface

    ! culaDeviceZhesv(char uplo, int n, int nrhs, culaDoubleComplex* a, int lda, culaInt* ipiv, culaDoubleComplex* b, int ldb);
    interface
        integer(C_INT) function cula_device_zhesv(uplo,n,nrhs,a,lda,ipiv,b,ldb)&
                BIND(C,name="culaDeviceZhesv")
            use ISO_C_BINDING
            character(C_CHAR), value :: uplo
            integer(C_INT), value :: n
            integer(C_INT), value :: nrhs
            type(C_PTR), value :: a
            integer(C_INT), value :: lda
            type(C_PTR), value :: ipiv
            type(C_PTR), value :: b
            integer(C_INT), value :: ldb
        end function
    end interface

    ! culaDeviceSsytrd(char uplo, int n, culaFloat* a, int lda, culaFloat* d, culaFloat* e, culaFloat* tau);
    interface
        integer(C_INT) function cula_device_ssytrd(uplo,n,a,lda,d,e,tau)&
                BIND(C,name="culaDeviceSsytrd")
            use ISO_C_BINDING
            character(C_CHAR), value :: uplo
            integer(C_INT), value :: n
            type(C_PTR), value :: a
            integer(C_INT), value :: lda
            type(C_PTR), value :: d
            type(C_PTR), value :: e
            type(C_PTR), value :: tau
        end function
    end interface

    ! culaDeviceDsytrd(char uplo, int n, culaDouble* a, int lda, culaDouble* d, culaDouble* e, culaDouble* tau);
    interface
        integer(C_INT) function cula_device_dsytrd(uplo,n,a,lda,d,e,tau)&
                BIND(C,name="culaDeviceDsytrd")
            use ISO_C_BINDING
            character(C_CHAR), value :: uplo
            integer(C_INT), value :: n
            type(C_PTR), value :: a
            integer(C_INT), value :: lda
            type(C_PTR), value :: d
            type(C_PTR), value :: e
            type(C_PTR), value :: tau
        end function
    end interface

    ! culaDeviceChetrd(char uplo, int n, culaFloatComplex* a, int lda, culaFloat* d, culaFloat* e, culaFloatComplex* tau);
    interface
        integer(C_INT) function cula_device_chetrd(uplo,n,a,lda,d,e,tau)&
                BIND(C,name="culaDeviceChetrd")
            use ISO_C_BINDING
            character(C_CHAR), value :: uplo
            integer(C_INT), value :: n
            type(C_PTR), value :: a
            integer(C_INT), value :: lda
            type(C_PTR), value :: d
            type(C_PTR), value :: e
            type(C_PTR), value :: tau
        end function
    end interface

    ! culaDeviceZhetrd(char uplo, int n, culaDoubleComplex* a, int lda, culaDouble* d, culaDouble* e, culaDoubleComplex* tau);
    interface
        integer(C_INT) function cula_device_zhetrd(uplo,n,a,lda,d,e,tau)&
                BIND(C,name="culaDeviceZhetrd")
            use ISO_C_BINDING
            character(C_CHAR), value :: uplo
            integer(C_INT), value :: n
            type(C_PTR), value :: a
            integer(C_INT), value :: lda
            type(C_PTR), value :: d
            type(C_PTR), value :: e
            type(C_PTR), value :: tau
        end function
    end interface

    ! culaDeviceSsytrf(char uplo, int n, culaFloat* a, int lda, culaInt* ipiv);
    interface
        integer(C_INT) function cula_device_ssytrf(uplo,n,a,lda,ipiv)&
                BIND(C,name="culaDeviceSsytrf")
            use ISO_C_BINDING
            character(C_CHAR), value :: uplo
            integer(C_INT), value :: n
            type(C_PTR), value :: a
            integer(C_INT), value :: lda
            type(C_PTR), value :: ipiv
        end function
    end interface

    ! culaDeviceDsytrf(char uplo, int n, culaDouble* a, int lda, culaInt* ipiv);
    interface
        integer(C_INT) function cula_device_dsytrf(uplo,n,a,lda,ipiv)&
                BIND(C,name="culaDeviceDsytrf")
            use ISO_C_BINDING
            character(C_CHAR), value :: uplo
            integer(C_INT), value :: n
            type(C_PTR), value :: a
            integer(C_INT), value :: lda
            type(C_PTR), value :: ipiv
        end function
    end interface

    ! culaDeviceCsytrf(char uplo, int n, culaFloatComplex* a, int lda, culaInt* ipiv);
    interface
        integer(C_INT) function cula_device_csytrf(uplo,n,a,lda,ipiv)&
                BIND(C,name="culaDeviceCsytrf")
            use ISO_C_BINDING
            character(C_CHAR), value :: uplo
            integer(C_INT), value :: n
            type(C_PTR), value :: a
            integer(C_INT), value :: lda
            type(C_PTR), value :: ipiv
        end function
    end interface

    ! culaDeviceZsytrf(char uplo, int n, culaDoubleComplex* a, int lda, culaInt* ipiv);
    interface
        integer(C_INT) function cula_device_zsytrf(uplo,n,a,lda,ipiv)&
                BIND(C,name="culaDeviceZsytrf")
            use ISO_C_BINDING
            character(C_CHAR), value :: uplo
            integer(C_INT), value :: n
            type(C_PTR), value :: a
            integer(C_INT), value :: lda
            type(C_PTR), value :: ipiv
        end function
    end interface

    ! culaDeviceChetrf(char uplo, int n, culaFloatComplex* a, int lda, culaInt* ipiv);
    interface
        integer(C_INT) function cula_device_chetrf(uplo,n,a,lda,ipiv)&
                BIND(C,name="culaDeviceChetrf")
            use ISO_C_BINDING
            character(C_CHAR), value :: uplo
            integer(C_INT), value :: n
            type(C_PTR), value :: a
            integer(C_INT), value :: lda
            type(C_PTR), value :: ipiv
        end function
    end interface

    ! culaDeviceZhetrf(char uplo, int n, culaDoubleComplex* a, int lda, culaInt* ipiv);
    interface
        integer(C_INT) function cula_device_zhetrf(uplo,n,a,lda,ipiv)&
                BIND(C,name="culaDeviceZhetrf")
            use ISO_C_BINDING
            character(C_CHAR), value :: uplo
            integer(C_INT), value :: n
            type(C_PTR), value :: a
            integer(C_INT), value :: lda
            type(C_PTR), value :: ipiv
        end function
    end interface

    ! culaDeviceSsytrs(char uplo, int n, int nrhs, culaFloat* a, int lda, culaInt* ipiv, culaFloat* b, int ldb);
    interface
        integer(C_INT) function cula_device_ssytrs(uplo,n,nrhs,a,lda,ipiv,b,ldb)&
                BIND(C,name="culaDeviceSsytrs")
            use ISO_C_BINDING
            character(C_CHAR), value :: uplo
            integer(C_INT), value :: n
            integer(C_INT), value :: nrhs
            type(C_PTR), value :: a
            integer(C_INT), value :: lda
            type(C_PTR), value :: ipiv
            type(C_PTR), value :: b
            integer(C_INT), value :: ldb
        end function
    end interface

    ! culaDeviceDsytrs(char uplo, int n, int nrhs, culaDouble* a, int lda, culaInt* ipiv, culaDouble* b, int ldb);
    interface
        integer(C_INT) function cula_device_dsytrs(uplo,n,nrhs,a,lda,ipiv,b,ldb)&
                BIND(C,name="culaDeviceDsytrs")
            use ISO_C_BINDING
            character(C_CHAR), value :: uplo
            integer(C_INT), value :: n
            integer(C_INT), value :: nrhs
            type(C_PTR), value :: a
            integer(C_INT), value :: lda
            type(C_PTR), value :: ipiv
            type(C_PTR), value :: b
            integer(C_INT), value :: ldb
        end function
    end interface

    ! culaDeviceCsytrs(char uplo, int n, int nrhs, culaFloatComplex* a, int lda, culaInt* ipiv, culaFloatComplex* b, int ldb);
    interface
        integer(C_INT) function cula_device_csytrs(uplo,n,nrhs,a,lda,ipiv,b,ldb)&
                BIND(C,name="culaDeviceCsytrs")
            use ISO_C_BINDING
            character(C_CHAR), value :: uplo
            integer(C_INT), value :: n
            integer(C_INT), value :: nrhs
            type(C_PTR), value :: a
            integer(C_INT), value :: lda
            type(C_PTR), value :: ipiv
            type(C_PTR), value :: b
            integer(C_INT), value :: ldb
        end function
    end interface

    ! culaDeviceZsytrs(char uplo, int n, int nrhs, culaDoubleComplex* a, int lda, culaInt* ipiv, culaDoubleComplex* b, int ldb);
    interface
        integer(C_INT) function cula_device_zsytrs(uplo,n,nrhs,a,lda,ipiv,b,ldb)&
                BIND(C,name="culaDeviceZsytrs")
            use ISO_C_BINDING
            character(C_CHAR), value :: uplo
            integer(C_INT), value :: n
            integer(C_INT), value :: nrhs
            type(C_PTR), value :: a
            integer(C_INT), value :: lda
            type(C_PTR), value :: ipiv
            type(C_PTR), value :: b
            integer(C_INT), value :: ldb
        end function
    end interface

    ! culaDeviceChetrs(char uplo, int n, int nrhs, culaFloatComplex* a, int lda, culaInt* ipiv, culaFloatComplex* b, int ldb);
    interface
        integer(C_INT) function cula_device_chetrs(uplo,n,nrhs,a,lda,ipiv,b,ldb)&
                BIND(C,name="culaDeviceChetrs")
            use ISO_C_BINDING
            character(C_CHAR), value :: uplo
            integer(C_INT), value :: n
            integer(C_INT), value :: nrhs
            type(C_PTR), value :: a
            integer(C_INT), value :: lda
            type(C_PTR), value :: ipiv
            type(C_PTR), value :: b
            integer(C_INT), value :: ldb
        end function
    end interface

    ! culaDeviceZhetrs(char uplo, int n, int nrhs, culaDoubleComplex* a, int lda, culaInt* ipiv, culaDoubleComplex* b, int ldb);
    interface
        integer(C_INT) function cula_device_zhetrs(uplo,n,nrhs,a,lda,ipiv,b,ldb)&
                BIND(C,name="culaDeviceZhetrs")
            use ISO_C_BINDING
            character(C_CHAR), value :: uplo
            integer(C_INT), value :: n
            integer(C_INT), value :: nrhs
            type(C_PTR), value :: a
            integer(C_INT), value :: lda
            type(C_PTR), value :: ipiv
            type(C_PTR), value :: b
            integer(C_INT), value :: ldb
        end function
    end interface

    ! culaDeviceStrtri(char uplo, char diag, int n, culaFloat* a, int lda);
    interface
        integer(C_INT) function cula_device_strtri(uplo,diag,n,a,lda)&
                BIND(C,name="culaDeviceStrtri")
            use ISO_C_BINDING
            character(C_CHAR), value :: uplo
            character(C_CHAR), value :: diag
            integer(C_INT), value :: n
            type(C_PTR), value :: a
            integer(C_INT), value :: lda
        end function
    end interface

    ! culaDeviceDtrtri(char uplo, char diag, int n, culaDouble* a, int lda);
    interface
        integer(C_INT) function cula_device_dtrtri(uplo,diag,n,a,lda)&
                BIND(C,name="culaDeviceDtrtri")
            use ISO_C_BINDING
            character(C_CHAR), value :: uplo
            character(C_CHAR), value :: diag
            integer(C_INT), value :: n
            type(C_PTR), value :: a
            integer(C_INT), value :: lda
        end function
    end interface

    ! culaDeviceCtrtri(char uplo, char diag, int n, culaFloatComplex* a, int lda);
    interface
        integer(C_INT) function cula_device_ctrtri(uplo,diag,n,a,lda)&
                BIND(C,name="culaDeviceCtrtri")
            use ISO_C_BINDING
            character(C_CHAR), value :: uplo
            character(C_CHAR), value :: diag
            integer(C_INT), value :: n
            type(C_PTR), value :: a
            integer(C_INT), value :: lda
        end function
    end interface

    ! culaDeviceZtrtri(char uplo, char diag, int n, culaDoubleComplex* a, int lda);
    interface
        integer(C_INT) function cula_device_ztrtri(uplo,diag,n,a,lda)&
                BIND(C,name="culaDeviceZtrtri")
            use ISO_C_BINDING
            character(C_CHAR), value :: uplo
            character(C_CHAR), value :: diag
            integer(C_INT), value :: n
            type(C_PTR), value :: a
            integer(C_INT), value :: lda
        end function
    end interface

    ! culaDeviceStrtrs(char uplo, char trans, char diag, int n, int nrhs, culaFloat* a, int lda, culaFloat* b, int ldb);
    interface
        integer(C_INT) function cula_device_strtrs(uplo,trans,diag,n,nrhs,a,lda,b,ldb)&
                BIND(C,name="culaDeviceStrtrs")
            use ISO_C_BINDING
            character(C_CHAR), value :: uplo
            character(C_CHAR), value :: trans
            character(C_CHAR), value :: diag
            integer(C_INT), value :: n
            integer(C_INT), value :: nrhs
            type(C_PTR), value :: a
            integer(C_INT), value :: lda
            type(C_PTR), value :: b
            integer(C_INT), value :: ldb
        end function
    end interface

    ! culaDeviceDtrtrs(char uplo, char trans, char diag, int n, int nrhs, culaDouble* a, int lda, culaDouble* b, int ldb);
    interface
        integer(C_INT) function cula_device_dtrtrs(uplo,trans,diag,n,nrhs,a,lda,b,ldb)&
                BIND(C,name="culaDeviceDtrtrs")
            use ISO_C_BINDING
            character(C_CHAR), value :: uplo
            character(C_CHAR), value :: trans
            character(C_CHAR), value :: diag
            integer(C_INT), value :: n
            integer(C_INT), value :: nrhs
            type(C_PTR), value :: a
            integer(C_INT), value :: lda
            type(C_PTR), value :: b
            integer(C_INT), value :: ldb
        end function
    end interface

    ! culaDeviceCtrtrs(char uplo, char trans, char diag, int n, int nrhs, culaFloatComplex* a, int lda, culaFloatComplex* b, int ldb);
    interface
        integer(C_INT) function cula_device_ctrtrs(uplo,trans,diag,n,nrhs,a,lda,b,ldb)&
                BIND(C,name="culaDeviceCtrtrs")
            use ISO_C_BINDING
            character(C_CHAR), value :: uplo
            character(C_CHAR), value :: trans
            character(C_CHAR), value :: diag
            integer(C_INT), value :: n
            integer(C_INT), value :: nrhs
            type(C_PTR), value :: a
            integer(C_INT), value :: lda
            type(C_PTR), value :: b
            integer(C_INT), value :: ldb
        end function
    end interface

    ! culaDeviceZtrtrs(char uplo, char trans, char diag, int n, int nrhs, culaDoubleComplex* a, int lda, culaDoubleComplex* b, int ldb);
    interface
        integer(C_INT) function cula_device_ztrtrs(uplo,trans,diag,n,nrhs,a,lda,b,ldb)&
                BIND(C,name="culaDeviceZtrtrs")
            use ISO_C_BINDING
            character(C_CHAR), value :: uplo
            character(C_CHAR), value :: trans
            character(C_CHAR), value :: diag
            integer(C_INT), value :: n
            integer(C_INT), value :: nrhs
            type(C_PTR), value :: a
            integer(C_INT), value :: lda
            type(C_PTR), value :: b
            integer(C_INT), value :: ldb
        end function
    end interface

    ! culaDeviceCgeConjugate(int m, int n, culaFloatComplex* a, int lda);
    interface
        integer(C_INT) function cula_device_cge_conjugate(m,n,a,lda)&
                BIND(C,name="culaDeviceCgeConjugate")
            use ISO_C_BINDING
            integer(C_INT), value :: m
            integer(C_INT), value :: n
            type(C_PTR), value :: a
            integer(C_INT), value :: lda
        end function
    end interface

    ! culaDeviceZgeConjugate(int m, int n, culaDoubleComplex* a, int lda);
    interface
        integer(C_INT) function cula_device_zge_conjugate(m,n,a,lda)&
                BIND(C,name="culaDeviceZgeConjugate")
            use ISO_C_BINDING
            integer(C_INT), value :: m
            integer(C_INT), value :: n
            type(C_PTR), value :: a
            integer(C_INT), value :: lda
        end function
    end interface

    ! culaDeviceCtrConjugate(char uplo, char diag, int m, int n, culaFloatComplex* a, int lda);
    interface
        integer(C_INT) function cula_device_ctr_conjugate(uplo,diag,m,n,a,lda)&
                BIND(C,name="culaDeviceCtrConjugate")
            use ISO_C_BINDING
            character(C_CHAR), value :: uplo
            character(C_CHAR), value :: diag
            integer(C_INT), value :: m
            integer(C_INT), value :: n
            type(C_PTR), value :: a
            integer(C_INT), value :: lda
        end function
    end interface

    ! culaDeviceZtrConjugate(char uplo, char diag, int m, int n, culaDoubleComplex* a, int lda);
    interface
        integer(C_INT) function cula_device_ztr_conjugate(uplo,diag,m,n,a,lda)&
                BIND(C,name="culaDeviceZtrConjugate")
            use ISO_C_BINDING
            character(C_CHAR), value :: uplo
            character(C_CHAR), value :: diag
            integer(C_INT), value :: m
            integer(C_INT), value :: n
            type(C_PTR), value :: a
            integer(C_INT), value :: lda
        end function
    end interface

    ! culaDeviceSgeNancheck(int m, int n, culaFloat* a, int lda);
    interface
        integer(C_INT) function cula_device_sge_nancheck(m,n,a,lda)&
                BIND(C,name="culaDeviceSgeNancheck")
            use ISO_C_BINDING
            integer(C_INT), value :: m
            integer(C_INT), value :: n
            type(C_PTR), value :: a
            integer(C_INT), value :: lda
        end function
    end interface

    ! culaDeviceDgeNancheck(int m, int n, culaDouble* a, int lda);
    interface
        integer(C_INT) function cula_device_dge_nancheck(m,n,a,lda)&
                BIND(C,name="culaDeviceDgeNancheck")
            use ISO_C_BINDING
            integer(C_INT), value :: m
            integer(C_INT), value :: n
            type(C_PTR), value :: a
            integer(C_INT), value :: lda
        end function
    end interface

    ! culaDeviceCgeNancheck(int m, int n, culaFloatComplex* a, int lda);
    interface
        integer(C_INT) function cula_device_cge_nancheck(m,n,a,lda)&
                BIND(C,name="culaDeviceCgeNancheck")
            use ISO_C_BINDING
            integer(C_INT), value :: m
            integer(C_INT), value :: n
            type(C_PTR), value :: a
            integer(C_INT), value :: lda
        end function
    end interface

    ! culaDeviceZgeNancheck(int m, int n, culaDoubleComplex* a, int lda);
    interface
        integer(C_INT) function cula_device_zge_nancheck(m,n,a,lda)&
                BIND(C,name="culaDeviceZgeNancheck")
            use ISO_C_BINDING
            integer(C_INT), value :: m
            integer(C_INT), value :: n
            type(C_PTR), value :: a
            integer(C_INT), value :: lda
        end function
    end interface

    ! culaDeviceSgeTranspose(int m, int n, culaFloat* a, int lda, culaFloat* b, int ldb);
    interface
        integer(C_INT) function cula_device_sge_transpose(m,n,a,lda,b,ldb)&
                BIND(C,name="culaDeviceSgeTranspose")
            use ISO_C_BINDING
            integer(C_INT), value :: m
            integer(C_INT), value :: n
            type(C_PTR), value :: a
            integer(C_INT), value :: lda
            type(C_PTR), value :: b
            integer(C_INT), value :: ldb
        end function
    end interface

    ! culaDeviceDgeTranspose(int m, int n, culaDouble* a, int lda, culaDouble* b, int ldb);
    interface
        integer(C_INT) function cula_device_dge_transpose(m,n,a,lda,b,ldb)&
                BIND(C,name="culaDeviceDgeTranspose")
            use ISO_C_BINDING
            integer(C_INT), value :: m
            integer(C_INT), value :: n
            type(C_PTR), value :: a
            integer(C_INT), value :: lda
            type(C_PTR), value :: b
            integer(C_INT), value :: ldb
        end function
    end interface

    ! culaDeviceCgeTranspose(int m, int n, culaFloatComplex* a, int lda, culaFloatComplex* b, int ldb);
    interface
        integer(C_INT) function cula_device_cge_transpose(m,n,a,lda,b,ldb)&
                BIND(C,name="culaDeviceCgeTranspose")
            use ISO_C_BINDING
            integer(C_INT), value :: m
            integer(C_INT), value :: n
            type(C_PTR), value :: a
            integer(C_INT), value :: lda
            type(C_PTR), value :: b
            integer(C_INT), value :: ldb
        end function
    end interface

    ! culaDeviceZgeTranspose(int m, int n, culaDoubleComplex* a, int lda, culaDoubleComplex* b, int ldb);
    interface
        integer(C_INT) function cula_device_zge_transpose(m,n,a,lda,b,ldb)&
                BIND(C,name="culaDeviceZgeTranspose")
            use ISO_C_BINDING
            integer(C_INT), value :: m
            integer(C_INT), value :: n
            type(C_PTR), value :: a
            integer(C_INT), value :: lda
            type(C_PTR), value :: b
            integer(C_INT), value :: ldb
        end function
    end interface

    ! culaDeviceSgeTransposeInplace(int n, culaFloat* a, int lda);
    interface
        integer(C_INT) function cula_device_sge_transpose_inplace(n,a,lda)&
                BIND(C,name="culaDeviceSgeTransposeInplace")
            use ISO_C_BINDING
            integer(C_INT), value :: n
            type(C_PTR), value :: a
            integer(C_INT), value :: lda
        end function
    end interface

    ! culaDeviceDgeTransposeInplace(int n, culaDouble* a, int lda);
    interface
        integer(C_INT) function cula_device_dge_transpose_inplace(n,a,lda)&
                BIND(C,name="culaDeviceDgeTransposeInplace")
            use ISO_C_BINDING
            integer(C_INT), value :: n
            type(C_PTR), value :: a
            integer(C_INT), value :: lda
        end function
    end interface

    ! culaDeviceCgeTransposeInplace(int n, culaFloatComplex* a, int lda);
    interface
        integer(C_INT) function cula_device_cge_transpose_inplace(n,a,lda)&
                BIND(C,name="culaDeviceCgeTransposeInplace")
            use ISO_C_BINDING
            integer(C_INT), value :: n
            type(C_PTR), value :: a
            integer(C_INT), value :: lda
        end function
    end interface

    ! culaDeviceZgeTransposeInplace(int n, culaDoubleComplex* a, int lda);
    interface
        integer(C_INT) function cula_device_zge_transpose_inplace(n,a,lda)&
                BIND(C,name="culaDeviceZgeTransposeInplace")
            use ISO_C_BINDING
            integer(C_INT), value :: n
            type(C_PTR), value :: a
            integer(C_INT), value :: lda
        end function
    end interface

    ! culaDeviceCgeTransposeConjugate(int m, int n, culaFloatComplex* a, int lda, culaFloatComplex* b, int ldb);
    interface
        integer(C_INT) function cula_device_cge_transpose_conjugate(m,n,a,lda,b,ldb)&
                BIND(C,name="culaDeviceCgeTransposeConjugate")
            use ISO_C_BINDING
            integer(C_INT), value :: m
            integer(C_INT), value :: n
            type(C_PTR), value :: a
            integer(C_INT), value :: lda
            type(C_PTR), value :: b
            integer(C_INT), value :: ldb
        end function
    end interface

    ! culaDeviceZgeTransposeConjugate(int m, int n, culaDoubleComplex* a, int lda, culaDoubleComplex* b, int ldb);
    interface
        integer(C_INT) function cula_device_zge_transpose_conjugate(m,n,a,lda,b,ldb)&
                BIND(C,name="culaDeviceZgeTransposeConjugate")
            use ISO_C_BINDING
            integer(C_INT), value :: m
            integer(C_INT), value :: n
            type(C_PTR), value :: a
            integer(C_INT), value :: lda
            type(C_PTR), value :: b
            integer(C_INT), value :: ldb
        end function
    end interface

    ! culaDeviceCgeTransposeConjugateInplace(int n, culaFloatComplex* a, int lda);
    interface
        integer(C_INT) function cula_device_cge_transpose_conjugate_inplace(n,a,lda)&
                BIND(C,name="culaDeviceCgeTransposeConjugateInplace")
            use ISO_C_BINDING
            integer(C_INT), value :: n
            type(C_PTR), value :: a
            integer(C_INT), value :: lda
        end function
    end interface

    ! culaDeviceZgeTransposeConjugateInplace(int n, culaDoubleComplex* a, int lda);
    interface
        integer(C_INT) function cula_device_zge_transpose_conjugate_inplace(n,a,lda)&
                BIND(C,name="culaDeviceZgeTransposeConjugateInplace")
            use ISO_C_BINDING
            integer(C_INT), value :: n
            type(C_PTR), value :: a
            integer(C_INT), value :: lda
        end function
    end interface
end module

