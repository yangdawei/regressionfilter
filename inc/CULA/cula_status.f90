
! types
module cula_type
    use ISO_C_BINDING
    implicit none

    enum, bind(C) 
        enumerator :: culaNoError, culaNotInitialized, culaNoHardware, culaInsufficientRuntime 
        enumerator :: culaInsufficientComputeCapability, culaInsufficientMemory
        enumerator :: culaFeatureNotImplemented, culaArgumentError, culaDataError 
        enumerator :: culaBlasError, culaRuntimeError, culaBadStorageFormat, culaInvalidReferenceHandle
        enumerator :: culaUnspecifiedError 
    end enum
end module

module cula_status
    use, intrinsic :: ISO_C_BINDING
    use cula_type

    implicit none

    interface
        integer(C_INT) function cula_initialize() &
        BIND(C,NAME="culaInitialize")
            use ISO_C_BINDING
        end function
    end interface

    interface
        subroutine cula_shutdown() &
        BIND(C,NAME="culaShutdown")
            use ISO_C_BINDING
        end subroutine
    end interface

    interface
      integer(C_INT) function cula_get_error_info_string(status,info,buf,bufsize) &
        BIND(C,NAME="culaGetErrorInfoString")
            use ISO_C_BINDING
            integer(C_INT), value, intent(in) :: status
            integer(C_INT), value, intent(in) :: info
            integer(C_INT), value, intent(in) :: bufsize
            character(C_CHAR), dimension(bufsize), intent(out) :: buf
      end function
    end interface

    interface
        integer(C_INT) function cula_get_error_info() &
        BIND(C,NAME="culaGetErrorInfo")
            use ISO_C_BINDING
        end function
    end interface

    interface
        subroutine cula_free_buffers() &
        BIND(C,NAME="culaFreeBuffers")
            use ISO_C_BINDING
        end subroutine
    end interface

    interface
        integer(C_INT) function cula_get_version() &
        BIND(C,NAME="culaGetVersion")
            use ISO_C_BINDING
        end function
    end interface

    interface
        integer(C_INT) function cula_get_cuda_minimum_version() &
        BIND(C,NAME="culaGetCudaMinimumVersion")
            use ISO_C_BINDING
        end function
    end interface

    interface
        integer(C_INT) function cula_get_cuda_runtime_version() &
        BIND(C,NAME="culaGetCudaRuntimeVersion")
            use ISO_C_BINDING
        end function
    end interface

    interface
        integer(C_INT) function cula_get_cuda_driver_version() &
        BIND(C,NAME="culaGetCudaDriverVersion")
            use ISO_C_BINDING
        end function
    end interface

    interface
        integer(C_INT) function cula_get_cublas_minimum_version() &
        BIND(C,NAME="culaGetCublasMinimumVersion")
            use ISO_C_BINDING
        end function
    end interface

    interface
        integer(C_INT) function cula_get_cublas_runtime_version() &
        BIND(C,NAME="culaGetCublasRuntimeVersion")
            use ISO_C_BINDING
        end function
    end interface

contains

    subroutine cula_check_status(status)
        integer (C_INT) :: status
        integer :: s
        integer :: info
        character(len=256) :: buf
        integer :: i

        if (status .ne. culanoerror) then
            info = cula_get_error_info()

            s = cula_get_error_info_string(status, info, buf, len(buf))
            write(*,'(a)') buf
            if (status .eq. culainsufficientcomputecapability) then
                stop 0
            else
                stop 1
            endif
        endif

    end subroutine

end module cula_status

