#define _CRT_SECURE_NO_WARNINGS

#include "neural_network.h"
#include <vector>
#include <iostream>
#include <iomanip>
#include <sstream>
using namespace std;

clock_t g_start;


void test_train()
{
	printf("testing train...\n");
	NeuralNetwork nn = read_nn("d:\\data\\btf\\test\\2_20_20_3.netconf");
	vector<Sample> samples;
	for (int i = 0; i < 4096; ++i) {
		float x = (float)rand() / RAND_MAX;
		float y = (float)rand() / RAND_MAX;
		float z = (x + y) / 2;
		Sample sample;
		sample.input[0] = x;
		sample.input[1] = y;
		sample.output[0] = z;
		samples.push_back(sample);
	}
	nn.log_file = fopen("d:\\data\\btf\\test\\2_20_20_1.trainlog", "w");
	float error;
	init_train();
	train(nn, samples.data(), samples.size(), error);
	printf("%f\n", error);
	fclose(nn.log_file);
	printf("train passed.\n");
}

int main()
{
	init_train();
	test_train();
}
