#ifndef _NEURAL_NETWORK_H_
#define _NEURAL_NETWORK_H_
#include <stdio.h>
#include <iostream>
#include <vector>
// #define DEBUG
// use double configuration
#ifdef _USE_DOUBLE_
typedef double Scalar;
#else
typedef float Scalar;
#endif

// number of nodes in input layer
const int INPUT_SIZE = 12;
// number of nodes in output layer
const int OUTPUT_SIZE = 4;
// LAYER_CNT and PARAM_SIZE influence singleton NeuralNetwork only, so these constants can be reasonably large. 
// Make sure that NeuralNetwork can fit into CUDA constant memory.
const int LAYER_CNT = 5;
const int PARAM_SIZE = 2000;

const int BLOCKS = 64;
const int THREADS = 64;
const int SAMPLES_SIZE = BLOCKS * THREADS;

struct Sample {
	Scalar input[INPUT_SIZE];
	Scalar output[OUTPUT_SIZE];
};

struct NeuralNetwork {
	int S[LAYER_CNT];
	int S_size;

	Scalar mu, beta, ERROR_THRESHOLD, MIN_GRAD, MIN_MU, MAX_MU;
	int MAX_EPOCH;

	FILE *log_file;

	// below are private parameters
	int layer_size;
	Scalar param[PARAM_SIZE];
	Scalar min_input[INPUT_SIZE], max_input[INPUT_SIZE];
	Scalar min_output[OUTPUT_SIZE], max_output[OUTPUT_SIZE];
	int param_size;
	int output_size;
};

void feed(const NeuralNetwork &nn, const Scalar *input, Scalar *output);
int train(NeuralNetwork &nn, const Sample *samples, int samples_size, float &error);
NeuralNetwork get_default_nn();
void normalize(std::vector<Sample> &samples, NeuralNetwork &net);
void dump_nn(FILE *f, NeuralNetwork nn);
NeuralNetwork read_nn(const char *filename);
void init_train();
void setDevice(int id);

#endif