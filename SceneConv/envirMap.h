#pragma once
#include <opencv2\opencv.hpp>
#include "GL\GL.h"
#include "GL\GLAUX.H"
#include "GL\GLEXT.H"

//#include "1.0\glm\glm\glm.hpp"
#include "1.0/wglh/wglshader.h"

#ifndef glGenerateMipmapEXT
#define glGenerateMipmapEXT
#endif

#ifndef ENVIRMAP_H
#define ENVIRMAP_H

class EnvirMap
{
private:
	IplImage * images[6];
public:
	GLuint cubeMap;
private:
	bool copyImg(IplImage * dst, IplImage * pfm, CvRect rect);
	bool loadPfm(const char * filename, IplImage * &dst);
	bool createImg(const char* filename);
public:
	bool createCubeMap(const char * filename);
	bool sendToShader(wglh::glsl::CwglProgramEx &p,const char * samplerName);
	bool deleteMap();
};

#endif