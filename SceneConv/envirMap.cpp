#include "stdafx.h"
#include "EnvirMap.h"
#include <stdio.h>

bool EnvirMap :: loadPfm(const char * filename, IplImage * &dst)
{
	char mark[255];
	FILE *in = fopen(filename, "rb");
	int width, height;
	fscanf(in, "%s\n%d %d\n%s\n", mark, &width, &height, mark);
	dst = cvCreateImage(cvSize(width, height), IPL_DEPTH_32F, 3);

	for (int i = 0; i <height; i++)
	{
		char*  buf = dst->imageData + i*dst->widthStep;
		fread(buf, 3*sizeof(float), width, in);
	}
	
	return true;
}

bool EnvirMap ::createImg(const char * filename)
{
	IplImage * tempImg;
	loadPfm(filename, tempImg);
	int width = tempImg->width/3;
	int height = tempImg->height/4;
	for (int i = 0; i < 6; i++)
	{
		images[i] = cvCreateImage(cvSize(width,height), IPL_DEPTH_32F, 3);
 	}
	
	copyImg(images[0], tempImg, cvRect(width,0,width,height));
	copyImg(images[1], tempImg, cvRect(width,height,width,height));
	copyImg(images[2], tempImg, cvRect(0,2*height,width,height));
	copyImg(images[3], tempImg, cvRect(width,2*height,width,height));
	copyImg(images[4], tempImg, cvRect(2*width,2*height,width,height));
	copyImg(images[5], tempImg, cvRect(width,3*height,width,height));
	cvFlip(images[0],images[0],-1);

	cvReleaseImage(&tempImg);
	return true;
}

bool EnvirMap ::copyImg(IplImage * dst, IplImage * pfm, CvRect rect)
{
	cvSetImageROI(pfm, rect);
	cvCopy(pfm, dst);
	cvResetImageROI(pfm);
	return true;
}

bool EnvirMap ::createCubeMap(const char * filename)
{
	createImg(filename);
	GLfloat largest_supported_anisotropic;
	glGetFloatv(GL_MAX_TEXTURE_MAX_ANISOTROPY_EXT, &largest_supported_anisotropic);
	//printf("%f\n", largest_supported_anisotropic);

	
	glEnable(GL_TEXTURE_CUBE_MAP);
	glPushAttrib(GL_ALL_ATTRIB_BITS);
	



	//glEnable(GL_TEXTURE_2D);
	wglh::ext::CwglMultiTextureEnv::ActiveTextureUnit(7);
	
	
	/*glBindTexture(GL_TEXTURE_2D, cubeMap);
	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, 256, 256,
		0, GL_RGB, GL_FLOAT, images[4]->imageData);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	return 0;*/
	glGenTextures(1, &cubeMap);
	//cout << cubeMap << endl;
	glBindTexture(GL_TEXTURE_CUBE_MAP, cubeMap);

	int width = images[0]->width;
	int height = images[0]->height;

	

	glTexImage2D(GL_TEXTURE_CUBE_MAP_POSITIVE_X, 0, GL_RGB, width, height,
		0, GL_RGB, GL_FLOAT, images[4]->imageData);
	glTexImage2D(GL_TEXTURE_CUBE_MAP_NEGATIVE_X, 0, GL_RGB, width, height,
		0, GL_RGB, GL_FLOAT, images[2]->imageData);
	glTexImage2D(GL_TEXTURE_CUBE_MAP_POSITIVE_Y, 0, GL_RGB, width, height,
		0, GL_RGB, GL_FLOAT, images[1]->imageData);
	glTexImage2D(GL_TEXTURE_CUBE_MAP_NEGATIVE_Y, 0, GL_RGB, width, height,
		0, GL_RGB, GL_FLOAT, images[5]->imageData);
	glTexImage2D(GL_TEXTURE_CUBE_MAP_POSITIVE_Z, 0, GL_RGB, width, height,
		0, GL_RGB, GL_FLOAT, images[3]->imageData);
	glTexImage2D(GL_TEXTURE_CUBE_MAP_NEGATIVE_Z, 0, GL_RGB, width, height,
		0, GL_RGB, GL_FLOAT, images[0]->imageData);

	glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_GENERATE_MIPMAP, GL_TRUE);
	glTexEnvf( GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_REPLACE );

	glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_S,	GL_REPEAT);
	glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_T,	GL_REPEAT);
	glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_R,	GL_REPEAT);
	glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);
	glTexParameterf(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_MAX_ANISOTROPY_EXT, largest_supported_anisotropic);

	//glGenerateMipmapEXT(GL_TEXTURE_CUBE_MAP);

	
	glPopAttrib();
	//glDisable(GL_TEXTURE_CUBE_MAP);
	return true;
}

bool EnvirMap ::sendToShader(wglh::glsl::CwglProgramEx &p, const char * samplerName)
{
	glEnable(GL_TEXTURE_CUBE_MAP);
	wglh::ext::CwglMultiTextureEnv::ActiveTextureUnit(7);
	glBindTexture(GL_TEXTURE_CUBE_MAP, cubeMap);
	p.SetUniform(samplerName,7);
	
	return true;
}

bool EnvirMap ::deleteMap()
{
	for (int i = 0; i < 6; i++)
	{
		if (images[i] != 0)
			cvReleaseImage(&images[i]);
	}
	glDeleteTextures(1, &cubeMap);
	return true;
}