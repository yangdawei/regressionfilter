// SceneConv.cpp : Defines the entry point for the application.
//


#include "stdafx.h"
#include "SceneConv.h"
#include "1.0\rt\expr_templ.h"
#include "mesh_proc.h"
#include "1.0\w32\w32_misc.h"
#include "1.0\wglh\ArcballWndEx.h"
#include "SceneWnd.h"

using namespace num;

CMainWnd::CMainWnd()
{
	m_bShowBox = TRUE;
	m_bWireframe = FALSE;
	m_bShowTexture = TRUE;
	m_bPointCloud = FALSE;
	m_IsDragging = FALSE;
	m_bShowSelected = TRUE;

	m_FileTitle = _T('\0');

	m_Mesh.EnableAutoUpdateElementId();
	m_wUnwarpUI.m_pMainWnd = this;
	{
		using namespace jgl;
		JSpriteContainer::setResourcePath(".\\raw\\");
		m_sprites = new JSpriteContainer(150,150,300,300,true);
		m_sprites->addSprite(new JGLLabel(10,10,"test label"));
		m_sprites->addSprite(new JGLSeekBar(new float(),0,100,100,80,"seekbar"));
	}
}

CMainWnd::~CMainWnd(){
	if(m_sprites)
		delete m_sprites;
}

LRESULT CMainWnd::WndProc(UINT message, WPARAM wParam, LPARAM lParam)
{
	if(message == WM_LBUTTONDOWN && (MK_CONTROL&wParam) )
	{
		m_IsDragging = TRUE;
		m_MouseEnd.x = GET_X_LPARAM(lParam);
		m_MouseEnd.y = GET_Y_LPARAM(lParam);
		m_MouseStart = m_MouseEnd;
		wglh::CwglArcballWnd::SetCapture();
		w32::HideMouseCursor();
		return 0;
	}

	switch(message){
		case WM_LBUTTONDOWN:
		case WM_LBUTTONUP:
		case WM_MOUSEMOVE:
			m_MouseEnd.x = GET_X_LPARAM(lParam);
			m_MouseEnd.y = GET_Y_LPARAM(lParam);
			m_MouseEnd.x = min(width-1,max(0,m_MouseEnd.x));
			m_MouseEnd.y = min(height-1,max(0,m_MouseEnd.y));

			if((message == WM_LBUTTONDOWN && m_sprites->MouseDown(m_MouseEnd.x,height - m_MouseEnd.y))
				|| (message == WM_LBUTTONUP && m_sprites->MouseUp(m_MouseEnd.x,height - m_MouseEnd.y))
				|| (message == WM_MOUSEMOVE && m_sprites->MouseMove(m_MouseEnd.x,height - m_MouseEnd.y)) ){
				OnTransformChanged();
				return 0;
			}
	}


	if(m_InteractiveMode==0)
	{
		float fine = (wParam & MK_CONTROL)?0.1f:1.0f;
		switch(message)
		{
		
		case WM_MOUSEWHEEL:			
			 m_transformCtrl.MouseWheel(fine*(((short)HIWORD(wParam))/1500.0f));
			 OnTransformChanged();
			break;
		case WM_LBUTTONDOWN:
		case WM_RBUTTONDOWN:
			m_MouseEnd.x = GET_X_LPARAM(lParam);
			m_MouseEnd.y = GET_Y_LPARAM(lParam);
			m_MouseEnd.x = min(width-1,max(0,m_MouseEnd.x));
			m_MouseEnd.y = min(height-1,max(0,m_MouseEnd.y));

			if(message == WM_LBUTTONDOWN)
				m_transformCtrl.MouseDown(m_MouseEnd.x,m_MouseEnd.y,wglh::CTransformCtrl::ENUM_MOVE);
			else if (message == WM_RBUTTONDOWN)
				m_transformCtrl.MouseDown(m_MouseEnd.x,m_MouseEnd.y,wglh::CTransformCtrl::ENUM_ROTATE);

			wglh::CwglArcballWnd::SetCapture();
			OnTransformChanged();
			break;
		case WM_MOUSEMOVE:
			m_MouseEnd.x = GET_X_LPARAM(lParam);
			m_MouseEnd.y = GET_Y_LPARAM(lParam);
			m_MouseEnd.x = min(width-1,max(0,m_MouseEnd.x));
			m_MouseEnd.y = min(height-1,max(0,m_MouseEnd.y));
			if(wParam&MK_LBUTTON)
				m_transformCtrl.MouseMove(m_MouseEnd.x,m_MouseEnd.y,wglh::CTransformCtrl::ENUM_MOVE);
			else if(wParam&MK_RBUTTON)
				m_transformCtrl.MouseMove(m_MouseEnd.x,m_MouseEnd.y,wglh::CTransformCtrl::ENUM_ROTATE);
			OnTransformChanged();
			break;
		case WM_LBUTTONUP:
		case WM_RBUTTONUP:
			m_MouseEnd.x = GET_X_LPARAM(lParam);
			m_MouseEnd.y = GET_Y_LPARAM(lParam);
			m_MouseEnd.x = min(width-1,max(0,m_MouseEnd.x));
			m_MouseEnd.y = min(height-1,max(0,m_MouseEnd.y));
			if(message == WM_LBUTTONUP)
				m_transformCtrl.MouseUp(m_MouseEnd.x,m_MouseEnd.y,wglh::CTransformCtrl::ENUM_MOVE);
			else if(message == WM_RBUTTONUP)
				m_transformCtrl.MouseUp(m_MouseEnd.x,m_MouseEnd.y,wglh::CTransformCtrl::ENUM_ROTATE);
			wglh::CwglArcballWnd::ReleaseCapture();
			OnTransformChanged();
			break;
		}
	} else if(m_InteractiveMode){
		switch(message)
		{
		case WM_LBUTTONDOWN:
			if(IMC_SCENE == m_InteractiveMode){	
				m_transformCtrl.MouseDown((short)LOWORD(lParam), (short)HIWORD(lParam),wglh::CTransformCtrl::ENUM_ROTATE);
			}
			break;
		case WM_LBUTTONUP:
			m_transformCtrl.MouseUp((short)LOWORD(lParam), (short)HIWORD(lParam),wglh::CTransformCtrl::ENUM_ROTATE);
			break;
		case WM_MOUSEMOVE:
			{
				if(wParam&MK_LBUTTON)
				{
					if(IMC_SCENE == m_InteractiveMode)
					{
						m_transformCtrl.MouseMove((short)LOWORD(lParam), (short)HIWORD(lParam),wglh::CTransformCtrl::ENUM_ROTATE);
					}	
				}
			}
			break;
		}
	}	

	if(m_IsDragging)
	{	
		switch(message)
		{
		case WM_MOUSEMOVE:
			m_MouseEnd.x = GET_X_LPARAM(lParam);
			m_MouseEnd.y = GET_Y_LPARAM(lParam);
			m_MouseEnd.x = min(width-1,max(0,m_MouseEnd.x));
			m_MouseEnd.y = min(height-1,max(0,m_MouseEnd.y));
			OnRender();
			break;
		case WM_LBUTTONUP:
			m_MouseEnd.x = GET_X_LPARAM(lParam);
			m_MouseEnd.y = GET_Y_LPARAM(lParam);
			m_MouseEnd.x = min(width-1,max(0,m_MouseEnd.x));
			m_MouseEnd.y = min(height-1,max(0,m_MouseEnd.y));
			m_IsDragging = FALSE;
			m_bShowSelected = TRUE;
			//per-vertex hit test
			//....
			wglh::CwglArcballWnd::ReleaseCapture();
			w32::ShowMouseCursor();
			Render();
			break;
		default:
			return __super::WndProc(message,wParam,lParam);
		}
		return 0;
	}


	switch(message)
	{
	case WM_DROPFILES:
		{
			w32::CFileList	flist;
			flist.ParseDropFileList((HDROP)wParam,TRUE);
			if(flist.GetFileCount())
			{
				rt::String fn(flist[0]);
				fn.MakeLower();
				if(fn.FindString(_T(".obj"))>0)
				{
					ImportObject(fn);
				}
				else if(fn.FindString(_T(".scene"))>0)
				{
					LoadScene(fn);
				}
			}
		}
		break;
	case WM_KEYDOWN:
		{
			switch(wParam)
			{
			case 'W':
				m_bWireframe = !m_bWireframe;
				OnRender();
				break;
			case 'B':
				m_bShowBox = !m_bShowBox;
				OnRender();
				break;
			case 'P':
				m_bPointCloud = !m_bPointCloud;
				OnRender();
				break;
			case 'S':
				if(::GetAsyncKeyState(VK_CONTROL)<0)
				{
					m_Mesh.LaplacianSmoothing();
					Render();
				}
				break;
			case 'T':
				m_bShowTexture = !m_bShowTexture;
				OnRender();
				break;
			}
		}
		break;
	case WM_COMMAND:
		{
			switch(wParam)
			{
			case ID_FILE_EXPORT:
				{
					USE_FILE_DIALOG_DN(*this,"Wavefront obj files (*.obj)\0*.obj\0All files\0*.*\0",m_FileTitle);
					LPTSTR p = GET_FILENAME(FALSE);
					if(p)
					{	APPEND_EXTNAME(p,_T(".obj"));
						ExportWavefrontObjFile(p,m_Mesh);
					}
				}
				break;
			case ID_FILE_SAVESCENEFILE:
				{
					USE_FILE_DIALOG_DN(*this,"Codelib Scene files (*.scene)\0*.scene\0All files\0*.*\0",m_FileTitle);
					LPTSTR p = GET_FILENAME(FALSE);
					if(p)
					{	APPEND_EXTNAME(p,".scene");
						SetWindowText(m_FileTitle = w32::CFullPathName(p).GetFileTitle());
						_CheckDump("Saving scene to "<<p<<".\n");
						if(FAILED(m_Mesh.Save(p)))
							_CheckDump("Failed to save "<<p<<"\n");
					}
				}
				break;
			case ID_FILE_LOAD: //load obj
				{
					USE_FILE_DIALOG(*this,"Wavefront obj files (*.obj)\0*.obj\0All files\0*.*\0");
					LPCTSTR p = GET_FILENAME(TRUE);
					if(p)ImportObject(p);
				}
				break;
			case ID_FILE_LOADSCENEFILE:
				{
					USE_FILE_DIALOG(*this,"Codelib Scene files (*.scene)\0*.scene\0All files\0*.*\0");
					LPCTSTR p = GET_FILENAME(TRUE);
					if(p)LoadScene(p);
				}
				break;	//load scene
			case ID_FILE_LOADSCENEINORIGINALSIZE:
				{
					USE_FILE_DIALOG(*this,"Codelib Scene files (*.scene)\0*.scene\0All files\0*.*\0");
					LPCTSTR p = GET_FILENAME(TRUE);
					if(p)LoadScene(p,FALSE);
				}
				break;	//load scene
			case ID_EDIT_SAMPLEUVFROM:
				{
					USE_FILE_DIALOG(*this,"Scene file (*.scene)\0*.scene\0");
					LPCTSTR p = GET_FILENAME(TRUE);
					if(p)
					{
						wglh::CwglMesh from;
						if(SUCCEEDED(from.Load(p,wglh::CwglMesh::LOADING_FLAG_BUFFER_IN_MEMORY|
												 wglh::CwglMesh::LOADING_FLAG_KEEP_OBJECT_SIZE))
						   && from.IsTextureBinded())
						{
							SampleTexCoord(from, m_Mesh);
							Show(ID_VIEW_SHOWUV);
						}
						else
						{
							_CheckDump("Failed to load or the scene file has no texture coordinate.\n");
						}

						m_wUnwarpUI.SetScene(m_Mesh);
					}
				}
				break;
			case ID_EDIT_ZPlan_UV:
				{
					Vec3f* uv = m_Mesh.GetBuffer_VertexTexCoord();
					const Vec3f* pos = m_Mesh.Get_VertexPosition();
					for(UINT i=0;i<m_Mesh.GetVertexCount();i++)
					{
						uv[i].x = pos[i].x+0.5f;
						uv[i].y = pos[i].y+0.5f;
						uv[i].z = 0;
					}
					m_wUnwarpUI.SetScene(m_Mesh);
					m_Mesh.SubmitBuffers(TRUE);
					UpdateShowMenu();
					Show(ID_VIEW_SHOWUV);
				}
				break;
			case ID_EDIT_ZPolar_UV:
				{
					Vec3f* uv = m_Mesh.GetBuffer_VertexTexCoord();
					const Vec3f* pos = m_Mesh.Get_VertexPosition();

					for(UINT i=0;i<m_Mesh.GetVertexCount();i++)
					{
						uv[i].z = 0;
						uv[i].x = (float)(acos(pos[i].z/sqrt(pos[i].L2Norm_Sqr()))/PI);

						float dist;
						dist = sqrt(pos[i].v2().L2Norm_Sqr())*2;
						if(dist > 0.01)
						{
							uv[i].x = (float)(acos(pos[i].z/sqrt(pos[i].L2Norm_Sqr()))/PI);
							uv[i].y = (float)(acos(pos[i].y/dist*2)/PI);
						}
						else
						{
							uv[i].x = (float)(acos(pos[i].z/sqrt(pos[i].L2Norm_Sqr()))/PI);
							uv[i].y = 0;
						}
					}

					for(UINT i=0;i<m_Mesh.GetVertexCount();i++)
					{
						float dist;
						dist = sqrt(pos[i].v2().L2Norm_Sqr())*2;
						if(dist < 0.01)
						{
							const UINT* ele = m_Mesh.Get_MeshElementIndic();						
							for(UINT t=0;t<m_Mesh.GetMeshElementIndicCount();t++)
							{	// search for triangle
								if(ele[t] == i)
								{
									UINT tb = (t/3)*3;
									float y = 0;
									for(UINT q=0;q<3;q++)
									{
										if(tb+q != t)
											y += uv[ele[tb+q]].y;
									}
									uv[i].y = y/2;
									break;
								}
							}
						}
					}

					m_wUnwarpUI.SetScene(m_Mesh);
					m_Mesh.SubmitBuffers(TRUE);
					UpdateShowMenu();
					Show(ID_VIEW_SHOWUV);
				}
				break;
			case ID_EDIT_FAKETANGENT:
				{
					if(!m_Mesh.IsNormalBinded())
						m_Mesh.RecalculateNormal();

					Vec3f axis = GetRotationToScene().Product(Vec3f(0,0,-1));

					const Vec3f* norm = m_Mesh.Get_VertexNormal();
					Vec3f* tang = m_Mesh.GetBuffer_VertexTangent();
					for(UINT i=0;i<m_Mesh.GetVertexCount();i++)
					{
						tang[i].CrossProduct(norm[i],axis);
						tang[i].Normalize();
					}

					m_Mesh.SubmitBuffers(TRUE);
					UpdateShowMenu();
					Show(ID_VIEW_SHOWTANGENT);
				}
				Show(ID_VIEW_SHOWTANGENT);
				break;
			case ID_EDIT_MESHSMOOTH:
				m_Mesh.LaplacianSmoothing();
				Render();
				break;
			case ID_EDIT_SMOOTHOUTLIER:
				m_Mesh.SmoothOutlier(1);
				Render();
				break;
			case ID_EDIT_SPLITSHAREDVERTEX:
				m_Mesh.SplitSharedVertex();
				m_wUnwarpUI.SetScene(m_Mesh);
				Render();
				break;
			case ID_EDIT_ORTHOGONALIZETANGENT:
				{	
					if(m_Mesh.IsTangentBinded())
					{
						const Vec3f* norm = m_Mesh.Get_VertexNormal();
						const Vec3f* tang = m_Mesh.Get_VertexTangent();

						Vec3f* tang_out = m_Mesh.GetBuffer_VertexTangent();
						for(UINT i=0;i<m_Mesh.GetVertexCount();i++)
						{
							Vec3f tt;
							tt.CrossProduct(tang[i],norm[i]);
							tang_out[i].CrossProduct(norm[i],tt);
							tang_out[i].Normalize();
						}

						m_Mesh.SubmitBuffers(TRUE);
						UpdateShowMenu();
						Show(ID_VIEW_SHOWTANGENT);
					}
				}
				break;
			case ID_EDIT_MERGEEQUALVERTEX:
				m_Mesh.MergeEquivalentVertex_Position();
				m_wUnwarpUI.SetScene(m_Mesh);
				Render();
				break;
			case ID_EDIT_SUBDIVIDISION:
				m_Mesh.Subdividision();
				m_wUnwarpUI.SetScene(m_Mesh);
				Render();
				break;
            case ID_EDIT_FLIPTRIANGLEFACET:
                m_Mesh.FlipOrientation();
				m_wUnwarpUI.SetScene(m_Mesh);
                Render();
                break;
            case ID_EDIT_FIXTRIANGLEFACETORIENTATION:
                m_Mesh.FixOrientation();
				m_wUnwarpUI.SetScene(m_Mesh);
                Render();
                break;
            case ID_EDIT_RECALCULATEVERTEXNORMAL:
                m_Mesh.RecalculateNormal();
				Show(ID_VIEW_SHOWNORMAL);
                break;
			case ID_SIGNALEDITING_RECALCULATETANGENT:
                m_Mesh.RecalculateTangent();
				Show(ID_VIEW_SHOWTANGENT);
				break;
			case ID_EDIT_COMFORMNORMAL:
				m_Mesh.ConformTangentSpace();
				Show(ID_VIEW_SHOWNORMAL);
				break;
			case ID_EDIT_COMFORMNORMALONLY:
				m_Mesh.ConformTangentSpace(TRUE);
				Show(ID_VIEW_SHOWNORMAL);
				break;
			case IDM_EXIT:
				DestroyWindow();
				break;
			case IDM_LOAD_TEXTURE:
				{
					USE_FILE_DIALOG(*this,"Image files (*.jpg,*.png,*.gif,*.bmp)\0*.jpg;*.png;*.gif;*.bmp\0All files\0*.*\0");
					LPCTSTR p = GET_FILENAME(TRUE);
					if(p)
					{	wglh::CwglImage	img;
						if(img.Load(p))
						{	_CheckDump("Load texture from "<<p<<"\n");
							img.VerticalFlip();
							m_TextureMap.Apply();
							m_TextureMap.DefineTextureMipmapped(img);
							Show(ID_VIEW_SHOWTEXTURE);
							OnRender();
							m_wUnwarpUI.LoadTexture(p);
						}
					}
				}
				break;
			case ID_UVUNWARP:
				m_wUnwarpUI.ShowWindow(SW_SHOW);
				m_wUnwarpUI.BringWindowToTop();
				m_wUnwarpUI.Render();
				break;
			case ID_VIEW_SHOWNORMAL:
			case ID_VIEW_SHOWNONE:
			case ID_VIEW_SHOWUV:
			case ID_VIEW_SHOWTANGENT:
			case ID_VIEW_SHOWTEXTURE:
				Show((UINT)wParam);
				break;
			}
		}
		UpdateShowMenu();
		break;
	}
	return __super::WndProc(message,wParam,lParam);
}

BOOL CMainWnd::OnInitWnd()
{	
	if(__super::OnInitWnd())
	{
		w32::CFileList::EnableFileDropping(*this);

		m_wUnwarpUI.Create();
		m_wUnwarpUI.InitGL();

		return TRUE;
	}

	return FALSE;
}

void CMainWnd::OnUninitScene()
{
	m_Mesh.Destroy();
	m_TextureMap.Destroy();
}

void CMainWnd::OnRender()
{
	glClear(GL_COLOR_BUFFER_BIT|GL_DEPTH_BUFFER_BIT);

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	_CheckErrorGL;

	glTranslated(0,0,-5);
	TransformScene();

	if(m_bShowBox)
		DrawUnitBox();
	SetPolygonMode(m_bWireframe,!m_bWireframe);



	//m_VisualSpaceView.glGet();

	//num::Vec2f vPos[4];
	//m_VisualSpaceView.Transform(Vec3f(0,0,0),vPos[0]);
	//m_VisualSpaceView.Transform(Vec3f(0.5,0,0),vPos[1]);
	//m_VisualSpaceView.Transform(Vec3f(0,0.5,0),vPos[2]);
	//m_VisualSpaceView.Transform(Vec3f(0,-0.5,0.5),vPos[3]);

	//std::cout << "original = " << vPos[0] << "\n";
	//std::cout << "x dir = " << vPos[1] << "\n";
	//std::cout << "y dir = " << vPos[2] << "\n";
	//std::cout << "z dir = " << vPos[3] << "\n";

	glPushMatrix();
	




	glPointSize(5);
	glBegin(GL_POINTS);
	glColor3f(1,1,1);
	glVertex3f(0,0,0);
	glColor3f(0,1,1);
	glVertex3f(0.1,0,0);
	glColor3f(1,0,1);
	glVertex3f(0,0.1,0);
	glColor3f(1,1,0);
	glVertex3f(0,0,0.1);
	glEnd();
	
	//m_transformCtrl.DrawCtrl();

	glPopMatrix();


	if(m_Mesh.IsLoaded())
	{
		glPushMatrix();
			m_transformCtrl.prepareDrag();
			m_transformCtrl.ApplyFull(GetRotationToScene());


			glScalef(m_SceneScale,m_SceneScale,m_SceneScale);
			glTranslatef(m_SceneTrans.x,m_SceneTrans.y,m_SceneTrans.z);

			if(m_TextureMap.IsTexture() && m_Mesh.IsTextureBinded() && m_bShowTexture){	
				m_TextureMap.Enable();
				m_TextureMap.Apply();
				m_TextureMap.TextureEnvMode(wglh::tx::EnvModulate);
				m_TextureMap.TextureFilterSet();
				m_TextureMap.TextureWrapMode(wglh::tx::WrapRepeat,wglh::tx::WrapRepeat);
			}

			m_Mesh.Render(m_bPointCloud?wglh::CwglMesh::RENDERING_FLAG_POINT_CLOUD:0);


			m_TextureMap.Disable();
						
		glPopMatrix();

		glPushMatrix();
			m_transformCtrl.ApplyTranslate();
			m_transformCtrl.DrawCtrl();
		glPopMatrix();

	}
	m_sprites->draw();


	if(m_IsDragging)
	{
		ScreenCoordinate_Begin();
			_CheckErrorGL;

			glEnable(GL_BLEND);
			glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
			{
				glColor4f(0.04f,0.05f,0.2f,0.5f);
				glBegin(GL_QUADS);
					glVertex2i(m_MouseStart.x,m_MouseStart.y);
					glVertex2i(m_MouseStart.x,m_MouseEnd.y);
					glVertex2i(m_MouseEnd.x,m_MouseEnd.y);
					glVertex2i(m_MouseEnd.x,m_MouseStart.y);
				glEnd();
			}
			glDisable(GL_BLEND);
			_CheckErrorGL;

			glColor3f(0.239f,0.241f,0.916f);
			glLineWidth(1);
			glBegin(GL_LINE_LOOP);
				glVertex2i(m_MouseStart.x,m_MouseStart.y);
				glVertex2i(m_MouseStart.x,m_MouseEnd.y);
				glVertex2i(m_MouseEnd.x,m_MouseEnd.y);
				glVertex2i(m_MouseEnd.x,m_MouseStart.y);
			glEnd();
			glLineWidth(1);
			_CheckErrorGL;

		ScreenCoordinate_End();
	}

	glFinish();
	SwapBuffers(wglGetCurrentDC());
	_CheckErrorGL;
}

void CMainWnd::UpdateShowMenu()
{	
	MainMenu_Enable(ID_VIEW_SHOWNORMAL, m_Mesh.IsLoaded() && m_Mesh.IsNormalBinded());
	MainMenu_Enable(ID_VIEW_SHOWTANGENT, m_Mesh.IsLoaded() && m_Mesh.IsTangentBinded());
//	MainMenu_Enable(ID_VIEW_SHOWCOLOR, m_Mesh.IsLoaded() && m_Mesh.IsColorBinded());
	MainMenu_Enable(ID_VIEW_SHOWUV, m_Mesh.IsLoaded() && m_Mesh.IsTextureBinded());
	MainMenu_Enable(ID_VIEW_SHOWTEXTURE, m_Mesh.IsLoaded() && m_Mesh.IsTextureBinded() && m_TextureMap.IsTexture() );
}

void CMainWnd::Show(UINT what)
{
	if( m_Mesh.IsLoaded() )
	{	UINT vco = m_Mesh.GetVertexCount();
		Vec4b* pClr = m_Mesh.GetBuffer_VertexColor();
		ASSERT(pClr);

		switch(what)
		{
		case ID_VIEW_SHOWNORMAL:
			{
				const Vec3f* pNormal = m_Mesh.Get_VertexNormal();
				for(UINT i=0;i<vco;i++)
				{	pClr[i].x = (int)(pNormal[i].x*127.5f) + 128;
					pClr[i].y = (int)(pNormal[i].y*127.5f) + 128;
					pClr[i].z = (int)(pNormal[i].z*127.5f) + 128;
					pClr[i].w = 0;
				}
				m_bShowTexture = FALSE;
			}
			break;
		case ID_VIEW_SHOWTANGENT:
			{
				const Vec3f* pTangent = m_Mesh.Get_VertexTangent();
				for(UINT i=0;i<vco;i++)
				{	pClr[i].x = (int)(pTangent[i].x*127.5f) + 128;
					pClr[i].y = (int)(pTangent[i].y*127.5f) + 128;
					pClr[i].z = (int)(pTangent[i].z*127.5f) + 128;
					pClr[i].w = 0;
				}
				m_bShowTexture = FALSE;
			}
			break;
		case ID_VIEW_SHOWUV:
			{
				const Vec3f* pUV = m_Mesh.Get_VertexTexCoord();
				for(UINT i=0;i<vco;i++)
				{	
					USING_EXPRESSION;
					pClr[i].v3() = 255.0f*pUV[i];
					pClr[i].w = 0;
				}
				m_bShowTexture = FALSE;
			}
			break;
		case ID_VIEW_SHOWTEXTURE:
			m_bShowTexture = TRUE;
			break;
		default:
			memset(pClr,0xff,4*vco);
			m_bShowTexture = FALSE;
		}

		m_Mesh.SubmitBuffers(TRUE);
		OnRender();
	}
}

void CMainWnd::ImportObject(LPCTSTR p)
{
	SetWindowText(w32::CFullPathName(p).GetFileTitle());

	ImportWavefrontObjFile(p,m_Mesh);

	{	Vec3d bounding_min(FLT_MAX);
		Vec3d bounding_max(-FLT_MAX);
		const Vec3f* pos = m_Mesh.Get_VertexPosition();
		for(UINT i=0;i<m_Mesh.GetVertexCount();i++)
		{
			bounding_min.Min(pos[i],bounding_min);
			bounding_max.Max(pos[i],bounding_max);
		}
		USING_EXPRESSION;
		m_SceneTrans = (bounding_min + bounding_max)*-0.5f;
		m_SceneScale = (float)(1/	max(bounding_max.x-bounding_min.x,
									max(bounding_max.y-bounding_min.y,
										bounding_max.z-bounding_min.z)));
		_CheckDump("Bounding: "<<bounding_min<<"-"<<bounding_max<<"\n");
	}

	UpdateShowMenu();
	Show(m_Mesh.IsNormalBinded()?ID_VIEW_SHOWNORMAL:0);
	SetWindowText(m_FileTitle = w32::CFullPathName(p).GetFileTitle());

	m_wUnwarpUI.SetScene(m_Mesh);
}

void CMainWnd::LoadScene(LPCTSTR p, BOOL Normalize_Size)
{
	if(Normalize_Size)
	{
		if(FAILED(m_Mesh.Load(p,wglh::CwglMesh::LOADING_FLAG_BUFFER_IN_MEMORY)))
			m_Mesh.Destroy();
		m_SceneTrans.Zero();
		m_SceneScale = 1;
	}
	else
	{
		if(FAILED(m_Mesh.Load(p,wglh::CwglMesh::LOADING_FLAG_BUFFER_IN_MEMORY|
								wglh::CwglMesh::LOADING_FLAG_KEEP_OBJECT_SIZE )))
			m_Mesh.Destroy();

		{	Vec3d bounding_min(FLT_MAX);
			Vec3d bounding_max(-FLT_MAX);
			const Vec3f* pos = m_Mesh.Get_VertexPosition();
			for(UINT i=0;i<m_Mesh.GetVertexCount();i++)
			{
				bounding_min.Min(pos[i],bounding_min);
				bounding_max.Max(pos[i],bounding_max);
			}
			USING_EXPRESSION;
			m_SceneTrans = (bounding_min+bounding_max)*-0.5f;
			m_SceneScale = (float)(1/	max(bounding_max.x-bounding_min.x,
										max(bounding_max.y-bounding_min.y,
											bounding_max.z-bounding_min.z)));
			_CheckDump("Bounding: "<<bounding_min<<"-"<<bounding_max<<"\n");
		}
	}

	UpdateShowMenu();
	Show(m_Mesh.IsNormalBinded()?ID_VIEW_SHOWNORMAL:0);
	SetWindowText(m_FileTitle = w32::CFullPathName(p).GetFileTitle());

	m_wUnwarpUI.SetScene(m_Mesh);
}

void CMainWnd::UpdateSelection(const float * pS)
{
	ASSERT_ARRAY(pS,m_Mesh.GetVertexCount());

	Apply();
	Vec4b* pClr = m_Mesh.GetBuffer_VertexColor();
	ASSERT(pClr);

	for(UINT i=0;i<m_Mesh.GetVertexCount();i++)
	{
		int d = (int)(150*pS[i]+0.5f);
		pClr[i] = Vec4b(255-d,255-d,255-d/2,1);
	}
	m_Mesh.SubmitBuffers();
	Render();
}

void CMainWnd::UpdateTextureCoord(const Vec3f* pUV)
{
	ASSERT_ARRAY(pUV,m_Mesh.GetVertexCount());

	Apply();
	memcpy(m_Mesh.GetBuffer_VertexTexCoord(),pUV,sizeof(Vec3f)*m_Mesh.GetVertexCount());
	m_Mesh.SubmitBuffers(TRUE);

	Render();
}


int APIENTRY _tWinMain(HINSTANCE hInstance,
                     HINSTANCE hPrevInstance,
                     LPTSTR    lpCmdLine,
                     int       nCmdShow)
{
	w32::Console::Create();

	w32::CCommandLine	cmdline;

	cmdline.Parse(lpCmdLine);

	if(cmdline.GetTextCount()>0)
	{
		//// try conversion
		//wglh::CwglMesh	scene;
		//ImportWavefrontObjFile(cmdline.GetText(0),scene);
		//TCHAR out_fn[MAX_PATH];

		//if(scene.IsLoaded())
		//{
		//	if(cmdline.GetTextCount()>1)
		//		_tcscpy(out_fn,cmdline.GetText(1));
		//	else
		//		_tcscpy(out_fn,cmdline.GetText(0));

		//	REPLACE_EXTNAME(out_fn,_T(".scene"));

		//	_CheckDump(_T("\nSave to ")<<out_fn<<_T(":\n"));
		//	scene.Save(out_fn);
		//}
	}
	else
	{	// display UI
		wglh::CwglApp glApp;

		CSceneWnd MainWnd;

		MainWnd.Create();
		MainWnd.SetMenu(IDC_SCENECONV);
		MainWnd.InitGL();
		MainWnd.ResizeWindowByClientArea(640,480);
		MainWnd.SetWindowText(_T("Scene File Viewer"));
		MainWnd.CenterWindow();
		MainWnd.ShowWindow();

		//_CheckErrorGL;
		//MainWnd.DestroyWindow();

		MainWnd.SetAsMainWnd();
		glApp.Run();
	}
	
	return 0;
}



