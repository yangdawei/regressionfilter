#pragma once

#include "1.0/wglh/ArcballWndEx.h"
#include "1.0/wglh/wglshader.h"
#include "1.0/wglh/wglLight.h"
#include "1.0/wglh/wglfbo.h"
#include "envirMap.h"
#include <vector>
#include <string>
using namespace std;
using namespace jgl;

class CSceneWnd : public wglh::CArcballWndEx {
public:
	EnvirMap map;
	struct BRDF {
		float kd, ks, lambda;
		num::Vec3f h;
		BRDF(float kd = 0.0, float ks = 0.0, num::Vec3f h = num::Vec3f(0.0, 0.0, 0.0), float lambda = 0.0): kd(kd), ks(ks), h(h), lambda(lambda) {}
	};
	struct SRBF {
		num::Vec3f center, color;
		float radius;
		float intensity;
	};
	struct Transformation {
		float scale;
		num::Vec3f translation;
		num::Mat4x4f rotation;
	};
	Transformation ViewTransformation, LightTransformation;
	wglh::glsl::CwglShaderCode m_fs_shader, m_vs_shader;
	wglh::glsl::CwglProgramEx m_shader_program;
	wglh::glsl::CwglShaderCodeManagment m_shader_project;
	vector<wglh::CwglMesh*> object;
	vector<wglh::CwglSpotLight> spotlight;
	num::Vec3f spotlightPos[10];
	num::Vec3f spotlightColor[10];
	num::Mat4x4f lightViewMatrix, lightProjectionMatrix, cameraViewMatrix, cameraProjectionMatrix;
	/*add here*/
	vector< vector<num::Vec3f> > triangle_list;
	wglh::CwglTexture2DBase<GL_TEXTURE_2D, GL_RGBA32F_ARB, GL_RGBA, GL_FLOAT> m_shTex;
	
	vector<string> object_name;
	vector<float> object_scale;
	vector<num::Vec3f> object_translation, object_color;
	vector<num::Mat4x4f> object_rotation;
	vector<BRDF> object_brdf;
	vector<SRBF> light_srbf;
	int current_object_id;
	int current_light_id;
	JGLCombo *m_combo;
	JGLButton *m_button_add_object, *m_button_remove_object, *m_button_rename_object;
	JGLButton *m_button_save_scene, *m_button_load_scene;
	JGLSeekBar *m_seek_brdf_kd, *m_seek_brdf_ks, *m_seek_brdf_hx, *m_seek_brdf_hy, *m_seek_brdf_hz, *m_seek_brdf_lambda;
	float m_kd, m_ks, m_hx, m_hy, m_hz, m_lambda;
	wglh::ext::CFrameBufferObject m_FBO;
	GLuint m_texID;
	num::Mat4x4f lightFaceMatrix[6];
	/** Spot light control */
public:
	CSceneWnd();
	virtual void OnInitScene();
	virtual void OnRender();
	virtual void OnRenderObject();
	
	/**
	 * Render the light source as a small luminous ball.
	 * @author Dawei Yang
	 * @since 2013-9-24
	 */
	void OnRenderLight();
	bool handleKeyboard(unsigned char c);
	void loadConfigFile(const char *fileName);
	void saveConfigFile(const char *fileName);
	num::Vec3f getLightDirection(int lightIndex);
	num::Vec3f getViewPosition();
	/*add*/
	void getTriangleList( const char* filename, vector<num::Vec3f>& triangle );
	void CreateTexture();
protected:
	void OnSaveConfigFile();
	void OnLoadConfigFile();
	void OnAddObject();
	void OnRemoveObject();
	void OnSelectionChange(int selection);
	/**
	 * @author Dawei Yang
	 * @since 2013-9-24
	 */
	void OnLightChange(int selection);
	void OnUpdateBRDF();

	/**
	 * Add light source .
	 * @author Dawei Yang
	 * @since 2013-9-24
	 */
	void OnAddLight();

	/**
	 * @author Dawei Yang
	 * @since 2013-9-24
	 */
	void OnRemoveLight();
private:
	string ShowFileDialog(LPSTR filter);
	void AddObject(string fileName);
	void stringToArray(string &str, float *array, int count);
	void stringToVec3f(string &str, num::Vec3f &vec);
	void stringToMat4x4f(string &str, num::Mat4x4f &mat);
	string arrayToString(float *array, int count);
	void AddLight( string fileName );
	void initShadowMap();
	void renderWithShadow();
	void setupMatrices(unsigned index);
	void renderTest();
	void renderObjectPrimitive();
	void renderDepthCubemap();
	void RenderScene();
	void setupLight(unsigned index);
	void renderWithoutShadow();
};
