#pragma once

#include "1.0\wglh\wglMesh.h"

void ExportWavefrontObjFile(LPCTSTR fn, const wglh::CwglMesh& mesh);
void ImportWavefrontObjFile(LPCTSTR fn, wglh::CwglMesh& mesh);
void SampleTexCoord(const wglh::CwglMesh& SrcMesh, wglh::CwglMesh& DstMesh);
