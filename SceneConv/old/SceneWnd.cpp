#include "stdafx.h"
#include "SceneWnd.h"
#include "mesh_proc.h"
#include "old_obj\GeoObject.h"
#include <boost/function.hpp>
#include <boost/bind.hpp>
#include <GL/glut.h>
#include <atlimage.h>
//#include "wglArcBall.h"

#include "XMLDoc.h"
#include <sstream>

using namespace std;
using namespace tixml;
//using namespace SMM;

CSceneWnd::CSceneWnd() : CArcballWndEx(), m_fs_shader(".//shader//demoShader2.cxx"), m_vs_shader(".//shader//demoShader2.hxx") //, m_shader_project(NULL)
{
	m_kd = 0.0;
	m_ks = 0.0;
	m_h_theta = 0.0;
	m_h_phi = 0.0;
	m_lambda = 0.0;
}

string CSceneWnd::ShowFileDialog(LPSTR filter)
{
	char fileName[100];
	OPENFILENAME  ofn;
	ZeroMemory(&ofn, sizeof(ofn));
	ofn.lStructSize = sizeof(ofn);
	ofn.hwndOwner = NULL;
	ofn.lpstrFile = fileName;
	ofn.lpstrFile[0] = '\0';
	ofn.nMaxFile = sizeof(fileName);
	ofn.lpstrFilter = filter;
	ofn.nFilterIndex = 0;
	ofn.lpstrFileTitle = NULL;
	ofn.nMaxFileTitle = 0;
	ofn.lpstrInitialDir = NULL;
	ofn.Flags = OFN_PATHMUSTEXIST | OFN_FILEMUSTEXIST;
	if (GetOpenFileName(&ofn))
		return string(fileName);
	return "";
}

void CSceneWnd::AddObject(string fileName)
{
	object.push_back(new wglh::CwglMesh);
	ImportWavefrontObjFile(fileName.c_str(), *object.back());
	vector<num::Vec3f> vec;
	triangle_list.push_back( vec ); 
	getTriangleList( fileName.c_str(), triangle_list[triangle_list.size()-1] );
	object_name.push_back("OBJECT");
	float scale = 1.0;
	object_scale.push_back(scale);
	num::Vec3f translation(0.0, 0.0, 0.0);
	object_translation.push_back(translation);
	num::Mat4x4f rotation;
	rotation.SetIdentity();
	object_rotation.push_back(rotation);
	object_brdf.push_back(BRDF(1.0, 1.0, 0.0, 0.0, 1.0));
	num::Vec3f color(1.0, 0.0, 0.0);
	object_color.push_back(color);
	m_combo->setData(object_name);
}

void CSceneWnd::OnAddObject()
{
	string fileName = ShowFileDialog("WaveFront Object File(*.obj)\0*.obj\0");
	if (fileName.length() != 0)
		AddObject(fileName);
}

void CSceneWnd::OnRemoveObject()
{
	object.erase(object.begin() + current_object_id);
	object_name.erase(object_name.begin() + current_object_id);
	object_scale.erase(object_scale.begin() + current_object_id);
	object_translation.erase(object_translation.begin() + current_object_id);
	object_rotation.erase(object_rotation.begin() + current_object_id);
	object_brdf.erase(object_brdf.begin() + current_object_id);
	object_color.erase(object_color.begin() + current_object_id);
	current_object_id = 0;
	m_combo->setData(object_name);
}

void CSceneWnd::OnSaveConfigFile()
{
	string fileName = ShowFileDialog("Scene Config File\0*.xml\0");
	if (fileName.length() != 0)
	{
		printf("Save: %s\n", fileName.c_str());
	}
}

void CSceneWnd::OnLoadConfigFile()
{
	string fileName = ShowFileDialog("Scene Config File\0*.xml\0");
	if (fileName.length() != 0)
	{
		printf("Loading Scene Config File: %s\n", fileName.c_str());
		loadConfigFile(fileName.c_str());
	}
}

void CSceneWnd::OnSelectionChange(int selection)
{
	current_object_id = selection;
	m_transformCtrl.SetScale(object_scale[current_object_id]);
	m_transformCtrl.GetTranslation_Ref() = object_translation[current_object_id];
	m_transformCtrl.SetRotationMat(GetRotationToScene(), object_rotation[current_object_id]);
	m_kd = object_brdf[current_object_id].kd;
	m_ks = object_brdf[current_object_id].ks;
	m_h_theta = object_brdf[current_object_id].h_theta;
	m_h_phi = object_brdf[current_object_id].h_phi;
	m_lambda = object_brdf[current_object_id].lambda;
	m_seek_brdf_kd->setValue(m_kd);
	m_seek_brdf_ks->setValue(m_ks);
	m_seek_brdf_h_theta->setValue(m_h_theta);
	m_seek_brdf_h_phi->setValue(m_h_phi);
	m_seek_brdf_lambda->setValue(m_lambda);
}

void CSceneWnd::OnRender()
{
	preRender();

	

	glClearColor(0.2f, 0.4f, 0.6f, 1.0f);
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	num::Mat4x4f lightTransformMatrix, viewTransformMatrix;
	//render environment
	{
		OnRenderEnvironment();
		//num::Mat4x4f m_cubeMapEnv.
		lightTransformMatrix = m_SceneArcBall_Env.GetMatrix();
	}

	glEnable(GL_DEPTH_TEST);

	{
		m_shTex.Enable();
		wglh::ext::CwglMultiTextureEnv::ActiveTextureUnit(0);
		m_shTex.Apply();
		m_shader_program.Apply();
		glMatrixMode(GL_MODELVIEW);
		glLoadIdentity();
		glTranslatef(0,0,-m_SceneZoom);
		m_SceneArcBall.Apply();

		float lightOriginalDirection[3] = {0.0, 0.0, m_SceneZoom};//{-1.0, 1.0, 0.0};
		float viewOriginalPosition[3] = {0.0, 0.0, m_SceneZoom};
		viewTransformMatrix = m_SceneArcBall.GetMatrix();
		m_shader_program.SetUniform("shTexture", 0);
		m_shader_program.SetUniformArray3f("lightOriginalDirection", lightOriginalDirection, 1);
		m_shader_program.SetUniformArray3f("viewOriginalPosition", viewOriginalPosition, 1);
		m_shader_program.SetUniformMatrix_4x4("lightTransformMatrix", lightTransformMatrix);
		m_shader_program.SetUniformMatrix_4x4("viewTransformMatrix", viewTransformMatrix);

		OnRenderObject();
		m_shader_program.DisableShaders();
		wglh::ext::CwglMultiTextureEnv::ActiveTextureUnit(0);
		m_shTex.Disable();

	}

	glFinish();

	//draw sprites
	if(m_sprites){
		m_sprites->draw();
	}

	SwapBuffers(wglGetCurrentDC());
}
float lambda = 500.0f;
float s = 170.0f;
void CSceneWnd::OnRenderObject()
{
	for (int i = 0; i < object.size(); i++)
	{
		//int tri_num = ( object[i]->GetMeshElementIndicCount() ) / 3;
		glPushMatrix();
		if (current_object_id == i)
		{
			m_transformCtrl.prepareDrag();
			m_transformCtrl.ApplyFull(GetRotationToScene());
			object_scale[i] = m_transformCtrl.GetScale();
			object_translation[i] = m_transformCtrl.GetTranslation_Ref();
			object_rotation[i] = m_transformCtrl.GetRotationMatrix(GetRotationToScene());
		} 
		else
		{
			glTranslatef(object_translation[i][0], object_translation[i][1], object_translation[i][2]);
			glScalef(object_scale[i], object_scale[i], object_scale[i]);
			glMultMatrixf(object_rotation[i]);
		}
		
		//m_shader_program.Apply();
		m_shader_program.SetUniformArray3f("pointColor", &object_color[i], 1);	
		m_shader_program.SetUniformMatrix_4x4("objectRotationMatrix", object_rotation[i]);
		m_shader_program.SetUniform("lambda", lambda);
		m_shader_program.SetUniform("s", s);
		//m_shader_program.SetUniform("tri_num", tri_num);
		float self_brdfxyz[6];
		self_brdfxyz[0] = object_brdf[i].kd;
		self_brdfxyz[1] = object_brdf[i].ks;
		self_brdfxyz[2] = object_brdf[i].lambda;
		self_brdfxyz[3] = cos( object_brdf[i].h_phi );
		self_brdfxyz[4] = sin( object_brdf[i].h_phi );
		self_brdfxyz[5] = cos( object_brdf[i].h_theta );
		m_shader_program.SetUniformArray3f("self_brdf", self_brdfxyz , 2);
		int other, start;
		/*for( int j = 0; j < object.size(); j++ ) {
			if( j != i ) {
				other = j;
				break;
			}
		}*/
		if( i == 0 ) {
			other = 1;
			start = triangle_list[0].size() / 3;
		}
		else {
			other = 0;
			start = 0;
		}
		int tri_num = ( object[other]->GetMeshElementIndicCount() ) / 3;
		float other_brdfxyz[6];
		other_brdfxyz[0] = object_brdf[other].kd;
		other_brdfxyz[1] = object_brdf[other].ks;
		other_brdfxyz[2] = object_brdf[other].lambda;
		other_brdfxyz[3] = cos( object_brdf[other].h_phi );
		other_brdfxyz[4] = sin( object_brdf[other].h_phi );
		other_brdfxyz[5] = cos( object_brdf[other].h_theta );
		m_shader_program.SetUniformArray3f("brdfs", other_brdfxyz , 2);
		m_shader_program.SetUniform("tri_num", tri_num);
		//m_shader_program.SetUniformArray3f("triangles", &triangle_list[other][0] , tri_num*3);
		m_shader_program.SetUniform("resX", 2048);
		m_shader_program.SetUniform("resY", 3072);
		m_shader_program.SetUniform("startIndex", start);
		

		object[i]->Render(0);
		glPopMatrix();
	}

	if(m_InteractiveMode == 0){
		glPushMatrix();
		m_transformCtrl.ApplyTranslate();
		m_transformCtrl.DrawCtrl();
		glPopMatrix();
	}
}

void CSceneWnd::OnUpdateBRDF()
{
	object_brdf[current_object_id].kd = m_seek_brdf_kd->getValue();
	object_brdf[current_object_id].ks = m_seek_brdf_ks->getValue();
	object_brdf[current_object_id].h_theta = m_seek_brdf_h_theta->getValue();
	object_brdf[current_object_id].h_phi = m_seek_brdf_h_phi->getValue();
	object_brdf[current_object_id].lambda = m_seek_brdf_lambda->getValue();
}

void CSceneWnd::OnInitScene()
{
	m_SceneZoom = 5.0f;
	m_cubeMapEnv.Load_PFM("grace.pfm");
	m_cubeMapEnv.InitForRendering();
	_SafeDel(m_sprites);
	using namespace jgl;
	JSpriteContainer::setResourcePath(".\\raw\\");
	m_sprites = new JSpriteContainer(10,10,250,200,true);

	m_seek_brdf_kd = new JGLSeekBar(&m_kd, 0, 100, 90, 120, "Kd:");
	m_seek_brdf_kd->addListener(boost::bind(&CSceneWnd::OnUpdateBRDF,this));
	m_sprites->addSprite(m_seek_brdf_kd);

	m_seek_brdf_ks = new JGLSeekBar(&m_ks, 0, 100, 90, 100, "Ks:");
	m_seek_brdf_ks->addListener(boost::bind(&CSceneWnd::OnUpdateBRDF,this));
	m_sprites->addSprite(m_seek_brdf_ks);

	m_seek_brdf_h_theta = new JGLSeekBar(&m_h_theta, 0, 100, 90, 80, "H_theta:");
	m_seek_brdf_h_theta->addListener(boost::bind(&CSceneWnd::OnUpdateBRDF,this));
	m_sprites->addSprite(m_seek_brdf_h_theta);

	m_seek_brdf_h_phi = new JGLSeekBar(&m_h_phi, 0, 100, 90, 60, "H_phi:");
	m_seek_brdf_h_phi->addListener(boost::bind(&CSceneWnd::OnUpdateBRDF,this));
	m_sprites->addSprite(m_seek_brdf_h_phi);

	m_seek_brdf_lambda = new JGLSeekBar(&m_lambda, 0, 100, 90, 40, "Lambda:");
	m_seek_brdf_lambda->addListener(boost::bind(&CSceneWnd::OnUpdateBRDF,this));
	m_sprites->addSprite(m_seek_brdf_lambda);

	m_button_save_scene = new JGLButton(10, 10, 50, 20, "SAVE");
	m_button_save_scene->addClickListener(boost::bind(&CSceneWnd::OnSaveConfigFile, this));
	m_sprites->addSprite(m_button_save_scene);
	m_button_load_scene = new JGLButton(80, 10, 50, 20, "LOAD");
	m_button_load_scene->addClickListener(boost::bind(&CSceneWnd::OnLoadConfigFile, this));
	m_sprites->addSprite(m_button_load_scene);

	m_button_add_object = new JGLButton(130, 170, 20, 20, "+");
	m_button_add_object->addClickListener(boost::bind(&CSceneWnd::OnAddObject, this));
	m_sprites->addSprite(m_button_add_object);
	m_button_remove_object = new JGLButton(170, 170, 20, 20, "-");
	m_button_remove_object->addClickListener(boost::bind(&CSceneWnd::OnRemoveObject, this));
	m_sprites->addSprite(m_button_remove_object);

	m_sprites->addSprite(new JGLLabel(10, 150, "BRDF Parameters:"));

	m_combo = new JGLCombo(10, 170, 100, 20, object_name);
	m_combo->addSelectionChangedListener(boost::bind(&CSceneWnd::OnSelectionChange, this, _1));
	m_sprites->addSprite(m_combo);

	m_shader_program = m_fs_shader + m_vs_shader;
	m_shader_project = m_shader_program;
	m_shader_project.UpdateShaderList();

	loadConfigFile("DefaultSceneConfig.xml");
	CreateTexture();
}

void CSceneWnd::stringToArray(string &str, float *array, int count)
{
	stringstream ss(str);
	char temp;
	for (int i = 0; i < count; i++)
	{
		ss >> temp;
		ss >> array[i];
	}
}

void CSceneWnd::stringToVec3f(string &str, num::Vec3f &vec)
{
	stringstream ss(str);
	char temp;
	for (int i = 0; i < 3; i++)
	{
		ss >> temp;
		ss >> vec[i];
	}
}

void CSceneWnd::stringToMat4x4f(string &str, num::Mat4x4f &mat)
{
	stringToArray(str, &mat.m[0][0], 16);
}

string CSceneWnd::arrayToString(float *array, int count)
{
	stringstream ss;
	ss << '(' << array[0];
	for (int i = 1; i < count; i++)
	{
		ss << "," << array[i];
		if (i == count - 1)
			ss << ')';
	}
	return ss.str();
}

void CSceneWnd::loadConfigFile(const char *fileName)
{
	object.clear();
	object_name.clear();
	object_brdf.clear();
	object_color.clear();
	object_rotation.clear();
	object_scale.clear();
	object_translation.clear();
	tixml::XMLDoc doc;
	doc.Load(fileName);

	string obj_name, obj_filename;
	int obj_num = doc.get<int>("ObjectList.@Count", 0);
	float obj_scale;
	num::Vec3f obj_translation, obj_color;
	num::Mat4x4f obj_rotation;

	for (int i = 0; i < obj_num; i++)
	{
		// Object Name
		obj_name = doc.get<string>("ObjectList.Object.@Name", i);
		object_name.push_back(obj_name.c_str());
		// File Name
		obj_filename = doc.get<string>("ObjectList.Object.SourceFile.@Name", i);
		object.push_back(new wglh::CwglMesh);
		ImportWavefrontObjFile(obj_filename.c_str(), *object.back());
		vector<num::Vec3f> vec;
		triangle_list.push_back( vec ); 
		getTriangleList( obj_filename.c_str(), triangle_list[triangle_list.size()-1] );
		// Scale
		obj_scale = doc.get<float>("ObjectList.Object.Transformation.@Scale", i);
		object_scale.push_back(obj_scale);
		// Translation
		stringToVec3f(doc.get<string>("ObjectList.Object.Transformation.@Translation", i), obj_translation);
		object_translation.push_back(obj_translation);
		// Rotation
		stringToMat4x4f(doc.get<string>("ObjectList.Object.Transformation.@Rotation", i), obj_rotation);
		object_rotation.push_back(obj_rotation);
		// Color
		stringToVec3f(doc.get<string>("ObjectList.Object.Color.@Value", i), obj_color);
		object_color.push_back(obj_color);
		// BRDF
		m_kd = doc.get<float>("ObjectList.Object.BRDF.@Kd", i);
		m_ks = doc.get<float>("ObjectList.Object.BRDF.@Ks", i);
		m_h_theta = doc.get<float>("ObjectList.Object.BRDF.@HTheta", i);
		m_h_phi = doc.get<float>("ObjectList.Object.BRDF.@HPhi", i);
		m_lambda = doc.get<float>("ObjectList.Object.BRDF.@Lambda", i);
		object_brdf.push_back(BRDF(m_kd, m_ks, m_h_theta, m_h_phi, m_lambda));
	}
	m_combo->setData(object_name);
	OnSelectionChange(0);
	
}

void CSceneWnd::saveConfigFile(const char *fileName)
{
	tixml::XMLDoc doc;
	doc.Create("SceneConfig");

	tixml::XMLNode * pNode1 = new tixml::XMLNode("GlobalSetting","");
	tixml::XMLNode * pNode1Child1 = new tixml::XMLNode("ViewTransformation","");
	tixml::XMLNode * pNode1Child2 = new tixml::XMLNode("LightTransformation","");
	tixml::XMLNode * pNode1Child3 = new tixml::XMLNode("CubeMap","");
	pNode1Child1->setAttrib<float>("Scale", 1.0);
	pNode1Child1->setAttrib<string>("Translation", "(xxx)");
	pNode1Child1->setAttrib<string>("Rotation", "(XXXX)");
	pNode1->children().push_back(pNode1Child1);
	pNode1->children().push_back(pNode1Child2);
	pNode1->children().push_back(pNode1Child3);

	tixml::XMLNode * pNode2 = new tixml::XMLNode("ObjectList","");


	tixml::XMLNode * pNode3 = new tixml::XMLNode("LightList","");

	doc.getRoot()->children().push_back(pNode1);
	doc.getRoot()->children().push_back(pNode2);
	doc.getRoot()->children().push_back(pNode3);

	doc.Save(fileName);
}


void CSceneWnd::getTriangleList( const char* filename, vector<num::Vec3f>& triangle_list ) {
	GeoObj::CGeoObject obj;
	if(obj.LoadOBJModel(CString(filename))) {
		for( int j = 0; j < obj.m_nTriangles; j++ ) {
			//cout << obj.m_pTriangles[j].m_nVertexIndices[0] << endl;
			num::Vec3f v1;
			v1.CopyFrom((float*)&obj.m_pVertices[(obj.m_pTriangles[j].m_nVertexIndices[0])].m_vPosition );
			num::Vec3f v2;
			v2.CopyFrom((float*)&obj.m_pVertices[(obj.m_pTriangles[j].m_nVertexIndices[1])].m_vPosition );
			num::Vec3f v3;
			v3.CopyFrom((float*)&obj.m_pVertices[(obj.m_pTriangles[j].m_nVertexIndices[2])].m_vPosition );
			triangle_list.push_back( v1 );
			triangle_list.push_back( v2 );
			triangle_list.push_back( v3 );
		}
	}
}

void CSceneWnd::CreateTexture() {
	int m_texResX = 2048;
	int m_texResY = 3072;
	int m_texResVertexY = 3072/3;
	int m_texResVertexX = 2048;

	//SMM::Buffer<num::Vec4f> pData;
	rt::Buffer<num::Vec4f> pData;
	pData.SetSize( m_texResX*m_texResY );
	//num::Vec4f v = num::Vec4f( 0, 0, 0, 0 );
	//pData.SetItem( v );
	
	//triangle_list
	/*for( int i = 0; i < m_texResVertexY; i++ ) {
		for( int j = 0; j < m_texResVertexX; j++ ) {
			int iVertex = i * m_texResVertexX + j;
			for( int s = 0; s < object.size(); s++ ) {
				for( int k = 0; k < triangle_list[s].size(); k++ ) {
					pData.At(i) = num::Vec4f( triangle_list[s][k].x, triangle_list[s][k].y, triangle_list[s][k].z, 0 );
				}
			}
		}
	}*/
	int i = 0;
	for( int s = 0; s < object.size(); s++ ) {
		for( int k = 0; k < triangle_list[s].size(); k+=3 ) {
			pData.At(i) = num::Vec4f( triangle_list[s][k].x, triangle_list[s][k].y, triangle_list[s][k].z, 0 );
			pData.At( i + m_texResX) = num::Vec4f( triangle_list[s][k+1].x, triangle_list[s][k+1].y, triangle_list[s][k+1].z, 0 );
			pData.At( i + 2*m_texResX) = num::Vec4f( triangle_list[s][k+2].x, triangle_list[s][k+2].y, triangle_list[s][k+2].z, 0 );
			i++;
			if( i % ( m_texResX ) == 0 )
				i += ( 2 * m_texResX );
		}
	}
	m_shTex.TextureFilterSet( wglh::tx::FilterNearest, wglh::tx::FilterNearest );
	m_shTex.DefineTexture( m_texResX, m_texResY, pData.Begin() );

	m_shTex.Enable();
	m_shTex.Apply();
	m_shTex.Disable();
}