#pragma once

#include "1.0/wglh/ArcballWndEx.h"
#include "1.0/wglh/wglshader.h"
#include <vector>
#include <string>
using namespace std;
using namespace jgl;

class CSceneWnd : public wglh::CArcballWndEx {
public:
	struct BRDF {
		float kd, ks, h_theta, h_phi, lambda;
		BRDF(float kd = 0.0, float ks = 0.0, float h_theta = 0.0, float h_phi = 0.0, float lambda = 0.0): kd(kd), ks(ks), h_theta(h_theta), h_phi(h_phi), lambda(lambda) {}
	};
	wglh::glsl::CwglShaderCode m_fs_shader, m_vs_shader;
	wglh::glsl::CwglProgramEx m_shader_program;
	wglh::glsl::CwglShaderCodeManagment m_shader_project;
	vector<wglh::CwglMesh*> object;
	vector< vector<num::Vec3f> > triangle_list;
	wglh::CwglTexture2DBase<GL_TEXTURE_2D, GL_RGBA32F_ARB, GL_RGBA, GL_FLOAT> m_shTex;
	vector<string> object_name;
	vector<float> object_scale;
	vector<num::Vec3f> object_translation, object_color;
	vector<num::Mat4x4f> object_rotation;
	vector<BRDF> object_brdf;
	int current_object_id;
	JGLCombo *m_combo;
	JGLButton *m_button_add_object, *m_button_remove_object;
	JGLButton *m_button_save_scene, *m_button_load_scene;
	JGLSeekBar *m_seek_brdf_kd, *m_seek_brdf_ks, *m_seek_brdf_h_theta, *m_seek_brdf_h_phi, *m_seek_brdf_lambda;
	float m_kd, m_ks, m_h_theta, m_h_phi, m_lambda;
public:
	CSceneWnd();
	virtual void OnInitScene();
	virtual void OnRender();
	virtual void OnRenderObject();
	void loadConfigFile(const char *fileName);
	void saveConfigFile(const char *fileName);
	void getTriangleList( const char* filename, vector<num::Vec3f>& triangle );
	void CreateTexture();
protected:
	void OnSaveConfigFile();
	void OnLoadConfigFile();
	void OnAddObject();
	void OnRemoveObject();
	void OnSelectionChange(int selection);
	void OnUpdateBRDF();
private:
	string ShowFileDialog(LPSTR filter);
	void AddObject(string fileName);
	void stringToArray(string &str, float *array, int count);
	void stringToVec3f(string &str, num::Vec3f &vec);
	void stringToMat4x4f(string &str, num::Mat4x4f &mat);
	string arrayToString(float *array, int count);
};
