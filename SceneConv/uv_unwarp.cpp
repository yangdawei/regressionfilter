#include "StdAfx.h"
#include "uv_unwarp.h"
#include "sceneconv.h"
#include <deque>

using namespace num;
using namespace std;

CUnwarpWnd::CUnwarpWnd(void)
{
	m_UVAtlas.EnableAutoUpdateElementId();
	m_bSelected = 0;
	m_SelectCount = 0;
	m_WheelState = 0;
	m_BoxSelecting = FALSE;
}

CUnwarpWnd::~CUnwarpWnd(void)
{
}


void CUnwarpWnd::SetScene(wglh::CwglMesh& mesh)
{
	Apply();
	m_UVAtlas.Destroy();

	if(mesh.GetVertexCount() && mesh.GetMeshElementIndicCount() && mesh.IsTextureBinded())
	{
		m_bSelected.SetSize(mesh.GetVertexCount());
		m_bSelected = 0;
		m_SelectCount = 0;
		m_Graph.SetSize(mesh.GetVertexCount());

		m_UVAtlas.SetMeshRenderMode(mesh.GetMeshRenderMode());
		m_UVAtlas.SetSize_Vertex(mesh.GetVertexCount());
		m_UVAtlas.SetSize_MeshElementIndic(mesh.GetMeshElementIndicCount());

		const Vec3f* pUV = mesh.Get_VertexTexCoord();
		Vec2f bounding[2];
		bounding[0] = bounding[1] = pUV[0].v2();

		Vec3f* pPos = m_UVAtlas.GetBuffer_VertexPosition();
		for(UINT i=0;i<mesh.GetVertexCount();i++)
		{
			bounding[0].Min(bounding[0],pUV[i].v2());
			bounding[1].Max(bounding[1],pUV[i].v2());

			pPos[i].v2() = pUV[i].v2();
			pPos[i].z = 0;
		}

		m_3DPostion.SetSize(mesh.GetVertexCount());
		m_3DPostion.CopyFrom(mesh.Get_VertexPosition());
	
		const UINT* pEle = mesh.Get_MeshElementIndic();
		memcpy(	m_UVAtlas.GetBuffer_MeshElementIndic(),pEle,
				sizeof(UINT)*mesh.GetMeshElementIndicCount());
		m_UVAtlas.SubmitBuffers(TRUE);

		m_Translate.x = - (bounding[0].x + bounding[1].x)/2;// - 0.5f;
		m_Translate.y = - (bounding[0].y + bounding[1].y)/2;// - 0.5f;
		m_Translate.z = -5;
		m_Scale = std::min(width,height) / (float)(std::max(bounding[1].x - bounding[0].x,bounding[1].y - bounding[0].y) );
		if(!num::IsNumberOk(m_Scale))m_Scale = 1;

		double UVScaleTotal = 0;
		double PosScale = 0;
		const Vec3f* pos = mesh.Get_VertexPosition();
		const Vec3f* uv = mesh.Get_VertexTexCoord();
		// build graph
		{	UINT vpe = m_UVAtlas.GetVPE();
			UINT i=0;
			w32::CTimeMeasureDisplay<> tm(&i,mesh.GetMeshElementIndicCount()/vpe);
			for(;i<mesh.GetMeshElementIndicCount()/vpe;i++)
			{
				const UINT* p = &pEle[i*vpe];
				int h = p[vpe-1];
				for(UINT j=0;j<vpe;j++)
				{
					for(UINT k=0;k<m_Graph[h].GetSize();k++)
						if(m_Graph[h][k] == p[j])goto NEXT_EDGE;
					m_Graph[h].push_back(p[j]);
					m_Graph[p[j]].push_back(h);

					UVScaleTotal += sqrt(uv[h].v2().Distance_Sqr(uv[p[j]].v2()));
					PosScale += sqrt(pos[h].Distance_Sqr(pos[p[j]]));

NEXT_EDGE:
					h = p[j];
				}
			}
			m_UVScaleTotal = (float)(UVScaleTotal/PosScale);
		}
		_CheckDump("Averaging UV scale: "<<m_UVScaleTotal<<"\n");

		Render();
	}
}

void CUnwarpWnd::OnInitScene()
{
	glClearColor(0.3f,0.3f,0.4f,1);
	glColor3f(0.9f,0.9f,0.9f);
	glDisable(GL_DEPTH_TEST);
	SetPolygonMode(TRUE,FALSE);
}

BOOL CUnwarpWnd::OnInitWnd()
{
	if(__super::OnInitWnd())
	{
		SetMenu(IDR_UVWARP);
		SetWindowText(_T("UV Warp"));

		m_WheelCursor[0] = ::LoadCursor(NULL,IDC_ARROW);
		m_WheelCursor[1] = ::LoadCursor(NULL,IDC_SIZEALL);
		m_WheelCursor[2] = ::LoadCursor(NULL,IDC_SIZEWE);
		m_WheelCursor[3] = ::LoadCursor(NULL,IDC_SIZENS);

		return TRUE;
	}
	else return FALSE;
}

void CUnwarpWnd::LoadTexture(LPCTSTR p)
{
	Apply();
	wglh::CwglImage	img;
	if(img.Load(p))
	{	
		img.VerticalFlip();
		m_TextureMap.Apply();
		m_TextureMap.DefineTextureMipmapped(img);
		Render();
	}
}

void CUnwarpWnd::OnRender()
{
	glClear(GL_COLOR_BUFFER_BIT);

	ScreenCoordinate_Begin();
		if(m_TextureMap.IsTexture())
		{	m_TextureMap.Enable();
			m_TextureMap.Apply();
			m_TextureMap.TextureEnvMode(wglh::tx::EnvModulate);
			m_TextureMap.TextureFilterSet();
			m_TextureMap.TextureWrapMode(wglh::tx::WrapRepeat,wglh::tx::WrapRepeat);
		}
		else m_TextureMap.Disable();

		SetPolygonMode(FALSE,FALSE,TRUE);
			
		glColor3f(0.3f,0.3f,0.4f);
		glBegin(GL_QUADS);
			glTexCoord2f(-width/2/m_Scale - m_Translate.x, -height/2/m_Scale - m_Translate.y);
			glVertex2f(0,0);

			glTexCoord2f( width/2/m_Scale - m_Translate.x, -height/2/m_Scale - m_Translate.y);
			glVertex2f((float)width,0);

			glTexCoord2f( width/2/m_Scale - m_Translate.x,  height/2/m_Scale - m_Translate.y);
			glVertex2f((float)width,(float)height);

			glTexCoord2f(-width/2/m_Scale - m_Translate.x,  height/2/m_Scale - m_Translate.y);
			glVertex2f(0,(float)height);
		glEnd();

		m_TextureMap.Disable();
		
		glPushMatrix();
			glTranslatef(width/2.0f,height/2.0f,0);
			glScalef(m_Scale,m_Scale,1);
			glTranslatef(m_Translate.x,m_Translate.y,m_Translate.z);

			glColor3f(0.7f,0.7f,0.9f);
			SetPolygonMode(TRUE,FALSE);
			glLineWidth(3);
			glBegin(GL_QUADS);
				glVertex2f(0,0);
				glVertex2f(1,0);
				glVertex2f(1,1);
				glVertex2f(0,1);
			glEnd();

			glLineWidth(1);
			if(m_UVAtlas.IsLoaded())m_UVAtlas.Render();
		glPopMatrix();

		if(m_BoxSelecting)
		{
			glColor3f(1,1,1);
			glLineWidth(1);
			glBegin(GL_QUADS);
				glVertex2f((GLfloat)m_Box.x.x,(GLfloat)m_Box.x.y);
				glVertex2f((GLfloat)m_Box.y.x,(GLfloat)m_Box.x.y);
				glVertex2f((GLfloat)m_Box.y.x,(GLfloat)m_Box.y.y);
				glVertex2f((GLfloat)m_Box.x.x,(GLfloat)m_Box.y.y);
			glEnd();
		}

	ScreenCoordinate_End();

	glFinish();
	SwapBuffers(wglGetCurrentDC());
	_CheckErrorGL;
}

LRESULT CUnwarpWnd::WndProc(UINT message, WPARAM wParam, LPARAM lParam)
{
	Apply();

	static int mouse_x=0,mouse_y=0;
	static int lmouse_x=0,lmouse_y=0;
	float fine = (wParam & MK_CONTROL)?0.1f:1.0f;

	switch(message)
	{
	case WM_CLOSE:
		ShowWindow(SW_HIDE);
		return 0;
		break;
	case WM_LBUTTONDOWN:
		lmouse_x = GET_X_LPARAM(lParam);
		lmouse_y = GET_Y_LPARAM(lParam);
		Apply();
		ScreenCoordinate_Begin();
			glTranslatef(width/2.0f,height/2.0f,0);
			glScalef(m_Scale,m_Scale,1);
			glTranslatef(m_Translate.x,m_Translate.y,m_Translate.z);

			m_BoxSelecting = !SetSelection(lmouse_x,lmouse_y,wParam&MK_CONTROL);
			m_BoxSelecting = FALSE;  // box select has bug
			m_Box.x.x = lmouse_x;
			m_Box.x.y = lmouse_y;
			m_Box.y = m_Box.x;
		Apply();
		ScreenCoordinate_End();
		SetCapture();
		Render();
		break;
	case WM_RBUTTONDOWN:
		mouse_x = GET_X_LPARAM(lParam);
		mouse_y = GET_Y_LPARAM(lParam);
		SetCapture();
		break;
	case WM_MOUSEMOVE:
		{
			int x = GET_X_LPARAM(lParam);
			int y = GET_Y_LPARAM(lParam);
			if(wParam & MK_RBUTTON)
			{
				m_Translate.x += fine*(x - mouse_x)/m_Scale;
				m_Translate.y += fine*(y - mouse_y)/m_Scale;
				mouse_x = x;
				mouse_y = y;
				Render();
			}

			if(wParam & MK_LBUTTON)
			{
				if(m_BoxSelecting)
				{
					m_Box.y.x = x;
					m_Box.y.y = y;
					Render();
				}
				else if(m_SelectCount && (wParam & MK_CONTROL)==0)
				{
					MoveSelection((x-lmouse_x)/m_Scale,(y-lmouse_y)/m_Scale);
					lmouse_x = x;
					lmouse_y = y;
					Render();
				}
			}
		}
		break;
	case WM_LBUTTONUP:
		{
			ReleaseCapture();
			m_BoxSelecting = FALSE;
			rt::BufferEx<UINT>	selection;

			ScreenCoordinate_Begin();
				glTranslatef(width/2.0f,height/2.0f,0);
				glScalef(m_Scale,m_Scale,1);
				glTranslatef(m_Translate.x,m_Translate.y,m_Translate.z);

				m_UVAtlas.PickElement(m_Box.x.x,	m_Box.x.y,
									  m_Box.y.x  -	m_Box.x.x,
									  m_Box.y.y  -	m_Box.x.y, selection);
			Apply();
			ScreenCoordinate_Begin();
			Render();
		}
		break;
	case WM_RBUTTONUP:
		ReleaseCapture();
		break;
	case WM_SETCURSOR:
		::SetCursor(m_WheelCursor[m_WheelState]);
		return TRUE;
	case WM_KEYDOWN:
		{
			switch(wParam)
			{
			case VK_SPACE:
				SwitchFullScreen();
				break;
			case VK_ESCAPE:
				if(IsInFullScreen())SwitchFullScreen();
				break;
			case 'F':
				FitScaleSelection();
				Render();
				break;
			case 'A':
				m_bSelected = 1;
				m_SelectCount = m_bSelected.GetSize();
				OnSelectionChanged();
				break;
			case VK_LEFT:
				MoveSelection(-1/m_Scale,0); 
				Render();
				break;
			case VK_RIGHT:
				MoveSelection(1/m_Scale,0);
				Render();
				break;
			case VK_UP:
				MoveSelection(0,-1/m_Scale);
				Render();
				break;
			case VK_DOWN:
				MoveSelection(0,1/m_Scale);
				Render();
				break;
			}
		}
		break;
	case WM_MOUSEWHEEL:
		if(!m_WheelState)
		{
			m_Scale += fine*(min(width,height)*(((short)HIWORD(wParam))/1500.0f));
			m_Scale = max(EPSILON,m_Scale);
			Render();
		}
		else
		{	static const float scale[3][2] = { {0.05f,0.05f}, {0.05f,0}, {0,0.05f} };
			
			float scale_x = 1 + scale[m_WheelState-1][0]*fine;
			float scale_y = 1 + scale[m_WheelState-1][1]*fine;

			if((short)HIWORD(wParam)<0)
			{
				scale_x = 1/scale_x;
				scale_y = 1/scale_y;
			}

			ScaleSelection(scale_x,scale_y);
			Render();
		}
		break;
	case WM_COMMAND:
		switch(wParam)
		{
		case ID_WHEEL_ZOOM:
		case ID_WHEEL_XY:
		case ID_WHEEL_X:
		case ID_WHEEL_Y:
			m_WheelState = (UINT)wParam - ID_WHEEL_ZOOM;
			break;
		case ID_TOOLS_ARRANGE:
			Rearrange();
			Render();
			break;
		case ID_TOOLS_FITRESOLUTION:
			FitScaleSelection();
			Render();
			break;
		case ID_TOOLS_FITSCALE:
			AutoFitScale();
			Render();
			break;
		}
		break;
	}

	return __super::WndProc(message,wParam,lParam);
}

void CUnwarpWnd::Rearrange()
{
	float x = 0,y = 0;
	float max_height = 0;
	rt::Buffer<int> access;
	access.SetSize(m_bSelected.GetSize());
	access = -1;

	UINT pid = 0;
	UINT vc = 0;
	w32::CTimeMeasureDisplay<> tm(&vc,access.GetSize());
	for(;;pid++)
	{
		UINT i=0;
		for(;i<access.GetSize();i++)
			if(access[i]==-1)break;

		m_bSelected = 0;
		m_SelectCount = 0;
		if(i==access.GetSize())return;

		Vec2f bmin,bmax;

		std::deque<UINT>	list;
		list.push_back(i);
		access[i] = pid;
		vc++; m_SelectCount++;
		bmin = bmax = m_UVAtlas.Get_VertexPosition()[i].v2();
		
		while(list.size())
		{
			UINT v = *list.begin();
			list.pop_front();

			m_bSelected[v] = 1;
			bmin.Min(bmin,m_UVAtlas.Get_VertexPosition()[v].v2());
			bmax.Max(bmax,m_UVAtlas.Get_VertexPosition()[v].v2());

			for(UINT i=0;i<m_Graph[v].GetSize();i++)
			{
				UINT peer = m_Graph[v][i];
				if(access[peer] == -1)
				{	access[peer] = pid;
					vc++; m_SelectCount++;
					list.push_back(peer);
				}
			}
		}

		max_height = max(max_height,bmax.y - bmin.y);
		Apply();
		MoveSelection(x - bmin.x, y - bmin.y);
		x += bmax.x - bmin.x + 0.02f;

		if(x>1)
		{	x = 0;
			y += max_height + 0.02f;
		}
	}
}


void CUnwarpWnd::MoveSelection(float dx, float dy)
{
	if(m_SelectCount)
	{	Vec3f* pUV = m_UVAtlas.GetBuffer_VertexPosition();
		const Vec3f* pUV_Org = m_UVAtlas.Get_VertexPosition();

		for(UINT i=0;i<m_UVAtlas.GetVertexCount();i++)
		{
			pUV[i].x = pUV_Org[i].x + dx*m_bSelected[i];
			pUV[i].y = pUV_Org[i].y + dy*m_bSelected[i];
		}

		m_pMainWnd->UpdateTextureCoord(pUV);

		Apply();
		m_UVAtlas.SubmitBuffers(TRUE);
		m_SelectedCenter += Vec2f(dx,dy);
	}
}

void CUnwarpWnd::AutoFitScale()
{
	rt::Buffer<int> access;
	access.SetSize(m_bSelected.GetSize());
	access = -1;

	m_SelectedCenter = 0;
	UINT pid = 0;
	UINT vc = 0;
	w32::CTimeMeasureDisplay<> tm(&vc,access.GetSize());
	for(;;pid++)
	{
		UINT i=0;
		for(;i<access.GetSize();i++)
			if(access[i]==-1)break;

		m_SelectCount = 0;
		m_bSelected = 0;
		if(i==access.GetSize())return;

		Vec2f bmin,bmax;

		std::deque<UINT>	list;
		list.push_back(i);
		access[i] = pid;
		vc++; m_SelectCount++;
		bmin = bmax = m_UVAtlas.Get_VertexPosition()[i].v2();
		
		while(list.size())
		{
			UINT v = *list.begin();
			list.pop_front();

			m_bSelected[v] = 1;
			bmin.Min(bmin,m_UVAtlas.Get_VertexPosition()[v].v2());
			bmax.Max(bmax,m_UVAtlas.Get_VertexPosition()[v].v2());

			for(UINT i=0;i<m_Graph[v].GetSize();i++)
			{
				UINT peer = m_Graph[v][i];
				if(access[peer] == -1)
				{	access[peer] = pid;
					vc++; m_SelectCount++;
					list.push_back(peer);
				}
			}
		}

		Apply();
		FitScaleSelection();
	}
}

void CUnwarpWnd::FitScaleSelection()
{
	if(m_SelectCount)
	{	
		double UVSize = 0;
		double PosSize = 0;

		const Vec3f* pUV_Org = m_UVAtlas.Get_VertexPosition();
		for(UINT i=0;i<m_Graph.GetSize();i++)
			if(m_bSelected[i] > EPSILON)
				for(UINT k=0;k<m_Graph[i].GetSize();k++)
				{	double wei = m_bSelected[i]*m_bSelected[m_Graph[i][k]];
					UVSize += sqrt(pUV_Org[i].v2().Distance_Sqr(pUV_Org[m_Graph[i][k]].v2())) * wei;
					PosSize+= sqrt(m_3DPostion[i].Distance_Sqr(m_3DPostion[m_Graph[i][k]])) * wei;
				}
		
		float scale = (float)(m_UVScaleTotal/(UVSize/PosSize));
		if(num::IsNumberOk(scale))
			ScaleSelection(scale,scale);
	}
}

void CUnwarpWnd::ScaleSelection(float sx, float sy)
{
	if(m_SelectCount)
	{	
		Vec3f* pUV = m_UVAtlas.GetBuffer_VertexPosition();
		const Vec3f* pUV_Org = m_UVAtlas.Get_VertexPosition();

		for(UINT i=0;i<m_UVAtlas.GetVertexCount();i++)
		{
			float a = m_bSelected[i];
			pUV[i].x = (1-a)*pUV_Org[i].x + a*( (pUV_Org[i].x-m_SelectedCenter.x)*sx + m_SelectedCenter.x);
			pUV[i].y = (1-a)*pUV_Org[i].y + a*( (pUV_Org[i].y-m_SelectedCenter.y)*sy + m_SelectedCenter.y);
		}

		m_pMainWnd->UpdateTextureCoord(pUV);

		Apply();
		m_UVAtlas.SubmitBuffers(TRUE);
	}
}


BOOL CUnwarpWnd::SetSelection(int x, int y, BOOL merge)
{
	UINT vpe = m_UVAtlas.GetVPE();
	UINT tid = m_UVAtlas.PickElement(x,y);
	UINT offset = vpe*tid;

	if(offset < m_UVAtlas.GetMeshElementIndicCount())
	{
		for(UINT i=0;i<vpe;i++)
			if(m_bSelected[m_UVAtlas.Get_MeshElementIndic()[offset+i]] < 0.5f)
				goto RE_SELECT;

		OnSelectionChanged();
		return TRUE;

RE_SELECT:
		rt::Buffer<float>	backup;
		if(merge)
		{
			backup.SetSize(m_bSelected.GetSize());
			backup = m_bSelected;
		}

		std::deque<UINT>	list;
		m_bSelected = 0;

		for(UINT i=0;i<vpe;i++)
		{
			list.push_back(m_UVAtlas.Get_MeshElementIndic()[offset+i]);
			m_bSelected[m_UVAtlas.Get_MeshElementIndic()[offset+i]] = 1;
		}

		while(list.size())
		{
			UINT v = *list.begin();
			list.pop_front();
			m_SelectCount++;

			for(UINT i=0;i<m_Graph[v].GetSize();i++)
			{
				UINT peer = m_Graph[v][i];
				if(m_bSelected[peer] < EPSILON)
				{	m_bSelected[peer] = 1;
					list.push_back(peer);
				}
			}
		}

		if(merge)
		{
			for(UINT i=0;i<m_bSelected.GetSize();i++)
				m_bSelected[i] = max(m_bSelected[i],backup[i]);
		}
	}
	else if(!merge)
	{
		m_SelectCount = 0;
		m_bSelected = 0;
		OnSelectionChanged();
		return FALSE;
	}
	
	OnSelectionChanged();
	return TRUE;
}

void CUnwarpWnd::OnSelectionChanged()
{
	m_SelectedCenter = 0;
	
	if(m_SelectCount)
	{
		double	tot_wei = 0;
		Vec2d	tot_cent(0,0);

		for(UINT i=0;i<m_bSelected.GetSize();i++)
		{
			Vec2f scaled;
			scaled = m_UVAtlas.Get_VertexPosition()[i].v2();
			scaled *= m_bSelected[i];

			tot_cent += scaled;
			tot_wei += m_bSelected[i];
		}
		tot_cent /= tot_wei;
		m_SelectedCenter = tot_cent;
	}

	Vec4b* Col = m_UVAtlas.GetBuffer_VertexColor();
	for(UINT i=0;i<m_UVAtlas.GetVertexCount();i++)
	{
		int d = (int)(100*m_bSelected[i]+0.5f);
		Col[i] = Vec4b(255-d,255-d,255-d/2,1);
	}
	m_UVAtlas.SubmitBuffers();
	Render();

	m_pMainWnd->UpdateSelection(m_bSelected);
}
