#pragma once

#include "1.0\wglh\wglhelper.h"
#include "1.0\wglh\wglmesh.h"

class CMainWnd;

class CUnwarpWnd: public wglh::CwglWnd
{
protected:
	wglh::CwglMesh		m_UVAtlas;
	num::Vec3f			m_Translate;
	float				m_Scale;

	rt::Buffer<num::Vec3f> m_3DPostion;
	float				m_UVScaleTotal;

	wglh::CwglTexture8u3c		m_TextureMap;

	BOOL					m_BoxSelecting;
	num::Vec2<num::Vec2i>	m_Box;
	UINT				m_SelectCount;
	rt::Buffer<float>	m_bSelected;	//vertex
	num::Vec2f			m_SelectedCenter;
	rt::Buffer<rt::BufferEx<UINT>>	m_Graph;

	UINT				m_WheelState;
	HCURSOR				m_WheelCursor[4];

public:
	CMainWnd*			m_pMainWnd;
	CUnwarpWnd(void);
	~CUnwarpWnd(void);

	void	OnRender();
	void	OnInitScene();
	BOOL	OnInitWnd();
	void	OnSelectionChanged();

public:
	void	LoadTexture(LPCTSTR fn);
	BOOL	SetSelection(int x, int y, BOOL merge);	// window coordiante
	void	SetScene(wglh::CwglMesh& mesh);
	void	MoveSelection(float dx, float dy);
	void	ScaleSelection(float sx, float sy);
	void	FitScaleSelection();
	void	AutoFitScale();

	void	Rearrange();
	LRESULT WndProc(UINT message, WPARAM wParam, LPARAM lParam);

};


