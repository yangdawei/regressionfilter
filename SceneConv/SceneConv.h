#pragma once

#include "uv_unwarp.h"

#include "1.0\wglh\TransCtrl.h"
#include "1.0\wglh\jgl\sprite.h"


class CMainWnd:public wglh::CwglArcballWnd
{
protected:

	wglh::CwglMesh				m_Mesh;

	BOOL						m_bShowBox;
	BOOL						m_bWireframe;
	BOOL						m_bPointCloud;
	BOOL						m_bShowTexture;
	wglh::CwglTransformation	m_VisualSpaceView;

	wglh::CwglTexture8u3c		m_TextureMap;
	num::Vec3f					m_SceneTrans;
	float						m_SceneScale;
	rt::String					m_FileTitle;

	CUnwarpWnd					m_wUnwarpUI;

protected:
	void OnRender();
	LRESULT WndProc(UINT message, WPARAM wParam, LPARAM lParam); // --
	// Selection
	BOOL						m_IsDragging;
	BOOL						m_bShowSelected;
	num::Vec2i					m_MouseStart;
	num::Vec2i					m_MouseEnd;

	void ImportObject(LPCTSTR fn);
	void LoadScene(LPCTSTR fn, BOOL Normalize_Size = TRUE);

public:
	CMainWnd();
	~CMainWnd();
	void UpdateShowMenu();
	void Show(UINT what);
	BOOL OnInitWnd();
	void OnUninitScene();
	void UpdateSelection(const float * pS);
	void UpdateTextureCoord(const num::Vec3f* pUV);

protected:
	wglh::CTransformCtrl m_transformCtrl;
	jgl::JSpriteContainer* m_sprites;
};

