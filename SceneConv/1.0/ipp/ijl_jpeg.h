#pragma once 

//////////////////////////////////////////////////////////////////////
// Visual Computing Foundation Classes (VCFC)
//					Jiaping Wang  2006.2
//					e_boris2002@hotmail.com
//
// (C) Copyright Jiaping Wang 2004.
//
// Permission to copy, use, modify, sell and distribute this software
// is granted provided this copyright notice appears in all copies.
// This software is provided "as is" without express or implied
// warranty, and with no claim as to its suitability for any purpose.
//
// Absolutely no warranty!!
//////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////
//  Brief Description
//
//  ijl_jpeg.h
//
//  Hi-Speed jpeg codec in IJL
//
//////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////
// Revise History
//
// Inital version	2005.5		Jiaping
//
//////////////////////////////////////////////////////////////////////


#include <ijl.h>
#pragma comment(lib,"ijl15l.lib")

#include "..\rt\compact_vector.h"
#include "..\w32\w32_basic.h"
#include "..\w32\file_64.h"


namespace ipp
{
namespace file
{

class CJpegFile
{
private:
	int						_SubsamplingMode;
protected:
	JPEG_CORE_PROPERTIES	m_JcProps;
	
	rt::Buffer<BYTE>		m_TempBuffer;
	int						m_BufferUsedLen;

	int						m_DecodedImageWidth;
	int						m_DecodedImageHeight;
	int						m_DecodedImageStep;
	int						m_DecodedImageChannel;

	void					_SetBufferSize(int size){ m_TempBuffer.SetSize(max((UINT)size,m_TempBuffer.GetSize())); m_BufferUsedLen=0; }

public:
	enum
	{
		SubSampling_444 = IJL_NONE,
		SubSampling_411 = IJL_411,
		SubSampling_422 = IJL_422
	};

	CJpegFile();
	~CJpegFile(){ ijlFree(&m_JcProps); }

	LPCBYTE					GetOutput()const { return m_TempBuffer; }
	UINT					GetOutputSize()const { return m_BufferUsedLen; }

	UINT					GetImageWidth()const { return m_DecodedImageWidth; }
	UINT					GetImageHeight()const { return m_DecodedImageHeight; }
	UINT					GetImageStep()const { return m_DecodedImageStep; }
	UINT					GetImageChannel()const { return m_DecodedImageChannel; }

	void SetQualityRatio(int quality){ ASSERT(quality<=100 && quality>=0); m_JcProps.jquality = quality; }
	void SetSubSamplingType(int	mode = SubSampling_422){ _SubsamplingMode = mode; }

	BOOL Encode(LPCTSTR Fn,LPCBYTE pData,int Channel,int Width,int Height,int Step = 0);
	BOOL Encode(LPCBYTE pData,int Channel,int Width,int Height,int Step = 0);

	BOOL Decode(LPCBYTE pDataJpeg,UINT DataLen,LPBYTE pImageOut=0,int Step=0);
	BOOL Decode(LPCTSTR Fn,LPBYTE pImageOut=0,int Step=0);
	BOOL Decode_HeaderOnly(LPCBYTE pDataJpeg,UINT DataLen);
	BOOL Decode_HeaderOnly(LPCTSTR Fn);


#ifdef INCLUDE_WGLH_IMAGE
	BOOL Encode(LPCTSTR Fn,const wglh::CwglImage &bitmap)
	{	if( (LPCBYTE)bitmap )return Encode(Fn,bitmap,bitmap.GetBPP()/8,bitmap.GetWidth(),bitmap.GetHeight(),bitmap.GetStep());
		return FALSE;
	}

	BOOL Encode(const wglh::CwglImage &bitmap)
	{	if( (LPCBYTE)bitmap )return Encode(bitmap,bitmap.GetBPP()/8,bitmap.GetWidth(),bitmap.GetHeight(),bitmap.GetStep());
		return FALSE;
	}

	void GetOutput(wglh::CwglImage &bitmap)
	{	ASSERT( m_DecodedImageStep );	//Check is image content exists
		bitmap.SetRef(m_TempBuffer,m_DecodedImageWidth,m_DecodedImageHeight,m_DecodedImageStep,m_DecodedImageChannel);
	}

	void CopyOutput(wglh::CwglImage &bitmap)
	{	ASSERT( m_DecodedImageStep );	//Check is image content exists
		wglh::CwglBitmap ref(m_TempBuffer,m_DecodedImageWidth,m_DecodedImageHeight,m_DecodedImageStep,m_DecodedImageChannel);
		bitmap.SetSize(m_DecodedImageWidth,m_DecodedImageHeight,8*m_DecodedImageChannel);
		ref.CopyTo(bitmap);
	}
#endif // INCLUDE_WGLH_IMAGE




#ifdef INCLUDE_IPP_IMAGE
	template<UINT channel,class BaseImage>
	BOOL Encode(LPCTSTR Fn,const ::ipp::CImage<BYTE,channel,BaseImage> &img)
	{	if( img.IsNotNull() )return Encode(Fn,img,channel,img.GetWidth(),img.GetHeight(),img.GetStep());
		return FALSE;
	}

	template<UINT channel,class BaseImage>
	BOOL Encode(const ::ipp::CImage<BYTE,channel,BaseImage> &img)
	{	if( img.IsNotNull() )return Encode(img,channel,img.GetWidth(),img.GetHeight(),img.GetStep());
		return FALSE;
	}

	template<UINT channel>
	BOOL GetOutput(::ipp::CImage_Ref<BYTE,channel> &img_ref)
	{	if(channel==m_DecodedImageChannel)
		{	ASSERT( m_DecodedImageStep );	//Check is image content exists
			img_ref.SetRef(	(::ipp::CImage_Ref<BYTE,channel>::PixelType*)m_TempBuffer.Begin(),
								m_DecodedImageWidth,m_DecodedImageHeight,m_DecodedImageStep);
			return TRUE;
		}else{ return FALSE; }
	}

	template<UINT channel,class BaseImage>
	void CopyOutput(::ipp::CImage<BYTE,channel,BaseImage> &img)
	{	ASSERT( m_DecodedImageStep );	//Check is image content exists
		img.SetSize(m_DecodedImageWidth,m_DecodedImageHeight);

		if(m_DecodedImageChannel == 3)
		{	::ipp::CImage_Ref<BYTE,3>	img_ref((::ipp::CImage_Ref<BYTE,3>::PixelType*)m_TempBuffer.Begin(),
												m_DecodedImageWidth,m_DecodedImageHeight,m_DecodedImageStep);
			img.CopyFrom(img_ref);
		}
		else if(m_DecodedImageChannel == 1)
		{	::ipp::CImage_Ref<BYTE,1>	img_ref((::ipp::CImage_Ref<BYTE,1>::PixelType*)m_TempBuffer.Begin(),
												m_DecodedImageWidth,m_DecodedImageHeight,m_DecodedImageStep);
			img.CopyFrom(img_ref);
		}
		else if(m_DecodedImageChannel == 4)
		{	::ipp::CImage_Ref<BYTE,4>	img_ref((::ipp::CImage_Ref<BYTE,4>::PixelType*)m_TempBuffer.Begin(),
												m_DecodedImageWidth,m_DecodedImageHeight,m_DecodedImageStep);
			img.CopyFrom(img_ref);
		}
		else ASSERT(0);
	}
#endif // #ifdef INCLUDE_IPP_IMAGE
};


class CJpegSequence_Writer
{
protected:
	class CJpegImage
	{
	public:
		double	m_TimeStamp; // ms
		LPBYTE	m_pJpegData;
		UINT	m_JpegDataSize;
	};

	w32::CCriticalSection		m_JpegSeqCCS;
	rt::BufferEx<CJpegImage>	m_JpegSeq;

	double  m_IncomingTime;
	int		m_FrameSubmitted;

	HANDLE	m_hEncodeThread;
	w32::CTimeMeasure		m_Timer;

	w32::CEvent				m_BufferUpdated;
	w32::CCriticalSection	m_IncomingBufferCCS;
	LPCBYTE	m_IncomingImage;
	int		m_Channel;
	int		m_Width;
	int		m_Height;
	int		m_Step;


	static  DWORD WINAPI _EncodeThread(LPVOID pThis);

public:
	CJpegSequence_Writer();
	~CJpegSequence_Writer();
	void	Initialize(LPCBYTE pDataImage,int Channel,int Width,int Height,int Step);
	BOOL	Save(LPCTSTR fn);
	BOOL	WriteFrame_Begin(BOOL Wait = TRUE);
	void	WriteFrame_End();
	void	Clean();

	int		GetFrameSubmitted(){ return m_FrameSubmitted; }
	int		GetFrameHandled()
	{ 
		EnterCCSBlock(m_JpegSeqCCS);
		return (int)m_JpegSeq.GetSize();
	}
};

class CJpegSequence_Reader 
{
protected:
	rt::Buffer<ULONGLONG>		m_FrameOffset;
	rt::Buffer<UINT>			m_FrameSize;
	rt::Buffer<double>			m_FrameTimestamp;

	rt::Buffer<BYTE>		m_TempBuffer;
	void					_SetBufferSize(int size);

	w32::CFile64			m_SeqFile;
	float					m_FramePreSec;
	CJpegFile				m_Codec;

	int						m_LastIndex;

public:
	UINT					GetImageWidth()const { return m_Codec.GetImageWidth(); }
	UINT					GetImageHeight()const { return m_Codec.GetImageHeight(); }
	UINT					GetImageStep()const { return m_Codec.GetImageStep(); }
	UINT					GetImageChannel()const { return m_Codec.GetImageChannel(); }
	UINT					GetFrameCount()const { return m_FrameOffset.GetSize(); }
	float					GetFps()const { return m_FramePreSec; }

	BOOL					IsOpen(){ return m_SeqFile.IsOpen(); }
	BOOL					Load(LPCTSTR fn);
	BOOL					ReadFrame(int index,LPBYTE pImage=0,int step=0);
	BOOL					ReadFrame_HeaderOnly(int index);
};

} // 
} // namespace ipp


