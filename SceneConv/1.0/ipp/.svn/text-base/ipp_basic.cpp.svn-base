#include "stdafx.h"
#include "ipp_basic.h"
#include "..\w32\debug_log.h"
#include "..\num\small_vec.h"

ipp::CIppiEnvParam	ipp::CIppiEnvParam::g_IppEnv;

#define IppEnvStackSize	32
#define EnvParamSize (reinterpret_cast<DWORD>(&(( static_cast<const CIppiEnvParam*>(NULL) )->m_pEnvParamStack)))

ipp::CIppiEnvParam::CIppiEnvParam()
{
	RoundMode = ippRndNear;
	InterpolationMode = IPPI_INTER_LINEAR;

	DitherMode = ippDitherNone;
	HintAlgorithm = ippAlgHintAccurate;

	FloatMin = 0.0f;
	FloatMax = 1.0f;

	ResultBitShift = 0;
	IntegerScaleFactor = 1;

	m_pEnvParamStack = _Malloc32AL(BYTE,IppEnvStackSize*EnvParamSize);
	ASSERT(m_pEnvParamStack);

	m_StackPointer = 0;
}

ipp::CIppiEnvParam::~CIppiEnvParam()
{
	_SafeFree32AL(m_pEnvParamStack);
}

void ipp::CIppiEnvParam::Push()
{
	ASSERT( m_StackPointer < IppEnvStackSize );
	memcpy(&m_pEnvParamStack[m_StackPointer*EnvParamSize],this,EnvParamSize);
	m_StackPointer++;
}

void ipp::CIppiEnvParam::Pop()
{
	ASSERT( m_StackPointer > 0 );
	m_StackPointer--;
	memcpy(this,&m_pEnvParamStack[m_StackPointer*EnvParamSize],EnvParamSize);	
}
#undef EnvParamSize

namespace ipp
{
static const LPCSTR TextBar= " Information\n==========================\n";
static const LPCSTR na= " N/A";
}

void ipp::_meta_::_DumpIntelIppInformation(const IppLibraryVersion* pInfo, IppCpuType Cpu, int core)
{
	_CheckDump("Intel IPP"<<TextBar);

	_CheckDump(  "Version     : "<<pInfo->Version);
	_CheckDump("\nBuild Date  : "<<pInfo->BuildDate);
	_CheckDump("\nTarget CPU  : "<<pInfo->targetCpu[0]
								<<pInfo->targetCpu[1]
								<<pInfo->targetCpu[2]
								<<pInfo->targetCpu[3]);

	_CheckDump("\nDetected CPU: ");
	switch(Cpu)
	{
	case ippCpuPP:           _CheckDump("Intel(R) Pentium(R) processor"); break; 
	case ippCpuPMX:          _CheckDump("Pentium(R) processor with MMX(TM) technology"); break; 
	case ippCpuPPR:          _CheckDump("Pentium(R) Pro processor"); break; 
	case ippCpuPII:          _CheckDump("Pentium(R) II processor"); break; 
	case ippCpuPIII:         _CheckDump("Pentium(R) III processor or Pentium(R) III Xeon(TM) processor"); break;
	case ippCpuP4:           _CheckDump("Pentium(R) 4 processor or Intel(R) Xeon(TM) processor"); break;
	case ippCpuP4HT:         _CheckDump("Pentium(R) 4 Processor with HT Technology"); break; 
	case ippCpuP4HT2:		 _CheckDump("Pentium(R) 4 Processor with HT2 Technology"); break; 
	case ippCpuCentrino:     _CheckDump("Intel(R) Centrino(TM) mobile technology"); break; 
	case ippCpuCoreSolo:     _CheckDump("Intel(R) Core(TM) Solo processor"); break; 
	case ippCpuCoreDuo:		 _CheckDump("Intel(R) Core(TM) Duo processor"); break; 
	case ippCpuITP:			 _CheckDump("Intel(R) Itanium(R) processor"); break; 
	case ippCpuITP2:         _CheckDump("Itanium(R) 2 processor"); break; 
	case ippCpuEM64T:		 _CheckDump("Intel(R) Extended Memory 64 Technology"); break;
	case ippCpuC2D:			 _CheckDump("Intel(R) Core(TM) 2 Duo processor"); break;
	case ippCpuC2Q:			 _CheckDump("Intel(R) Core(TM) 2 Quad processor"); break;
	case ippCpuBonnell:		 _CheckDump("Intel(R) Atom(TM) processor"); break;
	case ippCpuPenryn:		 _CheckDump("Intel(R) Core(TM) 2 processor with SSE 4.1"); break;
	case ippCpuSSE:			 _CheckDump("Processor w/ Streaming SIMD Extensions"); break;
	case ippCpuSSE2:		 _CheckDump("Processor w/ Streaming SIMD Extensions 2"); break;
	case ippCpuSSE3:		 _CheckDump("Processor w/ Streaming SIMD Extensions 3"); break;
	case ippCpuSSE41:		 _CheckDump("Processor w/ Streaming SIMD Extensions 4.1"); break;
	case ippCpuSSE42:		 _CheckDump("Processor w/ Streaming SIMD Extensions 4.2"); break;
	case ippCpuX8664:		 _CheckDump("Processor w/ 64-bit extension"); break;
	case ippCpuUnknown:		 _CheckDump("Unknown processor"); break;
	default:
		_CheckDump(na);
	}

	//_CheckDump("\nFrequency   : ");
	//{	int mhz;
	//	if( ippStsNoErr == ippGetCpuFreqMhz(&mhz) )
	//	{
	//		_CheckDump(((float)mhz/1000.0)<<" GHz");
	//	}
	//	else _CheckDump(na);
	//}

	_CheckDump("\nCore-on-Die : "<<core);
	//_CheckDump("\nFeatures    : ");
	//if(mask&(1<< 0))_CheckDump("MMX ");
	//if(mask&(1<< 1))_CheckDump("SSE ");
	//if(mask&(1<< 2))_CheckDump("SSE2 ");
	//if(mask&(1<< 3))_CheckDump("SSE3 ");
	//if(mask&(1<< 4))_CheckDump("S-SSE3 ");
	//if(mask&(1<< 5))_CheckDump("MOVBE ");
	//if(mask&(1<< 6))_CheckDump("SSE41 ");
	//if(mask&(1<< 7))_CheckDump("SSE42 ");

	_CheckDump('\n'<<'\n');
}



namespace ipp
{
namespace ipp_cpp
{

IppStatus ippiHaarWTInv_C1R(LPCIpp8u pSrc,int srcStep,LPIpp8u pDst,int dstStep, IppiSize roi)
{
	if((!pSrc)||(!pDst))return ippStsNullPtrErr;
	if(roi.height<=0 || roi.width<=0)return ippStsSizeErr;
	if(roi.height&1 || roi.width&1)return ippStsSizeErr;

	int Offset_HL,Offset_LH,Offset_HH;
	Offset_HL = roi.width>>1;
	Offset_LH = (roi.height>>1)*dstStep;
	Offset_HH = Offset_HL+Offset_LH;

	struct _threshold8u
	{	static __forceinline int call(int x){ return (x>=0?(x<=255?(x):255):0); }
	};

	const Ipp8u * pS;
	int DoubleStep = dstStep<<1;
	for(pS=pSrc;pS<&pSrc[Offset_LH];pDst+=DoubleStep,pS+=srcStep)
	{
		const Ipp8u * pLL=pS;
		Ipp8u *p=pDst;
		for(;pLL<&pS[Offset_HL];pLL++,p+=2)
		{
			int va,ha,aa;
			va = pLL[0]+pLL[Offset_HL] - 128;
			ha = pLL[0]+pLL[Offset_LH] - 128;
			aa = va-pLL[0]+pLL[Offset_HH] - 128 +ha;

			p[1] = _threshold8u::call((ha<<1) - aa);
			*p = _threshold8u::call(aa);
			p[srcStep] = _threshold8u::call((va<<1) - aa);
			p[srcStep+1] = _threshold8u::call(aa-((va+ha - (pLL[0]<<1))<<1));
		}
	}

	return ippStsNoErr;
}


IppStatus ippiHaarWTFwd_C1R(LPCIpp8u pSrc,int srcStep, LPIpp8u pDst,int dstStep, IppiSize roi)
{
	if((!pSrc)||(!pDst))return ippStsNullPtrErr;
	if(roi.height<=0 || roi.width<=0)return ippStsSizeErr;
	if(roi.height&1 || roi.width&1)return ippStsSizeErr;

	int Offset_HL,Offset_LH,Offset_HH;
	Offset_HL = roi.width>>1;
	Offset_LH = (roi.height>>1)*dstStep;
	Offset_HH = Offset_HL+Offset_LH;

	Ipp8u * pD;
	int DoubleStep = srcStep<<1;
	for(pD=pDst;pD<&pDst[Offset_LH];pD+=dstStep,pSrc+=DoubleStep)
	{
		Ipp8u * pLL=pD;
		const Ipp8u *p=pSrc;
		for(;pLL<&pD[Offset_HL];pLL++,p+=2)
		{
			int ha0 = ((int)p[0]+(int)p[1])>>1;
			int ha1 = (((int)p[srcStep]+(int)p[srcStep+1])>>1);
			int avg = (ha0+ha1)>>1;
			int va0 = ((int)p[0]+(int)p[srcStep])>>1;

			*pLL = avg;

			//ASSERT((va0 - avg)+128>=0);
			//ASSERT((va0 - avg)+128<=255);
			pLL[Offset_HL] = (va0 - avg)+128;

			//ASSERT((ha0 - avg)+128>=0);
			//ASSERT((ha0 - avg)+128<=255);
			pLL[Offset_LH] = (ha0 - avg)+128;

			//ASSERT(((((int)p[0] - ha0) - ((int)p[srcStep] - ha1))>>1)+128>=0);
			//ASSERT(((((int)p[0] - ha0) - ((int)p[srcStep] - ha1))>>1)+128<=255);
			pLL[Offset_HH] = ((((int)p[0] - ha0) - ((int)p[srcStep] - ha1))>>1)+128;
		}
	}

	return ippStsNoErr;
}

IppStatus ippiHaarWTInv_C1R(LPCIpp32f pSrc,int srcStep, LPIpp32f pDst,int dstStep, IppiSize roi)
{
	if((!pSrc)||(!pDst))return ippStsNullPtrErr;
	if(roi.height<=0 || roi.width<=0)return ippStsSizeErr;
	if(roi.height&1 || roi.width&1)return ippStsSizeErr;

	ASSERT((srcStep&3) == 0);
	ASSERT((dstStep&3) == 0);

	srcStep>>=2;
	dstStep>>=2;

	int Offset_HL,Offset_LH,Offset_HH;
	Offset_HL = roi.width>>1;
	Offset_LH = (roi.height>>1)*dstStep;
	Offset_HH = Offset_HL+Offset_LH;

	LPCIpp32f pS;
	int DoubleStep = dstStep<<1;
	for(pS=pSrc;pS<&pSrc[Offset_LH];pDst+=DoubleStep,pS+=srcStep)
	{
		LPCIpp32f pLL=pS;
		LPIpp32f p=pDst;
		for(;pLL<&pS[Offset_HL];pLL++,p+=2)
		{
			float va,ha,aa;
			va = pLL[0]+pLL[Offset_HL];
			ha = pLL[0]+pLL[Offset_LH];
			aa = va-pLL[0]+pLL[Offset_HH]+ha;

			p[1] = ((ha*2) - aa);
			*p = (aa);
			p[srcStep] = ((va*2) - aa);
			p[srcStep+1] = (aa-((va+ha - (pLL[0]*2))*2));
		}
	}

	return ippStsNoErr;
}


IppStatus ippiHaarWTFwd_C1R(LPCIpp32f pSrc,int srcStep, LPIpp32f pDst,int dstStep, IppiSize roi)
{
	if((!pSrc)||(!pDst))return ippStsNullPtrErr;
	if(roi.height<=0 || roi.width<=0)return ippStsSizeErr;
	if(roi.height&1 || roi.width&1)return ippStsSizeErr;

	ASSERT((srcStep&3) == 0);
	ASSERT((dstStep&3) == 0);

	srcStep>>=2;
	dstStep>>=2;

	int Offset_HL,Offset_LH,Offset_HH;
	Offset_HL = roi.width>>1;
	Offset_LH = (roi.height>>1)*dstStep;
	Offset_HH = Offset_HL+Offset_LH;

	LPIpp32f  pD;
	int DoubleStep = srcStep<<1;
	for(pD=pDst;pD<&pDst[Offset_LH];pD+=dstStep,pSrc+=DoubleStep)
	{
		LPIpp32f  pLL=pD;
		LPCIpp32f p=pSrc;
		for(;pLL<&pD[Offset_HL];pLL++,p+=2)
		{
			float ha0 = (p[0]+p[1])/2;
			float ha1 = ((p[srcStep]+p[srcStep+1])/2);
			float avg = (ha0+ha1)/2;
			float va0 = (p[0]+p[srcStep])/2;

			*pLL = avg;
			pLL[Offset_HL] = (va0 - avg);
			pLL[Offset_LH] = (ha0 - avg);
			pLL[Offset_HH] = (((p[0] - ha0) - (p[srcStep] - ha1))/2);
		}
	}

	return ippStsNoErr;
}


} // namespace ipp_cpp
} // namespace ipp