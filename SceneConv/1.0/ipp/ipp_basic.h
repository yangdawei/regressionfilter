#pragma once

//////////////////////////////////////////////////////////////////////
// Visual Computing Foundation Classes (VCFC)
//					Jiaping Wang  2006.2
//					e_boris2002@hotmail.com
//
// (C) Copyright Jiaping Wang 2004.
//
// Permission to copy, use, modify, sell and distribute this software
// is granted provided this copyright notice appears in all copies.
// This software is provided "as is" without express or implied
// warranty, and with no claim as to its suitability for any purpose.
//
// Absolutely no warranty!!
//////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////
//  Brief Description
//
//  ipp_basic.h
//  Macros to control linkage
//
//	Win32:
//	IPP_LINK_STATIC_LIB_PX - C-optimized for all IA-32 processors 
//	IPP_LINK_STATIC_LIB_T7 - Optimized for Pentium 4 processors with Streaming SIMD Extensions 3 (SSE3)
//	IPP_LINK_STATIC_LIB_W7 - Optimized for Pentium 4 processors 
//	IPP_LINK_STATIC_LIB_V8 - Intel Core2Duo processors
//	IPP_LINK_STATIC_LIB_P8 - Intel Core2Duo processors (Penryn)
//
//	Win64:
//	IPP_LINK_STATIC_LIB_MX - C-optimized for all Intel Xeon processors with Intel Extended Memory 64 Technology
//	IPP_LINK_STATIC_LIB_M7 - Optimized for Intel Xeon processors with Intel Extended Memory 64 Technology
//
//
//////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////
// Revise History
//
// Inital version	2006.6.9		Jiaping
//
//////////////////////////////////////////////////////////////////////

#include "..\w32\win32_ver.h"
#include "..\w32\file_64.h"
#include "..\rt\runtime_base.h"

#pragma warning(disable:4819)

#if (defined(IPP_LINK_STATIC_LIB_PX)||defined(IPP_LINK_STATIC_LIB_A6)||defined(IPP_LINK_STATIC_LIB_T7)||defined(IPP_LINK_STATIC_LIB_W7)) && defined(_WIN64)
	#error X86 CPU is specified for IPP static link but the target machine is x64
#endif

#if (defined(IPP_LINK_STATIC_LIB_MX)||defined(IPP_LINK_STATIC_LIB_M7)) && !defined(_WIN64)
	#error x64 CPU is specified for IPP static link but the target machine is X86
#endif

#if defined(IPP_LINK_STATIC_LIB_PX)
	#define IPP_LINK_STATIC
	#define IPPAPI(type,name,arg) extern type __STDCALL px_##name arg;
	#define IPPCALL(name) px_##name
#elif defined(IPP_LINK_STATIC_LIB_T7)
	#define IPP_LINK_STATIC
	#define IPPAPI(type,name,arg) extern type __STDCALL t7_##name arg;
	#define IPPCALL(name) t7_##name
#elif defined(IPP_LINK_STATIC_LIB_W7)
	#define IPP_LINK_STATIC
	#define IPPAPI(type,name,arg) extern type __STDCALL w7_##name arg;
	#define IPPCALL(name) w7_##name
#elif defined(IPP_LINK_STATIC_LIB_V8)
	#define IPP_LINK_STATIC
	#define IPPAPI(type,name,arg) extern type __STDCALL v8_##name arg;
	#define IPPCALL(name) v8_##name
#elif defined(IPP_LINK_STATIC_LIB_P8)
	#define IPP_LINK_STATIC
	#define IPPAPI(type,name,arg) extern type __STDCALL p8_##name arg;
	#define IPPCALL(name) p8_##name
#elif defined(IPP_LINK_STATIC_LIB_MX)
	#define IPP_LINK_STATIC
	#define IPPAPI(type,name,arg) extern type __STDCALL mx_##name arg;
	#define IPPCALL(name) mx_##name
#elif defined(IPP_LINK_STATIC_LIB_M7)
	#define IPP_LINK_STATIC
	#define IPPAPI(type,name,arg) extern type __STDCALL m7_##name arg;
	#define IPPCALL(name) m7_##name
#else
    #ifndef IPP_LINK_DYNAMIC
        #define IPP_LINK_DYNAMIC
    #endif
    #define IPPAPI(type,name,arg) extern type __STDCALL name arg;
    #define IPPCALL(name) name
#endif

#ifndef _MAKING_STATIC_LIB_

#ifdef _WIN64
	#ifdef IPP_LINK_STATIC
		#pragma message("\n>> Static linking with EM64t version of IPP\n")
		#pragma comment(lib,"ippcoreem64tl.lib")
		#pragma comment(lib,"ippccmergedem64t.lib")
		#pragma comment(lib,"ippimergedem64t.lib")
		#pragma comment(lib,"ippimergedem64t_t.lib")
		#pragma comment(lib,"ippsmergedem64t.lib")
		#pragma comment(lib,"ippjmergedem64t.lib")
		#pragma comment(lib,"ippcvmergedem64t.lib")
		#pragma comment(lib,"ippacemergedem64t.lib   ") 
	#else
		#pragma message("\n>> Linking with EM64t version of IPP\n")
		#pragma comment(lib,"ippcoreem64t.lib")
		#pragma comment(lib,"Ippiem64t.lib")
		#pragma comment(lib,"ippsem64t.lib")
		#pragma comment(lib,"ippjem64t.lib")
		#pragma comment(lib,"ippcvem64t.lib")
		#pragma comment(lib,"ippccemergedem64t.lib")
		#pragma comment(lib,"ippccmergedem64t.lib")
		#pragma comment(lib,"ippcoreem64t_t.lib")
	#endif

#else
	#ifdef IPP_LINK_STATIC
		#pragma message("\n>> Static linking with IA32 version of IPP\n")
		#pragma comment(lib,"ippcorel.lib")
		#pragma comment(lib,"ippccmerged.lib")
		#pragma comment(lib,"ippchmerged.lib")
		#pragma comment(lib,"ippimerged.lib")
		#pragma comment(lib,"ippsmerged.lib")
		#pragma comment(lib,"ippvmmerged.lib")
		#pragma comment(lib,"ippjemerged.lib")
		#pragma comment(lib,"ippvcmerged.lib")
		#pragma comment(lib,"ippcvmerged.lib")
		#pragma comment(lib,"ippacmerged.lib")
		#pragma comment(lib,"ippdcmerged.lib")
	#else
		#pragma message("\n>> Linking with IA32 version of IPP\n")
		#pragma comment(lib,"ippcore.lib")
		#pragma comment(lib,"ippcc.lib")
		#pragma comment(lib,"ippch.lib")
		#pragma comment(lib,"Ippi.lib")
		#pragma comment(lib,"ipps.lib")
		#pragma comment(lib,"ippj.lib")
		#pragma comment(lib,"ippcv.lib")
		#pragma comment(lib,"ippvc.lib")
		#pragma comment(lib,"ippvm.lib")
	#endif

#endif

#endif // _MAKING_STATIC_LIB_

#pragma warning(default:4819)

#include <ipp.h>

#ifdef IPP_LINK_STATIC
#define IPPAPI_S(type,name,arg) extern type __STDCALL name arg;
extern "C"
{
	IPPAPI_S( IppCpuType, ippGetCpuType, (void) )
	IPPAPI_S( IppStatus, ippGetCpuFreqMhz, ( int* pMhz ) )
	IPPAPI_S( int, ippGetNumCoresOnDie,( void ))
	IPPAPI_S( IppStatus, ippGetCpuFeatures, (	Ipp64u* pFeaturesMask,
												Ipp32u  pCpuidInfoRegs[4] ) )
}
#undef IPPAPI_S
#endif // IPP_LINK_STATIC

///////////////////////////////////////////////////
//Helper macro 
#define IPPVERIFY(x) VERIFY(ippStsNoErr == (x))
#define IPP_FUNC	__forceinline
#define IPPI_INTER_DEFAULT IPPI_INTER_LINEAR

namespace ipp
{

// IPP Pixel Types definition
#define PP_MASK  0x0FF
#define PP_SIGN  0x100
#define PP_FLOAT 0x200
#define PP_CPLX  0x400
typedef enum _tagPixelTypeId
{
   _NONE = 0 , 
   _8u   = 8 , 
   _8s   = 8  | PP_SIGN,
   _16u  = 16,
   _16s  = 16 | PP_SIGN,
   _32s  = 32 | PP_SIGN,
   _32sc = 64 | PP_SIGN | PP_CPLX,
   _32f  = 32 | PP_FLOAT ,
   _32fc = 64 | PP_FLOAT  | PP_CPLX
} t_PixelTypeId;

// Type definitions
#define IPP_TYPE_EXT(x) typedef x * LP##x; typedef const x * LPC##x;
	IPP_TYPE_EXT(Ipp8u);
	IPP_TYPE_EXT(Ipp8s);
	IPP_TYPE_EXT(Ipp16u);
	IPP_TYPE_EXT(Ipp16s);
	IPP_TYPE_EXT(Ipp32u);
	IPP_TYPE_EXT(Ipp32s);
	IPP_TYPE_EXT(Ipp32f);
	IPP_TYPE_EXT(Ipp64s);
	IPP_TYPE_EXT(Ipp64f);
#undef IPP_TYPE_EXT

enum _tagAxisOrientation
{
	AxisHorizontal = ippAxsHorizontal,
	AxisVertical = ippAxsVertical,
	AxisBoth = ippAxsBoth
};

// memcpy enhancement
IPP_FUNC void * memcpy8AL( void *dest,const void *src,int len)
{					   ASSERT(dest&&src);  IPPCALL(ippsCopy_8u)((LPCIpp8u)src,(LPIpp8u)dest,len); return dest;}
IPP_FUNC void * memcpy16AL( void *dest,const void *src,int len)
{ ASSERT((len&1) ==0); ASSERT(dest&&src);  IPPCALL(ippsCopy_16s)((LPCIpp16s)src,(LPIpp16s)dest,len>>1); return dest;}
IPP_FUNC void * memcpy32AL( void *dest,const void *src,int len)
{ ASSERT((len&3) ==0); ASSERT(dest&&src);  IPPCALL(ippsCopy_32f)((LPCIpp32f)src,(LPIpp32f)dest,len>>2); return dest;}
IPP_FUNC void * memcpy64AL( void *dest,const void *src,int len)
{ ASSERT((len&7) ==0); ASSERT(dest&&src);  IPPCALL(ippsCopy_64f)((LPCIpp64f)src,(LPIpp64f)dest,len>>3); return dest;}

// memmove enhancement
IPP_FUNC void * memmove8AL( void *dest,const void *src,int len)
{					   ASSERT(dest&&src);  IPPCALL(ippsMove_8u)((LPCIpp8u)src,(LPIpp8u)dest,len); return dest;}
IPP_FUNC void * memmove16AL( void *dest,const void *src,int len)
{ ASSERT((len&1) ==0); ASSERT(dest&&src);  IPPCALL(ippsMove_16s)((LPCIpp16s)src,(LPIpp16s)dest,len>>1); return dest;}
IPP_FUNC void * memmove32AL( void *dest,const void *src,int len)
{ ASSERT((len&3) ==0); ASSERT(dest&&src);  IPPCALL(ippsMove_32f)((LPCIpp32f)src,(LPIpp32f)dest,len>>2); return dest;}
IPP_FUNC void * memmove64AL( void *dest,const void *src,int len)
{ ASSERT((len&7) ==0); ASSERT(dest&&src);  IPPCALL(ippsMove_64f)((LPCIpp64f)src,(LPIpp64f)dest,len>>3); return dest;}

// ZeroMemory enhancement
IPP_FUNC void ZeroMemory8AL( void *dest,int len )
{						ASSERT(dest); IPPCALL(ippsZero_8u)((LPIpp8u)dest,len); }
IPP_FUNC void ZeroMemory16AL( void *dest,int len )
{  ASSERT((len&1) ==0); ASSERT(dest); IPPCALL(ippsZero_16s)((LPIpp16s)dest,len>>1); }
IPP_FUNC void ZeroMemory32AL( void *dest,int len )
{  ASSERT((len&3) ==0); ASSERT(dest); IPPCALL(ippsZero_32f)((LPIpp32f)dest,len>>2); }
IPP_FUNC void ZeroMemory64AL( void *dest,int len )
{  ASSERT((len&7) ==0); ASSERT(dest); IPPCALL(ippsZero_64f)((LPIpp64f)dest,len>>3); }

// memset enhancement
IPP_FUNC void memset8u( void *dest,BYTE val,int count )
{  ASSERT(dest); IPPCALL(ippsSet_8u)(val,(LPIpp8u)dest,count); }
IPP_FUNC void memset16u( void *dest,WORD val,int count )
{  ASSERT(dest); IPPCALL(ippsSet_16s)(val,(LPIpp16s)dest,count); }
IPP_FUNC void memset32u( void *dest,DWORD val,int count )
{  ASSERT(dest); IPPCALL(ippsSet_32s)(val,(LPIpp32s)dest,count); }
IPP_FUNC void memset32f( void *dest,float val,int count )
{  ASSERT(dest); IPPCALL(ippsSet_32f)(val,(LPIpp32f)dest,count); }
IPP_FUNC void memset64u( void *dest,ULONGLONG val,int count )
{  ASSERT(dest); IPPCALL(ippsSet_64s)(val,(LPIpp64s)dest,count); }
IPP_FUNC void memset64f( void *dest,double val,int count )
{  ASSERT(dest); IPPCALL(ippsSet_64f)(val,(LPIpp64f)dest,count); }

} // namespace ipp

namespace ipp
{
namespace _meta_
{	void _DumpIntelIppInformation(const IppLibraryVersion*, IppCpuType, int );
}} // namespace ipp/_meta_

///////////////////////////////////////////////////
// Dump Library version and misc
namespace ipp
{

IPP_FUNC void DumpIntelIppInformation()
{	_meta_::_DumpIntelIppInformation(IPPCALL(ippiGetLibVersion)(), ippGetCpuType(), ippGetNumCoresOnDie()); 
}

struct CIppiSize:public IppiSize
{
	CIppiSize(){}
	CIppiSize(const IppiSize&x){ width=x.width; height=x.height;  }
	CIppiSize(int w,int h){ width = w; height = h; }
	CIppiSize AddBorder(int border_x,int border_y) const
	{ return CIppiSize(width+border_x*2,height+border_y*2); }
	CIppiSize AddBorder(int border) const
	{ return AddBorder(border,border); }
};

struct CIppiPoint:public IppiPoint
{
	CIppiPoint(){}
	CIppiPoint(int ix,int iy){ x = ix; y = iy; }
};

struct CIppiRect:public IppiRect
{
	CIppiRect(){}
	CIppiRect(int ix,int iy,int w,int h){ x = ix; y = iy; width = w; height = h; }
	CIppiRect(const CIppiPoint& pt1,const CIppiPoint& pt2)
	{	if(pt1.x < pt2.x){ x=pt1.x; width=pt2.x-pt1.x; }
		else{ x=pt2.x; width=pt1.x-pt2.x; }
		if(pt1.y < pt2.y){ y=pt1.y; height=pt2.y-pt1.y; }
		else{ y=pt2.y; height=pt1.y-pt2.y; }
	}
};

/////////////////////////////////////////////////////////////////
// IppEnv
enum _tagIppEnvOption
{
	DitherMode_None = ippDitherNone,
	DitherMode_FS = ippDitherFS,
	DitherMode_JJN = ippDitherJJN,
	DitherMode_Stucki = ippDitherStucki,
	DitherMode_Bayer = ippDitherBayer,

	RoundMode_Zero = ippRndZero,
	RoundMode_Near = ippRndNear,
	
	HintAlgorithm_Default  = ippAlgHintNone,
	HintAlgorithm_Fast  = ippAlgHintFast,
	HintAlgorithm_Accurate  = ippAlgHintAccurate,

	InterpolationMode_Nearest = IPPI_INTER_NN,
	InterpolationMode_Linear = IPPI_INTER_LINEAR,
	InterpolationMode_Cubic = IPPI_INTER_CUBIC,
	InterpolationMode_Super = IPPI_INTER_SUPER,
	InterpolationMode_SmoothEdge = IPPI_SMOOTH_EDGE,

	BlendMethod_AlphaOver = ippAlphaOver,		//OVER	��(A)*A+[1-��(A)]*��(B)*B 
	BlendMethod_AlphaIn   = ippAlphaIn,			//IN	��(A)*A*��(B)
	BlendMethod_AlphaOut  = ippAlphaOut,		//OUT	��(A)*A*[1-��(B)]
	BlendMethod_AlphaATop = ippAlphaATop,		//ATOP	��(A)*A*��(B)+[1-��(A)]*��(B)*B 
	BlendMethod_AlphaXor  = ippAlphaXor,		//XOR	��(A)*A*[1-��(B)]+[1-��(A)]*��(B)*B 
	BlendMethod_AlphaPlus = ippAlphaPlus,		//PLUS	��(A)*A + ��(B)*B

	IppiOption_Max
};

class CIppiEnvParam
{	friend CIppiEnvParam * GetEnv();
public:
	IppiDitherType		DitherMode;
	IppRoundMode		RoundMode;
	IppHintAlgorithm	HintAlgorithm;
	DWORD				InterpolationMode;
	float				FloatMin;
	float				FloatMax;
	int					ResultBitShift;  // return result*2^(-ResultBitShift)
	int					IntegerScaleFactor;

protected:
	static CIppiEnvParam	g_IppEnv;
	LPBYTE					m_pEnvParamStack;
	int						m_StackPointer;

public:
	~CIppiEnvParam();
	CIppiEnvParam();
	void	Push();
	void	Pop();
};
IPP_FUNC CIppiEnvParam * GetEnv(){ return &CIppiEnvParam::g_IppEnv; }


namespace ipp_cpp
{
// Haar wavelet transform
IppStatus ippiHaarWTInv_C1R(LPCIpp8u pSrc,int srcStep, LPIpp8u pDst,int dstStep, IppiSize roi);
IppStatus ippiHaarWTFwd_C1R(LPCIpp8u pSrc,int srcStep, LPIpp8u pDst,int dstStep, IppiSize roi);
IppStatus ippiHaarWTInv_C1R(LPCIpp32f pSrc,int srcStep, LPIpp32f pDst,int dstStep, IppiSize roi);
IppStatus ippiHaarWTFwd_C1R(LPCIpp32f pSrc,int srcStep, LPIpp32f pDst,int dstStep, IppiSize roi);

}

} // namespace ipp

