#include "stdafx.h"
#include "ijl_jpeg.h"
#include <atlbase.h>
#include <atlconv.h>

using namespace rt;

//////////////////////////////////////////////////////////
// ipp::file::CJpegFile
ipp::file::CJpegFile::CJpegFile()
{
	VERIFY(ijlInit(&m_JcProps) == IJL_OK);
	m_JcProps.jquality = 95;
	m_JcProps.jprops.jpeg_comment = "";
	m_JcProps.jprops.jpeg_comment_size = 0;

	_SubsamplingMode = SubSampling_422;
}

BOOL ipp::file::CJpegFile::Encode(LPCTSTR Fn,LPCBYTE pData,int Channel,int Width,int Height,int Step)
{
	m_DecodedImageStep = 0;
	m_BufferUsedLen = 0;

	m_JcProps.DIBWidth = Width;
	m_JcProps.DIBHeight = Height;
	m_JcProps.DIBBytes = rt::_CastToNonconst(pData);
	if(Step)m_JcProps.DIBPadBytes = Step - Width*Channel;

	m_JcProps.DIBChannels = Channel;
	switch(Channel)
	{
	case 1:
		m_JcProps.DIBColor = IJL_G;
		m_JcProps.JPGColor = IJL_G;
		m_JcProps.JPGSubsampling = IJL_NONE;
		break;
	case 3:
		m_JcProps.DIBColor = IJL_RGB;
		m_JcProps.JPGColor = IJL_YCBCR;
		m_JcProps.JPGSubsampling = (IJL_JPGSUBSAMPLING)(_SubsamplingMode==IJL_NONE?IJL_NONE:_SubsamplingMode);
		break;
	case 4:
		m_JcProps.DIBColor = IJL_RGBA_FPX;
		m_JcProps.JPGColor = IJL_RGBA_FPX;
		m_JcProps.JPGSubsampling = (IJL_JPGSUBSAMPLING)(_SubsamplingMode==IJL_NONE?IJL_NONE:(_SubsamplingMode+2));
		break;
	default:
		return FALSE;
	}


	CT2A pfn(Fn);
	
	m_JcProps.JPGFile = pfn;
	m_JcProps.JPGWidth = Width;
	m_JcProps.JPGHeight = Height;
	m_JcProps.JPGChannels = Channel;

	return ijlWrite(&m_JcProps,IJL_JFILE_WRITEWHOLEIMAGE) == IJL_OK;
}


BOOL ipp::file::CJpegFile::Encode(LPCBYTE pData,int Channel,int Width,int Height,int Step)
{
	_SetBufferSize((max(Width*Channel,Step)*Height)/2);
	m_DecodedImageStep = 0;
	m_BufferUsedLen = 0;

	m_JcProps.DIBWidth = Width;
	m_JcProps.DIBHeight = Height;
	m_JcProps.DIBBytes = rt::_CastToNonconst(pData);
	if(Step)m_JcProps.DIBPadBytes = Step - Width*Channel;

	m_JcProps.DIBChannels = Channel;
	switch(Channel)
	{
	case 1:
		m_JcProps.JPGColor = IJL_G;
		m_JcProps.JPGSubsampling = IJL_NONE;
		m_JcProps.DIBColor = IJL_G;
		break;
	case 3:
		m_JcProps.JPGColor = IJL_YCBCR;
		m_JcProps.JPGSubsampling = (IJL_JPGSUBSAMPLING)(_SubsamplingMode==IJL_NONE?IJL_NONE:_SubsamplingMode);
		m_JcProps.DIBColor = IJL_RGB;
		break;
	case 4:
		m_JcProps.JPGColor = IJL_RGBA_FPX;
		m_JcProps.JPGSubsampling = (IJL_JPGSUBSAMPLING)(_SubsamplingMode==IJL_NONE?IJL_NONE:(_SubsamplingMode+2));
		m_JcProps.DIBColor = IJL_RGBA_FPX;
		break;
	default:
		return FALSE;
	}

	m_JcProps.JPGFile = NULL;
	m_JcProps.JPGBytes = m_TempBuffer;
	m_JcProps.JPGSizeBytes = m_TempBuffer.GetSize();

	m_JcProps.JPGWidth = Width;
	m_JcProps.JPGHeight = Height;
	m_JcProps.JPGChannels = Channel;

	if(ijlWrite(&m_JcProps,IJL_JBUFF_WRITEWHOLEIMAGE) == IJL_OK)
	m_BufferUsedLen = m_JcProps.JPGSizeBytes;

	return TRUE;
}

BOOL ipp::file::CJpegFile::Decode_HeaderOnly(LPCBYTE pDataJpeg,UINT DataLen)
{
	m_DecodedImageStep = 0;
	m_BufferUsedLen = 0;

	m_JcProps.JPGFile = NULL;
	m_JcProps.JPGBytes = rt::_CastToNonconst(pDataJpeg);
	m_JcProps.JPGSizeBytes = DataLen;
	if(ijlRead(&m_JcProps, IJL_JBUFF_READPARAMS)!=IJL_OK)return FALSE;

	m_DecodedImageWidth = m_JcProps.JPGWidth;
	m_DecodedImageHeight = m_JcProps.JPGHeight;
	m_DecodedImageChannel = m_JcProps.JPGChannels;
	m_DecodedImageStep = m_DecodedImageWidth*m_DecodedImageChannel;
	if( m_DecodedImageStep%4 )m_DecodedImageStep = ((m_DecodedImageStep>>2)+1)<<2;

	return TRUE;
}

BOOL ipp::file::CJpegFile::Decode(LPCBYTE pDataJpeg,UINT DataLen,LPBYTE pImageOut,int Step)
{
	m_DecodedImageStep = 0;
	m_BufferUsedLen = 0;

	m_JcProps.JPGFile = NULL;
	m_JcProps.JPGBytes = rt::_CastToNonconst(pDataJpeg);
	m_JcProps.JPGSizeBytes = DataLen;
	if(ijlRead(&m_JcProps, IJL_JBUFF_READPARAMS)!=IJL_OK)return FALSE;

	m_DecodedImageWidth = m_JcProps.JPGWidth;
	m_DecodedImageHeight = m_JcProps.JPGHeight;
	m_DecodedImageChannel = m_JcProps.JPGChannels;

	m_DecodedImageStep = m_DecodedImageWidth*m_DecodedImageChannel;
	if( m_DecodedImageStep%4 )m_DecodedImageStep = ((m_DecodedImageStep>>2)+1)<<2;

	if( pImageOut )
	{
		if( Step )m_DecodedImageStep = Step;
		m_JcProps.DIBBytes = pImageOut;
	}
	else
	{
		_SetBufferSize(m_DecodedImageHeight*m_DecodedImageStep);
		m_JcProps.DIBBytes = m_TempBuffer;
	}

	switch(m_DecodedImageChannel)
	{
	case 1:
		m_JcProps.JPGColor = IJL_G;
		m_JcProps.DIBColor = IJL_G;
		break;
	case 3:
		m_JcProps.JPGColor = IJL_YCBCR;
		m_JcProps.DIBColor = IJL_RGB;
		break;
	case 4:
		m_JcProps.JPGColor = IJL_RGBA_FPX;
		m_JcProps.DIBColor = IJL_RGBA_FPX;
		break;
	default:
		return FALSE;
	}

	// Set up the info on the desired DIB properties.
	m_JcProps.DIBWidth = m_DecodedImageWidth;
	m_JcProps.DIBHeight = m_DecodedImageHeight; // Implies a bottom-up DIB.
	m_JcProps.DIBChannels = m_DecodedImageChannel;
	m_JcProps.DIBPadBytes = m_DecodedImageStep - m_DecodedImageWidth*m_DecodedImageChannel;

	// Now get the actual JPEG image data into the pixel buffer.
	if(ijlRead(&m_JcProps, IJL_JBUFF_READWHOLEIMAGE) == IJL_OK)
	{
		if( pImageOut )m_DecodedImageStep = 0;	//disable internal buffer output
		return TRUE;
	}
	return FALSE;
}

BOOL ipp::file::CJpegFile::Decode_HeaderOnly(LPCTSTR Fn)
{
	m_DecodedImageStep = 0;
	m_BufferUsedLen = 0;

	CT2A pfn(Fn);
	m_JcProps.JPGFile = pfn;
	if(ijlRead(&m_JcProps, IJL_JFILE_READPARAMS) != IJL_OK)return FALSE;
	
	// Set up local data.
	m_DecodedImageWidth = m_JcProps.JPGWidth;
	m_DecodedImageHeight = m_JcProps.JPGHeight;
	m_DecodedImageChannel = m_JcProps.JPGChannels;

	m_DecodedImageStep = m_DecodedImageWidth*m_DecodedImageChannel;
	if( m_DecodedImageStep%4 )m_DecodedImageStep = ((m_DecodedImageStep>>2)+1)<<2;

	return TRUE;
}


BOOL ipp::file::CJpegFile::Decode(LPCTSTR Fn,LPBYTE pImageOut,int Step)
{
	m_DecodedImageStep = 0;
	m_BufferUsedLen = 0;

	CT2A pfn(Fn);
	m_JcProps.JPGFile = pfn;
	if(ijlRead(&m_JcProps, IJL_JFILE_READPARAMS) != IJL_OK)return FALSE;
	
	// Set up local data.
	m_DecodedImageWidth = m_JcProps.JPGWidth;
	m_DecodedImageHeight = m_JcProps.JPGHeight;
	m_DecodedImageChannel = m_JcProps.JPGChannels;

	m_DecodedImageStep = m_DecodedImageWidth*m_DecodedImageChannel;
	if( m_DecodedImageStep%4 )m_DecodedImageStep = ((m_DecodedImageStep>>2)+1)<<2;
	_SetBufferSize(m_DecodedImageHeight*m_DecodedImageStep);

	// Set up the info on the desired DIB properties.
	m_JcProps.DIBWidth = m_DecodedImageWidth;
	m_JcProps.DIBHeight = m_DecodedImageHeight; // Implies a bottom-up DIB.
	m_JcProps.DIBChannels = m_DecodedImageChannel;

	if( pImageOut )
	{
		if( Step )m_DecodedImageStep = Step;
		m_JcProps.DIBBytes = pImageOut;
	}
	else
	{
		_SetBufferSize(m_DecodedImageHeight*m_DecodedImageStep);
		m_JcProps.DIBBytes = m_TempBuffer;
	}
	m_JcProps.DIBPadBytes = m_DecodedImageStep - m_DecodedImageWidth*m_DecodedImageChannel;


	switch(m_DecodedImageChannel)
	{
	case 1:
		{
			m_JcProps.JPGColor = IJL_G;
			m_JcProps.DIBColor = IJL_G;
		}
		break;
	case 3:
		{
			m_JcProps.JPGColor = IJL_YCBCR;
			m_JcProps.DIBColor = IJL_RGB;
		}
		break;
	case 4:
		{
			m_JcProps.JPGColor = IJL_RGBA_FPX;
			m_JcProps.DIBColor = IJL_RGBA_FPX;
		}
		break;
	default:
		return FALSE;
	}


	// Now get the actual JPEG image data into the pixel buffer.
	if( ijlRead(&m_JcProps, IJL_JFILE_READWHOLEIMAGE) == IJL_OK )
	{
		if( pImageOut )m_DecodedImageStep = 0;	//disable internal buffer output
		return TRUE;
	}
	return FALSE;
}



//////////////////////////////////////////////////////////
// ipp::file::CJpegSequence_Writer
ipp::file::CJpegSequence_Writer::CJpegSequence_Writer()
:m_BufferUpdated(TRUE)
{
	m_BufferUpdated.Reset();
	m_hEncodeThread = CreateThread(NULL,0,_EncodeThread,this,0,NULL);
	ASSERT(m_hEncodeThread);
}

ipp::file::CJpegSequence_Writer::~CJpegSequence_Writer()
{
	::TerminateThread(m_hEncodeThread,0);
	Clean();
}


DWORD ipp::file::CJpegSequence_Writer::_EncodeThread(LPVOID pThis)
{
	CJpegSequence_Writer * This = (CJpegSequence_Writer *)pThis;

	CJpegImage	item;
	CJpegFile jpeg;
	for(;;)
	{
		This->m_BufferUpdated.WaitSignal();
		
		BOOL EncodeOk;
		{EnterCCSBlock(This->m_IncomingBufferCCS);
		
			EncodeOk = jpeg.Encode(	This->m_IncomingImage,
									This->m_Channel,
									This->m_Width,
									This->m_Height,
									This->m_Step);
		}

		if( EncodeOk )
		{
			item.m_TimeStamp = This->m_IncomingTime;
			item.m_JpegDataSize = jpeg.GetOutputSize();
			item.m_pJpegData = new BYTE[item.m_JpegDataSize];
			ASSERT(item.m_pJpegData);

			memcpy(item.m_pJpegData,jpeg.GetOutput(),item.m_JpegDataSize);

			{
				EnterCCSBlock(This->m_JpegSeqCCS);
				This->m_JpegSeq.push_back(item);
			}
		}
	}
}

void ipp::file::CJpegSequence_Writer::Initialize(LPCBYTE pDataImage,int Channel,int Width,int Height,int Step)
{
	Clean();

	m_IncomingImage = pDataImage;
	m_Channel = Channel;
	m_Width = Width;
	m_Height = Height;
	m_Step = Step;

	m_Timer.Start();
}

BOOL  ipp::file::CJpegSequence_Writer::Save(LPCTSTR fn)
{
	ASSERT(fn);

	w32::CFile64	file;
	if(file.Open(fn,w32::CFile64::Normal_Write))
	{
		EnterCCSBlock(m_JpegSeqCCS);

		file.Write("JSEQ",4);

		int co = (int)m_JpegSeq.GetSize();

		file.Write(&co,sizeof(int));
		file.Seek(sizeof(ULONGLONG),w32::CFile64::Seek_Current);

		Buffer<ULONGLONG>	FileOffset;
		Buffer<double>		FileTimestamp;

		FileOffset.SetSize((UINT)m_JpegSeq.GetSize());
		FileTimestamp.SetSize((UINT)m_JpegSeq.GetSize());

		for(UINT i=0;i<FileOffset.GetSize();i++)
		{
			FileOffset[i] = file.GetCurrentPosition();
			FileTimestamp[i] = m_JpegSeq[i].m_TimeStamp;

			if(file.Write(m_JpegSeq[i].m_pJpegData,m_JpegSeq[i].m_JpegDataSize) == m_JpegSeq[i].m_JpegDataSize){}
			else{ return FALSE; }
		}

		ULONGLONG TableOffset = file.GetCurrentPosition();
		if(file.Write(FileTimestamp.Begin(),sizeof(double)*FileTimestamp.GetSize())!=sizeof(double)*FileTimestamp.GetSize())
			return FALSE;

		if(file.Write(FileOffset.Begin(),sizeof(double)*FileOffset.GetSize())!=sizeof(double)*FileOffset.GetSize())
			return FALSE;

		file.Seek(4 + sizeof(int));
		file.Write(&TableOffset,sizeof(ULONGLONG));

		return TRUE;
	}

	return FALSE;
}

BOOL ipp::file::CJpegSequence_Writer::WriteFrame_Begin(BOOL Wait)
{
	double t = m_Timer.Snapshot();
	m_FrameSubmitted++;

	if( Wait )
	{
		m_IncomingBufferCCS.Lock();
	}
	else
	{
		if(m_IncomingBufferCCS.TryLock()){}
		else{ return FALSE; }
	}

	m_IncomingTime = t;
	return TRUE;
}

void ipp::file::CJpegSequence_Writer::WriteFrame_End()
{
	m_IncomingBufferCCS.Unlock();
	m_BufferUpdated.Set();
}

void ipp::file::CJpegSequence_Writer::Clean()
{
	EnterCCSBlock(m_JpegSeqCCS);

	for(UINT i=0;i<m_JpegSeq.GetSize();i++)
	{
		_SafeDelArray( m_JpegSeq[i].m_pJpegData );
	}

	m_JpegSeq.SetSize(0);
	m_FrameSubmitted = 0;
}

BOOL ipp::file::CJpegSequence_Reader::Load(LPCTSTR fn)
{
	if(m_SeqFile.IsOpen())m_SeqFile.Close();
	m_LastIndex = -1;

	if(m_SeqFile.Open(fn))
	{
		int co=0;
		m_SeqFile.Read(&co,4);
		if(co == 0x5145534a) // 0x5145534a == "JSEQ"
		{
			m_SeqFile.Read(&co,sizeof(int));
			m_FrameOffset.SetSize(co);
			m_FrameTimestamp.SetSize(co);

			if( m_FrameTimestamp.Begin() && m_FrameOffset.Begin() )
			{
				ULONGLONG offset;
				m_SeqFile.Read(&offset,sizeof(ULONGLONG));
				if(m_SeqFile.Seek(offset)==offset)
				{
					if( m_SeqFile.Read(m_FrameTimestamp.Begin(),sizeof(double)*co) != sizeof(double)*co )return FALSE;
					if( m_SeqFile.Read(m_FrameOffset.Begin(),sizeof(double)*co) != sizeof(double)*co )return FALSE;

					m_FramePreSec = (float)(m_FrameTimestamp.GetSize()*1000.0/(m_FrameTimestamp[m_FrameTimestamp.GetSize()-1] - m_FrameTimestamp[0]));
					m_FrameSize.SetSize(co);

					for(int i=0;i<co-1;i++)
					{
						m_FrameSize[i] = (UINT)(m_FrameOffset[i+1] - m_FrameOffset[i]);
					}
					m_FrameSize[co-1] = (UINT)(offset - m_FrameOffset[co-1]);

					return TRUE;
				}
			}
		}	
		else
		{
			m_SeqFile.Close();
		}
	}

	return FALSE;
}

BOOL ipp::file::CJpegSequence_Reader::ReadFrame(int index,LPBYTE pImage,int step)
{
	ASSERT(m_SeqFile.IsOpen());
	ASSERT((UINT)index < GetFrameCount());

	if( index == m_LastIndex )
	{
		return m_Codec.Decode(m_TempBuffer.Begin(),m_FrameSize[index],pImage,step);
	}
	else
	{
		m_LastIndex = -1;
		if( m_SeqFile.Seek(m_FrameOffset[index]) == m_FrameOffset[index] )
		{
			_SetBufferSize(m_FrameSize[index]);

			if( m_SeqFile.Read(m_TempBuffer.Begin(),m_FrameSize[index]) == m_FrameSize[index] )
			{
				m_LastIndex = index;
				return m_Codec.Decode(m_TempBuffer.Begin(),m_FrameSize[index],pImage,step);
			}
		}
	}

	return FALSE;
}

BOOL ipp::file::CJpegSequence_Reader::ReadFrame_HeaderOnly(int index)
{
	ASSERT(m_SeqFile.IsOpen());
	ASSERT((UINT)index < GetFrameCount());

	if( index == m_LastIndex )
	{
		return m_Codec.Decode_HeaderOnly(m_TempBuffer.Begin(),m_FrameSize[index]);
	}
	else
	{
		m_LastIndex = -1;
		if( m_SeqFile.Seek(m_FrameOffset[index]) == m_FrameOffset[index] )
		{
			_SetBufferSize(m_FrameSize[index]);

			if( m_SeqFile.Read(m_TempBuffer.Begin(),m_FrameSize[index]) == m_FrameSize[index] )
			{
				m_LastIndex = index;
				return m_Codec.Decode_HeaderOnly(m_TempBuffer.Begin(),m_FrameSize[index]);
			}
		}
	}

	return FALSE;
}

void ipp::file::CJpegSequence_Reader::_SetBufferSize(int size)
{
	m_TempBuffer.SetSize(max((UINT)size,m_TempBuffer.GetSize())); 
}

