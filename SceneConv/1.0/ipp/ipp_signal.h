#pragma once

//////////////////////////////////////////////////////////////////////
// Visual Computing Foundation Classes (VCFC)
//					Jiaping Wang  2006.2
//					e_boris2002@hotmail.com
//
// (C) Copyright Jiaping Wang 2004.
//
// Permission to copy, use, modify, sell and distribute this software
// is granted provided this copyright notice appears in all copies.
// This software is provided "as is" without express or implied
// warranty, and with no claim as to its suitability for any purpose.
//
// Absolutely no warranty!!
//////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////
//  Brief Description
//
//  ipp_signal.h
//
//  instantiations of Buffer for ipp signal library
//  macros:
//	control the accuracy of fixed-accuracy arithmetic functions
//  IPPS_FAAF_ACCURACY_HIGH	/ IPPS_FAAF_ACCURACY_LOW
//
//////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////
// Revise History
//
// Inital version	2006.6.9		Jiaping
//
//////////////////////////////////////////////////////////////////////

#include "ipp_basic.h"
#include "inc\ipps_cpp.h"
#include "..\num\small_vec.h"


namespace ipp
{

namespace _meta_
{

// deduce element type for ipps
template<typename t_Val, 
		 typename t_fund = typename ::rt::FundamentalType<t_Val>::t_Result,
		 int channel = sizeof(t_Val)/sizeof(t_fund) >
struct _DeduceIppsType;
template<typename t_Val, typename t_ipps>
struct _DeduceIppsType<t_Val,t_ipps,1>{ typedef t_ipps t_Result; };

template<typename t_Val> struct _DeduceIppsType<t_Val,Ipp8s ,2>{ typedef Ipp8sc 	t_Result; };
template<typename t_Val> struct _DeduceIppsType<t_Val,Ipp16s,2>{ typedef Ipp16sc	t_Result; };
template<typename t_Val> struct _DeduceIppsType<t_Val,Ipp32s,2>{ typedef Ipp32sc	t_Result; };
template<typename t_Val> struct _DeduceIppsType<t_Val,Ipp32f,2>{ typedef Ipp32fc	t_Result; };
template<typename t_Val> struct _DeduceIppsType<t_Val,Ipp64s,2>{ typedef Ipp64sc	t_Result; };
template<typename t_Val> struct _DeduceIppsType<t_Val,Ipp64f,2>{ typedef Ipp64fc	t_Result; };

	

// BaseVectorRoot
template<typename t_Val, typename SIZETYPE>
class _BaseIppSignalRoot
{
private:
	DYNTYPE_FUNC void __SafeFree(){ if(!_p)return; IPPCALL(ippsFree)(_p); }
public:
	typedef SIZETYPE t_SizeType;
	DYNTYPE_FUNC _BaseIppSignalRoot(){ _p=NULL; _len=0; }
	DYNTYPE_FUNC ~_BaseIppSignalRoot(){ __SafeFree(); }
	DYNTYPE_FUNC explicit _BaseIppSignalRoot(const _BaseIppSignalRoot &x){ ASSERT_STATIC(0); }	//copy ctor should be avoided, use reference for function parameters
	DYNTYPE_FUNC BOOL SetSize(SIZETYPE co=0) //zero for clear
	{	ASSERT(co>=0);
		if(co == _len){ return TRUE; }
		else
		{	__SafeFree();
			_len = co;
			if(co)
			{	switch(sizeof(t_Val)*8)
				{
				case 8:  // 1c8u
					_p = (t_Val*)IPPCALL(ippsMalloc_8u)(co); break;
				case 16: // 2c8u,1c16u
					_p = (t_Val*)IPPCALL(ippsMalloc_16u)(co); break;
				case 32: // 2c16u,1c32f,1c32s
					_p = (t_Val*)IPPCALL(ippsMalloc_32f)(co); break;
				case 64: // 2c32f,1c64f
					_p = (t_Val*)IPPCALL(ippsMalloc_64f)(co); break;
				case 128:// 2c64f
					_p = (t_Val*)IPPCALL(ippsMalloc_64fc)(co); break;
				default:
					ASSERT(0); //unsupported content format
				}
				
				if( _p ){ _len = co; return TRUE; }
				else{ _len = 0; return FALSE; }
			}
		}
		return TRUE;
	}
	DYNTYPE_FUNC BOOL ChangeSize(SIZETYPE new_size)
	{	if( new_size == _len )return TRUE;
		return FALSE;
	}

	//TypeTraits
	TYPETRAITS_DECL_LENGTH(TYPETRAITS_SIZE_UNKNOWN)	
	TYPETRAITS_DECL_IS_AGGREGATE(false)

	template<typename T>
	DYNTYPE_FUNC t_Val & At(const T index){ return _p[(int)index]; }
	template<typename T>
	DYNTYPE_FUNC const t_Val & At(const T index)const { return _p[(int)index]; }
	DYNTYPE_FUNC UINT GetSize() const{ return _len; }
protected:
	t_Val*		_p;
	SIZETYPE	_len;
};
} // namespace _meta_
template<typename SIZETYPE>
class _BaseIppSignal
{public:	template<typename t_Ele> class SpecifyItemType
			{public: typedef _meta_::_BaseIppSignalRoot<t_Ele, SIZETYPE> t_BaseVec; };
			static const bool IsCompactLinear = true;
};

#define IPPARG_SIG(x)	((x).GetSignalData())

template<typename t_Val,int channel=1>
class CSignal_Ref;

template<typename t_Value,UINT channel=1,class t_BaseVec=_BaseIppSignal<SIZE_T> >
class CSignal:public rt::Buffer<typename num::SpecifySmallVec<t_Value,channel>::t_Result,t_BaseVec>
{	
public:
	static const int chan_num = channel;
	TYPETRAITS_DECL_EXTENDTYPEID((rt::_typeid_codelib<<16)+12)
	IPP_FUNC static UINT  GetChannels(){ return chan_num;}
	IPP_FUNC static UINT  GetBPV(){ return (sizeof(t_Val)/chan_num)<<3;}

public:
	COMMON_CONSTRUCTOR_VEC(CSignal)
#pragma warning(disable:4244)
	ASSIGN_OPERATOR_VEC(CSignal)
#pragma warning(default:4244)
	DEFAULT_TYPENAME(CSignal)

protected:
	typedef typename num::SpecifySmallVec<t_Value,channel>::t_Result t_Val;
public:
	typedef typename rt::TypeTraits<t_Val>	t_PixelTR;
	typedef typename t_PixelTR::t_Accum		t_AccumPixel;

	typedef typename FundamentalItemType	ValueType;
	typedef typename ValueType*				LPValueType;
	typedef typename const ValueType*		LPCValueType;

	typedef typename t_Val					PixelType;
	typedef typename PixelType*				LPPixelType;
	typedef typename const PixelType*		LPCPixelType;

	typedef typename t_AccumPixel			AccumPixelType;
	typedef typename t_AccumPixel*			LPAccumPixelType;
	typedef typename const t_AccumPixel*	LPCAccumPixelType;

	typedef typename _meta_::_DeduceIppsType<PixelType>::t_Result	t_IppsType;
	t_IppsType* GetSignalData(){ return (t_IppsType*)Begin(); }
	const t_IppsType* GetSignalData() const{ return (const t_IppsType*)Begin(); }

public:
	typedef CSignal_Ref<t_Value,channel> Ref;
	typedef CSignal_Ref<t_Value,channel> Signal_Ref;
	IPP_FUNC operator Ref& (){ return *((Ref*)this); }
	IPP_FUNC operator const Ref& ()const{ return *((Ref*)this); }

	IPP_FUNC Ref GetRef(UINT x,UINT len)
	{	ASSERT(x+len<=GetSize());
		return Ref(GetPixelAddress(x),len); 
	}
	IPP_FUNC const Ref GetRef(UINT x,UINT len) const
	{	return rt::_CastToNonconst(this)->GetSubSignal(x,len); }
	
	IPP_FUNC Ref GetRef_Inside(UINT border)
	{	ASSERT(border*2<GetLength());
		return GetSubSignal(border,GetLength()-border*2);
	}

	IPP_FUNC LPVOID		GetBits(){ return &At(0);}
	IPP_FUNC LPCVOID	GetBits() const{ return &At(0);}
	IPP_FUNC LPVOID		GetBitsAt(UINT x){ return &At(x); }
	IPP_FUNC LPCVOID	GetBitsAt(UINT x) const{ return &At(x); }

	IPP_FUNC operator	LPValueType(){ return (LPValueType)GetBits(); }
	IPP_FUNC operator	LPCValueType() const{ return (LPCValueType)GetBits(); }

	IPP_FUNC LPPixelType	GetPixelAddress(UINT x=0){ return &At(x); }
	IPP_FUNC LPCPixelType	GetPixelAddress(UINT x=0) const{ return &At(x); }

	IPP_FUNC PixelType&			GetPixel(UINT x=0){ return At(x); }
	IPP_FUNC const PixelType&	GetPixel(UINT x=0) const{ return At(x); }

public:
	IPP_FUNC UINT	GetBPP() const{ return sizeof(t_Val)<<3;}
	IPP_FUNC UINT	GetLength() const{ return _len; }	
	IPP_FUNC BOOL	IsNull() const{return !(_p);}
	IPP_FUNC BOOL	IsNotNull() const{return (BOOL)_p;}

public:
	template<class PixelType>
	void GetInterpolatedPixel(float u,typename PixelType& p) const
	{	ASSERT(u>-EPSILON && u<=1.0f+EPSILON);
		float r_x = u*(GetLength()-1);
		int	i_x = r_x;
		p.Blend(GetPixel(i_x),GetPixel(i_x+1),r_x-i_x);
	}

public:
	/////////////////////////////////////////////////////////////////////////////////////////////
	// Mapped IPPS functions	
	IPP_FUNC void SortAscend(){ ipp_cpp::ippsSortAscend_I(IPPARG_SIG(*this),GetSize()); }
	IPP_FUNC void Pow(t_Value idx,Signal_Ref &d) const
	{	ASSERT(GetSize()==d.GetSize());
		ipp_cpp::ippsPow(IPPARG_SIG(*this),idx,IPPARG_SIG(d),GetSize());
	}
	IPP_FUNC void Pow(t_Value idx)
	{	ipp_cpp::ippsPow(IPPARG_SIG(*this),idx,IPPARG_SIG(*this),GetSize()); }

	// End of Mapped IPPS functions
	///////////////////////////////////////////////////////////////////////////////////////
};

/////////////////////////////////////////////////////////////
// Class for linear and compact vector reference/argument
template< typename t_Val,int channel >
class CSignal_Ref:public ipp::CSignal<t_Val,channel,rt::_BaseVecRef<UINT>>
{
protected:
	CSignal_Ref(){ ASSERT_STATIC(0); }
public:
	DYNTYPE_FUNC CSignal_Ref(t_Val* ptr,UINT len){ _p=ptr; _len=len; }
	DYNTYPE_FUNC CSignal_Ref(ValueType* ptr,UINT len){ _p=(t_Val*)ptr; _len=len; }
	CSignal_Ref(const CSignal_Ref& x){ ASSERT_STATIC(0); }	//The two ASSERTs fired when trying to construct 
	//a Ref from a Ref instance or an empty instance which is not allowed. Avoid trying to copy a Ref
	//instance, it is highly recommended to use Ref& instead  for local using and function arguments

#ifdef __INTEL_COMPILER
#pragma warning(disable: 525) //warning #525: type "xxx" is an inaccessible type (allowed for compatibility)
#endif
	typedef __super::Ref Ref;
#ifdef __INTEL_COMPILER
#pragma warning(default: 525)
#endif
	
public:
#pragma warning(disable:4244)
	ASSIGN_OPERATOR_VEC(CSignal_Ref)
#pragma warning(default:4244)
	DEFAULT_TYPENAME(CSignal_Ref)
};


typedef CSignal<float,1>	CSignal_1c32f;
typedef CSignal<BYTE,1>		CSignal_1c8u;

} // namespace ipp

