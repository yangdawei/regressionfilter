#pragma once

//////////////////////////////////////////////////////////////////////
// Visual Computing Foundation Classes (VCFC)
//					Jiaping Wang  2006.2
//					e_boris2002@hotmail.com
//
// (C) Copyright Jiaping Wang 2004.
//
// Permission to copy, use, modify, sell and distribute this software
// is granted provided this copyright notice appears in all copies.
// This software is provided "as is" without express or implied
// warranty, and with no claim as to its suitability for any purpose.
//
// Absolutely no warranty!!
//////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////
//  Brief Description
//
//  ipp_crypto.h
//
//	Cryptography Primitive Functions
//		Random Number Generator
//		Symmetric Cryptography
//		One-Way Hash Functions
//		Keyed Hash Functions
//
//  Cryptography's headers and libs are not shipped with IPP library, it 
//	should be applied and downloaded separately from Intel's website due
//	to USA's exporting policy. Registration is required.
//
//////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////
// Revise History
//
// Inital version	2009.7.12		Jiaping
//
//////////////////////////////////////////////////////////////////////

#include "ipp_basic.h"
#include "..\num\small_vec.h"

#ifndef _MAKING_STATIC_LIB_

#ifdef _WIN64
	#ifdef IPP_LINK_STATIC
		#pragma comment(lib,"ippcpmergedem64t.lib")
	#else
		#pragma comment(lib,"ippcpem64t.lib")
	#endif

#else
	#ifdef IPP_LINK_STATIC
		#pragma comment(lib,"ippcpmerged.lib")
	#else
		#pragma comment(lib,"ippcp.lib")
	#endif
#endif

#endif // _MAKING_STATIC_LIB_

namespace ipp
{

#define IPPCALL2(x)	IPPCALL(x)
//////////////////////////////////////////////////////
// Random Number Generator
template<typename t_Val> // for int
class RandomGen
{
protected:
	BYTE	m_Context[192];

public:
	RandomGen()
	{
		#ifdef _DEBUG
			int sz;
			IPPCALL(ippsPRNGGetSize)(&sz);
			ASSERT(sz<=sizeof(m_Context));
		#endif
		IPPCALL(ippsPRNGInit)(sizeof(t_Val)*8,(IppsPRNGState*)m_Context);
	}

	void SetSeed(t_Val seed)
	{
		BYTE	bn[64];
		int		bn_len = max(1,sizeof(t_Val)/sizeof(int));
		#ifdef _DEBUG
			int sz;
			IPPCALL(ippsBigNumGetSize)(bn_len,&sz);
			ASSERT(sizeof(bn)>=sz);
		#endif
		IPPCALL(ippsBigNumInit)(bn_len,(IppsBigNumState*)bn);
		IPPCALL(ippsSet_BN)(IppsBigNumPOS,bn_len,(Ipp32u*)&seed,(IppsBigNumState*)bn);
		IPPCALL(ippsPRNGSetSeed)((IppsBigNumState*)bn,(IppsPRNGState*)m_Context);
	}

	t_Val Generate()
	{
		rt::TypeTraits<t_Val>::t_Val	ret;
		IPPCALL(ippsPRNGen)((Ipp32u*)&ret,sizeof(t_Val)*8,(IppsPRNGState*)m_Context);
		return ret;
	}

	template<typename T>
	void Generate(T* x)
	{
		ASSERT(sizeof(T) >= sizeof(int));
		ASSERT((sizeof(T) & 3) == 0);

		IPPCALL(ippsPRNGen)((Ipp32u*)x,sizeof(T)*8,(IppsPRNGState*)m_Context);
	}
};

//////////////////////////////////////////////////////
// Symmetric Cryptography Primitive Functions
enum _tagCryptoMethod
{
	SC_DES,			// KeyLen = 7	BlockSize = 64
	SC_RIJNDAEL128,	// KeyLen = 16	BlockSize = 128
	SC_RIJNDAEL256,	// KeyLen = 32	BlockSize = 256
	SC_TWOFISH,		// KeyLen = *	BlockSize = 128
	SC_BLOWFISH,	// KeyLen = *	BlockSize = 64
	SC_RC5_64,		// KeyLen = *	BlockSize = 64
	SC_RC5_128		// KeyLen = *	BlockSize = 128
};

enum _tagChainMode
{
	SC_MODE_ECB,	// Electronic Code Book (ECB) mode
	SC_MODE_CBC,	// Cipher Block Chain (CBC) mode
	SC_MODE_CFB,	// Cipher Feedback (CFB) mode
	SC_MODE_OFB,	// Output Feedback (OFB) mode
	SC_MODE_CTR		// Counter (CTR) mode
};

namespace _meta_
{
template<UINT _METHOD>
struct	_get_cipher_align;
	template<> struct _get_cipher_align<SC_DES			>{ static const int Result = 64/8-1; };
	template<> struct _get_cipher_align<SC_RIJNDAEL128	>{ static const int Result = 128/8-1; };
	template<> struct _get_cipher_align<SC_RIJNDAEL256	>{ static const int Result = 256/8-1; };
	template<> struct _get_cipher_align<SC_TWOFISH		>{ static const int Result = 128/8-1; };
	template<> struct _get_cipher_align<SC_BLOWFISH		>{ static const int Result = 64/8-1; };
	template<> struct _get_cipher_align<SC_RC5_64		>{ static const int Result = 64/8-1; };
	template<> struct _get_cipher_align<SC_RC5_128		>{ static const int Result = 128/8-1; };

template<UINT _METHOD>
struct	_get_cipher_context_size;
	template<> struct _get_cipher_context_size<SC_DES			>{ static const int Result = 271;	};
	template<> struct _get_cipher_context_size<SC_RIJNDAEL128	>{ static const int Result = 551;	};
	template<> struct _get_cipher_context_size<SC_RIJNDAEL256	>{ static const int Result = 991;	};
	template<> struct _get_cipher_context_size<SC_TWOFISH		>{ static const int Result = 4263;	};
	template<> struct _get_cipher_context_size<SC_BLOWFISH		>{ static const int Result = 4179;	};
	template<> struct _get_cipher_context_size<SC_RC5_64		>{ static const int Result = 256;	};
	template<> struct _get_cipher_context_size<SC_RC5_128		>{ static const int Result = 512;   };

template<UINT _METHOD, bool _embed_context>
struct deduce_context_type;
	template<UINT _METHOD> struct deduce_context_type<_METHOD,true>
	{	typedef BYTE	t_Result[ _get_cipher_context_size<_METHOD>::Result ];	};
	template<UINT _METHOD> struct deduce_context_type<_METHOD,false>
	{	typedef rt::Buffer<BYTE> t_Result; };

} // namespace _meta_


template<UINT _METHOD = SC_TWOFISH, UINT _MODE = SC_MODE_CBC, bool _embed_context = false>
class SymCipher
{
	int				_RC5_round;
protected:
	typedef typename _meta_::deduce_context_type<_METHOD,_embed_context>::t_Result	CONTEXT_TYPE;
	CONTEXT_TYPE	m_Context;
	
public:
	static const UINT Align = _meta_::_get_cipher_align<_METHOD>::Result;

	SymCipher(int RC5_round = 2) // 
	{	int ctxlen = 0;
		_RC5_round = RC5_round;

		#define CALL_GETSIZE(method,function)			\
		__static_if(_METHOD == method){ IPPCALL(function)(&ctxlen); }

		CALL_GETSIZE(SC_DES,		ippsDESGetSize);
		CALL_GETSIZE(SC_RIJNDAEL128,ippsRijndael128GetSize);
		CALL_GETSIZE(SC_RIJNDAEL256,ippsRijndael256GetSize);
		CALL_GETSIZE(SC_TWOFISH,	ippsTwofishGetSize);
		CALL_GETSIZE(SC_BLOWFISH,	ippsBlowfishGetSize);
		__static_if(_METHOD == SC_RC5_64){ IPPCALL(ippsARCFive64GetSize)(RC5_round,&ctxlen); }
		__static_if(_METHOD == SC_RC5_128){ IPPCALL(ippsARCFive128GetSize)(RC5_round,&ctxlen); }

		#undef CALL_GETSIZE
		ASSERT(ctxlen);

		__static_if(_embed_context){	ASSERT(sizeof(m_Context) >= ctxlen); };
		__static_if(!_embed_context){	VERIFY(m_Context.SetSize(ctxlen));	 };
	}

	BOOL SetKey(LPCVOID pData, int len)
	{
		#define CALL_INIT(function,CTX_TYPE)		\
		{ return ippStsNoErr == IPPCALL(function)((LPCBYTE)pData,len, (CTX_TYPE*)((LPBYTE)m_Context)); }

		__static_if(_METHOD == SC_DES)
		{	ASSERT(len==7);
			return ippStsNoErr == IPPCALL(ippsDESInit)((LPCBYTE)pData,(IppsDESSpec*)((LPBYTE)m_Context));
		}
		__static_if(_METHOD == SC_RIJNDAEL128)
		{	ASSERT(len==16);
			return ippStsNoErr == IPPCALL(ippsRijndael128Init)((LPCBYTE)pData,(IppsRijndaelKeyLength)128,(IppsRijndael128Spec*)((LPBYTE)m_Context));
		}
		__static_if(_METHOD == SC_RIJNDAEL256)
		{	ASSERT(len==32);
			return ippStsNoErr == IPPCALL(ippsRijndael256Init)((LPCBYTE)pData,(IppsRijndaelKeyLength)256,(IppsRijndael256Spec*)((LPBYTE)m_Context));
		}
		__static_if(_METHOD == SC_TWOFISH)
		{	ASSERT(len<=32 && len>=16);
			CALL_INIT(ippsTwofishInit,	IppsTwofishSpec		);	
		}
		__static_if(_METHOD == SC_BLOWFISH)
		{	CALL_INIT(ippsBlowfishInit,	IppsBlowfishSpec	);	}
		__static_if(_METHOD == SC_RC5_64)
		{	
			return ippStsNoErr == IPPCALL(ippsARCFive64Init)((LPCBYTE)pData,len,_RC5_round,(IppsARCFive64Spec*)((LPBYTE)m_Context));
		}
		__static_if(_METHOD == SC_RC5_128)
		{	
			return ippStsNoErr == IPPCALL(ippsARCFive128Init)((LPCBYTE)pData,len,_RC5_round,(IppsARCFive128Spec*)((LPBYTE)m_Context));
		}
	
		#undef CALL_INIT
		ASSERT(0);
	}

	void Encrypt(LPCVOID pText, LPVOID pCrypt, int Len)
	{
		ASSERT(0 == (_meta_::_get_cipher_align<_METHOD>::Result & Len));

		#define CALL_ALL_MODES(method,function)				\
				__static_if(_METHOD == method){				\
					__static_if(_MODE == SC_MODE_ECB)		\
					{ IPPCALL2(ipps##function##EncryptECB)((LPCBYTE)pText,(LPBYTE)pCrypt,Len,(Ipps##function##Spec*)((LPBYTE)m_Context),IppsCPPaddingNONE); return; }	\
					__static_if(_MODE == SC_MODE_CBC)		\
					{ IPPCALL2(ipps##function##EncryptCBC)((LPCBYTE)pText,(LPBYTE)pCrypt,Len,(Ipps##function##Spec*)((LPBYTE)m_Context),IppsCPPaddingNONE); return; }	\
					__static_if(_MODE == SC_MODE_CFB)		\
					{ IPPCALL2(ipps##function##EncryptCFB)((LPCBYTE)pText,(LPBYTE)pCrypt,Len,(Ipps##function##Spec*)((LPBYTE)m_Context),IppsCPPaddingNONE); return; }	\
					__static_if(_MODE == SC_MODE_OFB)		\
					{ IPPCALL2(ipps##function##EncryptOFB)((LPCBYTE)pText,(LPBYTE)pCrypt,Len,(Ipps##function##Spec*)((LPBYTE)m_Context),IppsCPPaddingNONE); return; }	\
					__static_if(_MODE == SC_MODE_CTR)		\
					{ IPPCALL2(ipps##function##EncryptCTR)((LPCBYTE)pText,(LPBYTE)pCrypt,Len,(Ipps##function##Spec*)((LPBYTE)m_Context),IppsCPPaddingNONE); return; }	\
				}

		CALL_ALL_MODES(SC_DES,			DES			);
		CALL_ALL_MODES(SC_RIJNDAEL128,	Rijndael128	);
		CALL_ALL_MODES(SC_RIJNDAEL256,	Rijndael256	);
		CALL_ALL_MODES(SC_TWOFISH,		Twofish		);
		CALL_ALL_MODES(SC_BLOWFISH,		Blowfish	);
		CALL_ALL_MODES(SC_RC5_64,		ARCFive64	);
		CALL_ALL_MODES(SC_RC5_128,		ARCFive128	);

		#undef CALL_ALL_MODES
		ASSERT(0);
	}

	void Decrypt(LPCVOID pCrypt, LPVOID pText, int Len)
	{
		ASSERT(0 == (_meta_::_get_cipher_align<_METHOD>::Result & Len));

		#define CALL_ALL_MODES(method,function)				\
				__static_if(_METHOD == method){				\
					__static_if(_MODE == SC_MODE_ECB)		\
					{ IPPCALL2(ipps##function##DecryptECB)((LPCBYTE)pCrypt,(LPBYTE)pText,Len,(Ipps##function##Spec*)((LPBYTE)m_Context),IppsCPPaddingNONE); return; }	\
					__static_if(_MODE == SC_MODE_CBC)		\
					{ IPPCALL2(ipps##function##DecryptCBC)((LPCBYTE)pCrypt,(LPBYTE)pText,Len,(Ipps##function##Spec*)((LPBYTE)m_Context),IppsCPPaddingNONE); return; }	\
					__static_if(_MODE == SC_MODE_CFB)		\
					{ IPPCALL2(ipps##function##DecryptCFB)((LPCBYTE)pCrypt,(LPBYTE)pText,Len,(Ipps##function##Spec*)((LPBYTE)m_Context),IppsCPPaddingNONE); return; }	\
					__static_if(_MODE == SC_MODE_OFB)		\
					{ IPPCALL2(ipps##function##DecryptOFB)((LPCBYTE)pCrypt,(LPBYTE)pText,Len,(Ipps##function##Spec*)((LPBYTE)m_Context),IppsCPPaddingNONE); return; }	\
					__static_if(_MODE == SC_MODE_CTR)		\
					{ IPPCALL2(ipps##function##DecryptCTR)((LPCBYTE)pCrypt,(LPBYTE)pText,Len,(Ipps##function##Spec*)((LPBYTE)m_Context),IppsCPPaddingNONE); return; }	\
				}

		CALL_ALL_MODES(SC_DES,			DES			);
		CALL_ALL_MODES(SC_RIJNDAEL128,	Rijndael128	);
		CALL_ALL_MODES(SC_RIJNDAEL256,	Rijndael256	);
		CALL_ALL_MODES(SC_TWOFISH,		Twofish		);
		CALL_ALL_MODES(SC_BLOWFISH,		Blowfish	);
		CALL_ALL_MODES(SC_RC5_64,		ARCFive64	);
		CALL_ALL_MODES(SC_RC5_128,		ARCFive128	);

		#undef CALL_ALL_MODES
		ASSERT(0);
	}

	static BOOL Decrypt(LPCVOID key, int key_len, LPCVOID pCrypt, LPVOID pText, int len)
	{
		SymCipher	_cipher;
		if(_cipher.SetKey(key,key_len)){ _cipher.Decrypt(pCrypt,pText,len); return TRUE; }
		else return FALSE;
	}
	static BOOL Encrypt(LPCVOID key, int key_len, LPCVOID pText, LPVOID pCrypt, int len)
	{
		SymCipher	_cipher;
		if(_cipher.SetKey(key,key_len)){ _cipher.Encrypt(pText,pCrypt,len); return TRUE; }
		else return FALSE;
	}
};

}	// namespace ipp




namespace ipp
{

////////////////////////////////////////////////
// One-Way Hash Functions and Keyed Hash Functions 
enum _tagHashFunc
{
	HASH_MD5=0,
	HASH_SHA1,
	HASH_SHA224,
	HASH_SHA256,
	HASH_SHA384,
	HASH_SHA512,
};


namespace _meta_
{	
template<UINT _METHOD>
struct _get_hash_context_size;
	template<> struct _get_hash_context_size<HASH_MD5>{ static const UINT Result = 103; };
	template<> struct _get_hash_context_size<HASH_SHA1>{ static const UINT Result = 111; };
	template<> struct _get_hash_context_size<HASH_SHA224>{ static const UINT Result = 119; };
	template<> struct _get_hash_context_size<HASH_SHA256>{ static const UINT Result = 119; };
	template<> struct _get_hash_context_size<HASH_SHA384>{ static const UINT Result = 239; };
	template<> struct _get_hash_context_size<HASH_SHA512>{ static const UINT Result = 239; };

template<UINT _METHOD>
struct _get_hash_bits;
	template<> struct _get_hash_bits<HASH_MD5>{ static const UINT Result = 128; };
	template<> struct _get_hash_bits<HASH_SHA1>{ static const UINT Result = 160; };
	template<> struct _get_hash_bits<HASH_SHA224>{ static const UINT Result = 224; };
	template<> struct _get_hash_bits<HASH_SHA256>{ static const UINT Result = 256; };
	template<> struct _get_hash_bits<HASH_SHA384>{ static const UINT Result = 384; };
	template<> struct _get_hash_bits<HASH_SHA512>{ static const UINT Result = 512; };

template<UINT _METHOD>
struct _get_keyedhash_context_size;
	template<> struct _get_keyedhash_context_size<HASH_MD5>{ static const UINT Result = 243; };
	template<> struct _get_keyedhash_context_size<HASH_SHA1>{ static const UINT Result = 163; };
	template<> struct _get_keyedhash_context_size<HASH_SHA224>{ static const UINT Result = 259; };
	template<> struct _get_keyedhash_context_size<HASH_SHA256>{ static const UINT Result = 259; };
	template<> struct _get_keyedhash_context_size<HASH_SHA384>{ static const UINT Result = 507; };
	template<> struct _get_keyedhash_context_size<HASH_SHA512>{ static const UINT Result = 507; };

} // namespace _meta_
} // namespace ipp


namespace ipp
{

template<UINT _METHOD = HASH_MD5>
class OneWayHash
{
public:
	static const UINT HASH_CODE_SIZE = _meta_::_get_hash_bits<_METHOD>::Result/8;

protected:
	BYTE	m_Context[_meta_::_get_hash_context_size<_METHOD>::Result];
	BYTE	m_HashCode[HASH_CODE_SIZE];

public:
	OneWayHash()
	{	
#ifdef _DEBUG
		int sz;
		#define CALL(x,tag)	__static_if(_METHOD == tag)	\
				{	IPPCALL2(ipps##x##GetSize)(&sz); ASSERT(sz <= sizeof(m_Context));	\
					return;		}

		CALL(MD5,	HASH_MD5);
		CALL(SHA1,	HASH_SHA1);
		CALL(SHA224,HASH_SHA224);
		CALL(SHA256,HASH_SHA256);
		CALL(SHA384,HASH_SHA384);
		CALL(SHA512,HASH_SHA512);
		#undef CALL
		ASSERT(0);
#endif

		Reset();
	}

	void Reset()
	{
		#define CALL(x,tag)	__static_if(_METHOD == tag)	\
				{	IPPCALL2(ipps##x##Init)((Ipps##x##State*)m_Context); \
					return;		}

		CALL(MD5,	HASH_MD5);
		CALL(SHA1,	HASH_SHA1);
		CALL(SHA224,HASH_SHA224);
		CALL(SHA256,HASH_SHA256);
		CALL(SHA384,HASH_SHA384);
		CALL(SHA512,HASH_SHA512);
		#undef CALL

		ASSERT(0);
	}
	void Update(LPCVOID pData, int len)
	{	
		#define CALL(x,tag)	__static_if(_METHOD == tag)	\
		{	IPPCALL2(ipps##x##Update)((LPCBYTE)pData,len,(Ipps##x##State*)m_Context);	return; }

		CALL(MD5,	HASH_MD5);
		CALL(SHA1,	HASH_SHA1);
		CALL(SHA224,HASH_SHA224);
		CALL(SHA256,HASH_SHA256);
		CALL(SHA384,HASH_SHA384);
		CALL(SHA512,HASH_SHA512);
		#undef CALL

		ASSERT(0);
	}

	void Finalize()
	{
		#define CALL(x,tag)	__static_if(_METHOD == tag)	\
		{	IPPCALL2(ipps##x##Final)(m_HashCode,(Ipps##x##State*)m_Context);	return; }

		CALL(MD5,	HASH_MD5);
		CALL(SHA1,	HASH_SHA1);
		CALL(SHA224,HASH_SHA224);
		CALL(SHA256,HASH_SHA256);
		CALL(SHA384,HASH_SHA384);
		CALL(SHA512,HASH_SHA512);
		#undef CALL

		ASSERT(0);
	}

	LPCBYTE GetHashCode()const{ return m_HashCode; }
	UINT	GetHashCodeSize() const { return HASH_CODE_SIZE; }

	static BOOL GetHashCode(LPCBYTE pMsg, UINT MsgLen, LPBYTE pResult)
	{
		#define CALL(x,tag)	__static_if(_METHOD == tag)	\
		{	return ippStsNoErr == IPPCALL2(ipps##x##MessageDigest)(pMsg,MsgLen,pResult); }

		CALL(MD5,	HASH_MD5);
		CALL(SHA1,	HASH_SHA1);
		CALL(SHA224,HASH_SHA224);
		CALL(SHA256,HASH_SHA256);
		CALL(SHA384,HASH_SHA384);
		CALL(SHA512,HASH_SHA512);
		#undef CALL

		ASSERT(0);
	}
};

template<UINT _METHOD = HASH_MD5>
class OneWayHash_Keyed
{
public:
	static const UINT HASH_CODE_SIZE = _meta_::_get_hash_bits<_METHOD>::Result/8;

protected:
	BYTE	m_Context[_meta_::_get_keyedhash_context_size<_METHOD>::Result];
	BYTE	m_HashCode[HASH_CODE_SIZE];

public:
#ifdef _DEBUG
	OneWayHash_Keyed()
	{	int sz;
		#define CALL(x,tag)	__static_if(_METHOD == tag)	\
				{	IPPCALL2(ippsHMAC##x##GetSize)(&sz); ASSERT(sz <= sizeof(m_Context));	\
					return;		}

		CALL(MD5,	HASH_MD5);
		CALL(SHA1,	HASH_SHA1);
		CALL(SHA224,HASH_SHA224);
		CALL(SHA256,HASH_SHA256);
		CALL(SHA384,HASH_SHA384);
		CALL(SHA512,HASH_SHA512);
		#undef CALL

		ASSERT(0);
	}
#endif

	void SetKey(LPCVOID pKey, int len)
	{
		#define CALL(x,tag)	__static_if(_METHOD == tag)	\
				{	IPPCALL2(ippsHMAC##x##Init)((LPCBYTE)pKey,len,(IppsHMAC##x##State*)m_Context); return; }

		CALL(MD5,	HASH_MD5);
		CALL(SHA1,	HASH_SHA1);
		CALL(SHA224,HASH_SHA224);
		CALL(SHA256,HASH_SHA256);
		CALL(SHA384,HASH_SHA384);
		CALL(SHA512,HASH_SHA512);
		#undef CALL
		ASSERT(0);
	}

	void Update(LPCVOID pData, int len)
	{	
		#define CALL(x,tag)	__static_if(_METHOD == tag)	\
		{	IPPCALL2(ippsHMAC##x##Update)((LPCBYTE)pData,len,(IppsHMAC##x##State*)m_Context);	return; }

		CALL(MD5,	HASH_MD5);
		CALL(SHA1,	HASH_SHA1);
		CALL(SHA224,HASH_SHA224);
		CALL(SHA256,HASH_SHA256);
		CALL(SHA384,HASH_SHA384);
		CALL(SHA512,HASH_SHA512);
		#undef CALL

		ASSERT(0);
	}

	void Finalize()
	{
		#define CALL(x,tag)	__static_if(_METHOD == tag)	\
		{	IPPCALL2(ippsHMAC##x##Final)(m_HashCode,HASH_CODE_SIZE,(IppsHMAC##x##State*)m_Context);	return; }

		CALL(MD5,	HASH_MD5);
		CALL(SHA1,	HASH_SHA1);
		CALL(SHA224,HASH_SHA224);
		CALL(SHA256,HASH_SHA256);
		CALL(SHA384,HASH_SHA384);
		CALL(SHA512,HASH_SHA512);
		#undef CALL

		ASSERT(0);
	}

	LPCBYTE GetHashCode()const{ return m_HashCode; }
	UINT	GetHashCodeSize() const { return HASH_CODE_SIZE; }

	static void GetHashCode(BYTE HashCode[HASH_CODE_SIZE], LPCVOID pMsg, int msg_len, LPCVOID pKey, int key_len)
	{
		#define CALL(x,tag)	__static_if(_METHOD == tag)	\
		{	IPPCALL2(ippsHMAC##x##MessageDigest)((LPCBYTE)pMsg,msg_len,(LPCBYTE)pKey,key_len,HashCode,HASH_CODE_SIZE);	return; }

		CALL(MD5,	HASH_MD5);
		CALL(SHA1,	HASH_SHA1);
		CALL(SHA224,HASH_SHA224);
		CALL(SHA256,HASH_SHA256);
		CALL(SHA384,HASH_SHA384);
		CALL(SHA512,HASH_SHA512);
		#undef CALL

		ASSERT(0);
	}
};


#undef IPPCALL2
} // namespace ipp



