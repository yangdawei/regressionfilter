#pragma once

//////////////////////////////////////////////////////////////////////
// Visual Computing Foundation Classes (VCFC)
//					Jiaping Wang  2006.2
//					e_boris2002@hotmail.com
//
// (C) Copyright Jiaping Wang 2004.
//
// Permission to copy, use, modify, sell and distribute this software
// is granted provided this copyright notice appears in all copies.
// This software is provided "as is" without express or implied
// warranty, and with no claim as to its suitability for any purpose.
//
// Absolutely no warranty!!
//////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////
//  Brief Description
//
//  ipp_image.h
//
//  instantiations of Image for ipp image library
//
//////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////
// Revise History
//
// Inital version	2006.6.9		Jiaping
//
//////////////////////////////////////////////////////////////////////

#include "..\rt\compact_image.h"
#include "..\num\small_vec.h"
#include "..\num\tabulate_func.h"
#include "..\w32\w32_misc.h"

#include "ipp_basic.h"
#include "ipp_signal.h"

#include "inc\ippi_cpp.h"


#include <atlimage.h>

#ifndef _MAKING_STATIC_LIB_
#pragma comment(lib,"gdiplus.lib")
#endif

#define INCLUDE_IPP_IMAGE

namespace ipp
{

enum _tagImageCodec
{
	ImageCodec_PNG = 0,
	ImageCodec_JPG,
	ImageCodec_BMP,
	ImageCodec_GIF,
	ImageCodec_ICON
};

//////////////////////////////////////////////////////////////////////
// storage type for IPP Images
namespace _meta_
{
template<typename t_Val>
class _BaseIppImageRoot
{	//Element requirment: Aggregate, 0/1d array, channel<=4, BPV<=32
	ASSERT_STATIC(rt::TypeTraits<t_Val>::IsAggregate);	
	ASSERT_STATIC(rt::TypeDimension<t_Val>::Result<=1);
	ASSERT_STATIC(rt::TypeTraits<t_Val>::Length<=4);
protected:
	typedef typename rt::FundamentalType<t_Val>::t_Result	_ValueType;
	static const int _Channel = rt::TypeTraits<t_Val>::Length;
	static const int _BPV = sizeof(_ValueType);

protected:
	IPP_FUNC _BaseIppImageRoot(const _BaseIppImageRoot &x){ ASSERT_STATIC(0); }
public:
	IPP_FUNC _BaseIppImageRoot(){	lpData=NULL; width=0; height=0; Pad_Bytes=0; }
	IPP_FUNC ~_BaseIppImageRoot(){ __SafeFree(); }
	IPP_FUNC BOOL SetSize(UINT co=0){ return (co==GetSize())?TRUE:FALSE; }
	IPP_FUNC BOOL ChangeSize(UINT new_size){ return (new_size==GetSize())?TRUE:FALSE; }

	//TypeTraits
	TYPETRAITS_DECL_LENGTH(TYPETRAITS_SIZE_UNKNOWN)
	TYPETRAITS_DECL_IS_AGGREGATE(false)

	//template<typename T>
	//IPP_FUNC t_Val & At(const T index)
	//{ return *(t_Val*)((LPBYTE)lpData + (index/width)*Pad_Bytes + sizeof(t_Val)*index); }
	//template<typename T>
	//IPP_FUNC const t_Val & At(const T index)const
	//{ return *(const t_Val*)((LPCBYTE)lpData + (index/width)*Pad_Bytes + sizeof(t_Val)*index); }

	template<typename T,typename T2>
	DYNTYPE_FUNC t_Val & At(const T x,const T2 y)
	{ return *(t_Val*)(&((LPBYTE)lpData)[y*Step_Bytes+x*sizeof(t_Val)]); }
	template<typename T,typename T2>
	DYNTYPE_FUNC const t_Val & At(const T x,const T2 y) const
	{ return rt::_CastToNonconst(this)->At(x,y); }

	IPP_FUNC UINT GetSize() const{ return (UINT)(width*height); }

	// TODO: return ipp::CSignal when it is finished
	::rt::Buffer_Ref<t_Val>
	GetLine(UINT y){ return ::rt::Buffer_Ref<t_Val>(&At(0,y),GetWidth()); }
	const ::rt::Buffer_Ref<t_Val> 
	GetLine(UINT y) const{ return ::rt::_CastToNonconst(this)->GetLine(y); }

protected:
	UINT	width;
	UINT	height;
	UINT	Pad_Bytes;	//Pad_Bytes = Step - width*sizeof(t_Val)
	UINT	Step_Bytes; //Step = width*sizeof(t_Val) + Pad_Bytes
	t_Val*	lpData;

	void	__SafeFree(){ 
		// check the comment in SetSize_2D
		//#ifdef _INC_CRTDBG
		//	if(lpData){ delete[] lpData; lpData = NULL; }
		//#else
			if(lpData){ IPPCALL(ippiFree)(lpData); lpData = NULL; } 
		//#endif
	}
public:
	IPP_FUNC BOOL SetSize_2D(UINT w=0,UINT h=0)
	{	if(w==width && h==height){ return TRUE; }
		else
		{	__SafeFree();
			int step_size;
			////////////////////////////////////////////////////////////////////
			// Please let me know your reason for doing this. 
			// It is actually slow IPP down
			// and make it incompatibe with Win32 Bitmaps for not 4BYTE-aligned.
			// IPP will slow down if the leading address if each image row is
			// not 4BYTE-aligned through it will not cause error.
			//                                                -- Jiaping 3/2011
			////////////////////////////////////////////////////////////////////

			//#ifdef _INC_CRTDBG
			//	step_size = w * sizeof(t_Val);
			//	lpData = new t_Val[w * h];
			//#else
				if(w&&h)
				{	switch(sizeof(t_Val)*8)
					{
					case 8:  // 1c8u
						lpData = (t_Val*)IPPCALL(ippiMalloc_8u_C1)(w,h,&step_size); break;
					case 16: // 2c8u,1c16u
						lpData = (t_Val*)IPPCALL(ippiMalloc_16u_C1)(w,h,&step_size); break;
					case 24: // 3c8u
						lpData = (t_Val*)IPPCALL(ippiMalloc_8u_C3)(w,h,&step_size); break;
					case 32: // 4c8u,2c16u,1c32f
						lpData = (t_Val*)IPPCALL(ippiMalloc_8u_C4)(w,h,&step_size); break;
					case 48: // 3c16u
						lpData = (t_Val*)IPPCALL(ippiMalloc_16u_C3)(w,h,&step_size); break;
					case 64: // 4c16u,2c32f,1c64f
						lpData = (t_Val*)IPPCALL(ippiMalloc_16u_C4)(w,h,&step_size); break;
					case 96: // 3c32f
						lpData = (t_Val*)IPPCALL(ippiMalloc_32s_C3)(w,h,&step_size); break;
					case 128:// 4c32f,2c64f
						lpData = (t_Val*)IPPCALL(ippiMalloc_32s_C4)(w,h,&step_size); break;
					default:
						ASSERT(0); //unsupported pixel format
					}
			}
			//#endif

			if(lpData)
			{	width=w; height=h; 
				Step_Bytes = (UINT)step_size;
				Pad_Bytes = Step_Bytes - width*sizeof(t_Val);
				return TRUE;
			}
			else{ width=height=0; }
		}
		return FALSE;
	}
	DYNTYPE_FUNC UINT		GetWidth() const{ return width;}
	DYNTYPE_FUNC UINT		GetHeight() const{ return height;}
};
}// namespace _meta_
class _BaseIppImage
{public:	template<typename t_Ele> class SpecifyItemType
			{public: typedef ::ipp::_meta_::_BaseIppImageRoot<t_Ele> t_BaseVec; };
			static const bool IsCompactLinear = false;
};


typedef ATL::CImage	CImageFile;

#define _IppImgDump(x)	{ Ippi::CImageFile f; (x).Export(f); f.Save("dump_" #x ".png"); }

#define _CheckDumpImage(x)	_IppImgDump(x)
#define _CheckDumpImageHDR(x)	{ Ippi::Save_PFM((x),"dump_" #x ".pfm"); }

#define IPPARG_IMG(x)	 ((x).GetImageData()),((int)(x).GetStep())
#define IPPARG_IMG2(x)	 ((x).GetImageData()),(x),((int)(x).GetStep())

template<typename t_Val,UINT i>
class CImage_Ref;

template<typename t_Value,UINT channel,class BaseImage=::ipp::_BaseIppImage>
class CImage:public rt::Image<typename num::SpecifySmallVec<t_Value,channel>::t_Result,BaseImage>
{
protected:
	IPP_FUNC t_Value*		GetValueAddress(UINT x, UINT y){ return (t_Value*)GetPixelAddress(x,y); }
	IPP_FUNC const t_Value*	GetValueAddress(UINT x, UINT y)const{ return (const t_Value*)GetPixelAddress(x,y); }

public:
	TYPETRAITS_DECL_EXTENDTYPEID((rt::_typeid_codelib<<16)+106)
	COMMON_CONSTRUCTOR_IMG(CImage)
	COMMON_IMAGE_STUFFS(CImage)

	template<typename T,UINT ch, typename BaseVec>
	IPP_FUNC const CImage& operator = (const CImage<T,ch,BaseVec> & x)
	{ CopyFrom(x); return *this; }

	static const int chan_num = channel;

	IPP_FUNC static UINT  GetChannels(){ return chan_num;}
	IPP_FUNC static UINT  GetBPV(){ return sizeof(t_Value)<<3;}

public:
	typedef CImage_Ref<t_Value,channel> Ref;
	typedef CImage_Ref<t_Value,channel> Image_Ref;
	typedef CImage_Ref<t_Value,1>		Image1C_Ref;
	IPP_FUNC operator Ref& (){ return *((Ref*)this); }
	IPP_FUNC operator const Ref& () const{ return *((Ref*)this); }

	IPP_FUNC Ref GetSub(UINT x,UINT y,UINT w,UINT h)
	{	ASSERT(x+w<=GetWidth());
		ASSERT(y+h<=GetHeight());
		return Ref(GetPixelAddress(x,y),w,h,GetStep()); 
	}
	IPP_FUNC const Ref GetSub(UINT x,UINT y,UINT w,UINT h) const
	{	return rt::_CastToNonconst(this)->GetSub(x,y,w,h); }
	
	IPP_FUNC Ref GetSub_Inside(UINT border)
	{	ASSERT(border*2<GetWidth());
		ASSERT(border*2<GetHeight());
		return GetSub(border,border,GetWidth()-border*2,GetHeight()-border*2);
	}
	IPP_FUNC const Ref GetSub_Inside(UINT border) const
	{	return rt::_CastToNonconst(this)->GetSub_Inside(border); }

	IPP_FUNC Ref GetSub_Inside(UINT border_x,UINT border_y)
	{	ASSERT(border_x*2<GetWidth());
		ASSERT(border_y*2<GetHeight());
		return GetSub(border_x,border_y,GetWidth()-border_x*2,GetHeight()-border_y*2);
	}
	IPP_FUNC const Ref GetSub_Inside(UINT border_x,UINT border_y) const
	{	return rt::_CastToNonconst(this)->GetSub_Inside(border_x,border_y); }

public:
	CSignal_Ref<t_Value,1> GetVec()
	{	return CSignal_Ref<t_Value,1>(GetImageData(),GetDataSize()/sizeof(t_Value)); }
	const CSignal_Ref<t_Value,1> GetVec() const
	{	return ::rt::_CastToNonconst(this)->GetVec(); }


public:
	IPP_FUNC operator const IppiSize& ()const{ return *((const IppiSize*)this); }
	IPP_FUNC const CIppiSize& GetRegion()const{ return *((const CIppiSize*)this); }
	IPP_FUNC BOOL SetSize(const IppiSize& sz){ return SetSize(sz.width,sz.height); }
	IPP_FUNC BOOL SetSize(UINT w,UINT h){ return __super::SetSize(w,h); }
	IPP_FUNC LPValueType	GetImageData(){ return (LPValueType)GetBits(); }
	IPP_FUNC LPCValueType	GetImageData()const{ return (LPCValueType)GetBits(); }

public:
	template<typename T,UINT ch, typename BaseVec2>
	IPP_FUNC void CopyFrom(const CImage<T,ch,BaseVec2> & in)
	{	ASSERT_SIZE(*this,in);
		typedef typename rt::FundamentalType<T>::t_Result	_ValueType2;
		typedef ValueType _ValueType;
		typedef rt::IsTypeSame<_ValueType2,_ValueType>		_is_same;
		static const int chan_num2=CImage<T,ch,BaseVec2>::chan_num;

		__static_if(chan_num==chan_num2 && _is_same::Result)
		{ // copy directly
			switch(chan_num)
			{
			case 1: ipp::ipp_cpp::ippiCopy_C1R(IPPARG_IMG(in),IPPARG_IMG(*this),*this); return;
			case 2:	switch(sizeof(_ValueType)*8){
					case 8:	IPPCALL(ippiCopy_16s_C1R)((const Ipp16s*)in.GetBits(),in.GetStep(),(Ipp16s*)GetBits(),GetStep(),*this); return;
					case 16:IPPCALL(ippiCopy_32f_C1R)((const Ipp32f*)in.GetBits(),in.GetStep(),(Ipp32f*)GetBits(),GetStep(),*this); return;
					case 32:IPPCALL(ippiCopy_16s_C4R)((const Ipp16s*)in.GetBits(),in.GetStep(),(Ipp16s*)GetBits(),GetStep(),*this); return;
					} break;
			case 3: ::ipp::ipp_cpp::ippiCopy_C3R(IPPARG_IMG(in),IPPARG_IMG(*this),*this); return;
			case 4: ::ipp::ipp_cpp::ippiCopy_C4R(IPPARG_IMG(in),IPPARG_IMG(*this),*this); return;
			}
		}
		__static_if(chan_num==chan_num2 && !_is_same::Result)
		{ // scale and copy
			switch(chan_num)
			{
			case 1: ::ipp::ipp_cpp::ippiScale_C1R(IPPARG_IMG(in),IPPARG_IMG(*this),*this); return;
			case 3: ::ipp::ipp_cpp::ippiScale_C3R(IPPARG_IMG(in),IPPARG_IMG(*this),*this); return;
			case 4: ::ipp::ipp_cpp::ippiScale_C4R(IPPARG_IMG(in),IPPARG_IMG(*this),*this); return;
			}
		}
		__static_if(chan_num!=chan_num2 && _is_same::Result)
		{ // channel mix and copy
			switch(chan_num2)
			{
			case 1: //1->x
				switch(chan_num) 
				{	case 3: 
						{	const ValueType* const SrcImg[3] = { in.GetImageData(),in.GetImageData(),in.GetImageData() };
							ipp::ipp_cpp::ippiCopy_P3C3R(SrcImg,in.GetStep(),IPPARG_IMG(*this),*this);
						}	return;
					case 4:
						{	const ValueType* const SrcImg[4] = { in.GetImageData(),in.GetImageData(),in.GetImageData(),in.GetImageData() };
							ipp::ipp_cpp::ippiCopy_P4C4R(SrcImg,in.GetStep(),IPPARG_IMG(*this),*this);
						}	return;
 				} break;
			case 3: //3->x
				switch(chan_num) {
				case 1: ipp::ipp_cpp::ippiRGBToGray_C3C1R(IPPARG_IMG(in),IPPARG_IMG(*this),*this); return;
				case 4: 
					__static_if(sizeof(t_Value) < sizeof(float))
					{	ipp::ipp_cpp::ippiSet_C1R(rt::TypeTraits<t_Value>::MaxVal(),IPPARG_IMG(*this), CIppiSize(GetWidth()*4,GetHeight())); }
					__static_if(sizeof(t_Value) >= sizeof(float))
					{	ipp::ipp_cpp::ippiSet_C1R(GetEnv()->FloatMax,IPPARG_IMG(*this), CIppiSize(GetWidth()*4,GetHeight())); }
					ipp::ipp_cpp::ippiCopy_C3AC4R(IPPARG_IMG(in),IPPARG_IMG(*this),*this);
					return;
				} break;
			case 4: //4->x
				switch(chan_num) {
				case 1: ipp::ipp_cpp::ippiRGBToGray_AC4C1R(IPPARG_IMG(in),IPPARG_IMG(*this),*this); return;
				case 3: ipp::ipp_cpp::ippiCopy_AC4C3R(IPPARG_IMG(in),IPPARG_IMG(*this),*this); return; 
				} break;
			}
		}
		{// handle all remaining situations
			ASSERT(rt::IsInRange_CC(GetEnv()->FloatMin,-EPSILON,EPSILON)); // GetEnv()->FloatMin is not supported
			__super::CopyFrom(in,GetEnv()->FloatMax);
		}
    }
	template<typename t_Val2,UINT ch,class BaseImage2>	//conversion images if possible
	void CopyTo( CImage<t_Val2,ch,BaseImage2>& in ) const{ in.CopyFrom(*this); }

public:
	/////////////////////////////////////////////////////////////////////////////////////////////
	// Mapped IPPI functions	/////////////////////////////////////////////////////////////////
	//IPP_FUNC void Filter_LowerMask(const Image_Ref& kernel, ValueType ValidValueMin)
	//{	ASSERT(kernel.GetWidth()&1);
	//	ASSERT(kernel.GetHeight()&1);
	//	int knl_half_width = kernel.GetWidth()/2;
	//	int knl_half_height = kernel.GetHeight()/2;
	//	t_ImageObj temp;
	//	temp.SetSize( GetWidth()+kernel.GetWidth()-1, GetHeight()+kernel.GetHeight()-1 );
	//	{	//Clear to invalid value
	//		PixelType invalid;
	//		invalid.Set( rt::TypeTraits<ValueType>::MinVal() );
	//		temp.Set(invalid);
	//	}
	//	CopyTo(temp.GetSub(knl_half_width,knl_half_height,GetWidth(),GetHeight()));

	//	rt::TypeTraits<PixelType>::t_Accum accum,wei;

	//	for(int dy=0;dy<GetHeight();dy++)
	//	for(int dx=0;dx<GetWidth();dx++)
	//	{
	//		accum.Set(0);
	//		wei.Set(0);
	//		
	//		PixelType& dest = GetPixel(dx,dy);
	//		if(dest.x>=ValidValueMin)
	//		{
	//			Image_Ref & win = temp.GetSub( dx, dy, kernel.GetWidth(), kernel.GetHeight() );

	//			for(int y=0;y<kernel.GetHeight();y++)
	//			for(int x=0;x<kernel.GetWidth();x++)
	//			{
	//				PixelType& px = win.GetPixel(x,y);
	//				if( px.x>=ValidValueMin )
	//				{	accum = accum + px*kernel.GetPixel(x,y);
	//					wei = wei + kernel.GetPixel(x,y);
	//				}
	//			}

	//			dest = accum/wei;
	//		}
	//	}
	//}

	//IPP_FUNC void Filter_UpperMask(const Image_Ref& kernel, ValueType ValidValueMax)
	//{	ASSERT(kernel.GetWidth()&1);
	//	ASSERT(kernel.GetHeight()&1);
	//	int knl_half_width = kernel.GetWidth()/2;
	//	int knl_half_height = kernel.GetHeight()/2;
	//	t_ImageObj temp;
	//	temp.SetSize( GetWidth()+kernel.GetWidth()-1, GetHeight()+kernel.GetHeight()-1 );
	//	{	//Clear to invalid value
	//		PixelType invalid;
	//		invalid.Set( rt::TypeTraits<ValueType>::MinVal() );
	//		temp.Set(invalid);
	//	}
	//	CopyTo(temp.GetSub(knl_half_width,knl_half_height,GetWidth(),GetHeight()));

	//	for(int dy=0;dy<GetHeight();dy++)
	//	for(int dx=0;dx<GetWidth();dx++)
	//	{
	//		rt::TypeTraits<PixelType>::t_Accum accum,wei;
	//		
	//		accum.Set(0);
	//		wei.Set(0);
	//		
	//		PixelType& dest = GetPixel(dx,dy);
	//		if(dest.x<=ValidValueMax)
	//		{
	//			Image_Ref & win = temp.GetSub( dx, dy, kernel.GetWidth(), kernel.GetHeight() );

	//			for(int y=0;y<kernel.GetHeight();y++)
	//			for(int x=0;x<kernel.GetWidth();x++)
	//			{
	//				PixelType& px = win.GetPixel(x,y);
	//				if( px.x<=ValidValueMax )
	//				{	accum = accum + px*kernel.GetPixel(x,y);  
	//					wei = wei + kernel.GetPixel(x,y);
	//				}
	//			}

	//			dest = accum/wei;
	//		}
	//	}
	//}

	IPP_FUNC void Normalize()	//Not tested for 8u 16s image, only used in HDR
	{	rt::Vec<Ipp64f,chan_num>	s;
		L2Norm(s);
		__static_if(chan_num==1)
		{	Multiply(num::Vec1<ValueType>((ValueType)(1.0/s.x)));
			return;
		}
		__static_if(chan_num==3)
		{	ValueType d = (ValueType)(3.0/s.Sum()); 
			Multiply(num::Vec3<ValueType>(d,d,d));
			return;
		}
		__static_if(chan_num==4)
		{	ValueType d = (ValueType)(4.0/s.Sum()); 
			Multiply(num::Vec4<ValueType>(d,d,d,d));
			return;
		}
	}

	template<typename t_Val2>
	IPP_FUNC void L2Norm(rt::Vec<t_Val2,chan_num>& out) const
	{	rt::Vec<Ipp64f,chan_num>	s;
		switch(chan_num) {
		case 1: ipp_cpp::ippiNorm_L2_C1R(IPPARG_IMG(*this),*this,s); break;
		case 3: ipp_cpp::ippiNorm_L2_C3R(IPPARG_IMG(*this),*this,s); break;
		case 4: ipp_cpp::ippiNorm_L2_C4R(IPPARG_IMG(*this),*this,s); break;
		}
		out = s;
	}

	template<typename t_Val2>
	IPP_FUNC void L2Norm(const Image_Ref&x, rt::Vec<t_Val2,chan_num>& out) const
	{	rt::Vec<Ipp64f,chan_num>	s;
		switch(chan_num) {
		case 1: ipp_cpp::ippiNormDiff_L2_C1R(IPPARG_IMG(*this),IPPARG_IMG(x),*this,s); break;
		case 3: ipp_cpp::ippiNormDiff_L2_C3R(IPPARG_IMG(*this),IPPARG_IMG(x),*this,s); break;
		case 4: ipp_cpp::ippiNormDiff_L2_C4R(IPPARG_IMG(*this),IPPARG_IMG(x),*this,s); break;
		}
		out = s;
	}

	template<typename t_Val2>
	IPP_FUNC void Sum(rt::Vec<t_Val2,chan_num>& out) const
	{	rt::Vec<Ipp64f,chan_num>	s;
		switch(chan_num) {
		case 1: ipp_cpp::ippiSum_C1R(IPPARG_IMG(*this),*this,s); break;
		case 3: ipp_cpp::ippiSum_C3R(IPPARG_IMG(*this),*this,s); break;
		case 4: ipp_cpp::ippiSum_C4R(IPPARG_IMG(*this),*this,s); break;
		}
		out = s;
	}
	template<typename t_Val2>
	IPP_FUNC void Mean(rt::Vec<t_Val2,chan_num>& out) const
	{	rt::Vec<Ipp64f,chan_num>	s;
		switch(chan_num) {
		case 1: ipp_cpp::ippiMean_C1R(IPPARG_IMG(*this),*this,s); break;
		case 3: ipp_cpp::ippiMean_C3R(IPPARG_IMG(*this),*this,s); break;
		case 4: ipp_cpp::ippiMean_C4R(IPPARG_IMG(*this),*this,s); break;
		}
		out = s;
	}
	template<typename t_Val2,typename t_Val3>
	IPP_FUNC void Deviation(rt::Vec<t_Val2,chan_num>& dev, rt::Vec<t_Val3,chan_num>& mean) const
	{	rt::Vec<Ipp64f,chan_num>	s,d;
		switch(chan_num) {
			ippiMean_StdDev_8u_C1R
		case 1: ipp_cpp::ippiMean_StdDev_C1R(IPPARG_IMG(*this),*this,s,d); break;
		case 3: ASSERT(0); break;
		case 4: ASSERT(0); break;
		}
		mean = s;
		dev = d;
	}
	template<typename t_Val2>
	IPP_FUNC void Deviation(rt::Vec<t_Val2,chan_num>& dev) const
	{	rt::Vec<Ipp64f,chan_num>	d;
		switch(chan_num) {
		case 1: ipp_cpp::ippiMean_StdDev_C1R(IPPARG_IMG(*this),*this,NULL,d); break;
		case 3: ASSERT(0); break;
		case 4: ASSERT(0); break;
		}
		dev = d;
	}
	IPP_FUNC void Min(PixelType & p) const
	{	switch(chan_num) {
		case 1: ipp_cpp::ippiMin_C1R(IPPARG_IMG(*this),*this,p); break;
		case 3: ipp_cpp::ippiMin_C3R(IPPARG_IMG(*this),*this,p); break;
		case 4: ipp_cpp::ippiMin_C4R(IPPARG_IMG(*this),*this,p); break;
	}	}
	IPP_FUNC void Min(PixelType & p, rt::Vec<num::Vec2i,chan_num> & pos) const
	{	int x[4]; int y[4];
		switch(chan_num) {
		case 1: ipp_cpp::ippiMinIndx_C1R(IPPARG_IMG(*this),*this,p,x,y);
				pos[0].x = x[0];
				pos[0].y = y[0];
				break;
		case 3: ipp_cpp::ippiMinIndx_C3R(IPPARG_IMG(*this),*this,p,x,y); 
				pos[0].x = x[0];
				pos[0].y = y[0];
				pos[1].x = x[1];
				pos[1].y = y[1];
				pos[2].x = x[2];
				pos[2].y = y[2];
				break;
		case 4: ipp_cpp::ippiMinIndx_C4R(IPPARG_IMG(*this),*this,p,x,y); 
				pos[0].x = x[0];
				pos[0].y = y[0];
				pos[1].x = x[1];
				pos[1].y = y[1];
				pos[2].x = x[2];
				pos[2].y = y[2];
				pos[3].x = x[3];
				pos[3].y = y[3];
				break;
	}	}
	IPP_FUNC void Max(PixelType & p) const
	{	switch(chan_num) {
		case 1: ipp_cpp::ippiMax_C1R(IPPARG_IMG(*this),*this,p); break;
		case 3: ipp_cpp::ippiMax_C3R(IPPARG_IMG(*this),*this,p); break;
		case 4: ipp_cpp::ippiMax_C4R(IPPARG_IMG(*this),*this,p); break;
	}	}
	IPP_FUNC void Max(PixelType & p, rt::Vec<num::Vec2i,chan_num> & pos) const
	{	int x[4]; int y[4];
		switch(chan_num) {
		case 1: ipp_cpp::ippiMaxIndx_C1R(IPPARG_IMG(*this),*this,p,x,y);
				pos[0].x = x[0];
				pos[0].y = y[0];
				break;
		case 3: ipp_cpp::ippiMaxIndx_C3R(IPPARG_IMG(*this),*this,p,x,y); 
				pos[0].x = x[0];
				pos[0].y = y[0];
				pos[1].x = x[1];
				pos[1].y = y[1];
				pos[2].x = x[2];
				pos[2].y = y[2];
				break;
		case 4: ipp_cpp::ippiMaxIndx_C4R(IPPARG_IMG(*this),*this,p,x,y); 
				pos[0].x = x[0];
				pos[0].y = y[0];
				pos[1].x = x[1];
				pos[1].y = y[1];
				pos[2].x = x[2];
				pos[2].y = y[2];
				pos[3].x = x[3];
				pos[3].y = y[3];
				break;
	}	}
	IPP_FUNC void MinMax(PixelType & pmin,PixelType & pmax) const
	{	switch(chan_num) {
		case 1: ipp_cpp::ippiMinMax_C1R(IPPARG_IMG(*this),*this,pmin,pmax); break;
		case 3: ipp_cpp::ippiMinMax_C3R(IPPARG_IMG(*this),*this,pmin,pmax); break;
		case 4: ipp_cpp::ippiMinMax_C4R(IPPARG_IMG(*this),*this,pmin,pmax); break;
	}	}
	IPP_FUNC void Threshold_LessThan(const PixelType& v)
	{	switch(chan_num) {
		case 1: ipp_cpp::ippiThreshold_LT_C1R(IPPARG_IMG(*this),*this,v[0]); break;
		case 3: ipp_cpp::ippiThreshold_LT_C3R(IPPARG_IMG(*this),*this,v); break;
		case 4: ipp_cpp::ippiThreshold_LT_AC4R(IPPARG_IMG(*this),*this,v); break;
	}	}
	IPP_FUNC void ThresholdTo_LessThan(const PixelType& v,Image_Ref& x) const
	{	ASSERT_SIZE(*this,x); 
		switch(chan_num) {
		case 1: ipp_cpp::ippiThreshold_LT_C1R(IPPARG_IMG(*this),IPPARG_IMG(x),x,v[0]); break;
		case 3: ipp_cpp::ippiThreshold_LT_C3R(IPPARG_IMG(*this),IPPARG_IMG(x),x,v); break;
		case 4: ipp_cpp::ippiThreshold_LT_AC4R(IPPARG_IMG(*this),IPPARG_IMG(x),x,v); break;
	}	}
	IPP_FUNC void Threshold_GreatThan(const PixelType& v)
	{	switch(chan_num) {
		case 1: ipp_cpp::ippiThreshold_GT_C1R(IPPARG_IMG(*this),*this,v[0]); break;
		case 3: ipp_cpp::ippiThreshold_GT_C3R(IPPARG_IMG(*this),*this,v); break;
		case 4: ipp_cpp::ippiThreshold_GT_AC4R(IPPARG_IMG(*this),*this,v); break;
	}	}
	IPP_FUNC void ThresholdTo_GreatThan(const PixelType& v,Image_Ref& x) const
	{	ASSERT_SIZE(*this,x);
		switch(chan_num) {
		case 1: ipp_cpp::ippiThreshold_GT_C1R(IPPARG_IMG(*this),IPPARG_IMG(x),x,v[0]); break;
		case 3: ipp_cpp::ippiThreshold_GT_C3R(IPPARG_IMG(*this),IPPARG_IMG(x),x,v); break;
		case 4: ipp_cpp::ippiThreshold_GT_AC4R(IPPARG_IMG(*this),IPPARG_IMG(x),x,v); break;
	}	}
	IPP_FUNC void Mirror(int axis = AxisHorizontal)
	{	switch(chan_num) {
		case 1: ipp::ipp_cpp::ippiMirror_C1R(IPPARG_IMG(*this),*this,(IppiAxis)axis); break;
		case 3: ipp::ipp_cpp::ippiMirror_C3R(IPPARG_IMG(*this),*this,(IppiAxis)axis); break;
		case 4: ipp::ipp_cpp::ippiMirror_C4R(IPPARG_IMG(*this),*this,(IppiAxis)axis); break;
	}	}
	IPP_FUNC void MirrorTo(Image_Ref& x,int axis = AxisHorizontal) const
	{	ASSERT_SIZE(*this,x);
		switch(chan_num) {
		case 1: ipp_cpp::ippiMirror_C1R(IPPARG_IMG(*this),IPPARG_IMG(x),*this,(IppiAxis)axis); break;
		case 3: ipp_cpp::ippiMirror_C3R(IPPARG_IMG(*this),IPPARG_IMG(x),*this,(IppiAxis)axis); break;
		case 4: ipp_cpp::ippiMirror_C4R(IPPARG_IMG(*this),IPPARG_IMG(x),*this,(IppiAxis)axis); break;
	}	}
	IPP_FUNC void ResizeTo(Image_Ref& x) const
	{	ASSERT(x.IsNotNull());
		double xf = (x.GetWidth())/((double)GetWidth());
		double yf = (x.GetHeight())/((double)GetHeight());
		IppiRect rc; 	rc.x = 0;	rc.y = 0;		
		rc.width = GetWidth();		rc.height = GetHeight();
		switch(chan_num) {
		case 1: ipp_cpp::ippiResize_C1R(IPPARG_IMG2(*this),rc,IPPARG_IMG(x),x,xf,yf,GetEnv()->InterpolationMode); break;
		case 3: ipp_cpp::ippiResize_C3R(IPPARG_IMG2(*this),rc,IPPARG_IMG(x),x,xf,yf,GetEnv()->InterpolationMode); break;
		case 4: ipp_cpp::ippiResize_C4R(IPPARG_IMG2(*this),rc,IPPARG_IMG(x),x,xf,yf,GetEnv()->InterpolationMode); break;
		}
		//// IPP bugs fix up: supersampling will add 0.5 for each pixel
		//if(	ipp::GetEnv()->InterpolationMode == ipp::InterpolationMode_Super && 
		//	::rt::TypeTraits<t_Value>::Typeid == ::rt::_typeid_32f)
		//{	PixelType a; a=(t_Value)-0.5f; x.Add(a);	}
	}
	IPP_FUNC void RotateTo(double angle_deg, const num::Vec2d& cent_src, Image_Ref& x) const
	{	ASSERT(x.IsNotNull());
		IppiRect rc; 	
		rc.x = 0;				rc.y = 0;		
		rc.width = GetWidth();	rc.height = GetHeight();
		IppiRect rc_d; 	
		rc_d.x = 0;				rc_d.y = 0;
		rc_d.width = x.GetWidth();	rc_d.height = x.GetHeight();

		double xShift, yShift;
		IPPCALL(ippiGetRotateShift)(cent_src.x,cent_src.y,angle_deg,&xShift,&yShift);
		xShift += cent_src.x * (x.GetWidth()/(double)GetWidth() - 1);
		yShift += cent_src.y * (x.GetHeight()/(double)GetHeight() - 1);

		switch(chan_num) {
		case 1: ipp_cpp::ippiRotate_C1R(IPPARG_IMG2(*this),rc,IPPARG_IMG(x),rc_d,angle_deg,xShift,yShift,GetEnv()->InterpolationMode); break;
		case 3: ipp_cpp::ippiRotate_C3R(IPPARG_IMG2(*this),rc,IPPARG_IMG(x),rc_d,angle_deg,xShift,yShift,GetEnv()->InterpolationMode); break;
		case 4: ipp_cpp::ippiRotate_C4R(IPPARG_IMG2(*this),rc,IPPARG_IMG(x),rc_d,angle_deg,xShift,yShift,GetEnv()->InterpolationMode); break;
		}
	}

	IPP_FUNC void Multiply(const PixelType& scale)
	{	switch(chan_num) {
			case 1: ipp_cpp::ippiMulCScale_C1R(scale[0],IPPARG_IMG(*this),*this); break;
			case 3: ipp_cpp::ippiMulCScale_C3R(scale,IPPARG_IMG(*this),*this); break;
			case 4: ipp_cpp::ippiMulCScale_C4R(scale,IPPARG_IMG(*this),*this); break;
	}	}
	IPP_FUNC void MultiplyTo(Image_Ref& x,const PixelType& scale) const
	{	ASSERT_SIZE(*this,x);
		switch(chan_num) {
			case 1: ipp_cpp::ippiMulCScale_C1R(IPPARG_IMG(*this),scale[0],IPPARG_IMG(x),*this); break;
			case 3: ipp_cpp::ippiMulCScale_C3R(IPPARG_IMG(*this),scale,IPPARG_IMG(x),*this); break;
			case 4: ipp_cpp::ippiMulCScale_C4R(IPPARG_IMG(*this),scale,IPPARG_IMG(x),*this); break;
	}	}
	IPP_FUNC void Multiply(const Image_Ref& src)
	{	ASSERT_SIZE(*this,src);
		switch(chan_num) {
			case 1: ipp_cpp::ippiMul_C1R(IPPARG_IMG(src),IPPARG_IMG(*this),*this); break;
			case 3: ipp_cpp::ippiMul_C3R(IPPARG_IMG(src),IPPARG_IMG(*this),*this); break;
			case 4: ipp_cpp::ippiMul_C4R(IPPARG_IMG(src),IPPARG_IMG(*this),*this); break;
	}	}
	IPP_FUNC void MultiplyTo(const Image_Ref& src,Image_Ref& dst) const
	{	ASSERT_SIZE(*this,src);
		ASSERT_SIZE(*this,dst);
		switch(chan_num) {
			case 1: ipp_cpp::ippiMul_C1R(IPPARG_IMG(src),IPPARG_IMG(*this),IPPARG_IMG(dst),*this); break;
			case 3: ipp_cpp::ippiMul_C3R(IPPARG_IMG(src),IPPARG_IMG(*this),IPPARG_IMG(dst),*this); break;
			case 4: ipp_cpp::ippiMul_C4R(IPPARG_IMG(src),IPPARG_IMG(*this),IPPARG_IMG(dst),*this); break;
	}	}
	IPP_FUNC void Divide(const Image_Ref& src)
	{	ASSERT_SIZE(*this,src);
		switch(chan_num) {
			case 1: ipp_cpp::ippiDiv_C1R(IPPARG_IMG(src),IPPARG_IMG(*this),*this); break;
			case 3: ipp_cpp::ippiDiv_C3R(IPPARG_IMG(src),IPPARG_IMG(*this),*this); break;
			case 4: ipp_cpp::ippiDiv_C4R(IPPARG_IMG(src),IPPARG_IMG(*this),*this); break;
	}	}
	IPP_FUNC void DivideTo(const Image_Ref& src,Image_Ref& dst) const
	{	ASSERT_SIZE(*this,src);
		ASSERT_SIZE(*this,dst);
		switch(chan_num) {
			case 1: ipp_cpp::ippiDiv_C1R(IPPARG_IMG(src),IPPARG_IMG(*this),IPPARG_IMG(dst),*this); break;
			case 3: ipp_cpp::ippiDiv_C3R(IPPARG_IMG(src),IPPARG_IMG(*this),IPPARG_IMG(dst),*this); break;
			case 4: ipp_cpp::ippiDiv_C4R(IPPARG_IMG(src),IPPARG_IMG(*this),IPPARG_IMG(dst),*this); break;
	}	}
	IPP_FUNC void Sqr()
	{	switch(chan_num) {
			case 1: ipp_cpp::ippiSqr_C1R(IPPARG_IMG(*this),*this); break;
			case 3: ipp_cpp::ippiSqr_C3R(IPPARG_IMG(*this),*this); break;
			case 4: ipp_cpp::ippiSqr_C4R(IPPARG_IMG(*this),*this); break;
	}	}
	IPP_FUNC void SqrTo(Image_Ref& x) const
	{	ASSERT_SIZE(*this,x);
		switch(chan_num) {
			case 1: ipp_cpp::ippiSqr_C1R(IPPARG_IMG(*this),IPPARG_IMG(x),*this); break;
			case 3: ipp_cpp::ippiSqr_C3R(IPPARG_IMG(*this),IPPARG_IMG(x),*this); break;
			case 4: ipp_cpp::ippiSqr_C4R(IPPARG_IMG(*this),IPPARG_IMG(x),*this); break;
	}	}
	IPP_FUNC void Sqrt()
	{	switch(chan_num) {
			case 1: ipp_cpp::ippiSqrt_C1R(IPPARG_IMG(*this),*this); break;
			case 3: ipp_cpp::ippiSqrt_C3R(IPPARG_IMG(*this),*this); break;
			case 4: ipp_cpp::ippiSqrt_AC4R(IPPARG_IMG(*this),*this); break;
	}	}
	IPP_FUNC void SqrtTo(Image_Ref& x) const
	{	ASSERT_SIZE(*this,x);
		switch(chan_num) {
			case 1: ipp_cpp::ippiSqrt_C1R(IPPARG_IMG(*this),IPPARG_IMG(x),*this); break;
			case 3: ipp_cpp::ippiSqrt_C3R(IPPARG_IMG(*this),IPPARG_IMG(x),*this); break;
			case 4: ipp_cpp::ippiSqrt_AC4R(IPPARG_IMG(*this),IPPARG_IMG(x),*this); break;
	}	}
	IPP_FUNC void CopyChannelFrom(const CImage_Ref<t_Value,1>& x,int chan_id)
	{	ASSERT_SIZE(*this,x);
		switch(chan_num) {
			case 1: ASSERT(chan_id<1); ipp_cpp::ippiCopy_C1R(IPPARG_IMG(x),IPPARG_IMG(*this),*this); break;
			case 3: ASSERT(chan_id<3); ipp_cpp::ippiCopy_C1C3R(IPPARG_IMG(x),((LPValueType)GetPixelAddress())+chan_id,GetStep(),*this); break;
			case 4: ASSERT(chan_id<4); ipp_cpp::ippiCopy_C1C4R(IPPARG_IMG(x),((LPValueType)GetPixelAddress())+chan_id,GetStep(),*this); break;
	}	}
	IPP_FUNC void CopyChannelTo(CImage_Ref<t_Value,1>& x,int chan_id) const
	{	ASSERT_SIZE(*this,x);
		switch(chan_num) {
			case 1: ASSERT(chan_id<1); ipp_cpp::ippiCopy_C1R(IPPARG_IMG(*this),IPPARG_IMG(x),*this); break;
			case 3: ASSERT(chan_id<3); ipp_cpp::ippiCopy_C3C1R(((LPCValueType)GetPixelAddress())+chan_id,GetStep(),IPPARG_IMG(x),*this); break;
			case 4: ASSERT(chan_id<4); ipp_cpp::ippiCopy_C4C1R(((LPCValueType)GetPixelAddress())+chan_id,GetStep(),IPPARG_IMG(x),*this); break;
	}	}
	IPP_FUNC void AddWeightedTo(CImage_Ref<Ipp32f,chan_num>& x,float alpha) const//x = x*(1-a)+this*a
	{	ASSERT_SIZE(*this,x);
		ipp::CIppiSize sz = x;
		sz.width*=chan_num;
		ipp_cpp::ippiAddWeighted_C1R( IPPARG_IMG(*this),IPPARG_IMG(x),sz,alpha);
	}
	IPP_FUNC void Add(const Image_Ref& x)
	{	ASSERT_SIZE(*this,x);
		switch(chan_num) {	
			case 1: ipp_cpp::ippiAdd_C1R(IPPARG_IMG(x),IPPARG_IMG(*this),*this); break;
			case 3: ipp_cpp::ippiAdd_C3R(IPPARG_IMG(x),IPPARG_IMG(*this),*this); break;
			case 4: ipp_cpp::ippiAdd_C4R(IPPARG_IMG(x),IPPARG_IMG(*this),*this); break;
	}	}
	IPP_FUNC void AddTo(const Image_Ref& src1,Image_Ref& dst) const
	{	ASSERT_SIZE(*this,src1); 
		ASSERT_SIZE(*this,dst);
		switch(chan_num) {	
			case 1: ipp_cpp::ippiAdd_C1R(IPPARG_IMG(src1),IPPARG_IMG(*this),IPPARG_IMG(dst),*this); break;
			case 3: ipp_cpp::ippiAdd_C3R(IPPARG_IMG(src1),IPPARG_IMG(*this),IPPARG_IMG(dst),*this); break;
			case 4: ipp_cpp::ippiAdd_C4R(IPPARG_IMG(src1),IPPARG_IMG(*this),IPPARG_IMG(dst),*this); break;
	}	}
	IPP_FUNC void Add(const PixelType& val)
	{	switch(chan_num) {	
			case 1: ipp_cpp::ippiAdd_C1R(val[0],IPPARG_IMG(*this),*this); break;
			case 3: ipp_cpp::ippiAdd_C3R(val,IPPARG_IMG(*this),*this); break;
			case 4: ipp_cpp::ippiAdd_C4R(val,IPPARG_IMG(*this),*this); break;
	}	}
	IPP_FUNC void AddTo(const PixelType& val,Image_Ref& dst) const
	{	ASSERT_SIZE(*this,dst);
		switch(chan_num) {	
			case 1: ipp_cpp::ippiAdd_C1R(IPPARG_IMG(src1),val[0],IPPARG_IMG(*this),*this); break;
			case 3: ipp_cpp::ippiAdd_C3R(IPPARG_IMG(src1),val,IPPARG_IMG(*this),*this); break;
			case 4: ipp_cpp::ippiAdd_C4R(IPPARG_IMG(src1),val,IPPARG_IMG(*this),*this); break;
	}	}
	IPP_FUNC void Subtract(const Image_Ref& x)
	{	ASSERT_SIZE(*this,x);
		switch(chan_num) {	
			case 1: ipp_cpp::ippiSub_C1R(IPPARG_IMG(x),IPPARG_IMG(*this),*this); break;
			case 3: ipp_cpp::ippiSub_C3R(IPPARG_IMG(x),IPPARG_IMG(*this),*this); break;
			case 4: ipp_cpp::ippiSub_C4R(IPPARG_IMG(x),IPPARG_IMG(*this),*this); break;
	}	}
	IPP_FUNC void SubtractTo(const Image_Ref& src1,Image_Ref& dst) const
	{	ASSERT_SIZE(*this,src1); 
		ASSERT_SIZE(*this,dst);
		switch(chan_num) {	
			case 1: ipp_cpp::ippiSub_C1R(IPPARG_IMG(src1),IPPARG_IMG(*this),IPPARG_IMG(dst),*this); break;
			case 3: ipp_cpp::ippiSub_C3R(IPPARG_IMG(src1),IPPARG_IMG(*this),IPPARG_IMG(dst),*this); break;
			case 4: ipp_cpp::ippiSub_C4R(IPPARG_IMG(src1),IPPARG_IMG(*this),IPPARG_IMG(dst),*this); break;
	}	}
	IPP_FUNC void Subtract(const PixelType& val)
	{	switch(chan_num) {	
			case 1: ipp_cpp::ippiSub_C1R(val[0],IPPARG_IMG(*this),*this); break;
			case 3: ipp_cpp::ippiSub_C3R(val,IPPARG_IMG(*this),*this); break;
			case 4: ipp_cpp::ippiSub_C4R(val,IPPARG_IMG(*this),*this); break;
	}	}
	IPP_FUNC void SubtractTo(const PixelType& val,Image_Ref& dst) const
	{	ASSERT_SIZE(*this,dst);
		switch(chan_num) {	
			case 1: ipp_cpp::ippiSub_C1R(IPPARG_IMG(src1),val[0],IPPARG_IMG(*this),*this); break;
			case 3: ipp_cpp::ippiSub_C3R(IPPARG_IMG(src1),val,IPPARG_IMG(*this),*this); break;
			case 4: ipp_cpp::ippiSub_C4R(IPPARG_IMG(src1),val,IPPARG_IMG(*this),*this); break;
	}	}
	IPP_FUNC void Set(const PixelType& value)
	{	switch(chan_num) {
			case 1: ipp_cpp::ippiSet_C1R(value[0],IPPARG_IMG(*this),*this); break;
			case 3: ipp_cpp::ippiSet_C3R(value,IPPARG_IMG(*this),*this); break;
			case 4: ipp_cpp::ippiSet_C4R(value,IPPARG_IMG(*this),*this); break;
	}	}
	IPP_FUNC void Ln()
	{	switch(chan_num) {	
			case 1: ipp_cpp::ippiLn_C1R(IPPARG_IMG(*this),*this); break;
			case 3: ipp_cpp::ippiLn_C3R(IPPARG_IMG(*this),*this); break;
			case 4: {	ipp::CIppiSize sz(width*4,height);
						ipp_cpp::ippiLn_C1R(IPPARG_IMG(*this),sz);
						break;
	}	}			}
	IPP_FUNC void LnTo(Image_Ref& dst) const
	{	ASSERT_SIZE(*this,dst); 
		switch(chan_num) {	
			case 1: ipp_cpp::ippiLn_C1R(IPPARG_IMG(*this),IPPARG_IMG(dst),*this); break;
			case 3: ipp_cpp::ippiLn_C3R(IPPARG_IMG(*this),IPPARG_IMG(dst),*this); break;
			case 4: {	ipp::CIppiSize sz(width*4,height);
						ipp_cpp::ippiLn_C1R(IPPARG_IMG(*this),IPPARG_IMG(dst),sz);
						break;
	}	}			}
	IPP_FUNC void Exp()
	{	switch(chan_num) {	
			case 1: ipp_cpp::ippiExp_C1R(IPPARG_IMG(*this),*this); break;
			case 3: ipp_cpp::ippiExp_C3R(IPPARG_IMG(*this),*this); break;
			case 4: {	ipp::CIppiSize sz(width*4,height);
						ipp_cpp::ippiExp_C1R(IPPARG_IMG(*this),sz);
						break;
	}	}			}
	IPP_FUNC void ExpTo(Image_Ref& dst) const
	{	ASSERT_SIZE(*this,dst); 
		switch(chan_num) {	
			case 1: ipp_cpp::ippiExp_C1R(IPPARG_IMG(*this),IPPARG_IMG(dst),*this); break;
			case 3: ipp_cpp::ippiExp_C3R(IPPARG_IMG(*this),IPPARG_IMG(dst),*this); break;
			case 4: {	ipp::CIppiSize sz(width*4,height);
						ipp_cpp::ippiExp_C1R(IPPARG_IMG(*this),IPPARG_IMG(dst),sz);
						break;
	}	}			}
	IPP_FUNC void Lowpass_3x3(Image_Ref& dst) const
	{	ASSERT_SIZE(*this,dst); 
		IppiSize size;
		size.width = width-2;
		size.height = height-2;
		switch(chan_num) {	
			case 1: ipp_cpp::ippiFilterLowpass_C1R(GetValueAddress(1,1),GetStep(),dst.GetValueAddress(1,1),dst.GetStep(),size,ippMskSize3x3); break;
			case 3: ipp_cpp::ippiFilterLowpass_C3R(GetValueAddress(1,1),GetStep(),dst.GetValueAddress(1,1),dst.GetStep(),size,ippMskSize3x3); break;
			case 4: ipp_cpp::ippiFilterLowpass_AC4R(GetValueAddress(1,1),GetStep(),dst.GetValueAddress(1,1),dst.GetStep(),size,ippMskSize3x3); break;
	}	}
	IPP_FUNC void Lowpass_5x5(Image_Ref& dst) const
	{	ASSERT_SIZE(*this,dst); 
		IppiSize size;
		size.width = width-4;
		size.height = height-4;
		switch(chan_num) {	
			case 1: ipp_cpp::ippiFilterLowpass_C1R(GetValueAddress(2,2),GetStep(),dst.GetValueAddress(2,2),dst.GetStep(),size,ippMskSize5x5); break;
			case 3: ipp_cpp::ippiFilterLowpass_C3R(GetValueAddress(2,2),GetStep(),dst.GetValueAddress(2,2),dst.GetStep(),size,ippMskSize5x5); break;
			case 4: ipp_cpp::ippiFilterLowpass_AC4R(GetValueAddress(2,2),GetStep(),dst.GetValueAddress(2,2),dst.GetStep(),size,ippMskSize5x5); break;
	}	}
	IPP_FUNC void Gauss_3x3(Image_Ref& dst) const
	{	ASSERT_SIZE(*this,dst); 
		IppiSize size;
		size.width = width-2;
		size.height = height-2;
		switch(chan_num) {	
			case 1: ipp_cpp::ippiFilterGauss_C1R(GetValueAddress(1,1),GetStep(),dst.GetValueAddress(1,1),dst.GetStep(),size,ippMskSize3x3); break;
			case 3: ipp_cpp::ippiFilterGauss_C3R(GetValueAddress(1,1),GetStep(),dst.GetValueAddress(1,1),dst.GetStep(),size,ippMskSize3x3); break;
			case 4: ipp_cpp::ippiFilterGauss_AC4R(GetValueAddress(1,1),GetStep(),dst.GetValueAddress(1,1),dst.GetStep(),size,ippMskSize3x3); break;
	}	}
	IPP_FUNC void Gauss_5x5(Image_Ref& dst) const
	{	ASSERT_SIZE(*this,dst); 
		IppiSize size;
		size.width = width-4;
		size.height = height-4;
		switch(chan_num) {	
			case 1: ipp_cpp::ippiFilterGauss_C1R(GetValueAddress(2,2),GetStep(),dst.GetValueAddress(2,2),dst.GetStep(),size,ippMskSize5x5); break;
			case 3: ipp_cpp::ippiFilterGauss_C3R(GetValueAddress(2,2),GetStep(),dst.GetValueAddress(2,2),dst.GetStep(),size,ippMskSize5x5); break;
			case 4: ipp_cpp::ippiFilterGauss_AC4R(GetValueAddress(2,2),GetStep(),dst.GetValueAddress(2,2),dst.GetStep(),size,ippMskSize5x5); break;
	}	}
	IPP_FUNC void Gauss(UINT FilterSize,float SmoothRatio = 4.0f)
	{	if( FilterSize > 1 )
		{	FilterSize = min(FilterSize,min(GetWidth(),GetHeight() ));
			if( FilterSize&1 ){}else{ FilterSize++; }

			num::Tabulated_Function<float>	filter;
			filter.SetSize(FilterSize);
			filter.GaussKernel(SmoothRatio);

			CImage<ValueType,chan_num>	tmp;
			tmp.SetSize(GetRegion().AddBorder(FilterSize>>1));
			SeparableFilter(filter,FilterSize,tmp);
		}
	}
	//Performs a full convolution of two images.
	//input img is one image
	//this is another
	//this will be replace by the convoluion result
	IPP_FUNC void ConvFull(const Image_Ref& img)
	{
		int FilterHeight = img.GetHeight();
		int FilterWidth = img.GetWidth();
		ASSERT(GetBPV()==16 || GetBPV()==32); //HDR only 

		CImage<ValueType, chan_num>  workspace;
		int archorY = (int)((FilterHeight>>1) + 0.6);
		int archorX = (int)((FilterWidth>>1) + 0.6);
		//increase to 2, MirrorTo seems have problem with size = 1
		archorY = max(2, archorY); archorX = max(2, archorX);
		workspace.SetSize(GetRegion().AddBorder(archorX, archorY));
		Image_Ref& dstwin = workspace.GetSub_Inside((UINT)archorX, (UINT)archorY);

		//expand border
		CopyTo(dstwin);
		GetSub(0,0,GetWidth(),archorY).MirrorTo(workspace.GetSub(archorX,0,GetWidth(),archorY),AxisHorizontal);
		GetSub(0,GetHeight()-archorY-1,GetWidth(),archorY).MirrorTo(workspace.GetSub(archorX,GetHeight()+archorY,GetWidth(),archorY),AxisHorizontal);
		GetSub(0,0,archorX,GetHeight()).MirrorTo(workspace.GetSub(0,archorY,archorX,GetHeight()),AxisVertical);
		GetSub(GetWidth()-archorX-1,0,archorX,GetHeight()).MirrorTo(workspace.GetSub(GetWidth()+archorX,archorY,archorX,GetHeight()),AxisVertical);
		GetSub(0,0,archorX,archorY).MirrorTo(workspace.GetSub(0,0,archorX, archorY), AxisBoth);
		GetSub(0,GetHeight()-archorY-1,archorX,archorY).MirrorTo(workspace.GetSub(0,GetHeight()+archorY,archorX, archorY), AxisBoth);
		GetSub(GetWidth()-archorX-1,0,archorX,archorY).MirrorTo(workspace.GetSub(GetWidth()+archorX,0,archorX, archorY), AxisBoth);
		GetSub(GetWidth()-archorX-1,GetHeight()-archorY-1,archorX,archorY).MirrorTo(workspace.GetSub(GetWidth()+archorX,GetHeight()+archorY,archorX, archorY), AxisBoth);

		//workspace.Save("abc.pfm");
		CImage<ValueType, chan_num>  tar;
		tar.SetSize(GetRegion().AddBorder(archorX*2, archorY*2));

        switch(chan_num) {
		case 1:
			ipp_cpp::ippiConvValid_C1R(IPPARG_IMG(workspace), workspace, IPPARG_IMG(img), img, IPPARG_IMG(tar));
			break;
		case 3: 
    		ipp_cpp::ippiConvFull_C3R(IPPARG_IMG(workspace), workspace, IPPARG_IMG(img), img, IPPARG_IMG(tar));
			break;
		case 4:
			ipp_cpp::ippiConvFull_AC4R(IPPARG_IMG(workspace), workspace, IPPARG_IMG(img), img, IPPARG_IMG(tar));
			break;
		}
		CopyFrom(tar.GetSub_Inside((UINT)archorX*2, (UINT)archorY*2));
	}

	IPP_FUNC void SeparableFilter(LPCFLOAT pKernel_1D,UINT KernelSize,Image_Ref& workspace)
	{	ASSERT(KernelSize&1); //must be odd
		ASSERT_ARRAY(pKernel_1D,KernelSize);

		int archor = (int)KernelSize>>1;
		ASSERT_SIZE(*this,workspace.GetSub_Inside((UINT)archor)); //workspace must be initialized

		Image_Ref& dstwin = workspace.GetSub_Inside((UINT)archor);
		switch(chan_num) {
			case 1: //copy border along y and filter
					CopyTo(dstwin);
					GetSub(0,0,GetWidth(),archor).MirrorTo(workspace.GetSub(archor,0,GetWidth(),archor),AxisHorizontal);
					GetSub(0,GetHeight()-archor-1,GetWidth(),archor).MirrorTo(workspace.GetSub(archor,GetHeight()+archor,GetWidth(),archor),AxisHorizontal);
					ipp_cpp::ippiFilterColumn32f_C1R(IPPARG_IMG(dstwin),IPPARG_IMG(*this),*this,pKernel_1D,(int)KernelSize,archor);

					//copy with border along x and fileter
					CopyTo(dstwin);
					GetSub(0,0,archor,GetHeight()).MirrorTo(workspace.GetSub(0,archor,archor,GetHeight()),AxisVertical);
					GetSub(GetWidth()-archor-1,0,archor,GetHeight()).MirrorTo(workspace.GetSub(GetWidth()+archor,archor,archor,GetHeight()),AxisVertical);

					ipp_cpp::ippiFilterRow32f_C1R(IPPARG_IMG(dstwin),IPPARG_IMG(*this),*this,pKernel_1D,(int)KernelSize,archor);
					break;
			case 3: 
					CopyTo(dstwin);
					GetSub(0,0,GetWidth(),archor).MirrorTo(workspace.GetSub(archor,0,GetWidth(),archor),AxisHorizontal);
					GetSub(0,GetHeight()-archor-1,GetWidth(),archor).MirrorTo(workspace.GetSub(archor,GetHeight()+archor,GetWidth(),archor),AxisHorizontal);
					ipp_cpp::ippiFilterColumn32f_C3R(IPPARG_IMG(dstwin),IPPARG_IMG(*this),*this,pKernel_1D,(int)KernelSize,archor);

					CopyTo(dstwin);
					GetSub(0,0,archor,GetHeight()).MirrorTo(workspace.GetSub(0,archor,archor,GetHeight()),AxisVertical);
					GetSub(GetWidth()-archor-1,0,archor,GetHeight()).MirrorTo(workspace.GetSub(GetWidth()+archor,archor,archor,GetHeight()),AxisVertical);
					ipp_cpp::ippiFilterRow32f_C3R(IPPARG_IMG(dstwin),IPPARG_IMG(*this),*this,pKernel_1D,(int)KernelSize,archor);
					break;
			case 4: 
					CopyTo(dstwin);
					GetSub(0,0,GetWidth(),archor).MirrorTo(workspace.GetSub(archor,0,GetWidth(),archor),AxisHorizontal);
					GetSub(0,GetHeight()-archor-1,GetWidth(),archor).MirrorTo(workspace.GetSub(archor,GetHeight()+archor,GetWidth(),archor),AxisHorizontal);
					ipp_cpp::ippiFilterColumn32f_C4R(IPPARG_IMG(dstwin),IPPARG_IMG(*this),*this,pKernel_1D,(int)KernelSize,archor);

					CopyTo(dstwin);
					GetSub(0,0,archor,GetHeight()).MirrorTo(workspace.GetSub(0,archor,archor,GetHeight()),AxisVertical);
					GetSub(GetWidth()-archor-1,0,archor,GetHeight()).MirrorTo(workspace.GetSub(GetWidth()+archor,archor,archor,GetHeight()),AxisVertical);
					ipp_cpp::ippiFilterRow32f_C4R(IPPARG_IMG(dstwin),IPPARG_IMG(*this),*this,pKernel_1D,(int)KernelSize,archor);
					break;
	}	}
	IPP_FUNC void Gauss_WxH(UINT FilterWidth, UINT FilterHeight, float SmoothRatio = 4.0f)
	{
		num::Tabulated_Function<float> filter;
		CImage<ValueType, chan_num>  tmp;

		if (FilterWidth > 1)
		{
			FilterWidth = min(FilterWidth, GetWidth());
			FilterWidth |= 1;	

			filter.SetSize(FilterWidth);
			filter.GaussKernel(SmoothRatio);

			tmp.SetSize(GetRegion().AddBorder((FilterWidth>>1), 0));
			SeparableFilter_Row(filter, FilterWidth, tmp);
		}
		if (FilterHeight > 1)
		{
			FilterHeight = min(FilterHeight, GetHeight());
			FilterHeight |= 1;
			
			filter.SetSize(FilterHeight);
			filter.GaussKernel(SmoothRatio);

			tmp.SetSize(GetRegion().AddBorder(0, (FilterHeight>>1)));
			SeparableFilter_Col(filter, FilterHeight, tmp);
		}
	}
	IPP_FUNC void SeparableFilter_Col(LPCFLOAT pKernel_1D,UINT KernelSize,Image_Ref& workspace)
	{	ASSERT(KernelSize&1); //must be odd
		ASSERT_ARRAY(pKernel_1D,KernelSize);

		int archor = (int)KernelSize>>1;
		ASSERT_SIZE(*this,workspace.GetSub_Inside(0, (UINT)archor)); //workspace must be initialized

		Image_Ref& dstwin = workspace.GetSub_Inside(0, (UINT)archor);
		switch(chan_num) {
			case 1: //copy border along y and filter
					CopyTo(dstwin);
					GetSub(0,0,GetWidth(),archor).MirrorTo(workspace.GetSub(0,0,GetWidth(),archor),AxisHorizontal);
					GetSub(0,GetHeight()-archor-1,GetWidth(),archor).MirrorTo(workspace.GetSub(0,GetHeight()+archor,GetWidth(),archor),AxisHorizontal);
					ipp_cpp::ippiFilterColumn32f_C1R(IPPARG_IMG(dstwin),IPPARG_IMG(*this),*this,pKernel_1D,(int)KernelSize,archor);
					break;
			case 3: 
					CopyTo(dstwin);
					GetSub(0,0,GetWidth(),archor).MirrorTo(workspace.GetSub(0,0,GetWidth(),archor),AxisHorizontal);
					GetSub(0,GetHeight()-archor-1,GetWidth(),archor).MirrorTo(workspace.GetSub(0,GetHeight()+archor,GetWidth(),archor),AxisHorizontal);
					ipp_cpp::ippiFilterColumn32f_C3R(IPPARG_IMG(dstwin),IPPARG_IMG(*this),*this,pKernel_1D,(int)KernelSize,archor);
					break;
			case 4: 
					CopyTo(dstwin);
					GetSub(0,0,GetWidth(),archor).MirrorTo(workspace.GetSub(0,0,GetWidth(),archor),AxisHorizontal);
					GetSub(0,GetHeight()-archor-1,GetWidth(),archor).MirrorTo(workspace.GetSub(0,GetHeight()+archor,GetWidth(),archor),AxisHorizontal);
					ipp_cpp::ippiFilterColumn32f_C4R(IPPARG_IMG(dstwin),IPPARG_IMG(*this),*this,pKernel_1D,(int)KernelSize,archor);
					break;
	}	}
	IPP_FUNC void SeparableFilter_Row(LPCFLOAT pKernel_1D,UINT KernelSize,Image_Ref& workspace)
	{	ASSERT(KernelSize&1); //must be odd
		ASSERT_ARRAY(pKernel_1D,KernelSize);

		int archor = (int)KernelSize>>1;
		ASSERT_SIZE(*this,workspace.GetSub_Inside((UINT)archor, 0)); //workspace must be initialized

		Image_Ref& dstwin = workspace.GetSub_Inside((UINT)archor, 0);
		switch(chan_num) {
			case 1: //copy with border along x and filter
					CopyTo(dstwin);
					GetSub(0,0,archor,GetHeight()).MirrorTo(workspace.GetSub(0,0,archor,GetHeight()),AxisVertical);
					GetSub(GetWidth()-archor-1,0,archor,GetHeight()).MirrorTo(workspace.GetSub(GetWidth()+archor,0,archor,GetHeight()),AxisVertical);

					ipp_cpp::ippiFilterRow32f_C1R(IPPARG_IMG(dstwin),IPPARG_IMG(*this),*this,pKernel_1D,(int)KernelSize,archor);
					break;
			case 3: 
					CopyTo(dstwin);
					GetSub(0,0,archor,GetHeight()).MirrorTo(workspace.GetSub(0,0,archor,GetHeight()),AxisVertical);
					GetSub(GetWidth()-archor-1,0,archor,GetHeight()).MirrorTo(workspace.GetSub(GetWidth()+archor,0,archor,GetHeight()),AxisVertical);
					ipp_cpp::ippiFilterRow32f_C3R(IPPARG_IMG(dstwin),IPPARG_IMG(*this),*this,pKernel_1D,(int)KernelSize,archor);
					break;
			case 4: 
					CopyTo(dstwin);
					GetSub(0,0,archor,GetHeight()).MirrorTo(workspace.GetSub(0,0,archor,GetHeight()),AxisVertical);
					GetSub(GetWidth()-archor-1,0,archor,GetHeight()).MirrorTo(workspace.GetSub(GetWidth()+archor,0,archor,GetHeight()),AxisVertical);
					ipp_cpp::ippiFilterRow32f_C4R(IPPARG_IMG(dstwin),IPPARG_IMG(*this),*this,pKernel_1D,(int)KernelSize,archor);
					break;
	}	}
	IPP_FUNC void GaussTo(Image_Ref& dst, UINT FilterSize,float SmoothRatio = 4.0f) const
	{	if( FilterSize > 1 )
		{	FilterSize = min(FilterSize,min(GetWidth(),GetHeight() ));
			if( FilterSize&1 ){}else{ FilterSize++; }

			num::Tabulated_Function<float>	filter;
			filter.SetSize(FilterSize);
			filter.GaussKernel(SmoothRatio);

			CImage<ValueType,chan_num>	tmp;
			tmp.SetSize(GetRegion().AddBorder(FilterSize>>1));
			SeparableFilterTo(dst,filter,FilterSize,tmp);
		}
	}
	IPP_FUNC void SeparableFilterTo(Image_Ref& dst, LPCFLOAT pKernel_1D, UINT KernelSize, Image_Ref& workspace) const
	{	ASSERT_SIZE(dst,*this);
		ASSERT(KernelSize&1); //must be odd
		ASSERT_ARRAY(pKernel_1D,KernelSize);

		int archor = (int)KernelSize>>1;
		ASSERT_SIZE(*this,workspace.GetSub_Inside((UINT)archor)); //workspace must be initialized

		Image_Ref& dstwin = workspace.GetSub_Inside((UINT)archor);
		switch(chan_num) {
			case 1: //copy border along y and filter
					CopyTo(dstwin);
					GetSub(0,0,GetWidth(),archor).MirrorTo(workspace.GetSub(archor,0,GetWidth(),archor),AxisHorizontal);
					GetSub(0,GetHeight()-archor-1,GetWidth(),archor).MirrorTo(workspace.GetSub(archor,GetHeight()+archor,GetWidth(),archor),AxisHorizontal);
					ipp_cpp::ippiFilterColumn32f_C1R(IPPARG_IMG(dstwin),IPPARG_IMG(dst),dst,pKernel_1D,(int)KernelSize,archor);

					//copy with border along x and fileter
					dst.CopyTo(dstwin);
					dst.GetSub(0,0,archor,GetHeight()).MirrorTo(workspace.GetSub(0,archor,archor,GetHeight()),AxisVertical);
					dst.GetSub(GetWidth()-archor-1,0,archor,GetHeight()).MirrorTo(workspace.GetSub(GetWidth()+archor,archor,archor,GetHeight()),AxisVertical);

					ipp_cpp::ippiFilterRow32f_C1R(IPPARG_IMG(dstwin),IPPARG_IMG(dst),dst,pKernel_1D,(int)KernelSize,archor);
					break;
			case 3: 
					CopyTo(dstwin);
					GetSub(0,0,GetWidth(),archor).MirrorTo(workspace.GetSub(archor,0,GetWidth(),archor),AxisHorizontal);
					GetSub(0,GetHeight()-archor-1,GetWidth(),archor).MirrorTo(workspace.GetSub(archor,GetHeight()+archor,GetWidth(),archor),AxisHorizontal);
					ipp_cpp::ippiFilterColumn32f_C3R(IPPARG_IMG(dstwin),IPPARG_IMG(dst),dst,pKernel_1D,(int)KernelSize,archor);

					dst.CopyTo(dstwin);
					dst.GetSub(0,0,archor,GetHeight()).MirrorTo(workspace.GetSub(0,archor,archor,GetHeight()),AxisVertical);
					dst.GetSub(GetWidth()-archor-1,0,archor,GetHeight()).MirrorTo(workspace.GetSub(GetWidth()+archor,archor,archor,GetHeight()),AxisVertical);
					ipp_cpp::ippiFilterRow32f_C3R(IPPARG_IMG(dstwin),IPPARG_IMG(dst),dst,pKernel_1D,(int)KernelSize,archor);
					break;
			case 4: 
					CopyTo(dstwin);
					GetSub(0,0,GetWidth(),archor).MirrorTo(workspace.GetSub(archor,0,GetWidth(),archor),AxisHorizontal);
					GetSub(0,GetHeight()-archor-1,GetWidth(),archor).MirrorTo(workspace.GetSub(archor,GetHeight()+archor,GetWidth(),archor),AxisHorizontal);
					ipp_cpp::ippiFilterColumn32f_C4R(IPPARG_IMG(dstwin),IPPARG_IMG(dst),dst,pKernel_1D,(int)KernelSize,archor);

					dst.CopyTo(dstwin);
					dst.GetSub(0,0,archor,GetHeight()).MirrorTo(workspace.GetSub(0,archor,archor,GetHeight()),AxisVertical);
					dst.GetSub(GetWidth()-archor-1,0,archor,GetHeight()).MirrorTo(workspace.GetSub(GetWidth()+archor,archor,archor,GetHeight()),AxisVertical);
					ipp_cpp::ippiFilterRow32f_C4R(IPPARG_IMG(dstwin),IPPARG_IMG(dst),dst,pKernel_1D,(int)KernelSize,archor);
					break;
	}	}
	IPP_FUNC void HaarWaveletFwd(int level) // much be power of two
	{
		{	int mask = (1<<level)-1;
			ASSERT((GetWidth()&mask)==0);
			ASSERT((GetHeight()&mask)==0);
		}

		CImage<ValueType,chan_num>	aux;
		aux.SetSize(GetWidth(),GetHeight());

		IppiSize	roi = *this;
		for(int i=0;i<level;i++)
		{	
			switch(chan_num)
			{	case 1: ipp_cpp::ippiHaarWTFwd_C1R(IPPARG_IMG(*this),IPPARG_IMG(aux),roi); break;
				case 3: break;
				case 4: break;
			}
			*this = aux;
			roi.width>>=1;
			roi.height>>=1;
		}
	}
	IPP_FUNC void HaarWaveletInv(int level) // much be power of two
	{
		{	int mask = (1<<level)-1;
			ASSERT((GetWidth()&mask)==0);
			ASSERT((GetHeight()&mask)==0);
		}

		ipp::CImage<ValueType,chan_num>	aux;
		aux.SetSize(GetWidth(),GetHeight());

		IppiSize	roi = *this;
		roi.width>>=level;
		roi.height>>=level;
		for(int i=0;i<level;i++)
		{	
			roi.width<<=1;
			roi.height<<=1;
			switch(chan_num)
			{	case 1: ipp_cpp::ippiHaarWTInv_C1R(IPPARG_IMG(*this),IPPARG_IMG(aux),roi); break;
				case 3: break;
				case 4: break;
			}
			GetSub(0,0,roi.width,roi.height) = aux.GetSub(0,0,roi.width,roi.height);
		}
	}
	IPP_FUNC void DotProduct(const Image_Ref& src, PixelType& out)
	{	ASSERT_SIZE(*this,src);
		switch(chan_num) {
			case 1: ipp_cpp::ippiConvValid_C1R(IPPARG_IMG(*this),*this,IPPARG_IMG(src),*this,value,1); break;
			case 3: ipp_cpp::ippiConvValid_C3R(IPPARG_IMG(*this),*this,IPPARG_IMG(src),*this,value,1); break;
			case 4: ipp_cpp::ippiConvValid_AC4R(IPPARG_IMG(*this),*this,IPPARG_IMG(src),*this,value,1); break;
	}	}
	IPP_FUNC void RGB2YUV(Image_Ref& dst) const
	{	ASSERT_SIZE(*this,dst);
		ASSERT(GetBPV()==8); //LDR only 
		switch(chan_num) {
			case 1: ASSERT(0);
			case 3: ipp_cpp::ippiRGBToYUV_C3R(IPPARG_IMG(*this),IPPARG_IMG(dst),*this); break;
			case 4: ipp_cpp::ippiRGBToYUV_AC4R(IPPARG_IMG(*this),IPPARG_IMG(dst),*this); break;
	}	}
	IPP_FUNC void YUV2RGB(Image_Ref& dst) const
	{	ASSERT_SIZE(*this,dst);
		ASSERT(GetBPV()==8); //LDR only 
		switch(chan_num) {
			case 1: ASSERT(0);
			case 3: ipp_cpp::ippiYUVToRGB_C3R(IPPARG_IMG(*this),IPPARG_IMG(dst),*this); break;
			case 4: ipp_cpp::ippiYUVToRGB_AC4R(IPPARG_IMG(*this),IPPARG_IMG(dst),*this); break;
	}	}
	IPP_FUNC void RGB2HSV(Image_Ref& dst) const
	{	ASSERT_SIZE(*this,dst);
		ASSERT(GetBPV()==8 || GetBPV()==8); //LDR only 
		switch(chan_num) {
			case 1: ASSERT(0);
			case 3: ipp_cpp::ippiRGBToHSV_C3R(IPPARG_IMG(*this),IPPARG_IMG(dst),*this); break;
			case 4: ipp_cpp::ippiRGBToHSV_AC4R(IPPARG_IMG(*this),IPPARG_IMG(dst),*this); break;
	}	}
	IPP_FUNC void HSV2RGB(Image_Ref& dst) const
	{	ASSERT_SIZE(*this,dst);
		ASSERT(GetBPV()==8 || GetBPV()==8); //LDR only 
		switch(chan_num) {
			case 1: ASSERT(0);
			case 3: ipp_cpp::ippiHSVToRGB_C3R(IPPARG_IMG(*this),IPPARG_IMG(dst),*this); break;
			case 4: ipp_cpp::ippiHSVToRGB_AC4R(IPPARG_IMG(*this),IPPARG_IMG(dst),*this); break;
	}	}
	IPP_FUNC void RGB2LUV(Image_Ref& dst) const
	{	ASSERT_SIZE(*this,dst);
		switch(chan_num) {
			case 1: ASSERT(0);
			case 3: ipp_cpp::ippiRGBToLUV_C3R(IPPARG_IMG(*this),IPPARG_IMG(dst),*this); break;
			case 4: ipp_cpp::ippiRGBToLUV_AC4R(IPPARG_IMG(*this),IPPARG_IMG(dst),*this); break;
	}	}
	IPP_FUNC void LUV2RGB(Image_Ref& dst) const
	{	ASSERT_SIZE(*this,dst);
		switch(chan_num) {
			case 1: ASSERT(0);
			case 3: ipp_cpp::ippiLUVToRGB_C3R(IPPARG_IMG(*this),IPPARG_IMG(dst),*this); break;
			case 4: ipp_cpp::ippiLUVToRGB_AC4R(IPPARG_IMG(*this),IPPARG_IMG(dst),*this); break;
	}	}
	IPP_FUNC void RGB2HLS(Image_Ref& dst) const
	{	ASSERT_SIZE(*this,dst);
		switch(chan_num) {
			case 1: ASSERT(0);
			case 3: ipp_cpp::ippiRGBToHLS_C3R(IPPARG_IMG(*this),IPPARG_IMG(dst),*this); break;
			case 4: ipp_cpp::ippiRGBToHLS_AC4R(IPPARG_IMG(*this),IPPARG_IMG(dst),*this); break;
	}	}
	IPP_FUNC void HLS2RGB(Image_Ref& dst) const
	{	ASSERT_SIZE(*this,dst);
		switch(chan_num) {
			case 1: ASSERT(0);
			case 3: ipp_cpp::ippiHLSToRGB_C3R(IPPARG_IMG(*this),IPPARG_IMG(dst),*this); break;
			case 4: ipp_cpp::ippiHLSToRGB_AC4R(IPPARG_IMG(*this),IPPARG_IMG(dst),*this); break;
	}	}
	IPP_FUNC void BGR2Lab(Image_Ref& dst) const
	{	ASSERT_SIZE(*this,dst);
		ASSERT(GetBPV()==8); //LDR only 
		switch(chan_num) {
			case 1: ASSERT(0);
			case 3: ipp_cpp::ippiBGRToLab_C3R(IPPARG_IMG(*this),IPPARG_IMG(dst),*this); break;
			case 4: ASSERT(0);
	}	}
	IPP_FUNC void Lab2BGR(Image_Ref& dst) const
	{	ASSERT_SIZE(*this,dst);
		ASSERT(GetBPV()==8 || GetBPV()==16); //LDR only 
		switch(chan_num) {
			case 1: ASSERT(0);
			case 3: ipp_cpp::ippiLabToBGR_C3R(IPPARG_IMG(*this),IPPARG_IMG(dst),*this); break;
			case 4: ASSERT(0);
	}	}
	IPP_FUNC void SwapChannels_8u(LPCSTR pOrder = "\2\0\0\0\1\0\0\0\0\0\0\0\3\0\0\0")  //RGB<-->BGR, BGRA<-->RGBA by default
	{	ASSERT(GetBPV()==8);
		switch(chan_num) {
			case 1: ASSERT(0);
			case 3: IPPCALL(ippiSwapChannels_8u_C3IR)(IPPARG_IMG(*this),*this,(const int *)pOrder); break;
			case 4: IPPCALL(ippiSwapChannels_8u_C4IR)(IPPARG_IMG(*this),*this,(const int *)pOrder); break;
	}	}
	IPP_FUNC void SwapChannelsTo(Image_Ref& dst, num::Vec3i& Order = num::Vec3u(2,1,0))  //RGB<-->BGR by default
	{	ASSERT_SIZE(*this,dst);
		ASSERT(chan_num == 3 || chan_num == 4);
		switch(chan_num) {
			case 1: ASSERT(0);
			case 3: ipp_cpp::ippiSwapChannels_C3R(IPPARG_IMG(*this),IPPARG_IMG(dst),*this,(const int *)&Order); break;
			case 4: ipp_cpp::ippiSwapChannels_AC4R(IPPARG_IMG(*this),IPPARG_IMG(dst),*this,(const int *)&Order); break;
	}	}
	IPP_FUNC void AlphaBlend(const Image_Ref& src1,const Image_Ref& src2,float alpha_1, float alpha_2=1.0f,int AlphaType = BlendMethod_AlphaOver)
	{	ASSERT_SIZE(*this,src1);
		ASSERT_SIZE(*this,src2);
		ASSERT(GetBPV()==8 || GetBPV()==16); //LDR only 
		ASSERT(alpha_1>=0.0f && alpha_1<=1.0f);
		ASSERT(alpha_2>=0.0f && alpha_2<=1.0f);
		num::Vec2i a;
		a.x = ((int)rt::TypeTraits<ValueType>::MaxVal())*alpha_1;
		a.y = ((int)rt::TypeTraits<ValueType>::MaxVal())*alpha_2;

		switch(chan_num) {
			case 1: ipp_cpp::ippiAlphaComp_C1R(	IPPARG_IMG(src1),a.x,
										IPPARG_IMG(src2),a.y,
										IPPARG_IMG(*this),*this,(IppiAlphaType)BlendMethod_AlphaOver); 
					break;
			case 3: ipp_cpp::ippiAlphaComp_C3R(	IPPARG_IMG(src1),a.x,
										IPPARG_IMG(src2),a.y,
										IPPARG_IMG(*this),*this,(IppiAlphaType)BlendMethod_AlphaOver); 
					break;
			case 4: ipp_cpp::ippiAlphaComp_C4R(	IPPARG_IMG(src1),a.x,
										IPPARG_IMG(src2),a.y,
										IPPARG_IMG(*this),*this,(IppiAlphaType)BlendMethod_AlphaOver); 
					break;
	}	}
	IPP_FUNC void AlphaBlend(const Image_Ref& src1,const Image_Ref& src2,int AlphaType = BlendMethod_AlphaOver)
	{	ASSERT_SIZE(*this,src1);
		ASSERT_SIZE(*this,src2);
		ASSERT(GetBPV()==8 || GetBPV()==16); //LDR only 
		ASSERT(chan_num==4);
		ipp_cpp::ippiAlphaComp_AC4R(IPPARG_IMG(src1),IPPARG_IMG(src2),IPPARG_IMG(*this),*this,(IppiAlphaType)AlphaType);
	}
	IPP_FUNC void Bitwise_And(const PixelType & p)
	{	switch(chan_num) {
			case 1: ipp_cpp::ippiAnd_C1R(p.x,IPPARG_IMG(src),IPPARG_IMG(*this),*this); break;
			case 3: ipp_cpp::ippiAnd_C3R(p,IPPARG_IMG(src),IPPARG_IMG(*this),*this); break;
			case 4: ipp_cpp::ippiAnd_C4R(p,IPPARG_IMG(src),IPPARG_IMG(*this),*this); break;
	}	}
	IPP_FUNC void Threshold_LessThenSet(const PixelType& Threshold,const PixelType& Val)
	{	switch(chan_num) {
		case 1: ipp_cpp::ippiThreshold_LTVal_C1R(IPPARG_IMG(*this),*this,Threshold[0],Val[0]); break;
		case 3: ipp_cpp::ippiThreshold_LTVal_C3R(IPPARG_IMG(*this),*this,Threshold,Val); break;
		case 4: ipp_cpp::ippiThreshold_LTVal_C4R(IPPARG_IMG(*this),*this,Threshold,Val); break;
	}	}
	IPP_FUNC void ThresholdTo_LessThenSet(const PixelType& Threshold,const PixelType& Val,CImage& x) const
	{	ASSERT_SIZE(*this,x);
		switch(chan_num) {
		case 1: ipp_cpp::ippiThreshold_LTVal_C1R(IPPARG_IMG(*this),IPPARG_IMG(x),x,Threshold[0],Val[0]); break;
		case 3: ipp_cpp::ippiThreshold_LTVal_C3R(IPPARG_IMG(*this),IPPARG_IMG(x),x,Threshold,Val); break;
		case 4: ipp_cpp::ippiThreshold_LTVal_C4R(IPPARG_IMG(*this),IPPARG_IMG(x),x,Threshold,Val); break;
	}	}
	IPP_FUNC void Threshold_GreatThenSet(const PixelType& Threshold,const PixelType& Val)
	{	switch(chan_num) {
		case 1: ipp_cpp::ippiThreshold_GTVal_C1R(IPPARG_IMG(*this),*this,Threshold[0],Val[0]); break;
		case 3: ipp_cpp::ippiThreshold_GTVal_C3R(IPPARG_IMG(*this),*this,Threshold,Val); break;
		case 4: ipp_cpp::ippiThreshold_GTVal_C4R(IPPARG_IMG(*this),*this,Threshold,Val); break;
	}	}
	IPP_FUNC void ThresholdTo_GreatThenSet(const PixelType& Threshold,const PixelType& Val,CImage& x) const
	{	ASSERT_SIZE(*this,x);
		switch(chan_num) {
		case 1: ipp_cpp::ippiThreshold_GTVal_C1R(IPPARG_IMG(*this),IPPARG_IMG(x),x,Threshold[0],Val[0]); break;
		case 3: ipp_cpp::ippiThreshold_GTVal_C3R(IPPARG_IMG(*this),IPPARG_IMG(x),x,Threshold,Val); break;
		case 4: ipp_cpp::ippiThreshold_GTVal_C4R(IPPARG_IMG(*this),IPPARG_IMG(x),x,Threshold,Val); break;
	}	}
	IPP_FUNC void HistogramEven(	rt::Vec<Ipp32s,chan_num>* pHistBins, //BinCount
										const PixelType& Min_val, const PixelType& Max_val, UINT BinCount
										) const
	{	ASSERT_ARRAY( pHistBins, BinCount );
		typename rt::TypeTraits<PixelType>::t_Val t_level_pixel;
		rt::Buffer<BYTE> temp;
		temp.SetSize(sizeof(t_level_pixel)*(BinCount+1) + sizeof(Ipp32s)*chan_num*BinCount);
	
		typedef typename rt::TypeTraits<ValueType>::t_Val t_level;
		t_level* pLevels = (t_level*)temp.Begin();

		Ipp32s* pHist = (Ipp32s*)&temp[sizeof(t_level_pixel)*(BinCount+1)];
						
		switch(chan_num) {
		case 1: {	for(int i=0;i<=BinCount;i++)
						pLevels[0*(BinCount+1) + i] = (Min_val[0]*(BinCount-i) + Max_val[0]*i)/BinCount;
					ipp_cpp::ippiHistogramRange_C1R(IPPARG_IMG(*this),*this,pHist,pLevels,BinCount+1);
					// merge results
					ipp::memcpy32AL(pHistBins,Bins,sizeof(Ipp32s)*BinCount);
				} break;
		case 3: {	for(int i=0;i<=BinCount;i++)
					{
						pLevels[0*(BinCount+1) + i] = (Min_val[0]*(BinCount-i) + Max_val[0]*i)/BinCount;
						pLevels[1*(BinCount+1) + i] = (Min_val[1]*(BinCount-i) + Max_val[1]*i)/BinCount;
						pLevels[2*(BinCount+1) + i] = (Min_val[2]*(BinCount-i) + Max_val[2]*i)/BinCount;
					}
					Ipp32s* pBins[3];
					pBins[0] = &pHist[BinCount*0];
					pBins[1] = &pHist[BinCount*1];
					pBins[2] = &pHist[BinCount*2];

					const typename SMM::traits::Type_MoreAccurate<Ipp32s,ValueType>::t_Accurate* pLevel_all[3];
					pLevel_all[0] = &pLevels[(BinCount+1)*0];
					pLevel_all[1] = &pLevels[(BinCount+1)*1];
					pLevel_all[2] = &pLevels[(BinCount+1)*2];

					int BinCounts[3];
					BinCounts[0] = BinCounts[1] = BinCounts[2] = BinCount+1;

					ipp_cpp::ippiHistogramRange_C3R(IPPARG_IMG(*this),*this,pBins,pLevel_all,BinCounts);

					// merge results
					for(UINT i=0;i<BinCount;i++)
					{
						pHistBins[i].x = pHist[BinCount*0 + i];
						pHistBins[i].y = pHist[BinCount*1 + i];
						pHistBins[i].z = pHist[BinCount*2 + i];
					}

				} break;
		case 4: {	for(int i=0;i<=BinCount;i++)
					{
						pLevels[0*(BinCount+1) + i] = (Min_val[0]*(BinCount-i) + Max_val[0]*i)/BinCount;
						pLevels[1*(BinCount+1) + i] = (Min_val[1]*(BinCount-i) + Max_val[1]*i)/BinCount;
						pLevels[2*(BinCount+1) + i] = (Min_val[2]*(BinCount-i) + Max_val[2]*i)/BinCount;
						pLevels[3*(BinCount+1) + i] = (Min_val[3]*(BinCount-i) + Max_val[3]*i)/BinCount;
					}
					Ipp32s* pBins[4];
					pBins[0] = &pHist[BinCount*0];
					pBins[1] = &pHist[BinCount*1];
					pBins[2] = &pHist[BinCount*2];
					pBins[3] = &pHist[BinCount*3];

					const typename SMM::traits::Type_MoreAccurate<Ipp32s,ValueType>::t_Accurate* pLevel[4];
					pLevel[0] = &pLevels[(BinCount+1)*0];
					pLevel[1] = &pLevels[(BinCount+1)*1];
					pLevel[2] = &pLevels[(BinCount+1)*2];
					pLevel[3] = &pLevels[(BinCount+1)*3];

					int BinCounts[4];
					BinCounts[0] = BinCounts[3] = BinCounts[1] = BinCounts[2] = BinCount+1;

					ipp_cpp::ippiHistogramRange_C4R(IPPARG_IMG(*this),*this,pBins,pLevel,BinCounts);
					// merge results
					for(UINT i=0;i<BinCount;i++)
					{
						pHistBins[i].x = pHist[BinCount*0 + i];
						pHistBins[i].y = pHist[BinCount*1 + i];
						pHistBins[i].z = pHist[BinCount*2 + i];
						pHistBins[i].w = pHist[BinCount*4 + i];
					}
				} break;
		}
	}
	IPP_FUNC void Difference(const Image_Ref& x1, const Image_Ref& x2)
	{	ASSERT_SIZE(*this,x1);
		ASSERT_SIZE(*this,x2);
		ipp_cpp::ippiAbsDiff_C1R(	IPPARG_IMG(x1),IPPARG_IMG(x2),IPPARG_IMG(*this),
									ipp::CIppiSize(GetWidth()*chan_num, GetHeight()));
	}
	IPP_FUNC void Sub(const Image_Ref& x)
	{	ASSERT_SIZE(*this,x);
		switch(chan_num) {	
			case 1: ipp_cpp::ippiSub_C1R(IPPARG_IMG(x),IPPARG_IMG(*this),*this); break;
			case 3: ipp_cpp::ippiSub_C3R(IPPARG_IMG(x),IPPARG_IMG(*this),*this); break;
			case 4: ipp_cpp::ippiSub_C4R(IPPARG_IMG(x),IPPARG_IMG(*this),*this); break;
	}	}
	IPP_FUNC void SubTo(const Image_Ref& src1,Image_Ref& dst) const
	{	ASSERT_SIZE(*this,src1); 
		ASSERT_SIZE(*this,dst);
		switch(chan_num) {	
			case 1: ipp_cpp::ippiSub_C1R(IPPARG_IMG(src1),IPPARG_IMG(*this),IPPARG_IMG(dst),*this); break;
			case 3: ipp_cpp::ippiSub_C3R(IPPARG_IMG(src1),IPPARG_IMG(*this),IPPARG_IMG(dst),*this); break;
			case 4: ipp_cpp::ippiSub_C4R(IPPARG_IMG(src1),IPPARG_IMG(*this),IPPARG_IMG(dst),*this); break;
	}	}
	IPP_FUNC void Sub(const PixelType& val)
	{	switch(chan_num) {	
			case 1: ipp_cpp::ippiSub_C1R(val[0],IPPARG_IMG(*this),*this); break;
			case 3: ipp_cpp::ippiSub_C3R(val,IPPARG_IMG(*this),*this); break;
			case 4: ipp_cpp::ippiSub_C4R(val,IPPARG_IMG(*this),*this); break;
	}	}
	IPP_FUNC void SubTo(const PixelType& val,Image_Ref& dst) const
	{	ASSERT_SIZE(*this,dst);
		switch(chan_num) {	
			case 1: ipp_cpp::ippiSub_C1R(IPPARG_IMG(src1),val[0],IPPARG_IMG(*this),*this); break;
			case 3: ipp_cpp::ippiSub_C3R(IPPARG_IMG(src1),val,IPPARG_IMG(*this),*this); break;
			case 4: ipp_cpp::ippiSub_C4R(IPPARG_IMG(src1),val,IPPARG_IMG(*this),*this); break;
	}	}
	IPP_FUNC void BoxFilter(const CIppiSize& isize)
	{	CIppiSize size = isize;
		if(size.width&1){}else{ size.width++; }
		if(size.height&1){}else{ size.height++; }
		CIppiPoint pt(size.width>>1,size.height>>1);
		t_ImageWin win = GetSub_Inside(pt.x,pt.y);
		switch(chan_num) {
			case 1: ipp_cpp::ippiFilterBox_C1R(IPPARG_IMG(win),win,size,pt); break;
			case 3: ipp_cpp::ippiFilterBox_C3R(IPPARG_IMG(win),win,size,pt); break;
			case 4: ipp_cpp::ippiFilterBox_C4R(IPPARG_IMG(win),win,size,pt); break;
	}	}
	IPP_FUNC void Gamma()
	{	switch(chan_num) {	
			case 1: ASSERT(0); break;
			case 3: ipp_cpp::ippiGammaFwd_C3R(IPPARG_IMG(*this),*this); break;
			case 4: ipp_cpp::ippiGammaFwd_AC4R(IPPARG_IMG(*this),*this); break;
	}	}
	IPP_FUNC void GammaTo(Image_Ref& dst) const
	{	ASSERT_SIZE(*this,dst);
		switch(chan_num) {	
			case 1: ASSERT(0); break;
			case 3: ipp_cpp::ippiGammaFwd_C3R(IPPARG_IMG(*this),IPPARG_IMG(dst),*this); break;
			case 4: ipp_cpp::ippiGammaFwd_AC4R(IPPARG_IMG(*this),IPPARG_IMG(dst),*this); break;
	}	}
	IPP_FUNC void GammaInv()
	{	switch(chan_num) {	
			case 1: ASSERT(0); break;
			case 3: ipp_cpp::ippiGammaInv_C3R(IPPARG_IMG(*this),*this); break;
			case 4: ipp_cpp::ippiGammaInv_AC4R(IPPARG_IMG(*this),*this); break;
	}	}
	IPP_FUNC void GammaInvTo(Image_Ref& dst) const
	{	ASSERT_SIZE(*this,dst);
		switch(chan_num) {	
			case 1: ASSERT(0); break;
			case 3: ipp_cpp::ippiGammaInv_C3R(IPPARG_IMG(*this),IPPARG_IMG(dst),*this); break;
			case 4: ipp_cpp::ippiGammaInv_AC4R(IPPARG_IMG(*this),IPPARG_IMG(dst),*this); break;
	}	}
	IPP_FUNC void BackwardRemap(const Image1C_Ref& map_x, const Image1C_Ref& map_y, const Image_Ref& src)
	{	ASSERT_SIZE(*this,map_x);
		ASSERT_SIZE(*this,map_y);
		IppiRect rc;
		rc.x = rc.y = 0;
		rc.width = src.GetWidth();
		rc.height= src.GetHeight();
		switch(chan_num) {	
			case 1: ipp_cpp::ippiRemap_C1R(IPPARG_IMG2(src),rc,IPPARG_IMG(map_x),IPPARG_IMG(map_y),IPPARG_IMG(*this),*this,GetEnv()->InterpolationMode); break;
			case 3: ipp_cpp::ippiRemap_C3R(IPPARG_IMG2(src),rc,IPPARG_IMG(map_x),IPPARG_IMG(map_y),IPPARG_IMG(*this),*this,GetEnv()->InterpolationMode); break;
			case 4: ipp_cpp::ippiRemap_C4R(IPPARG_IMG2(src),rc,IPPARG_IMG(map_x),IPPARG_IMG(map_y),IPPARG_IMG(*this),*this,GetEnv()->InterpolationMode); break;
	}	}

	/////////////////////////////////////////////////
	// Functions for color images only
	//__static_if(channel == 3)    VC05 compiler fails here, internal error :'(
	//{

	IPP_FUNC void ChannelCompose(const Image1C_Ref& r, const Image1C_Ref& g, const Image1C_Ref& b)
	{	ASSERT_STATIC(channel == 3);
		ASSERT_SIZE(*this,r); ASSERT_SIZE(*this,g); ASSERT_SIZE(*this,b);
		ASSERT(r.GetStep() == g.GetStep());
		ASSERT(b.GetStep() == g.GetStep());
		
		const t_Value* pCh[3] = { r,g,b };
		ipp_cpp::ippiCopy_P3C3R(pCh,r.GetStep(),IPPARG_IMG(*this),*this);
	}
	IPP_FUNC void ChannelSplit(Image1C_Ref& r, Image1C_Ref& g, Image1C_Ref& b) const
	{	ASSERT_STATIC(channel == 3);
		ASSERT_SIZE(*this,r); ASSERT_SIZE(*this,g); ASSERT_SIZE(*this,b);
		ASSERT(r.GetStep() == g.GetStep());
		ASSERT(b.GetStep() == g.GetStep());
		
		t_Value* pCh[3] = { r,g,b };
		ipp_cpp::ippiCopy_C3P3R(IPPARG_IMG(*this),pCh,r.GetStep(),*this);
	}
	//} // __static_if(channel == 3)

	// End of Mapped IPPI functions
	///////////////////////////////////////////////////////////////////////////////////////


	////////////////////////////////////////////////////////////////////////////////////////
	// User Functions
	template<typename t_Val2, class t_BaseVec2>
	void DownsampleTo( CImage<t_Val2,channel,t_BaseVec2> & x) const
	{	ASSERT(GetWidth()/2 == x.GetWidth());
		ASSERT(GetHeight()/2 == x.GetHeight());
		for(UINT y=0;y<x.GetHeight();y++)
		{	const PixelType* pSrc = GetLineAddress(y*2);
			CImage_Ref<t_Val2,channel>::PixelType* pDst = x.GetLineAddress(y);
			CImage_Ref<t_Val2,channel>::PixelType* pDstEnd = &pDst[x.GetWidth()];
			for(;pDst<pDstEnd;pDst++,pSrc+=2)
				pDst->BilinearInterpolate(	*pSrc, *Scanline_Next(pSrc) );
		}
	}

protected:
	/////////////////////////////////////////////////////////////
	//// Bitmap conversion
	void _ExportToBitmap( LPBYTE pData_BGR, UINT ch, UINT w, UINT h, UINT step, BOOL NeedFlip ) const
	{	switch(ch){
		case 1:	{ CImage_Ref<BYTE,1> ref((num::Vec1b*)pData_BGR,w,h,step); ref.CopyFrom(*this); if(NeedFlip)ref.Mirror(); break; }
		case 3: { CImage_Ref<BYTE,3> ref((num::Vec3b*)pData_BGR,w,h,step); ref.CopyFrom(*this); ref.SwapChannels_8u(); if(NeedFlip)ref.Mirror(); break; }
		case 4: { CImage_Ref<BYTE,4> ref((num::Vec4b*)pData_BGR,w,h,step); ref.CopyFrom(*this); ref.SwapChannels_8u(); if(NeedFlip)ref.Mirror(); break; }
	}	}
	void _ImportFromBitmap( LPBYTE pData_BGR, UINT ch, UINT w, UINT h, UINT step, BOOL NeedFlip ) //pData will be modified to swap R-B and flipped
	{	
		switch(ch){
		case 1: { CImage_Ref<BYTE,1> ref((num::Vec1b*)pData_BGR,w,h,step); if(NeedFlip)ref.Mirror(); CopyFrom(ref); break; }
		case 3: { CImage_Ref<BYTE,3> ref((num::Vec3b*)pData_BGR,w,h,step); if(NeedFlip)ref.Mirror(); ref.SwapChannels_8u(); CopyFrom(ref); ref.SwapChannels_8u(); break; }
		case 4: { CImage_Ref<BYTE,4> ref((num::Vec4b*)pData_BGR,w,h,step); if(NeedFlip)ref.Mirror(); ref.SwapChannels_8u(); CopyFrom(ref); ref.SwapChannels_8u(); break; }
	}	}
public:
	HRESULT Export(CImageFile& img) const
	{	ASSERT(IsNotNull());
		if(	!img.IsNull() && img.GetWidth() == GetWidth() && img.GetHeight() == GetHeight()
			&& img.GetPitch() > 0 && img.GetBPP() == GetChannels()*8 )
		{}
		else
		{	if(!img.IsNull())img.Destroy();
			if(img.Create(GetWidth(),-(int)GetHeight(),GetChannels()*8,
						GetChannels()==4?CImageFile::createAlphaChannel:0))
			{}
			else{ return E_OUTOFMEMORY; }

			if( GetChannels() == 1 ) // Setup color table
			{	RGBQUAD	c[256];
				DWORD co=0;
				for(int i=0;i<256;i++,co+=0x10101)
					*((DWORD*)&c[i])=co;
				img.SetColorTable(0,256,c);
			}
		}

		_ExportToBitmap((LPBYTE)img.GetBits(),GetChannels(),GetWidth(),GetHeight(),img.GetPitch(),FALSE);
		return S_OK;
	}

	HRESULT Import(CImageFile& img) //after import, the data will be modified to swap R-B and maybe also flipped
	{	ASSERT(!img.IsNull());
		if(img.GetBPP()/8>4)return E_INVALIDARG;
		if(!SetSize(img.GetWidth(),img.GetHeight()))return E_OUTOFMEMORY;
		
		BOOL IsRGBA = FALSE;
		BOOL FreeDataOnExit = FALSE;
		LPBYTE pImgData; int step;

		if(img.IsIndexed())
		{	if(img.GetBPP() != 8)return E_INVALIDARG;
			#ifdef _INC_CRTDBG
				step = width * sizeof(BYTE) * 3;
				pImgData = new BYTE[width * height * 3];
			#else
				pImgData = IPPCALL(ippiMalloc_8u_C3)(width,height,&step); 
			#endif
			FreeDataOnExit = TRUE;
			if(!pImgData)return E_OUTOFMEMORY;

			RGBQUAD ct[256];
			img.GetColorTable(0,256,ct);
			const BYTE * pSrc = (const BYTE *)img.GetBits();
			LPIpp8u pDst = pImgData;
			int	Pitch = img.GetPitch();

			// Mapping color in RGB
			for(UINT y=0;y<height;y++,pSrc+=Pitch,pDst+=step)
			{	const BYTE * pS = pSrc;
				LPIpp8u pD = pDst;
				for(UINT x=0;x<width;x++,pS++,pD+=3)
				{	pD[2] = ct[pS[0]].rgbRed;
					pD[1] = ct[pS[0]].rgbGreen;
					pD[0] = ct[pS[0]].rgbBlue;
				}
		}	}
		else 
		{	ASSERT(img.GetBPP() > 8);
			IsRGBA = (img.GetBPP() == 32);
			step = img.GetPitch();
			if(step>0){	pImgData = (LPBYTE)img.GetBits(); }
			else{ 
				step=-step;  pImgData = (LPBYTE)img.GetBits() - step * (height - 1);
			}
		}

		_ImportFromBitmap(pImgData,IsRGBA?4:3,img.GetWidth(),img.GetHeight(),step,img.GetPitch()<0 && height>1 && !img.IsIndexed());
		if (FreeDataOnExit) {
			#ifdef _INC_CRTDBG
				delete[] pImgData;
			#else
				IPPCALL(ippiFree)(pImgData);
			#endif
		}
		return S_OK;
	}

	HGLOBAL ExportToDIB() const
	{	w32::CDIB	dib;
		if(dib.Create(GetWidth(),GetHeight(),GetBPP()))
		{	UINT step = GetWidth()*GetChannels();
			if(step & 3){ step += 4 - (step&3); }

			_ExportToBitmap(dib.GetBits(),GetChannels(),GetWidth(),GetHeight(),step,TRUE);
			dib.VerticalFlip();
			return dib.Detach();
		}
		return NULL;
	}

	BOOL ImportFromDIB(HGLOBAL hDib)
	{
		LPBYTE pData = (LPBYTE)GlobalLock(hDib);
		int size = (int)GlobalSize(hDib);

		BITMAPINFOHEADER* header = (BITMAPINFOHEADER*)pData;
		SetSize(header->biWidth,header->biHeight);
		if( IsNull() ) return FALSE;

		if( header->biCompression == BI_BITFIELDS )
		{	ASSERT( header->biBitCount > 8 );
			header->biClrUsed = 3;
		}
		if(header->biBitCount > 8)
		{	int step = GetWidth()*(header->biBitCount/8);
			if(step & 3){ step += 4 - (step&3); }
			_ImportFromBitmap(	&pData[header->biSize + header->biClrUsed*sizeof(RGBQUAD)],
								header->biBitCount/8,GetWidth(),GetHeight(),step,TRUE );
		}
		else
		{	header->biClrUsed = 256;
			int step = GetWidth()*(header->biBitCount/8);
			if(step & 3){ step += 4 - (step&3); }

			CImage<BYTE,3> tmp;
			tmp.SetSize(*this);

			RGBQUAD* pct = (RGBQUAD*)&pData[header->biSize];
			LPBYTE pLine = (LPBYTE)&pct[header->biClrUsed];
			for(int y=0;y<GetHeight();y++,pLine+=step)
			{	const BYTE * pS = pLine;
				const BYTE * pEnd = &pLine[width];
				num::Vec3<BYTE>* pD = tmp.GetLineAddress(y);
				for(;pS<pEnd;pS++,pD++)
				{	pD[2] = pct[pS[0]].rgbRed;
					pD[1] = pct[pS[0]].rgbGreen;
					pD[0] = pct[pS[0]].rgbBlue;
				}
			}

			_ImportFromBitmap((LPBYTE)tmp.GetBits(),3,width,height,tmp.GetStep(),TRUE);
		}
		return TRUE;
	}

	template<class t_w32_file>
	HRESULT	Load(t_w32_file & file){ return __super::Load(file); }

	template<class t_w32_file>
	HRESULT	Save(t_w32_file & file) const { return __super::Save(file);	}

	HRESULT Save(LPCTSTR fn) const
	{	
		LPCTSTR pext = _tcsrchr(fn,_T('.'));
		if(pext &&	( pext[1] == _T('p') || pext[1] == _T('P') ) &&
					( pext[2] == _T('f') || pext[2] == _T('F') ) &&
					( pext[3] == _T('m') || pext[3] == _T('M') ) )
		{
			return file::Save_PFM(fn,*this);
		}
		else
		{	CImageFile img_atl;
			HRESULT ret = Export(img_atl); 
			return (SUCCEEDED(ret))?img_atl.Save(fn):ret;
	}	}
	HRESULT Load(LPCTSTR fn)
	{	
		LPCTSTR pext = _tcsrchr(fn,_T('.'));
		if(pext &&	( pext[1] == _T('p') || pext[1] == _T('P') ) &&
					( pext[2] == _T('f') || pext[2] == _T('F') ) &&
					( pext[3] == _T('m') || pext[3] == _T('M') ) )
		{
			return file::Load_PFM(fn,*this);
		}
		else
		{	CImageFile img_atl;
			HRESULT ret = img_atl.Load(fn);
			return (SUCCEEDED(ret))?Import(img_atl):ret;
	}	}
	template<class t_Buf>
	HRESULT Save(t_Buf& data, UINT format) const // ipp::ImageCodec_PNG
	{	
		static const GUID* _image_codec[] = 
		{	
			&Gdiplus::ImageFormatJPEG,
			&Gdiplus::ImageFormatPNG,
			&Gdiplus::ImageFormatBMP,
			&Gdiplus::ImageFormatGIF,
			&Gdiplus::ImageFormatIcon
		};

		ASSERT(format < sizeofArray(_image_codec));

		CImageFile img_atl;
		ULONGLONG len;
		ULONGLONG zero = 0;
		IStream* stream = NULL;

		HRESULT ret = S_OK;

		if(	SUCCEEDED(ret = Export(img_atl)) &&
			SUCCEEDED(ret = ::CreateStreamOnHGlobal(NULL,TRUE,&stream)) &&
			SUCCEEDED(ret = img_atl.Save(stream,*_image_codec[format])) &&
			SUCCEEDED(ret = stream->Seek((LARGE_INTEGER&)zero,STREAM_SEEK_END,(ULARGE_INTEGER*)&len)) &&
			SUCCEEDED(ret = stream->Seek((LARGE_INTEGER&)zero,STREAM_SEEK_SET,NULL)) &&
			len<UINT_MAX )
		{
			static const int element_size = sizeof(typename ::rt::TypeTraits<t_Buf>::t_Element);
			len = (len+element_size)/element_size;

			__if_exists(t_Buf::ChangeSize){ if(!data.ChangeSize((UINT)len))ret = E_OUTOFMEMORY; }
			__if_not_exists(t_Buf::ChangeSize){ if(!data.SetSize((UINT)len))ret = E_OUTOFMEMORY; }
			if(FAILED(ret = stream->Read(data,(UINT)len,NULL)))
			{
				__if_exists(t_Buf::ChangeSize){ data.ChangeSize(0); }
				__if_not_exists(t_Buf::ChangeSize){ data.SetSize(0); }
			}
		}

		_SafeRelease(stream);
		return ret;
	}
	HRESULT Load(LPCVOID data, UINT len_byte)
	{	
		HRESULT ret = S_OK;
		IStream* stream = NULL;
		CImageFile img_atl;

		if( SUCCEEDED(ret = ::CreateStreamOnHGlobal(NULL,TRUE,&stream)) &&
			SUCCEEDED(ret = stream->Write(data,len_byte,NULL)) &&
			SUCCEEDED(ret = img_atl.Load(stream)) &&
			SUCCEEDED(ret = Import(img_atl)) )
		{}
		else SetSize(0,0);

		_SafeRelease(stream);
		return ret;
	}
};

template<typename t_Value,UINT channel>
class CImage_Ref:public ::ipp::CImage<t_Value,channel,rt::_BaseImageRef>
{
	template< typename t_Val,UINT ch, class BaseImage >
	friend class CImage;
public:
	typedef typename ::ipp::CImage<t_Value,channel,rt::_BaseImageRef>::ItemType ItemType;
	IPP_FUNC CImage_Ref(LPVOID ptr,UINT w=0,UINT h=0,UINT step_bytes=0){ SetRef((ItemType*)ptr,w,h,step_bytes); }
	IPP_FUNC CImage_Ref(){ ASSERT_STATIC(0); }
	IPP_FUNC CImage_Ref(const CImage_Ref& x){ ASSERT_STATIC(0); }	//The two ASSERTs fired when trying to construct 
	//a Ref from a Ref instance or an empty instance which is not allowed. Avoid trying to copy a Ref
	//instance, it is highly recommended to use Ref& instead  for local using and function arguments
public:
	//template<class BaseVec2>
	//IPP_FUNC Image_Ref(::rt::Image<ItemType,BaseVec2>& x)
	//{	SetRef(x,x.GetWidth(),x.GetHeight(),x.GetStep()); }
	//template<class BaseVec2>
	//IPP_FUNC Image_Ref(::rt::_TypedVector<ItemType,BaseVec2>& x,UINT w,UINT h,UINT step=0)
	//{	SetRef(x,w,h,step?step:(w*sizeof(ItemType))); }

	void SetRef(LPVOID ptr,UINT w,UINT h,UINT step_bytes)
	{	lpData=(ItemType*)ptr; width=w; height=h; Step_Bytes=step_bytes;
		ASSERT( width*sizeof(ItemType)<= step_bytes );
		Pad_Bytes=step_bytes - width*sizeof(ItemType); 
	}
	void SetRef(CImage_Ref& x)
	{	SetRef(x.lpData,x.width,x.height,x.Step_Bytes); }

public:
#ifdef __INTEL_COMPILER
#pragma warning(disable: 525) //warning #525: type "xxx" is an inaccessible type (allowed for compatibility)
#endif
	typedef __super::Ref Ref;
#ifdef __INTEL_COMPILER
#pragma warning(default: 525)
#endif

#pragma warning(disable:4244)
	ASSIGN_OPERATOR_IMG(CImage_Ref)
#pragma warning(default:4244)
	DEFAULT_TYPENAME(CImage_Ref)
	template<typename T,UINT ch, typename BaseVec>									
	IPP_FUNC const CImage_Ref& operator = (const CImage<T,ch,BaseVec> & x)	
	{ CopyFrom(x); return *this; }											

};

typedef CImage<float,1>	CImage_1c32f;
typedef CImage<float,3>	CImage_3c32f;
typedef CImage<float,4>	CImage_4c32f;

typedef CImage<BYTE,1>	CImage_1c8u;
typedef CImage<BYTE,3>	CImage_3c8u;
typedef CImage<BYTE,4>	CImage_4c8u;


namespace file
{

template<typename t_Val,UINT ch,class BaseImage>
IPP_FUNC HRESULT Load_PFM(LPCTSTR fn, ::ipp::CImage<t_Val,ch,BaseImage> & img)
{	ASSERT(fn);
	typedef rt::IsTypeSame<t_Val,float> _is_float;
	__static_if(_is_float::Result && (ch==1 ||ch==3))
	{	//Load directly
		::w32::image_codec::_PFM_Header header;
		HRESULT ret = _Open_PFM(fn,&header);
		if(SUCCEEDED(ret))
		{
			if(img.SetSize(header.width,header.height))
				return ::w32::image_codec::_Read_PFM(&header,img,ch,img.GetStep());
			else
				return E_OUTOFMEMORY;
		}else return ret;
	}
	__static_if(!_is_float::Result || (ch!=1 && ch!=3))
	{	
		::ipp::CImage<float, ::rt::_meta_::Min_Value<3,ch>::Result > tmp;
		HRESULT ret = Load_PFM(fn,tmp);
		img.SetSize(tmp);
		if(SUCCEEDED(ret))img = tmp;
		return ret;
	}
}

template<typename t_Val,UINT ch,class BaseImage>
IPP_FUNC HRESULT Save_PFM(LPCTSTR fn,const ::ipp::CImage<t_Val,ch,BaseImage> & img)
{	ASSERT(fn);
	typedef rt::IsTypeSame<t_Val,float> _is_float;
	__static_if(_is_float::Result && (ch==1 ||ch==3))
	{	// Save directly
		return ::w32::image_codec::_Write_PFM(fn,img,ch,img.GetWidth(),img.GetHeight(),img.GetStep());
	}
	__static_if(!_is_float::Result || (ch!=1 &&ch!=3))
	{
		::ipp::CImage<float, ::rt::_meta_::Min_Value<3,ch>::Result > tmp;
		tmp.SetSize(img);
		tmp = img;
		return Save_PFM(fn,tmp);
	}
}

} // namespace file
} // namespace ipp

