#pragma once

//////////////////////////////////////////////////////////////////////
// Visual Computing Foundation Classes (VCFC)
//					Jiaping Wang  2006.2
//					e_boris2002@hotmail.com
//
// (C) Copyright Jiaping Wang 2004.
//
// Permission to copy, use, modify, sell and distribute this software
// is granted provided this copyright notice appears in all copies.
// This software is provided "as is" without express or implied
// warranty, and with no claim as to its suitability for any purpose.
//
// Absolutly no garanty!!
//////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////
//  Brief Description
//
//  inc\mkl_cpp.h
//  C++ overriding for MKL functions
//
//////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////
// Revise History
//
// Inital version	2006.8.5		Jiaping
//
//////////////////////////////////////////////////////////////////////


namespace mkl
{
namespace mkl_cpp
{
////////////////////////////////////////////////////
// C++ overrides for MKL functions
////////////////////////////////////////////////////

////////////////////////////////////////////////////
// BLAS Level 1
__forceinline float mkl_asum(int N, const float *X, int incX)
{ return cblas_sasum(N,X,incX); }
__forceinline double mkl_asum(int N, const double *X, int incX)
{ return cblas_dasum(N,X,incX); }

__forceinline float mkl_nrm2(int N, const float *X, int incX)
{ return cblas_snrm2(N,X,incX); }
__forceinline double mkl_nrm2(int N, const double *X, int incX)
{ return cblas_dnrm2(N,X,incX); }

__forceinline void mkl_axpy(int N, float alpha, const float *X,int incX, float *Y,int incY)
{ cblas_saxpy(N,alpha,X,incX,Y,incY); }
__forceinline void mkl_axpy(int N, double alpha, const double *X,int incX, double *Y,int incY)
{ cblas_daxpy(N,alpha,X,incX,Y,incY); }

__forceinline float mkl_dot(int N, const float  *X,int incX, const float  *Y,int incY)
{ return cblas_sdot(N,X,incX,Y,incY); }
__forceinline double mkl_dot(int N, const double  *X,int incX, const double  *Y,int incY)
{ return cblas_ddot(N,X,incX,Y,incY); }

__forceinline void mkl_swap(int N, float *X, int incX, float *Y, int incY)
{ cblas_sswap(N,X,incX,Y,incY); }
__forceinline void mkl_swap(int N, double *X, int incX, double *Y, int incY)
{ cblas_dswap(N,X,incX,Y,incY); }

__forceinline size_t mkl_amax(int N, const float  *X, int incX)
{ return cblas_isamax(N,X,incX); }
__forceinline size_t mkl_amax(int N, const double  *X, int incX)
{ return cblas_idamax(N,X,incX); }

__forceinline size_t mkl_amin(int N, const float  *X, int incX)
{ return cblas_isamin(N,X,incX); }
__forceinline size_t mkl_amin(int N, const double  *X, int incX)
{ return cblas_idamin(N,X,incX); }

__forceinline void mkl_copy(int N, const float *X, int incX,float *Y, int incY)
{ return cblas_scopy(N,X,incX,Y,incY); }
__forceinline void mkl_copy(int N, const double *X, int incX,double *Y, int incY)
{ return cblas_dcopy(N,X,incX,Y,incY); }

__forceinline void mkl_scal(int N, float alpha, float *X, int incX)
{ cblas_sscal(N,alpha,X,incX); }
__forceinline void mkl_scal(int N, double alpha, double *X, int incX)
{ cblas_dscal(N,alpha,X,incX); }


////////////////////////////////////////////////////
// BLAS Level 2
__forceinline void mkl_gemv(CBLAS_ORDER order,
							CBLAS_TRANSPOSE TransA, int M, int N,
							float alpha, const float *A, int lda,
							const float *X, int incX, float beta,
							float *Y, int incY)
{
	cblas_sgemv(order,TransA,M,N,alpha,A,lda,X,incX,beta,Y,incY);
}
__forceinline void mkl_gemv(CBLAS_ORDER order,
							CBLAS_TRANSPOSE TransA, int M, int N,
							double alpha, const double *A, int lda,
							const double *X, int incX, double beta,
							double *Y, int incY)
{
	cblas_dgemv(order,TransA,M,N,alpha,A,lda,X,incX,beta,Y,incY);
}


////////////////////////////////////////////////////
// BLAS Level 3
__forceinline void mkl_gemm(	const  CBLAS_ORDER Order, const  CBLAS_TRANSPOSE TransA,
								const  CBLAS_TRANSPOSE TransB, const int M, const int N,
								const int K, const float alpha, const float *A,
								const int lda, const float *B, const int ldb,
								const float beta, float *C, const int ldc)
{
	cblas_sgemm(Order,TransA,TransB,M,N,K,alpha,A,lda,B,ldb,beta,C,ldc);
}

__forceinline void mkl_gemm(	const  CBLAS_ORDER Order, const  CBLAS_TRANSPOSE TransA,
								const  CBLAS_TRANSPOSE TransB, const int M, const int N,
								const int K, const double alpha, const double *A,
								const int lda, const double *B, const int ldb,
								const double beta, double *C, const int ldc)
{
	cblas_dgemm(Order,TransA,TransB,M,N,K,alpha,A,lda,B,ldb,beta,C,ldc);
}


////////////////////////////////////////////////////
// LAPACK
__forceinline int mkl_getrf(int m,int n,float *a,int lda,int *ipiv)
{	int ret;
	sgetrf(&m,&n,a,&lda,ipiv,&ret);
	return ret;
}
__forceinline int mkl_getrf(int m,int n,double *a,int lda,int *ipiv)
{	int ret;
	dgetrf(&m,&n,a,&lda,ipiv,&ret);
	return ret;
}
__forceinline int mkl_getri(int n,float *a,int lda,int *ipiv,float *work,int lwork)
{	int ret;
	sgetri(&n,a,&lda,ipiv,work,&lwork,&ret);
	return ret;
}
__forceinline int mkl_getri(int n,double *a,int lda,int *ipiv,double *work,int lwork)
{	int ret;
	dgetri(&n,a,&lda,ipiv,work,&lwork,&ret);
	return ret;
}
__forceinline int mkl_gesv(int n,int nrhs,float *a,int lda,int *ipiv,float *b,int ldb)
{	int ret;
	sgesv(&n,&nrhs,a,&lda,ipiv,b,&ldb,&ret);
	return ret;
}
__forceinline int mkl_gesv(int n,int nrhs,double *a,int lda,int *ipiv,double *b,int ldb)
{	int ret;
	dgesv(&n,&nrhs,a,&lda,ipiv,b,&ldb,&ret);
	return ret;
}
__forceinline int mkl_gels(char trans,int m,int n,int nrhs,float *a,int lda,float *b,int ldb,float *work,int lwork)
{	int ret;
	sgels(&trans,&m,&n,&nrhs,a,&lda,b,&ldb,work,&lwork,&ret);
	return ret;
}
__forceinline int mkl_gels(char trans,int m,int n,int nrhs,double *a,int lda,double *b,int ldb,double *work,int lwork)
{	int ret;
	dgels(&trans,&m,&n,&nrhs,a,&lda,b,&ldb,work,&lwork,&ret);
	return ret;
}
__forceinline int mkl_gelss(int m,int n,int nrhs,float *a,int lda,float *b,int ldb,float *s,float rcond,int *rank,float *work,int lwork)
{	int ret;
	sgelss(&m,&n,&nrhs,a,&lda,b,&ldb,s,&rcond,rank,work,&lwork,&ret);
	return ret;
}
__forceinline int mkl_gelss(int m,int n,int nrhs,double *a,int lda,double *b,int ldb,double *s,double rcond,int *rank,double *work,int lwork)
{	int ret;
	dgelss(&m,&n,&nrhs,a,&lda,b,&ldb,s,&rcond,rank,work,&lwork,&ret);
	return ret;
}
__forceinline int mkl_syev(char jobz,char uplo,int n,float *a,int lda,float *w,float *work,int lwork)
{	int ret;
	ssyev(&jobz,&uplo,&n,a,&lda,w,work,&lwork,&ret);
	return ret;
}
__forceinline int mkl_syev(char jobz,char uplo,int n,double *a,int lda,double *w,double *work,int lwork)
{	int ret;
	dsyev(&jobz,&uplo,&n,a,&lda,w,work,&lwork,&ret);
	return ret;
}
__forceinline int mkl_syevx(char jobz,char range,char uplo,int n,float *a,int lda,float vl,float vu,int il,int iu,float abstol,int* m,float *w,float *z,int ldz,float *work,int lwork,int *iwork,int *ifail)
{	int ret;
	ssyevx(&jobz,&range,&uplo,&n,a,&lda,&vl,&vu,&il,&iu,&abstol,m,w,z,&ldz,work,&lwork,iwork,ifail,&ret);
	return ret;
}
__forceinline int mkl_syevx(char jobz,char range,char uplo,int n,double *a,int lda,double vl,double vu,int il,int iu,double abstol,int* m,double *w,double *z,int ldz,double *work,int lwork,int *iwork,int *ifail)
{	int ret;
	dsyevx(&jobz,&range,&uplo,&n,a,&lda,&vl,&vu,&il,&iu,&abstol,m,w,z,&ldz,work,&lwork,iwork,ifail,&ret);
	return ret;
}
__forceinline int mkl_syevr(char jobz,char range,char uplo,int n,float *a,int lda,float vl,float vu,int il,int iu,float abstol,int *m,float *w,float *z,int ldz,int *isuppz,float *work,int lwork,int *iwork,int liwork)
{	int ret;
	ssyevr(&jobz,&range,&uplo,&n,a,&lda,&vl,&vu,&il,&iu,&abstol,m,w,z,&ldz,isuppz,work,&lwork,iwork,&liwork,&ret);
	return ret;
}
__forceinline int mkl_syevr(char jobz,char range,char uplo,int n,double *a,int lda,double vl,double vu,int il,int iu,double abstol,int *m,double *w,double *z,int ldz,int *isuppz,double *work,int lwork,int *iwork,int liwork)
{	int ret;
	dsyevr(&jobz,&range,&uplo,&n,a,&lda,&vl,&vu,&il,&iu,&abstol,m,w,z,&ldz,isuppz,work,&lwork,iwork,&liwork,&ret);
	return ret;
}
__forceinline int mkl_gesvd(char jobu,char jobvt,int m,int n,float *a,int lda,float *s,float *u,int ldu,float *vt,int ldvt,float *work,int lwork)
{	int ret;
	sgesvd(&jobu,&jobvt,&m,&n,a,&lda,s,u,&ldu,vt,&ldvt,work,&lwork,&ret);
	return ret;
}
__forceinline int mkl_gesvd(char jobu,char jobvt,int m,int n,double *a,int lda,double *s,double *u,int ldu,double *vt,int ldvt,double *work,int lwork)
{	int ret;
	dgesvd(&jobu,&jobvt,&m,&n,a,&lda,s,u,&ldu,vt,&ldvt,work,&lwork,&ret);
	return ret;
}
__forceinline int mkl_gees(char jobvs,char sort,int *select,int *n,float *a,int lda,int *sdim,float *wr,float *wi,float *vs,int ldvs,float *work,int lwork,int *bwork)
{	int ret;
	sgees(&jobvs,&sort,static_cast<int*>(select),n,a,&lda,sdim,wr,wi,vs,&ldvs,work,&lwork,static_cast<int*>(bwork),&ret);
	return ret;
}
__forceinline int mkl_gees(char jobvs,char sort,int *select,int *n,double *a,int lda,int *sdim,double *wr,double *wi,double *vs,int ldvs,double *work,int lwork,int *bwork)
{	int ret;
	dgees(&jobvs,&sort,static_cast<int*>(select),n,a,&lda,sdim,wr,wi,vs,&ldvs,work,&lwork,static_cast<int*>(bwork),&ret);
	return ret;
}






////////////////////////////////////////////////////
// VML functions
#define VML_DECL_2(name)																\
	__forceinline void vml##name(int n, const float* a, float* r) { vs##name(n,a,r); }	\
	__forceinline void vml##name(int n, const double* a, double* r) { vd##name(n,a,r); }\

VML_DECL_2(Inv)
VML_DECL_2(Sqrt)
VML_DECL_2(InvSqrt)
VML_DECL_2(Cbrt)
VML_DECL_2(InvCbrt)
VML_DECL_2(Exp)
VML_DECL_2(Ln)
VML_DECL_2(Log10)
VML_DECL_2(Sin)
VML_DECL_2(Cos)
VML_DECL_2(Tan)
VML_DECL_2(Sinh)
VML_DECL_2(Cosh)
VML_DECL_2(Tanh)
VML_DECL_2(Acos)
VML_DECL_2(Asin)
VML_DECL_2(Atan)
VML_DECL_2(Asinh)
VML_DECL_2(Acosh)
VML_DECL_2(Atanh)
VML_DECL_2(Erf)
VML_DECL_2(Erfc)

#undef VML_DECL_2

__forceinline void vmlAtan2(int n, const float* a, const float* b, float* r){ vsAtan2(n,a,b,r); }
__forceinline void vmlAtan2(int n, const double* a, const double* b, double* r){ vdAtan2(n,a,b,r); }

__forceinline void vmlDiv(int n, const float* a, const float* b, float* r){ vsDiv(n,a,b,r); }
__forceinline void vmlDiv(int n, const double* a, const double* b, double* r){ vdDiv(n,a,b,r); }

__forceinline void vmlPow(int n, const float* a, const float* b, float* r){ vsPow(n,a,b,r); }
__forceinline void vmlPow(int n, const double* a, const double* b, double* r){ vdPow(n,a,b,r); }

__forceinline void vmlPowx(int n, const float* a, float b, float* r){ vsPowx(n,a,b,r); }
__forceinline void vmlPowx(int n, const double* a, double b, double* r){ vdPowx(n,a,b,r); }

__forceinline void vmlSinCos(int n, const float* a, float* r1, float* r2){ vsSinCos(n,a,r1,r2); }
__forceinline void vmlSinCos(int n, const double* a, double* r1, double* r2){ vdSinCos(n,a,r1,r2); }





} // namespace mkl_cpp

} // namespace mkl



