#pragma once

//////////////////////////////////////////////////////////////////////
// Visual Computing Foundation Classes (VCFC)
//					Jiaping Wang  2006.2
//					e_boris2002@hotmail.com
//
// (C) Copyright Jiaping Wang 2004.
//
// Permission to copy, use, modify, sell and distribute this software
// is granted provided this copyright notice appears in all copies.
// This software is provided "as is" without express or implied
// warranty, and with no claim as to its suitability for any purpose.
//
// Absolutly no warranty!!
//////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////
//  Brief Description
//
//  mkl_basic.h
//  Macros to control linkage
//
//	MKL_LINK_STATIC_LIB  link with MKL statically
//
//////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////
// ChangeLog
//
// Inital version	2006.08.10		Jiaping
//
// Ver 1            2008.11.07      Yiming  
//     * Add an option to link with MKL10;
//     * Fix a typo in the copyright section;
//
//////////////////////////////////////////////////////////////////////


#include "..\w32\win32_ver.h"
#include "..\rt\runtime_base.h"

#include <mkl.h>
#include "inc\mkl_cpp.h"

#ifndef _MAKING_STATIC_LIB_

#ifdef MKL_LINK_STATIC_LIB
	#ifdef _WIN64
		#pragma message("\n>> Static Linking with EM64t version of MKL10\n")
		#pragma comment(lib, "mkl_intel_lp64.lib")
		#pragma comment(lib, "mkl_intel_thread.lib")
		#pragma comment(lib, "mkl_solver_lp64.lib")
		#pragma comment(lib, "mkl_core.lib")
		#pragma comment(lib, "libguide.lib")
	#else
		#pragma message("\n>> Static Linking with IA32 version of MKL10\n")
		#pragma comment(lib, "mkl_intel_c.lib")	
		#pragma comment(lib, "mkl_intel_thread.lib")	
		#pragma comment(lib, "mkl_solver.lib")
		#pragma comment(lib, "mkl_core.lib")
		#pragma comment(lib, "libguide.lib")
	#endif
#else
	#ifdef _WIN64
		#pragma message("\n>> Dynamic Linking with EM64t version of MKL10\n")
		#pragma comment(lib, "mkl_intel_lp64_dll.lib")	
		#pragma comment(lib, "mkl_intel_thread_dll.lib")	
		#pragma comment(lib, "mkl_core_dll.lib")
		#pragma comment(lib, "mkl_solver_lp64.lib")
		#pragma comment(lib, "libguide.lib")	
	#else
		#pragma message("\n>> Dynamic Linking with IA32 version of MKL10\n")
		#pragma comment(lib, "mkl_intel_c_dll.lib")	
		#pragma comment(lib, "mkl_intel_thread_dll.lib")	
		#pragma comment(lib, "mkl_core_dll.lib")
		#pragma comment(lib, "libguide.lib")	
		#pragma comment(lib, "mkl_solver.lib")
	#endif
#endif

#endif // _MAKING_STATIC_LIB_

namespace mkl
{

DYNTYPE_FUNC BOOL IsRuntimeLibraryExists()
{
#ifndef MKL_LINK_STATIC_LIB
	static const LPCTSTR dll_names[] = 
	{
#ifdef _WIN64
		_T("libguide40.dll"),
		_T("mkl_def.dll"),
		_T("mkl_ias.dll"),
		_T("mkl_lapack32.dll"),
		_T("mkl_lapack64.dll"),
		_T("mkl_p4n.dll"),
		_T("mkl_vml_def.dll"),
		_T("mkl_vml_p4n.dll")
#else
		_T("libguide40.dll"),
		_T("mkl_def.dll"),
		_T("mkl_p3.dll"),
		_T("mkl_p4.dll"),
		_T("mkl_lapack32.dll"),
		_T("mkl_lapack64.dll"),
		_T("mkl_vml_def.dll"),
		_T("mkl_vml_p3.dll"),
		_T("mkl_vml_p4.dll")
#endif
	};

	for(int i=0;i<sizeofArray(dll_names);i++)
	{
		_tprintf(_T("Checking MKL runtime DLLs ... (%s)              \r"),dll_names[i]);

		HMODULE hd;
		hd = ::LoadLibrary(dll_names[i]);
		if(hd)
		{
			::FreeLibrary(hd);
		}
		else
		{
			_tprintf(_T("\n"));
			return FALSE;
		}
	}

	_tprintf(_T("                                                                  \r"));
#endif
	return TRUE;
}


} // namespace mkl




