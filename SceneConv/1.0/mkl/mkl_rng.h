#pragma once

//////////////////////////////////////////////////////////////////////
// Visual Computing Foundation Classes (VCFC)
//					Jiaping Wang  2006.2
//					e_boris2002@hotmail.com
//
// (C) Copyright Jiaping Wang 2004.
//
// Permission to copy, use, modify, sell and distribute this software
// is granted provided this copyright notice appears in all copies.
// This software is provided "as is" without express or implied
// warranty, and with no claim as to its suitability for any purpose.
//
// Absolutely no warranty!!
//////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////
//  Brief Description
//
//  mkl_rng.h
//	Random number generator
//
//////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////
// Revise History
//
// Inital version	2006.8.10		Jiaping
//
//////////////////////////////////////////////////////////////////////

#include "mkl_basic.h"

namespace mkl
{

class CRandomNumberGenerator
{
protected:
	VSLStreamStatePtr	pStreamObj;
	int					BRNG_Id;
	void				_ReleaseRNStream()
	{	if( pStreamObj )
		{	vslDeleteStream( &pStreamObj );
			pStreamObj = NULL;
	}	}
public:
	enum 
	{								// Properties:  Func	Test		Error	Period
		BRNG_MCG31 =	VSL_BRNG_MCG31,        //	SS		All Ok 		10-20%	2.10E+09    // Default
		BRNG_R250  =	VSL_BRNG_R250,		   //	NN		3 Fails		0-80%	1.80E+75
		BRNG_MRG32K3A = VSL_BRNG_MRG32K3A,	   //	SN		All Ok 		0-20%	3.10E+57    // Intel recommanded
		BRNG_MCG59 = 	VSL_BRNG_MCG59,		   //	SS		1.5 Fails 	0-45%	1.40E+17
		BRNG_WH    =	VSL_BRNG_WH			   //	SS		1 Fails		0-60%	1.20E+24
	};
	CRandomNumberGenerator(int _BRNG_Id = BRNG_MCG31){ pStreamObj = NULL; ResetBRGN(0,_BRNG_Id); }
	~CRandomNumberGenerator(){ _ReleaseRNStream(); };
	CRandomNumberGenerator(CRandomNumberGenerator& in)  // the copy constructor
	{	BRNG_Id = in.BRNG_Id;
		if( in.pStreamObj )
			vslCopyStream(&pStreamObj,in.pStreamObj);
		else
			pStreamObj = NULL;
	}

	void ResetBRGN(DWORD seed = 0, int New_BRNG_Id = -1 )
	{	_ReleaseRNStream();
		if( New_BRNG_Id >=0 )BRNG_Id = New_BRNG_Id;
		vslNewStream( &pStreamObj, BRNG_Id,seed?seed:((DWORD)time(NULL)) );
		ASSERT( pStreamObj );
	}
	
	DYNTYPE_FUNC void Gen_UniformSequence(float *r, UINT len, float LowBoundary,float HighBoundary)
	{ vsRngUniform(0,pStreamObj,len,r,LowBoundary,HighBoundary); }
	DYNTYPE_FUNC void Gen_GaussianSequence(float *r, UINT len,float Mean,float Sigma)
	{ vsRngGaussian(VSL_METHOD_SGAUSSIAN_BOXMULLER,pStreamObj,len,r,Mean,Sigma); }
	DYNTYPE_FUNC void Gen_LaplaceSequence(float *r, UINT len,float A,float Beta)   
	{ vsRngLaplace(0,pStreamObj,len,r,A,Beta); }
	DYNTYPE_FUNC void Gen_WeibullSequence(float *r, UINT len,float alpha, float A,float beta)   
	{ vsRngWeibull(0,pStreamObj,len,r,alpha,A,beta); }
	DYNTYPE_FUNC void Gen_CauchySequence(float *r, UINT len,float A,float beta)   
	{ vsRngCauchy(0,pStreamObj,len,r,A,beta); }
	DYNTYPE_FUNC void Gen_RayleighSequence(float *r, UINT len,float A,float beta)   
	{ vsRngRayleigh(0,pStreamObj,len,r,A,beta); }
	DYNTYPE_FUNC void Gen_LognormalSequence(float *r, UINT len,float A,float sigma,float B,float beta)   
	{ vsRngLognormal(0,pStreamObj,len,r,A,sigma,B,beta); }
	DYNTYPE_FUNC void Gen_GumbelSequence(float *r, UINT len,float A,float beta)   
	{ vsRngGumbel(0,pStreamObj,len,r,A,beta); }

	DYNTYPE_FUNC void Gen_UniformSequence(double *r, UINT len,double LowBoundary,double HighBoundary)
	{ vdRngUniform(0,pStreamObj,len,r,LowBoundary,HighBoundary); }
	DYNTYPE_FUNC void Gen_GaussianSequence(double *r, UINT len,double Mean,double Sigma)
	{ vdRngGaussian(VSL_METHOD_SGAUSSIAN_BOXMULLER,pStreamObj,len,r,Mean,Sigma); }
	DYNTYPE_FUNC void Gen_LaplaceSequence(double *r, UINT len,double A,double Beta)   
	{ vdRngLaplace(0,pStreamObj,len,r,A,Beta); }
	DYNTYPE_FUNC void Gen_WeibullSequence(double *r, UINT len,double alpha, double A,double beta)   
	{ vdRngWeibull(0,pStreamObj,len,r,alpha,A,beta); }
	DYNTYPE_FUNC void Gen_CauchySequence(double *r, UINT len,double A,double beta)   
	{ vdRngCauchy(0,pStreamObj,len,r,A,beta); }
	DYNTYPE_FUNC void Gen_RayleighSequence(double *r, UINT len,double A,double beta)   
	{ vdRngRayleigh(0,pStreamObj,len,r,A,beta); }
	DYNTYPE_FUNC void Gen_LognormalSequence(double *r, UINT len,double A,double sigma,double B,double beta)   
	{ vdRngLognormal(0,pStreamObj,len,r,A,sigma,B,beta); }
	DYNTYPE_FUNC void Gen_GumbelSequence(double *r, UINT len,double A,double beta)   
	{ vdRngGumbel(0,pStreamObj,len,r,A,beta); }
};




} // namespace mkl




