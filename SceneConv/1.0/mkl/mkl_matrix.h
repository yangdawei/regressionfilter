#pragma once

//////////////////////////////////////////////////////////////////////
// Visual Computing Foundation Classes (VCFC)
//					Jiaping Wang  2006.2
//					e_boris2002@hotmail.com
//
// (C) Copyright Jiaping Wang 2004.
//
// Permission to copy, use, modify, sell and distribute this software
// is granted provided this copyright notice appears in all copies.
// This software is provided "as is" without express or implied
// warranty, and with no claim as to its suitability for any purpose.
//
// Absolutely no warranty!!
//////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////
//  Brief Description
//
//  mkl_matrix.h
//	CBlas level 2 and 3
//	SVD / Least Square
//
//////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////
// Revise History
//
// Inital version	2006.8.10		Jiaping
//
//////////////////////////////////////////////////////////////////////

#include "mkl_vector.h"
#include "..\rt\compact_image.h"


namespace mkl
{

////////////////////////////////////////////////////
//			Column major
//
//			3x4 matrix ( 3 row x 4 column )
//			Logic layout:
//			| --------- 4 -----------> j
//			| / a00, a01, a02, a03 \
//			3 | a10, a11, a12, a13 |
//			| \ a20, a21, a22, a23 /
//			V
//			i
//
//			mx1 is column vector
//			1xn is row vector
//
//			Physical layout:
//			a00, a10, a20, p1, p2 .. pn
//			a01, a11, a21, p1, p2 .. pn
//			...
//			a03, a13, a23, p1, p2 .. pn
//			( px is padding elements ), leading_dimension is row+padding


namespace _meta_
{
template<typename t_Val>
class _BaseMklMatrixRoot // Column major
{	//Element requirment: float/double
	ASSERT_STATIC(	rt::TypeTraits<t_Val>::Typeid == ::rt::_typeid_32f || 
					rt::TypeTraits<t_Val>::Typeid == ::rt::_typeid_64f	);
protected:
	DYNTYPE_FUNC _BaseMklMatrixRoot(const _BaseMklMatrixRoot &x){ ASSERT_STATIC(0); }

public:
	static const bool IsRef = false;
	DYNTYPE_FUNC _BaseMklMatrixRoot(){	lpData=NULL; column_count=0; row_count=0; Padding=0; }
	DYNTYPE_FUNC ~_BaseMklMatrixRoot(){ __SafeFree(); }
	DYNTYPE_FUNC BOOL SetSize(UINT co=0){ return (co==GetSize())?TRUE:FALSE; }
	DYNTYPE_FUNC BOOL ChangeSize(UINT new_size){ return (new_size==GetSize())?TRUE:FALSE; }

	//TypeTraits
	TYPETRAITS_DECL_LENGTH(TYPETRAITS_SIZE_UNKNOWN)
	TYPETRAITS_DECL_IS_AGGREGATE(false)

	template<typename T,typename T2>
	DYNTYPE_FUNC t_Val & At(const T i,const T2 j)
	{ return lpData[j*LeadingDimen+i]; }
	template<typename T,typename T2>
	DYNTYPE_FUNC const t_Val & At(const T i,const T2 j) const
	{ return lpData[j*LeadingDimen+i]; }

	DYNTYPE_FUNC UINT GetSize() const{ return (UINT)(column_count*row_count); }

	::mkl::CVector_RefCompact<t_Val>
	GetLine(UINT y){ return ::mkl::CVector_RefCompact<t_Val>(&At(0,y),GetWidth()); }
	const ::mkl::CVector_RefCompact<t_Val> 
	GetLine(UINT y) const{ return ::rt::_CastToNonconst(this)->GetLine(y); }

protected:
	UINT	column_count;
	UINT	row_count;
	UINT	Padding;	
	UINT	LeadingDimen; // LeadingDimen = row + Padding;
	t_Val*	lpData;

	void	__SafeFree(){ _SafeFree32AL(lpData); }
public:
	DYNTYPE_FUNC BOOL SetSize_2D(UINT row=0,UINT col=0)
	{	if(row==row_count && col==column_count){ return TRUE; }
		else
		{	__SafeFree();
			if(row&&col)
			{
				LeadingDimen = rt::EnlargeTo32AL(row);
				lpData = _Malloc32AL(t_Val,LeadingDimen*col);
			}
			if(lpData)
			{	row_count=row; column_count=col; 
				Padding = LeadingDimen - row;
				return TRUE;
			}
			else{ row_count=column_count=0; }
		}
		return FALSE;
	}
	DYNTYPE_FUNC UINT		GetWidth() const{ return row_count;}
	DYNTYPE_FUNC UINT		GetHeight() const{ return column_count;}
};
class _BaseMklMatrix
{public:	template<typename t_Ele> class SpecifyItemType
			{public: typedef ::mkl::_meta_::_BaseMklMatrixRoot<t_Ele> t_BaseVec; };
			static const bool IsCompactLinear = false;
			static const bool IsColumnMajor = true;
};

template<typename t_Val,bool column_major>
struct _DeduceVecType;
	template<typename t_Val> struct _DeduceVecType<t_Val,true>
	{	typedef CVector_RefCompact<t_Val>	t_ColumnRef;
		typedef CVector_Ref<t_Val>			t_RowRef;
	};
	template<typename t_Val> struct _DeduceVecType<t_Val,false>
	{	typedef CVector_RefCompact<t_Val>	t_RowRef;
		typedef CVector_Ref<t_Val>			t_ColumnRef;
	};
}// namespace _meta_



template<typename t_Val,bool column_major>
class CMatrix_Ref;

enum _tagGeneralSVD_Flag
{
	Replace_None = 0,
	Replace_U_in_This,
	Replace_V_in_This
};

template<typename t_Val, class t_BaseVec = ::mkl::_meta_::_BaseMklMatrix>
class CMatrix:public ::rt::_TypedImage<t_Val,t_BaseVec>
{	template<typename, class>friend class CMatrix;
public:
	TYPETRAITS_DECL_EXTENDTYPEID((rt::_typeid_codelib<<16)+105)
	static const bool IsColumnMajor = t_BaseVec::IsColumnMajor;
	static const int OPTI_WORKING_BLOCK_SIZE = 64;

private:
	typedef ::mkl::_meta_::_DeduceVecType<t_Val,IsColumnMajor> _VecType;
	DYNTYPE_FUNC CBLAS_ORDER		 _BlasMajor() const{ return IsColumnMajor?CblasColMajor:CblasRowMajor; }
	DYNTYPE_FUNC CBLAS_TRANSPOSE	 _BlasTranspose() const{ return IsColumnMajor?CblasNoTrans:CblasTrans; }
	DYNTYPE_FUNC char				 _LapackTranspose() const{ return IsColumnMajor?'N':'T'; }

public:
	DYNTYPE_FUNC UINT		GetWidth() const{ return __super::GetWidth(); }
	DYNTYPE_FUNC UINT		GetHeight() const{ return __super::GetHeight(); }
	

public:
	typedef typename _VecType::t_ColumnRef	t_ColumnRef;
	typedef typename _VecType::t_RowRef		t_RowRef;

	typedef t_Val ItemType;
	typedef t_Val* LPItemType;
	typedef const t_Val* LPCItemType;

	DYNTYPE_FUNC operator LPItemType(){ return &At(0,0); }
	DYNTYPE_FUNC operator LPCItemType()const { return &At(0,0); }
	DYNTYPE_FUNC operator LPVOID(){ return &At(0,0); }
	DYNTYPE_FUNC operator LPCVOID()const { return &At(0,0); }

public:
	typedef CMatrix_Ref<t_Val, IsColumnMajor> Ref;
	typedef CMatrix_Ref<t_Val, !IsColumnMajor> Ref_Transposed;

	DYNTYPE_FUNC Ref_Transposed operator !(){ return Transpose(); }
	DYNTYPE_FUNC const Ref_Transposed operator !() const{ return Transpose(); }

	DYNTYPE_FUNC Ref_Transposed Transpose()
	{ return Ref_Transposed(lpData,GetSize_Col(),GetSize_Row(),GetLeadingDim()); }
	const Ref_Transposed Transpose() const
	{ return ::rt::_CastToNonconst(this)->Transpose(); }

	DYNTYPE_FUNC Ref GetSub(UINT i,UINT j,UINT row_co,UINT col_co)
	{	ASSERT(i+row_co<=GetSize_Row());
		ASSERT(j+col_co<=GetSize_Col());
		return Ref(&At(i,j),row_co,col_co,LeadingDimen);
	}
	DYNTYPE_FUNC const Ref GetSub(UINT i,UINT j,UINT row_co,UINT col_co) const
	{	return rt::_CastToNonconst(this)->GetSub(i,j,row_co,col_co); }

public:
	DYNTYPE_FUNC BOOL SetSize(UINT row=0,UINT col=0){ return __super::SetSize_2D(row,col); } // row,col
	template<typename T>
	DYNTYPE_FUNC BOOL SetSizeAs(const T& x)
	{ return SetSize(x.GetSize_Row(),x.GetSize_Col()); }
	template<typename t_MA,typename t_MB>
	DYNTYPE_FUNC BOOL SetSizeAsProductOf(const t_MA& a,const t_MB& b)
	{ return SetSize(a.GetSize_Row(),b.GetSize_Col()); }

public:
	const CMatrix& operator = (const CMatrix_Ref<t_Val,IsColumnMajor>& x)
	{	ASSERT(GetSize_Col() == x.GetSize_Col());
		ASSERT(GetSize_Row() == x.GetSize_Row());
		if(IsColumnMajor)
		{
			for(UINT j=0;j<GetSize_Col();j++)
				GetCol(j) = x.GetCol(j);
		}
		else
		{
			for(UINT j=0;j<GetSize_Row();j++)
				GetRow(j) = x.GetRow(j);
		}
		return *this;
	}
	const CMatrix& operator = (const CMatrix_Ref<t_Val,!IsColumnMajor>& x)
	{	
		if(&At(0,0)!=&x.At(0,0))	// a = !b;
		{	
			ASSERT(GetSize_Col() == x.GetSize_Col());
			ASSERT(GetSize_Row() == x.GetSize_Row());
			for(UINT j=0;j<GetSize_Col();j++)
				GetCol(j) = x.GetCol(j);
		}
		else // a = !a;
		{	if(GetSize_Col()==GetSize_Row())
			{
				for(UINT i=0;i<GetSize_Col();i++)
				for(UINT j=0;j<i;j++)
					rt::Swap((*this)(i,j),(*this)(j,i));
			}
			else
			{	
				CMatrix	tmp;	
				tmp.SetSizeAs(x);	
				tmp = x;
				rt::Swap(tmp,*this);
			}
		}
		return *this;
	}

	COMMON_CONSTRUCTOR_IMG(CMatrix)
	COMMON_IMAGE_STUFFS(CMatrix)

public:
//Yue add GetSize_M and GetSize_N, use this if you are onfused by the size of row or number of row
	DYNTYPE_FUNC UINT GetSize_M() const{ return row_count; }
	DYNTYPE_FUNC UINT GetSize_N() const{ return column_count; }

	DYNTYPE_FUNC UINT GetSize_Row() const{ return row_count; }
	DYNTYPE_FUNC UINT GetSize_Col() const{ return column_count; }
	DYNTYPE_FUNC UINT GetLeadingDim() const{ return LeadingDimen; }
	DYNTYPE_FUNC BOOL IsSquare() const{ return row_count==column_count; }

	//DYNTYPE_FUNC operator CMatrix_Ref<t_Val,IsColumnMajor>& ()
	//{ return *((CMatrix_Ref<t_Val,IsColumnMajor>*)this); }
	//DYNTYPE_FUNC operator const CMatrix_Ref<t_Val,IsColumnMajor>& () const
	//{ return ::rt::_CastToNonconst(this)->operator CMatrix_Ref<t_Val,IsColumnMajor>& (); }

public:
	DYNTYPE_FUNC t_Val& operator ()(UINT i, UINT j)
	{ ASSERT(i<GetSize_Row() && j<GetSize_Col()); return At(i,j); }
	DYNTYPE_FUNC const t_Val& operator ()(UINT i, UINT j) const
	{ ASSERT(i<GetSize_Row() && j<GetSize_Col()); return At(i,j); }

public:
	DYNTYPE_FUNC CVector_RefCompact<t_Val> GetVec()
	{ return CVector_RefCompact<t_Val>(lpData,LeadingDimen*GetHeight()); }
	DYNTYPE_FUNC const CVector_RefCompact<t_Val> GetVec() const
	{ return ::rt::_CastToNonconst(this)->GetVec(); }

	DYNTYPE_FUNC t_ColumnRef GetCol(UINT j)
	{ ASSERT(j<GetSize_Col()); return t_ColumnRef(&At(0,j),GetSize_Row(),IsColumnMajor?1:GetLeadingDim()); }
	DYNTYPE_FUNC const t_ColumnRef GetCol(UINT j) const
	{ return ::rt::_CastToNonconst(this)->GetCol(j); }

	DYNTYPE_FUNC t_RowRef GetRow(UINT i)
	{ ASSERT(i<GetSize_Row()); return t_RowRef(&At(i,0),GetSize_Col(),IsColumnMajor?GetLeadingDim():1); }
	DYNTYPE_FUNC const t_RowRef GetRow(UINT i) const
	{ return ::rt::_CastToNonconst(this)->GetRow(i); }

	DYNTYPE_FUNC CVector_Ref<t_Val> GetDiag()
	{ return CVector_Ref<t_Val>(lpData,min(GetSize_Row(),GetSize_Col()),GetLeadingDim()+1); }
	DYNTYPE_FUNC const CVector_Ref<t_Val> GetDiag() const
	{ return ::rt::_CastToNonconst(this)->GetDiag(); }

public:
	DYNTYPE_FUNC void ExchangeColumn(UINT j1,UINT j2)
	{	ASSERT(j1<GetSize_Col());	ASSERT(j2<GetSize_Col());
		if(j1!=j2){ ::mkl::mkl_cpp::mkl_swap(GetSize_Row(),&At(0,j1),IsColumnMajor?1:GetLeadingDim(),
														   &At(0,j2),IsColumnMajor?1:GetLeadingDim()); 	
	}	}
	DYNTYPE_FUNC void ExchangeRow(UINT i1,UINT i2)
	{	ASSERT(i1<GetSize_Row());	ASSERT(i2<GetSize_Row());
		if(i1!=i2){ ::mkl::mkl_cpp::mkl_swap(GetSize_Row(),&At(i1,0),IsColumnMajor?GetLeadingDim():1,
														   &At(i2,0),IsColumnMajor?GetLeadingDim():1); 	
	}	}
	DYNTYPE_FUNC void FlipColumns()
	{	for(UINT i=0;i<GetSize_Col()/2;i++)
			ExchangeColumn(i,GetSize_Col()-i-1);
	}
	DYNTYPE_FUNC void FlipRows()
	{	for(UINT i=0;i<GetSize_Row()/2;i++)
			ExchangeRow(i,GetSize_Row()-1-i);
	}

public:
	//BLAS Level 2
	//y = alpha*This*x + beta*x
	template<class t_Vec1, class t_Vec2>
	void VectorProduct(const t_Vec1& x,t_Vec2& y,t_Val alpha=1,t_Val beta=0) const
	{
		ASSERT(x.GetSize() == GetSize_Col());
		ASSERT(y.GetSize() == GetSize_Row());
		::mkl::mkl_cpp::mkl_gemv(_BlasMajor(),CblasNoTrans,(int)GetSize_Row(),(int)GetSize_Col(),alpha,
								 (const t_Val*)*this,(int)GetLeadingDim(),
								 (const t_Val*)x,(int)x.GetStride(),beta,
								 (t_Val*)y,(int)y.GetStride());
	}

	//BLAS Level 3
	//This = alpha*A*B + beta*This
	template<bool _a,bool _b>
	void Product(const CMatrix_Ref<t_Val,_a>& a,const CMatrix_Ref<t_Val,_b>& b,t_Val alpha=1,t_Val beta=0)
	{	ASSERT(((size_t)this)!=((size_t)&a));	//a should not be the same object of This
		ASSERT(((size_t)this)!=((size_t)&b));	//b should not be the same object of This
		ASSERT(a.GetSize_Col() == b.GetSize_Row());
		ASSERT(GetSize_Row() == a.GetSize_Row());
		ASSERT(GetSize_Col() == b.GetSize_Col());

		::mkl::mkl_cpp::mkl_gemm(CblasColMajor,a._BlasTranspose(),b._BlasTranspose(),
								 GetSize_Row(),GetSize_Col(),a.GetSize_Col(),
								 alpha,a,a.GetLeadingDim(),b,b.GetLeadingDim(),
								 beta,*this,GetLeadingDim());
	}

	//LAPACK
	//Matrix Inversion
	//This = This^-1
	HRESULT Inverse()
	{	ASSERT(IsSquare()); 
		::rt::Buffer_32BIT<int> Ipiv;
		if(Ipiv.SetSize(GetSize_Col())){}
		else{ return E_OUTOFMEMORY; }
		// LU factorization
		int ret;
		ret = ::mkl::mkl_cpp::mkl_getrf(GetSize_Row(),GetSize_Col(),*this,GetLeadingDim(),Ipiv);

		if( !ret )
		{	mkl::_meta_::CVector_Compact<t_Val> Workspc;
			if(Workspc.SetSize(GetSize_Col()*OPTI_WORKING_BLOCK_SIZE)){}
			else{ return E_OUTOFMEMORY; }
			
			ret = ::mkl::mkl_cpp::mkl_getri(GetSize_Col(),*this,GetLeadingDim(),Ipiv,Workspc,Workspc.GetSize());
		}

		if(ret)
			return ret<0?E_INVALIDARG:E_ABORT;
		else
			return S_OK;
	}

	//A*A'=E
	HRESULT AccurateInverse()
	{	ASSERT(IsSquare()); 
		::rt::Buffer_32BIT<int> Ipiv;
		if(Ipiv.SetSize(GetSize_Col())){}
		else{ return E_OUTOFMEMORY; }

		mkl::CMatrix<t_Val>	origSolveLinear(*this);
		// LU factorization
		int ret;
		ret = ::mkl::mkl_cpp::mkl_getrf(GetSize_Row(),GetSize_Col(),*this,GetLeadingDim(),Ipiv);

		if( !ret )
		{	mkl::_meta_::CVector_Compact<t_Val> Workspc;
			if(Workspc.SetSize(GetSize_Col()*OPTI_WORKING_BLOCK_SIZE)){}
			else{ return E_OUTOFMEMORY; }
			::rt::Buffer_32BIT<int> intWorkspc;
			if(intWorkspc.SetSize(GetSize_Col()*OPTI_WORKING_BLOCK_SIZE)){}
			else{ return E_OUTOFMEMORY; }
			
			mkl::CMatrix<t_Val>		identity, X;
			identity.SetSizeAs(*this);
			X.SetSizeAs(identity);
			X.Zero();
			identity.Zero();

			for( UINT i=0; i<identity.GetSize_Row(); i++ )
			{
				X(i,i) = static_cast<t_Val>(1.0);
				identity.At(i,i) = static_cast<t_Val>(1.0);
			}
			
			ret = ::mkl::mkl_cpp::mkl_getrs(_LapackTranspose(), GetSize_Col(), GetSize_Row(),origSolveLinear,
				origSolveLinear.GetLeadingDim(), Ipiv, X, X.GetLeadingDim() );

			mkl::CMatrix<t_Val>		temp;
			temp.SetSizeAs(origSolveLinear);
			temp.Product(!!origSolveLinear,!!X);

			if(!ret)
			{
				mkl::_meta_::CVector_Compact<t_Val> ferr, berr;
				if(ferr.SetSize(GetSize_Col()) && berr.SetSize(GetSize_Col()) )  {}
				else {return E_OUTOFMEMORY;}
				//refine the solution
				ret = ::mkl::mkl_cpp::mkl_gerfs(_LapackTranspose(), GetSize_Col(), GetSize_Row(), 
					origSolveLinear, origSolveLinear.GetLeadingDim(), *this, GetLeadingDim(), Ipiv, identity, identity.GetLeadingDim(), 
					X, X.GetLeadingDim(), ferr, berr, Workspc, intWorkspc); 
			}

			temp.Product(!!origSolveLinear, !!X);
			
			if(ret)
				return ret<0?E_INVALIDARG:E_ABORT;
			else
			{
				*this = X;
				return S_OK;
			}
		}
		else
			return ret<0?E_INVALIDARG:E_ABORT;
	}

	//Linear Equations
	//This*x=B  --> A change to LU, B change to solution
	HRESULT SolveLinearEquations( CMatrix_Ref<t_Val,true>&B )
	{	ASSERT_STATIC(IsColumnMajor); // row major matrix is not supported
		ASSERT(IsSquare());
		ASSERT(GetSize_Row() == B.GetSize_Row());
		::rt::Buffer_32BIT<int> Ipiv;
		if(Ipiv.SetSize(GetSize_Col())){}
		else{ return E_OUTOFMEMORY; }

		int ret = ::mkl::mkl_cpp::mkl_gesv(GetSize_Row(),B.GetSize_Col(),*this,GetLeadingDim(),Ipiv,B,B.GetLeadingDim());
		if(ret)
			return ret<0?E_INVALIDARG:E_ABORT;
		else
			return S_OK;
	}
	DYNTYPE_FUNC HRESULT SolveLinearEquations( CVector_RefCompact<t_Val>&b )
	{	ASSERT(GetSize_Row() == b.GetSize());
		return SolveLinearEquations( CMatrix_Ref<t_Val,true>(b,b.GetSize(),1,b.GetSize()));
	}

	//Linear Least Squares
	//This*x=B  --> This change to LU, B change to solution
	HRESULT SolveLinearLeastSquares_LQR( CMatrix_Ref<t_Val,true>& B )
	{	ASSERT(max(GetSize_Row(),GetSize_Col()) == B.GetSize_Row());
		mkl::_meta_::CVector_Compact<t_Val> Workspc;
		if(Workspc.SetSize(min(GetSize_Col(),GetSize_Row())+max(B.GetSize_Col(),max(GetSize_Col(),GetSize_Row()))*OPTI_WORKING_BLOCK_SIZE)){}
		else{ return E_OUTOFMEMORY; }

		if(::mkl::mkl_cpp::mkl_gels(_LapackTranspose(),GetSize_Row(),GetSize_Col(),B.GetSize_Col(),*this,
									GetLeadingDim(),B,B.GetLeadingDim(),Workspc,Workspc.GetSize()))
		{	return E_INVALIDARG; }
		else
		{	return S_OK; }
	}
	DYNTYPE_FUNC HRESULT SolveLinearLeastSquares_LQR( CVector_RefCompact<t_Val>&b )
	{ return SolveLinearLeastSquares_LQR( CMatrix_Ref<t_Val,true>(b,b.GetSize(),1,b.GetSize())); }

	//Linear Least Squares
	//This*x=B  --> This change to singlar vectors, B change to solution
	HRESULT SolveLinearLeastSquares_SVD( CMatrix_Ref<t_Val,true>& B , UINT* pRank = NULL, t_Val rank_condition = ((t_Val)0.0001) )
	{	ASSERT_STATIC(IsColumnMajor); // row major matrix is not supported
		ASSERT(max(GetSize_Col(),GetSize_Row()) == B.GetSize_Row());
		UINT s_len = min(GetSize_Col(),GetSize_Row());
		mkl::_meta_::CVector_Compact<t_Val> Workspc; //first [s_len] elements used to output eigenvalue
		if(Workspc.SetSize(	3*s_len + s_len + 
							max(max(B.GetSize_Col(),max(GetSize_Col(),GetSize_Row())),2*s_len))
		  ){}
		else{ return E_OUTOFMEMORY; }
		UINT determined_rank;
		if(!pRank)pRank = &determined_rank;
		if(::mkl::mkl_cpp::mkl_gelss(	GetSize_Row(),GetSize_Col(),B.GetSize_Col(),
										*this,GetLeadingDim(),B,B.GetLeadingDim(),
										Workspc,rank_condition,(int*)pRank,&Workspc[s_len],Workspc.GetSize()-s_len))
		{	return E_INVALIDARG; }
		else
		{	return S_OK; }
	}
	DYNTYPE_FUNC HRESULT SolveLinearLeastSquares_SVD( CVector_RefCompact<t_Val>&b, UINT* pRank = NULL, t_Val rank_condition = ((t_Val)0.0001) )
	{	return SolveLinearLeastSquares_SVD(	CMatrix_Ref<t_Val,true>(b,b.GetSize(),1,b.GetSize()),
											pRank, rank_condition); 
	}

	//Singular Value Decomposition of a general rectangular matrix.
	//This = USV'
	template<class t_BaseVec2>
	HRESULT GeneralSVD( ::mkl::_meta_::CVector_Base<t_Val,t_BaseVec2>& EigenValue, DWORD ReplaceFlag = mkl::Replace_U_in_This, 
						CMatrix<t_Val>* pU_Matrix=NULL, CMatrix<t_Val>* pV_Matrix=NULL )
	{	UINT m = GetSize_Row();
		UINT n = GetSize_Col();
		UINT order = min(m,n);

		if(ReplaceFlag==Replace_U_in_This)ASSERT(pU_Matrix==NULL);
		if(ReplaceFlag==Replace_V_in_This)ASSERT(pV_Matrix==NULL);

		if(EigenValue.SetSize(order)){}else{ return E_OUTOFMEMORY; }
		if(pU_Matrix)
			if(pU_Matrix->SetSize(m,order)){}else{ return E_OUTOFMEMORY; }
		if(pV_Matrix)
			if(pV_Matrix->SetSize(order,n)){}else{ return E_OUTOFMEMORY; }
		
		int info;
		mkl::_meta_::CVector_Compact<t_Val> workspc;
		{	t_Val opti_work_size = 0;
			info = ::mkl::mkl_cpp::mkl_gesvd((ReplaceFlag==Replace_U_in_This)?'O':(pU_Matrix?'S':'N'),
                                             (ReplaceFlag==Replace_V_in_This)?'O':(pV_Matrix?'S':'N'),
											 m,n,*this,GetLeadingDim(),EigenValue,
											 pU_Matrix?(pU_Matrix->GetVec().Begin()):NULL,
											 pU_Matrix?(pU_Matrix->GetLeadingDim()):1,
											 pV_Matrix?(pV_Matrix->GetVec().Begin()):NULL,
											 pV_Matrix?(pV_Matrix->GetLeadingDim()):1,
											 &opti_work_size,-1);
			if(info){ return E_UNEXPECTED; }
			if(workspc.SetSize((UINT)opti_work_size)){}else{ return E_OUTOFMEMORY; }
		}

		info = ::mkl::mkl_cpp::mkl_gesvd((ReplaceFlag==Replace_U_in_This)?'O':(pU_Matrix?'S':'N'),
                                         (ReplaceFlag==Replace_V_in_This)?'O':(pV_Matrix?'S':'N'),
										 m,n,*this,GetLeadingDim(),EigenValue,
										 pU_Matrix?(pU_Matrix->GetVec().Begin()):NULL,
										 pU_Matrix?(pU_Matrix->GetLeadingDim()):1,
										 pV_Matrix?(pV_Matrix->GetVec().Begin()):NULL,
										 pV_Matrix?(pV_Matrix->GetLeadingDim()):1,
										 workspc,workspc.GetSize());
		_CheckHeap;
		if(info)
			return info<0?E_INVALIDARG:E_ABORT;
		else
			return S_OK;
	}

	//Symmetric Eigen Problem
	//This turn to EigenVectors (in columns)
	template<class t_BaseVec2>
	HRESULT SolveSymmetricEigen( ::mkl::_meta_::CVector_Base<t_Val,t_BaseVec2>& EigenValue, BOOL NeedEigenVectors = FALSE)
	{	ASSERT(IsSquare());
		if(EigenValue.SetSize(GetSize_Col())){}
		else{ return EigenValue.IsRef?E_INVALIDARG:E_OUTOFMEMORY; }

		mkl::_meta_::CVector_Compact<t_Val> Workspc;
		if(Workspc.SetSize(max(3*GetSize_Col()-1,GetSize_Col()*(OPTI_WORKING_BLOCK_SIZE+2)))){}
		else{ return E_OUTOFMEMORY; }

		int ret;
		ret = ::mkl::mkl_cpp::mkl_syev(	NeedEigenVectors?'V':'N','U',
										GetSize_Col(),*this,GetLeadingDim(),EigenValue,Workspc,Workspc.GetSize());
		if(ret)
			return ret<0?E_INVALIDARG:E_ABORT;
		else
			return S_OK;
	}

	// Symmetric Eigen Problem
	// solve eigens of [TermStart,TermEnd] zero-based, eigenvalue is in ascending order
	// EigenVectors in rows
	template<class t_BaseVec2>
	HRESULT PartialSolveSymmetricEigen(	UINT TermStart,UINT TermEnd, 
										::mkl::_meta_::CVector_Base<t_Val,t_BaseVec2>& EigenValue, 
										CMatrix<t_Val>* EigenVectors = NULL)
	{	ASSERT(IsSquare());
		ASSERT( TermStart>=0 && TermStart<=TermEnd && TermEnd<GetSize_Col() );
		TermStart++;   TermEnd++;   // MKL has 1-based index

		if(EigenValue.SetSize(TermEnd-TermStart+1)){}
		else{ EigenValue.IsRef?E_INVALIDARG:E_OUTOFMEMORY; }
		
		if( EigenVectors )
		{	if(EigenVectors->SetSize(GetSize_Col(),TermEnd-TermStart+1)){}
			else{ EigenVectors->IsRef?E_INVALIDARG:E_OUTOFMEMORY; }
		}

		::rt::Buffer_32BIT<int> ifail_iWork;
		if(ifail_iWork.SetSize(6*GetSize_Col()+2)){}
		else{ return E_OUTOFMEMORY; }

		t_Val abstol = 0;//2*slamch("S");
		int  m=0, info;

		mkl::_meta_::CVector_Compact<t_Val> Workspc;
		// query optimized working space
		{
			t_Val opti_workspace_size = 0;
			info = ::mkl::mkl_cpp::mkl_syevx(	(EigenVectors!=NULL)?'V':'N','I','U',
												GetSize_Col(),*this,GetLeadingDim(),0,0,
												TermStart,TermEnd,abstol,&m,EigenValue,
												EigenVectors?EigenVectors->GetVec().Begin():NULL,
												EigenVectors?EigenVectors->GetLeadingDim():0,
												&opti_workspace_size,-1,&ifail_iWork[GetSize_Col()+1],ifail_iWork);
			if(info){ return E_UNEXPECTED; }
			if(Workspc.SetSize((UINT)opti_workspace_size)){}
			else{ return E_OUTOFMEMORY; }
		}
	
		info = ::mkl::mkl_cpp::mkl_syevx(	(EigenVectors!=NULL)?'V':'N','I','U',
											GetSize_Col(),*this,GetLeadingDim(),0,0,
											TermStart,TermEnd,abstol,&m,EigenValue,
											*EigenVectors,EigenVectors?EigenVectors->GetLeadingDim():0,
											Workspc,Workspc.GetSize(),&ifail_iWork[GetSize_Col()+1],ifail_iWork);
		if(info)
		{
			if(info>0)
			{	_CheckDump("::mkl::CMatrix::PartialSolveSymmetricEigen: Total "<<info<<" eigenvalues failed to converge:");
				for(int i=0;i<info-1;i++)
					_CheckDump(ifail_iWork[i]<<", ");
				_CheckDump(ifail_iWork[info-1]<<"\n");
			}
			return info<0?E_INVALIDARG:E_ABORT;
		}
		else
			return S_OK;
	}

	// Symmetric Eigen Problem based on Relatively Robust Representations.
	// solve eigens of [TermStart,TermEnd] zero-based, eigenvalue is in ascending order
	// EigenVectors in rows
	template<class t_BaseVec2>
	HRESULT PartialSolveSymmetricEigen_Robust(	UINT TermStart,UINT TermEnd, 
												::mkl::_meta_::CVector_Base<t_Val,t_BaseVec2>& EigenValue, 
												CMatrix<t_Val>* EigenVectors = NULL)
	{	ASSERT(IsSquare());
		ASSERT( TermStart>=0 && TermStart<=TermEnd && TermEnd<GetSize_Col() );
		TermStart++;   TermEnd++;   // MKL has 1-based index

		if(EigenValue.SetSize(GetSize_Col())){}
		else{ EigenValue.IsRef?E_INVALIDARG:E_OUTOFMEMORY; }
		
		if( EigenVectors )
		{	if(EigenVectors->SetSize(GetSize_Col(),TermEnd-TermStart+1)){}
			else{ EigenVectors->IsRef?E_INVALIDARG:E_OUTOFMEMORY; }
		}

		::rt::Buffer_32BIT<int> isuppz;
		if(isuppz.SetSize(2*GetSize_Col())){}else{ return E_OUTOFMEMORY; }

		t_Val abstol = 2*slamch("S");
		int  m=0, info;

		mkl::_meta_::CVector_Compact<t_Val> Workspc;
		::rt::Buffer_32BIT<int> iwork;
		// query optimized working space
		{
			t_Val opti_workspace_size = 0;
			int   opti_iwork_size = 0;
			info = ::mkl::mkl_cpp::mkl_syevr(	(EigenVectors!=NULL)?'V':'N','I','U',
												GetSize_Col(),*this,GetLeadingDim(),0,0,
												TermStart,TermEnd,abstol,&m,EigenValue,
												*EigenVectors,EigenVectors?EigenVectors->GetLeadingDim():0,isuppz,
												&opti_workspace_size,-1,&opti_iwork_size,-1);
			if(info){ return E_UNEXPECTED; }
			if(Workspc.SetSize((UINT)opti_workspace_size)){}else{ return E_OUTOFMEMORY; }
			if(iwork.SetSize((UINT)opti_iwork_size)){}else{ return E_OUTOFMEMORY; }
		}

		info = ::mkl::mkl_cpp::mkl_syevr(	(EigenVectors!=NULL)?'V':'N','I','U',
											GetSize_Col(),*this,GetLeadingDim(),0,0,
											TermStart,TermEnd,abstol,&m,EigenValue,
											*EigenVectors,EigenVectors?EigenVectors->GetLeadingDim():0,isuppz,
											Workspc,Workspc.GetSize(),iwork,iwork.GetSize());
		if(info)
			return info<0?E_INVALIDARG:E_ABORT;
		else
		{	EigenValue.ChangeSize(m);
			return S_OK;
		}
	}


	// SVD (Driver function for PartialSolveSymmetricEigen)
	// [*this] turn to EigenVectors after solving in descending order
	template<class t_BaseVec2>
	HRESULT SolveEigen_ByTerm(::mkl::_meta_::CVector_Base<t_Val,t_BaseVec2>& EigenValue,UINT FeatureDimenMax,UINT FeatureDimenMin = 1,BOOL NeedEigenVectors = TRUE)
	{	UINT dimen = GetSize_Col();
		ASSERT( dimen == GetSize_Row() );
		ASSERT(FeatureDimenMax >= FeatureDimenMin);
		ASSERT(dimen >= FeatureDimenMax);
		
		CMatrix<t_Val> MatTemp;

		HRESULT ret;
		ret = PartialSolveSymmetricEigen_Robust(	dimen-FeatureDimenMax,dimen-1,
													EigenValue,NeedEigenVectors?&MatTemp:NULL);
		if( FAILED(ret) )return ret;
		UINT org_terms = EigenValue.GetSize();
		EigenValue.Flip();

		{	UINT i=0;
			t_Val EigenMax = EigenValue[0];
			for(;i<EigenValue.GetSize();i++)
			{
				if(	::num::IsNumberOk(EigenValue[i]) && 
					(EigenValue[i]/EigenMax)>::rt::TypeTraits<t_Val>::Epsilon()*10 ){ continue; }
				else{ break; }
			}
			EigenValue.ChangeSize(max(FeatureDimenMin,i));
		}

		//reserves eigenvectors
		if( NeedEigenVectors )
		{
			if(EigenValue.GetSize() == MatTemp.GetSize_Col()){ ::rt::Swap(*this,MatTemp); }
			else
			{	if(SetSize(MatTemp.GetSize_Row(),EigenValue.GetSize()))
				{	*this = MatTemp.GetSub(0,org_terms-EigenValue.GetSize(),MatTemp.GetSize_Row(),EigenValue.GetSize());
				}
				else{ return E_OUTOFMEMORY; }
			}
			FlipColumns();
		}

		return EigenValue.GetSize()==FeatureDimenMax?S_OK:S_FALSE;
	}

	// Symmetric Eigen Problem
	// solve eigens of (EigenvalueMin,EigenvalueMax], eigenvalue is in ascending order
	// EigenVectors in rows
	template<class t_BaseVec2>
	HRESULT PartialSolveSymmetricEigen_Robust(	::mkl::_meta_::CVector_Base<t_Val,t_BaseVec2>& EigenValue, 
												t_Val EigenvalueMin = EPSILON,
												CMatrix<t_Val>* EigenVectors = NULL,
												t_Val EigenvalueMax = FLT_MAX)
	{	ASSERT(IsSquare());
		ASSERT( EigenvalueMin<EigenvalueMax );
		UINT order = GetSize_Col();
		
		if(EigenValue.SetSize(order)){}else{ EigenValue.IsRef?E_INVALIDARG:E_OUTOFMEMORY; }
		
		if( EigenVectors )
		{	if(EigenVectors->SetSize(order,order)){}
			else{ EigenVectors->IsRef?E_INVALIDARG:E_OUTOFMEMORY; }
		}

		::rt::Buffer_32BIT<int> isuppz;
		if(isuppz.SetSize(2*GetSize_Col())){}else{ return E_OUTOFMEMORY; }

		t_Val abstol = 2*slamch("S");
		int  m=0, info;

		mkl::_meta_::CVector_Compact<t_Val> Workspc;
		::rt::Buffer_32BIT<int> iwork;
		// query optimized working space
		{
			t_Val opti_workspace_size = 0;
			int   opti_iwork_size = 0;
			info = ::mkl::mkl_cpp::mkl_syevr(	(EigenVectors!=NULL)?'V':'N','V','U',
												GetSize_Col(),*this,GetLeadingDim(),EigenvalueMin,EigenvalueMax,
												0,0,abstol,&m,EigenValue,
												EigenVectors?(EigenVectors->GetVec().Begin()):NULL,
												EigenVectors?EigenVectors->GetLeadingDim():1,isuppz,
												&opti_workspace_size,-1,&opti_iwork_size,-1);
			if(info){ return E_UNEXPECTED; }
			if(Workspc.SetSize((UINT)opti_workspace_size)){}else{ return E_OUTOFMEMORY; }
			if(iwork.SetSize((UINT)opti_iwork_size)){}else{ return E_OUTOFMEMORY; }
		}
	
		info = ::mkl::mkl_cpp::mkl_syevr(	(EigenVectors!=NULL)?'V':'N','V','U',
											GetSize_Col(),*this,GetLeadingDim(),EigenvalueMin,EigenvalueMax,
											0,0,abstol,&m,EigenValue,
											EigenVectors?(EigenVectors->GetVec().Begin()):NULL,
											EigenVectors?EigenVectors->GetLeadingDim():1,isuppz,
											Workspc,Workspc.GetSize(),iwork,iwork.GetSize());
		if(info)
			return info<0?E_INVALIDARG:E_ABORT;
		else
		{	EigenValue.ChangeSize(m);
			return S_OK;
		}
	}

	// SVD (Driver function for PartialSolveSymmetricEigen)
	// [*this] turn to EigenVectors (in rows) after solving in descending order
	template<class t_BaseVec2>
	HRESULT SolveEigen_ByValue(::mkl::_meta_::CVector_Base<t_Val,t_BaseVec2>& EigenValue,t_Val EigenvalueMin = EPSILON, BOOL NeedEigenVectors = TRUE)
	{	int dimen = GetSize_Col();
		ASSERT( dimen == GetSize_Row() );
		ASSERT(EigenvalueMin>0);

		CMatrix<t_Val> MatTemp;

		HRESULT ret;
		ret = PartialSolveSymmetricEigen_Robust(EigenValue,EigenvalueMin,NeedEigenVectors?&MatTemp:NULL);
		if( FAILED(ret) )return ret;

		EigenValue.Flip();

		//reserves eigenvectors
		if( NeedEigenVectors )
		{
			if(SetSize(MatTemp.GetSize_Row(),EigenValue.GetSize()))
			{	*this = MatTemp.GetSub(0,0,MatTemp.GetSize_Row(),EigenValue.GetSize());
			}
			else{ return E_OUTOFMEMORY; }
			FlipColumns();
		}

		return S_OK;
	}

	// SVD (Driver function for PartialSolveSymmetricEigen)
	// [*this] turn to EigenVectors after solving in descending order
	template<class t_BaseVec2>
	HRESULT SolveEigen_ByEnerge(::mkl::_meta_::CVector_Base<t_Val,t_BaseVec2>& EigenValue,float EnergePreservationRate = 0.98f,UINT dimen_max = UINT_MAX, BOOL ignore_first_eige_in_energy = FALSE)
	{	UINT term;
		if(EnergePreservationRate < 0.979999999f)
		{	HRESULT hr;
			CMatrix<t_Val>	mat_temp;
			mat_temp.SetSizeAs(*this);
			mat_temp = *this;
			if(SUCCEEDED(hr=mat_temp.SolveEigen_ByValue(EigenValue,FLT_EPSILON,FALSE)))
			{	if(ignore_first_eige_in_energy)EigenValue[0] = (t_Val)2.0 * ::rt::TypeTraits<t_Val>::Epsilon();
				t_Val tot_energe = EigenValue.Sum()*EnergePreservationRate;
				UINT i=0;
				for(;i<EigenValue.GetSize();i++)
					if(	EigenValue[i] < tot_energe && 
						EigenValue[i] > ::rt::TypeTraits<t_Val>::Epsilon() )
					{	tot_energe -= EigenValue[i]; }
					else{ break; }
				term = min(dimen_max,i);
			}else{ return hr; }
		}
		else term = min(dimen_max,GetSize_Col());

		return SolveEigen_ByTerm(EigenValue,term);
	}

};
template<class t_Ostream, typename t_Ele, class BaseVector>
t_Ostream& operator<<(t_Ostream& Ostream, const ::mkl::CMatrix<t_Ele,BaseVector> & km)
{
	UINT ele_limite = ::rt::_meta_::IsStreamStandard(Ostream)?8:UINT_MAX;
	if(km.IsNull())
	{	Ostream<<"{ -NULL- }";
		return Ostream;
	}

	Ostream<<"{ // "<<km.GetSize_Row()<<'x'<<km.GetSize_Col()<<" Matrix";
	static const bool transposed = !::mkl::CMatrix<t_Ele,BaseVector>::IsColumnMajor;

	__static_if( !transposed ){ Ostream<<'\n'; }
	__static_if( transposed ){ Ostream<<" (transposed)\n"; }
	if(km.GetSize_Row()<=ele_limite)
	{//print all
		for(UINT i=0;i<km.GetSize_Row()-1;i++)
		{
			Ostream<<km.GetRow(i)<<','<<'\n';
		}
		Ostream<<km.GetRow(km.GetSize_Row()-1)<<'\n';
	}
	else
	{//print head and tail
		for(UINT i=0;i<=4;i++)
			Ostream<<km.GetRow(i)<<','<<'\n';
		Ostream<<"  ... ...\n";
		Ostream<<km.GetRow(km.GetSize_Row()-2)<<','<<'\n';
		Ostream<<km.GetRow(km.GetSize_Row()-1)<<'\n';
	}
	Ostream<<'}'<<'\n';

	return Ostream;
}

//////////////////////////////////////////////////////////////////////
// Matrix reference
namespace _meta_
{

template<typename t_Val, bool t_ColumeMajor>
class _BaseMklMatrixRefRoot;
	template<typename t_Val> // column major
	class _BaseMklMatrixRefRoot<t_Val,true>
	{
	protected:
		DYNTYPE_FUNC _BaseMklMatrixRefRoot(const _BaseMklMatrixRefRoot &x){ ASSERT_STATIC(0); }
		DYNTYPE_FUNC _BaseMklMatrixRefRoot(){}

	public:
		static const bool IsRef = true;
		DYNTYPE_FUNC BOOL SetSize(UINT co=0){ return (co==GetSize())?TRUE:FALSE; }
		DYNTYPE_FUNC BOOL ChangeSize(UINT new_size){ return (new_size==GetSize())?TRUE:FALSE; }

		//TypeTraits
		TYPETRAITS_DECL_LENGTH(TYPETRAITS_SIZE_UNKNOWN)
		TYPETRAITS_DECL_IS_AGGREGATE(false)

		template<typename T,typename T2>
		DYNTYPE_FUNC t_Val & At(const T i,const T2 j)
		{ return lpData[j*LeadingDimen+i]; }
		template<typename T,typename T2>
		DYNTYPE_FUNC const t_Val & At(const T i,const T2 j) const
		{ return lpData[j*LeadingDimen+i]; }

		DYNTYPE_FUNC UINT GetSize() const{ return (UINT)(row_count*column_count); }

		::mkl::CVector_RefCompact<t_Val>
		GetLine(UINT y){ return ::mkl::CVector_RefCompact<t_Val>(&At(0,y),GetWidth()); }
		const ::mkl::CVector_RefCompact<t_Val> 
		GetLine(UINT y) const{ return ::rt::_CastToNonconst(this)->GetLine(y); }

	protected:
		UINT	column_count;
		UINT	row_count;
		UINT	Padding;	
		UINT	LeadingDimen; // LeadingDimen = row + Padding;
		t_Val*	lpData;

	public:
		DYNTYPE_FUNC BOOL SetSize_2D(UINT row=0,UINT col=0){ return row==row_count && col==column_count; }

		DYNTYPE_FUNC UINT		GetWidth() const{ return row_count;}
		DYNTYPE_FUNC UINT		GetHeight() const{ return column_count;}
	};
	template<typename t_Val> // row major
	class _BaseMklMatrixRefRoot<t_Val,false>: public _BaseMklMatrixRefRoot<t_Val,true>
	{
	protected:
		DYNTYPE_FUNC _BaseMklMatrixRefRoot(const _BaseMklMatrixRefRoot &x){ ASSERT_STATIC(0); }
		DYNTYPE_FUNC _BaseMklMatrixRefRoot(){}

	public:
		static const bool IsRef = true;

		template<typename T,typename T2>
		DYNTYPE_FUNC t_Val & At(const T i,const T2 j)
		{ return lpData[j+i*LeadingDimen]; }
		template<typename T,typename T2>
		DYNTYPE_FUNC const t_Val & At(const T i,const T2 j) const
		{ return lpData[j+i*LeadingDimen]; }

		::mkl::CVector_RefCompact<t_Val>
		GetLine(UINT y){ return ::mkl::CVector_RefCompact<t_Val>(&At(y,0),GetWidth()); }
		const ::mkl::CVector_RefCompact<t_Val> 
		GetLine(UINT y) const{ return ::rt::_CastToNonconst(this)->GetLine(y); }

		DYNTYPE_FUNC UINT		GetWidth() const{ return column_count; }
		DYNTYPE_FUNC UINT		GetHeight() const{ return row_count; }
	};

template<bool column_major>
class _BaseMklMatrixRef
{public:	template<typename t_Ele> class SpecifyItemType
			{public: typedef ::mkl::_meta_::_BaseMklMatrixRefRoot<t_Ele,column_major> t_BaseVec; };
			static const bool IsCompactLinear = false;
			static const bool IsColumnMajor = column_major;
};
}// namespace _meta_



//////////////////////////////////////////////////////////////////////
// Basic 2D image class
template<typename t_Val, bool t_ColumeMajor = true>
class CMatrix_Ref:public ::mkl::CMatrix<t_Val,::mkl::_meta_::_BaseMklMatrixRef<t_ColumeMajor> >
{	
	template< typename t_Val, class BaseImage >
	friend class CMatrix;
	DYNTYPE_FUNC void _Init(t_Val* ptr,UINT row_co,UINT col_co,UINT Leading) //internal use only
	{	lpData=ptr; row_count=row_co; column_count=col_co; LeadingDimen=Leading; 
		Padding=LeadingDimen - (t_ColumeMajor?row_co:col_co);
	}

protected:
	DYNTYPE_FUNC CMatrix_Ref(){ ASSERT_STATIC(0); }

public:
	DYNTYPE_FUNC CMatrix_Ref(const CMatrix_Ref& x){ ASSERT_STATIC(0); }	//The two ASSERTs fired when trying to construct 
	//a Ref from a Ref instance or an empty instance which is not allowed. Avoid trying to copy a Ref
	//instance, it is highly recommended to use Ref& instead  for local using and function arguments

public:
	template<class BaseVec>
	DYNTYPE_FUNC CMatrix_Ref(::mkl::CMatrix<t_Val,BaseVec>& x)
	{	/*_Init(x,x.GetSize_RowCount(),x.GetSize_ColumnCount(),x.GetLeadingDim());*/
		_Init(x, x.GetSize_Row(), x.GetSize_Col(), x.GetLeadingDim());
		ASSERT_STATIC(t_ColumeMajor == BaseVec::IsColumnMajor); 
	}  // MUST be the same major type

	template<class BaseVec>
	DYNTYPE_FUNC CMatrix_Ref(rt::Image<t_Val,BaseVec>& x)
	{	ASSERT(x.GetStep()%sizeof(t_Val) == 0);
		_Init(x,t_ColumeMajor?GetWidth():GetHeight(),t_ColumeMajor?GetHeight():GetWidth(),x.GetStep()/sizeof(t_Val));
	}

	DYNTYPE_FUNC CMatrix_Ref(t_Val* ptr,UINT row_co,UINT col_co,UINT Leading)
	{	_Init(ptr,row_co,col_co,Leading);
	}

public:
	COMMON_IMAGE_STUFFS(CMatrix_Ref)
	const CMatrix& operator = (const CMatrix<t_Val>& x){ return __super::operator =(!!x); }
	template<bool t_IsColumnMajor>
	const CMatrix& operator = (const CMatrix_Ref<t_Val,t_IsColumnMajor>& x){ return __super::operator =(!!x); }
};


typedef CMatrix<float>			CMatrix32;
typedef CMatrix<double>			CMatrix64;

typedef CMatrix_Ref<float>		CMatrix32_Ref;
typedef CMatrix_Ref<double>		CMatrix64_Ref;


} // namespace mkl

