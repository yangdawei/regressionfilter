#pragma once

//////////////////////////////////////////////////////////////////////
// Visual Computing Foundation Classes (VCFC)
//					Jiaping Wang  2006.2
//					e_boris2002@hotmail.com
//
// (C) Copyright Jiaping Wang 2004.
//
// Permission to copy, use, modify, sell and distribute this software
// is granted provided this copyright notice appears in all copies.
// This software is provided "as is" without express or implied
// warranty, and with no claim as to its suitability for any purpose.
//
// Absolutly no garanty!!
//////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////
//  Brief Description
//
//  mkl_linequ.h
//	Linear Equation Builder. setup lines of equations for the left-hand
//	matrix A and then returns A'A and A'B or solve the result
//
//////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////
// Revise History
//
// Inital version	2006.10.26		Jiaping
//
//////////////////////////////////////////////////////////////////////

#include "mkl_matrix.h"
#include <algorithm>

namespace mkl
{

template<typename t_Val = float, typename t_MatrixValue = float>
class CLinearEquationSolver
{
protected:
	struct _NonZeroItem
	{	union
		{	ULONGLONG	_ordinal;
			struct{UINT i,j; };
		};
		t_Val	value;
		TYPETRAITS_DECL_IS_AGGREGATE(true);
		bool operator <(const _NonZeroItem& x)
		{	ASSERT(_ordinal!=x._ordinal);	// duplicate entry found, entry should be unique
			return _ordinal<x._ordinal;  
		}
	};
	struct _Value
	{	UINT	i;
		t_Val	value;
		TYPETRAITS_DECL_IS_AGGREGATE(true);
	};
protected:
	::rt::BufferEx_32BIT<_NonZeroItem>	m_Entries;		// all non-zero entries

	UINT								m_VaribleCount;	// number of varibles to be solved
	UINT								m_EquationCount;// number of equation to be solved

	BOOL								m_StructIsDirty;
	::rt::Buffer_32BIT<UINT>			m_ColumnIndex;	// refer to m_Entries, UINT[m_VaribleCount+1]
	::rt::Buffer_32BIT<_Value>			m_Coefficients;	// all non-zero value

protected:
	void UpdateSparseStruct()
	{	ASSERT(m_StructIsDirty);
		if( m_Entries.GetSize() )
		{	if( !m_Coefficients.GetSize() ){}
			else // merge m_Coefficients to m_Entries
			{	UINT NewEntry = m_Entries.GetSize();
				VERIFY(m_Entries.ChangeSize(NewEntry + m_Coefficients.GetSize()));
				_NonZeroItem* pEntry = &m_Entries[NewEntry];
				_Value* p = m_Coefficients;
				for(UINT j=0;j<m_ColumnIndex.GetSize();j++)
				{	_Value* pend = &m_Coefficients[m_ColumnIndex[j+1]];
					for(;p<pend;p++,pEntry++)
					{	pEntry->i = p->i;
						pEntry->value = p->value;
						pEntry->j = j;
					}
				}
			}
			std::sort(m_Entries.Begin(),m_Entries.End());

			m_Coefficients.SetSize(m_Entries.GetSize());
			_NonZeroItem*	pEntry = m_Entries;
			_NonZeroItem*	pEntryEnd = m_Entries.End();
			{	// update value
				_Value*			pValue = m_Coefficients;
				for(;pEntry<pEntryEnd;pEntry++,pValue++)
				{	pValue->i = pEntry->i;
					pValue->value = pEntry->value;
				}
			}
			{	// update m_ColumnIndex
				ASSERT(m_ColumnIndex[0] == 0);
				pEntry = m_Entries;
				for(UINT offset=1;pEntry<pEntryEnd;pEntry++,offset++)
					m_ColumnIndex[pEntry->j+1] = offset;
			}
		}
		m_Entries.SetSize();
		m_StructIsDirty = FALSE;
	}

public:
	CLinearEquationSolver()
	{	m_VaribleCount=m_EquationCount=0; 
		m_StructIsDirty  =TRUE;
	}
	void DefineProblem(UINT equ_co, UINT x_co, UINT estimated_nonzeros = 100)
	{	ASSERT(x_co);
		ASSERT(equ_co);
		m_EquationCount = equ_co;
		m_VaribleCount = x_co;
		m_StructIsDirty = TRUE;

		m_Entries.SetSize();
		m_Entries.reserve(max(estimated_nonzeros,10));

		m_Coefficients.SetSize();
		m_ColumnIndex.SetSize(x_co+1);
		m_ColumnIndex.Zero();	
	}

	__forceinline void AddCoefficient(UINT equation_i,UINT varible_j)
	{	ASSERT( equation_i< m_EquationCount );
		ASSERT( varible_j< m_VaribleCount );
		_NonZeroItem& item = m_Entries.push_back();
		item.i = equation_i;
		item.j = varible_j;
		m_StructIsDirty = TRUE;
	}

	__forceinline void AddCoefficient(UINT equation_i,UINT varible_j,const t_Val & value)
	{	ASSERT( equation_i< m_EquationCount );
		ASSERT( varible_j< m_VaribleCount );
		_NonZeroItem& item = m_Entries.push_back();
		item.i = equation_i;
		item.j = varible_j;
		item.value = value;
		m_StructIsDirty = TRUE;
	}

	void SetCoefficient(UINT equation_i,UINT varible_j,const t_Val & value)
	{	ASSERT( !m_StructIsDirty ); // call only after solve once
		_Value* p = &m_Coefficients[m_ColumnIndex[varible_j]];
		_Value* pend = &m_Coefficients[m_ColumnIndex[varible_j+1]]-1;
		for(;p<=pend;)
		{	if(p->i == equation_i)
			{	p->value=value; return; }
		}
		ASSERT(0); // not exist, you can not change the sparse struct via this function
	}

	template<typename t_Val2, class t_BaseVec>
	void ComputeLeftHand(::mkl::CMatrix<t_Val2,t_BaseVec>& AT_A)	// A'*A
	{	if(m_StructIsDirty)UpdateSparseStruct();
		VERIFY(AT_A.SetSize(m_VaribleCount,m_VaribleCount));
		AT_A.Zero();

		for(UINT i=0;i<m_VaribleCount;i++)
		{	UINT count = m_ColumnIndex[i+1] - m_ColumnIndex[i];
			if(count)
			{	// col[i]*col[i]
				{	t_Val2& dst = AT_A(i,i);
					const _Value* p = &m_Coefficients[m_ColumnIndex[i]];
					const _Value* pend = &m_Coefficients[m_ColumnIndex[i+1]];
					for(;p<pend;p++)
						dst += ::rt::Sqr(p->value);
				}

				for(UINT j=i+1;j<m_VaribleCount;j++)
				{	const _Value* pi = &m_Coefficients[m_ColumnIndex[i]];
					const _Value* piend = &m_Coefficients[m_ColumnIndex[i+1]];
					const _Value* pj = &m_Coefficients[m_ColumnIndex[j]];
					const _Value* pjend = &m_Coefficients[m_ColumnIndex[j+1]];
					t_Val2& dst = AT_A(i,j);

					for(;;)
					{	if(pj->i < pi->i)
						{	pj++;	if(pj<pjend){}else break; }
						else if(pj->i > pi->i)
						{	pi++;	if(pi<piend){}else break; }
						else // matched
						{	dst += pi->value*pj->value;
							pi++; pj++;
							if(pi<piend && pj<pjend){}
							else break;
						}
					}

					AT_A(j,i) = dst;
				}
			}
		}
	}
	template<typename t_Val2, typename t_Val3>
	void ComputeRightHand(const ::mkl::CVector<t_Val2>& B, ::mkl::CVector<t_Val3>& AT_B)	// A'*B
	{	ASSERT(B.GetSize() == m_EquationCount);
		VERIFY(AT_B.SetSize(m_VaribleCount));
		AT_B.Zero();

		if(m_StructIsDirty)UpdateSparseStruct();

		_Value* p = m_Coefficients;
		for(UINT j=0;j<m_VaribleCount;j++)
		{	_Value* pend = &m_Coefficients[m_ColumnIndex[j+1]];
			t_Val3 & dst = AT_B[j];
			for(;p<pend;p++)
				dst += p->value*B[p->i];
		}
	}
	template<typename t_Val2, class t_BaseVec2,typename t_Val3, class t_BaseVec3>
	void ComputeRightHand(const ::mkl::CMatrix<t_Val2,t_BaseVec2>& B, ::mkl::CMatrix<t_Val3,t_BaseVec3>& AT_B)	// A'*B
	{	ASSERT( B.GetSize_Col());
		ASSERT(	B.GetSize_Row() == m_EquationCount );
		AT_B.SetSize(m_VaribleCount,B.GetSize_Col());
		AT_B.Zero();

		if(m_StructIsDirty)UpdateSparseStruct();

		_Value* p = m_Coefficients;
		for(UINT j=0;j<m_VaribleCount;j++)
		{	typename ::mkl::CMatrix<t_Val3,t_BaseVec3>::t_RowRef& dst_row = AT_B.GetRow(j);
			_Value* pend = m_Coefficients[m_ColumnIndex[j+1]];
			for(;p<pend;p++)
				dst_row.Add_Scaled( p->value, B.GetRow(p->i) );
		}
	}

	template<typename t_Val2, typename t_Val3>
	HRESULT Solve(const ::mkl::CVector<t_Val2>& B, ::mkl::CVector<t_Val3>& X)	// A'A*X = A'B
	{	if( !X.SetSize(m_VaribleCount) ){ return E_OUTOFMEMORY; }
		ComputeRightHand(B,X);
		
		::mkl::CMatrix<t_MatrixValue>	AT_A;
		if( !AT_A.SetSize(m_VaribleCount,m_VaribleCount) ){ return E_OUTOFMEMORY; }
		ComputeLeftHand(AT_A);

		return AT_A.SolveLinearLeastSquares_LQR(X);
	}

	template<typename t_Val2, class t_BaseVec2,typename t_Val3, class t_BaseVec3>
	HRESULT Solve(const ::mkl::CMatrix<t_Val2,t_BaseVec2>& B, ::mkl::CMatrix<t_Val3,t_BaseVec3>& AT_B) // A'A*X = A'B
	{	if( !X.SetSize(m_VaribleCount,B.GetSize_Col()) ){ return E_OUTOFMEMORY; }
		ComputeRightHand(B,X);
		
		::mkl::CMatrix<t_MatrixValue>	AT_A;
		if( !AT_A.SetSize(m_VaribleCount,m_VaribleCount) ){ return E_OUTOFMEMORY; }
		ComputeLeftHand(AT_A);

		return AT_A.SolveLinearLeastSquares_LQR(X);
	}
};

} // namespace mkl