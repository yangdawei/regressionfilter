#pragma once

//////////////////////////////////////////////////////////////////////
// Visual Computing Foundation Classes (VCFC)
//					Jiaping Wang  2006.2
//					e_boris2002@hotmail.com
//
// (C) Copyright Jiaping Wang 2004.
//
// Permission to copy, use, modify, sell and distribute this software
// is granted provided this copyright notice appears in all copies.
// This software is provided "as is" without express or implied
// warranty, and with no claim as to its suitability for any purpose.
//
// Absolutly no warranty!!
//////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////
//  Brief Description
//
//  mkl_sparse.h
//	Sparse Blas 
//	PARDISO in MKL
//
//////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////
// ChangeLog
//
// Inital version	2006.09.15		Jiaping
//
// Ver 1            2008.11.07		Yiming
// 		* Add a single-righthand conjugate-gradient iterative solver 
// 		  SolverLeastSquare_CG;
// 		* Fix a typo in the copyright section;
// 		* Change "Revise History" to "ChangeLog".
//
//////////////////////////////////////////////////////////////////////

#include "mkl_matrix.h"
#include "..\num\sparse_matrix.h"
#include <mkl_spblas.h>
#include <mkl_rci.h>

#ifndef MKL_INT
	#define MKL_INT int
#endif

namespace mkl
{

//////////////////////////////////////////////////////////////////////
// PARDISO CSR sparse matrix format p2289 in MKL's manual (A-8)
template<typename t_Val, bool t_Transposed = false, typename t_LinearizedPos = UINT>
class CMatrix_Sparse:public num::Sparse_Matrix<t_Val,t_Transposed,t_LinearizedPos>
{
protected:
	::rt::Buffer_32BIT<UINT>	m_Columns;	// size = value's length, index the col index that this value in (1-based)
	::rt::Buffer_32BIT<UINT>	m_RowIndex; // size = GetSize_Row()+1
	BOOL						m_SparseStructureForTranspose;

public:
	COMMON_CONSTRUCTOR_IMG(CMatrix_Sparse)
	COMMON_SPARSE_MATRIX_STUFFS(CMatrix_Sparse)

public:
	typedef CMatrix_Sparse<t_Val,!t_Transposed,t_LinearizedPos> t_TransposedMatrix;
	DYNTYPE_FUNC t_TransposedMatrix& operator !(){ return *((t_TransposedMatrix*)this); }
	DYNTYPE_FUNC const t_TransposedMatrix& operator !() const{ return ::rt::_CastToNonconst(this)->operator !(); }

	//////////////////////////////////////////////////////////
	// call sync before using any other computational methods 
	// in the same form. if you use (!x).LeftProduct(...), you
	// should call (!x).Sync() first. 
	void Sync() // update CSR's format consistent to Sparse_Matrix
	{	// sparse structure
		{	// determine m_RowIndex
			::rt::Buffer_32BIT<UINT> workspc;
			workspc.SetSize(GetSize_Row());
			{	// counting entries in every row
				::rt::Buffer_Ref_32BIT<UINT> row_counter(workspc.Begin(),GetSize_Row());
				row_counter.Zero();
				t_pNonzeroEntry pEntry = m_EntryMap.begin();
				for(;pEntry!=m_EntryMap.end();pEntry++)
				{	UINT i,j;
					_ParseLinearized(pEntry->first,i,j);
					row_counter[i]++;
				}
				// setup m_RowIndex
				m_RowIndex.SetSize(GetSize_Row()+1);
				m_RowIndex[0] = 0;
				for(UINT i=0;i<GetSize_Row();i++)
					m_RowIndex[i+1] = m_RowIndex[i] + row_counter[i];
			}
			// determine m_Columns
			m_Columns.SetSize(GetNonzeroEntryCount());
			{	m_Columns = INT_MAX;
				::rt::Buffer_Ref_32BIT<UINT> row_entrycount(workspc.Begin(),GetSize_Row());
				row_entrycount.Zero();
				t_pNonzeroEntry pEntry = m_EntryMap.begin();
				for(;pEntry!=m_EntryMap.end();pEntry++)
				{	UINT i,j;
					_ParseLinearized(pEntry->first,i,j);
					UINT *columns = &m_Columns[m_RowIndex[i]];
					UINT *end = &columns[row_entrycount[i]];
					for(;end>columns;end--)
					{	if(end[-1] > j){ end[0] = end[-1]; continue; }
						else{ break; }
					}
					end[0] = j;
					row_entrycount[i]++;
				}
#ifdef _DEBUG	// check row_entrycount
				for(UINT i=0;i<GetSize_Row();i++)
					ASSERT(row_entrycount[i] == (m_RowIndex[i+1] - m_RowIndex[i]));
#endif
			}
		}
		// re-order values
		{	::rt::BufferEx_32BIT<t_Val>	new_value;
			new_value.SetSize(m_Values.GetSize());
			UINT vid = 0;
			for(UINT i=0;i<GetSize_Row();i++)
			{	UINT * pj = &m_Columns[m_RowIndex[i]];
				UINT * pend = &m_Columns[m_RowIndex[i+1]];
				for(;pj<pend;pj++,vid++)
				{	t_EntryMap::iterator p = m_EntryMap.find(_Linearize(i,*pj));
					new_value[vid] = m_Values[p->second];
					p->second = vid;
				}
			}
			::rt::Swap(new_value,m_Values);
		}
		// MKL is 1-based index
		m_Columns++;
		m_RowIndex++;
		m_SparseStructureForTranspose = t_Transposed;
	}

	/////////////////////////////////////////////////////////////
	// c = This*b, c and b 
	template<class t_Vector_B, class t_Vector_C>
	void VectorProduct(const t_Vector_B& b, t_Vector_C& c) const // c = This*b
	{	ASSERT(m_SparseStructureForTranspose == (BOOL)t_Transposed); //call x.Sync before x.LeftProduct or (!x).Sync before (!x).LeftProduct
		ASSERT( GetSize_Col() == b.GetSize() );
		ASSERT( GetSize_Row() == c.GetSize() );

		// optimization for double dense vector (MKL Sparse BLAS level2)
		static const bool Can_Use_MKL_Sparse_BLAS = 
						::rt::TypeTraits<t_Val>::Typeid == ::rt::_typeid_64f &&
						::rt::TypeTraits<typename t_Vector_B::ItemType>::Typeid == ::rt::_typeid_64f &&
						::rt::TypeTraits<typename t_Vector_C::ItemType>::Typeid == ::rt::_typeid_64f &&
						t_Vector_B::IsCompactLinear &&
						t_Vector_C::IsCompactLinear;

		__static_if( Can_Use_MKL_Sparse_BLAS )
		{	char trans = 'N';
			int  m = GetSize_Row();
			mkl_dcsrgemv(	&trans,&m,::rt::_CastToNonconst(m_Values.Begin()),
							(int*)::rt::_CastToNonconst(m_RowIndex.Begin()),
							(int*)::rt::_CastToNonconst(m_Columns.Begin()),
							::rt::_CastToNonconst(b.Begin()),c);
			return;
		}
		// otherwise, general matrix-vector production
		for(UINT i=0;i<GetSize_Row();i++)
		{	UINT count = m_RowIndex[i+1] - m_RowIndex[i];
			const UINT *col_idx = &m_Columns[m_RowIndex[i]-1];
			const t_Val*row_value = &m_Values[m_RowIndex[i]-1];

			t_Val Total = 0;
			for(UINT k=0;k<count;k++)
				Total += row_value[k]*b[ col_idx[k]-1 ];
			c[i] = Total;
		}
	}


	/////////////////////////////////////////////////////////////
	// C = A*This, C and A can be dense or sparse
	template<class t_Matrix_A, class t_Matrix_C>
	void RightProduct(const t_Matrix_A& A, t_Matrix_C& C) const // C = A*This
	{	ASSERT(m_SparseStructureForTranspose != (BOOL)t_Transposed); //call x.Sync before (!x).RightProduct or (!x).Sync before x.RightProduct
		(!(*this)).LeftProduct(!A,!C);
	}

	/////////////////////////////////////////////////////////////
	// C = This*B, C and B can be dense or sparse
	template<class t_Matrix_B, class t_Matrix_C>
	void LeftProduct(const t_Matrix_B& B, t_Matrix_C& C) const // C = This*B
	{	ASSERT(m_SparseStructureForTranspose == (BOOL)t_Transposed); //call x.Sync before x.LeftProduct or (!x).Sync before (!x).LeftProduct
		ASSERT( GetSize_Col() == B.GetSize_Row() );
		ASSERT(C.GetSize_Row() == GetSize_Row());
		ASSERT(C.GetSize_Col() == B.GetSize_Col());

		// optimization for double dense matrix (MKL Sparse BLAS level3)
		typedef ::mkl::CMatrix<double,::mkl::_meta_::_BaseMklMatrix> _mkl_dense_matrix;
		static const bool Can_Use_MKL_Sparse_BLAS = 
						::rt::TypeTraits<t_Val>::Typeid == ::rt::_typeid_64f &&
						COMPILER_INTRINSICS_IS_CONVERTIBLE(t_Matrix_B,_mkl_dense_matrix) &&
						COMPILER_INTRINSICS_IS_CONVERTIBLE(t_Matrix_C,_mkl_dense_matrix);

		__static_if( Can_Use_MKL_Sparse_BLAS )
		{
			char trans[4] = {'N','G','\0','\0'};
			int  m		= GetSize_Row();
			int  n		= B.GetSize_Col();
			int  k		= GetSize_Col();
			double alpha=1,beta=0;
			int ldb = B.GetLeadingDim();
			int ldc = C.GetLeadingDim();
			mkl_dcsrmm(	trans,&m,&n,&k,&alpha,&trans[1],
						::rt::_CastToNonconst(m_Values.Begin()),
						(int*)::rt::_CastToNonconst(m_Columns.Begin()),
						(int*)::rt::_CastToNonconst(m_RowIndex.Begin()), (int*)::rt::_CastToNonconst(&m_RowIndex[1]),
						::rt::_CastToNonconst(B.GetVec().Begin()),&ldb,
						&beta,C,&ldc);		
			return;
		}
		static const bool IsSelfTransposedProduct = COMPILER_INTRINSICS_IS_BASE_OF(t_TransposedMatrix,t_Matrix_B);
		__static_if( IsSelfTransposedProduct )
		{
			if(((const t_TransposedMatrix*)this) == &B) // Same object but transposed
			{	C.Zero();
				for(UINT i=0;i<GetSize_Row();i++)
				{	UINT count = m_RowIndex[i+1] - m_RowIndex[i];
					if(count)
					{	const UINT *col_idx = &m_Columns[m_RowIndex[i]-1];
						const t_Val*row_value = &m_Values[m_RowIndex[i]-1];
						typename t_Matrix_C::t_RowRef& row_c = C.GetRow(i);
						
						for(UINT j=0;j<B.GetSize_Col();j++)
						{	UINT j_count = m_RowIndex[j+1] - m_RowIndex[j];
							if(j_count)
							{	const UINT *j_col_idx = &m_Columns[m_RowIndex[j]-1];
								const t_Val*j_row_value = &m_Values[m_RowIndex[j]-1];
								t_Val total = 0;

								UINT jk = 0;
								UINT k = 0;
								for(;;)
								{	if(j_col_idx[jk]<col_idx[k])
									{	jk++;	if(jk<j_count){}else{ break; } }
									else if(j_col_idx[jk]>col_idx[k])
									{	k++;	if(k<count){}else{ break; } }
									else // non-zero entry matched
									{	total+=row_value[k++]*j_row_value[jk++];
										if(jk<j_count && k<count){}
										else{ break; }
									}
								}
								row_c[j] = total;
							}
						}
					}
				}
			}
			return;
		}

		// for other cases, general matrix production
		for(UINT i=0;i<GetSize_Row();i++)
		{	UINT count = m_RowIndex[i+1] - m_RowIndex[i];
			const UINT *col_idx = &m_Columns[m_RowIndex[i]-1];
			const t_Val*row_value = &m_Values[m_RowIndex[i]-1];

			typename t_Matrix_C::t_RowRef& row_c = C.GetRow(i);

			for(UINT j=0;j<B.GetSize_Col();j++)
			{
				const typename t_Matrix_B::t_ColumnRef& col = B.GetCol(j);
				t_Val Total = 0;
				for(UINT k=0;k<count;k++)
					Total += row_value[k]*col[ col_idx[k]-1 ];
				row_c[j] = Total;
			}
		}
	}

	/////////////////////////////////////////////////////////////
	// Solve a linear least square problem Ax = b with the 
	// iterative conjugate-gradient sparse solver; 
	//
	// Parameters:
	//     * vRhs: [in] b, the right hand of the equation.
	//     * vSol: [inout] x, you should provide the initial 
	//           solution in it. After computation finished, 
	//           it stores the final solution.
	//     * lfRelTolerance: [in] relative tolerance.
	//     * nMaxIter: [in] Maximum number of iterations.
	//
	// Return Value:
	//     An integer, which means:
	//     * 0        : The solver fails.
	//     * Negative : After nMaxIter iterations, the relative 
	//                  error is still greater than lfRelTolerance.
	//                  It may needs some more iterations.
	//     * Positive : Successfully get a solution. The value is 
	//                  the number of iterations it has used.
	int SolveLinearLeastSquare_CG(const CVector_RefCompact<t_Val> &vRhs, CVector_RefCompact<t_Val> &vSol, double lfRelTolerance = 1e-7, UINT nMaxIter = 5000)
	{
		ASSERT(vRhs.GetSize() == GetSize_Row() && vSol.GetSize() == GetSize_Row());
		ASSERT_STATIC(::rt::TypeTraits<t_Val>::Typeid == ::rt::_typeid_64f);
		
		const UINT nEqn    = GetSize_Row();

		::rt::Buffer_32BIT<double>    tmp;

		int    ipar[128];
		double dpar[128];
		tmp.SetSize(nEqn * 4);

		memset(ipar, 0, sizeof(ipar));
		memset(dpar, 0, sizeof(dpar));

		MKL_INT nRet;
		DCG_INIT((int*)&nEqn, &vSol[0], (double *)&vRhs[0], &nRet, &ipar[0], &dpar[0], &tmp[0]);
		if (nRet != 0) {
			return false;
		} else {
			// NOTICE: the index in MKL doc is in Fortran Style.
			// set the maximum number of iterations
			ipar[4]  = (int) nMaxIter;
			// enable residual-based stopping test
			ipar[8]  = 1;
			// disable user-defined stopping test
			ipar[9]  = 0;
			// use non-preconditioned version
			ipar[10] = 0;

			// set relative tolerance
			dpar[0]  = lfRelTolerance;
		}
		DCG_CHECK((int*)&nEqn, &vSol[0], (double *)&vRhs[0], &nRet, &ipar[0], &dpar[0], &tmp[0]);
		
		if (nRet == -1100) {
			return 0;
		}
			
		const char trans = 'N';
		for (bool terminate = false; !terminate;) 
		{
			DCG((int*)&nEqn, &vSol[0], (double *)&vRhs[0], &nRet, &ipar[0], &dpar[0], &tmp[0]);
			switch (nRet) {
				case  0  :  // successfully finished
					terminate = true;
					break;
				case -1  :  // already nMaxIter iterations, but the residual test has not passed, whaw to do next
					return -(int)nMaxIter;
					break;
				case  1  :  // A * tmp(*,0) --> tmp(*,1)
					mkl_dcsrgemv((char *)&trans, (int *)&nEqn, m_Values.Begin(),
							(int*)::rt::_CastToNonconst(m_RowIndex.Begin()),
							(int*)::rt::_CastToNonconst(m_Columns.Begin()),
							&tmp[0],
							&tmp[nEqn]);
					break;
				case -10 :  // the residual norm is invalid
				case -11 :  // enter an inifinite cycle
				case -2  :  // divide by zero
				case  2  :  // this should not happen, because we have disabled user-defined stopping test
				default:
					return 0;
			}
		}

		int nIter;
		DCG_GET((int *)&nEqn, &vSol[0], (double *)&vRhs[0], &nRet, &ipar[0], &dpar[0], &tmp[0], &nIter);

		return nIter;
	}
};


}
