#pragma once

//////////////////////////////////////////////////////////////////////
// Visual Computing Foundation Classes (VCFC)
//					Jiaping Wang  2006.2
//					e_boris2002@hotmail.com
//
// (C) Copyright Jiaping Wang 2004.
//
// Permission to copy, use, modify, sell and distribute this software
// is granted provided this copyright notice appears in all copies.
// This software is provided "as is" without express or implied
// warranty, and with no claim as to its suitability for any purpose.
//
// Absolutely no warranty!!
//////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////
//  Brief Description
//
//  mkl_vector.h
//  VML and CBlas Level 1
//
//////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////
// Revise History
//
// Inital version	2006.8.8		Jiaping
//
//////////////////////////////////////////////////////////////////////

#include "mkl_basic.h"
#include "..\rt\compact_vector.h"
#include "..\num\small_vec.h"


namespace mkl
{

template<typename t_Val, class t_BaseVec>
class CMatrix;

namespace _meta_
{

template<typename t_Val,class t_BaseVec = ::rt::_BaseVecDyn<UINT>>
class CVector_Compact:public ::rt::Buffer<t_Val,t_BaseVec>
{
	ASSERT_STATIC(	rt::TypeTraits< typename ::rt::Remove_Qualifer<t_Val>::t_Result >::Typeid == rt::_typeid_32f || 
					rt::TypeTraits< typename ::rt::Remove_Qualifer<t_Val>::t_Result >::Typeid == rt::_typeid_64f	);
private:
	UINT	__always_be_one;		// make class memory layout same to Stride Vector

public:
	static const int _inc = 1;
	static const bool IsCompactVector = true;
	typedef CVector_Compact<t_Val,::rt::_BaseVecRef<SIZETYPE>>	Ref;
	CVector_Compact(){ __always_be_one = 1; }

public:
	typedef	typename num::SpecifySmallVec<t_Val,1>::t_Result t_V1;
	typedef typename num::SpecifySmallVec<t_Val,2>::t_Result t_V2;
	typedef typename num::SpecifySmallVec<t_Val,3>::t_Result t_V3;
	typedef typename num::SpecifySmallVec<t_Val,4>::t_Result t_V4;

	DYNTYPE_FUNC t_V1 &	v1(){ ASSERT(GetSize()>=1); return *((t_V1*)Begin()); }
	DYNTYPE_FUNC t_V2 &	v2(){ ASSERT(GetSize()>=1); return *((t_V2*)Begin()); }
	DYNTYPE_FUNC t_V3 &	v3(){ ASSERT(GetSize()>=1); return *((t_V3*)Begin()); }
	DYNTYPE_FUNC t_V4 &	v4(){ ASSERT(GetSize()>=1); return *((t_V4*)Begin()); }
	DYNTYPE_FUNC const t_V1& v1()const{ return rt::_CastToNonconst(this)->v1(); }
	DYNTYPE_FUNC const t_V2& v2()const{ return rt::_CastToNonconst(this)->v2(); }
	DYNTYPE_FUNC const t_V3& v3()const{ return rt::_CastToNonconst(this)->v3(); }
	DYNTYPE_FUNC const t_V4& v4()const{ return rt::_CastToNonconst(this)->v4(); }

public:
	// MKL VML functions
	//y = 1.0/x
	DYNTYPE_FUNC void vml_Reciprocal(CVector_Compact&y) const
	{ ASSERT(GetSize()==y.GetSize()); ::mkl::mkl_cpp::vmlInv(y.GetSize(),*this,y); }
	DYNTYPE_FUNC void vml_Reciprocal(){ vml_Reciprocal(*this); }
	//y = x^0.5
	DYNTYPE_FUNC void vml_SquareRoot(CVector_Compact&y) const
	{ ASSERT(GetSize()==y.GetSize()); ::mkl::mkl_cpp::vmlSqrt(y.GetSize(),*this,y); }
	DYNTYPE_FUNC void vml_SquareRoot(){ vml_SquareRoot(*this); }
	//y = 1/x^0.5
	DYNTYPE_FUNC void vml_ReciprocalSquareRoot(CVector_Compact&y) const
	{ ASSERT(GetSize()==y.GetSize()); ::mkl::mkl_cpp::vmlInvSqrt(y.GetSize(),*this,y); }
	DYNTYPE_FUNC void vml_ReciprocalSquareRoot(){ vml_ReciprocalSquareRoot(*this); }
	//y = x^0.3333333
	DYNTYPE_FUNC void vml_CubeRoot(CVector_Compact&y) const
	{ ASSERT(GetSize()==y.GetSize()); ::mkl::mkl_cpp::vmlCbrt(y.GetSize(),*this,y); }
	DYNTYPE_FUNC void vml_CubeRoot(){ vml_CubeRoot(*this); }
	//y = 1/x^0.333333333
	DYNTYPE_FUNC void vml_ReciprocalCubeRoot(CVector_Compact&y) const
	{ ASSERT(GetSize()==y.GetSize()); ::mkl::mkl_cpp::vmlInvCbrt(y.GetSize(),*this,y); }
	DYNTYPE_FUNC void vml_ReciprocalCubeRoot(){ vml_ReciprocalCubeRoot(*this); }
	//y = e^x
	DYNTYPE_FUNC void vml_Exponential(CVector_Compact&y) const
	{ ASSERT(GetSize()==y.GetSize()); ::mkl::mkl_cpp::vmlExp(y.GetSize(),*this,y); }
	DYNTYPE_FUNC void vml_Exponential(){ vml_Exponential(*this); }
	//y = ln(x)
	DYNTYPE_FUNC void vml_LogarithmNatural(CVector_Compact&y) const
	{ ASSERT(GetSize()==y.GetSize()); ::mkl::mkl_cpp::vmlLn(y.GetSize(),*this,y); }
	DYNTYPE_FUNC void vml_LogarithmNatural(){ LogarithmNatural(*this); }
	//y = log(x)
	DYNTYPE_FUNC void vml_LogarithmDecimal(CVector_Compact&y) const
	{ ASSERT(GetSize()==y.GetSize()); vmlLog10(y.GetSize(),*this,y); }
	DYNTYPE_FUNC void vml_LogarithmDecimal(){ vml_LogarithmDecimal(*this); }
	//y = 2/(sqrt(Pi))* Int(0,x){ e^(-t^2) } dt
	DYNTYPE_FUNC void vml_ErrorFunc(CVector_Compact&y) const
	{ ASSERT(GetSize()==y.GetSize()); ::mkl::mkl_cpp::vmlErf(y.GetSize(),*this,y); }
	DYNTYPE_FUNC void vml_ErrorFunc(){ vml_ErrorFunc(*this); }
	//y = 1 - 2/(sqrt(Pi))* Int(0,x){ e^(-t^2) } dt
	DYNTYPE_FUNC void vml_ErrorFuncComple(CVector_Compact&y) const
	{ ASSERT(GetSize()==y.GetSize()); ::mkl::mkl_cpp::vmlErfc(y.GetSize(),*this,y); }
	DYNTYPE_FUNC void vml_ErrorFuncComple(){ vml_ErrorFuncComple(*this); }
	//y = sin(x)
	DYNTYPE_FUNC void vml_Sin(CVector_Compact&y) const
	{ ASSERT(GetSize()==y.GetSize()); ::mkl::mkl_cpp::vmlSin(y.GetSize(),*this,y); }
	DYNTYPE_FUNC void vml_Sin(){ Sin(*this); }
	//y = cos(x)
	DYNTYPE_FUNC void vml_Cos(CVector_Compact&y) const
	{ ASSERT(GetSize()==y.GetSize()); ::mkl::mkl_cpp::vmlCos(y.GetSize(),*this,y); }
	DYNTYPE_FUNC void vml_Cos(){ vml_Cos(*this); }
	//y = tan(x)
	DYNTYPE_FUNC void vml_Tan(CVector_Compact&y) const
	{ ASSERT(GetSize()==y.GetSize()); ::mkl::mkl_cpp::vmlTan(y.GetSize(),*this,y); }
	DYNTYPE_FUNC void vml_Tan(){ vml_Tan(*this); }
	//y = sh(x)
	DYNTYPE_FUNC void vml_Sinh(CVector_Compact&y) const
	{ ASSERT(GetSize()==y.GetSize()); ::mkl::mkl_cpp::vmlSinh(y.GetSize(),*this,y); }
	DYNTYPE_FUNC void vml_Sinh(){ vml_Sinh(*this); }
	//y = ch(x)
	DYNTYPE_FUNC void vml_Cosh(CVector_Compact&y) const
	{ ASSERT(GetSize()==y.GetSize()); ::mkl::mkl_cpp::vmlCosh(y.GetSize(),*this,y); }
	DYNTYPE_FUNC void vml_Cosh(){ vml_Cosh(*this); }
	//y = th(x)
	DYNTYPE_FUNC void vml_Tanh(CVector_Compact&y) const
	{ ASSERT(GetSize()==y.GetSize()); ::mkl::mkl_cpp::vmlTanh(y.GetSize(),*this,y); }
	DYNTYPE_FUNC void vml_Tanh(){ vml_Tanh(*this); }
	//y = arcsin(x)
	DYNTYPE_FUNC void vml_ArcSin(CVector_Compact&y) const
	{ ASSERT(GetSize()==y.GetSize()); ::mkl::mkl_cpp::vmlAsin(y.GetSize(),*this,y); }
	DYNTYPE_FUNC void vml_ArcSin(){ vml_ArcSin(*this); }
	//y = arccos(x)
	DYNTYPE_FUNC void vml_ArcCos(CVector_Compact&y) const
	{ ASSERT(GetSize()==y.GetSize()); ::mkl::mkl_cpp::vmlAcos(y.GetSize(),*this,y); }
	DYNTYPE_FUNC void vml_ArcCos(){ vml_ArcCos(*this); }
	//y = arctan(x)
	DYNTYPE_FUNC void vml_ArcTan(CVector_Compact&y) const
	{ ASSERT(GetSize()==y.GetSize()); ::mkl::mkl_cpp::vmlAtan(y.GetSize(),*this,y); }
	DYNTYPE_FUNC void vml_ArcTan(){ vml_ArcTan(*this); }
	//y = arcsh(x)
	DYNTYPE_FUNC void vml_ArcSinh(CVector_Compact&y) const
	{ ASSERT(GetSize()==y.GetSize()); ::mkl::mkl_cpp::vmlAsinh(y.GetSize(),*this,y); }
	DYNTYPE_FUNC void vml_ArcSinh(){ vml_ArcSinh(*this); }
	//y = arcch(x)
	DYNTYPE_FUNC void vml_ArcCosh(CVector_Compact&y) const
	{ ASSERT(GetSize()==y.GetSize()); ::mkl::mkl_cpp::vmlAcosh(y.GetSize(),*this,y); }
	DYNTYPE_FUNC void vml_ArcCosh(){ vml_ArcCosh(*this); }
	//y = arcth(x)
	DYNTYPE_FUNC void vml_ArcTanh(CVector_Compact&y) const
	{ ASSERT(GetSize()==y.GetSize()); ::mkl::mkl_cpp::vmlAtanh(y.GetSize(),*this,y); }
	DYNTYPE_FUNC void vml_ArcTanh(){ vml_ArcTanh(*this); }
	// y = x^b
	DYNTYPE_FUNC void vml_Power(const CVector_Compact&b,CVector_Compact&y) const
	{ ASSERT(GetSize()==y.GetSize()); ASSERT(GetSize()==b.GetSize()); ::mkl::mkl_cpp::vmlPow(y.GetSize(),*this,b,y); }
	DYNTYPE_FUNC void vml_Power(t_Val b,CVector_Compact&y) const
	{ ASSERT(GetSize()==y.GetSize()); ::mkl::mkl_cpp::vmlPowx(y.GetSize(),*this,b,y); }
	DYNTYPE_FUNC void vml_Power(const CVector_Compact&b){ Power(b,*this); }
	DYNTYPE_FUNC void vml_Power(t_Val b){ vml_Power(b,*this); }
	// a = sin(x) b=cos(x)
	DYNTYPE_FUNC void vml_SinCos(CVector_Compact&a,CVector_Compact&b) const
	{ ASSERT(GetSize() == a.GetSize()); ASSERT(GetSize() == b.GetSize()); ::mkl::mkl_cpp::vmlSinCos(GetSize(),*this,a,b); }
	// y = arctan(x/b)
	DYNTYPE_FUNC void vml_ArctanDivision(const CVector_Compact&b,CVector_Compact&y) const
	{ ASSERT(GetSize()==y.GetSize()); ASSERT(GetSize()==b.GetSize()); ::mkl::mkl_cpp::vmlAtan2(y.GetSize(),*this,b,y); }
	// x = x^2
	DYNTYPE_FUNC void vml_Square()
	{ t_Val* p = *this; t_Val* end = &p[GetSize()]; for(;p<end;p++)*p = ::rt::Sqr(*p); }

};

} // namespace _meta_

DYNTYPE_FUNC void vml_LowAccuracy(){ vmlSetMode( VML_LA ); }
DYNTYPE_FUNC void vml_HighAccuracy(){ vmlSetMode( VML_HA ); }
DYNTYPE_FUNC void vml_OptimizeForFloat(){ vmlSetMode(VML_FLOAT_CONSISTENT); }
DYNTYPE_FUNC void vml_OptimizeForDouble(){ vmlSetMode(VML_DOUBLE_CONSISTENT); }



namespace _meta_
{

//////////////////////////////////////////////////////////////////////
//Vector reference type for all linear vectors
template<typename t_Val>
class _BaseStrideVecRefRoot
{
protected:
	DYNTYPE_FUNC _BaseStrideVecRefRoot(const _BaseStrideVecRefRoot &x){ ASSERT_STATIC(0); }
	DYNTYPE_FUNC _BaseStrideVecRefRoot(){}
public:
	DYNTYPE_FUNC BOOL SetSize(UINT co=0){ return (co==_len)?TRUE:FALSE; }
	DYNTYPE_FUNC BOOL ChangeSize(UINT new_size){ if(_len >= new_size){ _len=new_size; return TRUE;} return FALSE; }
	static const bool IsRef = true;
	typedef UINT t_SizeType;
	//TypeTraits
	TYPETRAITS_DECL_LENGTH(TYPETRAITS_SIZE_UNKNOWN)	
	TYPETRAITS_DECL_IS_AGGREGATE(false)

	template<typename T>
	DYNTYPE_FUNC t_Val & At(const T index){ return _p[(int)index*_inc]; }
	template<typename T>
	DYNTYPE_FUNC const t_Val & At(const T index)const { return _p[(int)index*_inc]; }
	DYNTYPE_FUNC UINT GetSize() const{ return _len; }
	static const bool IsCompactLinear = false;
protected:
	t_Val*	_p;
	UINT	_len;
	UINT	_inc;
};
class _BaseStrideVecRef
{public:	template<typename t_Ele> class SpecifyItemType
			{public: typedef ::mkl::_meta_::_BaseStrideVecRefRoot<t_Ele> t_BaseVec; };
			static const bool IsCompactLinear = false;
};
}// namespace _meta_

#define MKL_VEC_ASSIGN_OPERATOR(clsname)																\
		ASSIGN_OPERATOR_VEC(clsname)																	\
		template<typename t_BaseVec2>																	\
		DYNTYPE_FUNC const clsname& operator = (const ::mkl::_meta_::CVector_Base<t_Val,t_BaseVec2>& x)	\
		{ ASSERT(GetSize()==x.GetSize()); CopyFrom(x); return *this; }									\
										
namespace _meta_
{

template<typename t_Val, class t_BaseVector>
class CVector_Base:public t_BaseVector
{
protected:
	static const bool IsItemTypePlanar = t_BaseVector::IsItemTypePlanar;

	typedef typename t_BaseVector::FundamentalItemType FundamentalItemType;
	typedef typename t_BaseVector::TypeTraits_Item TypeTraits_Item;
public:
	typedef t_Val ItemType;
	typedef t_Val* LPItemType;
	typedef const t_Val* LPCItemType;

public:
	DYNTYPE_FUNC operator LPItemType(){ return &At(0); }
	DYNTYPE_FUNC operator LPCItemType()const { return &At(0); }
	DYNTYPE_FUNC operator LPVOID(){ return &At(0); }
	DYNTYPE_FUNC operator LPCVOID()const { return &At(0); }
	
	DYNTYPE_FUNC LPCItemType Begin()const { return &At(0); }
	DYNTYPE_FUNC LPItemType  Begin(){ return &At(0); }
	DYNTYPE_FUNC LPCItemType End()const { return &At(GetSize()); }
	DYNTYPE_FUNC LPItemType  End(){ return &At(GetSize()); }

	DYNTYPE_FUNC UINT		GetStride() const{ return _inc; }

#pragma warning(disable:4244)
	MKL_VEC_ASSIGN_OPERATOR(CVector_Base)
#pragma warning(default:4244)
	DEFAULT_TYPENAME(CVector_Base)

public:
	// BLAS Level 1
	// |P1|+|P2|+.....+|Pn|
	DYNTYPE_FUNC t_Val SumAbs() const{ return mkl_asum(_len,_p,_inc); }
	// Pi += scale*Xi
	template<class t_BaseVec2>
	DYNTYPE_FUNC void  Add_Scaled(t_Val scale,const CVector_Base<t_Val,t_BaseVec2> &x)
	{ ASSERT(GetSize() == x.GetSize());	::mkl::mkl_cpp::mkl_axpy(_len,scale,x.Begin(),x.GetStride(),_p,_inc); }
	// P1*X1+P2*X2+...Pn*Xn
	template<class t_BaseVec2>
	DYNTYPE_FUNC t_Val Dot(const CVector_Base<t_Val,t_BaseVec2>& x) const
	{ ASSERT(GetSize() == x.GetSize()); return ::mkl::mkl_cpp::mkl_dot(_len,x.Begin(),x.GetStride(),_p,_inc); }
	// P1*P1+P2*P2+...Pn*Pn
	DYNTYPE_FUNC t_Val L2Norm_Sqr() const{ return ::mkl::mkl_cpp::mkl_dot(_len,_p,_inc,_p,_inc); }
	// || P ||
	DYNTYPE_FUNC t_Val L2Norm() const{ return ::mkl::mkl_cpp::mkl_nrm2(_len,_p,_inc); }
	// let || P || = 1
	DYNTYPE_FUNC void Normalize(){ t_Val invl2Norm = 1/L2Norm(); *this *= invl2Norm; }
	// get the index of max/min ABS(value),the MKL library is 1-based
	DYNTYPE_FUNC int MaxAbsIndex() const{ return ::mkl::mkl_cpp::mkl_amax(_len,_p,_inc)-1; }
	DYNTYPE_FUNC int MinAbsIndex() const{ return ::mkl::mkl_cpp::mkl_amax(_len,_p,_inc)-1; }
	// get the value of max/min ABS(value),the MKL library is 1-based
	DYNTYPE_FUNC t_Val MinAbs() const{ return (*this)[MinAbsIndex()]; }
	DYNTYPE_FUNC t_Val MaxAbs() const{ return (*this)[MaxAbsIndex()]; }
	// swap content
	template<class t_BaseVec2>
	DYNTYPE_FUNC void Exchange(CVector_Base<t_Val,t_BaseVec2>& x)
	{ ASSERT(GetSize()==x.GetSize()); ::mkl::mkl_cpp::mkl_swap(_len,_p,_inc,x.Begin(),x._len); }
	// disturb values
	DYNTYPE_FUNC void Disturb(t_Val power = EPSILON)
	{ for(UINT i=0;i<GetSize();i++)_p[i]+=power*(::rand()-(RAND_MAX/2))/((t_Val)(RAND_MAX/2)); }


	//assignment
	template<class t_BaseVec2>
	DYNTYPE_FUNC void CopyTo(CVector_Base<t_Val,t_BaseVec2>& x) const
	{ ASSERT(x.GetSize() == GetSize()); ::mkl::mkl_cpp::mkl_copy(_len,_p,_inc,x._p,x._inc); }
	DYNTYPE_FUNC void CopyTo(t_Val* p) const
	{ ASSERT_ARRAY(p,GetSize()); ::mkl::mkl_cpp::mkl_copy(_len,_p,_inc,p,1); }

	template<class t_BaseVec2>
	DYNTYPE_FUNC void CopyFrom(const CVector_Base<t_Val,t_BaseVec2>& x)
	{ ASSERT(x.GetSize() == GetSize()); ::mkl::mkl_cpp::mkl_copy(_len,&x.At(0),x.GetStride(),_p,_inc); }
	DYNTYPE_FUNC void CopyFrom(const t_Val* p)
	{ ASSERT_ARRAY(p,GetSize()); ::mkl::mkl_cpp::mkl_copy(_len,p,1,_p,_inc); }

	template<typename T, typename BaseVec2>
	DYNTYPE_FUNC void CopyFrom(const ::rt::_TypedVector<T,BaseVec2> & x)
	{	ASSERT(GetSize()==x.GetSize());
#pragma warning(disable:4244)
		typedef rt::IsTypeSame<T,ItemType> IsSame;
		__static_if(TypeTraits_Item::IsAggregate && IsSame::Result && t_BaseVector::IsCompactVector && BaseVec2::IsCompactLinear)
		{ memcpy(&At(0),&x.At(0),sizeof(ItemType)*GetSize()); return; }
		__static_if(!TypeTraits_Item::IsAggregate || !IsSame::Result || !t_BaseVector::IsCompactVector || !BaseVec2::IsCompactLinear)
		{ for(UINT i=0;i<GetSize();i++)At(i) = x[i]; return; }
#pragma warning(default:4244)
		ASSERT(0); //avoid both __static_if fails
	}

	//////////////////////////////////////////////////////////
	// overrided operators
	template<typename T>
	DYNTYPE_FUNC void operator *= (const T& a){ ::mkl::mkl_cpp::mkl_scal(_len,(t_Val)a,_p,_inc); }
};

} // namespace _meta_
} // namespace mkl


namespace mkl
{

template< typename t_Val >
class CVector_Ref:public ::mkl::_meta_::CVector_Base<t_Val,::rt::_TypedVector<t_Val,::mkl::_meta_::_BaseStrideVecRef> >
{
	ASSERT_STATIC(	rt::TypeTraits<t_Val>::Typeid == rt::_typeid_32f || 
					rt::TypeTraits<t_Val>::Typeid == rt::_typeid_64f	);
	template< typename t_Val >
	friend class CVector;
	template< typename t_Val >
	friend class CVector_RefCompact;
	template< typename t_Val, class t_BaseVec >
	friend class ::mkl::CMatrix;

protected:
	static const bool IsCompactVector = false;
	DYNTYPE_FUNC CVector_Ref(){ ASSERT(0); }

public:
	DYNTYPE_FUNC CVector_Ref(t_Val* ptr,UINT len,UINT inc){ _p=ptr; _len=len; _inc=inc; }
	DYNTYPE_FUNC CVector_Ref(const CVector_Ref& x){ ASSERT_STATIC(0); }	//The two ASSERTs fired when trying to construct 
	//a Ref from a Ref instance or an empty instance which is not allowed. Avoid trying to copy a Ref
	//instance, it is highly recommended to use Ref& instead  for local using and function arguments

public:
	template<class _BaseVec>
	DYNTYPE_FUNC CVector_Ref(::mkl::_meta_::CVector_Base<t_Val,_BaseVec> &x)
	{ _p=x; _len=x.GetSize(); _inc=x.GetStride(); }

public:
#pragma warning(disable:4244)
	MKL_VEC_ASSIGN_OPERATOR(CVector_Ref)
#pragma warning(default:4244)
	DEFAULT_TYPENAME(CVector_Ref)

	typedef CVector_Ref Ref;

	DYNTYPE_FUNC Ref GetRef(){ return Ref(*this); }
	DYNTYPE_FUNC Ref GetRef(UINT x,UINT len){ return Ref(&_p[x*_inc],len,_inc); }

	DYNTYPE_FUNC const Ref GetRef() const
	{	return ::rt::_CastToNonconst(this)->GetRef(); }
	DYNTYPE_FUNC const Ref GetRef(UINT x,UINT len) const
	{	return ::rt::_CastToNonconst(this)->GetRef(x,len); }

	DYNTYPE_FUNC operator Ref& ()
	{ return *((Ref*)this); }
	DYNTYPE_FUNC operator const Ref& () const 
	{ return ::rt::_CastToNonconst(this)->operator Ref& (); }
};


template< typename t_Val >
class CVector_RefCompact:public ::mkl::_meta_::CVector_Base<t_Val,::mkl::_meta_::CVector_Compact<t_Val,::rt::_BaseVecRef<UINT>> >
{
	ASSERT_STATIC(	rt::TypeTraits< typename ::rt::Remove_Qualifer<t_Val>::t_Result >::Typeid == rt::_typeid_32f || 
					rt::TypeTraits< typename ::rt::Remove_Qualifer<t_Val>::t_Result >::Typeid == rt::_typeid_64f	);

	template< typename t_Val >
	friend class CVector;
	template< typename t_Val, class t_BaseVec >
	friend class ::mkl::CMatrix;

protected:
	static const bool IsCompactVector = true;
	DYNTYPE_FUNC CVector_RefCompact(){}
	
public:
	static const bool IsCompactLinear = true;
	DYNTYPE_FUNC CVector_RefCompact(t_Val* ptr,UINT len,UINT reserved_must_be_one=1){ _p=ptr; _len=len; ASSERT(reserved_must_be_one==1); }
	DYNTYPE_FUNC CVector_RefCompact(const CVector_RefCompact& x){ ASSERT_STATIC(0); }	//The two ASSERTs fired when trying to construct 
	//a Ref from a Ref instance or an empty instance which is not allowed. Avoid trying to copy a Ref
	//instance, it is highly recommended to use Ref& instead  for local using and function arguments
public:
	template<class _BaseVec>
	DYNTYPE_FUNC CVector_RefCompact(::mkl::_meta_::CVector_Base<t_Val,_BaseVec> &x){ SetRef(x); }

public:
#pragma warning(disable:4244)
	MKL_VEC_ASSIGN_OPERATOR(CVector_RefCompact)
#pragma warning(default:4244)
	DEFAULT_TYPENAME(CVector_RefCompact)

	typedef CVector_RefCompact Ref;

	DYNTYPE_FUNC Ref GetRef(){ return Ref(*this); }
	DYNTYPE_FUNC Ref GetRef(UINT x,UINT len){ return Ref(&At(x),len,_inc); }

	DYNTYPE_FUNC const Ref GetRef() const
	{	return ::rt::_CastToNonconst(this)->GetRef(); }
	DYNTYPE_FUNC const Ref GetRef(UINT x,UINT len) const
	{	return ::rt::_CastToNonconst(this)->GetRef(x,len); }

	DYNTYPE_FUNC CVector_Ref<t_Val>	GetRef(UINT x, UINT len, UINT inc)
	{ return CVector_Ref<t_Val>(&At(x),len,inc*_inc); }
	DYNTYPE_FUNC const CVector_Ref<t_Val> GetRef(UINT x, UINT len, UINT inc) const
	{ return ::rt::_CastToNonconst(this)->GetRef(x,len,inc); }

	DYNTYPE_FUNC operator CVector_Ref<t_Val>& ()
	{ return *((CVector_Ref<t_Val>*)this); }
	DYNTYPE_FUNC operator const CVector_Ref<t_Val>& () const 
	{ return ::rt::_CastToNonconst(this)->operator CVector_Ref<t_Val>& (); }

	template<class _BaseVec>
	void SetRef(::mkl::_meta_::CVector_Base<t_Val,_BaseVec> &x)
	{ _p=x; _len=x.GetSize(); ASSERT(x.GetStride()==1); }
};

template< typename t_Val>
class CVector:public ::mkl::_meta_::CVector_Base<t_Val,::mkl::_meta_::CVector_Compact<t_Val> >
{
public:
	TYPETRAITS_DECL_EXTENDTYPEID((rt::_typeid_codelib<<16)+10)
	COMMON_CONSTRUCTOR_VEC(CVector)
#pragma warning(disable:4244)
	MKL_VEC_ASSIGN_OPERATOR(CVector)
#pragma warning(default:4244)
	DEFAULT_TYPENAME(CVector)

public:
	typedef CVector_RefCompact<t_Val> Ref;

	DYNTYPE_FUNC Ref GetRef(){ return Ref(*this); }
	DYNTYPE_FUNC Ref GetRef(UINT x,UINT len){ return Ref(&At(x),len,_inc); }

	DYNTYPE_FUNC const Ref GetRef() const
	{	return ::rt::_CastToNonconst(this)->GetRef(); }
	DYNTYPE_FUNC const Ref GetRef(UINT x,UINT len) const
	{	return ::rt::_CastToNonconst(this)->GetRef(x,len); }

	DYNTYPE_FUNC operator Ref& ()
	{ return *((Ref*)this); }
	DYNTYPE_FUNC operator const Ref& () const 
	{ return ::rt::_CastToNonconst(this)->operator Ref& (); }

	DYNTYPE_FUNC CVector_Ref<t_Val>	GetRef(UINT x, UINT len, UINT inc)
	{ return CVector_Ref<t_Val>(&At(x),len,inc*_inc); }
	DYNTYPE_FUNC const CVector_Ref<t_Val> GetRef(UINT x, UINT len, UINT inc) const
	{ return ::rt::_CastToNonconst(this)->GetRef(x,len,inc); }

	DYNTYPE_FUNC operator CVector_Ref<t_Val>& ()
	{ return *((CVector_Ref<t_Val>*)this); }
	DYNTYPE_FUNC operator const CVector_Ref<t_Val>& () const 
	{ return ::rt::_CastToNonconst(this)->operator CVector_Ref<t_Val>& (); }
};

typedef CVector<float>		CVector32;
typedef CVector<double>		CVector64;

typedef CVector_Ref<float>	CVector32_Ref;
typedef CVector_Ref<double>	CVector64_Ref;

typedef CVector_RefCompact<float>	CVector32_RefCompact;
typedef CVector_RefCompact<double>	CVector64_RefCompact;




} // namespace mkl




