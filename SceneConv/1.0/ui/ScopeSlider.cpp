#include "stdafx.h"
#include "scopeslider.h"

using namespace num;
using namespace Gdiplus;

///////////////////////////////////
// CSlider
ui::CScopeSlider::CScopeSlider(_meta_::_GdiplusControlHost* pHost)
	:_meta_::_GdiplusControlRoot(pHost)
	,m_ScopeSolidBrush(Color(120,80,80,180))
	,m_LabelPen(Color(180,40,40,80),2.0f)
{	m_TotalLength = 100;
	m_ScopeRegion = 35;
	m_CurPos = 0;
	m_bhorizontal = TRUE;

	m_PosUnit = 1;
	m_pxBarStart = 0;
	m_pxBarWidth = 10;

	m_Style = STYLE_MOVEABLE|STYLE_RESIZEABLE|STYLE_HAS_LABEL|STYLE_AUTO_EXTEND;
	
	m_ScopeSize_Min = EPSILON;
	m_ScopeSize_Max = FLT_MAX;

	m_hHand = ::LoadCursor(NULL,IDC_HAND);
	m_hResizeV = ::LoadCursor(NULL,IDC_SIZENS);
	m_hResizeH = ::LoadCursor(NULL,IDC_SIZEWE);
}

void ui::CScopeSlider::UpdateLayout()
{
	m_ScopeRegion = min(m_ScopeSize_Max,max(m_ScopeSize_Min,m_ScopeRegion));

	if(m_Style&STYLE_AUTO_EXTEND)
	{	m_CurPos = max(0,m_CurPos);
		if(m_TotalLength < m_ScopeRegion+m_CurPos)
		{	m_TotalLength = m_ScopeRegion+m_CurPos;
			SendNotify(NOTIFY_LENGTH_EXTENDED);
		}
	}
	else
	{	m_ScopeRegion = max(2, min(m_ScopeRegion,m_TotalLength));
		m_CurPos = max(0,min(m_TotalLength-m_ScopeRegion,m_CurPos));
	}

	RECT layout;
	m_pControlHost->GetRedrawRect(&layout);
	if(m_bhorizontal)
	{	m_ScaleToPx = (layout.right - layout.left -6)/m_TotalLength;
		m_pxBarStart = (float)(layout.left+3+m_CurPos*m_ScaleToPx);
	}
	else
	{	m_ScaleToPx = (layout.bottom - layout.top -6)/m_TotalLength;
		m_pxBarStart = (float)(layout.top+3+m_CurPos*m_ScaleToPx);
	}

	m_pxBarWidth = (float)(m_ScopeRegion*m_ScaleToPx);
	if(m_pxBarWidth < 7)
	{	m_pxBarStart -= 7-m_pxBarWidth;
		m_pxBarWidth = 7;
	}

	RedrawContent();
}

void ui::CScopeSlider::RedrawContent()
{
	if(!IsUILocked()){}else{ return; }

#pragma warning(disable:4244)
	RECT layout;
	m_pControlHost->GetRedrawRect(&layout);

	if(!m_bhorizontal)
	{	::rt::Swap(layout.top,layout.left);
		::rt::Swap(layout.bottom,layout.right);
	}

	Graphics* pGfx = m_pControlHost->GetGraphics();
	if( pGfx )
	{
		Graphics& gfx(*pGfx);
		gfx.SetSmoothingMode( Gdiplus::SmoothingModeDefault );

		if(!m_bhorizontal)
		{
			gfx.SetTransform(&Gdiplus::Matrix(0,1,1,0,0,0));
			RectF rc;
			gfx.GetClipBounds(&rc);
			::rt::Swap(rc.X,rc.Y);
			::rt::Swap(rc.Width,rc.Height);
			gfx.SetClip(rc);
		}

		RedrawBackground(gfx,layout,FALSE);

		static const float bar_h_margin = 3.0f;

		UINT w = layout.right - layout.left -4;
		UINT h = layout.bottom - layout.top -2*bar_h_margin;
		{	float mid = (layout.bottom + layout.top)*0.5f;
			gfx.DrawLine(&m_GridPen,(int)(layout.left+0.5f),	(int)(mid+0.5f),
									(int)(layout.right+0.5f),	(int)(mid+0.5f));

			if(m_Style&STYLE_HAS_LABEL)
			{	for(UINT i=0;i<m_Labels.GetSize();i++)
				{	// draw label
					float x_mid = m_Labels[i]*m_ScaleToPx;
					gfx.DrawLine(&m_LabelPen,(int)(x_mid+3.5f),(int)(mid-2.5f),
											 (int)(x_mid+3.5f),(int)(mid+3.5f));
				}
			}
		}

		if(m_Style&STYLE_RESIZEABLE)
		{	gfx.DrawLine(&m_BorderPen,	(int)(m_pxBarStart+1.5f),(int)(layout.top+bar_h_margin+0.5f),
										(int)(m_pxBarStart+1.5f),(int)(h-1+layout.top+bar_h_margin+2.5f) );
			gfx.DrawLine(&m_BorderPen,	(int)(m_pxBarStart+m_pxBarWidth-1.5f),(int)(layout.top+bar_h_margin+0.5f),
										(int)(m_pxBarStart+m_pxBarWidth-1.5f),(int)(h-1+layout.top+bar_h_margin+2.5f));
		}
		gfx.FillRectangle(&m_ScopeSolidBrush,	(int)(m_pxBarStart+0.5f),(int)(layout.top+bar_h_margin+0.5f),
												(int)(m_pxBarWidth+0.5f),(int)(h+1.5f) );

		{	Vec4<Vec2f>	pts(	Vec2f((int)(layout.left+5.5f),(int)(layout.top)),	
								Vec2f((int)(layout.left+0.5f),(int)(layout.top)), 
								Vec2f((int)(layout.left+0.5f),(int)(layout.bottom+0.5f)),	
								Vec2f((int)(layout.left+5.5f),(int)(layout.bottom+0.5f)) );
			gfx.DrawLines(&m_BorderPen, (PointF*)&pts, 4);
		}

		{	Vec4<Vec2f>	pts(	Vec2f((int)(layout.right-4.5f),(int)(layout.top)),	
								Vec2f((int)(layout.right+0.5f),(int)(layout.top)), 
								Vec2f((int)(layout.right+0.5f),(int)(layout.bottom+0.5f)), 
								Vec2f((int)(layout.right-4.5f),(int)(layout.bottom+0.5f)) );
			gfx.DrawLines(&m_BorderPen, (PointF*)&pts, 4);
		}

		gfx.Flush(FlushIntentionSync);
		SendNotify(NOTIFY_GRAPHICS_UPDATED);
	}

#pragma warning(default:4244)
}

LRESULT ui::CScopeSlider::MessageProc(UINT uMsg, WPARAM wParam, LPARAM lParam)
{	static BOOL _BarDruging = FALSE;
	static POINT _DrugStart;
	static float _PosDrugFrom;

	static UINT  _resize_what = 0;
	static float _CurPos, _BarWidth;

	if(!IsUILocked()){}
	else{ return __super::MessageProc(uMsg,wParam,lParam); }

	switch(uMsg)
	{
	case WM_KEYDOWN:
		if(wParam>=VK_END && wParam<=VK_DOWN && (STYLE_MOVEABLE&m_Style))
		{	if(wParam == VK_HOME){ m_CurPos = 0; }
			else if(wParam == VK_END){ m_CurPos = m_TotalLength - m_ScopeRegion; }
			else if(m_bhorizontal)
			{	if(wParam == VK_LEFT){ m_CurPos -= m_PosUnit; }
				else if(wParam == VK_RIGHT){ m_CurPos += m_PosUnit; }
			}
			else
			{	if(wParam == VK_UP){ m_CurPos -= m_PosUnit; }
				else if(wParam == VK_DOWN){ m_CurPos += m_PosUnit; }
			}
			UpdateLayout();
			SendNotify(NOTIFY_POSITION_CHANGED);
		}
		
		break;
	case WM_SIZE:
		{	RECT rc;
			m_pControlHost->GetRedrawRect(&rc);
			m_bhorizontal = (rc.bottom-rc.top) < (rc.right-rc.left);
			UpdateLayout();
		}
		break;
	case WM_LBUTTONDBLCLK:
		if( (m_Style&(STYLE_MOVEABLE|STYLE_RESIZEABLE)) == (STYLE_MOVEABLE|STYLE_RESIZEABLE) )
		{	POINT point = { GET_X_LPARAM(lParam), GET_Y_LPARAM(lParam) };
			if( HitTestScope(point) )
			{	m_CurPos = 0;
				m_ScopeRegion = m_TotalLength;
				UpdateLayout();
				SendNotify(NOTIFY_SCOPE_RESIZED);
				SendNotify(NOTIFY_POSITION_CHANGED);
			}
		}
		break;
	case WM_LBUTTONDOWN:
		{	POINT point = { GET_X_LPARAM(lParam), GET_Y_LPARAM(lParam) };
			if( HitTestScope(point) )
			{	_DrugStart = point;
				UINT id = HitScopeSizingHander(point);
				m_pControlHost->SetMouseCapture();
				if(id==0)
				{	if(STYLE_MOVEABLE&m_Style)
					{
						_BarDruging = TRUE;
						_PosDrugFrom = m_CurPos;
					}
				}
				else
				{	if(STYLE_RESIZEABLE&m_Style)
					{
						_resize_what = id;
						_CurPos = m_CurPos;
						_BarWidth = m_ScopeRegion;
					}
					else if(STYLE_MOVEABLE&m_Style)
					{
						_BarDruging = TRUE;
						_PosDrugFrom = m_CurPos;
					}
				}
			}
			else
			{	_BarDruging = FALSE;
				if(STYLE_MOVEABLE&m_Style)
				{
					RECT layout;
					m_pControlHost->GetRedrawRect(&layout);
					if(m_bhorizontal)
						m_CurPos = (point.x-layout.left)/m_ScaleToPx - m_ScopeRegion*0.5f;
					else
						m_CurPos = (point.y-layout.top)/m_ScaleToPx - m_ScopeRegion*0.5f;
					UpdateLayout();
					SendNotify(NOTIFY_POSITION_CHANGED);
				}
			}
		}
		break;
	case WM_MOUSEWHEEL:
		if(wParam & MK_CONTROL)
		{	if(STYLE_RESIZEABLE&m_Style)
			{	POINT point = { GET_X_LPARAM(lParam), GET_Y_LPARAM(lParam) };
				int zDelta = ((int)wParam)>>16;
				float scale = (wParam&MK_SHIFT)?0.001f:0.05f;
				float offset;
				if( zDelta>0 )
					offset = m_ScopeRegion*scale;
				else
					offset = -m_ScopeRegion*scale;
				if( m_ScopeRegion + offset > 2)
				{	m_ScopeRegion += offset;
					m_CurPos -= offset*0.5f;
					UpdateLayout();
					SendNotify(NOTIFY_SCOPE_RESIZED);
					SendNotify(NOTIFY_POSITION_CHANGED);
				}
				return 0xffff;
			}
		}
		else
		{	if(m_Style&STYLE_MOVEABLE)
			{	POINT point = { GET_X_LPARAM(lParam), GET_Y_LPARAM(lParam) };
				int zDelta = ((int)wParam)>>16;
				float scale = (wParam&MK_SHIFT)?0.005f:0.1f;
				if( zDelta>0 )
					m_CurPos -= m_ScopeRegion*scale;
				else
					m_CurPos += m_ScopeRegion*scale;
				UpdateLayout();
				SendNotify(NOTIFY_POSITION_CHANGED);
				return 0xffff;
			}
		}
		break;
	case WM_MOUSEMOVE:
		{	POINT point = { GET_X_LPARAM(lParam), GET_Y_LPARAM(lParam) };

			if(	_BarDruging )
			{	if(m_bhorizontal)
					m_CurPos = (point.x - _DrugStart.x)/m_ScaleToPx + _PosDrugFrom;
				else
					m_CurPos = (point.y - _DrugStart.y)/m_ScaleToPx + _PosDrugFrom;
				UpdateLayout();
				SendNotify(NOTIFY_POSITION_CHANGED);
			}
			else if( _resize_what )
			{	float offset;
				if(m_bhorizontal)
					offset = (point.x - _DrugStart.x)/m_ScaleToPx;
				else
					offset = (point.y - _DrugStart.y)/m_ScaleToPx;

				if( _resize_what == 1)
				{	m_CurPos = _CurPos + offset;
					m_ScopeRegion = _BarWidth - offset;
				}
				else
				{	ASSERT( _resize_what == 2 );
					m_ScopeRegion = _BarWidth + offset;
					if(m_ScopeRegion<2)
					{	m_CurPos = _CurPos-2+m_ScopeRegion;
						m_ScopeRegion = 2;
					}
				}
				UpdateLayout();
				SendNotify(NOTIFY_SCOPE_RESIZED);
				SendNotify(NOTIFY_POSITION_CHANGED);
			}
			else if(HitTestScope(point))
			{	UINT id = (STYLE_RESIZEABLE&m_Style)?HitScopeSizingHander(point):0;
				if(!id){ ::SetCursor(m_hHand); }
				else{ ::SetCursor(m_bhorizontal?m_hResizeH:m_hResizeV); }
			}else { ::SetCursor(m_hArrow); }
		}
		break;
	case WM_LBUTTONUP:
		_BarDruging = FALSE;
		_resize_what = 0;
		m_pControlHost->ReleaseMouseCapture();
		break;
	case WM_SETCURSOR:
		return 1;
		break;
	}

	return __super::MessageProc(uMsg,wParam,lParam);
}

BOOL ui::CScopeSlider::HitTestScope(const POINT& point)
{	
	RECT layout;
	m_pControlHost->GetRedrawRect(&layout);

	if(m_bhorizontal)
		return	(	point.y > layout.top + 3 && point.y < layout.bottom - 3 &&
					point.x > m_pxBarStart && point.x < m_pxBarStart+m_pxBarWidth);
	else
		return	(	point.x > layout.left + 3 && point.x < layout.right - 3 &&
					point.y > m_pxBarStart && point.y < m_pxBarStart+m_pxBarWidth);
}

UINT ui::CScopeSlider::HitScopeSizingHander(const POINT& point)
{	ASSERT(HitTestScope(point));
	if(m_bhorizontal)
	{	if( point.x > m_pxBarStart + 5 && point.x < m_pxBarStart+m_pxBarWidth - 5 )
			return 0;
		else if( point.x <= m_pxBarStart + 5 )
			return 1;
		else
			return 2;
	}
	else
	{	if( point.y > m_pxBarStart + 5 && point.y < m_pxBarStart+m_pxBarWidth - 5 )
			return 0;
		else if( point.y <= m_pxBarStart + 5 )
			return 1;
		else
			return 2;
	}
}

