#pragma once

//////////////////////////////////////////////////////////////////////
// Visual Computing Foundation Classes (VCFC)
//					Jiaping Wang  2006.2
//					e_boris2002@hotmail.com
//
// (C) Copyright Jiaping Wang 2004.
//
// Permission to copy, use, modify, sell and distribute this software
// is granted provided this copyright notice appears in all copies.
// This software is provided "as is" without express or implied
// warranty, and with no claim as to its suitability for any purpose.
//
// Absolutly no garanty!!
//////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////
//  Brief Description
//
//  TimelineSlider.h
//
//  TimelineSlider: UI for tuning keyframes in timeline
//
//////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////
// Revise History
//
// Inital version	2006.11.10		Jiaping
//
//////////////////////////////////////////////////////////////////////

#include "ui_base.h"
#include "..\num\small_vec.h"
#include "scopeslider.h"

namespace ui
{

class CTimelineSlider : public _meta_::_GdiplusControlRoot,
						public _meta_::_ComposedControlHost  //composed control
{
protected:
	enum{ SCROLL_HEIGHT = 20 };
	enum{ MINI_TICK_HEIGHT = 3 };
	enum{ TIME_UINT = 10 };  //the unit for m_VisableRegionSlider
	enum{ TOOL_TIP_WIDTH = 12 };
	enum{ CURTM_TIP_HEIGHT = 12 };
	enum{ CURTM_TIP_BUTTON_COUNT = 1 };
	enum{ KEYFRAME_STAR_SIZE = 3 };
public:
	enum{ IDD = 1565 };
	enum{ NOTIFY_CURRENT_CHANGED = NOTIFY_CUSTOM_BASE + 1 ,
		  NOTIFY_NEW_KEYFRAME,		// wParam = index of the new key after new key is created
		  // For NOTIFY_XXXXXING_KEYFRAME messages, the action is taken after Nofify is returned with TRUE. otherwise nothing happens
		  NOTIFY_CREATING_KEYFRAME,	// wParam = 0 lParam = the time that the key is creating at (float&) 
		  NOTIFY_MOVING_KEYFRAME,	// wParam = index of the key to be moved, lParam =  the new time (float&)
		  NOTIFY_DELETING_KEYFRAME,	// wParam = index of the key to be deleted
          NOTIFY_SELECTED_ITEM, // wParam = index of the key to be selected (// 0xf00x = tooltip, 0xfffe = current time, 0xffff=hide or [0,key_count])
		  NOTIFY_MAX					
		};
	enum{ STYLE_KEYFRAME_EDITABLE = 1
		};

private:
	static const BYTE _Current_time_Tip_Image_Data[TOOL_TIP_WIDTH*CURTM_TIP_HEIGHT*4];

protected:
	CControlComposition<CScopeSlider>		m_VisableRegionSlider;

	// override from _ComposedControlHost
	BOOL Composed_Notify(UINT composed_id, UINT msg, WPARAM wParam, LPARAM lParam);
	void Composed_GetRedrawRect(UINT id, LPRECT lpRect);

protected:
	float					m_TickInterval;
	float					m_MiniTickWidth_px;
	UINT					m_TickCount;	// in the scope
	DWORD					m_TimeLevel;	//0:ms 1:sec 2:min 3:hr
	float					m_TotalTime;
	float					m_VisableTimeScope;
	float					m_ScaleToPixel;
	num::Vec2f				m_TickTextSize;
	UINT					m_DrugWhat;

	// interaction
	float					m_CurTime;
	HCURSOR					m_hResizeH;

	// tool tip
	UINT					m_Tooltip_binding; // 0xfffe = current time, 0xffff=hide or [0,key_count]
	CachedImage				m_CurTime_tip_Image;   // TOOL_TIP_WIDTH x (TOOL_TIP_WIDTH x 3)
	num::Vec2i				m_Tooltip_Pos_px;

protected:
	class _Keyframe
	{	friend class CTimelineSlider;
		Gdiplus::CachedBitmap *	m_pIcon;
		num::Vec2u				m_IconSize;
	public:	
		WPARAM					m_UserData;
		float					m_Time;
		_Keyframe(){ m_pIcon=NULL; m_UserData=0; }
		~_Keyframe(){ _SafeDel(m_pIcon); }
		void SetIcon(UINT w,UINT h,UINT step,LPBYTE * pBGR); // a buffer of 40x32x3=3840
	};

	void					RedrawContent();
	void					ToolTipButtonClicked(UINT id);
	void					UpdateLayout();
	void					UpdateKeyframes();
	virtual LRESULT			MessageProc(UINT uMsg, WPARAM wParam, LPARAM lParam);
	void					PrintTime(LPWSTR buf, float time);
	_Keyframe*				h2ptr(HANDLE handle)
	{
#pragma warning(disable:4312)
		ASSERT_PTR((LPCVOID)handle,sizeof(_Keyframe)); return (_Keyframe*)handle; 
#pragma warning(default:4312)
	}
	UINT					HitTest(const POINT& pt);	// 0xf00x = tooltip, 0xfffe = current time, 0xffff=hide or [0,key_count]

protected:
	::rt::BufferEx<_Keyframe>	m_Keyframes;
	UINT					KeyframeUpperBound(float t);
	void					AddNewKey();
	void					DeleteKey(UINT index);
	UINT					MoveKey(UINT index, float time_to);

public:
	// ui
	Gdiplus::Pen			m_TickPen;
	Gdiplus::Pen			m_TickCurPen;
	Gdiplus::Pen			m_KeyframePen;
	Gdiplus::Pen			m_KeyframeHotPen;

public:
	CTimelineSlider(_meta_::_GdiplusControlHost* pHost);
	float					GetCurrentTime(){ return m_CurTime; }
	void					SetCurrentTime(float t);

	UINT					GetKeyframeCount()const{ return (UINT)m_Keyframes.GetSize(); }
	WPARAM					GetKeyframeData(UINT id)const{ return m_Keyframes[id].m_UserData; }
	float					GetKeyframeTime(UINT id)const{ return m_Keyframes[id].m_Time; }

	void					SetKeyframeData(UINT id, WPARAM data){ m_Keyframes[id].m_UserData = data; }
	float					GetLastKeyframeTime();
	float					GetFirstKeyframeTime();

	void					SetKeyframes(UINT key_count=0, const float* pTime=NULL,const WPARAM* pData = NULL);  // sorted by time
};


}
