#pragma once

/************************************************************\

Wrapper classes for Visualization and interactive based on GdiPlus
					by Jiaping Wang   2004.8
					e_boris2002@hotmail.com

version: 0.1

\************************************************************/

#include <gdiplus.h>
#pragma comment(lib,"gdiplus.lib")

#include <string>
#include <vector>
#include <math.h>
#include <atlimage.h>
#include "..\ui\sd_tree.h"

namespace UI
{

namespace plot
{

#define RGBA(r,g,b,a)	Gdiplus::Color::MakeARGB(a*255,r*255,g*255,b*255)
#define RGBA_BYTE(r,g,b,a)	((DWORD)( (a<<24) | (r<<16) | (g<<8) | b ))

typedef Gdiplus::ARGB Color4b;
typedef const float * LPCFloat;

class CPos
{
public:
	float x,y;
};

class CShapeConst
{
public:
	const CPos *	pVertexs;
	int		VertexNum;
};



class CSearchPointTree:public Gfx::CSpatialPartitionTree<2,float,CPos,false,12>
{
protected:
	LPCFloat	m_pPoints;

	void		UpdateTree(CSPTreeNode* node);
public:
	LPCFloat	SearchPointAddress(LPCFloat pPoint);

	void	BuildTree(LPCFloat pPointPack,int Count)
	{	m_pPoints = pPointPack;
		__super::BuildTree((CPos*)w32::CastToNonconst(pPointPack),Count);
		UpdateTree(GetRoot());
	}

	UINT	SearchPointId(LPCFloat pPoint)
	{ return (SearchPointAddress(pPoint) - m_pPoints)/2; }
};


#define MatrixStackLength	(8)

class CLabelStyle
{
public:
	int PointMode;	//Point_Pixel ... and so on
	Color4b Color;
};

class CGraphicsEx:public Gdiplus::Graphics
{
public:
	enum PointStyle
	{
		PS_Cycle = 1000,
		PS_Triangle,
		PS_Square,
		PS_Cross,
		PS_Prism,
		PS_TriangleLeft,
		PS_TriangleRight,
		PS_TriangleDown,
		PS_BiTriangle,
		PS_HalfGrid,
		PS_Max
	};

private:
	Gdiplus::Matrix _MatrixStack[MatrixStackLength];
	int				_StackTop;

protected:
	static	const CShapeConst* m_PointShapes;
	const CLabelStyle *	m_pPointStyleList;
	int				m_PointStyleCount;

protected:
	Gdiplus::Pen	m_PointPen;
	float	m_DatasetScaleX;
	float	m_DatasetScaleY;
	//float	m_DatasetScaleX_RCP;
	//float	m_DatasetScaleY_RCP;

	float	m_PointSize;

protected:
	Gdiplus::SolidBrush	m_TextBrush;

public:

	CGraphicsEx(HDC hDC);
	~CGraphicsEx();
	void	SetupLabelStyleList(const CLabelStyle * pStyles,int Count);
	void DrawPoints(int Count,const CPos* pPoints,CLabelStyle * style = NULL);
	void DrawPoints(int Count,const CPos* pPoints,const Color4b* pColor,PointStyle ps = PS_Cycle);
	void DrawPoints(int Count,const CPos* pPoints,const BYTE* pLabel,const Color4b* pColor);
	void DrawPoints(int Count,const CPos* pPoints,const BYTE* pLabel);

	void PushMatrix()
	{
		_StackTop++;
		ASSERT(_StackTop < MatrixStackLength); //overflow

		GetTransform(&_MatrixStack[_StackTop]);
	}

	void PopMatrix()
	{
		ASSERT(_StackTop >= 0);

		SetTransform(&_MatrixStack[_StackTop]);

		_StackTop--;
	}

	void SetDatasetScale(float width,float height)
	{
		m_DatasetScaleX = width/m_PointSize;
		m_DatasetScaleY = height/m_PointSize;
		//m_DatasetScaleX_RCP = 1.0f/m_DatasetScaleX;
		//m_DatasetScaleY_RCP = 1.0f/m_DatasetScaleY;
	}

	void SetPointSize(float size)
	{
		m_PointSize = size;
	}

	__forceinline void DrawPoint(PointStyle ps)
	{
		ASSERT(ps>=PS_Cycle && ps<=PS_Max);

		if( ps == PS_Cycle )
		{
			DrawEllipse(&m_PointPen,-0.4f,-0.4f,0.8f,0.8f);
		}
		else
		{
			ps = (plot::CGraphicsEx::PointStyle)((int)ps - (int)PS_Cycle - 1);
			DrawLines(&m_PointPen,(Gdiplus::PointF*)m_PointShapes[ps].pVertexs,m_PointShapes[ps].VertexNum);
		}
	}

	__forceinline void SetTextColor(Color4b col)
	{
		m_TextBrush.SetColor(Gdiplus::Color(col));
	}

	void	DrawText(LPCWSTR pString,Gdiplus::Font * pFont,float x,float y)
	{
		ASSERT(pString);
		ASSERT(pFont);

		DrawString(pString,-1,pFont,Gdiplus::PointF(x,y),&m_TextBrush);
	}

	void	DrawBoundText(LPCWSTR pString,Gdiplus::Font * pFont,float x,float y,float width,float height)
	{
		Gdiplus::StringFormat sf;
		ASSERT(pString);
		ASSERT(pFont);

		DrawString(pString,-1,pFont,Gdiplus::RectF(x,y,width,height),&sf,&m_TextBrush);
	}

	const CShapeConst * GetPointShape(PointStyle ps)const 
	{
		ASSERT(ps>PS_Cycle && ps<=PS_Max); 
		return &m_PointShapes[ps-(int)PS_Cycle-1]; 
	}

	float MeasureTextWidth(LPCWSTR p,Gdiplus::Font * pFont)
	{
		static const float org_zero[2] = {0,0};

		ASSERT(p);
		ASSERT(pFont);

		Gdiplus::RectF bounding;
		MeasureString(p,-1,pFont,*((const Gdiplus::PointF*)org_zero),&bounding);
		return bounding.Width;
	}

};



class CFigure
{
public:

	class CPointPair
	{
	public:
		int	v1,v2;
	};

protected:
	ATL::CImage	m_DatasetLayer;

public:
	//// Figure modes
	enum
	{
		Line_None,
		Line_Straight,
		Line_Curve,

		AA_None = Gdiplus::SmoothingModeNone,
		AA_Invalid = Gdiplus::SmoothingModeInvalid,
		AA_Default = Gdiplus::SmoothingModeDefault,
		AA_HighSpeed = Gdiplus::SmoothingModeHighSpeed,
		AA_HighQuality = Gdiplus::SmoothingModeHighQuality,
		//AA_AntiAlias8x4 = Gdiplus::SmoothingModeAntiAlias8x4,
		AA_AntiAlias = Gdiplus::SmoothingModeAntiAlias,
		//AA_AntiAlias8x8 = Gdiplus::SmoothingModeAntiAlias8x8,

		Edge_None,
		Edge_LinearNeighbor,
		Edge_Customized,

		//DO NOT CHANGE !!
		Frame_AxisVertical = 0x001,
		Frame_GridVertical = 0x002,
		Frame_TextVertical = 0x004,
		Frame_AxisHorizontal = 0x010,
		Frame_GridHorizontal = 0x020,
		Frame_TextHorizontal = 0x040,

		Point_Pixel = 999,
		Point_Cycle = 1000,
		Point_Triangle,
		Point_Rectangle,
		Point_Cross,
		Point_Prism,


		Font_Regular = Gdiplus::FontStyleRegular,
		Font_Bold = Gdiplus::FontStyleBold,
		Font_Italic = Gdiplus::FontStyleItalic,
		Font_BoldItalic = Gdiplus::FontStyleBoldItalic,
		Font_Underline = Gdiplus::FontStyleUnderline,
		Font_Strikeout = Gdiplus::FontStyleStrikeout

	};

	class CConfig
	{
	public:
		BOOL KeepAspect;

		int	LineMode;
		Color4b LineColor;
		float LineWidth;
		int AA_Mode;
		int EdgeMode;

		CLabelStyle	PointStyle;
		float PointSize;

		int	FrameMode;
		int	FrameGridInterval;	// in pixel
		int FrameLineWidth;
		Color4b FrameColor;
		Color4b FrameGridColor;
		Color4b FrameTextColor; 

	public:
		CConfig();
	};



protected:
	class CDrawTools
	{
		friend class CFigure;
	protected:
		Gdiplus::Font*	pAxisFont;
	public:
		Gdiplus::Pen	LinePen;
		
		CDrawTools();
		void	SetAxisFont(LPCTSTR Fontname,int size,int fontstyle = Font_Italic);
	};

protected:

	// Dataset pointers
	const CPos*		m_pPos;
	const BYTE*		m_pLabel;
	const Color4b*	m_pColor;
	UINT			m_PointCount;

	CLabelStyle *	m_pPointStyleList;
	int				m_PointStyleCount;

	const CPointPair*	m_pEdge;
	int				m_EdgeCount;

	// State for dataset
	BYTE	m_MaxLabelId;
	float	m_DataSetCenterX,m_DataSetCenterY;
	float	m_DataSetScaleX,m_DataSetScaleY;	// 1.0/bounding box


	//Layout
	int		m_AreaX,m_AreaY;
	int		m_AreaWidth,m_AreaHeight;
	Gdiplus::Matrix	m_Projection;
	Gdiplus::Matrix	m_ProjectionInv;


	//internal functions
	void	LoadInitialTransform(Gdiplus::Graphics & g);
	void	UpdateProjectionMatrix();
	
public:
	CConfig	m_Config;
	CDrawTools m_Tools;

public:
	virtual void	SetupDatasetPoint(const float * pos_2f,int Count,const BYTE* label_1b=NULL,const Color4b* color_4b=NULL); //point buffer is refered not copied, DO NOT release buffer
	void	SetupLabelStyleList(const CLabelStyle * pStyles,int Count);
	void	SetConnectivityPointer(int mode = Edge_LinearNeighbor, const CPointPair* pEdges=NULL,int Count=0); // or Edge_Customized
	
	CFigure();
	~CFigure();

	virtual void	SetupLayout(int x,int y,int width,int height);
	virtual void	RenderDatasetLayer();
	void	FinalSynthesis(HDC out);
	void			ResetContent();

	__forceinline void	Image2Dataset2f(float & x,float & y)
	{
		float v[2] = {x,y};
		Image2Dataset2fv(v);
		x = v[0];
		y = v[1];
	}
	__forceinline void	Dataset2Image2f(float & x,float & y)
	{
		float v[2] = {x,y};
		Dataset2Image2fv(v);
		x = v[0];
		y = v[1];
	}

	__forceinline void Image2Dataset2fv(float * p)
	{
		p[0] -= m_AreaX;
		p[1] -= m_AreaY;

		m_ProjectionInv.TransformPoints((Gdiplus::PointF*)p);
	}

	__forceinline void Dataset2Image2fv(float * p)
	{
		m_Projection.TransformPoints((Gdiplus::PointF*)p);

		p[0] += m_AreaX;
		p[1] += m_AreaY;
	}

	__forceinline int GetMaxLabelId()
	{
		return m_MaxLabelId;
	}
};

class CDragableUI
{
	friend class CInteractiveFigure;
protected:
	float	ui_x,ui_y;
	float	ui_Width;
	float	ui_Height;
};

class CPointTip:public CDragableUI
{
	friend class CInteractiveFigure;
protected:
	std::wstring	m_Text;
	ATL::CImage		m_Image;

	int		ImageWidth;
	int		ImageHeight;

	float	PtImgSpace[2];
	

	///////////////////////
	// UI position cache
	float		ui_ButtonSize;
	float		ui_ButtonY;
	float		ui_CloseButtonX;
	float		ui_SwitchButtonX;

public:
	DWORD	HidenFlag;
	UINT	LinkedPointId;

	CPointTip(UINT pid,int ix,int iy,int width = 32,int height = 32)
	{
		LinkedPointId = pid;
		ui_x = ix;
		ui_y = iy;
		ImageWidth = width;
		ImageHeight = height;

		HidenFlag = 0;
	}

	enum
	{
		Hiden_None = 0x0,
		Hiden_Image = 0x1,
		Hiden_Text = 0x2
	};

	void	SetText(LPCTSTR p);
	void	ClearText(){ m_Text.clear(); }
	void	SetImage(ATL::CImage & img);
	void	ClearImage(){ if(!m_Image.IsNull())m_Image.Destroy(); }
	BOOL	HasImage(){ return !m_Image.IsNull(); }
};

typedef CPointTip * LPPointTip;
typedef BOOL (*LPFUNC_RetrievePointDescriptionCallback)(CPointTip * lpTip,LPVOID Cookie);

class CInteractiveFigure:public CFigure
{
public:
	class CUiObjectRef
	{
	public:
		int		Type;			//one of UiObject_XXXXX
		int		ObjectIndex;	//index to object, point or PointTip
		CDragableUI*	pDragableObject;
	};

protected:
	ATL::CImage	m_InteractiveLayer;
	HDC		m_FinalOutputDC;
	HWND	m_BindedWindow;

	/////////////////////////////////////
	// interactive state
	int		is_PointFoucsId;
	float	is_PointFoucsX;
	float	is_PointFoucsY;

	BOOL	is_MouseDraging;

	CDragableUI*	is_pDragingObject;
	int				is_DragingOrgX;
	int				is_DragingOrgY;
	int				is_DragingCurX;
	int				is_DragingCurY;
	int				is_DragingCursorOffsetX;
	int				is_DragingCursorOffsetY;
	ATL::CImage		is_DragingObjectImage;

	//////////////////////////////////////
	// Draw Tools
	Gdiplus::Font * m_pPointTipFont;

	//////////////////////////////////////
	// Data
	std::vector<LPPointTip>	m_pPointTips;
	CSearchPointTree	m_DataPointSearchTree;

	void _SeekPointTip(UINT id,std::vector<plot::LPPointTip>::iterator& p);
	void _LoadPointTipDesc(LPPointTip p);

	void _StartDraging(CDragableUI* pDragObj);
	void _CancelDraging();
	void _CommitDraging();
	BOOL _UpdateDragingObject(LPARAM lParam);	//TRUE if moved

	LPFUNC_RetrievePointDescriptionCallback m_pRetrieveCallback;
	LPVOID	m_Cookie;

public:
	enum
	{
		InteractiveMode_TracingMousePoint = 0x00000001,
		InteractiveMode_TracingMouseTip = 0x00000002,
		InteractiveMode_PointTipLinker = 0x00000004,

		UiObject_None	   = 0x1000,	
		UiObject_DataPoint = 0x1001,
		UiObject_TipText   = 0x1002,
		UiObject_TipSwitch = 0x1003,
		UiObject_TipClose  = 0x1004,
		UiObject_TipImage  = 0x1005,
		UiObject_TipOther  = 0x1006
	};

	class CConfigInteractive
	{
	public:
		DWORD InteractiveMode;
		DWORD PointTipStyle;

		Color4b	PointFoucs_Color;
		float	PointFoucs_Thick;
		int		PointFoucs_Range;  // in pixel

		int		Tip_LineWidth;
		Color4b Tip_LineColor;
		Color4b Tip_BgColor;
		Color4b Tip_ButtonBgColor;
		Color4b Tip_ButtonColor;
		int		Tip_ImageWidth;
		int		Tip_ImageHeight;
		Color4b	Tip_LinkerColor;

		CConfigInteractive();
	};


	CConfigInteractive	m_InteractiveConfig;

	CInteractiveFigure();
	virtual ~CInteractiveFigure();

	void	SetupLayout(int x,int y,int width,int height);
	void	RenderDatasetLayer();
	void	RenderInteractiveLayer();
	void	UpdateFinalView();
	void	SetPointTipFont(LPCTSTR Fontname,int size,int fontstyle);

	virtual void SetupDatasetPoint(const float * pos_2f,int Count,const BYTE* label_1b=NULL,const Color4b* color_4b=NULL);
	
	BOOL	BindToWindow(HWND hWnd,HDC hDC = NULL);
	void	MessageFilter(UINT uMsg,WPARAM wParam,LPARAM lParam);

	void	SetRetrievePointDescriptionCallback( LPFUNC_RetrievePointDescriptionCallback pFunc ,LPVOID Cookie)
	{
		m_pRetrieveCallback = pFunc; 
		m_Cookie = Cookie;
	}
	
	BOOL	AddPointTip(UINT id);
	void	RemovePointTipByPointId(UINT id);
	void	RemovePointTipByTipId(UINT id);
	void	RemoveAllPointTip();

	void	ObjectHitTest(float x,float y,CUiObjectRef& ret);

	LPCFloat	ImagePointToDataPoint(int x,int y);
	int			ImagePointToDataPointId(int x,int y)
	{ return (ImagePointToDataPoint(x,y) - ((float*)m_pPos))/2; }
};

} //namespace plot

} // namespace UI

