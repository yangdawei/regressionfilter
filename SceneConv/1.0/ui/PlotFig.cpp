#include "stdafx.h"
#include "plotfig.h"
#include <math.h>
#include <WindowsX.h>
#include <atlimage.h>
//#include <queue>

#pragma warning(disable:4305)
//warning C4305: 'zzz' : truncation from 'xxx' to 'yyy'


namespace UI
{

namespace plot
{
	static const CPos _ps_Triangle[] = { {0,-0.5}, {-0.433,0.25}, {0.433,0.25}, {0,-0.5} };
	static const CPos _ps_Cross[] = {	{-0.38,-0.38}, {0,0}, 
										{-0.38, 0.38}, {0,0},
										{ 0.38, 0.38}, {0,0},
										{ 0.38,-0.38}, {0,0},
										{-0.38,-0.38}, };
	static const CPos _ps_Rectangle[] = { {-0.35,0.35},{-0.35,-0.35},{0.35,-0.35},{0.35,0.35},{-0.35,0.35} };
	static const CPos _ps_Prism[] = { {0,0.5},{0.5,0},{0,-0.5},{-0.5,0},{0,0.5} };
	static const CPos _ps_TriangleLeft[] = { {-0.5,0}, {0.25,0.433}, {0.25,-0.433}, {-0.5,0} };
	static const CPos _ps_TriangleRight[] = { {0.5,0}, {-0.25,0.433}, {-0.25,-0.433}, {0.5,0} };
	static const CPos _ps_TriangleDown[] = { {0,0.5}, {0.433,-0.25}, {-0.433,-0.25}, {0,0.5} };
	static const CPos _ps_BiTriangle[] = { {-0.38,-0.38},{0.38,0.38},{0.38,-0.38},{-0.38,0.38},{-0.38,-0.38} };
	static const CPos _ps_HalfGrid[] = { {-0.5,-0.5}, {-0.5,0}, {0.5,0}, {0.5,0.5}, {0,0.5}, {0,-0.5}, {-0.5,-0.5} };

	
	static const CShapeConst _PointStyleList[] = 
	{
		{ _ps_Triangle, 4 },
		{ _ps_Rectangle, 5 },
		{ _ps_Cross, 9 },
		{ _ps_Prism, 5 },
		{ _ps_TriangleLeft, 4 },
		{ _ps_TriangleRight, 4 },
		{ _ps_TriangleDown, 4 },
		{ _ps_BiTriangle, 5 },
		{ _ps_HalfGrid, 7 }
	};

	const CShapeConst* UI::plot::CGraphicsEx::m_PointShapes = _PointStyleList;
}

}


////////////////////////////////////////////////////////////////
// class CGraphicsEx
void  UI::plot::CGraphicsEx::SetupLabelStyleList(const CLabelStyle * pStyles,int Count)
{
	ASSERT(pStyles);
	ASSERT(Count);

	m_pPointStyleList = pStyles;
	m_PointStyleCount = Count;
}

UI::plot::CGraphicsEx::CGraphicsEx(HDC hDC)
:	Gdiplus::Graphics(hDC),
	m_PointPen(RGBA(0,0,0,1),0.1),
	m_TextBrush(Gdiplus::Color(RGBA(0,0,0,1)))
{
	m_pPointStyleList = NULL;
	_StackTop = -1;
}

UI::plot::CGraphicsEx::~CGraphicsEx()
{
}

void UI::plot::CGraphicsEx::DrawPoints(int Count,const CPos* pPoints,UI::plot::CLabelStyle * style)
{
	ASSERT(Count);
	ASSERT(pPoints);

	m_PointPen.SetColor(style?style->Color:RGBA(0,0,0,0.9));
	PointStyle ps = (UI::plot::CGraphicsEx::PointStyle)(style?style->PointMode:PS_Cycle);

	PushMatrix();

	ScaleTransform(m_PointSize,m_PointSize);

	const CPos* p = pPoints;
	const CPos* end = &pPoints[Count];

	TranslateTransform(p->x*m_DatasetScaleX,p->y*m_DatasetScaleY);
	DrawPoint(ps);
	p++;

	for(;p<end;p++)
	{
		TranslateTransform((p[0].x-p[-1].x)*m_DatasetScaleX,(p[0].y-p[-1].y)*m_DatasetScaleY);
		DrawPoint(ps);
	}

	PopMatrix();
}

void UI::plot::CGraphicsEx::DrawPoints(int Count,const CPos* pPoints,const Color4b* pColor,CGraphicsEx::PointStyle ps)
{
	ASSERT(Count);
	ASSERT(pPoints);
	ASSERT(pColor);

	PushMatrix();

	ScaleTransform(m_PointSize,m_PointSize);

	const CPos* p = pPoints;
	const Color4b* pCol = pColor;
	const CPos* end = &pPoints[Count];

	TranslateTransform(p->x*m_DatasetScaleX,p->y*m_DatasetScaleY);
	DrawPoint(ps);
	p++;
	pCol++;

	for(;p<end;p++,pCol++)
	{
		m_PointPen.SetColor(*pCol);
		TranslateTransform((p[0].x-p[-1].x)*m_DatasetScaleX,(p[0].y-p[-1].y)*m_DatasetScaleY);
		DrawPoint(ps);
	}

	PopMatrix();
}

void UI::plot::CGraphicsEx::DrawPoints(int Count,const CPos* pPoints,const BYTE* pLabel)
{
	ASSERT(Count);
	ASSERT(pPoints);
	ASSERT(pLabel);
	ASSERT(m_pPointStyleList);
	ASSERT(m_PointStyleCount);

	PushMatrix();

	ScaleTransform(m_PointSize,m_PointSize);

	const CPos* p = pPoints;
	const BYTE* pLab = pLabel;
	const CPos* end = &pPoints[Count];

	TranslateTransform(p->x*m_DatasetScaleX,p->y*m_DatasetScaleY);
	m_PointPen.SetColor(m_pPointStyleList[*pLab].Color);
	DrawPoint((UI::plot::CGraphicsEx::PointStyle)m_pPointStyleList[*pLab].PointMode);

	ASSERT(*pLab < m_PointStyleCount);
	p++;
	pLab++;

	for(;p<end;p++,pLab++)
	{
		ASSERT(*pLab < m_PointStyleCount);
		TranslateTransform((p[0].x-p[-1].x)*m_DatasetScaleX,(p[0].y-p[-1].y)*m_DatasetScaleY);

		m_PointPen.SetColor(m_pPointStyleList[*pLab].Color);
		DrawPoint((UI::plot::CGraphicsEx::PointStyle)m_pPointStyleList[*pLab].PointMode);
	}

	PopMatrix();
}

void UI::plot::CGraphicsEx::DrawPoints(int Count,const CPos* pPoints,const BYTE* pLabel,const Color4b* pColor)
{
	ASSERT(Count);
	ASSERT(pPoints);
	ASSERT(pLabel);
	ASSERT(pColor);
	ASSERT(m_pPointStyleList);
	ASSERT(m_PointStyleCount);

	PushMatrix();

	ScaleTransform(m_PointSize,m_PointSize);

	const CPos* p = pPoints;
	const BYTE* pLab = pLabel;
	const Color4b* pCol = pColor;
	const CPos* end = &pPoints[Count];

	m_PointPen.SetColor(*pCol);
	TranslateTransform(p->x*m_DatasetScaleX,p->y*m_DatasetScaleY);
	DrawPoint((UI::plot::CGraphicsEx::PointStyle)m_pPointStyleList[*pLab].PointMode);
	ASSERT(*pLab < m_PointStyleCount);
	p++;
	pLab++;
	pCol++;

	for(;p<end;p++,pLab++,pCol++)
	{
		ASSERT(*pLab < m_PointStyleCount);
		m_PointPen.SetColor(*pCol);

		TranslateTransform((p[0].x-p[-1].x)*m_DatasetScaleX,(p[0].y-p[-1].y)*m_DatasetScaleY);
		DrawPoint((UI::plot::CGraphicsEx::PointStyle)m_pPointStyleList[*pLab].PointMode);
	}

	PopMatrix();
}


//////////////////////////////////////////////////////////////////
// class CFigure::CDrawTools
UI::plot::CFigure::CDrawTools::CDrawTools():
	LinePen(RGBA(0,0,0,1),2)	
{
	//Gdiplus::FontFamily ff(L"Arial");
	Gdiplus::FontFamily ff(L"Garamond");
	pAxisFont = new Gdiplus::Font(&ff,12,Font_BoldItalic,Gdiplus::UnitPixel);
	ASSERT(pAxisFont);
}

void UI::plot::CFigure::CDrawTools::SetAxisFont(LPCTSTR Fontname,int size,int fontstyle)
{
	ASSERT(Fontname);

	_SafeDel(pAxisFont);

	USES_CONVERSION;

	pAxisFont = new Gdiplus::Font(T2CW(Fontname),size,fontstyle,Gdiplus::UnitPixel);
	ASSERT(pAxisFont);
}


//////////////////////////////////////////////////////////////////
// class CFigure
UI::plot::CFigure::CConfig::CConfig()
{
	LineMode = Line_Curve;//Line_Straight;
	LineWidth = 1.5;
	LineColor = RGBA(0,0,0,0.7);

	AA_Mode = AA_AntiAlias;
	EdgeMode = Edge_None;

	PointStyle.PointMode = UI::plot::CGraphicsEx::PS_Cross;//Point_Triangle;
	PointStyle.Color = RGBA(0,0,0,1);
	PointSize = 10.0;

	FrameMode = 0x77;//Frame_AxisVertical|Frame_TextVertical|Frame_AxisHorizontal|Frame_TextHorizontal;
	FrameGridInterval = 80;
	FrameLineWidth = 3.5;
	FrameColor = RGBA(0.2,0.2,0.2,0.7);
	FrameGridColor = RGBA(0,0,0,0.2);
	FrameTextColor = RGBA(0.2,0.2,0.2,0.85);

	KeepAspect = FALSE;
}

UI::plot::CFigure::CFigure()
{
	m_pPos = NULL;
	m_pLabel = NULL;
	m_pColor = NULL;
	m_PointCount = 0;

	m_pEdge = NULL;
	m_pPointStyleList = NULL;
}

UI::plot::CFigure::~CFigure()
{
	m_DatasetLayer.Destroy(); 

	_SafeDelArray(m_pPointStyleList);
}

void UI::plot::CFigure::SetupDatasetPoint(const float * pos_2f,int Count,const BYTE* label_1u,const Color4b* color_4b)
{
	ASSERT(pos_2f);
	m_pPos = (const CPos*)pos_2f;
	m_PointCount = Count;

	m_pLabel = label_1u;
	m_pColor = color_4b;

	//bounding box
	{
		float x_max,x_min;
		float y_max,y_min;
		const float * p = pos_2f;
		const float * end = &pos_2f[Count*2];
		
		x_max = x_min = p[0];
		y_max = y_min = p[1];

		p+=2;

		for(;p<end;p+=2)
		{
			if(p[0] > x_max)
			{
				x_max = p[0];
			}
			else if(p[0] < x_min)
			{
				x_min = p[0];
			}

			if(p[1] > y_max)
			{
				y_max = p[1];
			}
			else if(p[1] < y_min)
			{
				y_min = p[1];
			}
		}

		m_DataSetCenterX = (x_max+x_min)/2.0f;
		m_DataSetCenterY = (y_max+y_min)/2.0f;
		m_DataSetScaleX = 1.0f/(x_max - x_min);
		m_DataSetScaleY = 1.0f/(y_max - y_min);
	}

	//Label bounding
    if(label_1u)
	{
		m_MaxLabelId = label_1u[0];
		for(int i=1;i<Count;i++)
		{
			if(m_MaxLabelId < label_1u[i])m_MaxLabelId = label_1u[i];
		}
	}

	UpdateProjectionMatrix();
	RenderDatasetLayer();
}

void UI::plot::CFigure::SetConnectivityPointer(int mode,const CPointPair* pEdges,int Count)
{
	switch(mode)
	{
	case Edge_None:
		m_Config.EdgeMode = Edge_None;
		return;
	case Edge_LinearNeighbor:
        m_Config.EdgeMode = Edge_LinearNeighbor;
		return;
	case Edge_Customized:
		if(Count)
		{
			m_Config.EdgeMode = Edge_Customized;
			m_pEdge = pEdges;
			m_EdgeCount = Count;
		}
		else
		{
			m_Config.EdgeMode = Edge_None;
		}
		return;
	default:
		ASSERT(0);
	}
}

void UI::plot::CFigure::ResetContent()
{
	const static float point[] = {1.0,0.0,0.0,0.0,0.0,1.0,1.0,1.0};
	SetupDatasetPoint(point,4);
	RenderDatasetLayer();
}

void UI::plot::CFigure::SetupLayout(int x,int y,int width,int height)
{
	m_AreaX = x;
	m_AreaY = y;
	m_AreaWidth = width;
	m_AreaHeight = height;

	if(!m_DatasetLayer.IsNull())
	{
		m_DatasetLayer.Destroy();
	}

	VERIFY(m_DatasetLayer.Create(m_AreaWidth,-m_AreaHeight,24));

	UpdateProjectionMatrix();

	ResetContent();
}


void UI::plot::CFigure::RenderDatasetLayer()
{
	if(m_PointCount)
	{
		//Clear Frame Buffer to white
	#ifdef IPP_ImageProcess_Included
		Ippi::memset32u(m_DatasetLayer.GetBits(),0xdddddddd,(m_DatasetLayer.GetPitch()*m_AreaHeight)>>2);
	#else
		memset(m_DatasetLayer.GetBits(),0xdd,m_DatasetLayer.GetPitch()*m_AreaHeight);
	#endif

		//Prepare Gdi+ Graphics object
		CGraphicsEx	figure(m_DatasetLayer.GetDC());
		figure.SetClip(Gdiplus::Rect(0,0,m_AreaWidth,m_AreaHeight),Gdiplus::CombineModeReplace);
		figure.SetSmoothingMode((Gdiplus::SmoothingMode)m_Config.AA_Mode);

		///////////////////////////////////////////////////////
		// setup frame in image space
		float axis_x,axis_y;
		float axis_xstep=-1,axis_ystep=-1;
		BOOL  FirstYDotDraw = FALSE;
		if(m_Config.FrameMode)
		{
			Gdiplus::PointF p[3];
			ZeroMemory(p,sizeof(p));
			p[1].X = m_AreaWidth;
			p[2].Y = m_AreaHeight;
			
			m_ProjectionInv.TransformPoints(p,3);
			
			if(m_Config.FrameMode & 0x7)
			{//Vertical
				float s = m_Config.FrameGridInterval*fabs(p[0].Y - p[2].Y)/m_AreaHeight;
				int int_s = log10(s);
				if(s<1)int_s--;

				axis_ystep = exp(w32::sc_Ln10*int_s);
				axis_ystep = ((int)(s/axis_ystep))*axis_ystep;
				axis_ystep = m_AreaHeight*axis_ystep/(p[0].Y - p[2].Y);

				s = exp(w32::sc_Ln10*(int_s));
				axis_y = ((int)((p[2].Y)/s))*s;

				if(axis_ystep > 0)
				{
					axis_y = axis_y - p[2].Y;
					if(axis_y < 0)axis_y+=s;
				}
				else
				{
					axis_y = p[2].Y - axis_y;
					if(axis_y < 0)axis_y+=s;
				}

				axis_y = m_AreaHeight - axis_y*m_AreaHeight/fabs(p[0].Y - p[2].Y);

				FirstYDotDraw = ( m_AreaHeight - axis_y) > ( m_Tools.pAxisFont->GetSize()*1.5 + m_Config.FrameLineWidth/2 + 3 );

				////////////////////////////////////////////////////////
				// Rendering
				float y;

				if(m_Config.FrameMode & Frame_GridVertical)  // with grid
				{
					m_Tools.LinePen.SetWidth(1);
					m_Tools.LinePen.SetColor(m_Config.FrameGridColor);
					for(y=axis_y;y>=0;y-=axis_ystep)
					{
						figure.DrawLine(&m_Tools.LinePen,0.0f,y,(float)m_AreaWidth,y);
					}
				}

				if(m_Config.FrameMode & Frame_AxisVertical)
				{
					m_Tools.LinePen.SetWidth(m_Config.FrameLineWidth);
					m_Tools.LinePen.SetColor(m_Config.FrameColor);

					y = FirstYDotDraw?axis_y:(axis_y - axis_ystep);
					for(;y>=0;y-=axis_ystep)
					{
						figure.DrawLine(&m_Tools.LinePen,0.0f,y,3.0f,y);
					}

					figure.DrawLine(&m_Tools.LinePen,0.0f,0.0f,0.0f,(float)m_AreaHeight);
				}
				///////////////////////////////////////////////////////
			}

			if(m_Config.FrameMode & 0x70)
			{//Horizontal
				float s = m_Config.FrameGridInterval*fabs(p[0].X - p[1].X)/m_AreaWidth;
				int int_s = log10(s);
				if(s<1)int_s--;

				axis_xstep = exp(w32::sc_Ln10*int_s);
				axis_xstep = ((int)(s/axis_xstep))*axis_xstep;
				axis_xstep = m_AreaWidth*axis_xstep/(p[1].X - p[0].X);

				s = exp(w32::sc_Ln10*(int_s));
				axis_x = ((int)((p[0].X)/s))*s;

				if(axis_xstep > 0)
				{
					axis_x = axis_x - p[0].X;
					if(axis_x < 0)axis_x+=s;
				}
				else
				{
					axis_x = p[0].X - axis_x;
					if(axis_x < 0)axis_x+=s;
				}

				axis_x = axis_x*m_AreaWidth/fabs(p[0].X - p[1].X);

				////////////////////////////////////////////////////////
				// Rendering
				float x;

				if(m_Config.FrameMode & Frame_GridHorizontal)  // with grid
				{
					m_Tools.LinePen.SetWidth(1);
					m_Tools.LinePen.SetColor(m_Config.FrameGridColor);
					for(x = axis_x;x<=m_AreaWidth;x+=axis_xstep)
					{
						figure.DrawLine(&m_Tools.LinePen,x,0.0f,x,(float)m_AreaHeight);
					}
				}


				if(m_Config.FrameMode & Frame_AxisHorizontal)  // with grid
				{
					m_Tools.LinePen.SetWidth(m_Config.FrameLineWidth);
					m_Tools.LinePen.SetColor(m_Config.FrameColor);

					x = FirstYDotDraw?(axis_x+axis_xstep):axis_x;
					for(;x<=m_AreaWidth;x+=axis_xstep)
					{
						figure.DrawLine(&m_Tools.LinePen,x,(float)m_AreaHeight - m_Config.FrameLineWidth/2,x,(float)m_AreaHeight-3.0f - m_Config.FrameLineWidth/2);
					}

					figure.DrawLine(&m_Tools.LinePen,0.0f,(float)m_AreaHeight - m_Config.FrameLineWidth/2,(float)m_AreaWidth,(float)m_AreaHeight - m_Config.FrameLineWidth/2);
				}
				///////////////////////////////////////////////////////
			}
		}

		//////////////////////////////////////////////////////
		// All edges
		float Scale_x,Scale_y;

		LoadInitialTransform(figure);
		{
			Scale_x = m_DataSetScaleX*(m_AreaWidth);
			Scale_y = m_DataSetScaleY*(m_AreaHeight);
			float scale = min(Scale_x,Scale_y);
			if(m_Config.KeepAspect)
			{
				Scale_x = Scale_y = scale;
			}
			m_Tools.LinePen.SetWidth(m_Config.LineWidth/scale);
		}

		figure.ScaleTransform(Scale_x,-Scale_y);
		figure.TranslateTransform(-m_DataSetCenterX,-m_DataSetCenterY);

		//m_Tools.LinePen.SetColor(RGBA(0,0,0,0.5));
		//figure.DrawEllipse(&m_Tools.LinePen,-5.0f,-5.0f,10.0f,10.0f);
		//figure.DrawLine(&m_Tools.LinePen,0.0f,4.0f,0.0f,6.0f);
		//figure.DrawLine(&m_Tools.LinePen,4.0f,0.0f,6.0f,0.0f);
		//m_Tools.LinePen.SetColor(RGBA(0,0,0,1));

		//Draw Edges
		if(m_Config.LineMode != Line_None)
		{
			m_Tools.LinePen.SetColor(m_Config.LineColor);

			switch( m_Config.EdgeMode )
			{
			case Edge_Customized:
				//go throught the Point Pair List
				if(m_EdgeCount)
				{
					const CPointPair* p = m_pEdge;
					const CPointPair* end = &m_pEdge[m_EdgeCount-1];

					for(;p<end;p++)
					{
						figure.DrawLine(&m_Tools.LinePen,*((Gdiplus::PointF*)&m_pPos[(p->v1)*2]),*((Gdiplus::PointF*)&m_pPos[(p->v2)*2]));
					}
				}
				break;
			case Edge_LinearNeighbor:
				if(m_Config.LineMode == Line_Straight)
				{
					figure.DrawLines(&m_Tools.LinePen,((Gdiplus::PointF*)m_pPos),m_PointCount);
				}
				else
				{
					ASSERT(m_Config.LineMode == Line_Curve);
					figure.DrawCurve(&m_Tools.LinePen,((Gdiplus::PointF*)m_pPos),m_PointCount);
				}
				break;
			}
		}

		////////////////////////////////////////////////////////////
		//All Point
		if(m_Config.PointStyle.PointMode != Point_Pixel)
		{
			figure.SetPointSize(m_Config.PointSize);
			figure.SetDatasetScale(Scale_x,-Scale_y);

			LoadInitialTransform(figure);

			figure.TranslateTransform(-m_DataSetCenterX*Scale_x,m_DataSetCenterY*Scale_y);

			//// Debug display space partations 
			//{
			//	typedef CSearchPointTree::CSPTreeNode* LPNODE;
			//	std::queue<LPNODE>	nodes;
			//	nodes.push(m_DataPointSearchTree.GetRoot());

			//	while(nodes.size())
			//	{
			//		LPNODE cur = nodes.front();
			//		nodes.pop();

			//		if(!cur->IsLeaf())
			//		{
			//			for(int i=0;i<4;i++)
			//			{
			//				if(cur->Children[i])
			//					nodes.push((LPNODE)cur->Children[i]);
			//			}
			//		}

			//		//draw bounding box
			//		Gdiplus::RectF rc;
			//		rc.X = (cur->Box.Min[0])*Scale_x;
			//		rc.Y = -(cur->Box.Max[1])*Scale_y;
			//		rc.Width = (cur->Box.Max[0] - cur->Box.Min[0])*Scale_x;
			//		rc.Height = (cur->Box.Max[1] - cur->Box.Min[1])*Scale_y;
			//		
			//		figure.DrawRectangle(&m_Tools.LinePen,rc);
			//	}
			//}

			//all points
			if( m_pLabel && m_pPointStyleList)
			{
				figure.SetupLabelStyleList(m_pPointStyleList,m_PointStyleCount);

				if( m_pColor )
				{
					figure.DrawPoints(m_PointCount,m_pPos,m_pLabel,m_pColor);
				}
				else
				{
					figure.DrawPoints(m_PointCount,m_pPos,m_pLabel);
				}
			}
			else
			{
				if( m_pColor )
				{
					figure.DrawPoints(m_PointCount,m_pPos,m_pColor,(UI::plot::CGraphicsEx::PointStyle)m_Config.PointStyle.PointMode);
				}
				else
				{
					figure.DrawPoints(m_PointCount,m_pPos,&m_Config.PointStyle);
				}
			}
		}

		////////////////////////////////////////////////////////
		// Axis Text
		figure.ResetTransform();
		if(m_Config.FrameMode & Frame_TextVertical)
		{
			figure.SetTextColor(m_Config.FrameTextColor);

			Gdiplus::PointF p;
			int FontHeight = m_Tools.pAxisFont->GetSize();
			WCHAR Text[100];
			float y = FirstYDotDraw?axis_y:(axis_y - axis_ystep);
			for(;y>=0;y-=axis_ystep)
			{
				p.X = 0;
				p.Y = y;
				m_ProjectionInv.TransformPoints(&p);

				swprintf(Text,L"%.2f",p.Y);
				ASSERT(m_Tools.pAxisFont);
				figure.DrawText(Text,m_Tools.pAxisFont,m_Config.FrameLineWidth/2 + 3,y - FontHeight/2);
			}
		}

		if(m_Config.FrameMode & Frame_TextHorizontal)
		{
			figure.SetTextColor(m_Config.FrameTextColor);

			Gdiplus::PointF p;
			WCHAR Text[100];
			int FontHeight = m_Tools.pAxisFont->GetSize();
			float x = FirstYDotDraw?(axis_x+axis_xstep):axis_x;
			for(;x<=m_AreaWidth;x+=axis_xstep)
			{
				p.X = x;
				p.Y = 0;
				m_ProjectionInv.TransformPoints(&p);

				int len = swprintf(Text,L"%.2f",p.X);
				ASSERT(m_Tools.pAxisFont);
				figure.DrawText(	Text,
									m_Tools.pAxisFont,
									x - m_Config.FrameLineWidth*len,
									m_AreaHeight - 3 - FontHeight - m_Config.FrameLineWidth/2);
			}
		}

		m_DatasetLayer.ReleaseDC();
	}
}

void  UI::plot::CFigure::SetupLabelStyleList(const UI::plot::CLabelStyle * pStyles,int Count)
{
	ASSERT(pStyles);
	ASSERT(Count);

	_SafeDelArray(m_pPointStyleList);
	m_pPointStyleList = new UI::plot::CLabelStyle[Count];
	ASSERT(m_pPointStyleList);

	memcpy(m_pPointStyleList,pStyles,sizeof(UI::plot::CLabelStyle)*Count);
	m_PointStyleCount = Count;
}

void UI::plot::CFigure::FinalSynthesis(HDC out)
{
	m_DatasetLayer.BitBlt(out,m_AreaX,m_AreaY);
}

void UI::plot::CFigure::LoadInitialTransform(Gdiplus::Graphics & g)
{
	g.ResetTransform();
	g.TranslateTransform(m_AreaWidth/2,m_AreaHeight/2);
	g.ScaleTransform(0.9,0.9);
}

void UI::plot::CFigure::UpdateProjectionMatrix()
{
	//////////////////////////////////////////
	// must equal to LoadInitialTransform
	m_Projection.Reset();
	m_Projection.Translate(m_AreaWidth/2,m_AreaHeight/2);
	m_Projection.Scale(0.9,0.9);
	////////////////////////////////

	/////////////////////////////////////////
	// must equal to RenderDatasetLayer
	{
		float Scale_x,Scale_y;
		Scale_x = m_DataSetScaleX*(m_AreaWidth);
		Scale_y = m_DataSetScaleY*(m_AreaHeight);
		float scale = min(Scale_x,Scale_y);
		if(m_Config.KeepAspect)
		{
			Scale_x = Scale_y = scale;
		}

		m_Projection.Scale(Scale_x,-Scale_y);
		m_Projection.Translate(-m_DataSetCenterX,-m_DataSetCenterY);
	}
	////////////////////////////////////////

	//float v[6] = { 0,0,0,1,1,0 };
	//m_Projection.TransformPoints((Gdiplus::PointF*)v,3);
	//m_PixelUnit =   sqrt(
	//						max (
	//							w32::Sqr_t(v[0] - v[2])+w32::Sqr_t(v[1] - v[3]),
	//							w32::Sqr_t(v[0] - v[4])+w32::Sqr_t(v[1] - v[5])
	//							)
	//					);

	float	m[6];
	m_Projection.GetElements(m);
	m_ProjectionInv.SetElements(m[0],m[1],m[2],m[3],m[4],m[5]);
	VERIFYH(m_ProjectionInv.Invert());
}

///////////////////////////////////////////////////////
// class CInteractiveFigure::CConfig
UI::plot::CInteractiveFigure::CConfigInteractive::CConfigInteractive()
{
	InteractiveMode = InteractiveMode_TracingMousePoint|InteractiveMode_PointTipLinker;

	PointFoucs_Color = RGBA(0.237,0.269,0.551,0.6);
	PointFoucs_Thick = 0.4;
	PointFoucs_Range = 16;

	Tip_LineWidth = 1;
	Tip_LineColor = RGBA(0.039,0.141,0.416,0.9);
	Tip_BgColor = RGBA(0.7,0.7,0.73,0.6);
	Tip_ButtonBgColor = RGBA(0.027,0.102,0.302,0.8);
	Tip_ButtonColor = RGBA(0.85,0.85,0.95,0.8);
	Tip_LinkerColor = RGBA(0.4,0.4,0.50,0.7);
	Tip_ImageWidth = 50;
	Tip_ImageHeight = 50;
}

//////////////////////////////////////////////////////////
// class CPointTip
void UI::plot::CPointTip::SetImage(ATL::CImage & img)
{
	if(!m_Image.IsNull())m_Image.Destroy();

	if(!img.IsNull())
	{
		VERIFY(m_Image.Create(ImageWidth,-ImageHeight,24));

		HDC dc = m_Image.GetDC();
		::SetStretchBltMode(dc,HALFTONE);

		img.StretchBlt(dc,0,0,ImageWidth,ImageHeight);
		m_Image.ReleaseDC();
	}
}

void UI::plot::CPointTip::SetText(LPCTSTR p)
{
	USES_CONVERSION;
	m_Text = (T2CW(p)); 
}

///////////////////////////////////////////////////////
// class CSearchPointTree
void UI::plot::CSearchPointTree::UpdateTree(CSPTreeNode* node)
{
	float pos[2];
	pos[0] = pos[1] = 0;
	if(node->IsLeaf())
	{
		ASSERT(node->TotalDots); 

		ASSERT(node->TotalDots==node->LeafNodeNextSlot);
		for(int i=0;i<node->TotalDots;i++)
		{
			pos[0]+=node->Children[i]->x;
			pos[1]+=node->Children[i]->y;
		}
	}
	else
	{
#ifdef _DEBUG
		int total_dots=0;
#endif
		for(int i=0;i<4;i++)
		{
			if(node->Children[i])
			{
				UpdateTree((CSPTreeNode*)node->Children[i]);

				pos[0] += node->Children[i]->x*((CSPTreeNode*)node->Children[i])->TotalDots;
				pos[1] += node->Children[i]->y*((CSPTreeNode*)node->Children[i])->TotalDots;
	#ifdef _DEBUG
				total_dots+=((CSPTreeNode*)node->Children[i])->TotalDots;
	#endif
			}
		}
		ASSERT( total_dots == node->TotalDots );
	}

	node->x = pos[0]/node->TotalDots;
	node->y = pos[1]/node->TotalDots;
}

UI::plot::LPCFloat UI::plot::CSearchPointTree::SearchPointAddress(UI::plot::LPCFloat pPoint)
{
	CSPTreeNode* pSubspace = GetRoot();
	for(;;)
	{
		if(pSubspace->IsLeaf())
		{
			ASSERT(pSubspace->TotalDots); 

			//search a closest point
			float dis[4];
			ASSERT(pSubspace->Children[0]);
			dis[0] = w32::L2Diff2d(pPoint,((LPCFloat)pSubspace->Children[0]));

			if(pSubspace->Children[1])
				dis[1] = w32::L2Diff2d(pPoint,((LPCFloat)pSubspace->Children[1]));
			else
				return (LPCFloat)pSubspace->Children[0];

			if(pSubspace->Children[2])
				dis[2] = w32::L2Diff2d(pPoint,((LPCFloat)pSubspace->Children[2]));
			else
				return (LPCFloat)pSubspace->Children[dis[0]>dis[1]];

			if(pSubspace->Children[3])
				dis[3] = w32::L2Diff2d(pPoint,((LPCFloat)pSubspace->Children[3]));
			else
				dis[3] = FLT_MAX;

			int id_min[2];
			id_min[0] = dis[0]>dis[1];
			id_min[1] = (dis[2]>dis[3]) + 2;

            
			return (LPCFloat)pSubspace->Children[id_min[dis[id_min[0]]>dis[id_min[1]]]];
		}
		else
		{
			//enter hit subspace
			int id = pSubspace->Box.HitOnSubspace(pPoint);

			if(pSubspace->Children[id])
				pSubspace = (CSPTreeNode*)pSubspace->Children[id];
			else
			{

				float dis[4];
				dis[id] = FLT_MAX;

				id = (id+1)&3;
				if(pSubspace->Children[id])
					dis[id] = w32::L2Diff2d(pPoint,((LPCFloat)pSubspace->Children[id]));
				else dis[id] = FLT_MAX;

				id = (id+1)&3;
				if(pSubspace->Children[id])
					dis[id] = w32::L2Diff2d(pPoint,((LPCFloat)pSubspace->Children[id]));
				else dis[id] = FLT_MAX;

				id = (id+1)&3;
				if(pSubspace->Children[id])
					dis[id] = w32::L2Diff2d(pPoint,((LPCFloat)pSubspace->Children[id]));
				else dis[id] = FLT_MAX;

				int id_min[2];
				id_min[0] = dis[0]>dis[1];
				id_min[1] = (dis[2]>dis[3]) + 2;

				pSubspace = (CSPTreeNode*)pSubspace->Children[id_min[dis[id_min[0]]>dis[id_min[1]]]];
			}
		}
	}
}


///////////////////////////////////////////////////////
// class CInteractiveFigure
void UI::plot::CInteractiveFigure::RenderDatasetLayer()
{
	__super::RenderDatasetLayer();

	RenderInteractiveLayer();
}

UI::plot::CInteractiveFigure::CInteractiveFigure()
{
	m_FinalOutputDC = NULL;

	is_PointFoucsId = -1;
	is_MouseDraging = FALSE;

	m_pPointTipFont = new Gdiplus::Font(L"Arial",10,Font_Bold,Gdiplus::UnitPixel);
	m_pRetrieveCallback = NULL;
}

void UI::plot::CInteractiveFigure::SetupDatasetPoint(const float * pos_2f,int Count,const BYTE* label_1b,const Color4b* color_4b)
{
	RemoveAllPointTip();
	__super::SetupDatasetPoint(pos_2f,Count,label_1b,color_4b);

	//Build search tree
	m_DataPointSearchTree.BuildTree((LPCFloat)m_pPos,m_PointCount);
	RenderInteractiveLayer();
}


UI::plot::CInteractiveFigure::~CInteractiveFigure()
{
	m_InteractiveLayer.Destroy();
	is_DragingObjectImage.Destroy();

	_SafeDel(m_pPointTipFont);
}

void UI::plot::CInteractiveFigure::SetupLayout(int x,int y,int width,int height)
{
	if(!m_InteractiveLayer.IsNull())
	{
		//scale x y in PointTip list
		float x_scale = width/((float)m_InteractiveLayer.GetWidth());
		float y_scale = height/((float)m_InteractiveLayer.GetHeight());

		std::vector<LPPointTip>::iterator ptr;
		for(ptr = m_pPointTips.begin();ptr!=m_pPointTips.end();ptr++)
		{
			((LPPointTip)(*ptr))->ui_x *= x_scale;
			((LPPointTip)(*ptr))->ui_y *= y_scale;
		}

		m_InteractiveLayer.Destroy();
	}

	VERIFY(m_InteractiveLayer.Create(width,-height,24));

	__super::SetupLayout(x,y,width,height);

	if(!m_InteractiveLayer.IsNull())
	{
		std::vector<LPPointTip>::iterator ptr;
		for(ptr = m_pPointTips.begin();ptr!=m_pPointTips.end();ptr++)
		{
			LPPointTip p = ((LPPointTip)(*ptr));
			w32::Copy2d(p->PtImgSpace,(float *)&m_pPos[p->LinkedPointId]);

			Dataset2Image2fv(p->PtImgSpace);

			p->PtImgSpace[0] -= m_AreaX;
			p->PtImgSpace[1] -= m_AreaY;
		}
	}

	RenderInteractiveLayer();
}

const float* UI::plot::CInteractiveFigure::ImagePointToDataPoint(int x,int y)
{
	float v[2];
	v[0] = x - m_AreaX;
	v[1] = y - m_AreaY;

	m_ProjectionInv.TransformPoints((Gdiplus::PointF*)v);

	return m_DataPointSearchTree.SearchPointAddress(v);
}

void UI::plot::CInteractiveFigure::RenderInteractiveLayer()
{
	ASSERT( m_DatasetLayer.GetPitch() == m_InteractiveLayer.GetPitch() );

	////////////////////////////////////////
	// BitBlt to m_InteractiveLayer

#ifdef IPP_ImageProcess_Included
	Ippi::memcpy32AL(m_InteractiveLayer.GetBits(),m_DatasetLayer.GetBits(),m_AreaHeight*m_DatasetLayer.GetPitch());
#else
	memcpy(m_InteractiveLayer.GetBits(),m_DatasetLayer.GetBits(),m_AreaHeight*m_DatasetLayer.GetPitch());
#endif

	HDC OutDC;
	UI::plot::CGraphicsEx	fig(OutDC = m_InteractiveLayer.GetDC());
	fig.SetClip(Gdiplus::Rect(0,0,m_AreaWidth,m_AreaHeight),Gdiplus::CombineModeReplace);
	fig.SetSmoothingMode((Gdiplus::SmoothingMode)m_Config.AA_Mode);

	ASSERT(m_pPointTipFont);
	////////////////////////////////////////
	// Render UI elements on m_InteractiveLayer
	std::vector<UI::plot::LPPointTip>::iterator ptr;
	float FontHeight = 0.9*m_pPointTipFont->GetHeight(&fig);

	Gdiplus::Pen pen(m_InteractiveConfig.Tip_LineColor,m_InteractiveConfig.Tip_LineWidth);
	Gdiplus::SolidBrush br(RGBA(0,0,0,0));
	//All borders and text
	{
		for(ptr = m_pPointTips.begin();ptr!=m_pPointTips.end();ptr++)
		{
			LPPointTip pTip = (*ptr);
			if( ((CDragableUI*)pTip) != is_pDragingObject){}
			else{ continue; }

			pTip->ui_ButtonSize = FontHeight;
			pTip->ui_ButtonY = FontHeight/2 + 2;

			float TextWidth;
			CGraphicsEx::PointStyle SwitchBut;

			// bottom Border
			if(pTip->HasImage())
			{
				if( pTip->HidenFlag & CPointTip::Hiden_Image )
				{
					if( pTip->HidenFlag & CPointTip::Hiden_Text )
					{
						TextWidth = pTip->ImageWidth - 3*FontHeight - 4 - m_InteractiveConfig.Tip_LineWidth;
						SwitchBut = CGraphicsEx::PS_TriangleDown;

					}
					else
					{
						TextWidth = fig.MeasureTextWidth(pTip->m_Text.c_str(),m_pPointTipFont);
						SwitchBut = CGraphicsEx::PS_TriangleLeft;
					}

					pTip->ui_Height = FontHeight + 4.0f + m_InteractiveConfig.Tip_LineWidth;
				}
				else
				{
					TextWidth = pTip->ImageWidth - 3*FontHeight - 4 - m_InteractiveConfig.Tip_LineWidth;
					SwitchBut = CGraphicsEx::PS_Triangle;

					Gdiplus::PointF Box[4];
					Box[0].X = pTip->ui_x;		
					Box[0].Y = pTip->ui_y + FontHeight + 4 + m_InteractiveConfig.Tip_LineWidth;

					Box[1].X = Box[0].X;
					Box[1].Y = pTip->ui_y + FontHeight + 4 + m_InteractiveConfig.Tip_LineWidth + 1 + pTip->ImageHeight;

					Box[2].X = pTip->ui_x + pTip->ImageWidth;
					Box[2].Y = Box[1].Y;

					Box[3].X = Box[2].X;
					Box[3].Y = Box[0].Y;

					fig.DrawLines(&pen,Box,4);

					pTip->ui_Height = FontHeight + 4.0f + m_InteractiveConfig.Tip_LineWidth + pTip->ImageHeight;
				}
			}
			else
			{
				if( pTip->HidenFlag & CPointTip::Hiden_Text )
				{
					TextWidth = pTip->ImageWidth - 3*FontHeight - 4 - m_InteractiveConfig.Tip_LineWidth;
					SwitchBut = CGraphicsEx::PS_TriangleRight;
				}
				else
				{
					TextWidth = fig.MeasureTextWidth(pTip->m_Text.c_str(),m_pPointTipFont);
					SwitchBut = CGraphicsEx::PS_TriangleLeft;
				}

				pTip->ui_Height = FontHeight + 4.0f + m_InteractiveConfig.Tip_LineWidth;
			}

			pTip->ui_Width = TextWidth + 4.0f + 3*FontHeight + m_InteractiveConfig.Tip_LineWidth + 3.0f;
			pTip->ui_Height += 5.0f;

			if(m_InteractiveConfig.InteractiveMode & InteractiveMode_PointTipLinker)
			{
				pen.SetColor(m_InteractiveConfig.Tip_LinkerColor);
				pen.SetWidth(1.8);
				Gdiplus::PointF pt(pTip->ui_x+pTip->ui_Width/2,pTip->ui_y+pTip->ui_Height/2);
				fig.DrawLine(&pen,*((Gdiplus::PointF*)pTip->PtImgSpace),pt);
				pen.SetWidth(m_InteractiveConfig.Tip_LineWidth);
				pen.SetColor(m_InteractiveConfig.Tip_LineColor);
			}

			// Top border
			{
				Gdiplus::PointF Box[4];
				Box[0].X = pTip->ui_x + 2 + m_InteractiveConfig.Tip_LineWidth/2 + TextWidth;
				Box[0].Y = pTip->ui_y;
				Box[1].X = Box[0].X + FontHeight*0.66666667f;
				Box[1].Y = pTip->ui_y + FontHeight + 4 + m_InteractiveConfig.Tip_LineWidth;

				Box[2].X = pTip->ui_x;
				Box[2].Y = Box[1].Y;
				Box[3].X = Box[2].X;
				Box[3].Y = Box[0].Y;
				br.SetColor(m_InteractiveConfig.Tip_BgColor);
				fig.FillPolygon(&br,Box,4);

				Box[2].X = pTip->ui_x + TextWidth + 4 + 3*FontHeight + m_InteractiveConfig.Tip_LineWidth;
				Box[2].Y = Box[1].Y;
				Box[3].X = Box[2].X;
				Box[3].Y = Box[0].Y;
				br.SetColor(m_InteractiveConfig.Tip_ButtonBgColor);
				fig.FillPolygon(&br,Box,4);

				//fig.FillRectangle(&br,	pTip->x,pTip->y,
				//						TextWidth + 4 + 3*FontHeight - 2 + m_InteractiveConfig.Tip_LineWidth,
				//						FontHeight + 4);
			}


			fig.DrawRectangle(&pen,	(float)pTip->ui_x,(float)pTip->ui_y,
									(float)(TextWidth + 4.0f + 3*FontHeight + m_InteractiveConfig.Tip_LineWidth),
									(float)(FontHeight + 4.0f + m_InteractiveConfig.Tip_LineWidth));

			// Text
			fig.DrawBoundText(pTip->m_Text.c_str(),m_pPointTipFont,	pTip->ui_x + 2 + m_InteractiveConfig.Tip_LineWidth/2,
															pTip->ui_y + 1,
															TextWidth,FontHeight );

			// Button
			pTip->ui_SwitchButtonX = m_InteractiveConfig.Tip_LineWidth/2+2+TextWidth+FontHeight*1.35f;
			pTip->ui_CloseButtonX = pTip->ui_SwitchButtonX + 1.25f*FontHeight;
			fig.TranslateTransform(pTip->ui_x + pTip->ui_SwitchButtonX,pTip->ui_y + pTip->ui_ButtonY);
			fig.ScaleTransform(FontHeight*0.9,FontHeight*0.9);

			br.SetColor(m_InteractiveConfig.Tip_ButtonColor);
			fig.FillPolygon(&br,(Gdiplus::PointF*)fig.GetPointShape(SwitchBut)->pVertexs,4);

			fig.TranslateTransform(1.35f*0.9f,0);
			pen.SetColor(m_InteractiveConfig.Tip_ButtonColor);
			//pen.SetWidth(FontHeight/4);
			pen.SetWidth(0.25);
			fig.DrawLines(&pen,	(Gdiplus::PointF*)fig.GetPointShape(CGraphicsEx::PS_Cross)->pVertexs,9);
								
			pen.SetColor(m_InteractiveConfig.Tip_LineColor);
			pen.SetWidth(m_InteractiveConfig.Tip_LineWidth);

			fig.ResetTransform();
		}
	}

	// All images
	{
		Gdiplus::Color c(m_InteractiveConfig.Tip_LineColor);
		BYTE a = min(255,c.GetAlpha()+50);
		
		for(ptr = m_pPointTips.begin();ptr!=m_pPointTips.end();ptr++)
		{
			LPPointTip pTip = (*ptr);

			if( ((CDragableUI*)pTip) != is_pDragingObject){}
			else{ continue; }

			if(pTip->HasImage())
			{
				if( m_InteractiveConfig.Tip_ImageWidth != pTip->m_Image.GetWidth() ||
					m_InteractiveConfig.Tip_ImageHeight != pTip->m_Image.GetHeight() )
				{
					pTip->ImageWidth = m_InteractiveConfig.Tip_ImageWidth;
					pTip->ImageHeight = m_InteractiveConfig.Tip_ImageHeight;

					_LoadPointTipDesc(pTip);
				}

				if(!(pTip->HidenFlag & CPointTip::Hiden_Image))
				{
					pTip->m_Image.AlphaBlend(OutDC,pTip->ui_x + 1,pTip->ui_y+4+FontHeight+m_InteractiveConfig.Tip_LineWidth+1,a);
				}
			}
		}
	}

	m_InteractiveLayer.ReleaseDC();

	UpdateFinalView();
}

BOOL UI::plot::CInteractiveFigure::BindToWindow(HWND hWnd,HDC hdc)
{
	if(m_FinalOutputDC)
	{
		ReleaseDC(m_BindedWindow,m_FinalOutputDC);
	}

	m_FinalOutputDC = hdc?hdc:GetDC(hWnd);

	if( !m_FinalOutputDC )return FALSE;

	m_BindedWindow = hWnd;

	return TRUE;
}

BOOL UI::plot::CInteractiveFigure::_UpdateDragingObject(LPARAM lParam)	//TRUE if moved
{
	int DragingCurX_new = max(4,min(m_AreaWidth-is_DragingObjectImage.GetWidth()+2,
									GET_X_LPARAM(lParam) - m_AreaX + is_DragingCursorOffsetX));
	int DragingCurY_new = max(2,min(m_AreaHeight-is_DragingObjectImage.GetHeight()-3,
									GET_Y_LPARAM(lParam) - m_AreaY + is_DragingCursorOffsetY));

	if(DragingCurX_new!=is_DragingCurX || DragingCurY_new!=is_DragingCurY)
	{
		is_DragingCurX = DragingCurX_new;
		is_DragingCurY = DragingCurY_new;
		return TRUE;
	}

	return FALSE;
}

void UI::plot::CInteractiveFigure::MessageFilter(UINT uMsg,WPARAM wParam,LPARAM lParam)
{
	if( !m_InteractiveLayer.IsNull() && m_PointCount )
	{
		BOOL NeedUpdateView = FALSE;
		switch(uMsg)
		{
		case WM_MOUSEMOVE:
			{
				if( is_MouseDraging )
				{
					NeedUpdateView = _UpdateDragingObject(lParam);
				}
				else
				{
					if( m_InteractiveConfig.InteractiveMode & InteractiveMode_TracingMousePoint)
					{
						// set foucs to nearest point within m_PointFoucsRange
						int x = GET_X_LPARAM(lParam);
						int y = GET_Y_LPARAM(lParam);
						int org_id = is_PointFoucsId;

						CUiObjectRef obj;
						ObjectHitTest(x,y,obj);

						if(obj.Type == UiObject_DataPoint)
						{
							is_PointFoucsId = obj.ObjectIndex;

							float p[2];
							memcpy(&p,&m_pPos[is_PointFoucsId],sizeof(p));

							Dataset2Image2fv(p);

							is_PointFoucsX = p[0];
							is_PointFoucsY = p[1];
						}
						else
						{
							is_PointFoucsId = -1;
						}

						if( org_id != is_PointFoucsId)
						{
							NeedUpdateView = TRUE;
						}
					}
				}
				break;
			};
		case WM_PAINT:
			NeedUpdateView = TRUE;
			break;
		case WM_LBUTTONUP:
			if(is_MouseDraging)
			{
				_UpdateDragingObject(lParam);
				_CommitDraging();
			}
			break;
		case WM_LBUTTONDOWN:

			_CancelDraging();

			int x = GET_X_LPARAM(lParam);
			int y = GET_Y_LPARAM(lParam);

			CUiObjectRef obj;
			ObjectHitTest(x,y,obj);

			switch(obj.Type)
			{
			case UiObject_DataPoint:
				AddPointTip(obj.ObjectIndex);
				break;
			case UiObject_TipText:
			case UiObject_TipImage:
			case UiObject_TipOther:
				//Start to drag this tip
				is_DragingCurX = obj.pDragableObject->ui_x;
				is_DragingCurY = obj.pDragableObject->ui_y;
				is_DragingCursorOffsetX = is_DragingCurX - x + m_AreaX;
				is_DragingCursorOffsetY = is_DragingCurY - y + m_AreaY;
				_StartDraging(obj.pDragableObject);

				break;
			case UiObject_TipSwitch:
				{
					LPPointTip tip = m_pPointTips[obj.ObjectIndex];
					ASSERT(tip);

					// switch display state
					if(tip)
					{
						if(tip->HasImage())
						{
							if(tip->HidenFlag & CPointTip::Hiden_Image)
							{
								if(tip->HidenFlag & CPointTip::Hiden_Text)
								{
									tip->HidenFlag = 0;
								}
								else
								{
									tip->HidenFlag = CPointTip::Hiden_Text|CPointTip::Hiden_Image;
								}
							}
							else
							{
								tip->HidenFlag = CPointTip::Hiden_Image;
							}
						}
						else
						{
							if( tip->HidenFlag & CPointTip::Hiden_Text )
							{
								tip->HidenFlag = 0;
							}
							else
							{
								tip->HidenFlag = CPointTip::Hiden_Text;
							}
						}

						RenderInteractiveLayer();
					}
				}
				break;
			case UiObject_TipClose:
				{
					RemovePointTipByTipId(obj.ObjectIndex);
					RenderInteractiveLayer();
				}
				break;
			}
		}

		if(NeedUpdateView)UpdateFinalView();
	}
}

void UI::plot::CInteractiveFigure::_StartDraging(CDragableUI* pDragObj)
{
	ASSERT(!is_MouseDraging);

	is_MouseDraging = TRUE;
	VERIFY(is_pDragingObject = pDragObj);
	is_DragingCurX = is_DragingOrgX = is_pDragingObject->ui_x;
	is_DragingCurY = is_DragingOrgY = is_pDragingObject->ui_y;

	::SetCapture(m_BindedWindow);

	ASSERT(is_DragingObjectImage.IsNull());
	is_DragingObjectImage.Create(pDragObj->ui_Width+2,-pDragObj->ui_Height+2,24);
	m_InteractiveLayer.BitBlt(	is_DragingObjectImage.GetDC(),
								0,0,pDragObj->ui_Width+2,pDragObj->ui_Height+2,
								pDragObj->ui_x-1,pDragObj->ui_y-1);

	is_DragingObjectImage.ReleaseDC();

	RenderInteractiveLayer();
}

void UI::plot::CInteractiveFigure::_CancelDraging()
{
	if(is_MouseDraging)
	{
		::ReleaseCapture();
		is_MouseDraging = FALSE;
		is_pDragingObject->ui_x = is_DragingOrgX;
		is_pDragingObject->ui_y = is_DragingOrgY;
		is_DragingObjectImage.Destroy();
		is_pDragingObject = NULL;

		RenderInteractiveLayer();
	}
}

void UI::plot::CInteractiveFigure::_CommitDraging()
{
	ASSERT(is_MouseDraging);

	::ReleaseCapture();
	is_MouseDraging = FALSE;
	is_pDragingObject->ui_x = is_DragingCurX;
	is_pDragingObject->ui_y = is_DragingCurY;
	is_DragingObjectImage.Destroy();
	is_pDragingObject = NULL;

	RenderInteractiveLayer();
}

void UI::plot::CInteractiveFigure::UpdateFinalView()
{
	if(m_FinalOutputDC)
	{
		m_InteractiveLayer.BitBlt(m_FinalOutputDC,m_AreaX,m_AreaY);

		if( is_MouseDraging )
		{
			is_DragingObjectImage.AlphaBlend(m_FinalOutputDC,m_AreaX+is_DragingCurX-1,m_AreaY+is_DragingCurY-1,100);
		}
		else
		{
			CGraphicsEx fig(m_FinalOutputDC);
			fig.SetSmoothingMode((Gdiplus::SmoothingMode)m_Config.AA_Mode);

			if( (m_InteractiveConfig.InteractiveMode & InteractiveMode_TracingMousePoint) && is_PointFoucsId >= 0)
			{
				m_Tools.LinePen.SetWidth(m_Config.PointSize*m_InteractiveConfig.PointFoucs_Thick);
				m_Tools.LinePen.SetColor(m_InteractiveConfig.PointFoucs_Color);

				fig.DrawEllipse(&m_Tools.LinePen,	is_PointFoucsX - m_Config.PointSize*0.6f,
													is_PointFoucsY - m_Config.PointSize*0.6f, 
													m_Config.PointSize*1.2f,m_Config.PointSize*1.2f);
			}
		}
	}
}

void UI::plot::CInteractiveFigure::SetPointTipFont(LPCTSTR Fontname,int size,int fontstyle)
{
	ASSERT(Fontname);

	_SafeDel(m_pPointTipFont);

	USES_CONVERSION;

	m_pPointTipFont = new Gdiplus::Font(T2CW(Fontname),size,fontstyle,Gdiplus::UnitPixel);
	ASSERT(m_pPointTipFont);
}

void UI::plot::CInteractiveFigure::_SeekPointTip(UINT id,std::vector<UI::plot::LPPointTip>::iterator& ptr)
{
	for(ptr = m_pPointTips.begin();ptr!=m_pPointTips.end();ptr++)
	{
		if((*ptr)->LinkedPointId == id)return;
	}
}


BOOL UI::plot::CInteractiveFigure::AddPointTip(UINT id)
{
	std::vector<LPPointTip>::iterator ptr;
	_SeekPointTip(id,ptr);
    
	if( ptr == m_pPointTips.end() )
	{
		float v[2];
		memcpy(v,&m_pPos[id],sizeof(v));

		m_Projection.TransformPoints((Gdiplus::PointF*)v);

		if( v[0] > m_AreaWidth/2 )v[0] -= m_InteractiveConfig.Tip_ImageWidth+2;
		if( v[1] > m_AreaHeight/2 )v[1] -= m_InteractiveConfig.Tip_ImageHeight+2;

		LPPointTip tip = new CPointTip(	id,v[0],v[1],
										m_InteractiveConfig.Tip_ImageWidth,
										m_InteractiveConfig.Tip_ImageHeight );
		ASSERT(tip);

		w32::Copy2d(tip->PtImgSpace,(float*)&m_pPos[id]);

		Dataset2Image2fv(tip->PtImgSpace);

		tip->PtImgSpace[0] -= m_AreaX;
		tip->PtImgSpace[1] -= m_AreaY;

		_LoadPointTipDesc(tip);
		m_pPointTips.push_back(tip);
		RenderInteractiveLayer();

		tip->ui_x = max(4,min(m_AreaWidth-tip->ui_Width-2,tip->ui_x));
		tip->ui_y = max(2,min(m_AreaHeight-tip->ui_Height-1,tip->ui_y));
		RenderInteractiveLayer();

		return TRUE;
	}
	else
	{
		return FALSE;
	}
}

void UI::plot::CInteractiveFigure::RemovePointTipByPointId(UINT id)
{
	std::vector<LPPointTip>::iterator ptr;
	_SeekPointTip(id,ptr);

	delete ((LPPointTip)(*ptr));
	m_pPointTips.erase(ptr);
}

void UI::plot::CInteractiveFigure::RemovePointTipByTipId(UINT id)
{
	delete ((LPPointTip)(m_pPointTips[id]));
	m_pPointTips.erase(m_pPointTips.begin()+id);
}

void UI::plot::CInteractiveFigure::RemoveAllPointTip()
{
	std::vector<LPPointTip>::iterator ptr;
	for(ptr = m_pPointTips.begin();ptr!=m_pPointTips.end();ptr++)
	{
		delete ((LPPointTip)(*ptr));
	}
	
	m_pPointTips.clear();
}

void UI::plot::CInteractiveFigure::_LoadPointTipDesc(UI::plot::LPPointTip tip)
{
	if(m_pRetrieveCallback)
	{
		if(m_pRetrieveCallback(tip,m_Cookie))
		{
			if(tip->m_Text.size())return;
		}
	}

	TCHAR buf[512];
	sprintf(buf,"(%.2f, %.2f)@%d",m_pPos[tip->LinkedPointId].x,m_pPos[tip->LinkedPointId].y,tip->LinkedPointId);
	tip->SetText(buf);
}

void UI::plot::CInteractiveFigure::ObjectHitTest(float x,float y,CUiObjectRef& ret)	//image space
{
	ret.pDragableObject = NULL;

	if( ( x > m_AreaX ) && ( y > m_AreaY ) &&
		( x - m_AreaX < m_AreaWidth ) && ( y - m_AreaY < m_AreaHeight ) )
	{
		x -= m_AreaX;
		y -= m_AreaY;

		////Search all point tips
		{
			std::vector<UI::plot::LPPointTip>::iterator p;
			for(p=m_pPointTips.begin();p!=m_pPointTips.end();p++)
			{
				LPPointTip tip = *p;
				float dx,dy;
				dx = x - tip->ui_x;
				dy = y - tip->ui_y;

				if( dx >=0 && dy >=0 && dx <=tip->ui_Width && dy <=tip->ui_Height )
				{
					// Hit !!
					ret.ObjectIndex = p - m_pPointTips.begin();
					ret.pDragableObject = tip;

					if(dy > tip->ui_ButtonY * 2)
					{
						ret.Type = tip->HasImage()?UiObject_TipImage:UiObject_TipOther;
					}
					else
					{
						if(dy < 2)
						{
							ret.Type = UiObject_TipOther;
						}
						else if(dy < tip->ui_ButtonY * 2 - 2)
						{
							if(dx < tip->ui_SwitchButtonX - tip->ui_ButtonSize*0.833333f)
							{
								ret.Type = UiObject_TipText;
							}
							else if(dx < tip->ui_SwitchButtonX - tip->ui_ButtonSize*0.5f)
							{
								ret.Type = UiObject_TipOther;
							}
							else if(dx < tip->ui_SwitchButtonX + tip->ui_ButtonSize*0.5f)
							{
								ret.Type = UiObject_TipSwitch;
							}
							else if(dx < tip->ui_CloseButtonX - tip->ui_ButtonSize*0.5f)
							{
								ret.Type = UiObject_TipOther;
							}
							else if(dx < tip->ui_CloseButtonX + tip->ui_ButtonSize*0.5f)
							{
								ret.Type = UiObject_TipClose;
							}
							else
							{
								ret.Type = UiObject_TipOther;
							}
						}
						else
						{
							ret.Type = UiObject_TipOther;
						}
					}

					return;
				}
			}
		}

		//Search all point
		{
			x += m_AreaX;
			y += m_AreaY;

			UINT id = ImagePointToDataPointId(x,y);
			ASSERT(id < m_PointCount );

			float p[2];
			memcpy(&p,&m_pPos[id],sizeof(p));

			Dataset2Image2fv(p);

			if( w32::Sqr(m_InteractiveConfig.PointFoucs_Range) > (w32::Sqr(x - p[0]) + w32::Sqr(y - p[1])) )
			{
				//Hit !!
				ret.Type = UiObject_DataPoint;
				ret.ObjectIndex = id;
				return;
			}
		}
	}

	ret.Type = UiObject_None;
}


