#pragma once

//////////////////////////////////////////////////////////////////////
// Visual Computing Foundation Classes (VCFC)
//					Jiaping Wang  2006.2
//					e_boris2002@hotmail.com
//
// (C) Copyright Jiaping Wang 2004.
//
// Permission to copy, use, modify, sell and distribute this software
// is granted provided this copyright notice appears in all copies.
// This software is provided "as is" without express or implied
// warranty, and with no claim as to its suitability for any purpose.
//
// Absolutly no garanty!!
//////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////
//  Brief Description
//
//  ConsoleView.h
//
//  A customized CEdit as an alternative for console
//
//////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////
// Revise History
//
// Inital version	2005.10		Jiaping
//
//////////////////////////////////////////////////////////////////////

#include <afxwin.h>         // MFC core and standard components

#include "..\w32\w32_basic.h"


// CConsoleView
namespace ui
{

class CConsoleView : public CEdit
{
	DECLARE_DYNAMIC(CConsoleView)

public:
	void Create(CWnd* pParent,UINT nCtrlId = 0x1001);
	CConsoleView();
	virtual ~CConsoleView();
	void RedirectOut();

protected:
	DECLARE_MESSAGE_MAP()


protected:
	CMenu	m_ShortCutMenu;

	HANDLE	m_hIOThread;
	HANDLE	m_hPipeRead;
	HANDLE	m_hPipeWrite;
	static  DWORD WINAPI IO_Thread_Func( LPVOID lpParameter );

	CFont	m_ConsoleFont;
	
	int		m_iTotalCharactors;
	w32::CCriticalSection	m_iTotalCharactorsCCS;
	
	BOOL	m_bOutputPaused;

public:
	afx_msg void OnDestroy();
	afx_msg void OnLButtonDblClk(UINT nFlags, CPoint point);
	afx_msg void OnRButtonUp(UINT nFlags, CPoint point);

protected:
	virtual BOOL OnCommand(WPARAM wParam, LPARAM lParam);
	virtual BOOL OnCmdMsg(UINT nID, int nCode, void* pExtra, AFX_CMDHANDLERINFO* pHandlerInfo);

public:
	afx_msg void OnKeyDown(UINT nChar, UINT nRepCnt, UINT nFlags);
};

} // namespace ui