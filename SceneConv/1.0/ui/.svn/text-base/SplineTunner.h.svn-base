#pragma once

//////////////////////////////////////////////////////////////////////
// Visual Computing Foundation Classes (VCFC)
//					Jiaping Wang  2006.2
//					e_boris2002@hotmail.com
//
// (C) Copyright Jiaping Wang 2004.
//
// Permission to copy, use, modify, sell and distribute this software
// is granted provided this copyright notice appears in all copies.
// This software is provided "as is" without express or implied
// warranty, and with no claim as to its suitability for any purpose.
//
// Absolutly no garanty!!
//////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////
//  Brief Description
//
//  SplineTunner.h
//
//  UI for tuning curves
//  
//
//////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////
// Revise History
//
// Inital version	2005.11.?		Jiaping
//
//////////////////////////////////////////////////////////////////////

#include "ui_base.h"
#include "..\num\small_vec.h"
#include "..\num\tabulate_func.h"
#include "..\w32\w32_basic.h"

// CCurveTuner

namespace ui
{


class CCurveTuner : public _meta_::_GdiplusControlRoot,
					public w32::CCriticalSection
{	
public:
	enum{ IDD = 1564 };
	enum
	{
		CurveMode_CardinalSpline = 0,  //Default
		CurveMode_LineSegment,
		CurveMode_BezierCurve,
		CurveMode_Max
	};
	enum
	{	NOTIFY_FUNCTION_CHANGED = NOTIFY_CUSTOM_BASE + 1  // WPARAM is the index of the changed function
	};
	enum
	{	STYLE_EDITABLE = 1		// not implemented yet !!
	};

protected:
	enum{ MAX_CONTROL_POINT = 50 };

public:
	class CCurve  //In [0,1] domain
	{	friend class CCurveTuner;
	private:
		::rt::Buffer<Gdiplus::PointF>	m_ControlPointScaled;
	protected:
		CCurveTuner*		m_pTuner;
		::rt::Buffer<Gdiplus::PointF>	m_ControlPoints;
		UINT				m_ControlPointCount;

		float				m_Origin_X;
		float				m_Origin_Y;
		float				m_Scale_X;
		float				m_Scale_Y;

		::num::Tabulated_Function<float>		m_Function;
	
	protected:
		void				FitInLayout(float & x,float & y)
		{	x = min(1.0f,max(0.0f,(x-m_Origin_X)*m_Scale_X));	
			y = min(1.0f,max(0.0f,(y-m_Origin_Y)*m_Scale_Y)); 	
		}
		void				ObtainFunction( ::rt::Buffer<float> & function_table, int Res = -1 );
		void				Plot(Gdiplus::Graphics & gfx, Gdiplus::Font* pFont,UINT_PTR hot_cp=NULL,BOOL Selected = FALSE); 

	public:
		BOOL				m_Locked;
		BOOL				m_Hidden;
		BOOL				m_FixedXValue;
		BOOL				m_FixedYValue;
		BOOL				m_DisableControlPointAdding;

		UINT				m_Id;
		WCHAR				m_Name[256];
		::num::Vec4b		m_LineColor;
		float				m_LineWidth;
		float				m_OutputValueScale;
		DWORD				m_CurveMode;

	public:
		CCurve();
		UINT_PTR			AddControlPoint(float x,float y); //return CP-Handle or NULL for denied
		void				UpdateControlPoint(UINT_PTR cp_handle,float x,float y);
		void				RemoveControlPoint(UINT_PTR cp_handle);
		UINT_PTR			HitControlPoint(float x,float y,float distance_max); //return hitted CP or NULL for non-hitted
		void				SetLayout(float x1,float y1,float x2,float y2);
		float				ProposeNewControlPointPostion( float x );
		void				GetControlPointPosition( UINT_PTR cp_handle, float & x,float & y);

		UINT_PTR			GetNextControlPoint(UINT_PTR = NULL);
		UINT_PTR			GetPrevControlPoint(UINT_PTR = NULL);
		void				SetCurveMode(int mode = CurveMode_CardinalSpline);

		UINT				GetControlPointCount(){ return m_ControlPointCount; }  //co*2 floats (x1,y1,x2,y2,x3 ... )
		const float*		GetControlPointBuffer(){ return (const float*)m_ControlPoints.Begin(); } //size is co*2*sizeof(float) (x1,y1,x2,y2,x3 ... )
		BOOL 				SetControlPointBuffer(const float* pControlPoints, int co_cp);
		void				GetFunctionTable(::rt::Buffer<float> & func);
		float				GetCurveFunctionValue(float x);  // 0<=x<=1

		void				PlotPositiveFunction(const float* pFunc, UINT len, UINT stride_of_float = 1, float scale = 1.0f, BOOL Redraw = TRUE);

		BOOL				Load(LPCTSTR fn);
		BOOL				Save(LPCTSTR fn);
	};

public:
	
	CCurveTuner(_meta_::_GdiplusControlHost* pHost);

protected:

	//UI elements
	HCURSOR					m_hMove;
	HCURSOR					m_hAddNew;


	//Curve Data
	::rt::Buffer<CCurve>	m_Curves;
	UINT_PTR				m_CurrentControlPoint;
	int						m_CurrentCurve;
	int						m_HitCurve;
	::num::Vec2f			m_ProposalPoint;

	void					RedrawContent();

protected:
	virtual LRESULT MessageProc(UINT uMsg, WPARAM wParam, LPARAM lParam);

	BOOL    HitControlPoint(POINT & pt, BOOL IsForButtonDown = FALSE);
	BOOL    HitCurve(POINT & pt, BOOL IsForButtonDown = FALSE);

public:
	void	SetCurveCount(UINT co, BOOL redraw = TRUE);
	CCurve&	GetCurve(UINT id){ return m_Curves[id]; }		//Use EnterCCSBlock(CCurveTuner_Object) before curve accessing in multithread env.
	UINT	GetCurveCount(){ return (UINT)m_Curves.GetSize(); }

	void	PlotPositiveFunction(const float* pData, UINT chan, UINT len, float scale = 1.0f, UINT curve_index_start = 0);	// Total number of float is chan*len, the data mapped to (curve_index_start,curve_index_start+chan)
};


}
