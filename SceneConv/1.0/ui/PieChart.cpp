#include "stdafx.h"
#include "PieChart.h"
#include "..\rt\expr_templ.h"

using namespace num;
using namespace Gdiplus;

///////////////////////////////////
// PieChart
ui::CPieChart::CPieChart(_meta_::_GdiplusControlHost* pHost)
	:_meta_::_GdiplusControlRoot(pHost)
{	
	m_FillAlpha = 70;
	m_FillAlpha_Selected = 80;
	m_StrokeAlpha = 160;
}

void ui::CPieChart::RedrawContent()
{
	if(!IsUILocked()){}else{ return; }

	Graphics* pGfx = NULL;
	if(pGfx = m_pControlHost->GetGraphics())
	{
		RECT rc;
		m_pControlHost->GetRedrawRect(&rc);
		Graphics& gfx(*pGfx);

		RedrawBackground(gfx,rc,FALSE);

		if(m_Pieces.GetSize())
		{
			Gdiplus::Pen stroke(&Gdiplus::SolidBrush(Gdiplus::Color::Black),2.5f);
			Gdiplus::Pen stroke_black(&Gdiplus::SolidBrush(Gdiplus::Color::MakeARGB(m_StrokeAlpha,0,0,0)),1.0f);
			Gdiplus::SolidBrush fill(Gdiplus::Color::Black);
			Gdiplus::SolidBrush text(Gdiplus::Color::Black);
			gfx.SetSmoothingMode( Gdiplus::SmoothingModeAntiAlias );
			gfx.SetTextRenderingHint( Gdiplus::TextRenderingHintAntiAlias );

			rc.top += 12;
			rc.left += 12; 
			rc.bottom -= 12;
			rc.right -= 12;

			int sz = (int)(min(rc.bottom - rc.top,rc.right - rc.left));
			int pad = (int)((rc.bottom - sz - rc.top)/2 + 0.5f);
			rc.top += pad;
			rc.bottom -= pad;
			pad = (int)((rc.right - sz - rc.left)/2 + 0.5f);
			rc.left += pad;
			rc.right -= pad;

			rt::Buffer<Piece>& _pie = m_Pieces;

			m_PieCenter = num::Vec2i(sz/2 + rc.left,sz/2 + rc.top);
			m_PieRadius = sz/2;
	
			float angle = 0;
			for(UINT i=0;i<_pie.GetSize();i++)
			{
				float dx = 0;
				float dy = 0;

				float sweep = _pie[i].Precentage*360.0f;
				float ang = angle + sweep/2;

				float x = (float)(cos(ang*PI/180)*sz/3 + m_PieCenter.x);
				float y = (float)(sin(ang*PI/180)*sz/3 + m_PieCenter.y);

				if(i == m_Focused)
				{
					dx = (float)cos(ang*PI/180) * 6;
					dy = (float)sin(ang*PI/180) * 6;
					fill.SetColor(GetStockColor(i,m_FillAlpha_Selected));
				}
				else
				{
					dx = (float)cos(ang*PI/180) * 0.1f;
					dy = (float)sin(ang*PI/180) * 0.1f;
					fill.SetColor(GetStockColor(i,m_FillAlpha));
				}

				stroke.SetColor(GetStockColor(i,m_StrokeAlpha));

				gfx.FillPie(&fill, rc.left + dx, rc.top + dy, (float)sz, (float)sz, angle, sweep);
				if(i == m_Focused)
					gfx.DrawPie(&stroke, rc.left + dx, rc.top + dy, (float)sz, (float)sz, angle, sweep);
				else 
					gfx.DrawPie(&stroke_black, rc.left + dx, rc.top + dy, (float)sz, (float)sz, angle, sweep);
				
				Gdiplus::RectF rc;
				ATL::CT2W w_str(_pie[i].Label.Begin());
				gfx.MeasureString(w_str,(INT)_pie[i].Label.GetLength(),GetFont(),Gdiplus::PointF(0,0),&rc);

				if(sweep < 90 && sin(sweep/2*PI/180)/5*PI*sz < rc.Width)
				{
					if(sin(sweep/2*PI/180)/3*PI*sz > rc.Height)
					{
						float deg = (float)(atan2(dy,dx)*180/PI);
						if(deg > 90)deg -= 180;
						if(deg <-90)deg += 180;

						Gdiplus::Matrix org;
						gfx.GetTransform(&org);

						Gdiplus::Matrix rot;
						gfx.GetTransform(&rot);
						rot.RotateAt(deg,Gdiplus::PointF(x + dx,y + dy));
						gfx.SetTransform(&rot);

						x -= rc.Width/2;
						y -= rc.Height/2;

						gfx.DrawString(w_str,(INT)_pie[i].Label.GetLength(),GetFont(),Gdiplus::PointF(x + dx,y + dy),&text);
						gfx.SetTransform(&org);
					}
				}
				else
				{
					x -= rc.Width/2;
					y -= rc.Height/2;

					gfx.DrawString(w_str,(INT)_pie[i].Label.GetLength(),GetFont(),Gdiplus::PointF(x + dx,y + dy),&text);
				}


				angle += sweep;
			}
			gfx.Flush(FlushIntentionSync);
			SendNotify(NOTIFY_GRAPHICS_UPDATED);
		}
	}
}


LRESULT ui::CPieChart::MessageProc(UINT uMsg, WPARAM wParam, LPARAM lParam)
{	
	if(!IsUILocked()){}
	else{ return __super::MessageProc(uMsg,wParam,lParam); }

	switch(uMsg)
	{
	case WM_SIZE:
		RedrawContent();
		break;
	case WM_MOUSEMOVE:
		if(wParam & MK_LBUTTON){}
		else{ break; }
	case WM_LBUTTONDOWN:
		{
			USING_EXPRESSION;

			int focus;
			num::Vec2d hit = (((num::Vec2s&)lParam) - m_PieCenter)/m_PieRadius;

			if(rt::IsInRange_CC(hit.L2Norm_Sqr(),0.05,0.95))
			{
				rt::Buffer<Piece>& _pie = m_Pieces;
				float precent = (float)(atan2(hit.y,hit.x)/(2*PI));
				if(precent < 0)precent += 1;

				UINT i=0;
				float tot = 0;
				for(;i<_pie.GetSize();i++)
				{	
					tot += _pie[i].Precentage;
					if(precent < tot)
						break;
				}
				focus = i;
			}
			else
				focus = -1;

			if(focus != m_Focused)
			{
				m_Focused = focus;
				RedrawContent();
				SendNotify(NOTIFY_SELECT_CHANGED);
			}
		}
		break;
	}

	return __super::MessageProc(uMsg,wParam,lParam);
}

