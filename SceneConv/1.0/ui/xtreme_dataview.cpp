#include "stdafx.h"
#include "xtreme_dataview.h"
#include <map>
#include <queue>

namespace ui{namespace xt{namespace _meta_{


typedef std::map<LPCVOID,_PropertyGridItemBase*> t_BindedVariableMap;
t_BindedVariableMap	g_theBindedVariables;

void _BindedVariableMap::AddItem(LPCVOID pVariable, _PropertyGridItemBase* pItem)
{
	t_BindedVariableMap::_Pairib ret = 
		g_theBindedVariables.insert(std::pair<LPCVOID,_PropertyGridItemBase*>(pVariable,pItem));
	ASSERT(ret.second); // duplicated pVariable
}

void _BindedVariableMap::RemoveItem(LPCVOID pVariable)
{	
	g_theBindedVariables.erase(pVariable);
}

_PropertyGridItemBase* _BindedVariableMap::QueryItem(LPCVOID pVariable)
{
	t_BindedVariableMap::iterator p =
		g_theBindedVariables.find(pVariable);

	if(p!=g_theBindedVariables.end())
		return p->second;
	else
		return NULL;
}

}}} // namespace _meta_/xt/ui

void uix::UpdateVariableAll()
{
	for (_meta_::t_BindedVariableMap::iterator p = _meta_::g_theBindedVariables.begin();
		p != _meta_::g_theBindedVariables.end(); p ++)
		if(p->second) p->second->UpdateDataToUI();
}

void uix::_PropertyGridItemBase::RemoveAllChild()
{	
	CXTPPropertyGridItems* p = GetChilds();
	p->Clear();
}

void uix::_PropertyGridItemBase::Remove()
{
	CXTPPropertyGridItem* p = GetParentItem();
	ASSERT(p);
	int i = p->GetChilds()->Find(this);
	ASSERT(i>=0 && i<p->GetChilds()->GetCount());
	p->GetChilds()->RemoveAt(i);
}

void uix::_PropertyGridItemBase::SendChangingNotification(LPVOID pObj, UINT size)
{	
	if(m_NotifyWnd)
	{
		PropertyGridChangeNotification	p;
		CWnd* GridWnd = GetGrid()->GetParent();
		ASSERT(GridWnd);
		p.hwndFrom = GridWnd->GetSafeHwnd();
		p.idFrom = GridWnd->GetDlgCtrlID();
		p.pVariable = pObj;
		p.pName = m_strCaption;
		p.VariableLen = size;
		::SendMessage(m_NotifyWnd,WM_NOTIFY,(WPARAM)p.idFrom,(LPARAM)(&p));
	}
}

HWND					uix::_PropertyGridItemBase::g_DefaultNotifyWnd = NULL;
CXTPPropertyGridItem*	uix::_PropertyGridItemBase::g_DefaultItemRoot = NULL;
BOOL					uix::_PropertyGridItemBase::g_NotifyDirectlyBindingOnly = TRUE;


void uix::SetVariableBindingTarget(CXTPPropertyGrid& GridCtrl, LPCTSTR CategoryName)
{
	CXTPPropertyGridItems* p = GridCtrl.GetCategories();
	CXTPPropertyGridItem*  item = p->FindItem(CString(CategoryName));
	if(item)
		SetVariableBindingTarget(item);
	else
		SetVariableBindingTarget(GridCtrl.AddCategory(CategoryName));
}

void uix::SetVariableBindingTarget(CXTPPropertyGridItem* p)
{	ASSERT(p);
	uix::_PropertyGridItemBase::g_DefaultItemRoot = p;
	p->Expand();
}

CXTPPropertyGridItem* uix::GetVariableBindingTarget()
{
	return uix::_PropertyGridItemBase::g_DefaultItemRoot;
}


void uix::SetVariableChangedFeedback(HWND hNotification, BOOL NotifyDirectlyBindingOnly)
{
	uix::_PropertyGridItemBase::g_DefaultNotifyWnd = hNotification;
	uix::_PropertyGridItemBase::g_NotifyDirectlyBindingOnly = NotifyDirectlyBindingOnly;
}

BOOL uix::BindVariable_SaveAll(LPCTSTR fn)
{
	std::queue<_PropertyGridItemBase*>	item_q;

	{	_meta_::t_BindedVariableMap::iterator p = _meta_::g_theBindedVariables.begin();
		for(;p!=_meta_::g_theBindedVariables.end();p++)
			item_q.push(p->second);
	}

	while(item_q.size())
	{
		_PropertyGridItemBase* item = item_q.front();
		item_q.pop();

		ASSERT(item);
		if(item->IsEditable())
		{	item->UpdateDataToUI();
			item->UpdateDataToUI();
			if(WritePrivateProfileString(_meta_::_text_sectionname(),item->GetName(),item->GetString(),fn)){}
			else return FALSE;
		}
		else
		{	CXTPPropertyGridItems& pc = *item->GetChilds();
			for(UINT i=0;i<(UINT)pc.GetCount();i++)
				item_q.push((_PropertyGridItemBase*)pc.GetAt(i));
		}
	}

	return TRUE;
}

BOOL uix::BindVariable_LoadAll(LPCTSTR fn)
{
	std::queue<_PropertyGridItemBase*>	item_q;

	{	_meta_::t_BindedVariableMap::iterator p = _meta_::g_theBindedVariables.begin();
		for(;p!=_meta_::g_theBindedVariables.end();p++)
			item_q.push(p->second);
	}

	while(item_q.size())
	{
		_PropertyGridItemBase* item = item_q.front();
		item_q.pop();

		ASSERT(item);
		if(item->IsEditable())
		{
			TCHAR text[1025];
			if(GetPrivateProfileString(_meta_::_text_sectionname(),item->GetName(),item->GetString(),text,1024,fn))
			{
				item->SetString(text);
			}
		}
		else
		{	CXTPPropertyGridItems& pc = *item->GetChilds();
			for(UINT i=0;i<(UINT)pc.GetCount();i++)
				item_q.push((_PropertyGridItemBase*)pc.GetAt(i));
		}
	}

	return TRUE;
}

