#pragma once

//////////////////////////////////////////////////////////////////////
// Visual Computing Foundation Classes (VCFC)
//					Jiaping Wang  2006.2
//					e_boris2002@hotmail.com
//
// (C) Copyright Jiaping Wang 2004.
//
// Permission to copy, use, modify, sell and distribute this software
// is granted provided this copyright notice appears in all copies.
// This software is provided "as is" without express or implied
// warranty, and with no claim as to its suitability for any purpose.
//
// Absolutly no garanty!!
//////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////
//  Brief Description
//
//  xtreme_ui.h
//
//  helper classes for xtreme ui library
//
//  uix::g_EnableFrameLayoutPersistent  to enable saving/loading layout information (enabled by default)
//  uix::g_EnableLayoutCustomizeDialog  to enable customize option dailog (disabled by default)
//	uix::g_EnableSplittingUpdate		to enable UI redraw when moving splitter  (enabled by default)
//  uix::g_EnableAlphaDockingIndicator	to enable transparent docking indicactor  (enabled by default)
//  uix::g_EnableDockingStickers		to enable docking stickers (disabled by default)
//
//////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////
// Revise History
//
// Inital version	2007.10		Jiaping
//
//////////////////////////////////////////////////////////////////////

#include <afxwin.h>         // MFC core and standard components


//#define _XTP_EXCLUDE_COMMANDBARS
#define _XTP_STATICLINK			// You must statically link to XTToolkitPro
#define _XTREME_UI_INCLUDED_

#ifdef __VSSYM32_H__			// XTToolkitPro.h includes tmschema.h that is conflict with vssym32.h in VS2008
	#define TMSCHEMA_H
#endif

//////////////////////////////////////////////////////////
// Add the follow line to %project.rc2% in %project%/res
// #include "XTToolkitPro.rc"
//////////////////////////////////////////////////////////

/////////////////////////////////////////////////////////////////////////////////////
// disable some error and warning, since the XTToolkitPro is ported from VS2003
// C4819: The file contains a character that cannot be represented in the current code page
// C4430: missing type specifier - int assumed
#pragma warning(disable:4819 4430) 
#include <XTToolkitPro.h>
#pragma warning(default:4819 4430)

#ifndef _XTP_INCLUDE_COMMANDBARS
	#error "Do not define _XTP_INCLUDE_COMMANDBARS"
#endif

#define uix		ui::xt

namespace ui
{
namespace xt
{

namespace _FrameWnd
{
	class _XtremeFrameWnd;
	class _XtremeMDIFrameWnd;
} // namespace _FrameWnd


enum _tagHostingMode
{
	HostingMode_None = 0,
	HostingMode_Fixed = 0,
	HostingMode_FreeMoving = 0x3,
	HostingMode_HorizontalMoving = 0x1,
	HostingMode_VerticalMoving = 0x2,
	HostingMode_Max,

	DockPose_Top = xtpPaneDockTop,
	DockPose_Bottom = xtpPaneDockBottom,
	DockPose_Left = xtpPaneDockLeft,
	DockPose_Right = xtpPaneDockRight
};

class CDockingPane;

extern BOOL g_EnableFrameLayoutPersistent;
extern BOOL g_EnableLayoutCustomizeDialog;
extern BOOL g_EnableSplittingUpdate;
extern BOOL g_EnableAlphaDockingIndicator;
extern BOOL g_EnableDockingStickers;

namespace _meta_
{	

struct _PanelHostWnd:public CWnd
{
	CXTPDockingPaneManager* m_pBindedPanelManager;
	UINT	m_pBindedPanelCtrlId;
	HWND	m_hHostedWnd;
	WNDPROC	m_pHostedWndProc;
	DWORD	m_HostingBehavior;
	UINT	m_HostedWndWidth;
	UINT	m_HostedWndHeight;

	static LRESULT CALLBACK WndProc_SubclassHostee(HWND hwnd, UINT message, WPARAM wParam, LPARAM lParam);
	_PanelHostWnd();
	void _PositioningHostedWnd(int cx, int cy);
	void _FixHostedWindowArea(CRect& hw);
	virtual LRESULT WindowProc(UINT message, WPARAM wParam, LPARAM lParam);
	void SetClientWindow(HWND hSubWnd, DWORD HostingMode=uix::HostingMode_Fixed);
};

class _XtremeFrameWndHook
{	
	friend class ::uix::_FrameWnd::_XtremeFrameWnd;
	friend class ::uix::_FrameWnd::_XtremeMDIFrameWnd;

	CFrameWnd*			m_pWnd;
	CXTPCommandBars*	_pCommandBars;
protected:
	static const LPCTSTR g_PanelLayout;
	enum
	{	ID_PANEL_BASE = 100,
		ID_DYNPANEL_ADD = 100,
	};
	CXTPDockingPaneManager*	m_pDockingPaneManager;
	CXTPStatusBar*			m_pStatusBar;
	UINT					m_NextDynPanelId;
	UINT					m_MenuResourceId;
	UINT					m_PanelMenuIdBase;
	UINT					m_PanelMenuIdMax;

	void 	_CreatePanelHost(CXTPDockingPane* panel);
	void 	_EnableDockingManager();
	void 	_SetupPanelMenu();
	BOOL	OnCmdMsg(UINT nID, int nCode, void* pExtra, AFX_CMDHANDLERINFO* pHandlerInfo);
	LRESULT WindowProc(UINT message, WPARAM wParam, LPARAM lParam);
	void	SetCommandBarsObject(CXTPCommandBars* p);

public:
	_XtremeFrameWndHook(CFrameWnd* pWnd);
	~_XtremeFrameWndHook();
	CXTPCommandBars*		GetCommandBars();
	CXTPDockingPaneManager*	GetPaneManager();
	uix::CDockingPane*		GetPanel(UINT id);

	void DestoryDynamicPanel(UINT panel_id);
	UINT CreateDynamicPanel(LPCTSTR title);	// return panel_id
	BOOL CreatePanels(const LPCTSTR* pPanelTitle, UINT panel_count, const SIZE* pPanelSizes = NULL, UINT menu_id_base = 40000); // also clear ids
	
	void ShowPanel(UINT id, BOOL bShow = TRUE);
	BOOL IsPanelShown(UINT id);
	void DockPanel(UINT id, DWORD pose = DockPose_Left, UINT relative_panel_id = INFINITE);
	void AttachPanel(UINT id, UINT base_id);

	void DisableDragInDockContext(BOOL disable);
	BOOL SetMenuBar(UINT menu_resource);
	// if undeclared identifier reported, include "resource.h" before 2.1\ui\xtreme_dataview.h or 2.1\ui\xtreme_ui.h"
	CXTPToolBar* SetToolBar(LPCTSTR title, UINT toolbar_resource);
	void SetStatusBarIndirector(const UINT* lpIDArray, int nIDCount);
	void SetStatusPaneInfo(DWORD cmd_id, int width, UINT style = SBPS_NORMAL);
	void SetStatusPaneText(DWORD cmd_id, LPCTSTR str, BOOL auto_resize_pane = FALSE);
	void HideView(BOOL hide = TRUE);
	void FixChildWindowEdge(CWnd* pWnd);
};

} // namespace _meta_


class CDockingPane:public CXTPDockingPane
{private:
	void Attach(CWnd* pWnd);	// do not call this function, call SetPanelClientWindow instead
	CWnd* AttachView(CWnd*, CRuntimeClass *, CDocument *, CCreateContext *);
	CDockingPane();  //  Panel Object is retrieved by get_uix()->GetPanel(.);
public:
	void SetClientWindow(HWND hSubWnd, DWORD HostingMode=uix::HostingMode_Fixed);
	void SetTitle(LPCTSTR title = NULL); // NULL to follow child's title
};


namespace _FrameWnd
{

class _XtremeFrameWnd:public CXTPFrameWnd
{
	::uix::_meta_::_XtremeFrameWndHook	_XtremeHook;
public:
	DECLARE_DYNCREATE(CXtremeFrameWnd);
	#pragma warning(disable:4355)
	_XtremeFrameWnd():_XtremeHook(this){}
	#pragma warning(default:4355)
protected:
	::uix::_meta_::_XtremeFrameWndHook* GetUIX(){ return &_XtremeHook; }
	::uix::_meta_::_XtremeFrameWndHook* get_uix(){ return &_XtremeHook; }
	virtual BOOL OnCmdMsg(UINT nID, int nCode, void* pExtra, AFX_CMDHANDLERINFO* pHandlerInfo)
	{	return _XtremeHook.OnCmdMsg(nID,nCode,pExtra,pHandlerInfo)?TRUE:(__super::OnCmdMsg(nID,nCode,pExtra,pHandlerInfo)); }
	virtual LRESULT WindowProc(UINT message, WPARAM wParam, LPARAM lParam)
	{	if(message == WM_CREATE)
		{	VERIFY(InitCommandBars());
			_XtremeHook.SetCommandBarsObject(GetCommandBars());
			LRESULT ret = __super::WindowProc(message,wParam,lParam);
			if(ret!=-1)_XtremeHook.WindowProc(WM_CREATE,wParam,lParam);
			return ret;
		}		
		_XtremeHook.WindowProc(message,wParam,lParam);
		return __super::WindowProc(message,wParam,lParam);
	}
};

class _XtremeMDIFrameWnd:public CXTPMDIFrameWnd
{
	DECLARE_DYNCREATE(CXtremeMDIFrameWnd);
};

} // namespace _FrameWnd

} // namespace xt
} // namespace ui


#define CFrameWnd		CXtremeFrameWnd
#define CMDIFrameWnd	CXtremeMDIFrameWnd

class CXtremeFrameWnd:public ::uix::_FrameWnd::_XtremeFrameWnd{};
class CXtremeMDIFrameWnd:public ::uix::_FrameWnd::_XtremeMDIFrameWnd{};

