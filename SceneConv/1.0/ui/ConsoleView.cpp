// ConsoleView.cpp : implementation file
//

#include "stdafx.h"
#include ".\consoleview.h"
#include "..\w32\file_64.h"
#include <errno.h>
#include <fcntl.h>
#include <Wincon.h>
#include <io.h>

using namespace std;

namespace ui
{

// CConsoleView

IMPLEMENT_DYNAMIC(CConsoleView, CEdit)
CConsoleView::CConsoleView()
{
	VERIFY(::CreatePipe(&m_hPipeRead,&m_hPipeWrite,NULL,1048576)); //1M buffer

	// Redirect standard out
	RedirectOut();

	m_hIOThread = NULL;

	m_iTotalCharactors = -1;
	m_bOutputPaused = FALSE;
}

CConsoleView::~CConsoleView()
{
	::CloseHandle(m_hPipeRead);
	::CloseHandle(m_hPipeWrite);
}

BEGIN_MESSAGE_MAP(CConsoleView, CEdit)
	ON_WM_DESTROY()
	ON_WM_LBUTTONDBLCLK()
//	ON_CONTROL_REFLECT(EN_MAXTEXT, OnEnMaxtext)
ON_WM_RBUTTONUP()
ON_WM_KEYDOWN()
END_MESSAGE_MAP()

void CConsoleView::RedirectOut()
{
	SetStdHandle(STD_OUTPUT_HANDLE,m_hPipeWrite);
	SetStdHandle(STD_ERROR_HANDLE,m_hPipeWrite);

	FILE   *fp;
	int  hConHandle;

	// redirect unbuffered STDOUT to the console
	hConHandle = _open_osfhandle((intptr_t)m_hPipeWrite, _O_TEXT);
	fp = _fdopen( hConHandle, "w" );
	*stdout = *fp;
	setvbuf( stdout, NULL, _IONBF, 0 ); 
	// redirect unbuffered STDERR to the console
	hConHandle = _open_osfhandle((intptr_t)m_hPipeWrite, _O_TEXT);
	fp = _fdopen( hConHandle, "w" );
	*stderr = *fp;
	setvbuf( stderr, NULL, _IONBF, 0 );

	// make cout, wcout, cin, wcin, wcerr, cerr, wclog and clog point to console as well
	ios::sync_with_stdio();
}

void CConsoleView::Create(CWnd* pParent,UINT nCtrlId)
{	
	ASSERT( m_hIOThread == NULL);

	VERIFY(__super::Create(	WS_CHILD|WS_VISIBLE|WS_VSCROLL|ES_LEFT|ES_MULTILINE|ES_NOHIDESEL|ES_READONLY|ES_AUTOVSCROLL,
								CRect(0,0,100,100),	pParent,nCtrlId));

	ModifyStyleEx(NULL,WS_EX_STATICEDGE);
	m_ConsoleFont.CreateFont(-12,0,0,0,400,0,0,0,0x0,0x3,0x2,0x1,0x31,_T("Lucida Console"));
	SetFont(&m_ConsoleFont);

	SetLimitText(0x7FFFFFFE);

	VERIFY(m_ShortCutMenu.CreatePopupMenu());
	m_ShortCutMenu.InsertMenu(0,MF_BYPOSITION|MF_STRING,1001,_T("&Copy"));
	m_ShortCutMenu.InsertMenu(1,MF_BYPOSITION|MF_STRING,1005,_T("Select &All"));
	m_ShortCutMenu.InsertMenu(2,MF_BYPOSITION|MF_STRING,MF_SEPARATOR);
	m_ShortCutMenu.InsertMenu(3,MF_BYPOSITION|MF_STRING,1002,_T("&Save"));
	m_ShortCutMenu.InsertMenu(4,MF_BYPOSITION|MF_STRING,1003,_T("&Pause"));
	m_ShortCutMenu.InsertMenu(5,MF_BYPOSITION|MF_STRING,MF_SEPARATOR);
	m_ShortCutMenu.InsertMenu(6,MF_BYPOSITION|MF_STRING,1004,_T("Clea&r"));

	// Create I/O thread
	m_iTotalCharactors = 0;
	m_hIOThread = ::CreateThread(NULL,0,CConsoleView::IO_Thread_Func,this,0,NULL);
	::SetThreadPriority(m_hIOThread,THREAD_PRIORITY_HIGHEST);
}

void CConsoleView::OnRButtonUp(UINT nFlags, CPoint point)
{
#ifndef _XTREME_UI_INCLUDED_
	m_ShortCutMenu.CheckMenuItem(1003,MF_BYCOMMAND|(m_bOutputPaused?MF_CHECKED:MF_UNCHECKED));
	int s,e;
	GetSel(s,e);
	m_ShortCutMenu.EnableMenuItem(1001,MF_BYCOMMAND|(s!=e)?MF_ENABLED:(MF_GRAYED|MF_DISABLED));
	m_ShortCutMenu.EnableMenuItem(1002,MF_BYCOMMAND|GetWindowTextLength()?MF_ENABLED:(MF_GRAYED|MF_DISABLED));
#endif

	ClientToScreen(&point);

#ifdef _XTREME_UI_INCLUDED_
     CFrameWnd* pMainWnd = DYNAMIC_DOWNCAST(CFrameWnd, AfxGetMainWnd());
     if (pMainWnd)
	 {	 CXTPCommandBars* pCommandBars = pMainWnd->GetCommandBars();
		 if(pCommandBars)
		 {	CXTPCommandBars::TrackPopupMenu(&m_ShortCutMenu, TPM_LEFTALIGN|TPM_RIGHTBUTTON,	point.x, point.y, this, 0, 0);
			return;
		 }
	 }
#endif

	VERIFY(m_ShortCutMenu.TrackPopupMenu(TPM_LEFTALIGN|TPM_RIGHTBUTTON,point.x,point.y,this));
}

BOOL CConsoleView::OnCmdMsg(UINT nID, int nCode, void* pExtra, AFX_CMDHANDLERINFO* pHandlerInfo)
{	
#ifdef _XTREME_UI_INCLUDED_
	CCmdUI* pCmdUI = (CCmdUI*)pExtra;
	if(nCode==CN_UPDATE_COMMAND_UI && pCmdUI)
	{
		switch(nID)
		{
		case 1001:
			{	int s,e;
				GetSel(s,e);
				pCmdUI->Enable(s!=e);
			}
			break;
		case 1002:
			pCmdUI->Enable(GetWindowTextLength());
			break;
		case 1003:
			pCmdUI->Enable();
			pCmdUI->SetCheck(m_bOutputPaused);
			break;
		case 1004:
			pCmdUI->Enable();
			break;
		case 1005:
			pCmdUI->Enable();
			break;
		}
		return TRUE;
	}
#endif

	return __super::OnCmdMsg(nID,nCode,pExtra,pHandlerInfo);
}

BOOL CConsoleView::OnCommand(WPARAM wParam, LPARAM lParam)
{
	if(wParam>=1001 && wParam<=1005)
	{
		switch(wParam)
		{
		case 1001:
			Copy();
			break;
		case 1002:
			{	USE_FILE_DIALOG(GetSafeHwnd(),_T("Text Files (*.txt)\0*.txt\0All Files (*.*)\0*.*\0"));
				LPTSTR fn = GET_FILENAME(FALSE);
				if(fn)
				{	APPEND_EXTNAME(fn,_T(".txt"));

					w32::CFile64_Text	text;
					if(text.Open(fn,w32::CFile64::Normal_Write))
					{
						{	SYSTEMTIME ti;
							GetLocalTime(&ti);

							char fn[MAX_PATH+1];
							fn[0] = 0;
							::GetModuleFileNameA(NULL,fn,MAX_PATH);

							char timestamp[200];
							sprintf(timestamp,"Debug Logged at %02d:%02d %d/%d/%d\nModule Image: %s\n\n",
									ti.wHour,ti.wMinute,ti.wDay,ti.wMonth,ti.wYear,fn);
							text.WriteString(timestamp);
						}
						CString str;
						GetWindowText(str);
					#ifdef UNICODE
						CString str_a(str);
						text.WriteString(str_a);
					#else
						text.WriteString(str);
					#endif
					}else ::MessageBox(GetSafeHwnd(),_T("Failed to create the text file"),_T("Error"),MB_ICONSTOP|MB_OK);
				}
			}
			break;
		case 1003:
			OnLButtonDblClk(0,CPoint(0,0));
			break;
		case 1004:
			SetWindowText(_T(""));
			break;
		case 1005:
			SetSel(0,-1);
			break;
		}
	}

	return CEdit::OnCommand(wParam, lParam);
}

void ui::CConsoleView::OnKeyDown(UINT nChar, UINT nRepCnt, UINT nFlags)
{
	if(nChar == 'A' && GetAsyncKeyState(VK_CONTROL)<0)
		SetSel(0,-1);
	else
		CEdit::OnKeyDown(nChar, nRepCnt, nFlags);
}

///////////////////////////////////////////////////////////////////
// This function is trying to handle \r, but has some bugs :(
//
//DWORD WINAPI CConsoleView::IO_Thread_Func( LPVOID lpParameter )
//{
//	CConsoleView	* pEdit = (CConsoleView*)lpParameter;
//	char input[512];
//	input[1] = 0;
//	DWORD Len;
//	int TotalCharCommitted = 0;
//	BOOL ClearLine = FALSE;
//
//	for(;;)
//	{
//		if(::ReadFile(pEdit->m_hPipeRead,&input,1,&Len,NULL))
//		{
//			Sleep(10);
//
//			{
//				DWORD Total;
//				PeekNamedPipe(pEdit->m_hPipeRead,NULL,0,NULL,&Total,NULL);
//
//				if(Total)
//				{
//					VERIFY(::ReadFile(pEdit->m_hPipeRead,&input[1],510,&Len,NULL));
//					input[Len+1] = 0;
//				}
//				else
//				{
//					input[1] = 0;
//				}
//			}
//
//			// parsing content
//			{EnterCCSBlock(pEdit->m_iTotalCharactorsCCS);
//
//				// Detect text overflow
//				if( pEdit->m_iTotalCharactors + strlen(input) >= pEdit->GetLimitText() )
//				{	TotalCharCommitted = 0;
//					ClearLine = FALSE;
//					pEdit->m_iTotalCharactors = 0;
//					pEdit->SetWindowText("");
//				}
//
//				// append text
//				LPTSTR pLine = input;
//				LPTSTR pHead = input;
//				for(;pLine[0];pLine++)
//				{
//					if( pLine[0] == '\r' )
//					{
//						// Move cursor
//						if(ClearLine)
//						{	ClearLine = FALSE;
//							pEdit->m_iTotalCharactors = TotalCharCommitted;
//							pEdit->SetSel(max(0,pEdit->m_iTotalCharactors),-1);
//						}
//						else
//							pEdit->SetSel(pEdit->m_iTotalCharactors,-1);
//
//						//output
//						if( pLine[1] == '\n' )
//						{
//							//commit
//							pEdit->m_iTotalCharactors += (int)(pLine - pHead + 2);
//							TotalCharCommitted = pEdit->m_iTotalCharactors;
//							char tt = pLine[2];
//							pLine[2] = 0;
//							pEdit->ReplaceSel(pHead);
//							pLine[2] = tt;
//
//							pLine++;
//							pHead = pLine+1;
//						}
//						else
//						{
//							pLine[0]=0;
//							pEdit->m_iTotalCharactors += (int)(pLine - pHead);
//							pEdit->ReplaceSel(pHead);
//
//							pHead = pLine+1;
//
//							ClearLine = TRUE;
//						}
//					}
//					else
//					{
//						if( pLine[0] == '\7' )::Beep( 750, 150 );
//						if( pLine[0] < 32 && pLine[0]!='\n' && pLine[0]!='\t' )
//							pLine[0] = 30;
//					}
//				}
//				if( pHead < pLine )
//				{
//					if(ClearLine)
//					{	ClearLine = FALSE;
//						pEdit->m_iTotalCharactors = TotalCharCommitted;
//					}
//					// Move cursor
//					pEdit->SetSel(pEdit->m_iTotalCharactors,-1);
//
//					pEdit->m_iTotalCharactors += (int)(pLine - pHead + 1);
//					pEdit->ReplaceSel(pHead);
//				}
//			}
//		}
//		else
//		{
//			Sleep(1000);
//		}
//	}
//}


DWORD WINAPI CConsoleView::IO_Thread_Func( LPVOID lpParameter )
{
	CConsoleView	* pEdit = (CConsoleView*)lpParameter;
	char input[512];
	input[1] = 0;
	DWORD Len;

	for(;;)
	{
		if(::ReadFile(pEdit->m_hPipeRead,&input,1,&Len,NULL))
		{
			Sleep(10);

			{
				DWORD Total;
				PeekNamedPipe(pEdit->m_hPipeRead,NULL,0,NULL,&Total,NULL);

				if(Total)
				{
					VERIFY(::ReadFile(pEdit->m_hPipeRead,&input[1],510,&Len,NULL));
					Len++;
				}
				else
				{
					Len = 1;
				}

				input[Len] = 0;
			}

			// parsing content
			for(DWORD i=0;i<Len;i++)
			{
				if( input[i] == '\7' )::Beep( 750, 150 );
				if( input[i] < 32 && input[i]!='\n' && input[i]!='\t' && input[i]!='\r')
					input[i] = 30;
			}

			{EnterCCSBlock(pEdit->m_iTotalCharactorsCCS);
				pEdit->SetSel(pEdit->m_iTotalCharactors,-1);
				pEdit->m_iTotalCharactors += Len;
				
				USES_CONVERSION;
				pEdit->ReplaceSel(CA2T(input));
			}
		}
		else
		{
			Sleep(1000);
		}
	}
}


void CConsoleView::OnDestroy()
{
	if( m_hIOThread )
	{
		::TerminateThread(m_hIOThread,0);
		::CloseHandle(m_hIOThread);
		m_hIOThread = NULL;
	}

	CEdit::OnDestroy();
}

void CConsoleView::OnLButtonDblClk(UINT nFlags, CPoint point)
{
	static TCHAR org_title[1024];

	if(m_bOutputPaused)
	{
		GetParent()->SetWindowText(org_title);
		m_iTotalCharactorsCCS.Unlock();
	}
	else
	{
		GetParent()->GetWindowText(org_title,1022);

		TCHAR tt[1024];
		wsprintf(tt,_T("%s - Suspended"),org_title);

		m_iTotalCharactorsCCS.Lock();
		GetParent()->SetWindowText(tt);
	}

	m_bOutputPaused = !m_bOutputPaused;
}

}


