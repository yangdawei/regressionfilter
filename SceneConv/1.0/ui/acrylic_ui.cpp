#include "stdafx.h"
#include "acrylic_ui.h"
#include <atlconv.h>
#include <1.0\w32\w32_misc.h>

using namespace Gdiplus;

namespace ui
{
namespace _meta_
{
	void _LoadImageSetResourceCatalog(LPCTSTR pCat)
	{	_LoadImage(pCat, 0xdeedbeef, NULL);
	}
	BOOL _LoadImage(LPCTSTR image_fn, DWORD flag, _ImageData* pID)
	{	
		static LPCTSTR res_cat = NULL;
		if(flag == 0xdeedbeef && !pID)
		{	res_cat = image_fn;
			return TRUE;
		}
		/////////////////////////////////////////////////////
		// Workaround For: Gdiplus::Bitmap will take a DPI setting from the file, 
		// and SetResolution doesn't work well in WinXP
		ATL::CImage image;

		if((flag&BGS_MASK) == BGS_RESOURCE)
		{
			ASSERT(res_cat);	// call ui::_meta_::_LoadImageSetResourceCatalog before load from resource
			image.Load(w32::CResourceStream(w32::GetResourceModule(), res_cat, image_fn));
		}
		else
		{
			image.Load(image_fn);
		}

		if(	!image.IsNull()	&& pID->channel*8 == image.GetBPP())
		{	
			int pitch = image.GetPitch();
			int step = abs(pitch);
			if(	pID->width != image.GetWidth() || 
				pID->height != image.GetHeight() ||
				pID->byte_steps != step
			)
			{	
				pID->data = _Malloc32AL(BYTE, step*image.GetHeight()); 
				if(!pID->data)return FALSE;

				pID->width = image.GetWidth();
				pID->height = image.GetHeight();
				pID->byte_steps = step;
			}
				
			LPBYTE pbits = (LPBYTE)image.GetBits();
			if(pitch < 0)
			{	
				pbits -= step*(image.GetHeight() - 1);
				// mirror bottom-up image
				for(int y = 0;y<image.GetHeight();y++)
				{	LPBYTE pline = (pID->data + step*y);
					LPBYTE ppeer = (pbits + step*(image.GetHeight() - y - 1));
					memcpy(pline,ppeer,step);
				}
			}
			else memcpy(pID->data, pbits, step*pID->height);
			return TRUE;
		}
		return FALSE;
	}
}} // namespace ui::_meta_


ui::CAcrylicUIElement::CAcrylicUIElement()
{
	m_Position.x = m_Position.y = 0;
	m_Size.cx = m_Size.cy = 64;
}


////////////////////////////////////////////////////////
// CAcrylicGroup
ui::CAcrylicGroup::CAcrylicGroup()
{
	_TotalFixedSize = 0;
	_AutoSizedCount = 0;
}

void ui::CAcrylicGroup::_Element::_Init()
{
	ZeroMemory(this,sizeof(*this));
	_Clipped = TRUE;
	_Size = _SIZE_AUTO;
}

void ui::CAcrylicGroup::InsertElement(int idx, ui::CAcrylicUIElement* p)
{
	ASSERT(idx < (int)GetElementCount());
	_Element& i = m_Elements.insert(idx);
	i._Init();
	i.pElement = p;
}

void ui::CAcrylicGroup::SetElementCount(int tot)
{
	VERIFY(m_Elements.SetSize(tot));
	for(UINT i=0;i<m_Elements.GetSize();i++)
		m_Elements[i]._Init();
}

void ui::CAcrylicGroup::RecalculateElememntLayout()
{
	_TotalFixedSize = 0;
	_AutoSizedCount = 0;

	_LeftFixedSize = 0;
	_CenterFixedSize = 0;
	_RightFixedSize = 0;

	_LeftAutoSizedCount = 0;
	_CenterAutoSizedCount = 0;
	_RightAutoSizedCount = 0;

	_CenterStartOffset = -1;
	for(UINT i=0;i<m_Elements.GetSize();i++)
	{
		int sz;
		if(!(m_Elements[i]._Flag & UGEF_HIDDEN) && m_Elements[i]._Size == _SIZE_AUTO)
		{
			_AutoSizedCount ++;
			sz = m_Elements[i]._PaddingFar + m_Elements[i]._PaddingNear;
		}
		else
		if(!(m_Elements[i]._Flag & UGEF_HIDDEN) && m_Elements[i]._Size != _SIZE_AUTO)
		{
			sz = m_Elements[i]._Size + m_Elements[i]._PaddingFar + m_Elements[i]._PaddingNear;
		}

		_TotalFixedSize += sz;
		if(_CenterStartOffset < 0)
			if((m_Elements[i]._Flag&UGEF_ALIGN_MASK) != UGEF_ALIGN_NEAR)
				_CenterStartOffset = i;
			else
			{
				_LeftFixedSize += sz;
				if(m_Elements[i]._Size == _SIZE_AUTO)_LeftAutoSizedCount++;
			}
	}
	if(_CenterStartOffset < 0)_CenterStartOffset = m_Elements.GetSize();


	_CenterEndOffset = 0;
	for(int i = m_Elements.GetSize() - 1;i>=0;i--)
	{
		int sz;
		if(!(m_Elements[i]._Flag & UGEF_HIDDEN) && m_Elements[i]._Size == _SIZE_AUTO)
		{
			sz = m_Elements[i]._PaddingFar + m_Elements[i]._PaddingNear;
		}
		else
		if(!(m_Elements[i]._Flag & UGEF_HIDDEN) && m_Elements[i]._Size != _SIZE_AUTO)
		{
			sz = m_Elements[i]._Size + m_Elements[i]._PaddingFar + m_Elements[i]._PaddingNear;
		}

		if((m_Elements[i]._Flag&UGEF_ALIGN_MASK) != UGEF_ALIGN_FAR)
		{	_CenterEndOffset = i+1;
			break;
		}
		else
		{
			_RightFixedSize += sz;
			if(m_Elements[i]._Size == _SIZE_AUTO)_RightAutoSizedCount++;
		}
	}

	//for(int i = _CenterStartOffset; i < _CenterEndOffset; i++)
	//	ASSERT((m_Elements[i]._Flag&UGEF_ALIGN_MASK) == UGEF_ALIGN_CENTER);	// Element must be ordered in left / center / right alignment

	_CenterFixedSize = _TotalFixedSize - _LeftFixedSize - _RightFixedSize;
	ASSERT(_CenterFixedSize >= 0);

	_CenterAutoSizedCount = _AutoSizedCount - _LeftAutoSizedCount - _RightAutoSizedCount;
	ASSERT(_CenterAutoSizedCount >= 0);
}

void ui::CAcrylicGroup::_CalucateLayout(UINT cl,UINT ele_start, UINT ele_end, int pos, int pos_end, int a_size)
{
	int cp = pos;
	UINT i = ele_start;
	if(a_size >= 0)
	{	
		int last_auto = -1;
		int last = -1;
		for(; i<ele_end && cp+2<pos_end ;i++)
		{
			if(!(UGEF_HIDDEN&m_Elements[i]._Flag))
			{
				int sz = (m_Elements[i]._Size == _SIZE_AUTO)?
						 a_size:(m_Elements[i]._Size+m_Elements[i]._PaddingFar+m_Elements[i]._PaddingNear);
				sz = min(sz, pos_end - cp);

				int sz_assign = sz - (m_Elements[i]._PaddingFar+m_Elements[i]._PaddingNear);
				if(sz_assign > 0)
				{
					if(m_Elements[i].pElement)
					{
						m_Elements[i].pElement->m_Position[cl] = cp + m_Elements[i]._PaddingNear;
						m_Elements[i].pElement->m_Size[cl] = sz_assign;

						m_Elements[i]._Clipped = FALSE;
						last = i;
						if(m_Elements[i]._Size == _SIZE_AUTO)last_auto = i;
					}
					cp += sz;
					continue;
				}	
			}
			m_Elements[i]._Clipped = TRUE;
		}

		if(last >= 0 && last_auto >= 0)
		{
			int missed = pos_end - (m_Elements[last].pElement->m_Position[cl] + m_Elements[last].pElement->m_Size[cl]) - 1;
			ASSERT(missed >= 0);
			m_Elements[last_auto].pElement->m_Size[cl] += missed;
			for(int i=last_auto+1;i<=last;i++)
				if(!m_Elements[i]._Clipped)
					m_Elements[i].pElement->m_Position[cl] += missed;
		}
	}
	else
	{
		for(; i<ele_end && cp+2<pos_end ;i++)
		{
			if(!(UGEF_HIDDEN&m_Elements[i]._Flag) && m_Elements[i]._Size != _SIZE_AUTO)
			{
				int sz = m_Elements[i]._Size+m_Elements[i]._PaddingFar+m_Elements[i]._PaddingNear;
				sz = min(sz, pos_end - cp);

				int sz_assign = sz - (m_Elements[i]._PaddingFar+m_Elements[i]._PaddingNear);
				if(sz_assign > 0)
				{
					if(m_Elements[i].pElement)
					{
						m_Elements[i].pElement->m_Position[cl] = cp + m_Elements[i]._PaddingNear;
						m_Elements[i].pElement->m_Size[cl] = sz_assign;
						m_Elements[i]._Clipped = FALSE;
					}
					cp += sz;
					continue;
				}	
			}
			m_Elements[i]._Clipped = TRUE;
		}
	}

	for(;i<m_Elements.GetSize();i++)
		m_Elements[i]._Clipped = TRUE;
}

void ui::CAcrylicGroup::UpdateLayout()
{
	int pos,size;
	int cl;
	int cl_oppo;
	if(m_Flag&UGF_LAYOUT_VERTICAL)
	{	pos = m_Position.y;
		size = m_Size.cy;
		cl = 1;
	}
	else
	{	pos = m_Position.x;
		size = m_Size.cx;
		cl = 0;
	}
	cl_oppo = (cl+1)%2;

	if(_Margin_Near + _Margin_Far + 4 > m_Size[cl_oppo])
	{
		for(UINT i=0;i<m_Elements.GetSize();i++)
			m_Elements[i]._Clipped = TRUE;
		return;
	}
	
	if(_AutoSizedCount)
	{	
		_CalucateLayout(cl, 0, m_Elements.GetSize(), pos, pos + size, (size - _TotalFixedSize - 1)/_AutoSizedCount);
	}
	else
	{	if(_TotalFixedSize <= size)
		{	
			_CalucateLayout(cl, 0, _CenterStartOffset, pos, pos + size, -1);

			int center_pos = max(_LeftFixedSize, size/2 - _CenterFixedSize/2) + pos;
			_CalucateLayout(cl, _CenterStartOffset, _CenterEndOffset, center_pos, pos + size, -1);

			int right_pos;
			if(_CenterFixedSize)
				right_pos = max(center_pos + _CenterFixedSize, pos + size - _RightFixedSize - 1);
			else
				right_pos = max(_LeftFixedSize, pos + size - _RightFixedSize - 1);
			_CalucateLayout(cl, _CenterEndOffset, m_Elements.GetSize(), right_pos, pos + size, -1);
		}
		else
		{	_CalucateLayout(cl, 0, m_Elements.GetSize(), pos, pos + size, -1);
		}
	}
	
	int pos_oppo = _Margin_Near + m_Position[cl_oppo];
	int sz_oppo = m_Size[cl_oppo] - (_Margin_Near + _Margin_Far);
	for(UINT i=0;i<m_Elements.GetSize();i++)
		if(!m_Elements[i]._Clipped)
		{
			ASSERT(m_Elements[i].pElement);
			m_Elements[i].pElement->m_Position[cl_oppo] = pos_oppo;
			m_Elements[i].pElement->m_Size[cl_oppo] = sz_oppo;
		}
}

void ui::CAcrylicGroup::Render(Gdiplus::Graphics & gfx)
{
	for(UINT i=0;i<m_Elements.GetSize();i++)
		if(!m_Elements[i]._Clipped)
		{
			ASSERT(m_Elements[i].pElement);
			m_Elements[i].pElement->Render(gfx);
		}
}


////////////////////////////////////////////////////////
// CAcrylicTextBox
ui::CAcrylicTextBox::CAcrylicTextBox()
{
	m_pFont = NULL;
	m_pStringFormat = NULL;
}

void ui::CAcrylicTextBox::SetText(const rt::String_Ref& str)
{
	if(!m_pStringFormat)
		m_pStringFormat = new Gdiplus::StringFormat;
	m_Text = str;
}

void ui::CAcrylicTextBox::Render(Gdiplus::Graphics & gfx)
{
	if(m_Text.IsEmpty() && _BorderWidth < EPSILON)return;

	RectF rc(	m_Position.x + _Margin_Left, m_Position.y + _Margin_Top,
				m_Size.cx - _Margin_Left - _Margin_Right,
				m_Size.cy - _Margin_Top - _Margin_Bottom);

	if(!m_Text.IsEmpty())
	{
		Gdiplus::Font* f = m_pFont?m_pFont : ui::defaults::m_pFont;

		m_pStringFormat->SetAlignment((StringAlignment)_Align);
		m_pStringFormat->SetLineAlignment((StringAlignment)_vAlign);
		m_pStringFormat->SetFormatFlags((StringFormatFlags)(_Flag&TBF_FORMATFLAG_MASK));
		m_pStringFormat->SetTrimming((StringTrimming)((_Flag&TBF_TRIMMINGFLAG_MASK)>>16));

		{
			SolidBrush	b(_Color);

			//rc.Offset(1,1);
			//gfx.DrawString(m_Text, m_Text.GetLength(), f, rc, m_pStringFormat, bs);
			//rc.Offset(-2,-2);
			//gfx.DrawString(m_Text, m_Text.GetLength(), f, rc, m_pStringFormat, bs);
			//rc.Offset(0,2);
			//gfx.DrawString(m_Text, m_Text.GetLength(), f, rc, m_pStringFormat, bs);
			//rc.Offset(2,-2);
			//gfx.DrawString(m_Text, m_Text.GetLength(), f, rc, m_pStringFormat, bs);

			//rc.Offset(-1,1);
			gfx.DrawString(m_Text, m_Text.GetLength(), f, rc, m_pStringFormat, &b);
		}
	}

	if(_BorderWidth > EPSILON)
		gfx.DrawRectangle(&Pen(_BorderColor, _BorderWidth), rc);
}

void ui::CAcrylicTextBox::Release()
{
	_SafeDel(m_pStringFormat);
}


////////////////////////////////////////////////////////
// CAcrylicBackground
void  ui::CAcrylicBackground::Render(Gdiplus::Graphics& gfx)
{
	if(m_BackgroundImage.IsNotNull())
	{
		switch(m_BackgroundMode&BGM_STRETCH_MASK)
		{
		case BGM_STRETCH_NONE:
			gfx.DrawImage(	m_BackgroundImage,0,0,
							m_BackgroundImageAncherSize,m_BackgroundImageAncherSize,
							m_BackgroundImage.GetWidth() - 2*m_BackgroundImageAncherSize,m_BackgroundImage.GetHeight() - 2*m_BackgroundImageAncherSize,
							UnitPixel
						 );
			break;
		case BGM_STRETCH_FREE:
			gfx.SetInterpolationMode(InterpolationModeHighQualityBicubic);
			gfx.DrawImage(	m_BackgroundImage,
							Rect(0,0,m_Size.cx,m_Size.cy),
							m_BackgroundImageAncherSize,m_BackgroundImageAncherSize,
							m_BackgroundImage.GetWidth() - 2*m_BackgroundImageAncherSize,m_BackgroundImage.GetHeight() - 2*m_BackgroundImageAncherSize,
							UnitPixel, NULL, NULL, NULL);
			break;
		case BGM_STRETCH_BOXING:
			// 4 non-stretch corners
			gfx.DrawImage(m_BackgroundImage,0,0,1,1,_BackgroundHorizontalBox[0],_BackgroundVerticalBox[0],UnitPixel);

			int STRETCH_W, STRETCH_H;
			STRETCH_W = max(0, m_Size.cx - _BackgroundHorizontalBox[0] - _BackgroundHorizontalBox[2]);
			STRETCH_H = max(0, m_Size.cy - _BackgroundVerticalBox[0] - _BackgroundVerticalBox[2]);

			gfx.DrawImage(m_BackgroundImage,STRETCH_W + _BackgroundHorizontalBox[0],0,
						  1+_BackgroundHorizontalBox[0]+_BackgroundHorizontalBox[1],1,
						  _BackgroundHorizontalBox[2],_BackgroundVerticalBox[0],UnitPixel);
			gfx.DrawImage(m_BackgroundImage,0,STRETCH_H + _BackgroundVerticalBox[0],
						  1,1+_BackgroundVerticalBox[0]+_BackgroundVerticalBox[1],
						  _BackgroundHorizontalBox[0],_BackgroundVerticalBox[2],UnitPixel);
			gfx.DrawImage(m_BackgroundImage,
						  STRETCH_W + _BackgroundHorizontalBox[0],STRETCH_H + _BackgroundVerticalBox[0],
						  1+_BackgroundHorizontalBox[0]+_BackgroundHorizontalBox[1],1+_BackgroundVerticalBox[0]+_BackgroundVerticalBox[1],
						  _BackgroundHorizontalBox[2],_BackgroundVerticalBox[2],UnitPixel);
			
			// 4 1D-stretching edges
			//gfx.SetInterpolationMode(InterpolationModeDefault);
			if(STRETCH_W)
			{	
				gfx.DrawImage(	m_BackgroundImage,
								Rect(_BackgroundHorizontalBox[0], 0, STRETCH_W, _BackgroundVerticalBox[0]),
								1+_BackgroundHorizontalBox[0],1,_BackgroundHorizontalBox[1],_BackgroundVerticalBox[0],	
								UnitPixel, NULL, NULL, NULL);
				gfx.DrawImage(	m_BackgroundImage,
								Rect(_BackgroundHorizontalBox[0], STRETCH_H + _BackgroundVerticalBox[0], STRETCH_W, _BackgroundVerticalBox[2]),
								1+_BackgroundHorizontalBox[0],1+_BackgroundVerticalBox[0]+_BackgroundVerticalBox[1],_BackgroundHorizontalBox[1],_BackgroundVerticalBox[2],	
								UnitPixel, NULL, NULL, NULL);
			}
			if(STRETCH_H)
			{
				gfx.DrawImage(	m_BackgroundImage,
								Rect(0, _BackgroundVerticalBox[0], _BackgroundHorizontalBox[0], STRETCH_H),
								1,1+_BackgroundVerticalBox[0],_BackgroundHorizontalBox[0],_BackgroundVerticalBox[1],
								UnitPixel, NULL, NULL, NULL);
				gfx.DrawImage(	m_BackgroundImage,
								Rect(_BackgroundHorizontalBox[0] + STRETCH_W, _BackgroundVerticalBox[0], _BackgroundHorizontalBox[2], STRETCH_H),
								1+_BackgroundHorizontalBox[0]+_BackgroundHorizontalBox[1],1+_BackgroundVerticalBox[0],_BackgroundHorizontalBox[2],_BackgroundVerticalBox[1],	
								UnitPixel, NULL, NULL, NULL);
			}

			// 2-D stretching center area
			if((BGM_RENDER_CENTER_BLOCK&m_BackgroundMode) && STRETCH_H && STRETCH_W)
			{
				gfx.DrawImage(	m_BackgroundImage,
								Rect(_BackgroundHorizontalBox[0], _BackgroundVerticalBox[0], STRETCH_W, STRETCH_H),
								1+_BackgroundHorizontalBox[0],1+_BackgroundVerticalBox[0],_BackgroundHorizontalBox[1],_BackgroundVerticalBox[1],
								UnitPixel, NULL, NULL, NULL);
			}

			break;
		default: ASSERT(0);
		}
	}
}

ui::CAcrylicBackground::CAcrylicBackground()
{
}

void ui::CAcrylicBackground::Release()
{
	m_BackgroundImage.Release();
}

void ui::CAcrylicBackground::SetStyle(const Style_Background& m)
{
	*((Style_Background*)this) = m;
}


BOOL ui::CAcrylicBackground::SetImage(LPCTSTR image_fn, DWORD flag)
{
	m_BackgroundMode = BGM_MASK&flag;
	m_BackgroundImageAncherSize = ((BGM_STRETCH_MASK&m_BackgroundMode) == BGM_STRETCH_BOXING);

	if(!m_BackgroundImage.Load(image_fn, flag))return FALSE;

	if((m_BackgroundMode&BGM_STRETCH_MASK) == BGM_STRETCH_BOXING)
	{	
		int w = m_BackgroundImage.GetWidth();
		int h = m_BackgroundImage.GetHeight();

		// detect box anchers, Horizontal (scan for black line-segment)
		_BackgroundHorizontalBox[0] = w/2-1;
		_BackgroundHorizontalBox[1] = 1;
		{	
			int i=0;
			for(;i<w;i++)
			{	px_4c& px = m_BackgroundImage(i,0);
				if(px.a > 128 && px.r<60 && px.g<60 && px.b<60)
				{	_BackgroundHorizontalBox[0] = i - 1;	i++;
					for(;i<w;i++)
					{	px_4c& px = m_BackgroundImage(i,0);
						if(px.a > 128 && px.r<60 && px.g<60 && px.b<60)
							_BackgroundHorizontalBox[1] = i - _BackgroundHorizontalBox[0];
						else goto NEXT1;
					}	
			}	}
		}
NEXT1:
		_BackgroundHorizontalBox[2] = w - (_BackgroundHorizontalBox[0] + _BackgroundHorizontalBox[1]) - 2;

		// detect box anchers, Vertical (scan for black line-segment)
		_BackgroundVerticalBox[0] = h/2-1;
		_BackgroundVerticalBox[1] = 1;
		{	
			int i=0;
			for(;i<h;i++)
			{	px_4c& px = m_BackgroundImage(0,i);
				if(px.a > 128 && px.r<60 && px.g<60 && px.b<60)
				{	_BackgroundVerticalBox[0] = i - 1;	i++;
					for(;i<h;i++)
					{	px_4c& px = m_BackgroundImage(0,i);
						if(px.a > 128 && px.r<60 && px.g<60 && px.b<60)
							_BackgroundVerticalBox[1] = i - _BackgroundVerticalBox[0];
						else goto NEXT2;
					}	
			}	}
		}
NEXT2:
		_BackgroundVerticalBox[2] = h - (_BackgroundVerticalBox[0] + _BackgroundVerticalBox[1]) - 2;
	}
	return TRUE;
}

///////////////////////////////////////////////////
// CAcrylicFrameWnd
BOOL ui::CAcrylicFrameWnd::Create(HWND Parent)
{
	m_Background.m_Position.x = m_Background.m_Position.y = 0;
	if(__super::Create(WS_OVERLAPPEDWINDOW|WS_CLIPCHILDREN|WS_CLIPSIBLINGS, Parent))
	{
		_PatchNCSYSCOMMAND();
		return TRUE;
	}

	return FALSE;
}

void ui::CAcrylicFrameWnd::SetRenderLayerExtension(int left, int top, int right, int bottom)
{
	__super::SetRenderLayerExtension(max(5,left),max(5,top),max(5,right),max(5,bottom));
}

ui::CAcrylicFrameWnd::CAcrylicFrameWnd()
{
	SetRenderLayerExtension(0,0,0,0);

	m_Caption.Style()._Align = 0;
	m_Caption.Style()._Color = px_4c(255,80,80,80);
	m_Caption.Style()._Flag = TBF_FORMAT_NOWRAP|TBF_TRIMMING_ELLIPSISCHARACTER;

	_pCaptionFont = NULL;
	_pStatusBarFont = NULL;
	
	m_FrameMsgPaneIdx = -1;

	// Hard code, to be cleared later
	_ResizingHandlerTop_CY[0] = 1;
	_ResizingHandlerTop_CY[1] = 5;
	_ResizingHandlerBottom_CY[0] = 1;
	_ResizingHandlerBottom_CY[1] = 5;
	_ResizingHandlerLeft_CX[0] = 1;
	_ResizingHandlerLeft_CX[1] = 5;
	_ResizingHandlerRight_CX[0] = 1;
	_ResizingHandlerRight_CX[1] = 5;

	_ResizingHandlerCornerSize = 16;

	_Caption_CY[0] = 4;
	_Caption_CY[1] = 23;

	_StatusBar_CY[0] = 8;
	_StatusBar_CY[1] = 24;

	_MinimizeBox_CX[0] = 77;
	_MinimizeBox_CX[1] = 100;
	_MaximizeBox_CX[0] = 52;
	_MaximizeBox_CX[1] = 75;
	_CloseBox_CX[0] = 10;
	_CloseBox_CX[1] = 50;
	_CommandBoxs_CY[0] = 2;
	_CommandBoxs_CY[1] = 14;

	_CaptionImagePlacement.X = 2;
	_CaptionImagePlacement.Y = -25;
}

ui::CAcrylicFrameWnd::~CAcrylicFrameWnd()
{
	//ASSERT(m_pBackgroundImage == NULL);
}

void ui::CAcrylicFrameWnd::_PatchNCSYSCOMMAND()
{
	static HHOOK hMHook = NULL;

	// Setup Mouse Hook, patch for lost of WM_NCLBUTTONUP in classic windows theme
	// http://www.codeguru.com/Cpp/misc/misc/windowsmessaging/article.php/c3885/
	struct _hook
	{	static LRESULT CALLBACK MouseHookProc( int nCode, WPARAM wParam, LPARAM lParam)
		{	
			if(nCode == HC_ACTION)
			{ 
				PMOUSEHOOKSTRUCT mhs = (PMOUSEHOOKSTRUCT) lParam;
				if(wParam == WM_LBUTTONUP && mhs->hwnd)
				{
					POINT pos = mhs->pt;
					int hitcode = ::SendMessage(mhs->hwnd,WM_NCHITTEST,0,POINTTOPOINTS(pos));
					if(hitcode > 0)
						::PostMessage(mhs->hwnd,WM_NCLBUTTONUP, hitcode, POINTTOPOINTS(pos));
				}
			}
			return CallNextHookEx(hMHook, nCode, wParam, lParam);
		}
	};

	if(!hMHook)
		hMHook = SetWindowsHookEx( WH_MOUSE, (HOOKPROC)_hook::MouseHookProc, NULL, ::GetCurrentThreadId());

	_IsVistaCompositionTheme = ui::IsWindowThemeComposited();
}

void ui::CAcrylicFrameWnd::SetStyle(const Style_FrameWnd& m)
{
	*((Style_FrameWnd*)this) = m;

	__FrameTop_CY = max(_CommandBoxs_CY[1], _Caption_CY[1]);
	_ResizingHandlerCornerSize = max(16, max(	max(_ResizingHandlerTop_CY[1] - _ResizingHandlerTop_CY[0],
													_ResizingHandlerBottom_CY[1] - _ResizingHandlerBottom_CY[0]),
												max(_ResizingHandlerLeft_CX[1] - _ResizingHandlerLeft_CX[0],
													_ResizingHandlerRight_CX[1] - _ResizingHandlerRight_CX[0])
									 )		);
	_Caption_CY[0] = max(_Caption_CY[0], _ResizingHandlerTop_CY[1]);
}

void ui::CAcrylicFrameWnd::GetStyle(Style_FrameWnd& m)
{
	m = *this;
}

void ui::CAcrylicFrameWnd::SetStatusBar(UINT pane_count, int frame_msg_pane_idx, int default_padding)
{
	for(UINT i=0;i<m_StatusBar.GetElementCount();i++)
	{
		ui::CAcrylicTextBox* p = (ui::CAcrylicTextBox*)m_StatusBar.GetElement(i);
		m_StatusBar.SetElement(i, NULL);
		_SafeDel(p);
	}

	m_StatusBar.SetElementCount(pane_count);
	m_FrameMsgPaneIdx = frame_msg_pane_idx;

	for(UINT i=0;i<m_StatusBar.GetElementCount();i++)
	{
		ui::CAcrylicTextBox* p = new ui::CAcrylicTextBox;
		p->m_pFont = _pStatusBarFont;
		p->Style()._Color = px_4c(255,40,40,40);
		p->Style()._BorderColor = px_4c(255,150,150,150);
		p->Style()._BorderWidth = 1;
		p->Style()._Flag = TBF_FORMAT_NOWRAP|TBF_TRIMMING_ELLIPSISCHARACTER;

		m_StatusBar.SetElement(i, p);
		if(i)m_StatusBar.ElementStyle(i)._PaddingNear = default_padding;
	}

	m_StatusBar.RecalculateElememntLayout();
}

void ui::CAcrylicFrameWnd::SetStatusPaneWidth(int i, int size)
{
	Style_GroupElement& s = m_StatusBar.ElementStyle(i);
	if(s._Size == size)return;
	BOOL mode_change = (s._Size == _SIZE_AUTO || size == _SIZE_AUTO);
	s._Size = size;
	if(mode_change)m_StatusBar.RecalculateElememntLayout();
}

void ui::CAcrylicFrameWnd::SetStatusPaneText(int i, LPCTSTR str)
{
	ui::CAcrylicTextBox* p = (ui::CAcrylicTextBox*)m_StatusBar.GetElement(i);
	ASSERT(p);
	p->SetText(str);
	Refresh();
}

void ui::CAcrylicFrameWnd::SetStatusPaneAlignment(int i, int align)
{
	int a = m_StatusBar.GetElementFlag(i);
	if(align != (a&UGEF_ALIGN_MASK))
	{
		m_StatusBar.SetElementFlag(i, (a&(~UGEF_ALIGN_MASK))|align);
		m_StatusBar.RecalculateElememntLayout();
	}
}

ui::Style_TextBox& ui::CAcrylicFrameWnd::GetStatusPaneTextStyle(int i)
{
	return ((ui::CAcrylicTextBox*)m_StatusBar.GetElement(i))->Style();
}

BOOL ui::CAcrylicFrameWnd::SetCaptionImage(LPCTSTR image_fn, DWORD flag)
{
	if(m_CaptionImage.Load(image_fn, flag))
	{
		if(flag&CPF_IMAGE_NO_STRETCH)
		{	
			_CaptionImagePlacement.Width = m_CaptionImage.GetWidth();
			_CaptionImagePlacement.Height = m_CaptionImage.GetHeight();
		}
	}
	return FALSE;
}

void ui::CAcrylicFrameWnd::OnRender(HDC hMemoryDC)
{
	Graphics gfx(hMemoryDC);
	gfx.Clear(Color(0));

	m_Background.Render(gfx);

	if(m_CaptionImage.IsNotNull())
		gfx.DrawImage(m_CaptionImage, (Rect&)_CaptionImagePlacement, 0,0, m_CaptionImage.GetWidth(), m_CaptionImage.GetHeight(), UnitPixel, NULL,NULL,NULL);

	m_Caption.Render(gfx);
	m_StatusBar.Render(gfx);
}

LRESULT ui::CAcrylicFrameWnd::WndProc(UINT message, WPARAM wParam, LPARAM lParam)
{
	switch(message)
	{
	case WM_CREATE:
		{	
			//NONCLIENTMETRICS noncli;
			//noncli.cbSize = sizeof(noncli);
			//VERIFY(SystemParametersInfo(SPI_GETNONCLIENTMETRICS, sizeof(noncli), &noncli, 0));
			//LPCTSTR facename = noncli.lfCaptionFont.lfFaceName;
			LPCTSTR facename = L"Tahoma";
			
			if(!_pCaptionFont)_pCaptionFont = new Gdiplus::Font(facename,11,FontStyleBold,UnitPixel,NULL);
			if(!_pStatusBarFont)_pStatusBarFont = new Gdiplus::Font(facename,11,0,UnitPixel,NULL);
			
			m_Caption.m_pFont = _pCaptionFont;
			m_FrameMsgPaneIdx = -1;
			SetStatusBar(0);
			break;
		}
	case WM_NCDESTROY:
		m_Background.Release();
		m_CaptionImage.Release();
		_SafeDel(_pCaptionFont);
		_SafeDel(_pStatusBarFont);
		SetStatusBar(0);
		break;
	case WM_NCHITTEST:
		return _NC_HitTest(GET_X_LPARAM(lParam)-m_Position.x, GET_Y_LPARAM(lParam)-m_Position.y);
	case WM_NCLBUTTONDOWN:
		_LastNCLBUTTONDOWN_Target = wParam;
		break;
	case WM_NCLBUTTONUP:
		if(_LastNCLBUTTONDOWN_Target == wParam && !_IsVistaCompositionTheme)
		{
			switch(wParam)
			{	case HTCLOSE:		::PostMessage(*this, WM_SYSCOMMAND, SC_CLOSE, 0); return 0;
				case HTMAXBUTTON:	::PostMessage(*this, WM_SYSCOMMAND, GetShowWindowState()==SW_MAXIMIZE?SC_RESTORE:SC_MAXIMIZE, 0); return 0;
				case HTMINBUTTON:	::PostMessage(*this, WM_SYSCOMMAND, SC_MINIMIZE, 0); return 0;
			}
		}
		break;
	case WM_THEMECHANGED:
		_IsVistaCompositionTheme = IsWindowThemeComposited();
		break;
	case WM_SETTEXT:
		m_Caption.SetText((LPCTSTR)lParam);
		Refresh();
		break;
	case 0x0362:	// MFC private mesage WM_SETMESSAGESTRING:
		if(m_FrameMsgPaneIdx >= 0 && m_FrameMsgPaneIdx < (int)m_StatusBar.GetElementCount())// && 0xe001 == wParam)
			SetStatusPaneText(m_FrameMsgPaneIdx, (LPCTSTR)lParam);
		break;
	}

	LRESULT ret = __super::WndProc(message, wParam, lParam);

	switch(message)
	{
	case WM_SIZE:
		m_Background.m_Size = m_Size;

		m_Caption.m_Position.x = max(_CaptionImagePlacement.X + _CaptionImagePlacement.Width + 2, _ResizingHandlerLeft_CX[1]);
		m_Caption.m_Position.y = _Caption_CY[0];
		m_Caption.m_Size.cx = m_Size.cx - m_Caption.m_Position.x - 
							  max(max(_ResizingHandlerRight_CX[1],_MinimizeBox_CX[1]),
								  max(_MaximizeBox_CX[1],_CloseBox_CX[1])) - 2;
		m_Caption.m_Size.cy = _Caption_CY[1] - _Caption_CY[0] + 1;

		m_StatusBar.m_Position.x = _ResizingHandlerLeft_CX[1];
		m_StatusBar.m_Position.y = m_Size.cy - _StatusBar_CY[1];
		m_StatusBar.m_Size.cx = m_Size.cx - _ResizingHandlerLeft_CX[1] - _ResizingHandlerRight_CX[1];
		m_StatusBar.m_Size.cy = _StatusBar_CY[1] - _StatusBar_CY[0];
		m_StatusBar.UpdateLayout();

		Refresh();
		break;
	}

	return ret;
}

LRESULT	ui::CAcrylicFrameWnd::_NC_HitTest(INT x, INT y)
{
	if(y > _CommandBoxs_CY[0] && y <= _CommandBoxs_CY[1])
	{	int offset = m_Size.cx - x;
		if(offset > _MinimizeBox_CX[0] && offset <= _MinimizeBox_CX[1])return HTMINBUTTON;
		if(offset > _MaximizeBox_CX[0] && offset <= _MaximizeBox_CX[1])return HTMAXBUTTON;
		if(offset > _CloseBox_CX[0] && offset <= _CloseBox_CX[1])return HTCLOSE;
	}

	if( x > _ResizingHandlerLeft_CX[1] && x < m_Size.cx - _ResizingHandlerRight_CX[1] - 1 &&
		y > _ResizingHandlerTop_CY[1] && y < m_Size.cy - _ResizingHandlerBottom_CY[1] - 1 
	)
	{
		if(y > _Caption_CY[0] && y <= _Caption_CY[1])
			return HTCAPTION;
		else
			return HTCLIENT;
	}
	
	if( y < _ResizingHandlerTop_CY[0] || 
		y >= m_Size.cy - _ResizingHandlerBottom_CY[0] ||
		x < _ResizingHandlerLeft_CX[0] ||
		x >= m_Size.cx - _ResizingHandlerRight_CX[0]
	)return HTNOWHERE;

	LRESULT ret = 0;

	if(y <= _ResizingHandlerCornerSize + _ResizingHandlerTop_CY[0])
		ret = HTTOP;
	else if(y > m_Size.cy - (_ResizingHandlerCornerSize + _ResizingHandlerBottom_CY[0]) - 1)
		ret = HTBOTTOM;
	else
		goto NOTHIT1;

	if( x <= _ResizingHandlerCornerSize + _ResizingHandlerLeft_CX[0] )return ret + 1;
	if( x > m_Size.cx - (_ResizingHandlerCornerSize + _ResizingHandlerRight_CX[0]) - 1 )return ret + 2;
	return ret;
		
NOTHIT1:
	if(x <= _ResizingHandlerLeft_CX[1])return HTLEFT;
	if(x >= m_Size.cx - _ResizingHandlerRight_CX[1] - 1)return HTRIGHT;

	ASSERT(0);
	return HTERROR;
}


////////////////////////////////////////////////////
// CAcrylicFrameAttachment

ui::CAcrylicFrameAttachment::CAcrylicFrameAttachment()
{
	_ClientOffset_XY[0] = _ClientOffset_XY[1] = 
	_FrameThick_CXCY[0] = _FrameThick_CXCY[1] = 0;
	m_hAttachee = NULL;
	m_AttcheeFlag = 0;
	m_pAttacheeWndProc = NULL;
}

BOOL ui::CAcrylicFrameAttachment::Create(HWND Parent)
{
	if(_CreateBaseWnd(WS_OVERLAPPEDWINDOW|WS_CLIPCHILDREN|WS_CLIPSIBLINGS, Parent))
	{
		_PatchNCSYSCOMMAND();
		return TRUE;
	}
	return FALSE;
}

void ui::CAcrylicFrameAttachment::_RearrangeAttachee()
{
	if(m_hAttachee)
	{
		RECT rc;
		::GetWindowRect(*this, &rc);

		int client_w = rc.right - rc.left - _FrameThick_CXCY[0];
		int client_h = rc.bottom - rc.top - _FrameThick_CXCY[1];
		int client_x = rc.left + _ClientOffset_XY[0];
		int client_y = rc.top + _ClientOffset_XY[1];

		::SetWindowPos(m_hAttachee, NULL, client_x, client_y, client_w, client_h, SWP_NOZORDER);
	}
}

BOOL ui::CAcrylicFrameAttachment::AttachToFrameWnd(HWND Wnd, UINT flag)
{
	ASSERT(hWnd);
	ASSERT(m_hAttachee == NULL);

	m_hAttachee = Wnd;
	m_AttcheeFlag = flag;

	static const DWORD style_to_remove = WS_CHILD|WS_CAPTION|WS_BORDER|WS_THICKFRAME|WS_MAXIMIZEBOX|WS_MINIMIZEBOX|WS_DLGFRAME|WS_SYSMENU|WS_SIZEBOX;
	static const DWORD styleEx_to_remove = WS_EX_WINDOWEDGE|WS_EX_APPWINDOW|WS_EX_CLIENTEDGE|WS_EX_DLGMODALFRAME|WS_EX_MDICHILD|WS_EX_STATICEDGE|WS_EX_TOOLWINDOW;

	::SetWindowLong( m_hAttachee, GWL_STYLE, ::GetWindowLong(m_hAttachee,GWL_STYLE) & ~(style_to_remove) | WS_POPUP );
	::SetWindowLong( m_hAttachee, GWL_EXSTYLE, ::GetWindowLong(m_hAttachee,GWL_EXSTYLE) & ~(styleEx_to_remove));

	_RearrangeAttachee();

	if(_CreateRenderLayer(m_hAttachee))
	{
		if(FHF_SET_TITLE&m_AttcheeFlag)
		{
			TCHAR title[1024] = {0};
			::GetWindowText(m_hAttachee, title, sizeofArray(title));
			SetWindowText(title);
		}

		if(m_AttcheeFlag&FHF_SET_SUBCLASS)
		{
			#pragma warning(disable: 4312) // warning C4312: 'type cast' : conversion from 'LONG' to 'WNDPROC' of greater size
			WNDPROC wndproc = (WNDPROC)::GetWindowLongPtr(m_hAttachee, GWLP_WNDPROC);
			#pragma warning(default: 4312) // warning C4312: 'type cast' : conversion from 'LONG' to 'WNDPROC' of greater size

			m_pAttacheeWndProc = wndproc;

			#pragma warning(disable:4312)
			#ifndef _WIN64
			::SetWindowLongPtr(m_hAttachee, GWLP_WNDPROC,(LONG)(WndProc_SubclassAttachee));
			::SetWindowLongPtr(m_hAttachee,GWLP_USERDATA,(LONG)this);
			#else
			::SetWindowLongPtr(m_hAttachee, GWLP_WNDPROC,(LONG_PTR)(WndProc_SubclassAttachee));
			::SetWindowLongPtr(m_hAttachee,GWLP_USERDATA,(LONG_PTR)this);
			#endif
			#pragma warning(default:4312)
		}

		return TRUE;
	}
	else return FALSE;
}

LRESULT CALLBACK ui::CAcrylicFrameAttachment::WndProc_SubclassAttachee(HWND hwnd, UINT uMsg, WPARAM wParam, LPARAM lParam)
{
	#pragma warning(disable:4312)
	CAcrylicFrameAttachment* This = (ui::CAcrylicFrameAttachment*)::GetWindowLongPtr(hwnd,GWLP_USERDATA);
	#pragma warning(default:4312)

	if( !IsBadReadPtr(This,sizeof(CAcrylicFrameAttachment)) && This->m_hAttachee == hwnd )
	{
		// Call orignal window procedure
		LRESULT ret = CallWindowProc(This->m_pAttacheeWndProc,hwnd,uMsg,wParam,lParam);

		if( uMsg == WM_SETTEXT && (This->m_AttcheeFlag&FHF_SET_TITLE) )
		{
			This->SetWindowText((LPCTSTR)lParam);
		}
		else if( uMsg == 0x0362 )	// MFC private mesage WM_SETMESSAGESTRING:
		{
			::SendMessage(*This, 0x0362, wParam, lParam);
		}

		return ret;
	}
	else
	{ ASSERT(0); } //GWLP_USERDATA is modified by the user (via SetWindowLongPtr/SetWindowLong)

	return 0;
}


LRESULT	ui::CAcrylicFrameAttachment::WndProc(UINT message, WPARAM wParam, LPARAM lParam)
{
	switch(message)
	{
	case WM_CLOSE:
		if(m_hAttachee)::SendMessage(m_hAttachee, WM_SYSCOMMAND, SC_CLOSE, 0);
		DestroyWindow();
		return 0;
	case WM_DESTROY:
		if(m_pAttacheeWndProc && m_hAttachee)
		{	
			#ifndef _WIN64
				::SetWindowLongPtr(m_hAttachee, GWLP_WNDPROC, (LONG)(m_pAttacheeWndProc));
			#else
				::SetWindowLongPtr(m_hAttachee, GWLP_WNDPROC, (LONG_PTR)(m_pAttacheeWndProc));
			#endif
		}
		m_hAttachee = NULL;
		m_pAttacheeWndProc = NULL;
		break;
	case WM_SIZE:
		{	LRESULT ret = __super::WndProc(message, wParam, lParam);
			_RearrangeAttachee();
			return ret;
		}
	case WM_MOVE:
	case WM_MOVING:
		{	LRESULT ret = __super::WndProc(message, wParam, lParam);
			_RearrangeAttachee();
			return ret;
		}
	}

	return __super::WndProc(message, wParam, lParam);
}

void ui::CAcrylicFrameAttachment::SetAttachmentMargin(int left, int top, int right, int bottom)
{
	_ClientOffset_XY[0] = left;
	_ClientOffset_XY[1] = top;
	_FrameThick_CXCY[0] = left + right;
	_FrameThick_CXCY[1] = top + bottom;

	if(m_hAttachee)
		_RearrangeAttachee();
}
