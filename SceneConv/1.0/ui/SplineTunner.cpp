// SplineTunner.cpp : implementation file
//

#include "stdafx.h"
#include "SplineTunner.h"
#include "..\w32\file_64.h"

using namespace num;
using namespace Gdiplus;

// CCurveTuner
ui::CCurveTuner::CCurveTuner(_meta_::_GdiplusControlHost* pHost)
:_meta_::_GdiplusControlRoot(pHost)
{
	m_CurrentControlPoint = NULL;

	m_hMove = ::LoadCursor(NULL,IDC_SIZEALL);
	m_hAddNew = ::LoadCursor(NULL,IDC_CROSS);

	m_CurrentCurve = -1;
	m_HitCurve = -1;
}

void ui::CCurveTuner::SetCurveCount(UINT co, BOOL redraw)
{	
	EnterCCSBlock(*this);

	if(m_Curves.GetSize()==co){}
	else
	{	m_CurrentControlPoint = NULL;
		m_CurrentCurve = -1;
		m_HitCurve = -1;

		m_Curves.SetSize(co);	
		for(UINT i=0;i<co;i++)
		{	m_Curves[i].m_pTuner = this;
			m_Curves[i].m_Id = i;
		}
		SendNotify(NOTIFY_SELECT_CHANGED);
	}
	if(redraw)
		RedrawContent();
}

BOOL ui::CCurveTuner::HitControlPoint(POINT & point, BOOL IsForButtonDown)
{
	if(m_Curves.GetSize())
	{
		int thres = IsForButtonDown?14:12;

		if( m_CurrentCurve >= 0 && !m_Curves[m_CurrentCurve].m_Locked )
		{
			UINT_PTR cp = m_Curves[m_CurrentCurve].HitControlPoint((float)point.x,(float)point.y,(float)thres);
			if( cp )
			{ 
				if(IsForButtonDown)
				{	if(m_CurrentControlPoint != cp)
					{	m_CurrentControlPoint=cp;
						SendNotify(NOTIFY_SELECT_CHANGED);
					}
				}
				return TRUE;
			}
		}

		for(UINT i=0;i<GetCurveCount();i++)
		{
			if( !m_Curves[i].m_Locked && !m_Curves[i].m_Hidden )
			{
				UINT_PTR cp = m_Curves[i].HitControlPoint((float)point.x,(float)point.y,(float)thres);
				
				if( cp )
				{ 
					if( IsForButtonDown )
					{	if( m_CurrentControlPoint!=cp || m_CurrentCurve!=i)
						{	m_CurrentControlPoint = cp;
							m_CurrentCurve = i;
							SendNotify(NOTIFY_SELECT_CHANGED);
						}
					}
					return TRUE;
				}
			}
		}

		if( IsForButtonDown )
		{	if( m_CurrentCurve != -1)
			{	m_CurrentCurve = -1;
				SendNotify(NOTIFY_SELECT_CHANGED);
			}
			m_CurrentControlPoint = NULL;
		}
		return FALSE;
	}
	else return FALSE;
}

BOOL ui::CCurveTuner::HitCurve(POINT & point, BOOL IsForButtonDown)
{
	int thres = IsForButtonDown?8:6;

	for(UINT i=0;i<m_Curves.GetSize();i++)
	{
		if(!m_Curves[i].m_Hidden)
		{
			float y = m_Curves[i].ProposeNewControlPointPostion((float)point.x);
			if( fabs(y-point.y) < thres )
			{
				m_ProposalPoint.x = (float)point.x;
				m_ProposalPoint.y = y;
				m_HitCurve = i;
				return TRUE;
			}
		}
	}

	m_HitCurve = -1;
	return FALSE;
}

void ui::CCurveTuner::PlotPositiveFunction(const float* pData, UINT chan, UINT len, float scale, UINT curve_index_start)	// Total number of float is chan*len
{	
	EnterCCSBlock(*this);

	ASSERT(chan+curve_index_start <= GetCurveCount());
	m_CurrentControlPoint = NULL;
	m_CurrentCurve = -1;
	m_HitCurve = -1;

	for(UINT i=0;i<chan;i++)
	{
		GetCurve(i+curve_index_start).PlotPositiveFunction(&pData[i],len,chan,scale);
		GetCurve(i+curve_index_start).m_CurveMode = CurveMode_LineSegment;
	}

	SendNotify(NOTIFY_SELECT_CHANGED);
	RedrawContent();
}

void ui::CCurveTuner::RedrawContent()
{
	if(!IsUILocked()){}else{ return; }

	RECT layout;
	m_pControlHost->GetRedrawRect(&layout);

	Graphics* pGfx = m_pControlHost->GetGraphics();
	if( pGfx )
	{
		Graphics& gfx(*pGfx);
		gfx.SetSmoothingMode( Gdiplus::SmoothingModeDefault );

		RedrawBackground(gfx,layout);

		if( m_Curves.GetSize() )
		{
			for(UINT i=0;i<m_Curves.GetSize();i++)
			{
				if(!m_Curves[i].m_Hidden)
				{
					m_Curves[i].SetLayout(	(float)layout.left,		(float)layout.bottom,
											(float)(layout.right-1),(float)(layout.top+1));
					m_Curves[i].Plot(gfx,m_pFont,m_CurrentControlPoint,m_CurrentCurve==i);
				}
			}
		}

		gfx.Flush(FlushIntentionSync);
		SendNotify(NOTIFY_GRAPHICS_UPDATED);
	}
}

void ui::CCurveTuner::CCurve::SetCurveMode(int mode)
{	m_CurveMode = mode%CurveMode_Max;
	m_Function.SetSize();
	ASSERT(m_pTuner);
	m_pTuner->SendNotify(NOTIFY_FUNCTION_CHANGED,m_Id);
}

ui::CCurveTuner::CCurve::CCurve()
{	m_pTuner = NULL;

	m_ControlPointScaled.SetSize(MAX_CONTROL_POINT);
	m_ControlPoints.SetSize(MAX_CONTROL_POINT);

	m_ControlPointCount = 3; 
	m_ControlPoints[0] = Gdiplus::PointF(0.0f,0.0f); 
	m_ControlPoints[1] = Gdiplus::PointF(0.5f,0.5f); 
	m_ControlPoints[2] = Gdiplus::PointF(1.0f,1.0f); 

	m_Origin_X = m_Origin_Y = 0.0f;
	m_Scale_X = m_Scale_Y = 1.0f;

	m_OutputValueScale = 1.0f;
	m_LineColor = Vec4b(60,60,150,150);
	m_Hidden = m_Locked = m_FixedXValue = m_FixedYValue = FALSE;
	m_DisableControlPointAdding = FALSE;
	m_LineWidth = 1.0f;
	wcscpy(m_Name, L"Untitled");
	m_CurveMode = CurveMode_CardinalSpline;

}

LRESULT ui::CCurveTuner::MessageProc(UINT uMsg, WPARAM wParam, LPARAM lParam)
{	EnterCCSBlock(*this);

	if(!IsUILocked()){}
	else{ return __super::MessageProc(uMsg,wParam,lParam); }

	switch(uMsg)
	{
	case WM_CHAR:
		return 0;
	case WM_SYSKEYDOWN:
	case WM_KEYDOWN:
		switch(wParam)
		{
		case VK_UP:
			if( m_CurrentControlPoint )
			{	float x,y;
				m_Curves[m_CurrentCurve].GetControlPointPosition(m_CurrentControlPoint,x,y);
				y--;
				m_Curves[m_CurrentCurve].UpdateControlPoint(m_CurrentControlPoint,x,y);
				RedrawContent();
			}
			break;
		case VK_DOWN:
			if( m_CurrentControlPoint )
			{
				float x,y;
				m_Curves[m_CurrentCurve].GetControlPointPosition(m_CurrentControlPoint,x,y);
				y++;
				m_Curves[m_CurrentCurve].UpdateControlPoint(m_CurrentControlPoint,x,y);
				RedrawContent();
			}
			break;
		case VK_LEFT:
			if( m_CurrentControlPoint )
			{
				float x,y;
				m_Curves[m_CurrentCurve].GetControlPointPosition(m_CurrentControlPoint,x,y);
				x--;
				m_Curves[m_CurrentCurve].UpdateControlPoint(m_CurrentControlPoint,x,y);
				RedrawContent();
			}
			break;
		case VK_RIGHT:
			if( m_CurrentControlPoint )
			{
				float x,y;
				m_Curves[m_CurrentCurve].GetControlPointPosition(m_CurrentControlPoint,x,y);
				x++;
				m_Curves[m_CurrentCurve].UpdateControlPoint(m_CurrentControlPoint,x,y);
				RedrawContent();
			}
			break;
		case VK_DELETE:
			if( m_CurrentControlPoint )
			{
				m_Curves[m_CurrentCurve].RemoveControlPoint(m_CurrentControlPoint);
				m_CurrentControlPoint = NULL;
				RedrawContent();
			}
			break;
		case VK_TAB:
			{
				if( m_Curves.GetSize() )
				{
					if( m_CurrentCurve>=0 )
					{
						if( GetAsyncKeyState(VK_SHIFT)<0 )
						{
							if( GetAsyncKeyState(VK_CONTROL)<0 )
							{ m_CurrentControlPoint = m_Curves[m_CurrentCurve].GetPrevControlPoint(m_CurrentControlPoint); }
							else
							{ m_CurrentCurve = (m_CurrentCurve-1)%m_Curves.GetSize(); }
						}
						else
						{
							if( GetAsyncKeyState(VK_CONTROL)<0 )
							{ m_CurrentControlPoint = m_Curves[m_CurrentCurve].GetNextControlPoint(m_CurrentControlPoint); }
							else
							{ m_CurrentCurve = (m_CurrentCurve+1)%m_Curves.GetSize(); }
						}
					}
					else
					{
						m_CurrentCurve = 0;
					}

					UINT i=0;
					for(;i<m_Curves.GetSize();i++)
					{
						if(!m_Curves[m_CurrentCurve].m_Hidden)break;
						m_CurrentCurve = (m_CurrentCurve+1)%m_Curves.GetSize();
					}
					if(i==m_Curves.GetSize())
						m_CurrentCurve = -1;

					SendNotify(NOTIFY_SELECT_CHANGED);
					RedrawContent();
				}
			}
			break;
		case VK_ESCAPE:
			{	if(m_CurrentCurve!=-1)
				{	m_CurrentCurve = -1;
					SendNotify(NOTIFY_SELECT_CHANGED);
					RedrawContent();
				}
				m_CurrentControlPoint = NULL;
			}
			break;
		case '1':
			{
				if( m_CurrentCurve >=0 && !m_Curves[m_CurrentCurve].m_Locked)
				{
					m_Curves[m_CurrentCurve].SetCurveMode(CurveMode_CardinalSpline);
					RedrawContent();
				}
			}
			break;
		case '2':
			{
				if( m_CurrentCurve >=0 && !m_Curves[m_CurrentCurve].m_Locked)
				{
					m_Curves[m_CurrentCurve].SetCurveMode(CurveMode_LineSegment);
					RedrawContent();
				}
			}
			break;
		case '3':
			{
				if( m_CurrentCurve >=0 && !m_Curves[m_CurrentCurve].m_Locked)
				{
					m_Curves[m_CurrentCurve].SetCurveMode(CurveMode_BezierCurve);
					RedrawContent();
				}
			}
			break;
		}
		return 0;
		break;
	case WM_SETCURSOR:
		return TRUE;
		break;
	case WM_MOUSEMOVE:
		{	POINT point = { GET_X_LPARAM(lParam), GET_Y_LPARAM(lParam) };
			if( wParam & MK_LBUTTON && m_CurrentControlPoint )
			{
				m_Curves[m_CurrentCurve].UpdateControlPoint(m_CurrentControlPoint,(float)point.x,(float)point.y);
				RedrawContent();
			}
			else
			{
				if( HitControlPoint(point) )
				{
					::SetCursor( m_hMove );
				}
				else if( HitCurve(point) )
				{
					::SetCursor( m_hAddNew );
				}
				else
				{
					::SetCursor( m_hArrow );
				}
			}
		}
		break;
	case WM_LBUTTONUP:
		{	POINT point = { GET_X_LPARAM(lParam), GET_Y_LPARAM(lParam) };
			m_pControlHost->ReleaseMouseCapture();
			if( m_CurrentControlPoint )
			{
				RedrawContent();

				CRect rc;
				m_pControlHost->GetRedrawRect(rc);
				if( point.x < -32 || point.y < -32 || point.x > rc.Width()+32 || point.y > rc.Height()+32 )
				{
					m_Curves[m_CurrentCurve].RemoveControlPoint(m_CurrentControlPoint);
					m_CurrentControlPoint = NULL;
					RedrawContent();
				}
			}
		}
		break;
	case WM_LBUTTONDOWN:
		{
			POINT point = { GET_X_LPARAM(lParam), GET_Y_LPARAM(lParam) };
			int org_cur = m_CurrentCurve;
			if( HitControlPoint(point, TRUE) ){ m_pControlHost->SetMouseCapture(); }
			else if( HitCurve(point, TRUE) )
			{
				if( org_cur == m_HitCurve && !m_Curves[org_cur].m_Locked )
				{
					m_CurrentCurve = m_HitCurve;
					m_CurrentControlPoint = m_Curves[m_CurrentCurve].AddControlPoint(m_ProposalPoint.x,m_ProposalPoint.y);
				}
				else{ m_CurrentCurve = m_HitCurve; }
				m_pControlHost->SetMouseCapture();
			}

			RedrawContent();
		}
		break;
	case WM_MOUSEWHEEL:
		if( m_Curves.GetSize() )
		{	int zDelta = ((int)wParam)>>16;
			if( m_CurrentCurve>=0 && !m_Curves[m_CurrentCurve].m_Locked )
			{
				float scale = (wParam&MK_SHIFT)?1.0001f:1.1f;
				if( zDelta>0 )
				{
					m_Curves[m_CurrentCurve].m_OutputValueScale *= scale;
				}
				else
				{
					m_Curves[m_CurrentCurve].m_OutputValueScale /= scale;
				}
				SendNotify(NOTIFY_FUNCTION_CHANGED,m_CurrentCurve);

				RedrawContent();
				return 0xffff;
			}
		}
		break;
	case WM_SIZE:
		RedrawContent();
		break;
	}

	return __super::MessageProc(uMsg,wParam,lParam);
}

UINT_PTR ui::CCurveTuner::CCurve::AddControlPoint(float x,float y) //return CP-Handle or NULL for denied
{
	FitInLayout(x,y);

	if(!m_DisableControlPointAdding && m_ControlPointCount < MAX_CONTROL_POINT)
	{
		UINT i=0;
		for(;i<m_ControlPointCount;i++)
		{
			if( m_ControlPoints[i].X - EPSILON <= x ){}
			else{ break; }
		}

		if( i != m_ControlPointCount )
		{// Add before i
			if( i && m_ControlPoints[i-1].X + EPSILON < x )
			{
				memmove(&m_ControlPoints[i+1],&m_ControlPoints[i],(m_ControlPointCount-i)*sizeof(m_ControlPoints[0]));
			}
			else{ return NULL; }
		}
		else
		{// Add in back
		}

		m_ControlPointCount++;
		m_ControlPoints[i].X = x;
		m_ControlPoints[i].Y = y;
		return (DWORD)(&m_ControlPoints[i]);
	}
	
	return NULL; 
}

void ui::CCurveTuner::CCurve::PlotPositiveFunction(const float* pFunc, UINT len, UINT stride, float scale, BOOL Redraw)
{
	ASSERT_ARRAY(pFunc,len);
	m_Locked = TRUE;

	// search maximum 
	m_OutputValueScale = 1;

	m_ControlPointScaled.SetSize(len);
	m_ControlPoints.SetSize(len);

	m_ControlPointCount = len;

	for(UINT i=0;i<len;i++)
	{
		m_ControlPoints[i].X = i/(len-1.0f);
		m_ControlPoints[i].Y =  min(1.0f,scale*pFunc[i*stride]);
	}

	m_Function.SetSize();
	if(Redraw)
		m_pTuner->RedrawContent();
}

void ui::CCurveTuner::CCurve::UpdateControlPoint(UINT_PTR cp_handle,float x,float y)
{
	int pid = int(((Gdiplus::PointF*)cp_handle) - m_ControlPoints);
	if( pid>=0 && pid<(int)m_ControlPointCount )
	{
		FitInLayout(x,y);

		if(!m_FixedYValue)
			m_ControlPoints[pid].Y = y;

		if(m_FixedXValue){}
		else
		{
			if( pid>0 )
			{ m_ControlPoints[pid].X = max( m_ControlPoints[pid-1].X, x ); }

			if( pid<(int)m_ControlPointCount-1 )
			{ m_ControlPoints[pid].X = min( m_ControlPoints[pid+1].X, m_ControlPoints[pid].X ); }

			m_ControlPoints[m_ControlPointCount-1].X = 1.0f;
			m_ControlPoints[0].X = 0.0f;
		}

		m_Function.SetSize();
		ASSERT(m_pTuner);
		m_pTuner->SendNotify(NOTIFY_FUNCTION_CHANGED,m_Id);
	}
}

void ui::CCurveTuner::CCurve::GetControlPointPosition( UINT_PTR cp_handle, float & x,float & y)
{
	int pid = int(((Gdiplus::PointF*)cp_handle) - m_ControlPoints);
	if( pid>=0 && pid<(int)m_ControlPointCount )
	{
		x = m_ControlPoints[pid].X/m_Scale_X + m_Origin_X;
		y = m_ControlPoints[pid].Y/m_Scale_Y + m_Origin_Y;
	}
}

void ui::CCurveTuner::CCurve::RemoveControlPoint(UINT_PTR cp_handle)
{
	int pid = (int)(((Gdiplus::PointF*)cp_handle) - m_ControlPoints);
	if( pid>0 && pid<(int)m_ControlPointCount-1 )
	{
		memmove(&m_ControlPoints[pid],&m_ControlPoints[pid+1],(m_ControlPointCount-pid)*sizeof(m_ControlPoints[0]));
		m_ControlPointCount--;

		m_Function.SetSize();
		ASSERT(m_pTuner);
		m_pTuner->SendNotify(NOTIFY_FUNCTION_CHANGED,m_Id);
	}
}

UINT_PTR ui::CCurveTuner::CCurve::HitControlPoint(float x,float y,float distance_max) //return hitted CP or NULL for non-hitted
{
	FitInLayout(x,y);

	float min_dist = FLT_MAX;
	int pid;
	for(UINT i=0; i<m_ControlPointCount; i++)
	{
		float dist = ::rt::Sqr(x-m_ControlPoints[i].X)+::rt::Sqr(y-m_ControlPoints[i].Y);
		if( dist > min_dist ){}
		else{ min_dist = dist; pid = i; }
	}

	if( sqrt(min_dist) < distance_max*max(m_Scale_X,m_Scale_Y) )
		return (UINT_PTR)&m_ControlPoints[pid];
	else
		return NULL;
}

void ui::CCurveTuner::CCurve::Plot(Gdiplus::Graphics & gfx,Gdiplus::Font* pFont,UINT_PTR hot_cp,BOOL Selected)
{
	//Scale 
	{
		float sx = 1.0f/m_Scale_X;
		float sy = 1.0f/m_Scale_Y;
		for(UINT i=0;i<m_ControlPointCount;i++)
		{
			m_ControlPointScaled[i].X = m_ControlPoints[i].X*sx;
			m_ControlPointScaled[i].Y = m_ControlPoints[i].Y*sy;
		}
	}

	Gdiplus::Color line_color(0,0,0);
	Gdiplus::Pen pen(line_color);

	if( Selected )
	{
		line_color = Color(min(255,m_LineColor.a*2),m_LineColor.x,m_LineColor.y,m_LineColor.z);
		pen.SetColor(line_color);
		pen.SetWidth(2.5f*m_LineWidth);
		pen.SetDashStyle( Gdiplus::DashStyleDashDot );

		// Title
		{
			static const LPCWSTR curve_mode[] = 
			{
				L"Cardinal Spline",
				L"Line Segment",
				L"Bezier Curve"
			};

			WCHAR Title[1024];
			if( !m_Locked )
				swprintf(Title,L"[ %s ] %5.3fX\15\12 - %s",m_Name,m_OutputValueScale,curve_mode[m_CurveMode]);
			else
				swprintf(Title,L"[ %s ] %5.3fX (Locked)\15\12 - %s",m_Name,m_OutputValueScale,curve_mode[m_CurveMode]);

			StringFormat  sf;
			Gdiplus::SolidBrush brush(Color(200,40,40,40));
			gfx.DrawString( Title, -1, pFont, 
							PointF(m_Origin_X,m_Origin_Y + 1.0f/m_Scale_Y), 
							&sf,&brush );
		}
	}
	else
	{
		hot_cp = NULL;
		line_color = Color(m_LineColor.a,m_LineColor.x,m_LineColor.y,m_LineColor.z);
		pen.SetColor(line_color);
		pen.SetWidth(2.0f*m_LineWidth);
		pen.SetDashStyle( Gdiplus::DashStyleSolid );
	}

	Matrix org_trans;
	gfx.GetTransform(&org_trans);
		gfx.SetSmoothingMode( Gdiplus::SmoothingModeAntiAlias );
		gfx.TranslateTransform(m_Origin_X,m_Origin_Y);

		switch(m_CurveMode)
		{
		case CurveMode_CardinalSpline:
			gfx.DrawCurve(&pen,m_ControlPointScaled,m_ControlPointCount);
			break;
		case CurveMode_LineSegment:
			gfx.DrawLines(&pen,m_ControlPointScaled,m_ControlPointCount);
			break;
		case CurveMode_BezierCurve:
			if( (m_ControlPointCount-1)%3 == 0 )
				gfx.DrawBeziers(&pen,m_ControlPointScaled,m_ControlPointCount);
			else
				gfx.DrawLines(&pen,m_ControlPointScaled,m_ControlPointCount);
			break;
		}

		if( !m_Locked )
		{
			pen.SetDashStyle( Gdiplus::DashStyleSolid );
			gfx.SetSmoothingMode( Gdiplus::SmoothingModeDefault );
			RectF rc;
			rc.Width = 5;
			rc.Height = 5;
			for(UINT i=0;i<m_ControlPointCount;i++)
			{
				rc.X = m_ControlPointScaled[i].X - 2;
				rc.Y = m_ControlPointScaled[i].Y - 2;

				if( hot_cp != (UINT_PTR)&m_ControlPoints[i] )
				{
					pen.SetWidth(1.5f*m_LineWidth);
					gfx.DrawRectangle(&pen,rc);
				}
				else
				{	Gdiplus::SolidBrush brush(line_color);
					RectF rc2 = rc;
					rc2.Offset(-0.5,-0.5);
					rc2.Width+=1.0f;
					rc2.Height+=1.0f;
					gfx.FillRectangle(&brush,rc2);
				}
			}
		}
	gfx.SetTransform(&org_trans);
}

void ui::CCurveTuner::CCurve::SetLayout(float x1,float y1,float x2,float y2)
{
	m_Origin_X = x1;
	m_Origin_Y = y1;
	m_Scale_X = 1.0f/(x2-x1);
	m_Scale_Y = 1.0f/(y2-y1);
}

void ui::CCurveTuner::CCurve::ObtainFunction( ::rt::Buffer<float> & function_table, int Res )
{
	BOOL bInternUse = (Res == -52);

	if( Res < 0 )Res = (int)min(20*m_ControlPointCount,2.0f/m_Scale_X);
	
	ATL::CImage img;
	img.Create(Res,-Res,24);
	ZeroMemory(img.GetBits(),Res*img.GetPitch());

	Pen pen(Color(255,255,255),3.0f);
	float scale = (Res-1.0f);

	{
		Graphics	gfx(img.GetDC());
		// Plot
		pen.SetWidth(1.0f/scale);
		gfx.TranslateTransform(-0.5f,-0.5f);
		gfx.ScaleTransform(scale+1,scale+1);
		{
			Matrix rot;
			rot.RotateAt(90.0f,PointF(0.5f,0.5f));
			gfx.MultiplyTransform(&rot);
		}
		gfx.SetSmoothingMode( Gdiplus::SmoothingModeAntiAlias );
		switch(m_CurveMode)
		{
		case CurveMode_CardinalSpline:
			gfx.DrawCurve(&pen,m_ControlPoints,m_ControlPointCount);
			break;
		case CurveMode_LineSegment:
			gfx.DrawLines(&pen,m_ControlPoints,m_ControlPointCount);
			break;
		case CurveMode_BezierCurve:
			if( (m_ControlPointCount-1)%3 == 0 )
				gfx.DrawBeziers(&pen,m_ControlPoints,m_ControlPointCount);
			else
				gfx.DrawLines(&pen,m_ControlPoints,m_ControlPointCount);
			break;
		}
		img.ReleaseDC();
	}

	function_table.SetSize(Res);

	if( bInternUse )
	{
		float last_value = 0;
		LPBYTE pImg = (LPBYTE)img.GetBits();
		for(int i=0;i<Res;i++,pImg+=img.GetPitch())
		{
			float tot = 0;
			float wei = 0;
			LPBYTE pLine = pImg;
			LPBYTE pEnd = &pImg[3*Res];
			for(int x=0;pLine<pEnd;pLine+=3,x++)
			{
				tot += *pLine*(1.0f-x/(Res-1.0f));
				wei += *pLine;
			}

			if(wei>EPSILON)
				last_value = function_table[i] = min(1.0f,max(0.0f, (tot/wei - 0.5f/Res)*Res/(Res-1.0f) ));
			else
				function_table[i] = last_value>0.5f?1.0f:0.0f;
		}
	}
	else
	{
		float last_value = 0;
		LPBYTE pImg = (LPBYTE)img.GetBits();
		for(int i=0;i<Res;i++,pImg+=img.GetPitch())
		{
			float tot = 0;
			float wei = 0;
			LPBYTE pLine = pImg;
			LPBYTE pEnd = &pImg[3*Res];
			for(int x=0;pLine<pEnd;pLine+=3,x++)
			{
				tot += *pLine*(1.0f-x/(Res-1.0f));
				wei += *pLine;
			}
			if(wei>EPSILON)
				last_value = function_table[i] = min(1.0f,max(0.0f, m_OutputValueScale*(tot/wei - 0.5f/Res)*Res/(Res-1.0f) ));
			else
				function_table[i] = last_value>0.5f?1.0f:0.0f;
		}
	}
}

void ui::CCurveTuner::CCurve::GetFunctionTable(::rt::Buffer<float> & func)
{	
	if( m_Function.GetSize() ){}
	else{ ObtainFunction(m_Function,400); }

	func.SetSize(m_Function.GetSize());
	func = m_Function;
}

float ui::CCurveTuner::CCurve::GetCurveFunctionValue(float x)  // 0<=x<=1
{	
	ASSERT(x>=0.0f && x<=1.0f);
	if( m_Function.GetSize() ){}
	else{ ObtainFunction(m_Function,400); }

	return m_Function.GetInterpolatedValue(x);
}


float ui::CCurveTuner::CCurve::ProposeNewControlPointPostion( float x )
{
	if( m_Function.GetSize() ){}
	else{ ObtainFunction(m_Function); }

	x = (x-m_Origin_X)*m_Scale_X;

	if( x>=0.0f && x<=1.0f )
		return (m_Function.GetInterpolatedValue(x))/m_Scale_Y + m_Origin_Y;
	else
		return -1e10;
}

UINT_PTR ui::CCurveTuner::CCurve::GetNextControlPoint(UINT_PTR cp_handle)
{
	int pid = int(((Gdiplus::PointF*)cp_handle) - m_ControlPoints);
	pid++;

	return (UINT_PTR)&m_ControlPoints[pid%m_ControlPointCount];
}

UINT_PTR ui::CCurveTuner::CCurve::GetPrevControlPoint(UINT_PTR cp_handle)
{
	int pid = int(((Gdiplus::PointF*)cp_handle) - m_ControlPoints);
	pid--;

	return (UINT_PTR)&m_ControlPoints[pid%m_ControlPointCount];
}

BOOL ui::CCurveTuner::CCurve::SetControlPointBuffer(const float* pControlPoints, int co)
{	ASSERT_ARRAY( pControlPoints, co*2 );
	if( co > MAX_CONTROL_POINT )return FALSE;
	if( *pControlPoints > EPSILON || *pControlPoints < -EPSILON )return FALSE;
	if( ( pControlPoints[(co-1)*2] > 1.0f + EPSILON) || (pControlPoints[(co-1)*2] < 1.0f - EPSILON) )return FALSE;
	
	m_ControlPointCount = co;
	memcpy(m_ControlPoints,pControlPoints,sizeof(float)*2*co);

	m_Function.SetSize();
	ASSERT(m_pTuner);
	m_pTuner->RedrawContent();
	m_pTuner->SendNotify(NOTIFY_FUNCTION_CHANGED,m_Id);

	return TRUE;
}

BOOL ui::CCurveTuner::CCurve::Load(LPCTSTR fn)
{
	w32::CFile64 file;
	if(file.Open(fn))
	{
		int cp = 0;
		file.Read(&cp,sizeof(cp));
		if( cp <= MAX_CONTROL_POINT && cp >= 2 )
		{
			m_ControlPointCount = cp;
			file.Read(m_ControlPoints,sizeof(m_ControlPoints));
			file.Read(&m_OutputValueScale,sizeof(m_OutputValueScale));
			file.Read(&m_CurveMode,sizeof(m_CurveMode));

			if( !file.ErrorOccured() )
			{
				m_Function.SetSize();
				ASSERT(m_pTuner);
				m_pTuner->SendNotify(NOTIFY_FUNCTION_CHANGED,m_Id);

				return TRUE;
			}

		}
	}

	return FALSE;
}

BOOL ui::CCurveTuner::CCurve::Save(LPCTSTR fn)
{
	w32::CFile64 file;
	if(file.Open(fn,w32::CFile64::Normal_Write))
	{
		file.Write(&m_ControlPointCount,sizeof(m_ControlPointCount));
		file.Write(m_ControlPoints,sizeof(m_ControlPoints));
		file.Write(&m_OutputValueScale,sizeof(m_OutputValueScale));
		file.Write(&m_CurveMode,sizeof(m_CurveMode));
			
		return !file.ErrorOccured();
	}

	return FALSE;
}
