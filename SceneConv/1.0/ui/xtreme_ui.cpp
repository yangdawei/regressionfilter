#include "stdafx.h"
#include "xtreme_ui.h"
#include "..\rt\runtime_base.h"
#include <CommandBars\XTPDockContext.h>

#ifdef CFrameWnd
	#undef CFrameWnd
#endif

#ifdef CMDIFrameWnd
	#undef CMDIFrameWnd
#endif


#define IMPLEMENT_RUNTIMECLASS_MY(real_cls, class_name, base_class_name, wSchema, pfnNew, class_init)	\
	CRuntimeClass* ui::xt::_FrameWnd::real_cls::GetRuntimeClass() const									\
		{ return ((CRuntimeClass*)(&::ui::xt::_FrameWnd::real_cls::class##class_name)); }				\
	AFX_COMDAT const CRuntimeClass ui::xt::_FrameWnd::real_cls::class##class_name = {					\
			#class_name, sizeof(class_name), wSchema, pfnNew,											\
			RUNTIME_CLASS(base_class_name), NULL, class_init };											\

IMPLEMENT_RUNTIMECLASS_MY(_XtremeFrameWnd,CXtremeFrameWnd, CFrameWnd, 0xFFFF, NULL, NULL)
IMPLEMENT_RUNTIMECLASS_MY(_XtremeMDIFrameWnd,CXtremeMDIFrameWnd, CFrameWnd, 0xFFFF, NULL, NULL)
// Set project setting [Use of MFC] to "Use MFC in a Static Library", when C2440 error occurs.

#undef IMPLEMENT_RUNTIMECLASS_MY

const LPCTSTR ui::xt::_meta_::_XtremeFrameWndHook::g_PanelLayout = _T("PanelLayout");
BOOL ui::xt::g_EnableFrameLayoutPersistent = TRUE;
BOOL ui::xt::g_EnableLayoutCustomizeDialog = FALSE;
BOOL ui::xt::g_EnableSplittingUpdate = TRUE;
BOOL ui::xt::g_EnableAlphaDockingIndicator = TRUE;
BOOL ui::xt::g_EnableDockingStickers = FALSE;

void ui::xt::_meta_::_XtremeFrameWndHook::_CreatePanelHost(CXTPDockingPane* panel)
{	
	ASSERT(panel);
	uix::_meta_::_PanelHostWnd* pHostWnd = new uix::_meta_::_PanelHostWnd;
	VERIFY(pHostWnd->Create(NULL,_T("PanelHostWnd@VCFC"),WS_CHILD,CRect(0,0,100,100),m_pWnd,1001));
	pHostWnd->SetWindowText(panel->GetTitle());
	ASSERT(panel);
	panel->Attach(pHostWnd);
	pHostWnd->m_pBindedPanelManager = m_pDockingPaneManager;
	pHostWnd->m_pBindedPanelCtrlId = panel->GetID();
}

void ui::xt::_meta_::_XtremeFrameWndHook::_EnableDockingManager()
{	
	if(!m_pDockingPaneManager)
	{	m_pDockingPaneManager = new CXTPDockingPaneManager;
		ASSERT(m_pDockingPaneManager);
		m_pDockingPaneManager->InstallDockingPanes(m_pWnd);
		m_pDockingPaneManager->SetTheme(xtpPaneThemeOffice2003);
		m_pDockingPaneManager->SetThemedFloatingFrames(TRUE);

		m_pDockingPaneManager->UseSplitterTracker(g_EnableSplittingUpdate);

		if(g_EnableDockingStickers)
		{
			m_pDockingPaneManager->SetAlphaDockingContext(TRUE);
			m_pDockingPaneManager->SetShowDockingContextStickers(TRUE);
		}
		else
		{
			m_pDockingPaneManager->SetAlphaDockingContext(g_EnableAlphaDockingIndicator);
		}
	}
}

CXTPCommandBars* ui::xt::_meta_::_XtremeFrameWndHook::GetCommandBars()
{
	return _pCommandBars;
}

CXTPDockingPaneManager*	ui::xt::_meta_::_XtremeFrameWndHook::GetPaneManager()
{
	return m_pDockingPaneManager;
}

ui::xt::_meta_::_XtremeFrameWndHook::_XtremeFrameWndHook(CFrameWnd* pWnd)
{	
	ASSERT(pWnd);
	m_pWnd = pWnd;
	m_pDockingPaneManager = NULL;
	m_pStatusBar = NULL;
	m_NextDynPanelId = 0;
	m_PanelMenuIdBase = 0;
	_pCommandBars = NULL;
}

ui::xt::_meta_::_XtremeFrameWndHook::~_XtremeFrameWndHook()
{	
	if(m_pDockingPaneManager)delete m_pDockingPaneManager;
	if(m_pStatusBar)delete m_pStatusBar;
}

void ui::xt::_meta_::_XtremeFrameWndHook::DestoryDynamicPanel(UINT panel_id)
{
	CXTPDockingPane * p = GetPanel(panel_id);
	m_pDockingPaneManager->DestroyPane(p);
}

void ui::xt::_meta_::_XtremeFrameWndHook::AttachPanel(UINT id, UINT base_id)
{
	CXTPDockingPaneBase * p1 = GetPanel(id);
	CXTPDockingPaneBase * p2 = GetPanel(base_id);
	ASSERT(p1);
	ASSERT(p2);

	m_pDockingPaneManager->AttachPane(p1,p2);
}

void ui::xt::_meta_::_XtremeFrameWndHook::ShowPanel(UINT id, BOOL bshow)
{
	CXTPDockingPane * p = GetPanel(id);
	ASSERT(p);
	if(bshow)
		m_pDockingPaneManager->ShowPane(p);
	else
		m_pDockingPaneManager->ClosePane(p);
}

BOOL ui::xt::_meta_::_XtremeFrameWndHook::IsPanelShown(UINT id)
{
	CXTPDockingPane * p = GetPanel(id);
	ASSERT(p);
	return !m_pDockingPaneManager->IsPaneClosed(p);
}

UINT ui::xt::_meta_::_XtremeFrameWndHook::CreateDynamicPanel(LPCTSTR title)	// return panel_id
{	
	ASSERT(title);
	_EnableDockingManager();
	CXTPDockingPane* pPanel = 
		m_pDockingPaneManager->CreatePane(m_NextDynPanelId+ID_DYNPANEL_ADD+ID_PANEL_BASE, CRect(0, 0,200, 120), xtpPaneDockLeft);
	pPanel->SetTitle(title);
	_CreatePanelHost(pPanel);

	m_NextDynPanelId++;
	return m_NextDynPanelId+ID_DYNPANEL_ADD-1;
}

uix::CDockingPane* ui::xt::_meta_::_XtremeFrameWndHook::GetPanel(UINT id)
{	
	if(m_pDockingPaneManager)
		return (uix::CDockingPane*)m_pDockingPaneManager->FindPane(ID_PANEL_BASE+id);
	return NULL;
}

void ui::xt::_meta_::_XtremeFrameWndHook::DockPanel(UINT id, DWORD pose, UINT relative_panel_id)
{
	uix::CDockingPane* p = GetPanel(id);
	ASSERT(p);

	uix::CDockingPane* neigh = NULL;
	if(relative_panel_id!=INFINITE)VERIFY(neigh = GetPanel(relative_panel_id));

	m_pDockingPaneManager->DockPane(p,(XTPDockingPaneDirection)pose,neigh);
}

BOOL ui::xt::_meta_::_XtremeFrameWndHook::CreatePanels(const LPCTSTR* pPanelTitle, UINT panel_count, const SIZE* pPanelSizes, UINT menu_id_base) // also clear ids
{
	ASSERT(panel_count);
	_EnableDockingManager();
	if(pPanelSizes)
	{	for(UINT i=0;i<panel_count;i++)
		{	CRect rc(0,0,pPanelSizes[i].cx,pPanelSizes[i].cy);
			m_pDockingPaneManager->CreatePane(i+ID_PANEL_BASE, rc, xtpPaneDockLeft);
		}
	}
	else
	{	for(UINT i=0;i<panel_count;i++)
			m_pDockingPaneManager->CreatePane(i+ID_PANEL_BASE, CRect(0, 0,200, 200), xtpPaneDockLeft);
	}

	ASSERT(m_pDockingPaneManager);
	BOOL layout_loaded = FALSE;

	if(uix::g_EnableFrameLayoutPersistent)
	{	CXTPDockingPaneLayout* pLayout = m_pDockingPaneManager->CreateLayout();
		if(pLayout->Load(g_PanelLayout) && pLayout->GetPaneList().GetCount()==panel_count)
		{
			m_pDockingPaneManager->SetLayout(pLayout);
			layout_loaded = TRUE;
		}
		delete pLayout;
	}

	for(UINT i=0;i<panel_count;i++)
	{	uix::CDockingPane* p = GetPanel(i);
		_CreatePanelHost(p);
		if(pPanelTitle[i])
			p->SetTitle(pPanelTitle[i]);
	}
	m_PanelMenuIdBase = menu_id_base;
	_SetupPanelMenu();

	return layout_loaded;
}

void ui::xt::_meta_::_XtremeFrameWndHook::HideView(BOOL hide)
{
	if(m_pDockingPaneManager)
		return m_pDockingPaneManager->HideClient(hide);
}

BOOL ui::xt::_meta_::_XtremeFrameWndHook::SetMenuBar(UINT menu_resource)
{	
	m_MenuResourceId = menu_resource;
	CXTPCommandBars* pCommandBars = GetCommandBars();
	ASSERT(pCommandBars);
	if(NULL!=pCommandBars->SetMenu(_T("Menu Bar"),menu_resource))
	{
		_SetupPanelMenu();
		return TRUE;
	}
	return FALSE;
}

void ui::xt::_meta_::_XtremeFrameWndHook::_SetupPanelMenu()
{	
	CMenu menu;
	CMenu menu_popup;

	CXTPCommandBars* pCommandBars = GetCommandBars();
	if(pCommandBars)
	{
		CXTPMenuBar* pMenuBar = pCommandBars->GetMenuBar();
		if(pMenuBar && m_pDockingPaneManager)
		{	ASSERT(m_PanelMenuIdBase);
			VERIFY(menu_popup.CreateMenu());
			CXTPDockingPaneInfoList& myList = m_pDockingPaneManager->GetPaneList();
			UINT total_len = (UINT)myList.GetCount();
			POSITION pos = myList.GetHeadPosition();
			m_PanelMenuIdMax = m_PanelMenuIdBase;
			for(UINT i=0;i<total_len;i++)
			{	CXTPDockingPane* pane = myList.GetNext(pos).pPane;
				UINT ctrl_id = pane->GetID();
				if(ctrl_id<ID_PANEL_BASE+ID_DYNPANEL_ADD && ctrl_id>=ID_PANEL_BASE)
				{
					menu_popup.InsertMenu(-1,MF_BYPOSITION|MF_STRING,m_PanelMenuIdBase+ctrl_id-ID_PANEL_BASE,pane->GetTitle());
					m_PanelMenuIdMax = max(m_PanelMenuIdMax,m_PanelMenuIdBase+i);
				}
			}

			if(g_EnableLayoutCustomizeDialog)
			{
				menu_popup.InsertMenu(-1,MF_BYPOSITION|MF_SEPARATOR);
				menu_popup.InsertMenu(-1,MF_BYPOSITION|MF_STRING,m_PanelMenuIdBase+ID_DYNPANEL_ADD,_T("Customize ..."));
			}
			
			if(menu.LoadMenu(m_MenuResourceId))
			{
				menu.InsertMenu(menu.GetMenuItemCount()-1,
								MF_BYPOSITION|MF_STRING|MF_POPUP,
								(UINT_PTR)menu_popup.GetSafeHmenu(),
								_T("Panels"));

				pMenuBar->LoadMenu(&menu);
			}
		}
	}
}

void ui::xt::_meta_::_XtremeFrameWndHook::SetCommandBarsObject(CXTPCommandBars* p)
{
	_pCommandBars = p;
}

void ui::xt::_meta_::_XtremeFrameWndHook::FixChildWindowEdge(CWnd * pv)
{
	if(::IsZoomed(*pv))
	{	if( !(pv->GetExStyle() & WS_EX_CLIENTEDGE) )
			pv->ModifyStyleEx(0,WS_EX_CLIENTEDGE);
	}
	else
	{
		if( pv->GetExStyle() & WS_EX_CLIENTEDGE )
			pv->ModifyStyleEx(WS_EX_CLIENTEDGE,0);
	}
}

CXTPToolBar* ui::xt::_meta_::_XtremeFrameWndHook::SetToolBar(LPCTSTR title, UINT toolbar_resource)
{	CXTPCommandBars* pCommandBars = GetCommandBars();
	CXTPToolBar* pCommandBar = (CXTPToolBar*)pCommandBars->Add(title, xtpBarTop);
	if(pCommandBar)
	{	if(toolbar_resource)
		{	if(pCommandBar->LoadToolBar(toolbar_resource))
				return pCommandBar;
			else
				pCommandBars->Remove(pCommandBar);
		}
		else return pCommandBar;
	}

	return NULL;
}

void ui::xt::_meta_::_XtremeFrameWndHook::DisableDragInDockContext(BOOL disable)
{
	__if_exists(CXTPDockContext::_DisableDraging)
	{
		CXTPDockContext::_DisableDraging = disable;
	}
}

void ui::xt::_meta_::_XtremeFrameWndHook::SetStatusBarIndirector(const UINT* lpIDArray, int nIDCount)
{	
	if(!m_pStatusBar)
	{	m_pStatusBar = new CXTPStatusBar;
		VERIFY(m_pStatusBar->Create(m_pWnd));
	}
	VERIFY(m_pStatusBar->SetIndicators(lpIDArray,nIDCount));
	m_pStatusBar->UseCommandBarsTheme(TRUE);
}

void ui::xt::_meta_::_XtremeFrameWndHook::SetStatusPaneInfo(DWORD cmd_id, int width, UINT style)
{	
	ASSERT(m_pStatusBar);
	m_pStatusBar->SetPaneInfo(m_pStatusBar->CommandToIndex(cmd_id),cmd_id, style, width );
}

void ui::xt::_meta_::_XtremeFrameWndHook::SetStatusPaneText(DWORD cmd_id, LPCTSTR str, BOOL auto_resize_pane)
{
	if(m_pStatusBar && ::IsWindow(*m_pStatusBar))
	{
		int idx = m_pStatusBar->CommandToIndex(cmd_id);
		m_pStatusBar->SetPaneText(idx, str);
		if(auto_resize_pane)
		{	
			CDC *pDC = m_pStatusBar->GetDC();
			pDC->GetTextExtent(str,(int)_tcslen(str));
			m_pStatusBar->SetPaneWidth(idx, (int)(pDC->GetTextExtent(str,(int)_tcslen(str)).cx*0.8f));
			m_pStatusBar->ReleaseDC(pDC);
		}
	}
}

BOOL ui::xt::_meta_::_XtremeFrameWndHook::OnCmdMsg(UINT nID, int nCode, void* pExtra, AFX_CMDHANDLERINFO* pHandlerInfo)
{	
	if(nCode==CN_UPDATE_COMMAND_UI)
	{
		if(nID>=m_PanelMenuIdBase && nID<=m_PanelMenuIdMax)
		{	CCmdUI* pCmdUI = (CCmdUI*)pExtra;
			ASSERT(pCmdUI);
			UINT pan_id = nID-m_PanelMenuIdBase;
			uix::CDockingPane* p = GetPanel(pan_id);
			if(p)
			{	pCmdUI->Enable();
				pCmdUI->SetCheck(!m_pDockingPaneManager->IsPaneClosed(p));
			}
			else
				pCmdUI->Enable(FALSE);
			return TRUE;
		}
		else if(m_PanelMenuIdBase+ID_DYNPANEL_ADD == nID)
		{	CCmdUI* pCmdUI = (CCmdUI*)pExtra;
			ASSERT(pCmdUI);
			pCmdUI->Enable();
			return TRUE;
		}
	}
	return FALSE;
}

LRESULT ui::xt::_meta_::_XtremeFrameWndHook::WindowProc(UINT message, WPARAM wParam, LPARAM lParam)
{	static const LPCTSTR MainFrmSection = _T("MainFrame");
	static const LPCTSTR MainFrmIsMaximized = _T("IsMaximized");
	static const LPCTSTR MainFrmRect = _T("Rect");
	static const LPCTSTR CommandBarLayout = _T("CommandBars");
	switch(message)
	{
	case WM_CREATE:
		{	
			XTPPaintManager()->SetTheme(xtpThemeWhidbey);

			if(uix::g_EnableFrameLayoutPersistent)
			{
				GetCommandBars()->LoadOptions(CommandBarLayout);
				GetCommandBars()->LoadBarState(CommandBarLayout,TRUE);
				// Load mainframe window layout
				BOOL is_maximum;
				RECT rc;
				HKEY key = AfxGetApp()->GetSectionKey(MainFrmSection);

				DWORD len = sizeof(DWORD);  DWORD type = REG_DWORD;
				if(ERROR_SUCCESS==RegQueryValueEx(key,MainFrmIsMaximized,0,&type,(LPBYTE)&is_maximum,&len))
					if(is_maximum)m_pWnd->ShowWindow(SW_MAXIMIZE);

				len = sizeof(RECT);  type = REG_BINARY;
				if(ERROR_SUCCESS==RegQueryValueEx(key,MainFrmRect,0,&type,(LPBYTE)&rc,&len))
				{
					if(rc.top < rc.bottom-64 && rc.left < rc.right-64)
						m_pWnd->SetWindowPos(NULL,rc.left,rc.top,rc.right-rc.left,rc.bottom-rc.top,SWP_NOZORDER);
				}
				RegCloseKey(key);
			}
		}
		break;
	case WM_DESTROY:
		if(uix::g_EnableFrameLayoutPersistent)
		{	// Save mainframe window layout
			// m_pWnd->ShowWindow(SW_HIDE);
			ASSERT(m_pWnd);
			BOOL is_maximum = ::IsZoomed(*m_pWnd);
			m_pWnd->ShowWindow(SW_NORMAL);
			RECT rc;
			m_pWnd->GetWindowRect(&rc);
			HKEY key = AfxGetApp()->GetSectionKey(MainFrmSection);
			RegSetValueEx(key,MainFrmIsMaximized,0,REG_DWORD,(LPCBYTE)&is_maximum,sizeof(DWORD));
			RegSetValueEx(key,MainFrmRect,0,REG_BINARY,(LPCBYTE)&rc,sizeof(rc));
			RegCloseKey(key);
			GetCommandBars()->SaveOptions(CommandBarLayout);
			GetCommandBars()->SaveBarState(CommandBarLayout);
		}
		if(m_pDockingPaneManager)
		{	// remove all dynamic panels
			CXTPDockingPaneInfoList& myList = m_pDockingPaneManager->GetPaneList();

			typedef CXTPDockingPane* LPCXTPDockingPane;
			UINT total_len = (UINT)myList.GetCount();

			LPCXTPDockingPane* dyn_pane = new LPCXTPDockingPane[total_len];
			UINT sz = 0;
			POSITION pos = myList.GetHeadPosition();
			for(UINT i=0;i<total_len;i++)
			{	CXTPDockingPane* pane = myList.GetNext(pos).pPane;
				if(pane->GetID()>=ID_PANEL_BASE+ID_DYNPANEL_ADD)
					dyn_pane[sz++] = pane;
			}
			for(UINT i=0;i<sz;i++)
				m_pDockingPaneManager->DestroyPane(dyn_pane[i]);
			delete [] dyn_pane;

			if(uix::g_EnableFrameLayoutPersistent)
			{
				CXTPDockingPaneLayout* pLayout = m_pDockingPaneManager->CreateLayout();
				m_pDockingPaneManager->GetLayout(pLayout);
				pLayout->Save(g_PanelLayout);
				delete pLayout;
			}
		}
		break;
	case WM_COMMAND:
		if(wParam>=m_PanelMenuIdBase && wParam<=m_PanelMenuIdMax)
		{	ASSERT(m_pDockingPaneManager);
			uix::CDockingPane* p = GetPanel(((UINT)wParam)-m_PanelMenuIdBase);
			if(p)
			{	if(m_pDockingPaneManager->IsPaneClosed(p))
					m_pDockingPaneManager->ShowPane(p);
				else
					m_pDockingPaneManager->ClosePane(p);
			}
		}
		else if(wParam == m_PanelMenuIdBase+ID_DYNPANEL_ADD && g_EnableLayoutCustomizeDialog)
		{	CXTPCommandBars* pCommandBars = GetCommandBars();
			if(pCommandBars != NULL)
			{
				// Instanciate the customize dialog object.
				CXTPCustomizeSheet dlg(pCommandBars);

				// Add the options page to the customize dialog.
				CXTPCustomizeOptionsPage pageOptions(&dlg);
				dlg.AddPage(&pageOptions);

				// Add the commands page to the customize dialog.
				CXTPCustomizeCommandsPage* pCommands = dlg.GetCommandsPage();
				//pCommands->AddCategories(IDR_PaneTYPE);

				// Use the command bar manager to initialize the
				// customize dialog.
				pCommands->InsertAllCommandsCategory();
				//pCommands->InsertBuiltInMenus(IDR_PaneTYPE);
				pCommands->InsertNewMenuCategory();

				// Dispaly the dialog.
				dlg.DoModal();
			}
		}
		break;
	}

	return 0;
}

LRESULT CALLBACK ui::xt::_meta_::_PanelHostWnd::WndProc_SubclassHostee(HWND hwnd, UINT uMsg, WPARAM wParam, LPARAM lParam)
{
	#pragma warning(disable:4312)
	ui::xt::_meta_::_PanelHostWnd* This = (ui::xt::_meta_::_PanelHostWnd*)::GetWindowLongPtr(hwnd,GWLP_USERDATA);
	#pragma warning(default:4312)

	if( !IsBadReadPtr(This,sizeof(ui::xt::_meta_::_PanelHostWnd)) && This->m_hHostedWnd == hwnd )
	{
		// Handle hosting modes
		int HostingMode = This->m_HostingBehavior;

		// Call orignal window procedure
		LRESULT ret = CallWindowProc(This->m_pHostedWndProc,hwnd,uMsg,wParam,lParam);

		if( uMsg == WM_SETTEXT )
		{
			if(This->m_pBindedPanelManager)
			{	CXTPDockingPane* p = This->m_pBindedPanelManager->FindPane(This->m_pBindedPanelCtrlId);
				if(p)p->SetTitle((LPCTSTR)lParam);
			}
		}
		else if( HostingMode_FreeMoving&HostingMode ) //allow moving
		{
			if( uMsg == WM_LBUTTONDOWN )
			{	//decide moveable
				CRect rc_clientarea;
				This->GetClientRect(rc_clientarea);

				if(	   ( (HostingMode & HostingMode_HorizontalMoving) && rc_clientarea.Width()< (int)This->m_HostedWndWidth ) 
					|| ( (HostingMode & HostingMode_VerticalMoving) && rc_clientarea.Height() < (int)This->m_HostedWndHeight) 
					)
				{
					::SetFocus(hwnd);
					::SendMessage(hwnd,WM_NCLBUTTONDOWN,HTCAPTION,lParam);
				}
			}
			else if( uMsg == 0x020A && ret!=0xffff ) //WM_MOUSEWHEEL 
			{
				int offset = ((short)HIWORD(wParam))/4;
                
				CRect rc;
				::GetWindowRect(hwnd,rc);
				rc.OffsetRect(CSize(offset,offset));

				This->ScreenToClient(rc);
				This->_FixHostedWindowArea(rc);

				::SetWindowPos(hwnd,NULL,rc.left,rc.top,0,0,SWP_NOZORDER|SWP_NOSIZE);
			}
			else if( uMsg == WM_MOVING )
			{
				CRect& hw = *((CRect*)lParam);

				This->ScreenToClient(hw);
				This->_FixHostedWindowArea(hw);
				This->ClientToScreen(hw);
			}
		}

		return ret;
	}
	else
	{ ASSERT(0); } //GWLP_USERDATA is modified by user (via SetWindowLongPtr/SetWindowLong)

	return 0;
}

void ui::xt::_meta_::_PanelHostWnd::_PositioningHostedWnd(int cx, int cy)
{
	if( (m_HostingBehavior & HostingMode_FreeMoving) == HostingMode_FreeMoving ){}
	else 
	{	CRect rc;
		::GetWindowRect(m_hHostedWnd,rc);
		ScreenToClient(rc);

		int w=cx,h=cy;
		int x,y;
		if(m_HostingBehavior & HostingMode_HorizontalMoving)
		{
			w = max((int)m_HostedWndWidth,cx);
			x = max(rc.left,min(0,cx - w));
		}
		else{ x=0; }

		if(m_HostingBehavior & HostingMode_VerticalMoving)
		{
			h = max((int)m_HostedWndHeight,cy);
			y = max(rc.top,min(0,cy - h));
		}
		else{ y=0; }

		::SetWindowPos(m_hHostedWnd,NULL,x,y,w,h,SWP_NOZORDER);
	}
}

LRESULT ui::xt::_meta_::_PanelHostWnd::WindowProc(UINT message, WPARAM wParam, LPARAM lParam)
{	
	switch(message)
	{
	case WM_NCDESTROY:
		m_hWnd = NULL;
		delete this;
		return 0;
		break;
	case WM_PAINT:
		if(m_hHostedWnd){}
		else
		{	RECT rc;
			GetClientRect(&rc);
			CClientDC dc(this);
			::FillRect(dc.GetSafeHdc(),&rc,GetSysColorBrush(COLOR_WINDOW));
		}
		break;
	case WM_SIZE:
		if( m_hHostedWnd )_PositioningHostedWnd(LOWORD(lParam), HIWORD(lParam));
		break;
	case WM_LBUTTONDOWN:
		SetFocus();
		break;
	case WM_SETTEXT:
		{	if(m_pBindedPanelManager)
			{	CXTPDockingPane* p = m_pBindedPanelManager->FindPane(m_pBindedPanelCtrlId);
				if(p)p->SetTitle((LPCTSTR)lParam);
			}
		}
		break;
	}
	return __super::WindowProc(message,wParam,lParam);
}

void ui::xt::_meta_::_PanelHostWnd::_FixHostedWindowArea(CRect& hw)
{
	CRect rc_clientarea;
	GetClientRect(rc_clientarea);

	int x=0,y=0;
	if(m_HostingBehavior&HostingMode_HorizontalMoving)
		x = min(0,max(hw.left,rc_clientarea.Width()-(int)m_HostedWndWidth));
	if(m_HostingBehavior&HostingMode_VerticalMoving)
		y = min(0,max(hw.top,rc_clientarea.Height()-(int)m_HostedWndHeight));

	hw.MoveToXY(x,y);
}


ui::xt::_meta_::_PanelHostWnd::_PanelHostWnd()
{
	m_hHostedWnd = NULL;
	m_pHostedWndProc = NULL;
	m_HostingBehavior = 0;
	m_pBindedPanelManager = NULL;
	m_pBindedPanelCtrlId = 0;
}

void ui::xt::_meta_::_PanelHostWnd::SetClientWindow(HWND hSubWnd, DWORD HostingMode)
{
	if( m_hHostedWnd )
	{
		::ShowWindow(m_hHostedWnd,SW_HIDE);
		if( m_pHostedWndProc )
		{//unsubclass last hosted window
			#ifndef _WIN64
				::SetWindowLongPtr(m_hHostedWnd, GWLP_WNDPROC, (LONG)(m_pHostedWndProc));
			#else
				::SetWindowLongPtr(m_hHostedWnd, GWLP_WNDPROC, (LONG_PTR)(m_pHostedWndProc));
			#endif
			m_pHostedWndProc = NULL;
		}
	}

	m_HostingBehavior = HostingMode;
	m_hHostedWnd = hSubWnd;

	//if(m_HostingBehavior && m_hHostedWnd)
	if(m_hHostedWnd)  // always subclass regardless of m_HostingBehavior
	{	//subclass current hosted window
		WNDPROC wndproc;

		#pragma warning(disable: 4312) // warning C4312: 'type cast' : conversion from 'LONG' to 'WNDPROC' of greater size
		wndproc = (WNDPROC)::GetWindowLongPtr(m_hHostedWnd, GWLP_WNDPROC);
		#pragma warning(default: 4312) // warning C4312: 'type cast' : conversion from 'LONG' to 'WNDPROC' of greater size

		m_pHostedWndProc = wndproc;

		#pragma warning(disable:4312)
		#ifndef _WIN64
		::SetWindowLongPtr(m_hHostedWnd, GWLP_WNDPROC,(LONG)(WndProc_SubclassHostee));
		::SetWindowLongPtr(m_hHostedWnd,GWLP_USERDATA,(LONG)this);
		#else
		::SetWindowLongPtr(m_hHostedWnd, GWLP_WNDPROC,(LONG_PTR)(WndProc_SubclassHostee));
		::SetWindowLongPtr(m_hHostedWnd,GWLP_USERDATA,(LONG_PTR)this);
		#endif
		#pragma warning(default:4312)
	}

	// setup layout of the subclassed window
	::SetParent(m_hHostedWnd,GetSafeHwnd());
	DWORD style;
	style = ::GetWindowLong(m_hHostedWnd,GWL_STYLE);
	::SetWindowLong(m_hHostedWnd,GWL_STYLE, 
					(style & ~(	WS_CAPTION			|
								WS_DLGFRAME			|
								WS_OVERLAPPED		|
								WS_OVERLAPPEDWINDOW	|
								WS_POPUP			|
								WS_POPUPWINDOW		|
								WS_SIZEBOX			|
								WS_SYSMENU			|
								WS_THICKFRAME		|
								WS_TILED			|
								WS_TILEDWINDOW
							)) |WS_CHILD   );

	style = ::GetWindowLong(m_hHostedWnd,GWL_EXSTYLE);
	::SetWindowLong(m_hHostedWnd,GWL_EXSTYLE, 
					(style & ~(	WS_EX_APPWINDOW   		|
								WS_EX_DLGMODALFRAME		|
								WS_EX_OVERLAPPEDWINDOW  |
								WS_EX_PALETTEWINDOW		|
								WS_EX_TOOLWINDOW		|
								WS_EX_TOPMOST			
							)) |WS_CHILD   );

	CRect rc;
	::GetWindowRect(m_hHostedWnd,rc);

	m_HostedWndWidth = rc.Width();
	m_HostedWndHeight= rc.Height();

	GetClientRect(rc);
	_PositioningHostedWnd(rc.Width(),rc.Height());
}

void ui::xt::CDockingPane::SetClientWindow(HWND hSubWnd, DWORD HostingMode)
{
	ui::xt::_meta_::_PanelHostWnd* pHost = (ui::xt::_meta_::_PanelHostWnd*)GetChild();	
	ASSERT(pHost);
	pHost->SetClientWindow(hSubWnd,HostingMode);

	::ShowWindow(hSubWnd,SW_SHOW);
	::SetWindowPos(hSubWnd,NULL,0,0,0,0,SWP_NOZORDER|SWP_NOSIZE);
}

void ui::xt::CDockingPane::SetTitle(LPCTSTR title)
{
	ui::xt::_meta_::_PanelHostWnd* pHost = (ui::xt::_meta_::_PanelHostWnd*)GetChild();	

	TCHAR child_text[256] = {0};
	if(title == NULL)
	{
		ASSERT(pHost->m_hHostedWnd);
		::GetWindowText(pHost->m_hHostedWnd,child_text,sizeofArray(child_text));
		title = child_text;
	}
	
	ASSERT(pHost);
	pHost->SetWindowText(title); // _PanelHostWnd will call CXTPDockingPane's SetTitle

}

