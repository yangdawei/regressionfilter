#pragma once

//////////////////////////////////////////////////////////////////////
// Visual Computing Foundation Classes (VCFC)
//					Jiaping Wang  2006.2
//					e_boris2002@hotmail.com
//
// (C) Copyright Jiaping Wang 2004.
//
// Permission to copy, use, modify, sell and distribute this software
// is granted provided this copyright notice appears in all copies.
// This software is provided "as is" without express or implied
// warranty, and with no claim as to its suitability for any purpose.
//
// Absolutly no garanty!!
//////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////
//  Brief Description
//
//  xtreme_dataview.h
//
//  helper classes for PropertyGrid and xtreme ui library
//
//////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////
// Revise History
//
// Inital version	2007.10		Jiaping
//
//////////////////////////////////////////////////////////////////////

#define TYPETRAITS_ENABLE_ALL_ATLMFC

#include "xtreme_ui.h"
#include "..\rt\type_traits.h"
#include "..\rt\type_traits_stl.h"
#include "..\rt\type_traits_afx.h"
#include "..\rt\type_traits_opencv.h"
#include "..\rt\fixvec_type.h"
#include <sstream>
#include "..\rt\member_visitor.h"

////////////////////////////////////////////////////////////
// VARIBLE is identified by its address. 
// So be caution the address of different variable may have
// the same address. Eg.: CSize sz; 
// sz and sz.x shares the same address
#define VARIABLE_BIND(obj)						::uix::BindVariable(&obj,_T(#obj),uix::GetVariableBindingTarget())
#define VARIABLE_BIND_READONLY(obj)				::uix::BindVariable(&obj,_T(#obj),uix::GetVariableBindingTarget(),uix::BIND_FLAG_READONLY)
#define VARIABLE_BIND_AS_BOOL(obj)				::uix::BindVariable(&obj,_T(#obj),uix::GetVariableBindingTarget(),uix::BIND_FLAG_FORCEBOOL)
#define VARIABLE_BIND_AS_BOOL_READONLY(obj)		::uix::BindVariable(&obj,_T(#obj),uix::GetVariableBindingTarget(),uix::BIND_FLAG_FORCEBOOL|uix::BIND_FLAG_READONLY)
#define VARIABLE_UPDATE(obj)					::uix::UpdateVariable(&obj)
#define VARIABLE_REMOVE(obj)					::uix::RemoveVariable(&obj)
#define _MEMBER_ENTRY_FORCE_READONLY_BEGIN()	{ flag=::uix::BIND_FLAG_READONLY; }
#define _MEMBER_ENTRY_FORCE_READONLY_END()		{ flag=0; }


// UI Metrics can be redefined even using static library of CobeLibLite
#ifndef UIX_GRIDITEM_MINIDUMP_TEXT_WIDTH_MAX
	#define UIX_GRIDITEM_MINIDUMP_TEXT_WIDTH_MAX	60
#endif

#ifndef UIX_GRIDITEM_CHILD_EXPAND_MAX
	#define UIX_GRIDITEM_CHILD_EXPAND_MAX			20
#endif


namespace ui
{
namespace xt
{

class CVariableGrid:public CXTPPropertyGrid
{
public:
	enum{ IDD = 1568 };
	BOOL Create(CWnd* pParent, UINT ctrlid = IDD)
	{	if(__super::Create(CRect(0,0,100,100),pParent,ctrlid))
		{	SetViewDivider(0.6); ShowToolBar();
			return TRUE;
		}else return FALSE;
	}
};

enum
{	BIND_FLAG_FORCEBOOL		= 0x1,
	BIND_FLAG_TCHAR_POINTER = 0x2,
	BIND_FLAG_READONLY		= 0x1000
};

// Affects subsequence VARIABLE_BIND_xxx macros
void SetVariableBindingTarget(CXTPPropertyGrid& pGridCtrl, LPCTSTR CategoryName);
void SetVariableBindingTarget(CXTPPropertyGridItem* p);
CXTPPropertyGridItem* GetVariableBindingTarget();
void SetVariableChangedFeedback(HWND hNotification, BOOL NotifyDirectlyBindingOnly = FALSE);


struct PropertyGridChangeNotification:public NMHDR
{	LPVOID		pVariable;
	LPCTSTR		pName;
	UINT		VariableLen;
};


template<typename T, class t_ParentItem>
void BindVariable(T* obj, LPCTSTR name, t_ParentItem* pRoot, DWORD BindFlag);

namespace _meta_
{
template<typename T>
	class _PropertyGridItem;
class _BindedVariableMap;
} // namespace _meta_

class _PropertyGridItemBase: public CXTPPropertyGridItem
{	template<typename T> friend class _meta_::_PropertyGridItem;
	friend class _meta_::_BindedVariableMap;
	template<typename T, class t_ParentItem>
	friend void BindVariable(T* obj, LPCTSTR name, t_ParentItem* pRoot,DWORD BindFlag);
	friend void SetVariableBindingTarget(CXTPPropertyGrid& pGridCtrl, LPCTSTR CategoryName);
	friend void SetVariableBindingTarget(CXTPPropertyGridItem* p);
	friend CXTPPropertyGridItem* GetVariableBindingTarget();
	friend void SetVariableChangedFeedback(HWND hNotification, BOOL NotifyDirectlyBindingOnly);

public:
	_PropertyGridItemBase(LPCTSTR title):CXTPPropertyGridItem(title)
	{	m_nFlags=0;
		m_pParentItem=NULL;
		m_NotifyWnd = g_DefaultNotifyWnd;
	}
	virtual ~_PropertyGridItemBase(){}
	virtual void UpdateDataToUI(){}
	void	Remove();
	BOOL	IsEditable()const{ return m_nFlags&xtpGridItemHasEdit; }
	DWORD	GetBindFlag()const{ return m_BindFlag; }
	LPCTSTR	GetName()const{ return m_strCaption; }
	LPCTSTR	GetString()const{ return m_strValue; }
	void	SetString(LPCTSTR str){ m_strValue = str; OnDataUpdatedByUI(); }

protected:
	void	SetParentItem(_PropertyGridItemBase* p){ m_pParentItem=p; }
	void	SetParentItem(...){}
	void	RemoveAllChild();
	void	SendChangingNotification(LPVOID pObj, UINT size);
	virtual LPCVOID GetBindedAddress(){ return NULL; }
	virtual	void OnDataUpdatedByUI(){};
	_PropertyGridItemBase*	m_pParentItem;
	DWORD	m_BindFlag;
	HWND	m_NotifyWnd;

	static HWND						g_DefaultNotifyWnd;
	static CXTPPropertyGridItem*	g_DefaultItemRoot;
	static BOOL						g_NotifyDirectlyBindingOnly;
};

namespace _meta_
{

#ifdef UNICODE
	typedef std::wostringstream	_ostringstream;
	typedef std::wistringstream	_istringstream;
#else
	typedef std::ostringstream	_ostringstream;
	typedef std::istringstream	_istringstream;
#endif

	__forceinline LPCTSTR _text_true(){ return _T("true"); }
	__forceinline LPCTSTR _text_sectionname(){ return _T("Binded Variable List"); }
	__forceinline LPCTSTR _text_false(){ return _T("false"); }
	__forceinline LPCTSTR _text_legal(){ return _T("legal"); }
	__forceinline LPCTSTR _text_illegal(){ return _T("illegal !!"); }
	__forceinline LPCTSTR _text_desc_temp1(){ return _T("(0x%p,%d)    BOOL(%s)"); }
	__forceinline LPCTSTR _text_desc_temp2(){ return _T("(0x%p,%d)    %s"); }
	__forceinline LPCTSTR _text_desc_ptr(){ return _T("\15\12Refered Address: (0x%p) is "); }
	__forceinline LPCTSTR _text_desc_click(){ return _T("\15\12Click value cell in the right side to edit."); }
	__forceinline LPCTSTR _text_value_dump1(){ return _T("[ %s ] (%d)"); }
	__forceinline LPCTSTR _text_value_dump2(){ return _T("[ %s ] (*)"); }
	__forceinline LPCTSTR _text_value_dump3(){ return _T("[ %s ]"); }
	__forceinline LPCTSTR _text_value_string(){ return _T("(%p) is %s"); }
	__forceinline LPCTSTR _text_null(){ return _T("(NULL)"); }
	__forceinline LPCTSTR _text_sp(){ return _T("..."); }
	__forceinline LPCTSTR _text_seperator(){ return _T("( an innocent seperator ^_^ )"); }
	__forceinline LPCTSTR _text_unk_cls(){ return _T("unknown class"); }
	__forceinline LPCTSTR _text_array(){ return _T("Array(%d) "); }
	__forceinline LPCTSTR _text_array_unk(){ return _T("Array(*) "); }
	__forceinline LPCTSTR _text_pod(){ return _T("POD "); }
	__forceinline LPCTSTR _text_numeric(){ return _T("Num "); }

	template<class T>
	void _DumpMFCObject(T* obj, CString& str)
	{	CMemFile f;		
		{	CDumpContext dc(&f);
			__if_exists(T::Dump){ obj->Dump(dc); }
			__if_not_exists(T::Dump){ dc<<(*obj); }
		}
		f.Write(&f,1);
		UINT len = (UINT)f.GetLength();
		LPBYTE buf = f.Detach();
		buf[len-1]=0;
		str = buf;
		f.Attach(buf,len);
	}

	template<typename T>
	CString _DeduceTypeName(T* pObj)
	{	typedef rt::TypeTraits<T> TT;
		__static_if(TT::Typeid == rt::_typeid_class && !rt::IsMemberVisitorSupported<T>::Result )
		{
		#ifdef _CPPRTTI  // when the type is not recognizable by rt::TypeTraits, USE RTTI if available.
			//return CString(typeid(T).name());
			//However, call typeid.name may cause memory leak, so I have to disable it.
			//more information: http://support.microsoft.com/kb/140670
			return CString(_text_unk_cls());
		#endif
		}
		_ostringstream oss;
		rt::TypeTraits<T>::TypeName(oss);
		return CString(oss.str().c_str());
	}
	template<typename T>
	static BOOL _DisplayPointerToString(T* p, CString& str, BOOL add_quota_to_string = TRUE)	// return TRUE if converted to string
	{	
		str.Empty();
		if(p)
		{
			typedef typename rt::Remove_QualiferAndRef<T>::t_Result _T;
			enum{ ele_typeid = rt::TypeTraits<_T>::Typeid };
			__static_if(ele_typeid == rt::_typeid_8s)			//Display ANSI string
			{	if(::IsBadStringPtrA(p,0xffffffff))
					str = _text_illegal();
				else
				{	if(add_quota_to_string)str = _T('"');
					str += CString(p);
					if(add_quota_to_string)str += _T('"');
					return TRUE;
				}
			}
			__static_if(ele_typeid == rt::_typeid_wchar_t)		//Display WCHAR string
			{	if(::IsBadStringPtrW(p,0xffffffff))
					str = _text_illegal();
				else
				{	if(add_quota_to_string)str = _T('"');
					str += CString(p);
					if(add_quota_to_string)str += _T('"');
					return TRUE;
				}
			}
			__static_if(ele_typeid!=rt::_typeid_wchar_t && ele_typeid!=rt::_typeid_8s)
			{	if(::IsBadReadPtr(p,sizeof(t_Ele)))
					str = _text_illegal();
				else
					str = _text_legal();
			}
			return FALSE;
		}
		else
		{	str = _text_null();
			return TRUE;
		}
	}

template<typename T, bool is_array = rt::TypeTraits<T>::IsArray>
class _DeduceTextWidth
{	typedef rt::TypeTraits<T> trait;
	enum{ Element_Length = _DeduceTextWidth<typename trait::t_Element>::Result };
public:
#pragma warning(disable:4307) // '*' : integral constant overflow
	enum{ Result = (trait::Length!=TYPETRAITS_SIZE_UNKNOWN && Element_Length!=TYPETRAITS_SIZE_UNKNOWN)?
					3+trait::Length*(Element_Length+1):TYPETRAITS_SIZE_UNKNOWN
	}; 
#pragma warning(default:4307)
};	
	template<typename T> struct _DeduceTextWidth<T,false>
	{	typedef rt::TypeTraits<T> trait;
		enum{ Result = trait::text_output_spec::width_total }; 
	};


class _BindMembers_
{
protected:
	_PropertyGridItemBase*		m_pRootItem;
public:
	enum{ Visitor_Id = rt::_MemberVisitor::MV_BindGrid };
	_BindMembers_(_PropertyGridItemBase* pRoot)
		:m_pRootItem(pRoot){}
	template< typename T >
	HRESULT Visit(T& member,DWORD flag,LPCTSTR pMemberName=NULL)
	{		
		BindVariable(&member,pMemberName,m_pRootItem,m_pRootItem->GetBindFlag()|0x40000000|flag);
		return S_OK;
	}
	HRESULT Begin(LPCTSTR pClassName){ return S_OK; }
	HRESULT End(LPCTSTR pClassName){ return S_OK; }
};

///////////////////////////////////////////////////////////////////////////
// _PropertyGridItem class
//

template<typename T>
class _PropertyGridItem: public uix::_PropertyGridItemBase
{	typedef typename rt::Remove_QualiferAndRef<T>::t_Result _T;
	typedef rt::TypeTraits<_T> _trait;
	enum{ _typeid = _trait::Typeid };
protected:
	CString		m_LPCTSTR_Value;
	T*			m_pObject;
	BOOL		m_bDynamicArray;
public:
	LPCVOID GetBindedAddress(){ return m_pObject; }
	_PropertyGridItem(LPCTSTR caption, T* pObj,DWORD bindflag):_PropertyGridItemBase(caption)
	{	if(pObj)
		{	m_pObject = pObj; 
			m_bDynamicArray = FALSE;
			m_BindFlag = bindflag;
			m_nFlags = xtpGridItemHasExpandButton; // Has a button by default
	
			__static_if(_typeid>=rt::_typeid_bool && _typeid<=rt::_typeid_64f)
			{	// all numeric types
				if(!(m_BindFlag&BIND_FLAG_READONLY))
				{
					if((m_BindFlag&BIND_FLAG_FORCEBOOL) || _typeid==rt::_typeid_bool)
					{	m_nFlags = xtpGridItemHasComboButton|xtpGridItemHasEdit;
						m_pConstraints->AddConstraint(_meta_::_text_true());
						m_pConstraints->AddConstraint(_meta_::_text_false());
						SetConstraintEdit(TRUE);
					}
					else
						m_nFlags = xtpGridItemHasEdit;
				}
				else m_nFlags = 0;
			}

			__static_if(rt::IsStringClass<T>::Result){ m_nFlags=0; }

			// BIND_FLAG_TCHAR_POINTER has bug
			//enum{ _is_LPCTSTR = rt::IsTypeSame<T,LPCTSTR>::Result || rt::IsTypeSame<T,LPTSTR>::Result };
			//__static_if(_is_LPCTSTR)
			//{	m_BindFlag |= BIND_FLAG_TCHAR_POINTER;
			//	m_nFlags = xtpGridItemHasEdit|xtpGridItemHasExpandButton;
			//	m_LPCTSTR_Value = *pObj;
			//}

			UpdateDescription();
		}
	}
	virtual ~_PropertyGridItem(){ if(m_pObject)_BindedVariableMap::RemoveItem(m_pObject); }
	void UpdateDataToUI()
	{	UpdateDataToUI_Internal();
		if(m_bDynamicArray)
		{	RemoveAllChild();
			OnAddChildItem();
		}
		else
		{	CXTPPropertyGridItems* p = GetChilds();
			for(int i=0;i<p->GetCount();i++)
				((_PropertyGridItem*)p->GetAt(i))->UpdateDataToUI();
		}
		UpdateDescription();
	}
	void UpdateDescription()
	{	/////////////////////////////////////////////////////////////////
		// Update Description Text
		CString desc;
		CString str;
		if(m_BindFlag&BIND_FLAG_FORCEBOOL)
			desc.Format(_text_desc_temp1(),m_pObject,sizeof(T),_meta_::_DeduceTypeName<T>(m_pObject));
		else
			desc.Format(_text_desc_temp2(),m_pObject,sizeof(T),_meta_::_DeduceTypeName<T>(m_pObject));

		// Dump TypeTraits
		__static_if(_trait::IsAggregate || _trait::IsNumeric || _trait::IsArray)
		{	desc += _T(' ');
			desc += _T('[');
			desc += _T(' ');
			__static_if(_trait::IsAggregate)
			{	desc += _text_pod();
			}
			__static_if(_trait::IsArray)
			{	if(_trait::Length != TYPETRAITS_SIZE_UNKNOWN)
					str.Format(_text_array(),_trait::Length);
				else
					str = _text_array_unk();
				desc += str;
			}
			__static_if(_trait::IsNumeric)
			{	desc += _text_numeric();
			}
			desc += _T(']');
		}

		// Dump Pointers
		__static_if(_typeid==rt::_typeid_pointer)
		{	
			str.Format(_text_desc_ptr(),*m_pObject);
			desc += str;

			_DisplayPointerToString(*m_pObject,str);
			desc += str;
		}
		// String classes
		__static_if(rt::IsStringClass<T>::Result)
		{{	// String Class
			typedef rt::Remove_QualiferAndRef<typename _trait::t_Element>::t_Result C;
			const C* p;
			__static_if(_typeid!=rt::_typeid_stl){ p = (*m_pObject); }
			__static_if(_typeid==rt::_typeid_stl){ p = (m_pObject->c_str()); }

			str.Format(_text_desc_ptr(),p);
			desc += str;
			_DisplayPointerToString(p,str);
			desc += str;
		}}

		// Editing hint
		if((m_nFlags&xtpGridItemHasEdit) || (m_nFlags&xtpGridItemHasComboButton))
			desc += _text_desc_click();

		CXTPPropertyGridItem::SetDescription(desc);

		/////////////////////////////////////////////////////////////////
		// Update Description Text
		// display typename of all non-numeric types
		__static_if(_typeid<rt::_typeid_bool || _typeid>rt::_typeid_64f)
		{	
			__static_if(_typeid!=rt::_typeid_pointer)
			{	
				__static_if(rt::IsStringClass<T>::Result)
				{	// String Class
					typedef rt::Remove_QualiferAndRef<typename _trait::t_Element>::t_Result C;
					__static_if(_typeid!=rt::_typeid_stl)
					{	_DisplayPointerToString((const C*)(*m_pObject),str); }
					__static_if(_typeid==rt::_typeid_stl)
					{	_DisplayPointerToString((const C*)(m_pObject->c_str()),str); }
					CXTPPropertyGridItem::SetValue(str);
					return;
				}
				__static_if(_typeid==rt::_typeid_codelib)
				{	// CodeLib classes
					enum{ text_width = _DeduceTextWidth<T>::Result };
					__static_if(text_width<=UIX_GRIDITEM_MINIDUMP_TEXT_WIDTH_MAX)
					{	_ostringstream oss;
						__static_if(_typeid!=rt::_typeid_array)
						{ oss<<(*m_pObject); }
						__static_if(_typeid==rt::_typeid_array)
						{	typedef rt::TypeTraits<_T> _TTR;
							oss<<(*((rt::Vec<typename _TTR::t_Element,_TTR::Length>*)m_pObject));
						}
						CString tip(oss.str().c_str());
						if(tip.GetLength()>64)
						{	tip.Truncate(64);
							tip.GetBuffer()[61] = _T('.');
							tip.GetBuffer()[62] = _T('.');
							tip.GetBuffer()[63] = _T('.');
						}
						CXTPPropertyGridItem::SetValue(tip);
						return;
					}
				}
			}
			__static_if(_typeid==rt::_typeid_pointer)
			{	
				if(_DisplayPointerToString(*m_pObject,str,FALSE))
					CXTPPropertyGridItem::SetValue(str);
				else
				{	CString sss;
					sss.Format(_text_value_string(),*m_pObject,str);
					CXTPPropertyGridItem::SetValue(sss);
				}
				return;
			}

			// By default display class name and its element count
			__static_if(_trait::IsArray)
			{	// some kind of container
				UINT sz = (UINT)rt::GetSize(*m_pObject);
				if(sz!=TYPETRAITS_SIZE_UNKNOWN)
					str.Format(_text_value_dump1(),_meta_::_DeduceTypeName<_T>(m_pObject),sz);
				else
					str.Format(_text_value_dump2(),_meta_::_DeduceTypeName<_T>(m_pObject));
			}
			__static_if(_trait::IsImage)
			{
				UINT Rows = rt::GetRowCount(*m_pObject), Cols = rt::GetColCount(*m_pObject);
				str.Format(_T("[ %s ](%dx%d)"),_meta_::_DeduceTypeName<_T>(m_pObject),Rows,Cols);
			}
			__static_if(!_trait::IsArray && !_trait::IsImage)
			{	str.Format(_text_value_dump3(),_meta_::_DeduceTypeName<_T>(m_pObject)); }

			CXTPPropertyGridItem::SetValue(str);
			return;
		}
	}
protected:
	void UpdateDataToUI_Internal()
	{	__static_if(_typeid>=rt::_typeid_bool && _typeid<=rt::_typeid_64f)
		{	if((BIND_FLAG_FORCEBOOL&m_BindFlag) || _typeid==rt::_typeid_bool)
				CXTPPropertyGridItem::SetValue((*m_pObject)?_meta_::_text_true():_meta_::_text_false());
			else
			{	_ostringstream oss;
				__static_if(sizeof(T)!=1){ oss<<(*m_pObject); }
				__static_if(_typeid==rt::_typeid_8s||_typeid==rt::_typeid_8u){ oss<<((int)(*m_pObject)); }
				CXTPPropertyGridItem::SetValue(CString(oss.str().c_str()));
			}
		}
	}
	void OnDataUpdatedByUI()
	{	
		__static_if(_typeid>=rt::_typeid_bool && _typeid<=rt::_typeid_64f)
		{	if((BIND_FLAG_FORCEBOOL&m_BindFlag) || _typeid==rt::_typeid_bool)
				(*m_pObject) = (T)(m_strValue[0] == _text_true()[0]);
			else
			{
#pragma warning(disable:4800)	// 'int' : forcing value to bool 'true' or 'false' (performance warning)
				_istringstream iss;
				iss.str(_istringstream::_Mystr(m_strValue));
				__static_if(sizeof(T)!=1){ iss>>(*m_pObject); }
				__static_if(sizeof(T)==1){ int i; iss>>i; *m_pObject = (T)i; }
#pragma warning(default:4800)
			}

			/////////////////////////////////////////////
			// call changing event handler here, value might be modified
			SendChangingNotification(rt::_CastToNonconst(m_pObject), sizeof(T));

			UpdateDataToUI_Internal();
		}
		__static_if(_typeid<rt::_typeid_bool || _typeid>rt::_typeid_64f)
		{	
			if(m_BindFlag & BIND_FLAG_TCHAR_POINTER)
			{
				m_LPCTSTR_Value = m_strValue;
				*((LPCTSTR*)m_pObject) = m_LPCTSTR_Value;
			}
			
			SendChangingNotification(rt::_CastToNonconst(m_pObject), sizeof(T));
		}

		UpdateDescription();

		if(m_pParentItem)
			m_pParentItem->OnDataUpdatedByUI();
	}
	virtual void OnInplaceButtonDown()
	{	__super::OnInplaceButtonDown();
		if(!(m_nFlags&xtpGridItemHasComboButton))
		{	// Dump
			__static_if(_typeid==rt::_typeid_codelib)
			{ _STD_OUT<<_T('\n')<<(*m_pObject)<<_T('\n'); }
			#ifdef _DEBUG
			{	__static_if(_typeid==rt::_typeid_afx)
				{	enum{ subtypeid = rt::TypeTraits<T>::ExtendTypeid&SUBTYPEID_MASK };
					__static_if(subtypeid >= SUBTYPEID_MFC_MIN && subtypeid <= SUBTYPEID_MFC_MAX)
					{	CString str;
						_DumpMFCObject(m_pObject,str);
						_putts(str);
			}	}	}
			#endif
			__if_exists(T::MEMBER_ALLOW_TEXTDUMP)
			{ _STD_OUT<<_T('\n')<<(*m_pObject)<<_T('\n'); }
		}
	}
	virtual void OnAddChildItem()
	{	
		__static_if(rt::IsMemberVisitorSupported<T>::Result)
		{	_BindMembers_ bm(this);
			rt::CallMemberVisitor(*m_pObject,bm);
		}	
		__static_if(_trait::IsArray && !rt::IsStringClass<T>::Result )
		{	ASSERT_STATIC(UIX_GRIDITEM_CHILD_EXPAND_MAX >= 20);
			m_bDynamicArray = rt::TypeTraits<T>::Length==TYPETRAITS_SIZE_UNKNOWN;
			UINT count = (UINT)rt::GetSize(*m_pObject);
			if(count!=TYPETRAITS_SIZE_UNKNOWN) // add child items
			{	CString name = GetCaption();
				if(count < UIX_GRIDITEM_CHILD_EXPAND_MAX)
				{	// add all items
					for(UINT i=0;i<count;i++)_ExpandSubItem(i,name);
				}
				else
				{	// Add items in head
					for(UINT i=0;i<15;i++)_ExpandSubItem(i,name);
					// Add a seperator
					_PropertyGridItemBase* p = new _PropertyGridItemBase(_text_sp());
					p->SetValue(_text_sp());
					p->SetDescription(_text_seperator());
					AddChildItem(p);
					// Add items in tail
					for(UINT i=count-4;i<count;i++)_ExpandSubItem(i,name);
				}
			}
		}
		__static_if(_trait::IsImage)
		{
			ASSERT_STATIC(UIX_GRIDITEM_CHILD_EXPAND_MAX >= 20);
			m_bDynamicArray = rt::TypeTraits<T>::Length==TYPETRAITS_SIZE_UNKNOWN;
			UINT count = rt::GetRowCount(*m_pObject)*rt::GetColCount(*m_pObject);
			if(count!=TYPETRAITS_SIZE_UNKNOWN) // add child items
			{	CString name = GetCaption();
				if(count < UIX_GRIDITEM_CHILD_EXPAND_MAX)
				{	// add all items
					for(UINT RowId=0;RowId<rt::GetRowCount(*m_pObject);RowId++)
					for(UINT ColId=0;ColId<rt::GetColCount(*m_pObject);ColId++)
						_ExpandSubItem(RowId,ColId,name);
				}
				else
				{	// Add items in head
					UINT AddedCount = 0;
					for(UINT RowId=0;RowId<rt::GetRowCount(*m_pObject) && AddedCount<UIX_GRIDITEM_CHILD_EXPAND_MAX-1;RowId++)
					for(UINT ColId=0;ColId<rt::GetColCount(*m_pObject) && AddedCount<UIX_GRIDITEM_CHILD_EXPAND_MAX-1;ColId++, AddedCount++)
						_ExpandSubItem(RowId,ColId,name);
					// Add a seperator
					_PropertyGridItemBase* p = new _PropertyGridItemBase(_text_sp());
					p->SetValue(_text_sp());
					p->SetDescription(_text_seperator());
					AddChildItem(p);
					// Add items in tail
					_ExpandSubItem(rt::GetRowCount(*m_pObject)-1,rt::GetColCount(*m_pObject)-1,name);
				}
			}
		}
		
	}
	void _ExpandSubItem(UINT id, LPCTSTR caption_prefix)
	{	CString sub_name;
		sub_name.Format(_T("%s[%d]"),caption_prefix,id);
		typedef typename rt::Remove_QualiferAndRef<typename _trait::t_Element>::t_Result E;
		const E& item = (*m_pObject)[id];
		BindVariable(rt::_CastToNonconst(&item),sub_name,this,m_BindFlag|0x40000000);
	}
	void _ExpandSubItem(UINT RowId, UINT ColId, LPCTSTR caption_prefix)
	{
		CString sub_name;
		sub_name.Format(_T("%s[%d][%d]"),caption_prefix,RowId,ColId);
		typedef typename rt::Remove_QualiferAndRef<typename _trait::t_Element>::t_Result E;
		const E& item = (*m_pObject)(RowId, ColId);
		BindVariable(rt::_CastToNonconst(&item),sub_name,this,m_BindFlag|0x40000000);
	}
	virtual void SetValue(CString strValue){ m_strValue=strValue; OnDataUpdatedByUI(); }
	virtual void OnBeforeInsert(){ UpdateDataToUI_Internal(); }
};

// Map address of binded variable to _PropertyGridItem*
class _BindedVariableMap	
{	template<typename T>
	friend class _PropertyGridItem;
protected:
	static void RemoveItem(LPCVOID pVariable);
public:
	static void AddItem(LPCVOID pVariable, _PropertyGridItemBase* pItem);
	static _PropertyGridItemBase* QueryItem(LPCVOID pVariable);
};

} // namespace _meta_


template<typename T, class t_ParentItem>
void BindVariable(T* obj, LPCTSTR name, t_ParentItem* pRoot, DWORD bindflag = 0)
{	ASSERT(name);
	ASSERT(pRoot); // call uix::SetVariableBindingTarget first before Binding variables
	
	_PropertyGridItemBase* p = _meta_::_BindedVariableMap::QueryItem(obj);
	if(p)
	{	// search exists item for detecting duplication
		p->UpdateDataToUI();
	}
	else
	{	typedef typename rt::Remove_QualiferAndRef<T>::t_Result real_T;
		_meta_::_PropertyGridItem<real_T>* pChild = 
			new _meta_::_PropertyGridItem<real_T>(name,obj,bindflag);
		ASSERT(pChild);

		pChild->SetParentItem(pRoot);
		pRoot->AddChildItem(pChild);
		// Add to item map
		if(!(bindflag&0x40000000))
			_meta_::_BindedVariableMap::AddItem(obj,pChild);
	}
}

void UpdateVariableAll();

template<typename T>
void UpdateVariable(T* obj)
{	
	_PropertyGridItemBase* p = _meta_::_BindedVariableMap::QueryItem(obj);
	if(p)p->UpdateDataToUI();
}

template<typename T>
void RemoveVariable(T* obj)
{
	_PropertyGridItemBase* p = _meta_::_BindedVariableMap::QueryItem(obj);
	if(p)p->Remove();
}

BOOL BindVariable_SaveAll(LPCTSTR fn);
BOOL BindVariable_LoadAll(LPCTSTR fn);


} // namespace xt
} // namespace ui


