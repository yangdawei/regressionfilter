#include "stdafx.h"
#include "TimelineSlider.h"
#include <set>

using namespace num;
using namespace Gdiplus;

//////////////////////////////////////////////////////
// CTimelineSlider
namespace ui
{
namespace _meta_
{	

static const float _TickInterval[] = 
{	10,25,50,75,100,250,500,750,1000,		// less than 1s
	2000,4000,8000,16000,30000,450000,60000,// less than 1min
	120000,300000,600000,1800000,3600000	// less than 1hour
};

class _ProposeTickInterval
{	std::set<float>	_ProposeTick;
public:
	_ProposeTickInterval()
	{	for(UINT i=0;i<sizeofArray(_TickInterval);i++)_ProposeTick.insert(_TickInterval[i]); }
	float Propose(float low_bound)
	{	if(low_bound <= 3600000)
			return *_ProposeTick.upper_bound(low_bound);
		else
			return 3600000;
	}
};
_ProposeTickInterval _TheTickIntervalProposal;

} // namespace _meta_
} // namespace ui

#pragma warning(disable: 4355)
ui::CTimelineSlider::CTimelineSlider(_meta_::_GdiplusControlHost* pHost)
	:_meta_::_GdiplusControlRoot(pHost)
	,m_VisableRegionSlider(this,0)
	,m_TickPen(Color(255,80,80,80))
	,m_TickCurPen(Color(180,0,0,60),2)
	,m_KeyframePen(Color(220,40,40,100),3)
	,m_KeyframeHotPen(Color(140,40,40,100),3)
{
	m_CurTime = 0;
	m_Tooltip_binding = 0xffff;
	m_hResizeH = ::LoadCursor(NULL,IDC_SIZEWE);
	m_TickCurPen.SetDashStyle(DashStyleDash);
	m_DrugWhat = 0xffff;
	m_Style = STYLE_KEYFRAME_EDITABLE;
}
#pragma warning(default: 4355)

BOOL ui::CTimelineSlider::Composed_Notify(UINT id, UINT msg,WPARAM wParam, LPARAM lParam)
{	// notify from m_VisableRegionSlider
	ASSERT(id==0);
	switch(msg)
	{
	case CScopeSlider::NOTIFY_SCOPE_RESIZED:
	case CScopeSlider::NOTIFY_LENGTH_EXTENDED:
		UpdateLayout();
		break;
	case CScopeSlider::NOTIFY_POSITION_CHANGED:
		RedrawContent();
		break;
	case NOTIFY_GRAPHICS_UPDATED:
		if( IsOncePainted() )
			SendNotify(NOTIFY_GRAPHICS_UPDATED);
		break;
	}
	return TRUE;
}

void ui::CTimelineSlider::Composed_GetRedrawRect(UINT id, LPRECT lpRect)
{	// RedrawRect for m_VisableRegionSlider
	ASSERT(id==0);
	m_pControlHost->GetRedrawRect(lpRect);
	lpRect->top = max(lpRect->top,lpRect->bottom-SCROLL_HEIGHT);
}

void ui::CTimelineSlider::UpdateLayout()
{	
	RECT layout;
	m_pControlHost->GetRedrawRect(&layout);
	layout.bottom -= SCROLL_HEIGHT;

	m_TotalTime = m_VisableRegionSlider.GetTotalLength()*TIME_UINT;
	LPCWSTR pSampleText;
	if(m_TotalTime<=1000){			m_TimeLevel = 0; pSampleText = L"000"; }
	else if(m_TotalTime<=60*1000){	m_TimeLevel = 1; pSampleText = L"00:000"; }
	else if(m_TotalTime<=3600*1000){m_TimeLevel = 2; pSampleText = L"00:00:000";  }
	else{							m_TimeLevel = 3; pSampleText = L"00:00:00:000"; }

	m_VisableTimeScope = m_VisableRegionSlider.GetScopeSize()*TIME_UINT;
	m_ScaleToPixel = (layout.right-layout.left)/m_VisableTimeScope;

	float max_text_tick;
	{	Vec4f bounding;
		Graphics gfx(::GetDC(NULL));	// use screen dc in case that m_BackBuffer is not initialized
		gfx.MeasureString(pSampleText,-1,m_pFont,(PointF&)Vec2f(),(RectF*)&bounding);
		m_TickTextSize = bounding.GetRef<2>(2);
		m_TickTextSize.y -= 5;
		max_text_tick = (layout.right-layout.left)/(m_TickTextSize.x + 20);
	}

	m_TickInterval = _meta_::_TheTickIntervalProposal.Propose(m_VisableTimeScope/max_text_tick);
	m_TickCount = (UINT)(m_VisableTimeScope/m_TickInterval) + 2;
	m_MiniTickWidth_px = m_TickInterval/10*m_ScaleToPixel;

	m_Tooltip_Pos_px.y = (int)(layout.bottom - CURTM_TIP_HEIGHT - 0.5f);

	RedrawContent();
}

void ui::CTimelineSlider::PrintTime(LPWSTR buf, float time)
{	if(m_TimeLevel == 0)
	{	_swprintf(buf,L"%03d",(int)time); }
	else if(m_TimeLevel == 1)
	{	_swprintf(buf,L"%02d:%03d",(int)(time/1000),(int)(((int)(time+0.5f))%1000)); }
	else if(m_TimeLevel == 2)
	{	int minf = (int)(time/60000);
		time -= minf*60000;
		_swprintf(buf,L"%02d:%02d:%03d",minf,(int)(time/1000),(int)(((int)(time+0.5f))%1000)); 
	}
	else
	{	ASSERT(m_TimeLevel==3);
		int hour = (int)(time/(60000*60));
		time -= hour*60000*60;
		int minf = (int)(time/60000);
		time -= minf*60000;
		_swprintf(buf,L"%02d:%02d:%02d:%03d",hour,minf,(int)(time/1000),(int)(((int)(time+0.5f))%1000)); 
	}
}

void ui::CTimelineSlider::RedrawContent()
{
	if(!IsUILocked()){}else{ return; }

	RECT layout;
	m_pControlHost->GetRedrawRect(&layout);
	layout.bottom -= SCROLL_HEIGHT; // above part is the slider bar

	Graphics* pGfx = m_pControlHost->GetGraphics();
	if( pGfx )
	{
		Graphics& gfx(*pGfx);
		gfx.SetSmoothingMode( Gdiplus::SmoothingModeDefault );
		Gdiplus::SolidBrush brush(Color(220,40,40,40));

		RedrawBackground(gfx,layout,FALSE);
		
		UINT win_w_px = layout.right-layout.left;
		float time_base = m_VisableRegionSlider.GetPos()*TIME_UINT;
		float tick_bottom;
		// draw time tick
		{	
			UINT tick_start = (UINT)(time_base/m_TickInterval);
			for(UINT i=0;i<m_TickCount;i++)
			{	float t = (tick_start+i)*m_TickInterval;
				float x_px = (t-time_base)*m_ScaleToPixel;
				gfx.DrawLine(&m_TickPen,(int)(layout.left+x_px+0.5f),	layout.top, 
										(int)(layout.left+x_px+0.5f),(int)(2.5f + layout.top + MINI_TICK_HEIGHT + m_TickTextSize.y));
				WCHAR buf[250];
				PrintTime(buf,t);
				gfx.DrawString(buf,-1,m_pFont,PointF(floor(x_px+1.5f),(float)(layout.top-3)),&brush);

				tick_bottom = 2 + layout.top + MINI_TICK_HEIGHT + m_TickTextSize.y;
				for(UINT j=1;j<10;j++)
				{	
					gfx.DrawLine(&m_TickPen,(int)(layout.left+x_px+0.5f+j*m_MiniTickWidth_px),	(int)(1.5f + layout.top + m_TickTextSize.y), 
											(int)(layout.left+x_px+0.5f+j*m_MiniTickWidth_px),	(int)(tick_bottom + 0.5f));
				}
			}

			gfx.DrawLine(&m_TickPen, layout.left, (int)(tick_bottom+0.5f), layout.right, (int)(tick_bottom+0.5f) );
		}

								 
		// draw current time
		float ct_x_px = (m_CurTime - time_base)*m_ScaleToPixel + 0.8f;
		gfx.DrawLine(&m_TickCurPen, (int)(layout.left+ct_x_px+0.5f), (int)(1.5f + tick_bottom), 
									(int)(layout.left+ct_x_px+0.5f), layout.bottom );

		// draw tooltip
		if( m_Tooltip_binding == 0xffff ){}
		else
		{	if(m_Tooltip_binding == 0xfffe) // bind to current time
			{	m_Tooltip_Pos_px.x = (int)(ct_x_px+1.5f);	
				gfx.DrawCachedBitmap(m_CurTime_tip_Image,m_Tooltip_Pos_px.x,m_Tooltip_Pos_px.y);
			}
		}

		// draw keyframe
		{	float frame_h = layout.bottom - (4.0f + layout.top + MINI_TICK_HEIGHT + m_TickTextSize.y);
			for(UINT i=0;i<m_Keyframes.GetSize();i++)
			{	int x = (int)((m_Keyframes[i].m_Time - time_base)*m_ScaleToPixel + 0.5f);
				if( m_Keyframes[i].m_pIcon )
				{	int icon_width = (int)(m_Keyframes[i].m_IconSize.x*frame_h/m_Keyframes[i].m_IconSize.y + 0.5f);
					ASSERT(0);   //TBD
				}
				else if( x > layout.left-KEYFRAME_STAR_SIZE-1 && x < layout.right+KEYFRAME_STAR_SIZE+1 )
				{	
					Vec4<Vec2i> pts(	Vec2i((int)x						,(int)(tick_bottom+KEYFRAME_STAR_SIZE)),
										Vec2i((int)(x-KEYFRAME_STAR_SIZE)	,(int)tick_bottom),
										Vec2i((int)x						,(int)(tick_bottom-KEYFRAME_STAR_SIZE)),
										Vec2i((int)(x+KEYFRAME_STAR_SIZE)	,(int)tick_bottom) );
					if(i==m_DrugWhat)
					{
						gfx.DrawPolygon(&m_KeyframeHotPen,(Point*)&pts,4);
						gfx.DrawLine(&m_KeyframeHotPen,(int)x,(int)(tick_bottom+KEYFRAME_STAR_SIZE+2),(int)x,layout.bottom);
					}
					else
					{
						gfx.DrawPolygon(&m_KeyframePen,(Point*)&pts,4);
						gfx.DrawLine(&m_KeyframePen,(int)x,(int)(tick_bottom+KEYFRAME_STAR_SIZE+2),(int)x,layout.bottom);
					}

					if(i==m_Tooltip_binding)
					{	m_Tooltip_Pos_px.x = (int)(x + 1.5f);
						gfx.DrawCachedBitmap(m_CurTime_tip_Image,m_Tooltip_Pos_px.x,m_Tooltip_Pos_px.y);
					}
				}
			}
		}

		gfx.Flush(FlushIntentionSync);
		m_bFrameBufferPainted = TRUE;

		SendNotify(NOTIFY_GRAPHICS_UPDATED);
	}
}

void ui::CTimelineSlider::SetCurrentTime(float t)
{
	float time_base = m_VisableRegionSlider.GetPos()*TIME_UINT;
	float time_delta = t - m_CurTime;

	m_CurTime = t;

	if( m_CurTime<=m_VisableTimeScope+time_base && m_CurTime>=time_base ){}
	else
	{	
		m_VisableRegionSlider.SetPos(m_CurTime/TIME_UINT);
		m_bFrameBufferPainted = FALSE;
		m_VisableRegionSlider.SetPos( m_VisableRegionSlider.GetPos() + time_delta/TIME_UINT );
	}

    m_Tooltip_binding = 0xffff;
	RedrawContent();
	SendNotify(NOTIFY_CURRENT_CHANGED);
}

void ui::CTimelineSlider::ToolTipButtonClicked(UINT id)
{	
	ASSERT(m_Tooltip_binding != 0xffff);
	switch(id)
	{
	case 0:
		{	if(m_Tooltip_binding == 0xfffe)
				AddNewKey();
			else if(m_Tooltip_binding < 0xf000)
				DeleteKey(m_Tooltip_binding);
		}
		break;
	case 1:
		break;
	}
}

void ui::CTimelineSlider::AddNewKey()
{	// insert frame
	if(m_Style&STYLE_KEYFRAME_EDITABLE)
	{
		UINT i = KeyframeUpperBound(m_CurTime);

		float epsf = 6/m_ScaleToPixel;
		// avoid duplicated keyframe
		if(i && m_Keyframes[i-1].m_Time + epsf >= m_CurTime)return;
		if(i < m_Keyframes.GetSize() && m_Keyframes[i].m_Time - epsf <= m_CurTime)return;

		if( SendNotify(NOTIFY_CREATING_KEYFRAME,0,(LPARAM&)m_CurTime) )
		{
			m_Keyframes.insert(i);
			m_Keyframes[i].m_Time = m_CurTime;
			m_Tooltip_binding = i;
			SendNotify(NOTIFY_NEW_KEYFRAME,i);
			UpdateKeyframes();
		}
	}
}

float ui::CTimelineSlider::GetLastKeyframeTime()
{	if(m_Keyframes.GetSize())
	{	return m_Keyframes[m_Keyframes.GetSize()-1].m_Time;
	}else return 0;
}

float ui::CTimelineSlider::GetFirstKeyframeTime()
{	if(m_Keyframes.GetSize())
	{	return m_Keyframes[0].m_Time;
	}else return 0;
}

UINT ui::CTimelineSlider::MoveKey(UINT index, float time_to)
{
	if(m_Style&STYLE_KEYFRAME_EDITABLE)
	{	ASSERT(m_Keyframes.GetSize() > index);
		// propose new time if collusion occurs
		UINT insert_before;
		float best_time = 0;
		{	float epsf = 6/m_ScaleToPixel;
			float min_dist = FLT_MAX;
			float lower_bound = 0;
			for(UINT i=0;i<m_Keyframes.GetSize();i++) // fit in all left slots
			{	if(i!=index)
				{	float kt = m_Keyframes[i].m_Time;
					if(kt-epsf > lower_bound)  
					{	if(time_to < lower_bound)
						{	if( (lower_bound - time_to) < min_dist )
							{	best_time = lower_bound;
								min_dist = lower_bound - time_to;
								insert_before = i;
							}
						}
						else if(time_to > kt-epsf)
						{	if( (time_to - (kt-epsf) ) < min_dist )
							{	best_time = kt-epsf;
								min_dist = time_to - (kt-epsf);
								insert_before = i;
							}
						}
						else // a free region that contained target time
						{	best_time = time_to;
							min_dist = 0;
							insert_before = i;
							lower_bound = kt+epsf;
							break;
						}
					}
					lower_bound = kt+epsf;
				}
			}
			if(time_to < lower_bound)
			{	if( (lower_bound - time_to) < min_dist )
				{	best_time = lower_bound;
					min_dist = lower_bound - time_to;
					insert_before = (UINT)m_Keyframes.GetSize();
				}
			}
			else
			{	best_time = time_to;
				insert_before = (UINT)m_Keyframes.GetSize();
			}
			if(insert_before == index+1)insert_before--; // the previous of index+1 is index-1
		}
		
		if(SendNotify(NOTIFY_MOVING_KEYFRAME,index,(WPARAM&)best_time))
		{	m_Keyframes[index].m_Time = best_time;
			if(index != insert_before-1)
			{	if(index < insert_before)	// go forward
				{	for(UINT i=index;i<insert_before-1;i++)
						::rt::Swap(m_Keyframes[i],m_Keyframes[i+1]);
					insert_before--;
				}
				else
				{	ASSERT(index >= insert_before);  // go backward
					for(UINT i=index;i>insert_before;i--)
						::rt::Swap(m_Keyframes[i-1],m_Keyframes[i]);
				}
			}
			else insert_before--;
			UpdateKeyframes();
			
			// Check mono
			{
				for(UINT i=1;i<m_Keyframes.GetSize();i++)
					ASSERT(m_Keyframes[i-1].m_Time < m_Keyframes[i].m_Time);
			}
			return insert_before;
		}
	}
	return index;
}

void ui::CTimelineSlider::DeleteKey(UINT id)
{
	if(m_Style&STYLE_KEYFRAME_EDITABLE)
	{
		if(SendNotify(NOTIFY_DELETING_KEYFRAME,id))
		{
			ASSERT(id<m_Keyframes.GetSize());
			m_Keyframes.erase(id);
			m_Tooltip_binding = 0xffff;
			UpdateKeyframes();
		}
	}
}

UINT ui::CTimelineSlider::KeyframeUpperBound(float t)
{
	for(UINT i=0;i<m_Keyframes.GetSize();i++)
		if(m_Keyframes[i].m_Time >= t)return i;
	return (UINT)m_Keyframes.GetSize();
}

void ui::CTimelineSlider::SetKeyframes(UINT key_count, const float* pTime,const WPARAM* pData)
{
	VERIFY(m_Keyframes.SetSize(key_count));
	if(key_count)
	{	ASSERT_ARRAY(pTime,key_count);
		m_Keyframes[0].m_Time = pTime[0];
		if(pData)m_Keyframes[0].m_UserData = pData[0];
		for(UINT i=1;i<key_count;i++)
		{	ASSERT(pTime[i-1] < pTime[i]);
			m_Keyframes[i].m_Time = pTime[i];
			if(pData)m_Keyframes[i].m_UserData = pData[i];
		}
	}
	m_Tooltip_binding = 0xffff;
	UpdateKeyframes();
}

void ui::CTimelineSlider::UpdateKeyframes()
{
	m_VisableRegionSlider.m_Labels.SetSizeAs(m_Keyframes);
	for(UINT i=0;i<m_Keyframes.GetSize();i++)
		m_VisableRegionSlider.m_Labels[i] = m_Keyframes[i].m_Time/TIME_UINT;

	m_bFrameBufferPainted = FALSE;
	m_VisableRegionSlider.RedrawContent();
	RedrawContent();
	SendNotify(NOTIFY_CURRENT_CHANGED);
}

UINT ui::CTimelineSlider::HitTest(const POINT& pt)
{	RECT rc;
	m_pControlHost->GetRedrawRect(&rc);
	if(pt.y >= rc.top && pt.y <= rc.bottom - SCROLL_HEIGHT)
	{	if( m_Tooltip_binding != 0xffff )
		{	if(pt.x >= m_Tooltip_Pos_px.x && pt.x <= m_Tooltip_Pos_px.x-1+TOOL_TIP_WIDTH )
			{	
				if( pt.y >= m_Tooltip_Pos_px.y )
				{
					if(pt.y < m_Tooltip_Pos_px.y+TOOL_TIP_WIDTH*CURTM_TIP_BUTTON_COUNT)
					{
						return 0xf000 + (pt.y-m_Tooltip_Pos_px.y)/TOOL_TIP_WIDTH;	
					}
					else
					{	UINT x = pt.x - m_Tooltip_Pos_px.x;
						UINT y = pt.y - m_Tooltip_Pos_px.y;
						if( _Current_time_Tip_Image_Data[ (y*TOOL_TIP_WIDTH + x)*4 + 3 ] > 40 )
							return 0xf000 + CURTM_TIP_BUTTON_COUNT;
					}
				}
			}
		}

		float epsf = 1/m_ScaleToPixel;
		float time_base = m_VisableRegionSlider.GetPos()*TIME_UINT;

		// Hit test on keyframes
		{	float mt = time_base + (pt.x-rc.left)/m_ScaleToPixel;
			UINT i = KeyframeUpperBound(mt);
			float prev_dist = FLT_MAX;
			if(i)prev_dist = fabs(mt-m_Keyframes[i-1].m_Time);
			
			float dist = FLT_MAX;
			if(i<m_Keyframes.GetSize())dist = fabs(mt-m_Keyframes[i].m_Time);

			if( prev_dist > dist )
			{	if( dist < TOOL_TIP_WIDTH*epsf/2 )return i;	}
			else
			{	if( prev_dist < TOOL_TIP_WIDTH*epsf/2)return i-1;	}
		}
		
		float ct = time_base + (pt.x-rc.left)/m_ScaleToPixel;
		if( ct > m_CurTime - epsf*2 && ct < m_CurTime + epsf*TOOL_TIP_WIDTH )
			return 0xfffe;
	}

	return 0xffff;
}


LRESULT ui::CTimelineSlider::MessageProc(UINT uMsg, WPARAM wParam, LPARAM lParam)
{
	if(!IsUILocked()){}
	else{ return __super::MessageProc(uMsg,wParam,lParam); }

	BOOL msg_is_ate = FALSE;
	switch(uMsg)
	{
	case WM_SIZE:
		UpdateLayout();
		break;
	case WM_CREATE:
		m_VisableRegionSlider.SetControlStyle(	CScopeSlider::STYLE_MOVEABLE|
										CScopeSlider::STYLE_RESIZEABLE|
										CScopeSlider::STYLE_HAS_LABEL|
										CScopeSlider::STYLE_AUTO_EXTEND );
		m_VisableRegionSlider.SetTotalLength(60000/TIME_UINT); //60s at starting
		m_VisableRegionSlider.SetScopeSize(5000/TIME_UINT);    //view 5s
		m_VisableRegionSlider.SetPos(0);
		m_VisableRegionSlider.SetMoveUnit(1000/TIME_UINT);

		m_CurTime_tip_Image.Create(m_pControlHost->GetGraphics(),TOOL_TIP_WIDTH,CURTM_TIP_HEIGHT,_Current_time_Tip_Image_Data);
		break;
	case WM_DESTROY:
		break;
	case WM_LBUTTONDOWN:
		{	RECT rc;
			m_pControlHost->SetMouseCapture();
			m_pControlHost->GetRedrawRect(&rc);
			POINT point = { GET_X_LPARAM(lParam), GET_Y_LPARAM(lParam) };
			if(point.y < rc.bottom - SCROLL_HEIGHT)
			{	msg_is_ate = TRUE;
				UINT hit = HitTest(point);
				if(hit == 0xffff || hit == 0xfffe)
				{	m_DrugWhat = 0xfffe; // drug current time
					m_Tooltip_binding = 0xffff;
				}
				else
				{	if(hit < 0xf000)
					{	m_DrugWhat = hit; // drug current time
						m_Tooltip_binding = 0xffff;// drug some keyframe
					}
					else{ ToolTipButtonClicked(hit-0xf000); }
				}
                SendNotify(NOTIFY_SELECTED_ITEM,hit);
			}
			else break;
		}
	case WM_MOUSEMOVE:
		{	RECT rc;
			m_pControlHost->GetRedrawRect(&rc);
			POINT point = { GET_X_LPARAM(lParam), GET_Y_LPARAM(lParam) };
			if( point.y > rc.top && point.y < rc.bottom - SCROLL_HEIGHT)
			{	
				if((wParam&MK_LBUTTON) && m_DrugWhat!=0xffff)
				{
					if(m_DrugWhat == 0xfffe)
					{	float time_base = m_VisableRegionSlider.GetPos()*TIME_UINT;
						m_CurTime = min(m_VisableRegionSlider.GetTotalLength()*TIME_UINT, 
									max(0,(point.x - rc.left)/m_ScaleToPixel + time_base) );
						RedrawContent();
						SendNotify(NOTIFY_CURRENT_CHANGED);
					}
                    else if(wParam&MK_CONTROL)
					{	ASSERT(m_DrugWhat < m_Keyframes.GetSize());
						float mtf = m_VisableRegionSlider.GetPos()*TIME_UINT + (point.x-rc.left)/m_ScaleToPixel;
						m_DrugWhat = MoveKey(m_DrugWhat,mtf);
					}
				}
				else
				{	// show/hide tool tip	
					UINT hover = HitTest(point);
					if( (hover&0xfff0) == 0xf000 ) // hit on tooltip button
					{
					}
					else if(hover != m_Tooltip_binding)   // hover changed
					{	m_Tooltip_binding = hover;
						RedrawContent();
					}

					::SetCursor((hover<0xf000)?m_hResizeH:m_hArrow);
				}
				msg_is_ate = TRUE;
			}
			else if(m_Tooltip_binding != 0xffff)
			{	m_Tooltip_binding = 0xffff;
				RedrawContent();
			}
		}
		break;
	case WM_LBUTTONUP:
		m_DrugWhat = 0xffff;
		m_pControlHost->ReleaseMouseCapture();
		break;
	case WM_SETCURSOR:
		return 1;
		break;
	}

	if(!msg_is_ate)m_VisableRegionSlider.MessageProc(uMsg,wParam,lParam);
	return __super::MessageProc(uMsg,wParam,lParam);
}

const BYTE ui::CTimelineSlider::_Current_time_Tip_Image_Data // BGRA data
[	ui::CTimelineSlider::TOOL_TIP_WIDTH*ui::CTimelineSlider::CURTM_TIP_HEIGHT*4] = 
{
	0,0,0,180, 0,0,0,180, 0,0,0,180, 0,0,0,180, 0,0,0,180, 0,0,0,180, 0,0,0,180, 0,0,0,180, 0,0,0,180, 0,0,0,180, 0,0,0,180, 0,0,0,180, 
	0,0,0,180, 0,0,0,180, 0,0,0,180, 0,0,0,180, 0,0,0,180, 0,0,0,180, 0,0,0,180, 0,0,0,180, 0,0,0,180, 0,0,0,180, 0,0,0,180, 0,0,0,180, 
	0,0,0,180, 0,0,0,180, 0,0,0,180, 0,0,0,180, 0,0,0,180, 0,0,0,180, 0,0,0,180, 0,0,0,180, 0,0,0,180, 0,0,0,180, 0,0,0,180, 0,0,0,180, 
	0,0,0,180, 0,0,0,180, 0,0,0,180, 0,0,0,180, 0,0,0,180, 0,0,0,180, 0,0,0,180, 0,0,0,180, 0,0,0,180, 0,0,0,180, 0,0,0,180, 0,0,0,180, 
	0,0,0,180, 0,0,0,180, 0,0,0,180, 0,0,0,180, 0,0,0,180, 0,0,0,180, 0,0,0,180, 0,0,0,180, 0,0,0,180, 0,0,0,180, 0,0,0,180, 0,0,0,180, 
	0,0,0,180, 0,0,0,180, 0,0,0,180, 0,0,0,180, 0,0,0,180, 0,0,0,180, 0,0,0,180, 0,0,0,180, 0,0,0,180, 0,0,0,180, 0,0,0,180, 0,0,0,180, 
	0,0,0,180, 0,0,0,180, 0,0,0,180, 0,0,0,180, 0,0,0,180, 0,0,0,180, 0,0,0,180, 0,0,0,180, 0,0,0,180, 0,0,0,180, 0,0,0,180, 0,0,0,180, 
	0,0,0,180, 0,0,0,180, 0,0,0,180, 0,0,0,180, 0,0,0,180, 0,0,0,180, 0,0,0,180, 0,0,0,180, 0,0,0,180, 0,0,0,180, 0,0,0,180, 0,0,0,180, 
	0,0,0,180, 0,0,0,180, 0,0,0,180, 0,0,0,180, 0,0,0,180, 0,0,0,180, 0,0,0,180, 0,0,0,180, 0,0,0,180, 0,0,0,180, 0,0,0,180, 0,0,0,180, 
	0,0,0,180, 0,0,0,180, 0,0,0,180, 0,0,0,180, 0,0,0,180, 0,0,0,180, 0,0,0,180, 0,0,0,180, 0,0,0,180, 0,0,0,180, 0,0,0,180, 0,0,0,180, 
	0,0,0,180, 0,0,0,180, 0,0,0,180, 0,0,0,180, 0,0,0,180, 0,0,0,180, 0,0,0,180, 0,0,0,180, 0,0,0,180, 0,0,0,180, 0,0,0,180, 0,0,0,180, 
	0,0,0,180, 0,0,0,180, 0,0,0,180, 0,0,0,180, 0,0,0,180, 0,0,0,180, 0,0,0,180, 0,0,0,180, 0,0,0,180, 0,0,0,180, 0,0,0,180, 0,0,0,180
};
