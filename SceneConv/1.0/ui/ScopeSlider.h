#pragma once
//////////////////////////////////////////////////////////////////////
// Visual Computing Foundation Classes (VCFC)
//					Jiaping Wang  2006.2
//					e_boris2002@hotmail.com
//
// (C) Copyright Jiaping Wang 2004.
//
// Permission to copy, use, modify, sell and distribute this software
// is granted provided this copyright notice appears in all copies.
// This software is provided "as is" without express or implied
// warranty, and with no claim as to its suitability for any purpose.
//
// Absolutly no garanty!!
//////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////
//  Brief Description
//
//  ScopeSlider.h
//
//  ScopeSlider: UI for scrolling view of some large content
//             also acts as ProgressBar and SliderBar
//
//////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////
// Revise History
//
// Inital version	2006.11.9		Jiaping
//
//////////////////////////////////////////////////////////////////////

#include "ui_base.h"
#include "..\num\small_vec.h"

namespace ui
{

class CTimelineSlider;

class CScopeSlider : public _meta_::_GdiplusControlRoot
{	friend class CTimelineSlider;
public:
	enum{ IDD = 1566 };
	enum{ NOTIFY_POSITION_CHANGED = NOTIFY_CUSTOM_BASE + 1 ,
		  NOTIFY_SCOPE_RESIZED,	
		  NOTIFY_LENGTH_EXTENDED,
		};
	enum{ STYLE_MOVEABLE	= 1,
		  STYLE_RESIZEABLE	= 2,
		  STYLE_HAS_LABEL	= 4,
		  STYLE_AUTO_EXTEND = 8
		};

protected:
	// Basic
	float				m_ScopeSize_Min;
	float				m_ScopeSize_Max;

	float				m_TotalLength;
	float				m_ScopeRegion;
	float				m_CurPos;		// <=(m_TotalLength-m_ScopeRegion) && >=0

	float				m_pxBarStart;
	float				m_pxBarWidth;
	float				m_ScaleToPx;

	float				m_PosUnit;

	BOOL				m_bhorizontal;
	HCURSOR				m_hHand;
	HCURSOR				m_hResizeH;
	HCURSOR				m_hResizeV;
	DWORD				m_Style;

public:
	// labels
	::rt::Buffer<float>	m_Labels;		// >=0 && <= m_TotalLength
	Gdiplus::Pen		m_LabelPen;
	Gdiplus::SolidBrush	m_ScopeSolidBrush;

protected:
	void				UpdateLayout();
	BOOL				HitTestScope(const POINT& pt);
	UINT				HitScopeSizingHander(const POINT& pt);  // 0 not hit 1 left 2 right
	virtual LRESULT MessageProc(UINT uMsg, WPARAM wParam, LPARAM lParam);

public:
	CScopeSlider(_meta_::_GdiplusControlHost* pHost);

	void				SetTotalLength(float len){ m_TotalLength=len; UpdateLayout(); }
	float				GetTotalLength()const{ return m_TotalLength; }
	void				SetScopeSize(float len){ m_ScopeRegion=len; UpdateLayout(); }
	float				GetScopeSize()const{ return m_ScopeRegion; }
	void				SetPos(float x){ m_CurPos=x; UpdateLayout(); }
	float				GetPos()const{ return m_CurPos; }
	void				SetMoveUnit(float x){ m_PosUnit = x; }
	void				SetControlStyle(DWORD x){ m_Style = x; RedrawContent(); }
	DWORD				GetControlStyle()const{ return m_Style; }
	void				SetScopeSizeRange(float imin,float imax = FLT_MAX){ m_ScopeSize_Min=imin; m_ScopeSize_Max=imax; UpdateLayout(); }
	void				GetScopeSizeRange(float* imin,float* imax = NULL){ if(imin)*imin=m_ScopeSize_Min; if(imax)*imax=m_ScopeSize_Max; }
	void				RedrawContent();
};


} // namespace ui