#include "stdafx.h"
#include "ui_base.h"
#include <atlconv.h>


using namespace Gdiplus;

void ui::CachedImage::Create(Graphics* pGfx, UINT w, UINT h, LPCBYTE pData, UINT px_format, UINT stride)
{	ASSERT(pGfx);
	_SafeDel(m_pCachedBitmap);
	
	if(stride == UINT_MAX)stride = w*(((px_format>>8)&0xff)/8);
	Bitmap img(w,h,stride,px_format,::rt::_CastToNonconst(pData));
	
	_SafeDel(m_pCachedBitmap);
	m_pCachedBitmap = new CachedBitmap(&img,pGfx);
	ASSERT(m_pCachedBitmap);
}

//void ui::CachedImage::Load(Graphics* pGfx, LPCTSTR fn)
//{	ASSERT(pGfx);
//	_SafeDel(m_pCachedBitmap);
//	
//	Bitmap img(CT2W(fn));
//	
//	_SafeDel(m_pCachedBitmap);
//	m_pCachedBitmap = new CachedBitmap(&img,pGfx);
//	ASSERT(m_pCachedBitmap);
//}

namespace ui
{

namespace defaults
{
	Gdiplus::Font*	m_pFont = NULL;
};

namespace _meta_
{	class _Gdiplus_Startup
	{	ULONG_PTR _gditoken;
	public:
		_Gdiplus_Startup()
		{	GdiplusStartupInput gdiplusStartupInput;
			GdiplusStartup(&_gditoken,&gdiplusStartupInput,NULL);

			defaults::m_pFont = new Gdiplus::Font(L"Arial",10,0,Gdiplus::UnitPixel);
		}
		~_Gdiplus_Startup()
		{	
			_SafeDel(defaults::m_pFont);
			GdiplusShutdown(_gditoken);
		}
	};
	_Gdiplus_Startup	_TheGdiplus;
} //namespace _meta_

} // namespace ui


ui::_meta_::_GdiplusControlRoot::_GdiplusControlRoot(_GdiplusControlHost* p)
	:m_pControlHost(p)
	,m_BgBrush(Color(0))
	,m_BorderPen(Color(0))
	,m_GridPen(Color(0))
{
	Gdiplus::FontFamily ff(L"Verdana");
	m_pFont = new Gdiplus::Font(&ff,8);

	{// Set Color
		Color clr;

		clr.SetFromCOLORREF(GetSysColor(COLOR_BTNFACE));
		m_BgBrush.SetColor(clr);

		clr.SetFromCOLORREF(GetSysColor(COLOR_3DDKSHADOW));
		m_BorderPen.SetColor(clr);

		clr.SetFromCOLORREF(GetSysColor(COLOR_3DSHADOW));
		m_GridPen.SetColor(clr);
	}

	m_BorderPen.SetWidth(2.5f);
	m_GridPen.SetDashStyle(DashStyleDot);
	m_iGridWidth = 32;

	m_hArrow = ::LoadCursor(NULL,IDC_ARROW);
	m_bFrameBufferPainted = FALSE;

	_Lock = 0;
}



LRESULT ui::_meta_::_GdiplusControlRoot::MessageProc(UINT uMsg, WPARAM wParam, LPARAM lParam)
{
	if(WM_BUFFER_REALLOCATED!=uMsg)
	{
		if(WM_KEYDOWN!=uMsg){}
		else SendNotify(NOTIFY_KEYDOWN,wParam,lParam);

		return m_pControlHost->MessageProcDefault(uMsg,wParam,lParam);	
	}
	else
	{
		m_bFrameBufferPainted = FALSE; return 0;
	}
}


ui::_meta_::_GdiplusControlRoot::~_GdiplusControlRoot()
{
	_SafeDel(m_pFont);
}

Gdiplus::ARGB ui::_meta_::_GdiplusControlRoot::GetStockColor(int index, BYTE alpha)
{
	static const COLORREF _colors[] = 
	{
		RGB(0x00,0x00,0x99),	RGB(0x99,0x00,0x00),	RGB(0x00,0x99,0x00),
		RGB(0x55,0x55,0x00),	RGB(0x99,0x55,0x00),	RGB(0xEE,0x55,0x00),	RGB(0xEE,0x00,0x00),
		RGB(0x55,0x99,0x00),	RGB(0x99,0x99,0x00),	RGB(0xEE,0x99,0x00),
		RGB(0x00,0xEE,0x00),	RGB(0x55,0xEE,0x00),	RGB(0x99,0xEE,0x00),	RGB(0xEE,0xEE,0x00),
		RGB(0x55,0x00,0x55),	RGB(0x99,0x00,0x55),	RGB(0xEE,0x00,0x55),
		RGB(0x00,0x55,0x55),	RGB(0x55,0x55,0x55),	RGB(0x99,0x55,0x55),	RGB(0xEE,0x55,0x55),
		RGB(0x00,0x99,0x55),	RGB(0x55,0x99,0x55),	RGB(0x99,0x99,0x55),	RGB(0xEE,0x99,0x55),
		RGB(0x00,0xEE,0x55),	RGB(0x55,0xEE,0x55),	RGB(0x99,0xEE,0x55),	RGB(0xEE,0xEE,0x55),
		RGB(0x55,0xEE,0xEE),	RGB(0x55,0x00,0x99),	RGB(0x99,0x00,0x99),	RGB(0xEE,0x00,0x99),
		RGB(0x00,0x55,0x99),	RGB(0x55,0x55,0x99),	RGB(0x99,0x55,0x99),	RGB(0xEE,0x55,0x99),
		RGB(0x00,0x99,0x99),	RGB(0x55,0x99,0x99),	RGB(0x99,0x99,0x99),	RGB(0xEE,0x99,0x99),
		RGB(0x00,0xEE,0x99),	RGB(0x55,0xEE,0x99),	RGB(0x99,0xEE,0x99),	RGB(0xEE,0xEE,0x99),
		RGB(0x00,0x00,0xEE),	RGB(0x55,0x00,0xEE),	RGB(0x99,0x00,0xEE),	RGB(0xEE,0x00,0xEE),
		RGB(0x00,0x55,0xEE),	RGB(0x55,0x55,0xEE),	RGB(0x99,0x55,0xEE),	RGB(0xEE,0x55,0xEE),
		RGB(0x00,0x99,0xEE),	RGB(0x55,0x99,0xEE),	RGB(0x99,0x99,0xEE),	RGB(0xEE,0x99,0xEE),
		RGB(0x00,0xEE,0xEE),	RGB(0x99,0xEE,0xEE)
	};

	index = index%sizeofArray(_colors);
	return Gdiplus::Color::MakeARGB(alpha,GetRValue(_colors[index]),GetGValue(_colors[index]),GetBValue(_colors[index]));
}

void ui::_meta_::_GdiplusControlRoot::RedrawBackgroundChessboard(Gdiplus::Graphics& gfx,const RECT& region, UINT bright_a, UINT bright_b)
{
	if( region.right > region.left && region.bottom > region.top )
	{
		SolidBrush	a(Color(bright_a,bright_a,bright_a)),b(Color(bright_b,bright_b,bright_b));

		for(int y=region.top; y<region.bottom; y+=8)
		for(int x=region.left, use_a=1&(y/8); x<region.right; x+=8,use_a++)
		{
			Rect rc(x,y,8,8);
			gfx.FillRectangle((use_a&1)?&a:&b,(Rect&)rc);
		}
	}
}

void ui::_meta_::_GdiplusControlRoot::RedrawBackground(Gdiplus::Graphics& gfx,const RECT& layout, BOOL with_gird)
{
	RECT region = layout;

	if( region.right > region.left + 11 && region.bottom > region.top + 11 )
	{
		//Border
		gfx.FillRectangle(&m_BgBrush,(Rect&)region);

		region.bottom--;
		region.right--;

		if(with_gird)
		{
			gfx.DrawRectangle(&m_BorderPen,(Rect&)region);

			region.left += 5;
			region.top += 5;
			region.right -= 5;
			region.bottom -= 5;

			UINT h = region.bottom - region.top;
			UINT w = region.right - region.left;

			//Grid
			int cx = max(2,w / m_iGridWidth);
			int cy = max(2,h / m_iGridWidth);
			for(int i=0;i<=cy;i++)
			{
				gfx.DrawLine(&m_GridPen,	region.left,	region.top+i*h/cy,
											region.left+w,	region.top+i*h/cy);
			}
			for(int i=0;i<=cx;i++)
			{
				gfx.DrawLine(&m_GridPen,	region.left+i*w/cx,	region.top,
											region.left+i*w/cx,	region.top+h);
			}
		}
	}
}

BOOL ui::IsWindowThemeComposited()
{
	typedef BOOL (__stdcall *Func_IsCompositionActive)();
	Func_IsCompositionActive _call = NULL;

	HMODULE lib = NULL;
	BOOL ret =	(lib = ::LoadLibrary(_T("uxtheme.dll"))) &&
				(_call = (Func_IsCompositionActive)::GetProcAddress(lib,"IsCompositionActive")) &&
				_call();
	if(lib)::FreeLibrary(lib);
	return ret;
}
