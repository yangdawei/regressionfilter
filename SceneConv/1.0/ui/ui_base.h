#pragma once

//////////////////////////////////////////////////////////////////////
// Visual Computing Foundation Classes (VCFC)
//					Jiaping Wang  2006.2
//					e_boris2002@hotmail.com
//
// (C) Copyright Jiaping Wang 2004.
//
// Permission to copy, use, modify, sell and distribute this software
// is granted provided this copyright notice appears in all copies.
// This software is provided "as is" without express or implied
// warranty, and with no claim as to its suitability for any purpose.
//
// Absolutely no warranty!!
//////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////
//  Brief Description
//
//  ui_base.h
//
//  basic definition for UI elements based on GDI+
//  automatically startup GDI+ runtime
//	misc GDI+ wrapper classes
//
//////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////
// Revise History
//
// Inital version	2006.11.8		Jiaping
//
//////////////////////////////////////////////////////////////////////

#include "..\w32\win32_ver.h"
#include "..\w32\wnd_app.h"

#include <atlimage.h>
#include <gdiplus.h>
#pragma comment(lib,"gdiplus.lib")

namespace ui
{

class CachedImage
{
public:
	enum
	{	PXFMT_16BPP_GrayScale	   = PixelFormat16bppGrayScale,
		PXFMT_16BPP_RGB555   	   = PixelFormat16bppRGB555   ,
		PXFMT_16BPP_RGB565   	   = PixelFormat16bppRGB565   ,
		PXFMT_16BPP_ARGB1555 	   = PixelFormat16bppARGB1555 ,
		PXFMT_24BPP_RGB      	   = PixelFormat24bppRGB      ,
		PXFMT_32BPP_RGB      	   = PixelFormat32bppRGB      ,
		PXFMT_32BPP_ARGB     	   = PixelFormat32bppARGB     ,
		PXFMT_32BPP_PARGB    	   = PixelFormat32bppPARGB    ,
		PXFMT_48BPP_RGB      	   = PixelFormat48bppRGB      ,
		PXFMT_64BPP_ARGB     	   = PixelFormat64bppARGB     ,
		PXFMT_64BPP_PARGB    	   = PixelFormat64bppPARGB    
	};	
	
protected:
	UINT					m_Width;
	UINT					m_Height;
	Gdiplus::CachedBitmap*	m_pCachedBitmap;
	
public:
	CachedImage(){ m_pCachedBitmap = NULL; }
	~CachedImage(){ _SafeDel(m_pCachedBitmap); }
	operator Gdiplus::CachedBitmap*()
	{ ASSERT(m_pCachedBitmap); return m_pCachedBitmap; }
	UINT GetWidth(){ return m_Width; }
	UINT GetHeight(){ return m_Height; }
	
	void Create(Gdiplus::Graphics* pGfx, UINT w, UINT h, LPCBYTE pData, UINT px_format = PXFMT_32BPP_ARGB, UINT stride = UINT_MAX);
	void Load(Gdiplus::Graphics* pGfx, LPCTSTR fn);
};

namespace _meta_
{
class _GdiplusControlHost
{	
public:
	virtual LRESULT MessageProcDefault(UINT uMsg, WPARAM wParam, LPARAM lParam) = 0;
	virtual BOOL Notify(UINT msg,WPARAM wParam=0, LPARAM lParam=0) = 0;
	virtual void GetRedrawRect(LPRECT lpRect) = 0;
	virtual void SetMouseCapture() = 0;
	virtual void ReleaseMouseCapture() = 0;
	virtual Gdiplus::Graphics* GetGraphics() = 0;
};

class _GdiplusControlRoot
{	int _Lock;	
public:
	enum
	{	NOTIFY_GRAPHICS_UPDATED = 1,	// Graphics content is updated
		NOTIFY_SELECT_CHANGED,
		NOTIFY_KEYDOWN,
		NOTIFY_CUSTOM_BASE = 100
	};
	enum
	{	WM_BUFFER_REALLOCATED = WM_USER + 1
	};
protected:
	_GdiplusControlHost*	m_pControlHost;
	UINT					m_CtrlId;

	Gdiplus::Font*			m_pFont;
	Gdiplus::Pen			m_BorderPen;
	Gdiplus::Pen			m_GridPen;
	Gdiplus::SolidBrush		m_BgBrush;
	UINT					m_iGridWidth;

	HCURSOR					m_hArrow;
	BOOL					m_bFrameBufferPainted;
	DWORD					m_Style;

protected:
	_GdiplusControlRoot(_GdiplusControlHost*);
	~_GdiplusControlRoot();

public:
	Gdiplus::ARGB			GetStockColor(int index, BYTE alpha = 255);
	Gdiplus::SolidBrush*	GetBackgroundBrush(){ return &m_BgBrush; }
	Gdiplus::Font*			GetFont(){ return m_pFont; }
	UINT					GetControlID() const { return m_CtrlId; }
	virtual LRESULT			MessageProc(UINT uMsg, WPARAM wParam, LPARAM lParam);
	virtual void			RedrawContent() = 0;

protected: //helpers
	void RedrawBackground(Gdiplus::Graphics& gfx,const RECT& region, BOOL with_gird = TRUE);
	void RedrawBackgroundChessboard(Gdiplus::Graphics& gfx,const RECT& region, UINT bright_a = 255, UINT bright_b = 204);
	BOOL SendNotify(UINT msg,WPARAM wParam=0, LPARAM lParam=0){ return m_pControlHost->Notify(msg,wParam,lParam); }

public:
	void SetControlStyle(DWORD x){ m_Style = x; MessageProc(WM_PAINT,0,0); }
	DWORD GetControlStyle()const{ return m_Style; }
	void SetControlHost(_GdiplusControlHost* p){ m_pControlHost=p; }
	_GdiplusControlHost* GetControlHost(){ return m_pControlHost; }
	BOOL IsOncePainted()const{ return m_bFrameBufferPainted; }
	void LockUI(){ _Lock++; }
	void UnlockUI(){ _Lock--; if(!IsUILocked())RedrawContent(); }
	BOOL IsUILocked(){ ASSERT(_Lock>=0); return _Lock; }
};
} // namespace _meta_

struct ControlNotification:public NMHDR
{
	WPARAM wParam;
	LPARAM lParam;
};

#pragma warning(disable: 4355)	//  'this' used in base member initializer list

template<class ui_class>
class CControlBase:	public ui_class,
					public w32::CWndBase,
					public _meta_::_GdiplusControlHost
{	
protected:
	HWND				m_hNotifyWnd;
	ATL::CImage			m_FrameBuffer;
	Gdiplus::Graphics*	m_pGraphics;

protected:
	BOOL Notify(UINT msg,WPARAM wParam, LPARAM lParam)
	{	ControlNotification	msgpack;
		msgpack.code     = msg;
		msgpack.hwndFrom = hWnd;
		msgpack.idFrom   = m_CtrlId;
		msgpack.wParam   = wParam;
		msgpack.lParam   = lParam;
		::SendMessage(m_hNotifyWnd,WM_NOTIFY,(WPARAM)m_CtrlId,(LPARAM)&msgpack);

		if(msg == NOTIFY_GRAPHICS_UPDATED){ m_bFrameBufferPainted = TRUE; Redraw(); }
		return TRUE;
	}
	void GetRedrawRect(LPRECT lpRect){ ::GetClientRect(*this,lpRect); }
	void SetMouseCapture(){ ::SetCapture(hWnd); }
	void ReleaseMouseCapture(){ ::ReleaseCapture(); }
	LRESULT MessageProcDefault(UINT uMsg, WPARAM wParam, LPARAM lParam){ return w32::CWndBase::WndProc(uMsg,wParam,lParam); }

	void Redraw()
	{	if(!m_FrameBuffer.IsNull())
		{	m_FrameBuffer.Draw(w32::CWndBase::GetDC(),0,0);
			w32::CWndBase::ReleaseDC();
		}
	}

public:
	CControlBase():m_pGraphics(NULL),ui_class(this),m_hNotifyWnd(NULL){}
	Gdiplus::Graphics* GetGraphics()
	{	if(m_pGraphics)
		{	RECT rc;				
			GetRedrawRect(&rc);
			Gdiplus::Rect grc(rc.left,rc.top,rc.right-rc.left,rc.bottom-rc.top);
			m_pGraphics->SetClip(grc,Gdiplus::CombineModeReplace);
		}
		return m_pGraphics;
	}
	void SetNotifyWindow(HWND wnd){ m_hNotifyWnd = wnd; }
	HWND GetNotifyWindow() const { return m_hNotifyWnd; }
	BOOL Create(HWND Parent, UINT nID = ui_class::IDD)
	{	if(CWndBase::Create(WS_CHILDWINDOW,Parent))
		{	m_CtrlId = nID;
			if(m_hNotifyWnd==NULL)m_hNotifyWnd = Parent;
			return TRUE;
		}else return FALSE;
	}
	void	ShowWindow(int nCmdShow=SW_SHOW)
	{	if(nCmdShow==SW_SHOW)RedrawContent();
		w32::CWndBase::ShowWindow(nCmdShow); 
	}
	LRESULT WndProc(  UINT message,  WPARAM wParam,  LPARAM lParam )
	{	if(message != WM_CREATE)
		{
			if(message != WM_PAINT)
			{
				if(message != WM_SIZE)
				{	if(message == WM_LBUTTONDOWN)SetFoucs();
					LRESULT ret = MessageProc(message,wParam,lParam);
					if(message == WM_DESTROY)
					{	_SafeDel(m_pGraphics);
						if(!m_FrameBuffer.IsNull())
							m_FrameBuffer.ReleaseDC();
					}
					return ret;
				}
				else
				{	
					UINT cx = max(1,GET_X_LPARAM(lParam));
					UINT cy = max(1,GET_Y_LPARAM(lParam));
					
					MessageProc(WM_BUFFER_REALLOCATED,0,0);
					if( !m_FrameBuffer.IsNull() && m_FrameBuffer.GetWidth() == cx && m_FrameBuffer.GetHeight() == cy ){}
					else
					{	_SafeDel(m_pGraphics);
						if( !m_FrameBuffer.IsNull() )
						{	m_FrameBuffer.ReleaseDC();
							m_FrameBuffer.Destroy();
							if(m_FrameBuffer.Create(cx,cy,24))
								m_pGraphics = new Gdiplus::Graphics(m_FrameBuffer.GetDC());
						}
						else
						{	if(m_FrameBuffer.Create(cx,cy,24))
							{	m_pGraphics = new Gdiplus::Graphics(m_FrameBuffer.GetDC());
								MessageProc(WM_CREATE,0,0);
							}
						}
					}

					return MessageProc(message,wParam,lParam);
				}
			}
			else
			{	w32::CWndBase::WndProc(message,wParam,lParam);
				Redraw();
				return 0;
			}
		}
		else
			return w32::CWndBase::WndProc(message,wParam,lParam);
	}
};
#pragma warning(default: 4355)

namespace _meta_
{	class _ComposedControlHost
	{public:
		virtual BOOL Composed_Notify(UINT composed_id, UINT msg, WPARAM wParam, LPARAM lParam) = 0;
		virtual void Composed_GetRedrawRect(UINT id, LPRECT lpRect) = 0;
	};
} // namespace _meta_

#pragma warning(disable: 4355)
template<class ui_class>
class CControlComposition:	public ui_class,
							public _meta_::_GdiplusControlHost
{	
private:
	LPARAM			_LastWM_SIZE;

protected:
	UINT		m_CompositionId;
	_meta_::_GdiplusControlHost*	m_pExternalControlHost;
	_meta_::_ComposedControlHost*	m_pControlComposer;

	BOOL Notify(UINT msg,WPARAM wParam, LPARAM lParam)
	{	if(msg == NOTIFY_GRAPHICS_UPDATED)m_bFrameBufferPainted = TRUE;
		return m_pControlComposer->Composed_Notify(m_CompositionId,msg,wParam,lParam);
	}
	void GetRedrawRect(LPRECT lpRect)
	{	m_pControlComposer->Composed_GetRedrawRect(m_CompositionId,lpRect);
	}

	void SetMouseCapture(){ m_pExternalControlHost->SetMouseCapture(); }
	void ReleaseMouseCapture(){ m_pExternalControlHost->ReleaseMouseCapture(); }
	Gdiplus::Graphics* GetGraphics()
	{	Gdiplus::Graphics* g = m_pExternalControlHost->GetGraphics();
		if(g)
		{	RECT rc;
			m_pControlComposer->Composed_GetRedrawRect(m_CompositionId,&rc);
			Gdiplus::Rect grc(rc.left,rc.top,rc.right-rc.left,rc.bottom-rc.top);
			g->SetClip(grc,Gdiplus::CombineModeIntersect);
		}
		return g;
	}
	LRESULT MessageProcDefault(UINT uMsg, WPARAM wParam, LPARAM lParam){ return 0xffff; }

public:
	template<class T> // T should inherit both _ComposedControlHost and _GdiplusControlRoot
	CControlComposition(T*	pExternalHost, UINT composed_id):ui_class(this)
	{	m_pControlComposer = pExternalHost;
		m_pExternalControlHost = pExternalHost->GetControlHost(); 
		m_CompositionId = composed_id;
		_LastWM_SIZE = 0;
	}
};
#pragma warning(default: 4355)

BOOL IsWindowThemeComposited();

namespace defaults
{
	extern Gdiplus::Font*	m_pFont;
};

} // namespace ui

