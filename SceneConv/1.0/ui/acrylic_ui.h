#pragma once

//////////////////////////////////////////////////////////////////////
// Visual Computing Foundation Classes (VCFC)
//					Jiaping Wang  2006.2
//					e_boris2002@hotmail.com
//
// (C) Copyright Jiaping Wang 2004.
//
// Permission to copy, use, modify, sell and distribute this software
// is granted provided this copyright notice appears in all copies.
// This software is provided "as is" without express or implied
// warranty, and with no claim as to its suitability for any purpose.
//
// Absolutely no warranty!!
//////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////
//  Brief Description
//
//  AcrylicFrame.h
//
//  A lightweight WPF-like UI, semi-transparency, anti-aliased shaped window.
//  Based on layered window, supported from Win2000
//  Just like Acrylic resin, transparency and lightweight.
//
//////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////
// Revise History
//
// Inital version	2011.2.27		Jiaping
//
//////////////////////////////////////////////////////////////////////

#include "ui_base.h"
#include "..\rt\string_type.h"
#include "..\rt\compact_image.h"
#include "..\rt\fixvec_type.h"

namespace ui
{

class CAcrylicBackground;		// RGBA background
	class CAcrylicFrameWnd;		// Interactions for SYSMENU/MINIMIZE/MAXIMIZE/CLOSE/RESIZING
		class CAcrylicFrameAttachment;

static const int _SIZE_AUTO = INFINITE;

//////////////////////////////////////////////
// Macros
enum _tagBGMode
{	BGM_STRETCH_NONE		= 0x00,
	BGM_STRETCH_FREE		= 0x01,
	BGM_STRETCH_BOXING		= 0x02,
	BGM_STRETCH_MASK		= 0x0f,

	//BGM_SYSFRAME_NONE		= 0x00,
	//BGM_SYSFRAME_CMDBOX		= 0x10,		// Minimize/Maximize/Close
	//BGM_SYSFRAME_SIZEBOX	= 0x20,		// Resizing handles
	//BGM_SYSFRAME_MASK		= 0xf0,

	BGM_RENDER_CENTER_BLOCK	= 0x80,

	BGM_MASK = 0xff
};

enum _tagBGSource
{	BGS_FILFNAME = 0x00000000,
	BGS_RESOURCE = 0x10000000,
	BGS_MASK = 0x10000000
};

enum _tagFrameHostFlag
{	
	FHF_SET_TITLE = 0x1,
	FHF_SET_SUBCLASS = 0xff
};

enum _tagCaptionFlag
{
	CPF_IMAGE_NO_STRETCH = 0x1	// override _CaptionImagePlacement's width and height
};

///////////////////////////////////////////////////////////
// Resources
#pragma pack(1)
class px_1c
{public:	union
			{	struct{ BYTE _p[1];	};
				struct{ BYTE a;		};
};			};
class px_3c
{public:	union
			{	struct{ BYTE _p[3];	};
				struct{ BYTE b,g,r;		};
};			};
class px_4c
{public:	union
			{	struct{ BYTE _p[4];	};
				struct{ BYTE b,g,r,a;	};
			};
 px_4c(){}
 px_4c(BYTE ia, BYTE ir, BYTE ig, BYTE ib){ a=min(254,ia); r=ir; g=ig; b=ib; }
 // alpha=255 will cause composition artifact which result in a color with alpha=0
 operator Gdiplus::Color& (){ return *((Gdiplus::Color*)this); }
};
class Position
{public:	union
			{	struct{ INT _p[2];	};
				struct{ INT x,y;	};
			};
 Position(){}
 Position(const POINT& x){ *((ULONGLONG*)this) = (ULONGLONG&)x; }
 INT& operator [](int i){ return _p[i]; }
};
class Dimension
{public:	union
			{	struct{ INT _p[2];	};
				struct{ INT cx,cy;	};
			};
 Dimension(){}
 Dimension(const SIZE& x){ *((ULONGLONG*)this) = (ULONGLONG&)x; }
 INT& operator [](int i){ return _p[i]; }
};
#pragma pack()

namespace _meta_
{	struct _ImageData
	{	int width;	int height;
		int byte_steps;
		int channel;
		LPBYTE data;
	};
	void _LoadImageSetResourceCatalog(LPCTSTR pCat);
	BOOL _LoadImage(LPCTSTR image_fn, DWORD flag, _ImageData* pID);
	template<int chann>
	struct _PixelFormat{};
		template<> struct _PixelFormat<1>
		{	enum{ Result = PixelFormat8bppIndexed }; 
			typedef ui::px_1c PixelType;
		};
		template<> struct _PixelFormat<3>
		{	enum{ Result = PixelFormat24bppRGB };
			typedef ui::px_3c PixelType;
		};
		template<> struct _PixelFormat<4>
		{	enum{ Result = PixelFormat32bppARGB };
			typedef ui::px_4c PixelType;
		};
}

template<int chann = 4>
class CAcrylicImage: public rt::Image< typename _meta_::_PixelFormat<chann>::PixelType >
{
	Gdiplus::Bitmap*	_pBitmap;
public:
	operator Gdiplus::Bitmap* (){ return _pBitmap; }
	BOOL Load(LPCTSTR image_fn, DWORD flag = BGS_RESOURCE)
	{
		_meta_::_ImageData id = { width, height, Step_Bytes, chann, (LPBYTE)lpData };
		BOOL ret = _meta_::_LoadImage(image_fn, flag, &id);
		if(ret)
		{	if(id.data != (LPBYTE)lpData)
			{	SetSize(0,0);
				width = id.width;
				height = id.height;
				Step_Bytes = id.byte_steps;
				Pad_Bytes = width*sizeof(PixelType) - Step_Bytes;
				lpData = (PixelType*)id.data;
				_SafeDel(_pBitmap);
				VERIFY(_pBitmap = new Gdiplus::Bitmap(width, height, Step_Bytes, _meta_::_PixelFormat<chann>::Result , (LPBYTE)lpData));
			}
		}
		return ret;
	}
	void Release(){ _SafeDel(_pBitmap); SetSize(0,0);  }
	~CAcrylicImage(){ Release(); }
};



/////////////////////////////////////////////////////////
// Styles
struct Style_Background
{
	// background mode 
	// - BGM_STRETCH_BOXING: H[3], V[3] defines all 9 blocks
	// - others, ignore
	int		_BackgroundHorizontalBox[3];
	int		_BackgroundVerticalBox[3];

	Style_Background(){ ZeroMemory(this,sizeof(*this)); }
};

struct Style_FrameWnd
{
	int				_ResizingHandlerTop_CY[2];
	int				_ResizingHandlerBottom_CY[2];
	int				_ResizingHandlerLeft_CX[2];
	int				_ResizingHandlerRight_CX[2];
	int				_ResizingHandlerCornerSize;

	int				_Caption_CY[2];
	int				_StatusBar_CY[2];

	int				_MinimizeBox_CX[2];
	int				_MaximizeBox_CX[2];
	int				_CloseBox_CX[2];
	int				_CommandBoxs_CY[2];
	

	Gdiplus::Rect	_CaptionImagePlacement;

	Style_FrameWnd(){ ZeroMemory(this,sizeof(*this)); }
};

enum _tagTextBoxFlag
{
	TBF_FORMAT_DIRECTIONRIGHTTOLEFT	= 0x0001,
    TBF_FORMAT_DIRECTIONVERTICAL	= 0x0002,
    TBF_FORMAT_NOFONTFALLBACK		= 0x0400,
    TBF_FORMAT_NOWRAP				= 0x1000,
    TBF_FORMAT_LINELIMIT			= 0x2000,		// don't allow partial line to be displayed
    TBF_FORMAT_NOCLIP				= 0x4000,

    TBF_TRIMMING_CHARACTER			= 0x10000,
    TBF_TRIMMING_WORD				= 0x20000,
    TBF_TRIMMING_ELLIPSISCHARACTER	= 0x30000,
    TBF_TRIMMING_ELLIPSISWORD		= 0x40000,
    TBF_TRIMMING_ELLIPSISPATH		= 0x50000,

	TBF_FORMATFLAG_MASK				= 0x0ffff,
	TBF_TRIMMINGFLAG_MASK			= 0xf0000,
};

enum _tagUIGroupFlag
{
	UGF_LAYOUT_VERTICAL		= 0x0001,
	UGF_LAYOUT_HORIZONTAL	= 0x0000,
	UGF_LAYOUT_MASK			= 0x0001,
};

enum _tagUIGroupElementFlag
{
	UGEF_ALIGN_NEAR			= 0x0000,
	UGEF_ALIGN_CENTER		= 0x0001,
	UGEF_ALIGN_FAR			= 0x0002,
	UGEF_ALIGN_MASK			= 0x0003,

	UGEF_HIDDEN				= 0x80000000,
};

struct Style_TextBox
{
	int				_Align;		// 0, 1, 2 for left,center,right
	int				_vAlign;	// 0, 1, 2 for top,center,right
	float			_Margin_Top;
	float			_Margin_Bottom;
	float			_Margin_Left;
	float			_Margin_Right;
	DWORD			_Flag;		// _tagTextBoxFlag
	px_4c			_Color;
	px_4c			_BorderColor;
	float			_BorderWidth;

	Style_TextBox()
	{	ZeroMemory(this,sizeof(*this));
		_Color.a = 255;
		_vAlign = 1;
	}
};

struct Style_Group
{
	int				_Margin_Near;
	int				_Margin_Far;

	Style_Group(){ ZeroMemory(this,sizeof(*this)); }
};

struct Style_GroupElement
{	int		_PaddingNear;
	int		_PaddingFar;
	int		_Size;			// _SIZE_AUTO
};


///////////////////////////////////////////////////////////
// UI Elements
class CAcrylicUIElement
{	
public:
	Position	m_Position;
	Dimension	m_Size;
protected:
	CAcrylicUIElement();
public:
	virtual	void UpdateLayout(){};
	virtual void Render(Gdiplus::Graphics & gfx) = 0;
	virtual void Release(){};
};

class CAcrylicGroup: public CAcrylicUIElement,
					 protected Style_Group
{
	int				_AutoSizedCount;
	int				_TotalFixedSize;

	int				_LeftFixedSize;
	int				_CenterFixedSize;
	int				_RightFixedSize;

	int				_LeftAutoSizedCount;
	int				_CenterAutoSizedCount;
	int				_RightAutoSizedCount;

	int				_CenterStartOffset;
	int				_CenterEndOffset;

	void			_CalucateLayout(UINT cl,UINT ele_start, UINT ele_end, int pos, int pos_end, int a_size);
protected:
	struct _Element:public Style_GroupElement
	{	
		CAcrylicUIElement*	pElement;
		UINT	_Flag;		// _tagUIGroupElementFlag
		BOOL	_Clipped;
		void	_Init();
		TYPETRAITS_DECL_IS_AGGREGATE(true);
	};
	rt::BufferEx<_Element>	m_Elements;
	UINT					m_Flag;			// _tagUIGroupFlag
public:
	CAcrylicGroup();
	Style_Group& Style(){ return *this; }
	virtual	void UpdateLayout();
	virtual void Render(Gdiplus::Graphics & gfx);

	UINT				GetElementCount() const { return m_Elements.GetSize(); }
	CAcrylicUIElement*	GetElement(int idx){ return m_Elements[idx].pElement; }
	void				SetElement(int idx, CAcrylicUIElement* p = NULL){ m_Elements[idx].pElement = p; }
	Style_GroupElement&	ElementStyle(int idx){ return m_Elements[idx]; }
	UINT				GetElementFlag(int idx) const { return m_Elements[idx]._Flag; }		//_tagUIGroupElementFlag
	void				SetElementFlag(int idx, UINT flag){ m_Elements[idx]._Flag = flag; } //_tagUIGroupElementFlag
	void				RemoveElement(int idx){ m_Elements.erase(idx); }
	void				InsertElement(int idx, CAcrylicUIElement* p = NULL);
	void				SetElementCount(int tot);
	void				RecalculateElememntLayout();
};

class CAcrylicTextBox:public CAcrylicUIElement,
					  protected Style_TextBox
{
protected:
	rt::StringW			m_Text;
public:
	Gdiplus::Font*		m_pFont;

	// created
	Gdiplus::StringFormat* m_pStringFormat;
public:
	CAcrylicTextBox();
	Style_TextBox& Style(){ return *this; }
	void SetText(LPCTSTR str){ SetText(rt::String_Ref(str)); }
	void SetText(const rt::String_Ref& str);
	void Render(Gdiplus::Graphics & gfx);
	void Release();
};

class CAcrylicBackground:public CAcrylicUIElement,
						 protected Style_Background
{
protected:
	// background
	INT					m_BackgroundImageAncherSize;
	CAcrylicImage<4>	m_BackgroundImage;
	UINT				m_BackgroundMode;

public:
	CAcrylicBackground();
	~CAcrylicBackground(){ Release(); }
	Style_Background& Style(){ return *this; }
	void	Release();
	void	Render(Gdiplus::Graphics& gfx);
	void	SetStyle(const Style_Background& m);
	BOOL	SetImage(LPCTSTR image_fn, DWORD flag = BGS_RESOURCE|BGM_STRETCH_NONE); // call after window is created
};


///////////////////////////////////////////////////////////
// UI Classes
class CAcrylicFrameWnd:public w32::CLayeredWnd,
					   protected Style_FrameWnd
{
	int					__FrameTop_CY;

protected:
	LRESULT			WndProc(UINT message, WPARAM wParam, LPARAM lParam);
	LRESULT			_NC_HitTest(INT x, INT y);

	BOOL			_IsVistaCompositionTheme;
	UINT			_LastNCLBUTTONDOWN_Target;
	void			_PatchNCSYSCOMMAND();

protected:
	Gdiplus::Font*		_pCaptionFont;
	Gdiplus::Font*		_pStatusBarFont;

public:
	virtual void OnRender(HDC hMemoryDC);
	CAcrylicBackground	m_Background;
	CAcrylicTextBox		m_Caption;
	CAcrylicImage<4>	m_CaptionImage;

	CAcrylicGroup		m_StatusBar;
	int					m_FrameMsgPaneIdx;

public:
	CAcrylicFrameWnd();
	~CAcrylicFrameWnd();
	BOOL	Create(HWND Parent = NULL);
	void	SetRenderLayerExtension(int left, int top, int right, int bottom);

	void	SetStyle(const Style_FrameWnd& m);
	void	GetStyle(Style_FrameWnd& m);

	BOOL	SetCaptionImage(LPCTSTR image_fn, DWORD flag = BGS_RESOURCE|CPF_IMAGE_NO_STRETCH);

	void	SetStatusBar(UINT pane_count, int frame_msg_pane_idx = -1, int default_padding = 3);
	void	SetStatusPaneWidth(int idx, int size = _SIZE_AUTO);
	void	SetStatusPaneText(int idx, LPCTSTR str);	// auto-refresh
	void	SetStatusPaneAlignment(int idx, int align);	// align = 0, 1, 2
	Style_TextBox& GetStatusPaneTextStyle(int idx);
};

class CAcrylicFrameAttachment:public CAcrylicFrameWnd
{
	// attachee area
	int			_ClientOffset_XY[2];
	int			_FrameThick_CXCY[2];
	void		_RearrangeAttachee();

	static LRESULT	CALLBACK WndProc_SubclassAttachee(HWND hwnd, UINT uMsg, WPARAM wParam, LPARAM lParam);

protected:
	LRESULT		WndProc(UINT message, WPARAM wParam, LPARAM lParam);
	HWND		m_hAttachee;
	UINT		m_AttcheeFlag;
	WNDPROC		m_pAttacheeWndProc;

public:
	CAcrylicFrameAttachment();
	BOOL		Create(HWND Parent = NULL);			// Use CAcrylicFrameAttachment as the parent when creating the FrameWnd to be attached
	BOOL		AttachToFrameWnd(HWND hFrame, UINT flag = FHF_SET_TITLE);
	void		SetAttachmentMargin(int left, int top, int right, int bottom);
};

}