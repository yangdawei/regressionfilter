#include "stdafx.h"
#include "wglshader.h"

// glsl API entry functions

PFNGLDELETEOBJECTARBPROC			wglh::glsl::CwglLanguageObject::DeleteObjectARB = NULL;
PFNGLGETHANDLEARBPROC				wglh::glsl::CwglLanguageObject::GetHandleARB = NULL;
PFNGLDETACHOBJECTARBPROC			wglh::glsl::CwglLanguageObject::DetachObjectARB = NULL;
PFNGLCREATESHADEROBJECTARBPROC		wglh::glsl::CwglLanguageObject::CreateShaderObjectARB = NULL;
PFNGLSHADERSOURCEARBPROC			wglh::glsl::CwglLanguageObject::ShaderSourceARB = NULL;
PFNGLCOMPILESHADERARBPROC			wglh::glsl::CwglLanguageObject::CompileShaderARB = NULL;
PFNGLCREATEPROGRAMOBJECTARBPROC		wglh::glsl::CwglLanguageObject::CreateProgramObjectARB = NULL;
PFNGLATTACHOBJECTARBPROC			wglh::glsl::CwglLanguageObject::AttachObjectARB = NULL;
PFNGLLINKPROGRAMARBPROC				wglh::glsl::CwglLanguageObject::LinkProgramARB = NULL;
PFNGLUSEPROGRAMOBJECTARBPROC		wglh::glsl::CwglLanguageObject::UseProgramObjectARB = NULL;
PFNGLVALIDATEPROGRAMARBPROC			wglh::glsl::CwglLanguageObject::ValidateProgramARB = NULL;

PFNGLGETOBJECTPARAMETERFVARBPROC	wglh::glsl::CwglLanguageObject::GetObjectParameterfvARB = NULL;
PFNGLGETOBJECTPARAMETERIVARBPROC	wglh::glsl::CwglLanguageObject::GetObjectParameterivARB = NULL;
PFNGLGETINFOLOGARBPROC				wglh::glsl::CwglLanguageObject::GetInfoLogARB = NULL;
PFNGLGETATTACHEDOBJECTSARBPROC		wglh::glsl::CwglLanguageObject::GetAttachedObjectsARB = NULL;
PFNGLGETUNIFORMLOCATIONARBPROC		wglh::glsl::CwglLanguageObject::GetUniformLocationARB = NULL;
PFNGLGETACTIVEUNIFORMARBPROC		wglh::glsl::CwglLanguageObject::GetActiveUniformARB = NULL;
PFNGLGETSHADERSOURCEARBPROC			wglh::glsl::CwglLanguageObject::GetShaderSourceARB = NULL;	

PFNGLUNIFORM1FARBPROC				wglh::glsl::CwglLanguageObject::Uniform1fARB = NULL;
PFNGLUNIFORM2FARBPROC				wglh::glsl::CwglLanguageObject::Uniform2fARB = NULL;
PFNGLUNIFORM3FARBPROC				wglh::glsl::CwglLanguageObject::Uniform3fARB = NULL;
PFNGLUNIFORM4FARBPROC				wglh::glsl::CwglLanguageObject::Uniform4fARB = NULL;
PFNGLUNIFORM1IARBPROC				wglh::glsl::CwglLanguageObject::Uniform1iARB = NULL;
PFNGLUNIFORM2IARBPROC				wglh::glsl::CwglLanguageObject::Uniform2iARB = NULL;
PFNGLUNIFORM3IARBPROC				wglh::glsl::CwglLanguageObject::Uniform3iARB = NULL;
PFNGLUNIFORM4IARBPROC				wglh::glsl::CwglLanguageObject::Uniform4iARB = NULL;
PFNGLUNIFORM1FVARBPROC				wglh::glsl::CwglLanguageObject::Uniform1fvARB = NULL;
PFNGLUNIFORM2FVARBPROC				wglh::glsl::CwglLanguageObject::Uniform2fvARB = NULL;
PFNGLUNIFORM3FVARBPROC				wglh::glsl::CwglLanguageObject::Uniform3fvARB = NULL;
PFNGLUNIFORM4FVARBPROC				wglh::glsl::CwglLanguageObject::Uniform4fvARB = NULL;
PFNGLUNIFORM1IVARBPROC				wglh::glsl::CwglLanguageObject::Uniform1ivARB = NULL;
PFNGLUNIFORM2IVARBPROC				wglh::glsl::CwglLanguageObject::Uniform2ivARB = NULL;
PFNGLUNIFORM3IVARBPROC				wglh::glsl::CwglLanguageObject::Uniform3ivARB = NULL;
PFNGLUNIFORM4IVARBPROC				wglh::glsl::CwglLanguageObject::Uniform4ivARB = NULL;
PFNGLUNIFORMMATRIX2FVARBPROC		wglh::glsl::CwglLanguageObject::UniformMatrix2fvARB = NULL;
PFNGLUNIFORMMATRIX3FVARBPROC		wglh::glsl::CwglLanguageObject::UniformMatrix3fvARB = NULL;
PFNGLUNIFORMMATRIX4FVARBPROC		wglh::glsl::CwglLanguageObject::UniformMatrix4fvARB = NULL;
PFNGLGETUNIFORMFVARBPROC			wglh::glsl::CwglLanguageObject::GetUniformfvARB = NULL;
PFNGLGETUNIFORMIVARBPROC			wglh::glsl::CwglLanguageObject::GetUniformivARB = NULL;

PFNGLBINDATTRIBLOCATIONARBPROC		wglh::glsl::CwglLanguageObject::BindAttribLocationARB;
PFNGLGETACTIVEATTRIBARBPROC			wglh::glsl::CwglLanguageObject::GetActiveAttribARB;
PFNGLGETATTRIBLOCATIONARBPROC		wglh::glsl::CwglLanguageObject::GetAttribLocationARB;

PFNGLPROGRAMPARAMETERIEXTPROC		wglh::glsl::CwglLanguageObject::ProgramParameteriEXT;


/////////////////////////////////////////////////////////////
// CwglLanguageObject
BOOL wglh::glsl::CwglLanguageObject::LoadExtensionEntryPoints()
{
	if(CreateProgramObjectARB)
	{
		//All entry point should be available
		ASSERT(CreateProgramObjectARB);
		ASSERT(CreateShaderObjectARB);
		ASSERT(DeleteObjectARB);
		ASSERT(GetHandleARB);
		ASSERT(DetachObjectARB);
		ASSERT(ShaderSourceARB);
		ASSERT(CompileShaderARB);
		ASSERT(AttachObjectARB);
		ASSERT(LinkProgramARB);
		ASSERT(UseProgramObjectARB);
		ASSERT(ValidateProgramARB);
		ASSERT(GetObjectParameterfvARB);
		ASSERT(GetObjectParameterivARB);
		ASSERT(GetInfoLogARB);
		ASSERT(GetAttachedObjectsARB);
		ASSERT(GetUniformLocationARB);
		ASSERT(GetActiveUniformARB);
		ASSERT(GetShaderSourceARB);	

		ASSERT(Uniform1fARB);
		ASSERT(Uniform2fARB);
		ASSERT(Uniform3fARB);
		ASSERT(Uniform4fARB);
		ASSERT(Uniform1iARB);
		ASSERT(Uniform2iARB);
		ASSERT(Uniform3iARB);
		ASSERT(Uniform4iARB);
		ASSERT(Uniform1fvARB);
		ASSERT(Uniform2fvARB);
		ASSERT(Uniform3fvARB);
		ASSERT(Uniform4fvARB);
		ASSERT(Uniform1ivARB);
		ASSERT(Uniform2ivARB);
		ASSERT(Uniform3ivARB);
		ASSERT(Uniform4ivARB);
		ASSERT(UniformMatrix2fvARB);
		ASSERT(UniformMatrix3fvARB);
		ASSERT(UniformMatrix4fvARB);
		ASSERT(GetUniformfvARB);
		ASSERT(GetUniformivARB);

		ASSERT(BindAttribLocationARB);
		ASSERT(GetActiveAttribARB);
		ASSERT(GetAttribLocationARB);

		return TRUE;
	}
	else
	{
#define VPLEEP_Load(x) if(!( *((PROC*)&x) = wglGetProcAddress("gl"#x) ))goto VPLEEP_LoadFailed
#define VPLEEP_TryLoad(x) if(!( *((PROC*)&x) = wglGetProcAddress("gl"#x) )){x=NULL; _CheckDump("Faild to load"#x<<"\n");}

		VPLEEP_Load(CreateProgramObjectARB);
		VPLEEP_Load(CreateShaderObjectARB);
		VPLEEP_Load(DeleteObjectARB);
		VPLEEP_Load(GetHandleARB);
		VPLEEP_Load(DetachObjectARB);
		VPLEEP_Load(ShaderSourceARB);
		VPLEEP_Load(CompileShaderARB);
		VPLEEP_Load(AttachObjectARB);
		VPLEEP_Load(LinkProgramARB);
		VPLEEP_Load(UseProgramObjectARB);
		VPLEEP_Load(ValidateProgramARB);
		VPLEEP_Load(GetObjectParameterfvARB);
		VPLEEP_Load(GetObjectParameterivARB);
		VPLEEP_Load(GetInfoLogARB);
		VPLEEP_Load(GetAttachedObjectsARB);
		VPLEEP_Load(GetUniformLocationARB);
		VPLEEP_Load(GetActiveUniformARB);
		VPLEEP_Load(GetShaderSourceARB);	

		VPLEEP_Load(Uniform1fARB);
		VPLEEP_Load(Uniform2fARB);
		VPLEEP_Load(Uniform3fARB);
		VPLEEP_Load(Uniform4fARB);
		VPLEEP_Load(Uniform1iARB);
		VPLEEP_Load(Uniform2iARB);
		VPLEEP_Load(Uniform3iARB);
		VPLEEP_Load(Uniform4iARB);
		VPLEEP_Load(Uniform1fvARB);
		VPLEEP_Load(Uniform2fvARB);
		VPLEEP_Load(Uniform3fvARB);
		VPLEEP_Load(Uniform4fvARB);
		VPLEEP_Load(Uniform1ivARB);
		VPLEEP_Load(Uniform2ivARB);
		VPLEEP_Load(Uniform3ivARB);
		VPLEEP_Load(Uniform4ivARB);
		VPLEEP_Load(UniformMatrix2fvARB);
		VPLEEP_Load(UniformMatrix3fvARB);
		VPLEEP_Load(UniformMatrix4fvARB);
		VPLEEP_Load(GetUniformfvARB);
		VPLEEP_Load(GetUniformivARB);
		VPLEEP_Load(BindAttribLocationARB);
		VPLEEP_Load(GetActiveAttribARB);
		VPLEEP_Load(GetAttribLocationARB);

		VPLEEP_TryLoad(ProgramParameteriEXT);

#undef VPLEEP_Load

		return TRUE;

VPLEEP_LoadFailed:
		
		_CheckErrorW32;
		CreateProgramObjectARB = NULL;
		return FALSE;	
	}

}

int wglh::glsl::CwglLanguageObject::GetVersion()
{
	LPCSTR extstr = ex::GetExtensionString();
	if(extstr)
	{
		LPCSTR p = strstr(extstr,"GL_ARB_shading_language_");
		if(p)
		{
			CHAR buf[100];
			ZeroMemory(buf,100);
			p+=24;
			int i=0;
			for(;i<98;i++)
			{
				if( p[i] >= '0' && p[i] <= '9' )
					buf[i] = p[i];
			}

			return atoi(buf);
		}
	}

	_CheckErrorGL;
	return -1;
}

void wglh::glsl::CwglLanguageObject::Destroy()
{
	if(m_Handle)
	{
		DeleteObjectARB(m_Handle); 
		_CheckErrorGL;
		m_Handle = NULL;
	}
}

void wglh::glsl::CwglLanguageObject::DumpLog()
{
	if( m_Handle )
	{
		int len = GetLogLength();
		if(len)
		{
			LPSTR log = new char[len+2];
			GetInfoLogARB(m_Handle,len,NULL,log);
			_CheckErrorGL;
			_CheckDump('\n'<<log<<'\n');
			_SafeDelArray(log);
		}
	}
	else
	{
		_CheckDump(" Object is not created.\n");
	}
}

////////////////////////////////////////////////////


/////////////////////////////////////////////////////////////
// CwglProgram
void wglh::glsl::CwglProgram::Create()
{
	ASSERT(m_Handle==NULL); 
	m_Handle = CreateProgramObjectARB();
	_CheckErrorGL;
}

void wglh::glsl::CwglProgram::Link()
{
	try
	{
		ASSERT(m_Handle); 
		if(m_GeometryEnabled)
		{
			ASSERT(ProgramParameteriEXT);
			int temp;
			glGetIntegerv(GL_MAX_GEOMETRY_OUTPUT_VERTICES_EXT,&temp);
			ProgramParameteriEXT(m_Handle, GL_GEOMETRY_VERTICES_OUT_EXT, temp-1);
			ProgramParameteriEXT(m_Handle, GL_GEOMETRY_INPUT_TYPE_EXT, m_GeometryInputMode);
			ProgramParameteriEXT(m_Handle, GL_GEOMETRY_OUTPUT_TYPE_EXT, m_GeometryOutputMode);
		}
		LinkProgramARB(m_Handle); 
		_CheckErrorGL; 
		DumpCompiledInformation();
	}
	catch(...)  //stop exception
	{
		_CheckErrorGL; 
	}
}

void wglh::glsl::CwglProgram::DumpCompiledInformation()
{
	_CheckDump(_T("\tAttached Shaders :\t")<<GetAttachedShaderCount()<<_T('\n'));
	_CheckDump(_T("\tActived Attribute:\t")<<GetActivedAttribCount()<<_T('\n'));
	_CheckDump(_T("\tActived Uniform  :\t")<<GetActivedUniformCount()<<_T('\n'));
}

void wglh::glsl::CwglProgram::AddShader(GLhandleARB sh)
{
	ASSERT(m_Handle);
	ASSERT( ((GLhandleARB)sh) != NULL );

	AttachObjectARB(m_Handle,sh);
	_CheckErrorGL;
}

void wglh::glsl::CwglProgram::RemoveShader(GLhandleARB sh)
{
	ASSERT(m_Handle);
	ASSERT( ((GLhandleARB)sh) != NULL );

	DetachObjectARB(m_Handle,sh);
	_CheckErrorGL;
}

void wglh::glsl::CwglProgram::BindAttribLocation(LPCSTR VarName,int AttribIndex)
{
	ASSERT(m_Handle);
	ASSERT(VarName);

	BindAttribLocationARB(m_Handle,AttribIndex,VarName);
	_CheckErrorGL;
}

void wglh::glsl::CwglProgram::GetActivedUniform(int index,wglh::glsl::CwglShaderVarible_RT& varible)
{
	varible.SetNameBufferSize(GetMaxUniformNameLength()+1);
	varible.Index = index;

	GetActiveUniformARB(	m_Handle,
							index,
							varible.NameLen-1,
							NULL,
							&varible.Size,
							&varible.Type,
							varible.Name);
	_CheckErrorGL;
}

void wglh::glsl::CwglProgram::GetActivedAttrib(int index,wglh::glsl::CwglShaderVarible_RT& varible)
{
	varible.SetNameBufferSize(GetMaxUniformNameLength()+1);
	varible.Index = index;

	GetActiveAttribARB(m_Handle,index,varible.NameLen-1,NULL,&varible.Size,&varible.Type,varible.Name);
	_CheckErrorGL;
}

void wglh::glsl::CwglProgram::SetGeometryShaderIOMode(int input, int output)
{
	m_GeometryEnabled = true;
	m_GeometryInputMode = input;
	m_GeometryOutputMode = output;
}


/////////////////////////////////////////////////////////

/////////////////////////////////////////////////////////
// Shader Object

void wglh::glsl::CwglShaderVarible_RT::SetNameBufferSize(int len)
{
	if(len == NameLen){}
	else
	{
		ASSERT(len+1 > 0);
		_SafeDelArray(Name);
		Name = new char[len+1];
		ASSERT(Name);
		NameLen = len;
	}
}

void wglh::glsl::CwglShaderVarible_RT::DumpDescription()
{
	ASSERT(Size);

	switch(Type)
	{
	case GL_FLOAT						:_CheckDump(" float "); break;
	case GL_FLOAT_VEC2_ARB				:_CheckDump(" vec2 "); break;
	case GL_FLOAT_VEC3_ARB				:_CheckDump(" vec3 "); break;
	case GL_FLOAT_VEC4_ARB				:_CheckDump(" vec4 "); break;
	case GL_INT							:_CheckDump(" int "); break;
	case GL_INT_VEC2_ARB				:_CheckDump(" ivec2 "); break;
	case GL_INT_VEC3_ARB				:_CheckDump(" ivec3 "); break;
	case GL_INT_VEC4_ARB				:_CheckDump(" ivec4 "); break;
	case GL_BOOL_ARB					:_CheckDump(" bool "); break;
	case GL_BOOL_VEC2_ARB				:_CheckDump(" bvec2 "); break;
	case GL_BOOL_VEC3_ARB				:_CheckDump(" bvec3 "); break;
	case GL_BOOL_VEC4_ARB				:_CheckDump(" bvec4 "); break;
	case GL_FLOAT_MAT2_ARB				:_CheckDump(" mat2 "); break;
	case GL_FLOAT_MAT3_ARB				:_CheckDump(" mat3 "); break;
	case GL_FLOAT_MAT4_ARB				:_CheckDump(" mat4 "); break;
	case GL_SAMPLER_1D_ARB				:_CheckDump(" sampler1D "); break;
	case GL_SAMPLER_2D_ARB				:_CheckDump(" sampler2D "); break;
	case GL_SAMPLER_3D_ARB				:_CheckDump(" sampler3D "); break;
	case GL_SAMPLER_CUBE_ARB			:_CheckDump(" samplerCube "); break;
	case GL_SAMPLER_1D_SHADOW_ARB		:_CheckDump(" sampler1DShadow "); break;
	case GL_SAMPLER_2D_SHADOW_ARB		:_CheckDump(" sampler2DShadow "); break;
	case GL_SAMPLER_2D_RECT_ARB			:_CheckDump(" sampler2DRect "); break;
	case GL_SAMPLER_2D_RECT_SHADOW_ARB	:_CheckDump(" sampler2DRectShadow "); break;
	default: _CheckDump(" [UnknownType] ");
	}

	_CheckDump(Name);

	if(Size > 1)						
	{
		_CheckDump("["<<Size<<"]");
	}

	_CheckDump("; ");
}

void wglh::glsl::CwglShaderCode::Create(GLenum ShaderType)
{
	ASSERT(m_Handle==NULL); 
	m_Handle = CreateShaderObjectARB(ShaderType); 
	_CheckErrorGL; 
}

void wglh::glsl::CwglShaderCode::Compile()
{
	ASSERT(m_Handle);
	CompileShaderARB(m_Handle);
	_CheckErrorGL;
	m_bSourceCodeChanged = FALSE;
}
void wglh::glsl::CwglShaderCode::SetSourceCode(const GLcharARB * pStr)
{ 
	ASSERT(pStr);
	ShaderSourceARB(m_Handle,1,&pStr,NULL); 
	_CheckErrorGL; 

	m_bSourceCodeChanged = TRUE;
}

BOOL wglh::glsl::CwglShaderCode::IsCompiled()
{
	if( m_bSourceCodeChanged ) return FALSE;

	GLint ret = 0; 
	if(IsCreated())
	{
		GetObjectParameterivARB(m_Handle,GL_OBJECT_COMPILE_STATUS_ARB,&ret); _CheckErrorGL; 
	} 
	return ret;
}

/////////////////////////////////////////////
// Program project
BOOL wglh::glsl::CwglProgramEx::Rebuild()
{
	Destroy();

	if( m_ShaderList.GetSize() )
	{
		__super::Create();
		_CheckErrorGL;
		ASSERT(m_Handle);

		BOOL CompileOk = TRUE;
		// Compile all shaders
		_CheckDump("Compile all affected shaders:\n");
		for(UINT i=0;i<m_ShaderList.GetSize();i++)
		{
			ASSERT( m_ShaderList[i]->m_FileName );
			
			if( m_ShaderList[i]->m_FileName[0] != _T(':') )
			{// 
				_CheckDump("   [ "<<m_ShaderList[i]->m_FileName<<" ] >>> ");
			}
			else
			{// Stock shader
				_CheckDump("   [ "<<(m_ShaderList[i]->m_FileName+1)<<" ] >>> ");
			}

			if( !m_ShaderList[i]->IsCompiled() ) 
			{
				if( m_ShaderList[i]->IsCreated() )
				{
					m_ShaderList[i]->Compile();

					if( m_ShaderList[i]->IsCompiled() )
					{
						_CheckDump("Compiled ok.\n");
					}
					else
					{
						CompileOk = FALSE;
						_CheckDump("Compiled failed.\n");
						_CheckDump("Compile Log : ");
						m_ShaderList[i]->DumpLog();
						_CheckDump('\n');
					}
				}
				else
				{
					CompileOk = FALSE;
					_CheckDump("No Source code available.\n");
				}
			}
			else
			{
				_CheckDump("Ready.\n");
			}

			if( m_ShaderList[i]->IsCompiled() )
			{
				__super::AddShader(*m_ShaderList[i]);
				_CheckErrorGL;
			}
		}

		if( CompileOk )
		{
			__super::Link();
			_CheckErrorGL;
			_CheckDump("Linker >>> ");
			if(IsLinked())
			{
				_CheckDump("ok.\n");
			}
			else
			{
				CompileOk = FALSE;
				_CheckDump("failed.\nCompile Log : ");
				DumpLog();
				_CheckDump('\n');
			}

			return CompileOk;
		}
		else
		{
			_CheckDump("Linker >>> One or more shader is not compiled, build stopped.\n\n");
		}
	}

	return FALSE;
}

void wglh::glsl::CwglShaderCodeManagment::DestroyAllLoadedObjects()
{
	for(UINT i=0;i<m_ProgramList.GetSize();i++)
		m_ProgramList[i]->Destroy();
	m_ProgramList.Clear();

	for(UINT i=0;i<m_SourceCodeList.GetSize();i++)
		m_SourceCodeList[i].Shader->Destroy();
	m_SourceCodeList.SetSize(0);
}

void wglh::glsl::CwglShaderCodeManagment::OnFolderChanged()
{
	if(m_hNotifyWnd)
		::SendMessage(m_hNotifyWnd,WGLH_GLSL_SHADER_CHANGED_MESSAGE,0,0); 
}

const wglh::glsl::CObjectExpression<wglh::glsl::CwglProgramEx>& 
wglh::glsl::CwglShaderCodeManagment::operator = 
(const wglh::glsl::CObjectExpression<wglh::glsl::CwglProgramEx>& expr)
{
	m_ProgramList = expr;
	m_SourceCodeList.SetSize();
	return m_ProgramList;
}

const wglh::glsl::CObjectExpression<wglh::glsl::CwglProgramEx>& 
wglh::glsl::CwglShaderCodeManagment::operator = (wglh::glsl::CwglProgramEx& prog)
{
	m_ProgramList = prog;
	m_SourceCodeList.SetSize();
	return m_ProgramList;
}

const wglh::glsl::CObjectExpression<wglh::glsl::CwglProgramEx>& 
wglh::glsl::CwglShaderCodeManagment::operator += 
(const wglh::glsl::CObjectExpression<wglh::glsl::CwglProgramEx>& expr)
{
	m_ProgramList = wglh::glsl::CObjectExpression<wglh::glsl::CwglProgramEx>(m_ProgramList,expr);
	m_SourceCodeList.SetSize();
	return m_ProgramList;
}

const wglh::glsl::CObjectExpression<wglh::glsl::CwglProgramEx>& 
wglh::glsl::CwglShaderCodeManagment::operator += (wglh::glsl::CwglProgramEx& obj)
{
	m_ProgramList = wglh::glsl::CObjectExpression<wglh::glsl::CwglProgramEx>(m_ProgramList,obj);
	m_SourceCodeList.SetSize();
	return m_ProgramList;
}

wglh::glsl::CwglShaderCodeManagment::CwglShaderCodeManagment(LPCTSTR pShaderFolder)
{
	// ASSERT( g_ShaderManager == NULL ); //single object in one module EXE/DLL
	// Allow the latest created CwglShaderCodeManagment to be the global one.
	g_ShaderManager = this;

	int len = (int)_tcslen(pShaderFolder);
	ASSERT( len < MAX_PATH );

	_tcscpy(m_szFilenameBuffer,pShaderFolder);
	m_pFilenameBegin = &m_szFilenameBuffer[len];
	if( m_szFilenameBuffer[len - 1] != _T('\\') )
	{
		m_szFilenameBuffer[len] = _T('\\');
		m_pFilenameBegin++;
	}
}

BOOL wglh::glsl::CwglShaderCodeManagment::UpdateShaderList(HWND notify_wnd)
{
	if(wglh::glsl::CwglLanguageObject::IsSupportted()){}
	else
	{
		::MessageBox(NULL,_T("This program requires OpenGL Shading Language 1.0 support."),_T("Antiquated Graphics Card Detected"),MB_OK);
		return FALSE;
	}

	m_SourceCodeList.SetSize();  //clear old

	{
		CObjectExpression<CwglShaderCode> m_ShaderAll;

		for(UINT i=0;i<m_ProgramList.GetSize();i++)
		{
			m_ShaderAll += m_ProgramList[i]->m_ShaderList;
		}

		m_SourceCodeList.SetSize( m_ShaderAll.GetSize() );
		for(UINT i=0;i<m_SourceCodeList.GetSize();i++)
		{
			ASSERT( m_ShaderAll[i]->m_FileName );

			m_SourceCodeList[i].Filename = m_ShaderAll[i]->m_FileName[0] == _T(':')?NULL:m_ShaderAll[i]->m_FileName;
			m_SourceCodeList[i].LastModify.dwHighDateTime = NULL;
			m_SourceCodeList[i].LastModify.dwLowDateTime = NULL;
			m_SourceCodeList[i].Shader = m_ShaderAll[i];
			_CheckHeap;
		}
	}

	LoadChangedShaders();

	ASSERT( m_pFilenameBegin[-1] == _T('\\') );
	m_pFilenameBegin[-1] = _T('\0');
	Create(m_szFilenameBuffer,TRUE);

	m_pFilenameBegin[-1] = _T('\\');

	if(notify_wnd)
		SetNotifyWindow(notify_wnd);

	return TRUE;
}

void wglh::glsl::CwglShaderCodeManagment::LoadChangedShaders()
{

	rt::Buffer<BOOL> m_ProgramChanged;
	m_ProgramChanged.SetSize(m_ProgramList.GetSize());
	m_ProgramChanged.Zero();

	BOOL SomeProgChanged = FALSE;
	BOOL SomeCodeChanged = FALSE;

	::Sleep(300);

    for(UINT i=0;i<m_SourceCodeList.GetSize();i++)
	{
		// check changing
		BOOL change = FALSE;

		if( m_SourceCodeList[i].Filename )
		{
			_tcscpy(m_pFilenameBegin,m_SourceCodeList[i].Filename);

			w32::CFile64 file;

			{	for(UINT i=0;i<5;i++)
					if(file.Open(m_szFilenameBuffer))break;
					else ::Sleep(200);
			}

			if(file.IsOpen())
			{
				FILETIME ft;
				file.GetTime_LastModify(&ft);
				if( ft.dwLowDateTime != m_SourceCodeList[i].LastModify.dwLowDateTime || 
					ft.dwHighDateTime != m_SourceCodeList[i].LastModify.dwHighDateTime )
				{
					if( !SomeCodeChanged )
						SomeCodeChanged = TRUE;
#ifdef WGLH_SHADER_ENABLE_DEBUGDUMP
					_CheckDump("[ "<<m_SourceCodeList[i].Filename<<" ]  ");
#endif
					char * pText = new char[(size_t)(file.GetLength()+1)];
					ZeroMemory(pText,(size_t)(file.GetLength()+1));
					ASSERT(pText);
					if(file.Read(pText,(DWORD)file.GetLength()) == file.GetLength())
					{
						m_SourceCodeList[i].LastModify = ft;
						if(!m_SourceCodeList[i].Shader->IsCreated())
						{
							if(_tcsstr(m_SourceCodeList[i].Filename,_T(".cxx")))
							{
								m_SourceCodeList[i].Shader->Create(wglh::glsl::CwglShaderCode::FragmentShader);
							}
							else if(_tcsstr(m_SourceCodeList[i].Filename,_T(".hxx")))
							{
								m_SourceCodeList[i].Shader->Create();
							}
							else
							{
								m_SourceCodeList[i].Shader->Create(wglh::glsl::CwglShaderCode::GeometryShader);
							}
						}
						m_SourceCodeList[i].Shader->SetSourceCode(pText);
#ifdef WGLH_SHADER_ENABLE_DEBUGDUMP
						_CheckDump("loaded ok.\n");
#endif

						change = TRUE;
						m_SourceCodeList[i].LastModify.dwLowDateTime = ft.dwLowDateTime;
						m_SourceCodeList[i].LastModify.dwHighDateTime = ft.dwHighDateTime;
					}
#ifdef WGLH_SHADER_ENABLE_DEBUGDUMP
					else
					{
						_CheckDump("failed to load.\n");
					}
#endif

					_SafeDelArray(pText);
				}
			}
			else
			{
				_CheckDump(m_SourceCodeList[i].Filename<<" failed to open.\n");
			}

		}
		else if( !m_SourceCodeList[i].Shader->IsCompiled() )	//For stock shader
		{
			change = TRUE;
		}

		if(change)
		{
			// search dependence
			for(UINT g=0;g<m_ProgramList.GetSize();g++)
			{
				if( m_ProgramList[g]->IsDepended(*m_SourceCodeList[i].Shader) )
				{
					m_ProgramChanged[g] = TRUE;
					SomeProgChanged = TRUE;
				}
			}	
		}
	}

	if( SomeProgChanged )
	{
		_CheckDump("\nRebuild all affected programs:\n");
		for(UINT i=0;i<m_ProgramChanged.GetSize();i++)
		{
			if( m_ProgramChanged[i] )
			{
				_CheckDump("No."<<i<<"  ");
				m_ProgramList[i]->Rebuild();
				_CheckDump('\n');
			}
		}

		::Sleep(500);
	}
}

void wglh::glsl::CwglShaderCodeManagment::UpdateShaders(const CObjectExpression<CwglShaderCode>& expr)
{
	rt::Buffer<BOOL> m_ProgramChanged;
	m_ProgramChanged.SetSize(m_ProgramList.GetSize());
	m_ProgramChanged.Zero();

	ASSERT(expr.GetSize());

	BOOL SomeProgChanged = FALSE;

	_CheckDump("\nShader changing is explicitly declared, search program dependecy:\n");
	for(UINT i=0;i<expr.GetSize();i++)
	{
		ASSERT(expr[i]->m_FileName);
		_CheckDump("[ ");
		if( expr[i]->m_FileName[0] == _T(':') )
		{
			_CheckDump(expr[i]->m_FileName+1);
		}
		else
		{
			_CheckDump(expr[i]->m_FileName);
		}

		_CheckDump(" ] \n");

		for(UINT g=0;g<m_ProgramList.GetSize();g++)
		{
			if( m_ProgramList[g]->IsDepended(*expr[i]) )
			{
				m_ProgramChanged[g] = TRUE;
				SomeProgChanged = TRUE;
			}
		}
	}

	if( SomeProgChanged )
	{
		_CheckDump("\nRebuild all affected programs:\n");
		for(UINT i=0;i<m_ProgramChanged.GetSize();i++)
		{
			if( m_ProgramChanged[i] )
			{
				_CheckDump("No."<<i<<"  ");
				m_ProgramList[i]->Rebuild();
				_CheckDump('\n');
			}
		}

		::Sleep(500);
	}
}

wglh::glsl::CwglShaderCodeManagment* wglh::glsl::CwglShaderCodeManagment::g_ShaderManager = NULL;

wglh::glsl::CwglShaderCodeManagment* wglh::glsl::GetShaderManager()
{
	ASSERT(CwglShaderCodeManagment::g_ShaderManager);
	return CwglShaderCodeManagment::g_ShaderManager; 
}



