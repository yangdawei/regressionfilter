#include "stdafx.h"

#include "wglhelper.h"
#include "wglextension.h"
#include <math.h>


/////////////////////////////////////////////
//CwglApp
wglh::CwglApp * wglh::CwglApp::g_pAppInstance = NULL;
LPCSTR			wglh::CwglWnd::pOpenGLExtensionsString = NULL;

#ifdef _DEBUG
BOOL			wglh::CwglApp::m_bOpenGLErrorFired = FALSE;
#endif

wglh::CwglApp::CwglApp(BOOL fake_obj)
{
	if(!fake_obj)g_pAppInstance = this; 

	hInstance = ::GetModuleHandle(NULL);
	pMainWnd = NULL;

	CmdLine = NULL;
	m_bRealtimeRenderingPaused = FALSE;
	m_bInRealtimeMode = FALSE;
	_bRealtimeRenderingDriven = FALSE;
}

wglh::CwglApp::~CwglApp()
{
	g_pAppInstance = NULL;
}

wglh::CwglApp * wglh::CwglApp::GetApp()
{
	static CwglApp theApp(TRUE);

	if(g_pAppInstance)
		return g_pAppInstance;
	else
		return &theApp;
}

void wglh::CwglApp::Pause(BOOL pause_it)
{
	m_bRealtimeRenderingPaused = pause_it;
}

BOOL wglh::CwglApp::IsPaused() const
{
	return m_bRealtimeRenderingPaused;
}


HINSTANCE wglh::CwglApp::GetInstanceHandle() const
{
	return hInstance;
}

wglh::CwglWnd * wglh::CwglApp::GetMainWnd()
{
	return pMainWnd;
}

BOOL wglh::CwglApp::IsRealtimeActived() const
{
	return m_bInRealtimeMode && !m_bRealtimeRenderingPaused;
}

void wglh::CwglApp::ActiveRealtimeMode(BOOL active)
{
	if(active)
	{
		m_bInRealtimeMode = TRUE;
		m_bRealtimeRenderingPaused = FALSE;
	}
	else
		m_bInRealtimeMode = FALSE;
}

BOOL wglh::CwglApp::IsIdleDrivenRendering() const
{
	return _bRealtimeRenderingDriven;
}

UINT wglh::CwglApp::Run(LPTSTR pCmd)
{
	static const int CRTA_RENDER_FPS_COUNT = 30;
	CmdLine = pCmd;

	MSG msg;
	msg.wParam = 0;

	////////////////////////////////////////////
	//A window with OpenGL must be created
	ASSERT(pMainWnd); // initialize MainWnd before running (call CwglWnd::SetAsMainWnd after CwglRealtimeApp is created)
	ASSERT(pMainWnd->IsGLInitialized()); // initialize GL render context before running   

	_bRealtimeRenderingDriven = TRUE;
	RedrawWindow(*pMainWnd,NULL,NULL,RDW_INVALIDATE);

	UINT	FrameCount = CRTA_RENDER_FPS_COUNT;
	DWORD	LastTick = timeGetTime();

	w32::_meta_::_IdleKick_MFC idle_kick;

	for(;;)
	{
		if(m_bInRealtimeMode)
		{
			while(PeekMessage(&msg,NULL,0,0,PM_REMOVE))
			{
				if(msg.message == WM_QUIT)goto CRLA_END_OF_APP;
				TranslateMessage(&msg);
				DispatchMessage(&msg);
				idle_kick.OnMessage();
			}

			if(FrameCount--){}
			else
			{
				pMainWnd->m_fRenderFPS = CRTA_RENDER_FPS_COUNT*1000.0/(max(0.5,(double)(timeGetTime() - LastTick)));
				LastTick = timeGetTime();
				FrameCount = CRTA_RENDER_FPS_COUNT;

				idle_kick.OnIdle();
			}
				
			///////////////////////////
			//Call Render
#ifdef _DEBUG		
			if(pMainWnd && (!CwglApp::GetApp()->m_bOpenGLErrorFired) )pMainWnd->Render();
#else
			if(pMainWnd)pMainWnd->Render();
#endif
		}
		else
		{
			FrameCount = CRTA_RENDER_FPS_COUNT;
			pMainWnd->m_fRenderFPS = 0;

			GetMessage(&msg,NULL,0,0);
			if(msg.message == WM_QUIT)
				goto CRLA_END_OF_APP;
			TranslateMessage(&msg);
			DispatchMessage(&msg);
		}
	}

CRLA_END_OF_APP:
#ifdef WGLH_REFRESH_SYNC
	FrameSync.Stop();
#endif

	return (UINT)msg.wParam;
}

///////////////////////////////////////////////
//CwglWnd
void (*wglh::CwglWnd::_OpenGLExtensionEntrypointLoader)() = NULL;

wglh::CwglWnd::CwglWnd(void)
{
	m_fRenderFPS = 0;
	pOpenGLExtensionsString = 0;
	_bSizeChanged = FALSE;
	_SwapIntervalExt = NULL;

	_DepthMin = 1.0f;
	_DepthMax = 20.0f;
	_FOV = 60.0f;

	SetWindowAspect();
	_bVerticalSynchronized = TRUE;

	_bInitialized = FALSE;
}

void wglh::CwglWnd::UninitGL()
{
	if((HDC)glRc)
	{
		glRc.MakeCurrent(FALSE);
		glRc.Destroy();
		_bInitialized = FALSE;
		_SwapIntervalExt = NULL;
	}
}

int wglh::CwglWnd::GetRefreshFreq()
{
	DEVMODE dm;
	if(EnumDisplaySettings(NULL,ENUM_CURRENT_SETTINGS,&dm))
	{
		return max(60,dm.dmDisplayFrequency);
	}
	else
	{
		_CheckErrorW32;
		return 60;
	}
}

void wglh::CwglWnd::DrawUnitBox()
{
	_CheckErrorGL;

	glMatrixMode(GL_MODELVIEW);
	glPushMatrix();
	glPushAttrib(0xfffffff);

	glTranslatef(-0.5f,-0.5f,-0.5f);

	num::Vec4f clr_pad;
	glGetFloatv(GL_COLOR_CLEAR_VALUE,clr_pad.data());

#define CLR_VARY 0.5f

	if(clr_pad.x>0.5f)clr_pad.x-=CLR_VARY;
	if(clr_pad.y>0.5f)clr_pad.y-=CLR_VARY;
	if(clr_pad.z>0.5f)clr_pad.z-=CLR_VARY;

#define GLV(x,y,z)	glColor3f(x*CLR_VARY+clr_pad.r,y*CLR_VARY+clr_pad.g,z*CLR_VARY+clr_pad.b); glVertex3f((x-0.01f)*1.02f,(y-0.01f)*1.02f,(z-0.01f)*1.02f);
	{
		glCullFace(GL_FRONT);
		glEnable(GL_CULL_FACE);
		glPolygonMode(GL_BACK,GL_FILL);

		glDepthMask(FALSE);
		glBegin(GL_QUADS);
		{
			//Z
			GLV(0,1,0);
			GLV(1,1,0);
			GLV(1,0,0);
			GLV(0,0,0);

			GLV(0,0,1);
			GLV(1,0,1);
			GLV(1,1,1);
			GLV(0,1,1);

			////Y
			GLV(0,0,0);
			GLV(1,0,0);
			GLV(1,0,1);
			GLV(0,0,1);
				   
			GLV(0,1,1);
			GLV(1,1,1);
			GLV(1,1,0);
			GLV(0,1,0);

			////X
			GLV(0,0,1);
			GLV(0,1,1);
			GLV(0,1,0);
			GLV(0,0,0);
				 
			GLV(1,0,0);
			GLV(1,1,0);
			GLV(1,1,1);
			GLV(1,0,1);
		}
		glEnd();
		glDepthMask(TRUE);

		//glDepthFunc(GL_ALWAYS);

		glLineWidth(1);
		glColor3f(0,0,0);
		glBegin(GL_LINE_STRIP);
		{
			glVertex3f(-0.01f, 1.01f,-0.01f);
			glVertex3f( 1.01f, 1.01f,-0.01f);
			glVertex3f( 1.01f,-0.01f,-0.01f);
			glVertex3f( 1.01f,-0.01f, 1.01f);
			glVertex3f(-0.01f,-0.01f, 1.01f);
			glVertex3f(-0.01f, 1.01f, 1.01f);
			glVertex3f( 1.01f, 1.01f, 1.01f);
			glVertex3f( 1.01f, 1.01f,-0.01f);

		}
		glEnd();
		glBegin(GL_LINES);
		{
			glVertex3f(-0.01f, 1.01f, 1.01f);
			glVertex3f(-0.01f, 1.01f,-0.01f);

			glVertex3f( 1.01f, 1.01f, 1.01f);
			glVertex3f( 1.01f,-0.01f, 1.01f);
		}
		glEnd();

		glLineWidth(2);

		glBegin(GL_LINES);
		{	
			glColor3f(1,0,0);
			glVertex3f(-0.01f,-0.01f,-0.01f);
			glVertex3f( 1.11f,-0.01f,-0.01f);

			glColor3f(0,1,0);
			glVertex3f(-0.01f,-0.01f,-0.01f);
			glVertex3f(-0.01f, 1.11f,-0.01f);

			glColor3f(0,0,1);
			glVertex3f(-0.01f,-0.01f,-0.01f);
			glVertex3f(-0.01f,-0.01f, 1.11f);
		}
		glEnd();

		//glDepthFunc(GL_LEQUAL);
		glLineWidth(1);

	}
	glPopAttrib();
	glPopMatrix();
}

BOOL wglh::CwglWnd::OnInitWnd()
{
	if(__super::OnInitWnd())
	{
		RECT rc;
		::GetWindowRect(*this,&rc);
		width = abs(rc.left - rc.right);
		height = abs(rc.top - rc.bottom);

		return TRUE;
	}
	else
		return FALSE;
}

void wglh::CwglWnd::SetPolygonMode(BOOL ForceWireframe,BOOL Cullback,BOOL All_Filled)
{
	if(ForceWireframe)
	{	glPolygonMode(GL_FRONT_AND_BACK,GL_LINE);
	}
	else
	{	glPolygonMode(GL_BACK,All_Filled?GL_FILL:GL_LINE);
		glPolygonMode(GL_FRONT,GL_FILL);
	}

	if(Cullback)
	{	glEnable(GL_CULL_FACE);
		glCullFace(GL_BACK);
	}
	else
	{	glDisable(GL_CULL_FACE);
	}
}

void wglh::CwglWnd::ScreenCoordinate_Begin(float z_near,float z_far)
{	
	glMatrixMode(GL_PROJECTION);
	glPushMatrix();
	glLoadIdentity();

	float v[4];
	glGetFloatv(GL_VIEWPORT,v);
	glOrtho(-0.5,v[2]-0.5,v[3]-0.5,-0.5,z_near,z_far);
	_CheckErrorGL;

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glTranslated(0,0,-5);

	_Z_test_enabled = glIsEnabled(GL_DEPTH_TEST);
	glDisable(GL_DEPTH_TEST);
	_Culling_enabled = glIsEnabled(GL_CULL_FACE);
	glDisable(GL_CULL_FACE);
}

void wglh::CwglWnd::ScreenCoordinate_End()
{
	glMatrixMode(GL_PROJECTION);
	glPopMatrix();
	glMatrixMode(GL_MODELVIEW);

	if(_Z_test_enabled)glEnable(GL_DEPTH_TEST);
	if(_Culling_enabled)glEnable(GL_CULL_FACE);
}

void wglh::CwglWnd::SetAsMainWnd()
{
	__super::SetAsMainWnd();
	wglh::CwglApp::GetApp()->pMainWnd = this;
}

void wglh::CwglWnd::OnInitScene()
{
	glDrawBuffer(GL_BACK);

	glClearColor(0, 0, 0, 0);
	glEnable(GL_DEPTH_TEST);

	glPolygonMode(GL_FRONT,GL_FILL);
	glPolygonMode(GL_BACK,GL_LINE);
}

BOOL wglh::CwglWnd::IsVerticalSynchronized()
{
	return _bVerticalSynchronized;
}

void wglh::CwglWnd::SetVerticalSynchronization(BOOL Synchronized)
{
	_bVerticalSynchronized = Synchronized;
	if(_SwapIntervalExt)
		((PFNWGLSWAPINTERVALEXTPROC)_SwapIntervalExt)(Synchronized);
}

BOOL wglh::CwglWnd::InitGL(DWORD AddFlags,UINT ColorBits,UINT DepthBits,UINT StencilBits,UINT AccumBits,UINT AlphaBits,UINT AuxBuf,BOOL DoubleBuf)
{
	ASSERT(hWnd);

	if(glRc.Create(hWnd,AddFlags,ColorBits,DepthBits,StencilBits,AccumBits,AlphaBits,AuxBuf,DoubleBuf))
	{
		if(!pOpenGLExtensionsString)
		{
			if(glRc.MakeCurrent())
			{
				pOpenGLExtensionsString = (LPCSTR)glGetString(GL_EXTENSIONS);
				if(!pOpenGLExtensionsString)_CheckErrorGL;

				glRc.MakeCurrent(FALSE);
			}
			else
				_CheckErrorW32;
		}

		if(glRc.MakeCurrent())
		{
			_SwapIntervalExt = wglGetProcAddress("wglSwapIntervalEXT");

			if(_OpenGLExtensionEntrypointLoader)
				_OpenGLExtensionEntrypointLoader();

			_bInitialized = TRUE;

			OnInitScene();
			ResizeWindowByClientArea(640,480);

			return TRUE;
		}
		else
		{	_CheckErrorW32;
			return FALSE;
		}
	}
	else
	{	_CheckErrorW32;
		return FALSE;
	}
}




void wglh::CwglWnd::SetWindowAspect(float Aspect)
{
	m_RenderAreaAspectRatio = Aspect;
}


void wglh::CwglWnd::SetGLView(int left,int top, int right, int bottom,float DepthMin,float DepthMax,float Fov)
{
	if( _bInitialized )
	{	_DepthMin = DepthMin;
		_DepthMax = DepthMax;
		_FOV = Fov;

		SetGLView(left,top,right,bottom);
	}
}

void wglh::CwglWnd::SetGLView(int left,int top, int right, int bottom)
{
	if( _bInitialized )
	{	double AspectRatio = (double)(right - left)/(double)(bottom - top);

		glMatrixMode(GL_PROJECTION);
		glLoadIdentity();

		gluPerspective(_FOV,AspectRatio,_DepthMin,_DepthMax);

		glViewport(left, top, right - left, bottom - top);
		_CheckErrorGL;

		OnViewportChanged(right - left,bottom - top);
	}
}

void wglh::CwglWnd::SetFOV(float Fov)
{
	if( _bInitialized )
	{	_FOV = Fov;
		float vp[4];
		glGetFloatv(GL_VIEWPORT,vp);
		double AspectRatio = (double)(vp[2])/(double)(vp[3]);

		glMatrixMode(GL_PROJECTION);
		glLoadIdentity();

		gluPerspective(_FOV,AspectRatio,_DepthMin,_DepthMax);
	}
}

void wglh::CwglWnd::GetFrameBuffer(LPBYTE pPixel,GLenum format)
{
	glReadBuffer(GL_BACK);
	glReadPixels(0,0,width,height,format,GL_UNSIGNED_BYTE,pPixel);
	_CheckErrorGL;
}

void wglh::CwglWnd::UpdateViewportLayout()
{
	if( m_RenderAreaAspectRatio < 0 )
	{
		SetGLView(0,0,width,height);
		OnRender();
	}
	else
	{//  maintain aspect
		if( (m_RenderAreaAspectRatio*height + 0.5) == width )
		{
			SetGLView(0,0,width,height);
			OnRender();	
		}
		else
		{
			if( (m_RenderAreaAspectRatio*height + 0.5) > width ) //obey width
				ResizeWindowByClientArea(width,(int)(width/m_RenderAreaAspectRatio + 0.5f));
			else //obey height
				ResizeWindowByClientArea((int)(height*m_RenderAreaAspectRatio + 0.5f),height);

			SetGLView(0,0,width,height);
			OnRender();	
		}
	}
}


LRESULT wglh::CwglWnd::WndProc(UINT message, WPARAM wParam, LPARAM lParam)
{
	Apply();

	switch(message)
	{
	case WM_SIZE:
		if(glRc.IsOk())
		{
			width = max(1,LOWORD(lParam));
			height = max(1,HIWORD(lParam));
			_bSizeChanged = TRUE;

			switch(wParam)
			{
			case SIZE_MAXHIDE:
			case SIZE_MINIMIZED:
				_bSizeMINIMIZED = TRUE;
				GetApp()->Pause(TRUE);
				break;
			case SIZE_RESTORED:
				GetApp()->Pause(FALSE);
				_bSizeMINIMIZED = FALSE;
				UpdateViewportLayout();
				break;
			case SIZE_MAXIMIZED:
				GetApp()->Pause(FALSE);
				_bSizeMINIMIZED = FALSE;
				UpdateViewportLayout();
			case SIZE_MAXSHOW:
				GetApp()->Pause(FALSE);
				break;
			}

			break;
		}
#ifndef _DEBUG
	case WM_ERASEBKGND:
		return 0;
		break;
#endif
	case WM_PAINT:
		if(glRc.IsOk() && (!GetApp()->IsRealtimeActived()))
			Render();
		break;
	case WM_ENTERSIZEMOVE:
		break;
	case WM_EXITSIZEMOVE:
		if( GetApp()->IsRealtimeActived() )
		{
			if( _bSizeChanged )
			{	Apply();
                UpdateViewportLayout();
               	_bSizeChanged = FALSE;
			}
		}
		break;
	case WM_CLOSE:
		Apply();
		OnUninitScene();
		UninitGL();
		break;
	}
	return __super::WndProc(message,wParam,lParam);
}


/////////////////////////////////////
//CwglTransformation
void wglh::CwglTransformation::glGet(BOOL involve_viewport)
{
	float proj[16];
	float mv[16];
	float v[4];

	glGetFloatv(GL_PROJECTION_MATRIX,proj);
	glGetFloatv(GL_MODELVIEW_MATRIX,mv);

	float rot_mat[9];
	num::Vec3f rotCol[3];
	rotCol[0][0] = mv[0]; rotCol[0][1] = mv[1]; rotCol[0][2] = mv[2];
	rotCol[1][0] = mv[4]; rotCol[1][1] = mv[5]; rotCol[1][2] = mv[6];
	rotCol[2][0] = mv[8]; rotCol[2][1] = mv[9]; rotCol[2][2] = mv[10];
	//m_viewRotMat.SetMat(rot_mat);
	rotCol[0].Normalize();
	rotCol[1].Normalize();
	rotCol[2].Normalize();
	m_viewRotMat.SetColumn(0,rotCol[0]);
	m_viewRotMat.SetColumn(1,rotCol[1]);
	m_viewRotMat.SetColumn(2,rotCol[2]);

	if(involve_viewport)
	{	
		glGetFloatv(GL_VIEWPORT,v);
		m_viewport = num::Vec4f(v[0],v[1],v[2],v[3]);

		v[0] = v[0] + v[2]*0.5f;
		v[1] = v[1] + v[3]*0.5f;
		v[2] = v[2]*0.5f;
		v[3] = -v[3]*0.5f;

		
	}
	else
	{	// map to [0-1]
		v[0] = v[1] = 0.5f;
		v[2] = 0.5f;
		v[3] = 0.5f;

		glGetFloatv(GL_VIEWPORT,m_viewport.data());
	}

	// Total = Projection * ModelView
	m_TotalTransformMatrix[0][0] = mv[0]*proj[0] + mv[1]*proj[4] + mv[2]*proj[8] + mv[3]*proj[12];
	m_TotalTransformMatrix[1][0] = mv[0]*proj[1] + mv[1]*proj[5] + mv[2]*proj[9] + mv[3]*proj[13];
	m_TotalTransformMatrix[2][0] = mv[0]*proj[2] + mv[1]*proj[6] + mv[2]*proj[10]+ mv[3]*proj[14];
	m_TotalTransformMatrix[3][0] = mv[0]*proj[3] + mv[1]*proj[7] + mv[2]*proj[11]+ mv[3]*proj[15];
							 	
	m_TotalTransformMatrix[0][1] = mv[4]*proj[0] + mv[5]*proj[4] + mv[6]*proj[8] + mv[7]*proj[12];
	m_TotalTransformMatrix[1][1] = mv[4]*proj[1] + mv[5]*proj[5] + mv[6]*proj[9] + mv[7]*proj[13];
	m_TotalTransformMatrix[2][1] = mv[4]*proj[2] + mv[5]*proj[6] + mv[6]*proj[10]+ mv[7]*proj[14];
	m_TotalTransformMatrix[3][1] = mv[4]*proj[3] + mv[5]*proj[7] + mv[6]*proj[11]+ mv[7]*proj[15];
							 
	m_TotalTransformMatrix[0][2] = mv[8]*proj[0] + mv[9]*proj[4] + mv[10]*proj[8] + mv[11]*proj[12];
	m_TotalTransformMatrix[1][2] = mv[8]*proj[1] + mv[9]*proj[5] + mv[10]*proj[9] + mv[11]*proj[13];
	m_TotalTransformMatrix[2][2] = mv[8]*proj[2] + mv[9]*proj[6] + mv[10]*proj[10]+ mv[11]*proj[14];
	m_TotalTransformMatrix[3][2] = mv[8]*proj[3] + mv[9]*proj[7] + mv[10]*proj[11]+ mv[11]*proj[15];
							 	 
	m_TotalTransformMatrix[0][3] = mv[12]*proj[0] + mv[13]*proj[4] + mv[14]*proj[8]  + mv[15]*proj[12];
	m_TotalTransformMatrix[1][3] = mv[12]*proj[1] + mv[13]*proj[5] + mv[14]*proj[9]  + mv[15]*proj[13];
	m_TotalTransformMatrix[2][3] = mv[12]*proj[2] + mv[13]*proj[6] + mv[14]*proj[10] + mv[15]*proj[14];
	m_TotalTransformMatrix[3][3] = mv[12]*proj[3] + mv[13]*proj[7] + mv[14]*proj[11] + mv[15]*proj[15];
		
	// Total = Window * Total
	// [v2, 0, 0,v0]
	// [ 0,v3, 0,v1]
	// [ 0, 0, 1, 0]
	// [ 0, 0, 0, 1]
    m_TotalTransformMatrix[0][0] = v[2]*m_TotalTransformMatrix[0][0] + v[0]*m_TotalTransformMatrix[3][0];
    m_TotalTransformMatrix[0][1] = v[2]*m_TotalTransformMatrix[0][1] + v[0]*m_TotalTransformMatrix[3][1];
    m_TotalTransformMatrix[0][2] = v[2]*m_TotalTransformMatrix[0][2] + v[0]*m_TotalTransformMatrix[3][2];
    m_TotalTransformMatrix[0][3] = v[2]*m_TotalTransformMatrix[0][3] + v[0]*m_TotalTransformMatrix[3][3];
																 
    m_TotalTransformMatrix[1][0] = v[3]*m_TotalTransformMatrix[1][0] + v[1]*m_TotalTransformMatrix[3][0];
    m_TotalTransformMatrix[1][1] = v[3]*m_TotalTransformMatrix[1][1] + v[1]*m_TotalTransformMatrix[3][1];
    m_TotalTransformMatrix[1][2] = v[3]*m_TotalTransformMatrix[1][2] + v[1]*m_TotalTransformMatrix[3][2];
    m_TotalTransformMatrix[1][3] = v[3]*m_TotalTransformMatrix[1][3] + v[1]*m_TotalTransformMatrix[3][3];

    if (!involve_viewport)
    {
        m_TotalTransformMatrix[2][0] = 0.5f*m_TotalTransformMatrix[2][0] + 0.5f*m_TotalTransformMatrix[3][0];
        m_TotalTransformMatrix[2][1] = 0.5f*m_TotalTransformMatrix[2][1] + 0.5f*m_TotalTransformMatrix[3][1];
        m_TotalTransformMatrix[2][2] = 0.5f*m_TotalTransformMatrix[2][2] + 0.5f*m_TotalTransformMatrix[3][2];
        m_TotalTransformMatrix[2][3] = 0.5f*m_TotalTransformMatrix[2][3] + 0.5f*m_TotalTransformMatrix[3][3];
    }
}

void wglh::CwglTransformation::GetMatrix4x4(num::Mat4x4f &mat)
{
    int i, j;
    for ( i = 0; i < 4; ++i )
        for ( j = 0; j < 4; ++j )
        {
            mat(i, j) = m_TotalTransformMatrix[i][j];
        }
}

///////////////////////////////////////
//CwglRenderContext
BOOL wglh::CwglRenderContext::Create(HWND hWnd,DWORD AddFlags,UINT ColorBits,UINT DepthBits,UINT StencilBits,UINT AccumBits,UINT AlphaBits,UINT AuxBuf,BOOL DoubleBuf)
{
	ASSERT(dc == NULL);
	dc = GetDC(hWnd);
	if(!dc)
	{
		_CheckErrorW32;
		return FALSE;
	}

	AssocWnd = hWnd;

	PIXELFORMATDESCRIPTOR pfd;
	ZeroMemory(&pfd,sizeof(PIXELFORMATDESCRIPTOR));

	pfd.nSize = sizeof(PIXELFORMATDESCRIPTOR);
	pfd.nVersion = 1;
	pfd.cAccumBits = AccumBits;
	pfd.cAlphaBits = AlphaBits;
	pfd.cAuxBuffers = AuxBuf;
	pfd.cColorBits = ColorBits;
	pfd.cDepthBits = DepthBits;
	pfd.cStencilBits = StencilBits;
	pfd.iLayerType = PFD_MAIN_PLANE;
	pfd.iPixelType = PFD_TYPE_RGBA;
	pfd.dwFlags = PFD_DRAW_TO_WINDOW|PFD_SUPPORT_OPENGL;
	if(DoubleBuf)pfd.dwFlags|=PFD_DOUBLEBUFFER;
	pfd.dwFlags|=AddFlags;

	int pixelformat;

    if(!(pixelformat = ChoosePixelFormat(dc, &pfd)))
    {
		_CheckErrorW32;
		ReleaseDC(AssocWnd,dc);
		dc=0;
        return FALSE;
    }

    if(!SetPixelFormat(dc, pixelformat, &pfd))
    {
		_CheckErrorW32;
		ReleaseDC(AssocWnd,dc);
		dc=0;
        return FALSE;
    }

	rc = wglCreateContext(dc);
	if(!rc)
	{
		_CheckErrorW32;
		ReleaseDC(AssocWnd,dc);
		dc=0;
        return FALSE;
	}

	return TRUE;
}

BOOL wglh::CwglRenderContext::Create(HDC hDc)
{
#pragma warning(disable:4312)
	AssocWnd = ((HWND)0xffffffff);
#pragma warning(default:4312)
	dc = hDc;

	rc = wglCreateContext(dc);
	return IsOk();
}


void wglh::CwglRenderContext::Destroy()
{
	ASSERT(dc);
	ASSERT(rc);

	wglDeleteContext(rc);
	rc = NULL;
	if( ((DWORD)AssocWnd) != 0xffffffff )
		ReleaseDC(AssocWnd,dc);
	
	dc = NULL;
}

BOOL wglh::CwglRenderContext::MakeCurrent(BOOL DoCurrent) const
{
	ASSERT(dc);
	ASSERT(rc);

	if(DoCurrent)
        return wglMakeCurrent(dc,rc);
	else
		return wglMakeCurrent(NULL,NULL);
}


///////////////////////////////////////
//CwglTimer
wglh::CwglTimer::CwglTimer()
{
	ZeroMemory(this,sizeof(CwglTimer)); 

	hEvent = CreateEvent(NULL,TRUE,FALSE,NULL);
	if(!hEvent)_CheckErrorW32;
}

wglh::CwglTimer::~CwglTimer()
{
	Stop(); 
	if(hEvent)CloseHandle(hEvent);
}

void wglh::CwglTimer::SetTimer(UINT i,BOOL a,UINT r)
{
	ASSERT(!TimeID);  //Must be stoped
	Interval = i;
	AutoReset = a;
	Resolution = r;
}

BOOL wglh::CwglTimer::Start()
{
	if(hEvent && !TimeID)
	{
		ResetEvent(hEvent);
		TimeID = timeSetEvent(Interval,Resolution,(LPTIMECALLBACK)hEvent,NULL,TIME_PERIODIC|(AutoReset?TIME_CALLBACK_EVENT_PULSE:TIME_CALLBACK_EVENT_SET));
		if(TimeID)
		{
			return TRUE;
		}
		else
		{
			_CheckErrorW32;
			return FALSE;
		}
	}

	return FALSE;
}

void wglh::CwglTimer::Stop()
{
	if(TimeID)
	{
		if(timeKillEvent(TimeID) != TIMERR_NOERROR)_CheckErrorW32;
		TimeID = NULL;
	}
}

void wglh::CwglPrimitive::Init()
{
	ASSERT(m_pQuadricObj==NULL);
	m_pQuadricObj = gluNewQuadric();
	ASSERT(m_pQuadricObj); 

	SetDrawStyle();
	SetNormalMode();
	SetOrientation();
}


namespace wglh
{

void _DumpPixelFormat(HDC hdc)
{
	{
		PIXELFORMATDESCRIPTOR  pfd; 
		if(DescribePixelFormat(hdc,GetPixelFormat(hdc),sizeof(PIXELFORMATDESCRIPTOR), &pfd))
		{
			_CheckDump("\tIndexed Color      : "<<(pfd.iPixelType == PFD_TYPE_COLORINDEX?"Yes":"No"));
			_CheckDump("\n\tColor Buffer       : "<<((int)pfd.cColorBits)<<"Bits ("<<(int)pfd.cRedBits<<'.'<<(int)pfd.cGreenBits<<'.'<<(int)pfd.cBlueBits<<'.'<<(int)pfd.cAlphaBits);
			_CheckDump(")\n\tAccumulation Buffer: "<<(int)pfd.cAccumBits<<"Bits ("<<(int)pfd.cAccumRedBits<<'.'<<(int)pfd.cAccumGreenBits<<'.'<<(int)pfd.cAccumBlueBits<<'.'<<(int)pfd.cAccumAlphaBits);
			_CheckDump(")\n\tDepth Buffer       : "<<(int)pfd.cDepthBits);
			_CheckDump("Bits\n\tStencil Buffer     : "<<(int)pfd.cStencilBits);
			_CheckDump("Bits\n\tAux Buffers        : "<<(int)pfd.cAuxBuffers);
			_CheckDump(    "\n\tDual Frame Buffer  : "<<(LPCSTR)((pfd.dwFlags & PFD_DOUBLEBUFFER)?"Yes":"False"));
			_CheckDump(    "\n\tStereo Frame Buffer: "<<(LPCSTR)((pfd.dwFlags & PFD_STEREO)?"Yes":"False"));
			_CheckDump(    "\n\tH/W Acceleration   : "<<(LPCSTR)((pfd.dwFlags & PFD_GENERIC_ACCELERATED)?"Yes":"False"));
			_CheckDump('\n');
		}
		else
		{
			_CheckDump("Failed for "<<ERRMSG_LASTERR()<<'\n');
		}
	}

}


static LPCTSTR TextTail = _T(" Information\n");
static const LPCTSTR TextBar= _T(" Information\n==========================\n");

void _DumpOGLExtensionInfo()
{
	_CheckDump(_T("OpenGL Extensions")<<TextBar);

	LPCSTR pExtString = (LPCSTR)glGetString(GL_EXTENSIONS);
	if(!pExtString)
	{
		_CheckDump(_T("Failed to get OpenGL extension string\n"));
		return;
	}

	///Dump Specific extensions
	if(strstr(pExtString,"GL_ARB_multitexture"))
	{
		_CheckDump(_T("GL_ARB_multitexture")<<TextTail);
		_CheckDump(_T("\tTexture Stage Count: ")<<mt::MaxTextureStage()<<_T("\n\n"));
	}

	if(strstr(pExtString,"GL_EXT_texture_filter_anisotropic"))
	{
		_CheckDump(_T("GL_EXT_texture_filter_anisotropic")<<TextTail);
		{	GLfloat max = -1.0f;
			glGetFloatv(GL_MAX_TEXTURE_MAX_ANISOTROPY_EXT,&max);
			if(max<0)
			{
				_CheckDump(_T("\tFailed to query MAX_TEXTURE_MAX_ANISOTROPY_EXT"));
			}
			else
			{
				_CheckDump(_T("\tMax Texture Max Anisotropy: ")<<max);
			}
		}
		_CheckDump(_T("\n\n"));
	}

	if(strstr(pExtString,"GL_EXT_texture3D"))
	{
		_CheckDump(_T("GL_EXT_texture3D")<<TextTail);
		{
			GLint sz = 0;
			
			glGetIntegerv(GL_MAX_3D_TEXTURE_SIZE_EXT,&sz);
			if(sz)
			{	_CheckDump(_T("\tMax 3D Texture Size: ")<<sz); }
			else
			{	_CheckDump(_T("\tFailed to query MAX_3D_TEXTURE_SIZE_EXT")); }
		}
		_CheckDump(_T("\n\n"));
	}

	if(strstr(pExtString,"GL_ARB_texture_cube_map"))
	{
		_CheckDump(_T("GL_ARB_texture_cube_map")<<TextTail);
		{
			_CheckDump(_T("\tMax cubemap face size: ")<<ext::CwglCubemapBase::GetMaxFaceSize());
		}
		_CheckDump(_T("\n\n"));
	}

	if(strstr(pExtString,"GL_ATI_draw_buffers"))
	{
		_CheckDump(_T("GL_ATI_draw_buffers")<<TextTail);
		{
			_CheckDump(_T("\tDraw buffer count: ")<<ex::GetDrawBufferCount());
		}
		_CheckDump(_T("\n\n"));
	}

#ifdef INCLUDE_WGLH_SHADER
	if(strstr(pExtString,"GL_ARB_vertex_program"))
	{
		_CheckDump(_T("GL_ARB_vertex_program")<<TextTail);
		if(ext::ARBvpfp::CwglGpuProgram::LoadExtensionEntryPoints())
		{
			_CheckDump(_T("\tMax Environment Parameters: ")<<ext::ARBvpfp::CwglGpuProgram::MaxEnvParam(GL_VERTEX_PROGRAM_ARB));
			_CheckDump(_T("\n\tMax Local Parameters      : ")<<ext::ARBvpfp::CwglGpuProgram::MaxLocalParam(GL_VERTEX_PROGRAM_ARB));
			_CheckDump(_T("\n\tMax Parameter Bindings    : ")<<ext::ARBvpfp::CwglGpuProgram::MaxParamBinding(GL_VERTEX_PROGRAM_ARB));
			_CheckDump(_T("\n\tMax Attribute Bindings    : ")<<ext::ARBvpfp::CwglGpuProgram::MaxAttribBinding(GL_VERTEX_PROGRAM_ARB));
			_CheckDump(_T("\n\tMax Temp Variable         : ")<<ext::ARBvpfp::CwglGpuProgram::MaxTempVariable(GL_VERTEX_PROGRAM_ARB));
			_CheckDump(_T("\n\tMax Instructions          : ")<<ext::ARBvpfp::CwglGpuProgram::MaxInstructions(GL_VERTEX_PROGRAM_ARB));
			_CheckDump(_T("\n\n"));
		}
		else
		{
			_CheckDump(_T("Failed in loading extension entrypoints\n\n"));
		}
	}

	if(strstr(pExtString,"GL_ARB_fragment_program"))
	{
		_CheckDump(_T("GL_ARB_fragment_program")<<TextTail);
		if(ext::ARBvpfp::CwglGpuProgram::LoadExtensionEntryPoints())
		{
			_CheckDump(_T("\tMax Environment Parameters: ")<<ext::ARBvpfp::CwglGpuProgram::MaxEnvParam(GL_FRAGMENT_PROGRAM_ARB));
			_CheckDump(_T("\n\tMax Local Parameters      : ")<<ext::ARBvpfp::CwglGpuProgram::MaxLocalParam(GL_FRAGMENT_PROGRAM_ARB));
			_CheckDump(_T("\n\tMax Parameter Bindings    : ")<<ext::ARBvpfp::CwglGpuProgram::MaxParamBinding(GL_FRAGMENT_PROGRAM_ARB));
			_CheckDump(_T("\n\tMax Attribute Bindings    : ")<<ext::ARBvpfp::CwglGpuProgram::MaxAttribBinding(GL_FRAGMENT_PROGRAM_ARB));
			_CheckDump(_T("\n\tMax Temp Variable         : ")<<ext::ARBvpfp::CwglGpuProgram::MaxTempVariable(GL_FRAGMENT_PROGRAM_ARB));
			_CheckDump(_T("\n\tMax Instructions          : ")<<ext::ARBvpfp::CwglGpuProgram::MaxInstructions(GL_FRAGMENT_PROGRAM_ARB));
			GLint	TexUnit=0,TexCoord=0;
			glGetIntegerv(GL_MAX_TEXTURE_COORDS_ARB,&TexCoord);
			glGetIntegerv(GL_MAX_TEXTURE_IMAGE_UNITS_ARB,&TexUnit);
			_CheckDump(_T("\n\tMax Texture Coord         : ")<<TexCoord);
			_CheckDump(_T("\n\tMax Texture Unit          : ")<<TexUnit);

			_CheckDump(_T("\n\tNative Limitations:"));
			_CheckDump(_T("\n\t\tTotal Instructions: ")<<ext::ARBvpfp::CwglGpuProgram::MaxNativeInstructions(GL_FRAGMENT_PROGRAM_ARB));
			_CheckDump(_T("\n\t\tALU Instructions  : ")<<ext::ARBvpfp::CwglGpuProgram::MaxNativeAluInstructions(GL_FRAGMENT_PROGRAM_ARB));
			_CheckDump(_T("\n\t\tTEX Instructions  : ")<<ext::ARBvpfp::CwglGpuProgram::MaxNativeTexInstructions(GL_FRAGMENT_PROGRAM_ARB));
			_CheckDump(_T("\n\t\tTEX Indirections  : ")<<ext::ARBvpfp::CwglGpuProgram::MaxNativeTexIndirections(GL_FRAGMENT_PROGRAM_ARB));
			_CheckDump(_T("\n\t\tATTRIB Bindings   : ")<<ext::ARBvpfp::CwglGpuProgram::MaxNativeAttribBinding(GL_FRAGMENT_PROGRAM_ARB));
			_CheckDump(_T("\n\t\tPARAM Bindings    : ")<<ext::ARBvpfp::CwglGpuProgram::MaxNativeParamBinding(GL_FRAGMENT_PROGRAM_ARB));
			_CheckDump(_T("\n\t\tTEMP Bindings     : ")<<ext::ARBvpfp::CwglGpuProgram::MaxNativeTempVariable(GL_FRAGMENT_PROGRAM_ARB));
			_CheckDump(_T("\n\n"));
		}
		else
		{
			_CheckDump(_T("Failed in loading extension entrypoints\n\n"));
		}
	}

	if(strstr(pExtString,"GL_ARB_shading_language"))
	{
		_CheckDump(_T("OpenGL Shading Language")<<TextTail);
		if(glsl::CwglLanguageObject::LoadExtensionEntryPoints())
		{
			_CheckDump(_T("\tVersion = ")<<glsl::CwglLanguageObject::GetVersion());
			_CheckDump(_T("\n\tHardware Limitation:"));
			_CheckDump(_T("\n\t\tVarying floats        :")<<glsl::CwglLanguageObject::Fragment_MaxVarying());
			_CheckDump(_T("\n\t\tTexture unit          :")<<glsl::CwglLanguageObject::Vertex_MaxTextureUnitCombined());
			_CheckDump(_T("\n\t\tTexture coord         :")<<glsl::CwglLanguageObject::Vertex_MaxTextureCoord());

			_CheckDump(_T("\n\t\tVertex attribute      :")<<glsl::CwglLanguageObject::Vertex_MaxAttrib());
			_CheckDump(_T("\n\t\tVertex texture unit   :")<<glsl::CwglLanguageObject::Vertex_MaxTextureUnit());
			_CheckDump(_T("\n\t\tVertex uniform        :")<<glsl::CwglLanguageObject::Vertex_MaxUniform());

			_CheckDump(_T("\n\t\tFragment texture unit :")<<glsl::CwglLanguageObject::Fragment_MaxTextureUnit());
			_CheckDump(_T("\n\t\tFragment uniform      :")<<glsl::CwglLanguageObject::Fragment_MaxUniform());

			_CheckDump(_T("\n\n"));
		}
	}
#endif // INCLUDE_WGLH_SHADER

	_CheckDump(_T('\n'));
}

void _DumpOGLInfo(HDC dc = NULL)
{
	_CheckDump(_T("OpenGL")<<TextBar);

	{
		LPCSTR pStr;
		_CheckDump(_T("Vendor    : "));
		pStr = (LPCSTR)glGetString(GL_VENDOR);
		if(pStr)_CheckDump(pStr);

		_CheckDump(_T("\nRender    : "));
		pStr = (LPCSTR)glGetString(GL_RENDERER);
		if(pStr)_CheckDump(pStr);

		_CheckDump(_T("\nVersion   : "));
		pStr = (LPCSTR)glGetString(GL_VERSION);
		if(pStr)_CheckDump(pStr);

		_CheckDump(_T("\nExtensions: "));
		pStr = (LPCSTR)glGetString(GL_EXTENSIONS);
		if(pStr)
		{
			char enbuf[MAX_PATH];
			enbuf[0] = ' ';
			enbuf[1] = '\t';
			char * pen = &enbuf[2];

			LPCSTR head = pStr;
			LPCSTR ps=NULL;
			int	len;

			while(ps=strchr(head,' '))
			{
				len = (int)(ps-head);
				ASSERT(len<(MAX_PATH-6));

				memcpy(pen,head,len);

				pen[len] = '\n';
				pen[len+1] = '\0';
				_CheckDump(enbuf);

				head = ps+1;
				enbuf[0] = '\t';
			}

			*pen = '\0';
			_CheckDump(enbuf<<head);
		}      

#ifdef CodeLib_wglh_ext_Included
		if( wglh::ext::CwglExtensions::LoadExtensionEntryPoints() )
		{
			_CheckDump("\nWGL Extensions");
			char enbuf[MAX_PATH];
			enbuf[0] = ':';
			enbuf[1] = ' ';
			char * pen = &enbuf[2];

			LPCSTR head = wglh::ext::CwglExtensions::GetExtensionStringWGL();
			ASSERT(head);

			LPCSTR ps=NULL;
			int	len;

			while(ps=strchr(head,' '))
			{
				len = (int)(ps-head);
				ASSERT(len<(MAX_PATH-6));

				memcpy(pen,head,len);

				pen[len] = '\n';
				pen[len+1] = '\0';
				_CheckDump(enbuf);

				head = ps+1;
				enbuf[0] = '\t';
				enbuf[1] = '\t';
			}

			*pen = '\0';
			_CheckDump(enbuf<<head);
		}
#endif
	}

	_CheckDump("\n\n");
}


void DumpOpenGLInformation(HWND	GL_Wnd)
{
	if(GL_Wnd)
	{	HDC hdc = ::GetDC(GL_Wnd);
		_DumpOGLInfo(hdc);
		::ReleaseDC(GL_Wnd,hdc);
		_DumpOGLExtensionInfo();
	}
	else
	{
		wglh::CwglWnd	TestWnd;

		if(TestWnd.Create())
		{
			if(TestWnd.InitGL())
			{
				HDC dc = ::GetDC(TestWnd);
				_DumpOGLInfo(dc);
				::ReleaseDC(GL_Wnd,dc);
				_DumpOGLExtensionInfo();
				return;
			}
			else
			{
				_CheckDumpSrcLoc;
			}
		}
		else
		{
			_CheckDumpSrcLoc;
		}

		_CheckDump("Failed to create OpenGL test window.\n");
	}
}


BOOL wglCheckError()
{
	if(wglGetCurrentContext()){} // no RC in current thread
	else return FALSE;

	int ec = glGetError();
	if(ec == GL_NO_ERROR)return FALSE;

	_CheckDump("GL error occurred:");

	BOOL	InvalidOp = FALSE;
	BOOL	GetNextError = TRUE;
wglCheckError_NextError:
	{
		switch(ec)
		{
		case GL_INVALID_ENUM:
			_CheckDump(" GL_INVALID_ENUM");
			break;
		case GL_INVALID_VALUE:
			_CheckDump(" GL_INVALID_VALUE");
			break;
		case GL_INVALID_OPERATION:
			if(InvalidOp)
				GetNextError = FALSE;
			else
			{
				InvalidOp = TRUE;
				_CheckDump(" GL_INVALID_OPERATION");
			}
			break;
		case GL_STACK_OVERFLOW:
			_CheckDump(" GL_STACK_OVERFLOW");
			break;
		case GL_STACK_UNDERFLOW:
			_CheckDump(" GL_STACK_UNDERFLOW");
			break;
		case GL_OUT_OF_MEMORY:
			_CheckDump(" GL_OUT_OF_MEMORY");
			break;
		default:
			_CheckDump(" #"<<(int)ec);
			break;
		}
	}

#ifdef _DEBUG
	CwglApp::GetApp()->m_bOpenGLErrorFired = TRUE;
	ASSERT(0);
	CwglApp::GetApp()->m_bOpenGLErrorFired = FALSE;
#endif
	
	if(GetNextError&&((ec = glGetError())!=GL_NO_ERROR))
		goto wglCheckError_NextError;

	_CheckDump('\n');
	return TRUE;
}


};
