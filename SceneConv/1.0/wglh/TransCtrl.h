#pragma once

#include "1.0/num/small_vec.h"
#include "1.0/num/arcball.h"
#include "1.0/num/small_mat.h"

namespace wglh{

class CMoveCtrl
{
public:
	CMoveCtrl(){
		m_bDragging = false;
		m_draggingType = -1;

		m_trans = num::Vec3f(0,0,0);
	}
	~CMoveCtrl(){}

	num::Vec3f& GetTranslation_Ref() 
	{ return (m_trans);}
	const num::Vec3f& GetTranslation_Ref() const
	{ return (m_trans);}


	//must be call as inside the matrix of the controlling object, before dragging
	void prepareDrag();	

	void DrawCtrl();

	void MouseDown(int x,int y);
	void MouseMove(int x,int y);
	void MouseUp(int x,int y);

	void SetIdentity() {
		GetTranslation_Ref() = num::Vec3f(0,0,0);
	}

protected:	
	void Update(int x,int y);
	int getDragType(int x,int y) const;
	
protected:
	num::Vec3f     m_trans;
	bool            m_bDragging;
	bool			m_bDragPrepared;

	// 0(X-axis),1(Y-axis),2(Z-axis)
	// 3(YZ-plane),4(XZ-plane), 5(XY-plane)
	int			    m_draggingType;

	num::Vec2i      m_dragStartScreenPos;
	num::Vec2f      m_dragStartGradient[3];
	num::Vec3f		m_dragStart3DPos;

	wglh::CwglTransformation	m_VisualSpaceView;

	num::Vec2i     m_mousePos;
};

class CRotationCtrl{

public:
	CRotationCtrl(){
		m_bDragging = false;
		m_fScale = 1.0f;
	}

	//must be call as inside the matrix of the controlling object, before rotation
	void prepareDrag();

    num::Mat4x4f    GetRotationMatrix(const num::Mat4x4f& viewRotMat) const{ 

		num::Mat4x4f inv_mat = viewRotMat;
		inv_mat.Transpose();
		num::Mat4x4f res;
		res.Product(inv_mat,m_rotArcball.GetMatrix());
		return res;
	}

    void   SetRotationMat(const num::Mat4x4f& viewRotMat,const num::Mat4x4f& mat)
    { 
		num::Mat4x4f res;
		res.Product(viewRotMat,mat);
		m_rotArcball.SetMatrix(res);
    }

	float GetScale() const {
		return m_fScale;
	}
	void SetScale(float fScale) {
		m_fScale = fScale;
	}


	void MouseDown(int x,int y);
	void MouseMove(int x,int y);
	void MouseUp(int x,int y);
	void MouseWheel(float f);	//Scale Change Radius 

protected:
	void update(int x,int y);

	bool m_bDragging;
	wglh::CwglTransformation	m_VisualSpaceView;

	float		m_fScale;
	num::ArcBallf m_rotArcball;	
	
};



/*


Usage of this class: 

1. prepareDrag() must be called before rendering the object inside the render function, 
	this method is used to query for the modelview and projection matrices of the object;
   
   if and only if this function is correctly called, the dragging operation would be correct;

2. ApplyFull(const num::Mat4x4f& viewRotMat) is called before drawing the object.
   this function is used to apply the transformation of the control;

3. MouseDown,MouseMove,MouseUp,MouseWheel are called for corresponding mouse events.
   NOTE THAT: 
   for the rotation control, 
   MouseDown,MouseMove,MouseUp,MouseWheel must also be called when the viewing matrix is being operated.

4. DrawCtrl 
   called at the end of the render function, must be set the same transformation matrix as drawing the object;

e.g. 
void OnRender(){
	
	//applying matrix
	glPushMatrix();
	ctrl.prepareDrag();
	ctrl.ApplyFull(GetRotationToScene());

	draw object ...;

	glPopMatrix();


	... other staffs;

	glPushMatrix();
	ctrl.ApplyTranslate();

	ctr.DrawCtrl();

	glPopMatrix();
	
}


*/


class CTransformCtrl
    {
    protected:
        CMoveCtrl  moveCtrl;
        CRotationCtrl rotationCtrl;
	public:
	enum { ENUM_MOVE = 0,ENUM_ROTATE = 1};

	void prepareDrag(){
		moveCtrl.prepareDrag();
		rotationCtrl.prepareDrag();
	}

	float GetScale() const { return rotationCtrl.GetScale();}
	void SetScale(float fScale) { rotationCtrl.SetScale(fScale);}
    num::Mat4x4f    GetRotationMatrix(const num::Mat4x4f& viewRotMat) const{ 
		return rotationCtrl.GetRotationMatrix(viewRotMat);
	}
    void   SetRotationMat(const num::Mat4x4f& viewRotMat,const num::Mat4x4f& mat){ 
		rotationCtrl.SetRotationMat(viewRotMat,mat);
    }
	num::Vec3f& GetTranslation_Ref() 
	{ return moveCtrl.GetTranslation_Ref();}
	const num::Vec3f& GetTranslation_Ref() const
	{ return moveCtrl.GetTranslation_Ref();}

	void DrawCtrl(){
		moveCtrl.DrawCtrl();
	}

    void ApplyFull(const num::Mat4x4f& viewRotMat) const
    {
        const num::Vec3f& vVec = GetTranslation_Ref();
        float fScale = GetScale();
        glTranslatef(vVec.x,vVec.y,vVec.z);
        glScalef(fScale,fScale,fScale);
        glMultMatrixf(GetRotationMatrix(viewRotMat));
    }
	void ApplyTranslate() const {
		const num::Vec3f& vVec = GetTranslation_Ref();
		glTranslatef(vVec.x,vVec.y,vVec.z);
	}

	void MouseDown(int x,int y,int type){
		if(type==ENUM_MOVE)
			moveCtrl.MouseDown(x,y);
		else if(type==ENUM_ROTATE){
			rotationCtrl.MouseDown(x,y);
		}
	}
	void MouseMove(int x,int y,int type){
		if(type==ENUM_MOVE)
			moveCtrl.MouseMove(x,y);
		else if(type==ENUM_ROTATE){
			rotationCtrl.MouseMove(x,y);
		}
	}
	void MouseUp(int x,int y,int type){
		if(type==ENUM_MOVE)
			moveCtrl.MouseUp(x,y);
		else if(type==ENUM_ROTATE){
			rotationCtrl.MouseUp(x,y);
		}
	}

	void MouseWheel(float f) {
		rotationCtrl.MouseWheel(f);
	}

	};

}