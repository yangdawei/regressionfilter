#include "StdAfx.h"
#include "ArcballWndEx.h"
#include "wglshader.h"

#include <boost/function.hpp>
#include <boost/bind.hpp>

namespace wglh {

	CArcballWndEx::CArcballWndEx() {
		m_sprites = NULL;
	}
	CArcballWndEx::~CArcballWndEx(void){
		_SafeDel(m_sprites);
	}

	void CArcballWndEx::OnRender(){

		preRender();

		glClearColor(0.2f, 0.4f, 0.6f, 1.0f);
		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

		//render environment
		{
			OnRenderEnvironment();
		}

		glEnable(GL_DEPTH_TEST);

		{
			glMatrixMode(GL_MODELVIEW);
			glLoadIdentity();
			glTranslatef(0,0,-m_SceneZoom);
			//glScalef(m_SceneZoom,m_SceneZoom,m_SceneZoom);
			m_SceneArcBall.Apply();

			OnRenderObject();
		}


		glFinish();

		//draw sprites
		if(m_sprites){
			m_sprites->draw();
		}

		SwapBuffers(wglGetCurrentDC());

	}

	LRESULT CArcballWndEx::WndProc(UINT message, WPARAM wParam, LPARAM lParam){

	switch(message){
		case WM_LBUTTONDOWN:
		case WM_LBUTTONUP:
		case WM_MOUSEMOVE:
			m_MouseEnd.x = GET_X_LPARAM(lParam);
			m_MouseEnd.y = GET_Y_LPARAM(lParam);
			m_MouseEnd.x = std::min(width-1,std::max(0,m_MouseEnd.x));
			m_MouseEnd.y = std::min(height-1,std::max(0,m_MouseEnd.y));

#ifndef USE_SPOT_LIGHT
			if((message == WM_LBUTTONDOWN && m_sprites->MouseDown(m_MouseEnd.x,height - m_MouseEnd.y))
				|| (message == WM_LBUTTONUP && m_sprites->MouseUp(m_MouseEnd.x,height - m_MouseEnd.y))
				|| (message == WM_MOUSEMOVE && m_sprites->MouseMove(m_MouseEnd.x,height - m_MouseEnd.y)) ){
				OnTransformChanged();
				return 0;
			}
#endif
	}

	const float fine = (wParam & MK_CONTROL)?0.1f:1.0f;
	if(m_InteractiveMode==0)
	{
		switch(message)
		{		
		case WM_MOUSEWHEEL:			
			 m_transformCtrl.MouseWheel(fine*(((short)HIWORD(wParam))/1500.0f));
			 OnTransformChanged();
			break;
		case WM_LBUTTONDOWN:
		case WM_RBUTTONDOWN:
			m_MouseEnd.x = GET_X_LPARAM(lParam);
			m_MouseEnd.y = GET_Y_LPARAM(lParam);
			m_MouseEnd.x = std::min(width-1,std::max(0,m_MouseEnd.x));
			m_MouseEnd.y = std::min(height-1,std::max(0,m_MouseEnd.y));

			if(message == WM_LBUTTONDOWN)
				m_transformCtrl.MouseDown(m_MouseEnd.x,m_MouseEnd.y,wglh::CTransformCtrl::ENUM_MOVE);
			else if (message == WM_RBUTTONDOWN)
				m_transformCtrl.MouseDown(m_MouseEnd.x,m_MouseEnd.y,wglh::CTransformCtrl::ENUM_ROTATE);

			wglh::CwglArcballWnd::SetCapture();
			OnTransformChanged();
			break;
		case WM_MOUSEMOVE:
			m_MouseEnd.x = GET_X_LPARAM(lParam);
			m_MouseEnd.y = GET_Y_LPARAM(lParam);
			m_MouseEnd.x = std::min(width-1,std::max(0,m_MouseEnd.x));
			m_MouseEnd.y = std::min(height-1,std::max(0,m_MouseEnd.y));
			if(wParam&MK_LBUTTON)
				m_transformCtrl.MouseMove(m_MouseEnd.x,m_MouseEnd.y,wglh::CTransformCtrl::ENUM_MOVE);
			else if(wParam&MK_RBUTTON)
				m_transformCtrl.MouseMove(m_MouseEnd.x,m_MouseEnd.y,wglh::CTransformCtrl::ENUM_ROTATE);
			OnTransformChanged();
			break;
		case WM_LBUTTONUP:
		case WM_RBUTTONUP:
			m_MouseEnd.x = GET_X_LPARAM(lParam);
			m_MouseEnd.y = GET_Y_LPARAM(lParam);
			m_MouseEnd.x = std::min(width-1,std::max(0,m_MouseEnd.x));
			m_MouseEnd.y = std::min(height-1,std::max(0,m_MouseEnd.y));
			if(message == WM_LBUTTONUP)
				m_transformCtrl.MouseUp(m_MouseEnd.x,m_MouseEnd.y,wglh::CTransformCtrl::ENUM_MOVE);
			else if(message == WM_RBUTTONUP)
				m_transformCtrl.MouseUp(m_MouseEnd.x,m_MouseEnd.y,wglh::CTransformCtrl::ENUM_ROTATE);
			wglh::CwglArcballWnd::ReleaseCapture();
			OnTransformChanged();
			break;
		}
	} 
#ifdef USE_SPOT_LIGHT
	else if (m_InteractiveMode == IMC_ENVIROMENT)
	{
		switch(message)
		{		
		case WM_MOUSEWHEEL:			
			//Not supported
			break;
		case WM_LBUTTONDOWN:
			m_MouseEnd.x = GET_X_LPARAM(lParam);
			m_MouseEnd.y = GET_Y_LPARAM(lParam);
			m_MouseEnd.x = std::min(width-1, std::max(0,m_MouseEnd.x));
			m_MouseEnd.y = std::min(height-1, std::max(0,m_MouseEnd.y));

			m_lightCtrl.MouseDown(m_MouseEnd.x, m_MouseEnd.y);

			wglh::CwglArcballWnd::SetCapture();
			OnTransformChanged();
			break;
		case WM_MOUSEMOVE:
			m_MouseEnd.x = GET_X_LPARAM(lParam);
			m_MouseEnd.y = GET_Y_LPARAM(lParam);
			m_MouseEnd.x = std::min(width-1, std::max(0,m_MouseEnd.x));
			m_MouseEnd.y = std::min(height-1, std::max(0,m_MouseEnd.y));
			m_lightCtrl.MouseMove(m_MouseEnd.x, m_MouseEnd.y);
			OnTransformChanged();
			break;
		case WM_LBUTTONUP:
			m_MouseEnd.x = GET_X_LPARAM(lParam);
			m_MouseEnd.y = GET_Y_LPARAM(lParam);
			m_MouseEnd.x = std::min(width-1,std::max(0,m_MouseEnd.x));
			m_MouseEnd.y = std::min(height-1,std::max(0,m_MouseEnd.y));
			m_lightCtrl.MouseUp(m_MouseEnd.x,m_MouseEnd.y);
			wglh::CwglArcballWnd::ReleaseCapture();
			OnTransformChanged();
			break;
		}
	}
#endif
	else if(m_InteractiveMode){
		switch(message)
		{
		case WM_LBUTTONDOWN:
			if(IMC_SCENE == m_InteractiveMode){	
				m_transformCtrl.MouseDown((short)LOWORD(lParam), (short)HIWORD(lParam),wglh::CTransformCtrl::ENUM_ROTATE);
			}
			break;
		case WM_LBUTTONUP:
			m_transformCtrl.MouseUp((short)LOWORD(lParam), (short)HIWORD(lParam),wglh::CTransformCtrl::ENUM_ROTATE);
			break;
		case WM_MOUSEMOVE:
			{
				if(wParam&MK_LBUTTON)
				{
					if(IMC_SCENE == m_InteractiveMode)
					{
						m_transformCtrl.MouseMove((short)LOWORD(lParam), (short)HIWORD(lParam),wglh::CTransformCtrl::ENUM_ROTATE);
					}	
				}
			}
			break;
		}
	}

	switch(message)
	{
	case WM_KEYDOWN:
		if(handleKeyboard(wParam)){ return true;}
		break;
	}

		return __super::WndProc(message,wParam,lParam);
	}

	void CArcballWndEx::OnInitScene(){

		{
			m_SceneZoom = 5.0f;
		}
		{
			m_cubeMapEnv.Load_PFM("grace.pfm");
			m_cubeMapEnv.InitForRendering();
		}

		{
			_SafeDel(m_sprites);
			using namespace jgl;
			JSpriteContainer::setResourcePath(".\\raw\\");
			m_sprites = new JSpriteContainer(10,10,250,80,true);
			m_sprites->addSprite(new JGLLabel(10,50,"Panel"));
			JGLSeekBar* seek1 = new JGLSeekBar(&m_SceneZoom,2,50,80,10,"Distance");
			seek1->addListener(boost::bind(&CArcballWndEx::updateSceneScale,this,_1));
			m_sprites->addSprite(seek1);
		}
		
	}
	void CArcballWndEx::OnUninitScene(){
		_SafeDel(m_sprites);
		m_cubeMapEnv.DestroyForRendering();
	}

	void CArcballWndEx::preRender(){
	}
	void CArcballWndEx::postRender(){

	}
	void CArcballWndEx::OnRenderEnvironment(){
		
		glMatrixMode(GL_MODELVIEW);
		glPushMatrix();
		glLoadIdentity();
		RotateScene_AsEnviroment();
		m_cubeMapEnv.RenderEnvironment();
		glPopMatrix();
	}

	void CArcballWndEx::OnRenderObject(){

		glPushMatrix();

		m_transformCtrl.prepareDrag();
		m_transformCtrl.ApplyFull(GetRotationToScene());
		DrawUnitBox();

		glPopMatrix();

		if(m_InteractiveMode==0){
			glPushMatrix();
				m_transformCtrl.ApplyTranslate();
				m_transformCtrl.DrawCtrl();
			glPopMatrix();
		}
#ifdef USE_SPOT_LIGHT //We need to render the spotlight too
		else if (m_InteractiveMode == IMC_SCENE)
		{
			glPushMatrix();
			const num::Vec3f trans = m_lightCtrl.GetTranslation_Ref();
			glTranslatef(trans.x, trans.y, trans.z);
			m_lightCtrl.DrawCtrl();
			glPopMatrix();
		}
#endif
	}

	void CArcballWndEx::updateSceneScale(float scale){
		m_SceneZoom = scale;
		OnTransformChanged();
	}

	bool CArcballWndEx::handleKeyboard(unsigned char c){
		return false;
	}
}