#pragma once

#include "stdafx.h"

namespace wglh{
/**
 * The simple light source implementing.
 * @author Dawei Yang
 * @since 2013-9-24
 */
class CwglSpotLight
{
public:
	/** Translation. */
	num::Vec3f translation;
	num::Vec3f color;

	void render();
};
}
