#pragma once

//////////////////////////////////////////////////////////////////////
// Visual Computing Foundation Classes (VCFC)
//					Jiaping Wang  2006.2
//					e_boris2002@hotmail.com
//
// (C) Copyright Jiaping Wang 2004.
//
// Permission to copy, use, modify, sell and distribute this software
// is granted provided this copyright notice appears in all copies.
// This software is provided "as is" without express or implied
// warranty, and with no claim as to its suitability for any purpose.
//
// Absolutely no warranty!!
//////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////
//  Brief Description
//
//  wglArcBall.h
//
//  ArcBall 3D interactive window
//
//////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////
// Revise History
//
// Inital version	2004.?.?		Jiaping
//
//////////////////////////////////////////////////////////////////////

#include "wglhelper.h"
#include "..\num\small_mat.h"
#include "..\num\arcball.h"
#include "..\rt\string_type.h"



namespace wglh
{

///////////////////////////////////////////////
// Interactive Mode
//
// [ Arrow Cursor ]  Nothing is enabled
// [ Cross Cursor ]  Wheel: Zoom
//					 Mouse LButton: Rotate scene
//					 Mouse RButton: Move scene
// [ UpArrow Cursor] Wheel: Zoom
//					 Mouse LButton: Rotate light
//					 Mouse RButton: Move scene

class CwglArcballWnd:public CwglWnd
{
private:
	rt::String		_WindowText;
	LPTSTR			_pFPSText;

protected:
	enum _tagInteractiveMode
	{
		IMC_NONE  = 0,
		IMC_SCENE,
		IMC_ENVIROMENT,
		IMC_MAX
	};

	DWORD			m_InteractiveMode;
	HCURSOR			m_hCursor[IMC_MAX];

protected:
	class _glArcBall : public num::ArcBallf
	{public:
		WGLH_FUNC void Apply() const{ glMultMatrixf(m_Rotation); }
		__forceinline const num::Quaternion<float>& operator = (const num::Quaternion<float>& x)
		{ return __super:: operator = (x); }
	};

protected:
	BOOL	OnInitWnd();
	LRESULT WndProc(UINT message, WPARAM wParam, LPARAM lParam);
    void	OnViewportChanged(int w, int h);
	virtual void OnRender();
	void	_UpdateCaption();

	//Callback
    virtual void OnTransformChanged();

	/////////////////////////////////
	// Realtime
	BOOL	m_bIsRealtime;

	/////////////////////////////////
	// Transform coefficient
	float	m_SceneZoom;
	float	m_SceneTranslationX, m_SceneTranslationY;

	/////////////////////////////////
	// Pre-transform
	//num::Mat4x4f		m_ToSceneTransform;
	num::Mat4x4f		m_ToEnvRotation;

protected:
	_glArcBall			m_SceneArcBall;
	_glArcBall			m_SceneArcBall_Env;

public:
	CwglArcballWnd(BOOL IsRealtimeRendering = FALSE);   //if realtime the OnRender is driver by CwglRealtimeApp's timer otherwise the caller
	DWORD	GetInteractiveMode() const;
	void	SetInteractiveMode(DWORD mode = IMC_SCENE);
	void	TransformScene(BOOL No_Rotate = FALSE) const;
	void	RotateScene() const;
	void	RotateScene_AsEnviroment() const;
	BOOL	SetWindowText(LPCTSTR str);

	const num::Mat4x4f& GetRotationToScene() const{ return m_SceneArcBall.GetMatrix(); }
	const num::Mat4x4f& GetRotationToScene_AsEnviroment() const{ return m_SceneArcBall_Env.GetMatrix(); }
	const num::Mat4x4f& GetRotationToEnviroment() const{ return m_ToEnvRotation; }	// Rotate GetRotationToScene^{-1} \cdot GetRotationToScene_AsEnviroment
	
	//const num::Mat4x4f& GetMatrix_TransformToScene() const{ return m_ToSceneTransform; }
};


}



