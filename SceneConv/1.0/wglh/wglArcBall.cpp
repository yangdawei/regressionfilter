// ArcBall.cpp: implementation of the _glArcBall class.
//
//////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "wglArcBall.h"
#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <string.h>

#include "inc\gl.h"
#include "inc\glu.h"

namespace wglh
{

CwglArcballWnd::CwglArcballWnd(BOOL IsRealtimeRendering)
	:m_bIsRealtime(IsRealtimeRendering)
{
	m_InteractiveMode = IMC_SCENE;

	_WindowText.SetLength(32);
	_pFPSText = _WindowText.Begin();

	m_SceneZoom = 1.0f;
	m_SceneTranslationX = 0.0f;
	m_SceneTranslationY = 0.0f;

	//m_ToSceneTransform.SetIdentity();
	m_ToEnvRotation.SetIdentity();
}

BOOL CwglArcballWnd::OnInitWnd()
{
	if(__super::OnInitWnd())
	{
		m_hCursor[0] = ::LoadCursor(NULL,IDC_ARROW);
		m_hCursor[1] = ::LoadCursor(NULL,IDC_SIZEALL);
		m_hCursor[2] = ::LoadCursor(NULL,IDC_UPARROW);

		SetTimer(*this,0x10000,250,NULL);

		return TRUE;
	}
	else
		return FALSE;
}

void CwglArcballWnd::OnTransformChanged()
{
	if(!m_bIsRealtime)
	{
		Apply();
		Render();
	}
}

void CwglArcballWnd::_UpdateCaption()
{
	if(wglh::GetApp()->IsRealtimeActived())
	{
		if(IsVerticalSynchronized())
			_stprintf(_pFPSText,_T("  [ Realtime/VSync - %5.2ffps ]"),(float)m_fRenderFPS);
		else
			_stprintf(_pFPSText,_T("  [ Realtime - %5.2ffps ]"),(float)m_fRenderFPS);
	}
	else{
		LPCTSTR texts[3] = {_T("None"),_T("View"),_T("Light")};

		_stprintf(_pFPSText,_T("  [ Interactive Mode: %s]"),texts[m_InteractiveMode]);
	}

	__super::SetWindowText(_WindowText);
}

BOOL CwglArcballWnd::SetWindowText(LPCTSTR str)
{
	UINT len = (UINT)_tcslen(str);
	_WindowText.SetLength(len + 32);
	_tcscpy(_WindowText.GetBuffer(),str);
	_pFPSText = &_WindowText[len];

	_UpdateCaption();

	return TRUE;
}

LRESULT CwglArcballWnd::WndProc(UINT message, WPARAM wParam, LPARAM lParam)
{
	static int CapMousex,CapMousey;
	static float OrgX,OrgY;

	if(WM_KEYDOWN == message)
	{
		switch(wParam)
		{
		case VK_SPACE:	SwitchFullScreen(); break;
		case VK_ESCAPE:	if(IsInFullScreen())SwitchFullScreen(); break;
		case VK_RETURN:	
			SetInteractiveMode((m_InteractiveMode+1)%IMC_MAX); 
			OnTransformChanged();
			break;
		case VK_TAB:
			{
				if(wglh::GetApp()->IsIdleDrivenRendering())
				{
					if(wglh::GetApp()->IsRealtimeActived())
					{
						if(IsVerticalSynchronized())
							SetVerticalSynchronization(FALSE);
						else
							wglh::GetApp()->ActiveRealtimeMode(FALSE);
					}
					else
					{
						SetVerticalSynchronization(TRUE);
						wglh::GetApp()->ActiveRealtimeMode();
					}
				}
				break;
			}
		}
	}
	else if(WM_TIMER == message)
	{
		if(0x10000 == wParam)
			_UpdateCaption();
	}
	else if(m_InteractiveMode)
	{
		float fine = (wParam & MK_CONTROL)?0.1f:1.0f;

		switch(message)
		{
		case WM_MOUSEWHEEL:
			m_SceneZoom += fine*(((short)HIWORD(wParam))/1500.0f);
			m_SceneZoom = max(EPSILON,m_SceneZoom);
			OnTransformChanged();
			break;
		case WM_LBUTTONDOWN:
			::SetCursor(m_hCursor[m_InteractiveMode]);
			SetCapture();
			if(IMC_SCENE == m_InteractiveMode)
			{	m_SceneArcBall.OnMouseDown((short)LOWORD(lParam), (short)HIWORD(lParam));
				m_SceneArcBall_Env.OnMouseDown((short)LOWORD(lParam), (short)HIWORD(lParam));
			}
			else if(IMC_ENVIROMENT == m_InteractiveMode)
			{	m_SceneArcBall_Env.OnMouseDown((short)LOWORD(lParam), (short)HIWORD(lParam));
			}
			break;
		case WM_LBUTTONUP:
			m_SceneArcBall.OnMouseUp((short)LOWORD(lParam), (short)HIWORD(lParam));
			m_SceneArcBall_Env.OnMouseUp((short)LOWORD(lParam), (short)HIWORD(lParam));
			ReleaseCapture();
			break;
		case WM_RBUTTONDOWN:
			::SetCursor(m_hCursor[m_InteractiveMode]);
			SetCapture();
			CapMousex = LOWORD(lParam);
			CapMousey = HIWORD(lParam);
			OrgX = m_SceneTranslationX;
			OrgY = m_SceneTranslationY;
			break;
		case WM_RBUTTONUP:
			ReleaseCapture();
			break;
		case WM_MOUSEMOVE:
			{
				::SetCursor(m_hCursor[m_InteractiveMode]);

				if(wParam&MK_LBUTTON)
				{
					if(IMC_SCENE == m_InteractiveMode)
					{	m_SceneArcBall.OnMouseMove((short)LOWORD(lParam), (short)HIWORD(lParam));
						m_SceneArcBall_Env.OnMouseMove((short)LOWORD(lParam), (short)HIWORD(lParam));
						OnTransformChanged();
					}
					else if(IMC_ENVIROMENT == m_InteractiveMode)
					{	m_SceneArcBall_Env.OnMouseMove((short)LOWORD(lParam), (short)HIWORD(lParam));
						// Relative Env direction relative to Scene changed
						num::Mat4x4f inv_mat;
						m_SceneArcBall.GetInverseMatrix4x4(inv_mat);
						m_ToEnvRotation.Product(inv_mat,	GetRotationToScene_AsEnviroment());
						OnTransformChanged();
					}
				}
				else if(wParam&MK_RBUTTON)
				{
					m_SceneTranslationX = OrgX+fine*((short)LOWORD(lParam) - CapMousex)/100.0f;
					m_SceneTranslationY = OrgY-fine*((short)HIWORD(lParam) - CapMousey)/100.0f;
					OnTransformChanged();
				}
			}
			break;
		}
	}

	return __super::WndProc(message,wParam,lParam);
}

void CwglArcballWnd::SetInteractiveMode(DWORD mode)
{
	ASSERT(mode<IMC_MAX);
	m_InteractiveMode = mode;
	::SetCursor(m_hCursor[mode]);
}

void CwglArcballWnd::TransformScene(BOOL No_Rotate) const
{
	glTranslated(2*m_SceneTranslationX,2*m_SceneTranslationY,0);
	glScalef(4*m_SceneZoom,4*m_SceneZoom,4*m_SceneZoom);

	if(No_Rotate){}
	else
		m_SceneArcBall.Apply();
}

void CwglArcballWnd::OnViewportChanged(int w, int h)
{
    m_SceneArcBall.SetWindowSize(0, 0, w - 1, h - 1);
    m_SceneArcBall_Env.SetWindowSize(0, 0, w - 1, h - 1);
}

void CwglArcballWnd::RotateScene() const
{
	m_SceneArcBall.Apply();
}

void CwglArcballWnd::RotateScene_AsEnviroment() const
{
	m_SceneArcBall_Env.Apply();
}

DWORD CwglArcballWnd::GetInteractiveMode() const
{
	return m_InteractiveMode;
}

void CwglArcballWnd::OnRender()
{
	glClear(GL_COLOR_BUFFER_BIT|GL_DEPTH_BUFFER_BIT);

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();

	glTranslated(0,0,-5);
	TransformScene();

	DrawUnitBox();

	glFinish();
	SwapBuffers(wglGetCurrentDC());
	_CheckErrorGL;
}
}