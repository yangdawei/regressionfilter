#pragma once

//////////////////////////////////////////////////////////////////////
// Visual Computing Foundation Classes (VCFC)
//					Jiaping Wang  2006.2
//					e_boris2002@hotmail.com
//
// (C) Copyright Jiaping Wang 2004.
//
// Permission to copy, use, modify, sell and distribute this software
// is granted provided this copyright notice appears in all copies.
// This software is provided "as is" without express or implied
// warranty, and with no claim as to its suitability for any purpose.
//
// Absolutely no warranty!!
//////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////
//  Brief Description
//
//  wglfbo.h
//
//  Frame buffer object
//
//////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////
// Revise History
//
// Inital version	2005.?.?		Kunxu
//
//////////////////////////////////////////////////////////////////////

#include "wglextension.h"

namespace wglh
{

namespace ext
{

class CFrameBufferObjectBase
{
public:
	static BOOL LoadExtensionEntryPoint();
	static BOOL CheckFramebufferStatus();

	static GLint iMaxColorAttachment;
	static GLint iMaxRenderBufferSize;
	
	//////////////////////////////////////////////////////////////////////////
	// ATTACHMENTS	
	enum{
		ATTACHMENT_COLOR0	=	GL_COLOR_ATTACHMENT0_EXT,
		ATTACHMENT_COLOR1	=	GL_COLOR_ATTACHMENT1_EXT,
		ATTACHMENT_COLOR2	=	GL_COLOR_ATTACHMENT2_EXT,
		ATTACHMENT_COLOR3	=	GL_COLOR_ATTACHMENT3_EXT,
		ATTACHMENT_DEPTH	=	GL_DEPTH_ATTACHMENT_EXT,
		ATTACHMENT_STENCIL	=	GL_STENCIL_ATTACHMENT_EXT
	};
	//////////////////////////////////////////////////////////////////////////
	// INTERNAL FORMATS FOR RENDER BUFFER:
	// normal texture formats
	// STENCIL_INDEX1_EXT
	// STENCIL_INDEX4_EXT
	// STENCIL_INDEX8_EXT
	// STENCIL_INDEX16_EXT

protected:
	static PFNGLISRENDERBUFFEREXTPROC glIsRenderbufferEXT;
	static PFNGLBINDRENDERBUFFEREXTPROC glBindRenderbufferEXT;
	static PFNGLDELETERENDERBUFFERSEXTPROC glDeleteRenderbuffersEXT ;
	static PFNGLGENRENDERBUFFERSEXTPROC glGenRenderbuffersEXT ;
	static PFNGLRENDERBUFFERSTORAGEEXTPROC glRenderbufferStorageEXT ;
	static PFNGLGETRENDERBUFFERPARAMETERIVEXTPROC glGetRenderbufferParameterivEXT ;
	static PFNGLISFRAMEBUFFEREXTPROC glIsFramebufferEXT ;
	static PFNGLBINDFRAMEBUFFEREXTPROC glBindFramebufferEXT ;
	static PFNGLDELETEFRAMEBUFFERSEXTPROC glDeleteFramebuffersEXT ;
	static PFNGLGENFRAMEBUFFERSEXTPROC glGenFramebuffersEXT ;
	static PFNGLCHECKFRAMEBUFFERSTATUSEXTPROC glCheckFramebufferStatusEXT ;
	static PFNGLFRAMEBUFFERTEXTURE1DEXTPROC glFramebufferTexture1DEXT ;
	static PFNGLFRAMEBUFFERTEXTURE2DEXTPROC glFramebufferTexture2DEXT ;
	static PFNGLFRAMEBUFFERTEXTURE3DEXTPROC glFramebufferTexture3DEXT ;
	static PFNGLFRAMEBUFFERRENDERBUFFEREXTPROC glFramebufferRenderbufferEXT ;
	static PFNGLGETFRAMEBUFFERATTACHMENTPARAMETERIVEXTPROC glGetFramebufferAttachmentParameterivEXT ;
	static PFNGLGENERATEMIPMAPEXTPROC glGenerateMipmapEXT ;

	static  PFNGLFRAMEBUFFERTEXTUREEXTPROC			FramebufferTextureEXT;
	static  PFNGLFRAMEBUFFERTEXTURELAYEREXTPROC		FramebufferTextureLayerEXT;
	static  PFNGLFRAMEBUFFERTEXTUREFACEEXTPROC		FramebufferTextureFaceEXT;

public:
};


class CRenderBuffer : public wglh::ext::CFrameBufferObjectBase
{
public:
	CRenderBuffer()
	{
		m_rbName = 0;
	}
	~CRenderBuffer()
	{
		Destroy();
	}
	UINT GetRenderBufferName()
	{
		return m_rbName;
	}

	BOOL Create()
	{
		if(m_rbName)
		{
			return TRUE;
		}
		else
		{
			glGenRenderbuffersEXT(1,&m_rbName);
		}
		return TRUE;
	}
	void Destroy()
	{
		if(m_rbName)
		{
			glDeleteRenderbuffersEXT(1,&m_rbName);
			m_rbName = 0;
		}
	}
	void SetSize(GLenum interalFormat,GLint width,GLint height)
	{
		ASSERT(m_rbName);
		ASSERT(width>0&&width<iMaxRenderBufferSize);
		ASSERT(height>0&&height<iMaxRenderBufferSize);
		glBindRenderbufferEXT(GL_RENDERBUFFER_EXT,m_rbName);
		glRenderbufferStorageEXT(GL_RENDERBUFFER_EXT,interalFormat,width,height);
	}
	

protected:
	UINT m_rbName;	
};


class CFrameBufferObject : public wglh::ext::CFrameBufferObjectBase
{
public:
	CFrameBufferObject()
	{
		m_fbName = 0;
	}
	~CFrameBufferObject()
	{
		Destroy();
	}

	UINT GetFrameBufferName();

	BOOL Create();
	void Destroy();

	void Apply();
	void ApplyNull();

	void AttachTexture1D(GLenum attachment,GLuint textureName,GLint level);
	void AttachTexture2D(GLenum attachment,GLenum textureTarget,GLuint textureName,GLint level);
	void AttachTexture3D(GLenum attachment,GLuint textureName,GLint level,GLint zOffset);

	void AttachTexture3DExt(GLenum attachment, GLuint textureName, GLint level);

	void DetachTexture(GLenum attchament);

	void AttachRenderBuffer(GLenum attachment,CRenderBuffer&rb);
	void DetachRenderBuffer(GLenum attachment);
	
	void DrawBuffer(GLenum attachment);
	void ReadBuffer(GLenum attachment);

protected:
	UINT m_fbName;	
};

}

}