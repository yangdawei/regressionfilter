#pragma once

//////////////////////////////////////////////////////////////////////
// Visual Computing Foundation Classes (VCFC)
//					Jiaping Wang  2006.2
//					e_boris2002@hotmail.com
//
// (C) Copyright Jiaping Wang 2004.
//
// Permission to copy, use, modify, sell and distribute this software
// is granted provided this copyright notice appears in all copies.
// This software is provided "as is" without express or implied
// warranty, and with no claim as to its suitability for any purpose.
//
// Absolutely no warranty!!
//////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////
//  Brief Description
//
//  wglMesh.h
//
//  geometry data and processing
//
//////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////
// Revise History
//
// Inital version	2005.?.?		Jiaping
// Subdivision		2006.12.17		Jiaping
// SmoothOutlier	2006.12.19		Jiaping
// Tangent			2008.10			Jiaping
//
//////////////////////////////////////////////////////////////////////

#include "wglextension.h"
#include "..\num\small_vec.h"


namespace wglh
{

///////////////////////////////////////////
// Scene Render via traditional pipeline without localframe info
class CwglMesh
{
private:
	void	_AppendIdenticalIndex(::rt::BufferEx<UINT>& list, UINT vid);
	double	_Compute_Laplacian(const ::rt::BufferEx<UINT>& neighbor_list, UINT i, num::Vec3d& average);

private:
	BOOL		_bUpdateTriangleId;
	num::Vec3f*	_p_VertexPosition;
	num::Vec3f*	_p_VertexNormal;
	num::Vec3f*	_p_VertexTangent;
	num::Vec3f*	_p_VertexTexCoord;
	num::Vec4b*	_p_VertexColor;    
	UINT*		_p_MeshElementIndic;
	num::Vec3f	_Bounding_Cent;
	float		_Bounding_Scale;

public:
	struct MeshFileHeader
	{
		DWORD	Magic;	//
		DWORD	Version;
		DWORD	RenderMode_OpenGL;
		DWORD	MeshElementIndexCount;
		DWORD	VertexCount;
        
		DWORD	Offset_MeshElementIndic;	// Offset to the begin of file

		DWORD	Offset_VertexPosition;		// N/A if it is zero
		DWORD	Offset_VertexColor;
		DWORD	Offset_VertexTexCoord;
		DWORD	Offset_VertexNormal;
		DWORD	Offset_VertexTangent;
	};

	static const int ObjectFileMagicNum = 0x4e454353 ;
	enum
	{
		LOADING_FLAG_NONE			  = 0x0,
		LOADING_FLAG_BUFFER_IN_MEMORY = 0x1,
		LOADING_FLAG_KEEP_OBJECT_SIZE = 0x2,

		RENDERING_FLAG_NONE					= 0x0,
		RENDERING_FLAG_POINT_CLOUD			= 0x1,
		RENDERING_FLAG_DISABLE_VERTEX_COLOR = 0x2,
		RENDERING_FLAG_TANGENT_AS_COLOR		= 0x8,
	};

protected:

	// Mesh
	UINT							m_MeshRenderMode;
	ext::CwglVertexIndexBuffer		m_MeshElementIndic;

	// Vertex
	ext::CwglVertexAttributeBuffer	m_VertexPosition;	// Vec3f
	ext::CwglVertexAttributeBuffer	m_VertexNormal;		// Vec3f
	ext::CwglVertexAttributeBuffer	m_VertexTangent;	// Vec3f
	ext::CwglVertexAttributeBuffer	m_VertexTexCoord;	// Vec3f
	ext::CwglVertexAttributeBuffer	m_VertexColor;		// Vec4b

	// Vertex Pick
	ext::CwglVertexAttributeBuffer	m_VertexId_Color;			// Vec4b
	ext::CwglVertexAttributeBuffer	m_VertexFrontId_Position;	// Vec3f

	// Triangle Pick
	ext::CwglVertexAttributeBuffer	m_ElementId_Color;		// Vec4b
	ext::CwglVertexAttributeBuffer	m_ElementId_Position;	// Vec3f

	UINT	m_VertexCount;
	UINT	m_MeshElementIndicCount;

	// Buffered for read
	rt::Buffer<UINT>			m_Buffered_MeshElementIndic;

	rt::Buffer<num::Vec3f>		m_Buffered_VertexPosition;
	rt::Buffer<num::Vec3f>		m_Buffered_VertexNormal;
	rt::Buffer<num::Vec3f>		m_Buffered_VertexTangent;
	rt::Buffer<num::Vec4b>		m_Buffered_VertexColor;
	rt::Buffer<num::Vec3f>		m_Buffered_VertexTexCoord;

public:
	CwglMesh();

	const UINT *		Get_MeshElementIndic()const{ return (UINT*)m_Buffered_MeshElementIndic.Begin(); }
	const num::Vec3f*	Get_VertexPosition()const{ return m_Buffered_VertexPosition; }
	const num::Vec3f*	Get_VertexNormal()const{ return m_Buffered_VertexNormal; }
	const num::Vec3f*	Get_VertexTangent()const{ return m_Buffered_VertexTangent; }
	const num::Vec4b*	Get_VertexColor()const{ return m_Buffered_VertexColor; }
	const num::Vec3f*	Get_VertexTexCoord()const{ return m_Buffered_VertexTexCoord; }

	void SetSize_MeshElementIndic(int len=0);
	void SetSize_Vertex(int len=0);
	
	void SubmitBuffers(BOOL UpdateInMemoryBackup = FALSE);

	UINT *		GetBuffer_MeshElementIndic();
	num::Vec3f* GetBuffer_VertexPosition(DWORD StorageHint = wglh::ext::CwglVertexBufferBase::StorageHint_OnBoard);
	num::Vec3f* GetBuffer_VertexNormal(DWORD StorageHint = wglh::ext::CwglVertexBufferBase::StorageHint_OnBoard);
	num::Vec3f* GetBuffer_VertexTangent(DWORD StorageHint = wglh::ext::CwglVertexBufferBase::StorageHint_OnBoard);
	num::Vec3f* GetBuffer_VertexTexCoord(DWORD StorageHint = wglh::ext::CwglVertexBufferBase::StorageHint_OnBoard);
	num::Vec4b* GetBuffer_VertexColor(DWORD StorageHint = wglh::ext::CwglVertexBufferBase::StorageHint_OnBoard);		// RGBA
	void		NormalizePosition(num::Vec3f* pMinv=NULL,num::Vec3f* pMaxv=NULL);
	void		NormalizePosition(const CwglMesh&);

	void		Modify_VertexPosition(const num::Vec3f & val,int vertex_index);
	void		Modify_VertexNormal(const num::Vec3f & val,int vertex_index);
	void		Modify_VertexTangent(const num::Vec3f & val,int vertex_index);
	void		Modify_VertexTexCoord(const num::Vec3f & val,int vertex_index);
	void		Modify_VertexColor(const num::Vec4b & val,int vertex_index);

	void		Destroy();
	void		SetMeshRenderMode(GLenum mode = GL_POINTS){ m_MeshRenderMode = mode; }
	UINT		GetMeshRenderMode()const{ return m_MeshRenderMode; }
	void		Render(int Flag = RENDERING_FLAG_NONE);
	void		RenderVertexId(BOOL bOnlyFront = TRUE);
	void		RenderTriangleId();
	UINT		GetVPE() const; //vertex per element

	static void DisableVertexArray();

	HRESULT		Load(LPCTSTR fn, DWORD LoadFlag = LOADING_FLAG_NONE);
	HRESULT		Save(LPCTSTR fn);

	BOOL		IsNormalBinded() const{ return m_VertexNormal.IsOk(); }
	BOOL		IsTangentBinded() const{ return m_VertexTangent.IsOk(); }
	BOOL		IsTextureBinded() const{ return m_VertexTexCoord.IsOk(); }
	BOOL		IsColorBinded() const{ return m_VertexColor.IsOk(); }

	UINT		GetVertexCount()const{ return m_VertexCount; }
	UINT		GetMeshElementIndicCount()const{ return m_MeshElementIndicCount; }

	BOOL		IsLoaded(){ return GetVertexCount(); }
	void		EnableAutoUpdateElementId(BOOL enable = TRUE);
	UINT		PickVertex(int x, int y, BOOL bOnlyFront = TRUE);	// viewport coordinate, return vertex id
	UINT		PickVertex(int x, int y, int w, int h, rt::BufferEx<UINT>& elements, BOOL bOnlyFront = TRUE);	// viewport coordinate, return vertex id
	UINT		PickElement(int x, int y);	// viewport coordinate, return triangle id
	UINT		PickElement(int x, int y, int w, int h, rt::BufferEx<UINT>& elements);	// viewport coordinate, return triangle id
	//UINT		PickElements(int x,int y, int width, int height, UINT* p); 	// viewport coordinate, return countn of picked trianges

	// Mesh manipulation
	void		SplitSharedVertex();
	void		ConformTangentSpace(BOOL do_normal_only = FALSE);
	void		MergeEquivalentVertex_Position();
	void		MergeEquivalentVertex_TexCoord();
	void		Subdividision();
	void		SmoothOutlier(float strength = 1.0f); // strength >= EPSILON
    void		FlipOrientation();
	void		FixOrientation();
    void		RecalculateNormal();
	void		RecalculateTangent();
	void		LaplacianSmoothing(float strength = 0.2f);
};



//////////////////////////////////////////////////
// Scene Render via GPU pipeline with localframe info
class CwglMeshEx:public CwglMesh
{
protected:

	rt::Buffer<BOOL>							m_bLockedVertexTexCoordExt;
	rt::Buffer<ext::CwglVertexAttributeBuffer>	m_VertexTexCoordExt;
	rt::Buffer<rt::Vec<char,100> >				m_VertexAttribNames;

public:
	CwglMeshEx( LPCSTR VertexAttribNames );	//"name0\0name1\0name2\0\0" 

	num::Vec4f * GetBuffer_VertexAttribute(UINT ind,DWORD StorageHint = wglh::ext::CwglVertexBufferBase::StorageHint_OnBoard);  // ind>=1 

	BOOL	IsAttributeBinded(int ind){ ASSERT(ind>0); return m_VertexTexCoordExt[ind-1].IsOk(); } // for TexCoord0 use IsTextureBinded instead

	void SetSize_Vertex(int len=0);
	void SubmitBuffers();
	void Destroy();
	void Render(UINT Flag = RENDERING_FLAG_NONE);
	static void	 DisableVertexArray();
};



}

