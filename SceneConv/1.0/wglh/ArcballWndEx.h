#pragma once

#include "TransCtrl.h"
#include "jgl/sprite.h"
#include "jgl/cubeMap.h"

namespace wglh {

class CArcballWndEx : public CwglArcballWnd
{
public:
	CArcballWndEx(void);
	~CArcballWndEx(void);
	
protected:	
	LRESULT WndProc(UINT message, WPARAM wParam, LPARAM lParam);
	virtual void OnRender();
	void OnRenderEnvironment();
protected:
	wglh::CTransformCtrl m_transformCtrl;
#ifdef USE_SPOT_LIGHT
	wglh::CMoveCtrl m_lightCtrl;
#endif
	jgl::JSpriteContainer* m_sprites;
	jgl::CubeMap m_cubeMapEnv;

	num::Vec2i					m_MouseStart;
	num::Vec2i					m_MouseEnd;
protected:
	
	//this function is to be overrided
	virtual void OnInitScene();
	virtual void OnUninitScene();
	virtual void OnRenderObject();
	
	virtual void preRender();
	virtual void postRender();

	virtual bool handleKeyboard(unsigned char c);
protected:
	void updateSceneScale(float scale);
};

}