#pragma once

//////////////////////////////////////////////////////////////////////
// Visual Computing Foundation Classes (VCFC)
//					Jiaping Wang  2006.2
//					e_boris2002@hotmail.com
//
// (C) Copyright Jiaping Wang 2004.
//
// Permission to copy, use, modify, sell and distribute this software
// is granted provided this copyright notice appears in all copies.
// This software is provided "as is" without express or implied
// warranty, and with no claim as to its suitability for any purpose.
//
// Absolutely no warranty!!
//////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////
//  Brief Description
//
//  wglImage.h
//
//  Simple image file manipulation for OpenGL texturing
//
//////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////
// Revise History
//
// Inital version	2006.9.15		Jiaping
//
//////////////////////////////////////////////////////////////////////

#include "wglhelper.h"

#define INCLUDE_WGLH_IMAGE

#pragma warning(disable:1684)

namespace wglh
{

class CwglImage:public CwglSize
{
protected:
	BOOL			_IsHDR;
	UINT			BytePerPixel;
	UINT			Step;
	LPBYTE			pImg;
	BOOL			IsMemoryRefered;

	BOOL ScaleImage(LPBYTE dst,int w,int h) const;
public:
	enum _tagTextureStorage
	{	FT_TEXTURE_FILE			 = 0x0,
		FT_TEXTURE_RESID		 = 0x1,
		FT_TEXTURE_RESSTRING	 = 0x2,
		FT_TEXTURE_STORAGE_MASK	 = 0x3
	};
	
	CwglImage();
	CwglImage(const CwglSize &size, int BPP, BOOL HDR = FALSE);
	CwglImage(const CwglImage &in, BOOL never_refer = FALSE);	//new bitmap, or keep refer
	CwglImage(LPVOID pData,int w,int h,int step,int BytePP, BOOL HDR = FALSE);  //Ref bitmap
	~CwglImage(){ if(!IsMemoryRefered)_SafeFree32AL(pImg); }

	BOOL	IsNotNull(){ return (BOOL)pImg; }
	BOOL	IsNull(){ return !pImg; }
	int		GetWidth()const{ return width; }
	int		GetHeight()const{ return height; }
	UINT	GetBPP() const{return BytePerPixel*8;};
	UINT	GetDataSize() const{return Step*height;}
	UINT	GetStep()const {return Step;}
	BOOL	IsHDR()const { return _IsHDR; }

	LPBYTE		GetPixelAddress(int x=0,int y=0){ return &pImg[y*Step+x*BytePerPixel]; }
	LPCBYTE		GetPixelAddress(int x=0,int y=0) const{ return &pImg[y*Step+x*BytePerPixel]; }

	template<typename t_Val>
	t_Val&			GetPixel(int x=0,int y=0){ return *((t_Val*)GetPixelAddress(x,y)); }
	template<typename t_Val>
	const t_Val&	GetPixel(int x=0,int y=0) const{ return *((t_Val*)GetPixelAddress(x,y)); }

	CwglImage	GetSub(int x,int y,int w,int h)
	{	ASSERT(x+w <= width);
		ASSERT(y+h <= height);
		return CwglImage(GetPixelAddress(x,y),w,h,Step,BytePerPixel);
	}

	BOOL SetSize(int w,int h,int BPP, BOOL HDR = FALSE);
	void SetRef(LPVOID pData,int w,int h,int stp,int BytePP, BOOL HDR = FALSE)
	{
		_IsHDR = HDR;
		if(!IsMemoryRefered)_SafeFree32AL(pImg);
        BytePerPixel = BytePP;	Step = stp;
		width = w;	height = h;	pImg = (LPBYTE)pData;
		IsMemoryRefered = TRUE;
	}

	void CopyTo(CwglImage &in)const;

	GLenum GetGlPixelFormat() const;
	GLenum GetGlPixelValue() const;	// GL_UNSIGNED_BYTE or GL_FLOAT
	void Zero(){ ZeroMemory(pImg,GetDataSize()); }

	void operator >> (CwglImage &in) const{ ScaleImage(in.pImg,in.width,in.height); }
	operator LPBYTE (){return pImg;}
	operator LPCBYTE () const{return pImg;}

	BOOL Resize(int w,int h);
	BOOL Resize(CwglSize &size){return Resize(size.width,size.height);}
	void VerticalFlip();
	void HorizontalFlip();
	void TransposeFlip();

	void Rotate_90(){ TransposeFlip(); HorizontalFlip(); }
	void Rotate_180(){ VerticalFlip(); HorizontalFlip(); }
	void Rotate_270(){ HorizontalFlip(); TransposeFlip(); }

	BOOL Load(LPCTSTR fn,_tagTextureStorage LoadFrom = FT_TEXTURE_FILE);
	BOOL Save(LPCTSTR fn) const;
	void CopyFrameBuffer(int x=0,int y=0);
    void CopyTexture();

	HGLOBAL GetDIB() const;
};


}

