#pragma once

//////////////////////////////////////////////////////////////////////
// Visual Computing Foundation Classes (VCFC)
//					Jiaping Wang  2006.2
//					e_boris2002@hotmail.com
//
// (C) Copyright Jiaping Wang 2004.
//
// Permission to copy, use, modify, sell and distribute this software
// is granted provided this copyright notice appears in all copies.
// This software is provided "as is" without express or implied
// warranty, and with no claim as to its suitability for any purpose.
//
// Absolutely no warranty!!
//////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////
//  Brief Description
//
//  wglshader.h
//  glsl support
//
//  Handle WGLH_GLSL_SHADER_CHANGED_MESSAGE to reload changed shaders
//
//////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////
// Revise History
//
// Inital version	2005.?.?		Jiaping
//
//////////////////////////////////////////////////////////////////////

#include "wglHelper.h"
#include "wglextension.h"
#include "..\num\small_vec.h"
#include "..\w32\file_64.h"

#define INCLUDE_WGLH_SHADER

#define WGLH_GLSL_SHADER_CHANGED_MESSAGE	WM_USER+1842

#ifndef	GL_SAMPLER_1D_ARB
	#define GL_SAMPLER_1D_ARB				0x8B5D
#endif

#ifndef	GL_SAMPLER_2D_ARB
	#define GL_SAMPLER_2D_ARB				0x8B5E
#endif

#ifndef	GL_SAMPLER_3D_ARB
	#define GL_SAMPLER_3D_ARB				0x8B5F
#endif

#ifndef	GL_SAMPLER_CUBE_ARB
	#define GL_SAMPLER_CUBE_ARB				0x8B60
#endif

#ifndef	GL_SAMPLER_1D_SHADOW_ARB
	#define GL_SAMPLER_1D_SHADOW_ARB		0x8B61
#endif

#ifndef	GL_SAMPLER_2D_SHADOW_ARB
	#define GL_SAMPLER_2D_SHADOW_ARB		0x8B62
#endif

#ifndef	GL_SAMPLER_2D_RECT_ARB
	#define GL_SAMPLER_2D_RECT_ARB			0x8B63
#endif

#ifndef	GL_SAMPLER_2D_RECT_SHADOW_ARB
	#define GL_SAMPLER_2D_RECT_SHADOW_ARB	0x8B64
#endif


namespace wglh
{

#define DEF_STATUS_QUERY(type,name_func,arug) type name_func() \
		{ GLint ret = 0; if(IsCreated()){ GetObjectParameterivARB(m_Handle,arug,&ret); _CheckErrorGL; } return ret; }

namespace glsl
{

class CwglShaderVarible
{
public:
	int	Index;	//available for attrib varibles
	int Size;
	LPSTR Name;
	LPSTR Name2;

	GLhandleARB	nn;
};

class CwglShaderVarible_RT:public CwglShaderVarible
{
public:
	GLenum	Type;
	int		NameLen;
public:
	CwglShaderVarible_RT(){ Name = NULL; NameLen = 0;}
	~CwglShaderVarible_RT(){ _SafeDelArray(Name); }
	void SetNameBufferSize(int len);
	void DumpDescription();
};


class CwglLanguageObject
{
public:
// Language Object management
	static PFNGLCREATEPROGRAMOBJECTARBPROC			CreateProgramObjectARB;
	static PFNGLCREATESHADEROBJECTARBPROC			CreateShaderObjectARB;
	static PFNGLDELETEOBJECTARBPROC					DeleteObjectARB;
	static PFNGLGETHANDLEARBPROC					GetHandleARB;
	static PFNGLDETACHOBJECTARBPROC					DetachObjectARB;
	static PFNGLSHADERSOURCEARBPROC					ShaderSourceARB;
	static PFNGLCOMPILESHADERARBPROC				CompileShaderARB;
	static PFNGLATTACHOBJECTARBPROC					AttachObjectARB;
	static PFNGLLINKPROGRAMARBPROC					LinkProgramARB;
	static PFNGLUSEPROGRAMOBJECTARBPROC				UseProgramObjectARB;
	static PFNGLVALIDATEPROGRAMARBPROC				ValidateProgramARB;
	static PFNGLGETOBJECTPARAMETERFVARBPROC			GetObjectParameterfvARB;
	static PFNGLGETOBJECTPARAMETERIVARBPROC			GetObjectParameterivARB;
	static PFNGLGETINFOLOGARBPROC					GetInfoLogARB;
	static PFNGLGETATTACHEDOBJECTSARBPROC			GetAttachedObjectsARB;
	static PFNGLGETUNIFORMLOCATIONARBPROC			GetUniformLocationARB;
	static PFNGLGETACTIVEUNIFORMARBPROC				GetActiveUniformARB;
	static PFNGLGETSHADERSOURCEARBPROC				GetShaderSourceARB;	

// Access global varibles in shader
	static PFNGLUNIFORM1FARBPROC					Uniform1fARB;
	static PFNGLUNIFORM2FARBPROC					Uniform2fARB;
	static PFNGLUNIFORM3FARBPROC					Uniform3fARB;
	static PFNGLUNIFORM4FARBPROC					Uniform4fARB;
	static PFNGLUNIFORM1IARBPROC					Uniform1iARB;
	static PFNGLUNIFORM2IARBPROC					Uniform2iARB;
	static PFNGLUNIFORM3IARBPROC					Uniform3iARB;
	static PFNGLUNIFORM4IARBPROC					Uniform4iARB;
	static PFNGLUNIFORM1FVARBPROC					Uniform1fvARB;
	static PFNGLUNIFORM2FVARBPROC					Uniform2fvARB;
	static PFNGLUNIFORM3FVARBPROC					Uniform3fvARB;
	static PFNGLUNIFORM4FVARBPROC					Uniform4fvARB;
	static PFNGLUNIFORM1IVARBPROC					Uniform1ivARB;
	static PFNGLUNIFORM2IVARBPROC					Uniform2ivARB;
	static PFNGLUNIFORM3IVARBPROC					Uniform3ivARB;
	static PFNGLUNIFORM4IVARBPROC					Uniform4ivARB;
	static PFNGLUNIFORMMATRIX2FVARBPROC				UniformMatrix2fvARB;
	static PFNGLUNIFORMMATRIX3FVARBPROC				UniformMatrix3fvARB;
	static PFNGLUNIFORMMATRIX4FVARBPROC				UniformMatrix4fvARB;
	static PFNGLGETUNIFORMFVARBPROC					GetUniformfvARB;
	static PFNGLGETUNIFORMIVARBPROC					GetUniformivARB;

// Vertex shader
	static  PFNGLBINDATTRIBLOCATIONARBPROC			BindAttribLocationARB;
	static  PFNGLGETACTIVEATTRIBARBPROC				GetActiveAttribARB;
	static  PFNGLGETATTRIBLOCATIONARBPROC			GetAttribLocationARB;

// Geometry shader
	static  PFNGLPROGRAMPARAMETERIEXTPROC			ProgramParameteriEXT;

protected:
	DEF_STATUS_QUERY(int,GetLogLength,GL_OBJECT_INFO_LOG_LENGTH_ARB);

public:

#define ReturnCurrentGLInteger(x) 	GLint name; glGetIntegerv((GL_##x),&name); _CheckErrorGL; return name;

	static int	Vertex_MaxAttrib(){ ReturnCurrentGLInteger(MAX_VERTEX_ATTRIBS_ARB) }
	static int	Vertex_MaxUniform(){ ReturnCurrentGLInteger(MAX_VERTEX_UNIFORM_COMPONENTS_ARB) }
	static int	Vertex_MaxTextureUnitCombined(){ ReturnCurrentGLInteger(MAX_COMBINED_TEXTURE_IMAGE_UNITS_ARB) }
	static int	Vertex_MaxTextureUnit(){ ReturnCurrentGLInteger(MAX_VERTEX_TEXTURE_IMAGE_UNITS_ARB) }
	
	static int	Vertex_MaxTextureCoord(){ ReturnCurrentGLInteger(MAX_TEXTURE_COORDS_ARB) }

	static int	Fragment_MaxVarying(){ ReturnCurrentGLInteger(MAX_VARYING_FLOATS_ARB) }
	static int	Fragment_MaxTextureUnit(){ ReturnCurrentGLInteger(MAX_TEXTURE_IMAGE_UNITS_ARB) }
	static int	Fragment_MaxUniform(){ ReturnCurrentGLInteger(MAX_FRAGMENT_UNIFORM_COMPONENTS_ARB) }

#undef ReturnCurrentGLInteger

protected:
	GLhandleARB m_Handle;

public:
	static BOOL	LoadExtensionEntryPoints();
	static int	GetVersion();
	static BOOL	IsSupportted(){ return (BOOL)CreateProgramObjectARB; }

	CwglLanguageObject(){ m_Handle = NULL; }
	~CwglLanguageObject(){ if(IsCreated())Destroy(); }  //Destroy should be explicit called
	operator GLhandleARB ()const { return m_Handle; }

	void Destroy();
	BOOL IsCreated(){ return (BOOL)m_Handle; }
	void DumpLog();
};

class CwglShaderCode:public CwglLanguageObject
{
	friend class CwglShaderCodeManagment;
	friend class CwglLanguageObject;
	friend class CwglProgramEx;
protected:
	LPCTSTR m_FileName;
	BOOL	m_bSourceCodeChanged;
public:
	enum
	{
		VertexShader = GL_VERTEX_SHADER_ARB,
		FragmentShader = GL_FRAGMENT_SHADER_ARB,
		GeometryShader = GL_GEOMETRY_SHADER_EXT
	};

	CwglShaderCode(LPCTSTR SourceCodeFileName = _T(":Unknown Stock Shader"))
			:m_FileName(SourceCodeFileName){}

	void SetFilename(LPCTSTR fn){ m_FileName = fn; }	//must be a static memory
	LPCTSTR GetFilename() const{ return m_FileName; }
	
	void Create(GLenum ShaderType = VertexShader);
	void Compile();
	void SetSourceCode(const GLcharARB * pStr);
	BOOL IsCompiled();

	DEF_STATUS_QUERY(int,GetSourceCodeLength,GL_OBJECT_SHADER_SOURCE_LENGTH_ARB);
};

class CwglProgram:public CwglLanguageObject
{
public:
	CwglProgram(){m_GeometryEnabled = false; m_GeometryInputMode = 0; m_GeometryOutputMode = 0;}
	void Create();
	void AddShader(GLhandleARB sh);
	void RemoveShader(GLhandleARB sh);
	void Link();
	void Apply(){ ASSERT(m_Handle); UseProgramObjectARB(m_Handle); _CheckErrorGL; }
	BOOL Validate(){ ASSERT(m_Handle); ValidateProgramARB(m_Handle); _CheckErrorGL; return IsReadyToRun(); }

	static void DisableShaders(){ UseProgramObjectARB(NULL); _CheckErrorGL;  }

	void BindAttribLocation(LPCSTR VarName,int AttribIndex);
	int  GetAttribLocation(LPCSTR Varname){ ASSERT(m_Handle); return GetAttribLocationARB(m_Handle,Varname); }
	int	 GetUniformLocation(LPCSTR Varname){ ASSERT(m_Handle); return GetUniformLocationARB(m_Handle,Varname); }

	DEF_STATUS_QUERY(BOOL,IsLinked,GL_OBJECT_LINK_STATUS_ARB);
	DEF_STATUS_QUERY(BOOL,IsReadyToRun,GL_OBJECT_VALIDATE_STATUS_ARB);
	DEF_STATUS_QUERY(int,GetAttachedShaderCount,GL_OBJECT_ATTACHED_OBJECTS_ARB);
	DEF_STATUS_QUERY(int,GetActivedAttribCount,GL_OBJECT_ACTIVE_ATTRIBUTES_ARB);
	DEF_STATUS_QUERY(int,GetMaxAttribNameLength,GL_OBJECT_ACTIVE_ATTRIBUTE_MAX_LENGTH_ARB);
	DEF_STATUS_QUERY(int,GetActivedUniformCount,GL_OBJECT_ACTIVE_UNIFORMS_ARB);
	DEF_STATUS_QUERY(int,GetMaxUniformNameLength,GL_OBJECT_ACTIVE_UNIFORM_MAX_LENGTH_ARB);
	void DumpCompiledInformation();

	void GetActivedUniform(int index, CwglShaderVarible_RT& varible);
	void GetActivedAttrib(int index, CwglShaderVarible_RT& varible);
	static GLhandleARB GetCurrentShader(){ return GetHandleARB(GL_PROGRAM_OBJECT_ARB); }

	bool	m_GeometryEnabled;
	int		m_GeometryInputMode;
	int		m_GeometryOutputMode;
	void	SetGeometryShaderIOMode(int input, int output);

public:
#define UFLO	ASSERT(m_Handle); ASSERT(Name); GLint loc = GetUniformLocationARB(m_Handle,Name); /*if(loc<0){ _CheckDump("\nUniform "<<Name<<" not found.\n");}*/
	void SetUniform(LPCSTR Name, float x){ UFLO; Uniform1fARB(loc,x); }
	void SetUniform(LPCSTR Name, float x,float y){ UFLO; Uniform2fARB(loc,x,y); }
	void SetUniform(LPCSTR Name, float x,float y,float z){ UFLO; Uniform3fARB(loc,x,y,z); }
	void SetUniform(LPCSTR Name, float x,float y,float z,float w){ UFLO; Uniform4fARB(loc,x,y,z,w); }
	void SetUniform(LPCSTR Name, int x){ UFLO; Uniform1iARB(loc,x); }
	void SetUniform(LPCSTR Name, int x,int y){ UFLO; Uniform2iARB(loc,x,y); }
	void SetUniform(LPCSTR Name, int x,int y,int z){ UFLO; Uniform3iARB(loc,x,y,z); }
	void SetUniform(LPCSTR Name, int x,int y,int z,int w){ UFLO; Uniform4iARB(loc,x,y,z,w); }

	void SetUniformArray1f(LPCSTR Name, const float* p, int count){ UFLO; ASSERT(p); Uniform1fvARB(loc,count,p); }
	void SetUniformArray2f(LPCSTR Name, const float* p, int count){ UFLO; ASSERT(p); Uniform2fvARB(loc,count,p); }
	void SetUniformArray3f(LPCSTR Name, const float* p, int count){ UFLO; ASSERT(p); Uniform3fvARB(loc,count,p); }
	void SetUniformArray4f(LPCSTR Name, const float* p, int count){ UFLO; ASSERT(p); Uniform4fvARB(loc,count,p); }
	void SetUniformArray1f(LPCSTR Name, const int* p, int count){ UFLO; ASSERT(p); Uniform1ivARB(loc,count,p); }
	void SetUniformArray2f(LPCSTR Name, const int* p, int count){ UFLO; ASSERT(p); Uniform2ivARB(loc,count,p); }
	void SetUniformArray3f(LPCSTR Name, const int* p, int count){ UFLO; ASSERT(p); Uniform3ivARB(loc,count,p); }
	void SetUniformArray4f(LPCSTR Name, const int* p, int count){ UFLO; ASSERT(p); Uniform4ivARB(loc,count,p); }

	void SetUniform(LPCSTR Name, const num::Vec2f& v){ UFLO; Uniform2fARB(loc,v.x,v.y); }
	void SetUniform(LPCSTR Name, const num::Vec3f& v){ UFLO; Uniform3fARB(loc,v.x,v.y,v.z); }
	void SetUniform(LPCSTR Name, const num::Vec4f& v){ UFLO; Uniform4fARB(loc,v.x,v.y,v.z,v.w); }
	void SetUniform(LPCSTR Name, const num::Vec2i& v){ UFLO; Uniform2iARB(loc,v.x,v.y); }
	void SetUniform(LPCSTR Name, const num::Vec3i& v){ UFLO; Uniform3iARB(loc,v.x,v.y,v.z); }
	void SetUniform(LPCSTR Name, const num::Vec4i& v){ UFLO; Uniform4iARB(loc,v.x,v.y,v.z,v.w); }

	void SetUniformArray2f(LPCSTR Name, const num::Vec2f* p, int count){ UFLO; ASSERT(p); Uniform2fvARB(loc,count,p[0].data()); }
	void SetUniformArray3f(LPCSTR Name, const num::Vec3f* p, int count){ UFLO; ASSERT(p); Uniform3fvARB(loc,count,p[0].data()); }
	void SetUniformArray4f(LPCSTR Name, const num::Vec4f* p, int count){ UFLO; ASSERT(p); Uniform4fvARB(loc,count,p[0].data()); }
	void SetUniformArray2f(LPCSTR Name, const num::Vec2i* p, int count){ UFLO; ASSERT(p); Uniform2ivARB(loc,count,p[0].data()); }
	void SetUniformArray3f(LPCSTR Name, const num::Vec3i* p, int count){ UFLO; ASSERT(p); Uniform3ivARB(loc,count,p[0].data()); }
	void SetUniformArray4f(LPCSTR Name, const num::Vec4i* p, int count){ UFLO; ASSERT(p); Uniform4ivARB(loc,count,p[0].data()); }

	void SetUniformMatrix_2x2(LPCSTR Name,const float* mat,BOOL transpose = FALSE)
		{ UFLO; ASSERT(mat); UniformMatrix2fvARB(loc,1,transpose,mat); }
	void SetUniformMatrix_3x3(LPCSTR Name,const float* mat,BOOL transpose = FALSE)
		{ UFLO; ASSERT(mat); UniformMatrix3fvARB(loc,1,transpose,mat); }
	void SetUniformMatrix_4x4(LPCSTR Name,const float* mat,BOOL transpose = FALSE)
		{ UFLO; ASSERT(mat); UniformMatrix4fvARB(loc,1,transpose,mat); }

	void SetUniformArrayMatrix_4x4(LPCSTR Name,const float* mat,int count,BOOL transpose = FALSE)
		{ UFLO; ASSERT(mat); UniformMatrix4fvARB(loc,count,transpose,mat); }

#undef UFLO

};

template<typename t_Obj>
class CObjectExpression:protected rt::BufferEx<t_Obj*>
{
public:
	UINT GetSize()const{ return (UINT)__super::GetSize(); }
	void Clear(){ __super::SetSize(); }
	int SearchItem(const t_Obj& p) const
	{	return (int)__super::SearchItem(::rt::_CastToNonconst(&p));	}

	CObjectExpression(){}
	CObjectExpression(const CObjectExpression& in){ *this = in; }
	CObjectExpression(t_Obj* p1,t_Obj* p2)
	{	if(p1!=p2)
		{	SetSize(2);
			_p[0] = p1;
			_p[1] = p2;
		}
		else
		{	SetSize(1);
			_p[0] = p1;
		}
	}
	CObjectExpression(const CObjectExpression& in,t_Obj& p)
	{	reserve(in.GetSize()+1);
		ChangeSize(in.GetSize());
		*this = in;

		if( in.SearchItem(p)<0 )push_back(&p);
	}
	CObjectExpression(const CObjectExpression& in,const CObjectExpression& in2)
	{	reserve(in.GetSize()+in2.GetSize());
		ChangeSize(in.GetSize());
		*this = in;
		for(UINT i=0;i<in2.GetSize();i++)
			if( in.SearchItem( *in2[i] )<0 )push_back(in2[i]);
	}

	const CObjectExpression& operator = (const CObjectExpression& in)
	{	SetSize(in.GetSize());
		CopyFrom(in);
		return in;
	}

	t_Obj& operator = (t_Obj& obj)
	{
		SetSize(1);
		_p[0] = &obj;
		return obj;
	}
	
	template<typename t_Obj>
	const CObjectExpression& operator += (const t_Obj& in)
	{
		*this = CObjectExpression(*this,in);
		return in;
	}
};

template<typename t_Obj>
CObjectExpression<t_Obj> operator + (const CObjectExpression<t_Obj>& expr,t_Obj& obj)
{
	return CObjectExpression<t_Obj>(expr,obj);
}

template<typename t_Obj>
CObjectExpression<t_Obj> operator + (t_Obj& obj,t_Obj& obj2)
{
	return CObjectExpression<t_Obj>(&obj,&obj2);
}

class CwglProgramEx:public CwglProgram
{
	friend class CwglShaderCodeManagment;
protected:
	CObjectExpression<CwglShaderCode>	m_ShaderList;

	// These functions are forbidden
	void Create();
	void AddShader(GLhandleARB sh);
	void RemoveShader(GLhandleARB sh);
	void Link();

public:
	const CObjectExpression<CwglShaderCode>& operator = (const CObjectExpression<CwglShaderCode>& expr)
	{	m_ShaderList = expr;	return m_ShaderList;	}

	const CObjectExpression<CwglShaderCode>& operator = (CwglShaderCode& obj)
	{	m_ShaderList = obj;		return m_ShaderList;	}

	const CObjectExpression<CwglShaderCode>& operator += (const CObjectExpression<CwglShaderCode>& expr)
	{	m_ShaderList = CObjectExpression<CwglShaderCode>(m_ShaderList,expr);	return m_ShaderList;	}

	const CObjectExpression<CwglShaderCode>& operator += (CwglShaderCode& obj)
	{	m_ShaderList = CObjectExpression<CwglShaderCode>(m_ShaderList,obj);		return m_ShaderList;	}
	
	BOOL IsDepended(const CwglShaderCode& obj){ return m_ShaderList.SearchItem(obj) >= 0; }
	BOOL Rebuild();
};


/////////////////////////////////////////////
// hxx regard as vertex shader
// cxx regard as fragment shader
class CwglShaderCodeManagment:protected w32::CFolderChangingMonitor
{
	friend glsl::CwglShaderCodeManagment* GetShaderManager();
protected:
	CObjectExpression<CwglProgramEx>	m_ProgramList;

	TCHAR	m_szFilenameBuffer[MAX_PATH + MAX_PATH + 1];
	LPTSTR	m_pFilenameBegin;

	HWND	m_hNotifyWnd;

	static CwglShaderCodeManagment*	g_ShaderManager;

protected:
	class CSourceCode
	{
	public:
		LPCTSTR				Filename;
		FILETIME			LastModify;
		CwglShaderCode*		Shader;
		TYPETRAITS_DECL_IS_AGGREGATE(true);
	};

	rt::Buffer<CSourceCode> m_SourceCodeList;

	void OnFolderChanged();

public:
	void SetNotifyWindow(HWND hWnd){ m_hNotifyWnd = hWnd; }
	~CwglShaderCodeManagment(){ ASSERT(g_ShaderManager!=NULL); g_ShaderManager = NULL; Destroy(); }
	CwglShaderCodeManagment(LPCTSTR pShaderFolder = _T(".\\"));

	const CObjectExpression<CwglProgramEx>& operator = (const CObjectExpression<CwglProgramEx>& expr);
	const CObjectExpression<CwglProgramEx>& operator = (CwglProgramEx& prog);
    const CObjectExpression<CwglProgramEx>& operator += (const CObjectExpression<CwglProgramEx>& expr);
	const CObjectExpression<CwglProgramEx>& operator += (CwglProgramEx& obj);

    BOOL UpdateShaderList(HWND notify_wnd = NULL);
	void DestroyAllLoadedObjects();
	void LoadChangedShaders();
	void UpdateShaders(const CObjectExpression<CwglShaderCode>& expr);
	void UpdateShaders(CwglShaderCode& obj){ UpdateShaders( CObjectExpression<CwglShaderCode>(&obj,&obj) ); }
};

CwglShaderCodeManagment* GetShaderManager(); // return the latest created CwglShaderCodeManagment

};

#undef	DEF_STATUS_QUERY


};

