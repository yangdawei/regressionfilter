#include "StdAfx.h"
#include "CubeMap.h"
#include "Pfm.h"

namespace jgl{
	CubeMap::CubeMap(void)
	{
		faces.SetSize(6);
		faces.Set(NULL);
		m_bAllocateTexture = false;
	}


	CubeMap::~CubeMap(void)
	{
		releaseImages();
	}

	void CubeMap::releaseImages(){
		for(unsigned int i=0;i<faces.GetSize();i++){
			if(faces[i]!=NULL){
				cvReleaseImage(&faces[i]);
			}
		}
	}

	bool CubeMap::Load_PFM(LPCTSTR fn){


		IplImage * tmpImg;
		
		if(!Pfm::Load_PFM(fn,tmpImg)){
			_CheckDump("[CubeMap] PFM File " << fn << " not exist !\n");
			return false;
		}

		int cx = tmpImg->width;
		int cy = tmpImg->height;

		if(!((cx%3 == 0) && (cy%4 == 0))){  // Read data here
			_CheckDump("[CubeMap] PFM File " << fn << " inlegal !\n");
			return false;
		}

		int H = cy;
		cx/=3;
		cy/=4;
		H-=cy;

		releaseImages();
		//create images 
		for(int i=0;i<6;i++){
			faces[i] = cvCreateImage(cvSize(cx,cy),IPL_DEPTH_32F,3);
		}

		Pfm::CopySubImageTo(tmpImg,cvRect(cx,H,cx,cy),faces[0]);
		Pfm::CopySubImageTo(tmpImg,cvRect(0,H-cy,cx,cy),faces[1]);
		Pfm::CopySubImageTo(tmpImg,cvRect(cx,H-cy,cx,cy),faces[2]);
		Pfm::CopySubImageTo(tmpImg,cvRect(cx*2,H-cy,cx,cy),faces[3]);
		Pfm::CopySubImageTo(tmpImg,cvRect(cx,H-cy*2,cx,cy),faces[4]);
		Pfm::CopySubImageTo(tmpImg,cvRect(cx,H-cy*3,cx,cy),faces[5]);

		for(int i=0;i<6;i++){
			cvFlip(faces[i],NULL,0);
		}

		_CheckDump("[CubeMap] Load PFM File " << fn << " Complete !\n");

		cvReleaseImage(&tmpImg);
		return true;
		
	}

	bool CubeMap::InitForRendering()
	{
		if(faces[0]==NULL)
			return false;

		if(!m_bAllocateTexture)
		{
			glGenTextures(6,m_iTexture);
			m_bAllocateTexture = true;
		}

		glPushAttrib(GL_ALL_ATTRIB_BITS);
		glEnable(GL_TEXTURE_2D);   
		for(int i=0;i<6;i++)
		{
			glPixelStorei(GL_UNPACK_ALIGNMENT, 1);
			glTexEnvf(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_REPLACE);

			glBindTexture(GL_TEXTURE_2D,m_iTexture[i]);
			glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_MAG_FILTER,GL_LINEAR);
			glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_MIN_FILTER,GL_LINEAR);
			glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP);
			glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP);
			glTexImage2D(GL_TEXTURE_2D,0,3,faces[i]->width,faces[i]->height,1,GL_RGB,GL_FLOAT,
				faces[i]->imageData);
		}
		glDisable(GL_TEXTURE_2D);
		glPopAttrib();

		//initPointLights();

		return true;
	}

	void CubeMap::initPointLights(){
		using namespace num;
		float size = 10.0f;
		Vec3f vVertex[8];
		vVertex[0] = Vec3f( size, size, size);
		vVertex[1] = Vec3f(-size, size, size);
		vVertex[2] = Vec3f( size,-size, size);
		vVertex[3] = Vec3f(-size,-size, size);
		vVertex[4] = Vec3f( size, size,-size);
		vVertex[5] = Vec3f(-size, size,-size);
		vVertex[6] = Vec3f( size,-size,-size);
		vVertex[7] = Vec3f(-size,-size,-size);

		int iVertexIndex[][4] = { 
			{2,6,7,3},
			{2,0,4,6},
			{6,4,5,7},
			{7,5,1,3},
			{4,0,1,5},
			{0,2,3,1}
		};

		int index = 0;
		for(int i=0;i<6;i++){
			Vec3f startPoint = vVertex[ iVertexIndex[i][0] ];
			Vec3f endPointx = vVertex[ iVertexIndex[i][3] ];
			Vec3f endPointy = vVertex[ iVertexIndex[i][1] ];
			Vec3f sdist_x = Vec3f((endPointx.x-startPoint.x)/32,(endPointx.y-startPoint.y)/32,(endPointx.z-startPoint.z)/32);
			Vec3f sdist_y = Vec3f((endPointy.x-startPoint.x)/32,(endPointy.y-startPoint.y)/32,(endPointy.z-startPoint.z)/32);
			for(int j = 0;j<16;j++){
				for(int k = 0;k<16;k++){
					lights[index].center = Vec3f(-startPoint.x-(2*j+1)*sdist_x.x-(2*k+1)*sdist_y.x,
												-startPoint.y-(2*j+1)*sdist_x.y-(2*k+1)*sdist_y.y,
												-startPoint.z-(2*j+1)*sdist_x.z-(2*k+1)*sdist_y.z);
					//lights[index].center.Normalize();
					int row = faces[i]->width/32*(2*k+1);
					int colume = faces[i]->height/32*(2*j+1);
					float sumR=0, sumG=0, sumB=0, sum=0;
					for(int m = row-faces[i]->width/32+1;m<row+faces[i]->width/32-1;m++){
						for(int n = colume-faces[i]->height/32+1;n<colume+faces[i]->height/32-1;n++){
							sumR += CV_IMAGE_ELEM(faces[i], float, m, n*3);
							sumG += CV_IMAGE_ELEM(faces[i], float, m, n*3+1);
							sumB += CV_IMAGE_ELEM(faces[i], float, m, n*3+2);
							sum += 1;
						}
					}
					lights[index].inten = Vec3f(sumR/sum, sumG/sum, sumB/sum);

					//lights[index].center = Vec3f(-2.341, 7.759, -5.858);
					//lights[index].inten = Vec3f(0.669513, 0.64862, 1.41777);

					index++;
				}
			}
		}
		//image::Save_PFM("show.pfm", faces[0]);
	}

	void CubeMap::renderPointLights(){
	}

	bool CubeMap::DestroyForRendering(){
		if(m_bAllocateTexture){
			glDeleteTextures(6,m_iTexture);
			return true;
		}
		return false;
	}

	bool CubeMap::RenderEnvironment(){
	
		using namespace num;

		if(m_bAllocateTexture){
			
			float size = 10.0f;
			Vec3f vVertex[8];
			vVertex[0] = Vec3f( size, size, size);
			vVertex[1] = Vec3f(-size, size, size);
			vVertex[2] = Vec3f( size,-size, size);
			vVertex[3] = Vec3f(-size,-size, size);
			vVertex[4] = Vec3f( size, size,-size);
			vVertex[5] = Vec3f(-size, size,-size);
			vVertex[6] = Vec3f( size,-size,-size);
			vVertex[7] = Vec3f(-size,-size,-size);

			int iVertexIndex[][4] = { 
				{2,6,7,3},
				{2,0,4,6},
				{6,4,5,7},
				{7,5,1,3},
				{4,0,1,5},
				{0,2,3,1}
			};

			/*glMatrixMode(GL_MODELVIEW);
			for(int i=0;i<6*16*16;i++){
				glPushMatrix();
				glTranslatef(-lights[i].center.x, -lights[i].center.y, -lights[i].center.z);
				glColor3f(lights[i].inten.x, lights[i].inten.y, lights[i].inten.z);
				//glColor3f(1.0, 0, 0);
				glutSolidSphere(0.1, 30,30);
				glPopMatrix();
			}*/

			glPushAttrib(GL_ALL_ATTRIB_BITS);
			glEnable(GL_TEXTURE_2D);
			glEnable(GL_CULL_FACE);
			glCullFace(GL_BACK);
			//glEnable(GL_DEPTH_TEST);
			//glDepthFunc(GL_NEVER);

			glTexEnvf(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_REPLACE);
			for(int i=0;i<6;i++)
			{
				glBindTexture(GL_TEXTURE_2D,m_iTexture[i]);
				glBegin(GL_QUADS);
				glTexCoord2f(0,0);
				glVertex3fv(vVertex[ iVertexIndex[i][0] ].data());
				glTexCoord2f(0,1);
				glVertex3fv(vVertex[ iVertexIndex[i][1] ].data());
				glTexCoord2f(1,1);
				glVertex3fv(vVertex[ iVertexIndex[i][2] ].data());
				glTexCoord2f(1,0);
				glVertex3fv(vVertex[ iVertexIndex[i][3] ].data());
				glEnd();
			}
			//glDisable(GL_TEXTURE_2D);
			glPopAttrib();

			return true;
		}
		return false;
	}
}
