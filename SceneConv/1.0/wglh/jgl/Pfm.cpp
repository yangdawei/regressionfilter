#include "StdAfx.h"
#include "Pfm.h"

namespace jgl{
	Pfm::Pfm(void){
	}

	Pfm::~Pfm(void){
	}

	void Pfm::CopySubImageTo(IplImage* srcImg,CvRect rect, IplImage* & subImage){

		cvSetImageROI(srcImg,rect);
		// sub-image
		//subImage = cvCreateImage( cvSize(rect.width, rect.height), srcImg->depth, srcImg->nChannels );
		cvCopy(srcImg,subImage);
		cvResetImageROI(srcImg); // release image ROI

	}

	bool Pfm::Load_PFM(const char* fn,IplImage* & img){
		
		char Header[513];

		w32::CFile64	file;

		if(file.Open(fn))
		{
			DWORD len = file.Read(Header,512);
			Header[len] = 0;

			if(len>3)
			{
				Header[len-1] = 0;
				if( (Header[0] == 'P' && Header[1] == 'F') || 
					(Header[0] == 'p' && Header[1] == 'f') )
				{
					LPSTR p = strchr(Header,0xa);
					if(!p)return false;

					LPSTR end;

					p++;
					end = strchr(p,0xa);
					if(!end)return false;

					end[0] = 0;

					UINT	cx,cy;
					if(sscanf(p,"%d %d",&cx,&cy) == 2)
					{
						p = &end[1];
						end = strchr(p,0xa);
						
						if(!end)return false;

						
						file.Seek(((UINT)(end-Header))+1);
						rt::Buffer<num::Vec3f> rawData;
						rawData.SetSize(cx*cy);

						//_CheckDump("Loading float-point map "<<cx<<"x"<<cy<<" ...");
						
						if(file.Read(rawData.Begin(),rawData.GetSize()*sizeof(num::Vec3f)) == rawData.GetSize()*sizeof(num::Vec3f))
						{
							img = cvCreateImage(cvSize(cx,cy),IPL_DEPTH_32F,3);
							for(UINT y=0;y<cy;y++){
								char* start = img->imageData + y*img->widthStep;
								memcpy(start,rawData.Begin()+y*cx,cx*sizeof(num::Vec3f));
							}
							return true;
						}
					}
				}
			}
		}

		return false;
	}
	bool Pfm::Save_PFM(const char* fn,IplImage* img){
		
		if(img==NULL || img->depth!=IPL_DEPTH_32F || img->nChannels!=3)
			return false;

		char Header[512];

		int cx = img->width;
		int cy = img->height;
		sprintf(Header,"PF\x0a%d %d\x0a-1.000000\x0a",cx,cy);

		w32::CFile64	file;
		if(file.Open(fn,w32::CFile64::Normal_Write))
		{
			// write header
			if(file.Write(Header,strlen(Header)) == strlen(Header))
			{

				for(int y=0;y<cy;y++){
					char* start = img->imageData + y*img->widthStep;
					if(!file.Write(start,cx*3*sizeof(float)) == cx*3*sizeof(float)){
						return false;
					}
				}
			}
		}

		return true;
	}

	bool Pfm::Save_PFM(const char* fn,const rt::Buffer<num::Vec3f> & data, int dim1,int dim2){

		char Header[512];

		int cx = dim1;
		int cy = dim2;
		sprintf(Header,"PF\x0a%d %d\x0a-1.000000\x0a",cx,cy);

		rt::Buffer<num::Vec3f> data2;
		data2.SetSize(dim1*dim2);
		data2.Set(0);

		data.CopyTo(data2.Begin());

		w32::CFile64	file;
		if(file.Open(fn,w32::CFile64::Normal_Write))
		{
			// write header
			if(file.Write(Header,strlen(Header)) == strlen(Header))
			{
				if(!file.Write(data2.Begin(),data2.GetSize()*sizeof(num::Vec3f)) == data2.GetSize()*sizeof(num::Vec3f)){
					return false;
				}
			}
		}

		return true;
	}

	bool Pfm::Save_PFM(const char* fn,const rt::Buffer<float> & data, int dim1,int dim2){

		rt::Buffer<num::Vec3f> data2;
		data2.SetSize(data.GetSize());
		data2.Set(0);

		for(UINT i=0;i<data.GetSize();i++){
			data2[i] = num::Vec3f(data[i],data[i],data[i]);
		}

		return Save_PFM(fn,data2,dim1,dim2);
	}
}