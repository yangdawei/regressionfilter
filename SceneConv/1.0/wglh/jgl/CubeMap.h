#pragma once

#include <opencv\cv.h>
#include <opencv\cxcore.h>
#include <opencv\highgui.h>

namespace jgl{
	
	class CubeMap
	{
	public:
		CubeMap(void);
		~CubeMap(void);

	public:
		//void testSaveFaces();
		bool Load_PFM(LPCTSTR fn);
		bool Save_PFM(LPCTSTR fn);

		//Rendering Related
	public:
		bool InitForRendering();
		bool DestroyForRendering();
		bool RenderEnvironment();

	protected:
		rt::Buffer<IplImage*> faces;
		void releaseImages();

		GLuint  m_iTexture[6];

	public:
		bool m_bAllocateTexture;
		typedef struct pointLight{
			num::Vec3f center;
			num::Vec3f inten;
		} pLight;

		pLight lights[6*16*16];
		void initPointLights();
		void renderPointLights();
	};
}

