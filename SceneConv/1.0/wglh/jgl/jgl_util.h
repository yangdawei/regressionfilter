#pragma once
#include <opencv\cv.h>
#include <opencv\cxcore.h>
#include <opencv\highgui.h>

#include "..\inc\glut.h"

namespace jgl{

	namespace util
	{
		void saveScreen(const string& fileName,int w,int h);
		void saveTexture32f(GLuint pnter, const string& PfmName, int width = 0, int height = 0);
		IplImage* getTexture32f(GLuint texture, int width = 0, int height = 0);
		IplImage* loadPNG4C(const string& filename);
		IplImage* loadImage(const string& filename);
		IplImage* loadTGA(const string& filename);

		void drawString2D(std::string str, int x, int y,const num::Vec4f& color= num::Vec4f(1,1,1,1), void *font = GLUT_BITMAP_8_BY_13);

	}

}