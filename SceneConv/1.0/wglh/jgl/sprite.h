#pragma once
#include <string>
#include <vector>
#include <boost/signal.hpp>

#include "jgl_util.h"


/*
JSpriteContainer::setResourcePath() must be called before using any JGLSprite class
*/

namespace jgl{

	class JGLSprite{
	public:
		JGLSprite(void){}
		virtual ~JGLSprite(void){}
	public:
		virtual void draw() = 0;
		virtual bool isChoosed(int x, int y) = 0;
		virtual bool handleMouseClickEvent(int button, int action, int x, int y) = 0;
		virtual bool handleMouseMotionEvent(int x, int y) = 0;
	};

	class JGLImage : public JGLSprite {
	protected:
		int xPos, yPos;
		int width, height;
		IplImage* img;
	public:
		JGLImage(int xPos, int yPos, int width, int height, const std::string& imgName, float opacity = 1);
		~JGLImage(void);
		void draw();
		void setImage(const std::string& imgName);
		void setPos(int x, int y);
		bool isChoosed(int x, int y);
		bool handleMouseClickEvent(int button, int action, int x, int y);
		bool handleMouseMotionEvent(int x, int y); 
	};

	class JGLLabel : public JGLSprite {
	protected:
		int xPos, yPos;
		std::string name;
	public:
		JGLLabel(int xPos, int yPos, const std::string& str);
		~JGLLabel(void);
		void draw();
		void setPos(int x, int y);
		void setText(string str);
		string getText();
		bool isChoosed(int x, int y); 
		bool handleMouseClickEvent(int button, int action, int x, int y);
		bool handleMouseMotionEvent(int x, int y); 
	};

	class JGLButton : public JGLSprite {
		int xPos, yPos;
		int width, height;
		std::string name;
		bool choosed;
		boost::signal<void()> sig;
	public:
		JGLButton(int xPos, int yPos, int width, int height, const std::string& str);
		~JGLButton(void);
		void draw();
		void setPos(int x, int y);
		bool isChoosed(int x, int y); 
		bool handleMouseClickEvent(int button, int action, int x, int y);
		bool handleMouseMotionEvent(int x, int y); 
		void addClickListener(const boost::signal<void()>::slot_type & slot) {
			sig.connect(slot);
		}
	};

	class JGLSeekBar : public JGLSprite {
		float maxValue, minValue, value;
		int xPos, yPos;
		int lengthInPixel;
		bool choosed;
		string name;
		bool highlighted;
		float* bindData;
		boost::signal<void(float)> sig;
	public:
		JGLSeekBar(float* bindData, float minValue, float maxValue, int xPos, int yPos, std::string str, int lengthInPixel = 100);
		~JGLSeekBar(void);

		float getMinValue() const { return minValue;}
		float getMaxValue() const { return maxValue;}

		float getValue() const;
		void setValue(float value);

		void setHighLighted(bool b);
		void draw();
		bool isChoosed(int x, int y); 
		bool handleMouseClickEvent(int button, int action, int x, int y);
		bool handleMouseMotionEvent(int x, int y);

		void addListener(const boost::signal<void(float)>::slot_type & slot) {
			sig.connect(slot);
		}
	};

	class JGLCheckBox: public JGLSprite {
		bool selected;
		int xPos, yPos;
		std::string name;
		bool* bindData;
		boost::signal<void(bool)> sig;
	public:
		JGLCheckBox(bool* bindData, int xPos, int yPos, std::string name);
		~JGLCheckBox(void);
		bool isSelected();
		void setSelected(bool s);
		void draw();
		bool isChoosed(int x, int y);
		bool handleMouseClickEvent(int button, int action, int x, int y);
		bool handleMouseMotionEvent(int x, int y);

		void addListener(const boost::signal<void(bool)>::slot_type & slot) {
			sig.connect(slot);
		}
	};

	class JGLCombo: public JGLSprite {
		int xPos, yPos;
		int width, height;
		vector<string> data;		
		bool dropped;
		bool choosed;
		int mousecovered;
		string value;

		int selectedIndex;
		boost::signal<void(int)> sig;
	public:
		JGLCombo(int xPos, int yPos, int width, int height, vector<string>& data);
		~JGLCombo(void);
		void draw();
		bool setSelect(int index);
		int getSelect();
		bool setData(vector<string>& data);
		bool isChoosed(int x, int y);
		bool handleMouseClickEvent(int button, int action, int x, int y);
		bool handleMouseMotionEvent(int x, int y);

		void addSelectionChangedListener(const boost::signal<void(int)>::slot_type & slot) {
			sig.connect(slot);
		}
	};



	class JSpriteContainer
	{
	protected:
		std::vector<JGLSprite*> sprites;
		JGLImage* bgSprite;
		bool bUseBg;
		num::Vec4i panelPort;

		JGLSprite* getMouseClickSprite(int x,int y);
		JGLSprite* pMouseDownSprite;

	public:
		JSpriteContainer(int x,int y,int width,int height,bool bHaveBackground=true);
		virtual ~JSpriteContainer();

		void addSprite(JGLSprite* sprite) {
			sprites.push_back(sprite);
		}
		void removeSprite(JGLSprite* sprite){
			for(std::vector<JGLSprite*>::iterator it = sprites.begin();it!=sprites.end();it++){
				if(*it == sprite){
					sprites.erase(it);
					break;
				}
			}
		}
		void setPanelPosAndSize(int x,int y,int width,int height);
		num::Vec4i getPanelPosAndSize() const{ return panelPort;}

		bool MouseDown(int x,int y);
		bool MouseMove(int x,int y);
		bool MouseUp(int x,int y);
		void draw();

	public:
		static std::string getResourcePath();		
		static bool setResourcePath(const std::string& path);

	protected:
		static std::string srcPath;
	};
}