#include "StdAfx.h"
#include "jgl_util.h"
#include "Pfm.h"
#include <assert.h>

#ifdef DEBUG
#pragma comment(lib, "opencv_imgproc220d.lib")
#pragma comment(lib, "opencv_highgui220d.lib")
#pragma comment(lib, "opencv_core220d.lib")
#else
#pragma comment(lib, "opencv_imgproc220.lib")
#pragma comment(lib, "opencv_highgui220.lib")
#pragma comment(lib, "opencv_core220.lib")
#endif


using namespace std;

namespace jgl {

	namespace util{

	void saveScreen(const string& fileName,int w,int h){
		assert(w>0&&h>0);

		IplImage* screenSave = cvCreateImage(cvSize(w,h),8,3);
		GLubyte* temp = new GLubyte[w*h*3];
		glPixelStorei(GL_UNPACK_ALIGNMENT,1);
		glReadPixels(0,0,w,h,GL_BGR_EXT,GL_UNSIGNED_BYTE,temp);
		memcpy(screenSave->imageData, temp, w*h*3);
		cvConvertImage(screenSave,screenSave,CV_CVTIMG_FLIP);
		cvSaveImage(fileName.c_str(), screenSave);
		delete [] temp;
		cvReleaseImage(&screenSave);
	}
	void saveTexture32f(GLuint pnter,const string& PfmName, int width, int height){
		assert(width>0&&height>0);
		//if(width == 0) width = getW();
		//if(height == 0) height = getH();
		IplImage* img = getTexture32f(pnter, width, height);
		Pfm::Save_PFM(PfmName.c_str(), img);
		cvReleaseImage(&img);
	}
	IplImage* getTexture32f(GLuint texture, int width, int height){
		
		//if(width == 0) width = getW();
		//if(height == 0) height = getH();

		glPixelStorei(GL_UNPACK_ALIGNMENT,1);
		IplImage * img = cvCreateImage(cvSize(width,height),IPL_DEPTH_32F,3);
		glEnable(GL_TEXTURE_2D);
		glBindTexture(GL_TEXTURE_2D,texture);		
		glGetTexImage(GL_TEXTURE_2D,0,GL_RGB,GL_FLOAT,img->imageData);
		return img;
	}


	bool gdiInit = false;

	IplImage* loadPNG4C(const string& file){
		if(!gdiInit){
			Gdiplus::GdiplusStartupInput gdiplusStartupInput;
			ULONG_PTR gdiplusToken;
			GdiplusStartup(&gdiplusToken, &gdiplusStartupInput, NULL);
			gdiInit = true;
		}
		WCHAR *filename = new wchar_t[file.size()*2+1]; 
		swprintf(filename, L"%S ", file.c_str()); 
		Gdiplus::Bitmap destBmp(filename,TRUE);
		delete [] filename;

		Gdiplus::Rect rect1(0, 0, destBmp.GetWidth(), destBmp.GetHeight());
		Gdiplus::BitmapData bitmapData;
		memset(&bitmapData, 0, sizeof(bitmapData));
		destBmp.LockBits(&rect1, Gdiplus::ImageLockModeRead, PixelFormat32bppARGB, &bitmapData);
		int nStride1 = bitmapData.Stride;
		if( nStride1 < 0 )
			nStride1 = -nStride1;
		byte* DestPixels = (byte*)bitmapData.Scan0;

		IplImage *img = cvCreateImage(cvSize(destBmp.GetWidth(), destBmp.GetHeight()), 8, 4);
		for(int i = 0;i<img->height;i++){
			for(int j = 0;j<img->width;j++){
				CV_IMAGE_ELEM(img, byte, i, 4*j) = DestPixels[(img->height-1-i)*nStride1 + j*4+2];
				CV_IMAGE_ELEM(img, byte, i, 4*j+1) = DestPixels[(img->height-1-i)*nStride1 + j*4+1];
				CV_IMAGE_ELEM(img, byte, i, 4*j+2) = DestPixels[(img->height-1-i)*nStride1 + j*4];
				CV_IMAGE_ELEM(img, byte, i, 4*j+3) = DestPixels[(img->height-1-i)*nStride1 + j*4+3];
			}
		}
		destBmp.UnlockBits(&bitmapData);
		return img;
	}
	IplImage* loadImage(const string& file){
		string fileType = file.substr(file.find_last_of(".")+1, file.length()-file.find_last_of(".")-1);
		if(fileType == "tga" || fileType == "TGA") return loadTGA(file);
		IplImage* img = cvLoadImage(file.c_str(), -1);
		return img;
	}
	IplImage* loadTGA(const string& file){ 
		FILE *pfile; unsigned char bitCount; int colorMode;
		long tgaSize; unsigned char unCompressHeader[12] = {0, 0, 2, 0, 0, 0, 0, 0, 0, 0, 0, 0};
		unsigned char tgaHeader[12]; unsigned char header[6];
		if(file == "") return NULL;

		pfile = fopen(file.c_str(), "rb");
		if(!pfile) return NULL;
		fread(tgaHeader, 1, sizeof(tgaHeader), pfile);
		if(memcmp(unCompressHeader, tgaHeader, sizeof(unCompressHeader)) != 0){
			fclose(pfile);
			return NULL;
		}
		fread(header, 1, sizeof(header), pfile);
		int imageWidth = header[1] * 256 + header[0];    
		int imageHeight = header[3] * 256 + header[2];
		bitCount = header[4];
		colorMode = bitCount / 8;
		IplImage* result = cvCreateImage(cvSize(imageWidth, imageHeight), 8, colorMode);
		tgaSize = imageWidth * imageHeight * colorMode;
		unsigned char* image = new unsigned char[sizeof(unsigned char) * tgaSize];
		fread(image, sizeof(unsigned char), tgaSize, pfile);
		for(long index = 0; index < tgaSize; index += colorMode){
			unsigned char tempColor = image[index];
			image[index] = image[index + 2];
			image[index + 2] = tempColor;
		}
		memcpy(result->imageData, (const char*)image, tgaSize);
		fclose(pfile);
		delete [] image;
		return result;
	}

	void drawString2D(std::string str, int x, int y,const num::Vec4f& color, void *font){
		glPushAttrib(GL_ALL_ATTRIB_BITS);
		glDisable(GL_TEXTURE_2D);
		glColor4f(color[0], color[1], color[2], color[3]);
		glRasterPos2i(x, y);
		glPixelStorei(GL_UNPACK_ALIGNMENT,1);
		char* ch = (char*)str.c_str();
		while(*ch){
			if(*ch>=0) glutBitmapCharacter(font, *ch);
			//else{
			//	open_hzk();
			//	GLubyte bytes[32];
			//	get_hz(ch,bytes);
			//	for(int i=0;i<8;i++){
			//		___swap(bytes[i*2],bytes[(15-i)*2]);
			//		___swap(bytes[i*2+1],bytes[(15-i)*2+1]);
			//	}
			//	glBitmap(16,16,0,4,16,0,bytes);
			//	if(++ch==0)break;
			//}
			++ch;
		}
		glEnable(GL_TEXTURE_2D);
		glPopAttrib();
	}
}
}