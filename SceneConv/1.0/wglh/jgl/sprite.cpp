#include "StdAfx.h"
#include "sprite.h"

namespace jgl{
	
	//string JSpriteContainer::getResourcePath();

	//JGLImage
	JGLImage::JGLImage(int xPos, int yPos, int width, int height, const std::string& imgName, float opacity):xPos(xPos), yPos(yPos), width(width), height(height){
		img = 0;
		setImage(imgName);
	}
	JGLImage::~JGLImage(void){
		if(img)
			cvReleaseImage(&img);
	}
	void JGLImage::setImage(const std::string& imgName){
		if(img){
			cvReleaseImage(&img); img = 0;
		}
		string fileType = imgName.substr(imgName.find_last_of(".")+1, imgName.length()-imgName.find_last_of(".")-1);
		if(fileType == "png" || fileType == "PNG") img = jgl::util::loadPNG4C(imgName);
		else{
			img = jgl::util::loadImage(imgName);
			if(!img) return;
			cvFlip(img, NULL, 0);
			cvConvertImage(img, img, CV_CVTIMG_SWAP_RB);
		}
	}
	void JGLImage::draw(){
		if(!img) return;
		glRasterPos2i(xPos, yPos); 
		glPixelZoom(((float)width)/img->width, (float)height/img->height);
		if(img->nChannels == 4) glDrawPixels(img->width, img->height, GL_RGBA, GL_UNSIGNED_BYTE, img->imageData);
		else if(img->nChannels == 3) glDrawPixels(img->width, img->height, GL_RGB, GL_UNSIGNED_BYTE, img->imageData);
	}
	bool JGLImage::isChoosed(int x, int y){return (x-xPos>=0 && x-xPos <= width && y-yPos >=0 && y-yPos <= height);}
	void JGLImage::setPos(int x, int y){
			xPos = x; yPos = y;
	}
	bool JGLImage::handleMouseClickEvent(int button, int action, int x, int y){return false;};
	bool JGLImage::handleMouseMotionEvent(int x, int y){return false;}; 

	//JGLLabel
	JGLLabel::JGLLabel(int xPos, int yPos, const std::string& str):xPos(xPos), yPos(yPos), name(str){}
	JGLLabel::~JGLLabel(void){}
	void JGLLabel::draw(){
		util::drawString2D(name, xPos, yPos);
	}
	void JGLLabel::setText(string str){name = str;}
	string JGLLabel::getText(){return name;}
	void JGLLabel::setPos(int x, int y){xPos = x; yPos = y;}
	bool JGLLabel::isChoosed(int x, int y){
		return (x-xPos>=0 && x-xPos <= glutBitmapLength(GLUT_BITMAP_8_BY_13, (const unsigned char*)name.c_str()) && y-yPos >=0 && y-yPos <= 15);
	}
	bool JGLLabel::handleMouseClickEvent(int button, int action, int x, int y){return false;}
	bool JGLLabel::handleMouseMotionEvent(int x, int y){return false;}

	//JGLButton
	IplImage* button1 = 0, *button2 = 0;
	JGLButton::JGLButton(int xPos, int yPos, int width, int height, const std::string& str):xPos(xPos), yPos(yPos), width(width), height(height), name(str){
		if(button1 == 0) button1 = jgl::util::loadPNG4C(JSpriteContainer::getResourcePath()+"button1.png");
		if(button2 == 0) button2 = jgl::util::loadPNG4C(JSpriteContainer::getResourcePath()+"button2.png");
		choosed = false;
	}
	JGLButton::~JGLButton(void){}
	void JGLButton::draw(){
		glRasterPos2i(xPos, yPos);
		if(!choosed){
			glPixelZoom(((float)width)/button1->width, ((float)height)/button1->height);
			glDrawPixels(button1->width, button1->height, GL_RGBA, GL_UNSIGNED_BYTE, button1->imageData);
		}else{
			glPixelZoom(((float)width)/button2->width, ((float)height)/button2->height);
			glDrawPixels(button2->width, button2->height, GL_RGBA, GL_UNSIGNED_BYTE, button2->imageData);
		}
		util::drawString2D(name, xPos+width/2-glutBitmapLength(GLUT_BITMAP_8_BY_13, (const unsigned char*)name.c_str())/2, yPos+height/2-4,num::Vec4f(0,0,0,1));
	}
	void JGLButton::setPos(int x, int y){xPos = x; yPos = y;}
	bool JGLButton::isChoosed(int x, int y){return (x-xPos>=0 && x-xPos <= width && y-yPos >=0 && y-yPos <= height);}
	bool JGLButton::handleMouseClickEvent(int button, int action, int x, int y){
		if(button == GLUT_LEFT_BUTTON){
			if(action == GLUT_DOWN)
				choosed = true;
			else if(action == GLUT_UP){
				choosed = false;
				sig();
			}
		}
		return false;
	}
	bool JGLButton::handleMouseMotionEvent(int x, int y){return false;}

	//JGLSeekbar
	IplImage* seek1 = 0, *seek2 = 0, *seeker = 0, *seeker2 = 0;
	JGLSeekBar::JGLSeekBar(float* bindData, float minValue, float maxValue, int xPos, int yPos, std::string name, int lengthInPixel){
		this->bindData = bindData;
		this->maxValue = maxValue; this->minValue = minValue; this->value = *bindData; this->lengthInPixel = lengthInPixel;
		this->xPos = xPos; this->yPos = yPos; this->name = name;
		choosed = false; this->highlighted = false;
		if(seek1 == 0) seek1 = jgl::util::loadPNG4C(JSpriteContainer::getResourcePath()+"seekbar1.png");
		if(seek2 == 0) seek2 = jgl::util::loadPNG4C(JSpriteContainer::getResourcePath()+"seekbar2.png");
		if(seeker == 0) seeker = jgl::util::loadPNG4C(JSpriteContainer::getResourcePath()+"seekbar3.png");
		if(seeker2 == 0) seeker2 = jgl::util::loadPNG4C(JSpriteContainer::getResourcePath()+"seekbar4.png");
	}
	JGLSeekBar::~JGLSeekBar(void){}
	float JGLSeekBar::getValue()const{return value;}
	void JGLSeekBar::setHighLighted(bool b){highlighted = b;}
	void JGLSeekBar::setValue(float value_){
		if(minValue < maxValue){
			if(value_ < minValue) value_ = minValue;
			if(value_ > maxValue) value_ = maxValue;
		}else{
			if(value_ > minValue) value_ = minValue;
			if(value_ < maxValue) value_ = maxValue;
		}
		value = value_;
		*bindData = value;
	}
	void JGLSeekBar::draw(){
		util::drawString2D(name, xPos-10-glutBitmapLength(GLUT_BITMAP_8_BY_13, (const unsigned char*)name.c_str()), yPos+seeker->height/2-4, 
			highlighted? num::Vec4f(1,1,0,1): num::Vec4f(1,1,1,1));
		glRasterPos2i(xPos, yPos); 
		if(!highlighted){
			glPixelZoom(((float)lengthInPixel)/seek1->width, 1.0f);
			glDrawPixels(seek1->width, seek1->height, GL_RGBA, GL_UNSIGNED_BYTE, seek1->imageData);
		}else{
			glPixelZoom(((float)lengthInPixel)/seek2->width, 1.0f);
			glDrawPixels(seek2->width, seek2->height, GL_RGBA, GL_UNSIGNED_BYTE, seek2->imageData);
		}
		glPixelZoom(1.0f, 1.0f);
		glRasterPos2i(xPos+(float)lengthInPixel*(value-minValue)/(maxValue-minValue)-seeker->width/2, yPos); 
		if(!highlighted)
			glDrawPixels(seeker->width, seeker->height, GL_RGBA, GL_UNSIGNED_BYTE, seeker->imageData);
		else
			glDrawPixels(seeker2->width, seeker2->height, GL_RGBA, GL_UNSIGNED_BYTE, seeker2->imageData);
		char* valueStr= new char[10];
		sprintf(valueStr, "%.3f", value);
		util::drawString2D(valueStr, xPos+lengthInPixel+10, yPos+seeker->height/2-4, 
			highlighted? num::Vec4f(1,1,0,1): num::Vec4f(1,1,1,1));
		delete valueStr;
	}
	bool JGLSeekBar::isChoosed(int x, int y){return (x-xPos>=-seeker->width/2 && x-xPos <= lengthInPixel+seeker->width/2 && y-yPos >=0 && y-yPos <= seeker->height);}
	bool JGLSeekBar::handleMouseMotionEvent(int x, int y){
		if(choosed){
			float ratio = ((float)(x-xPos))/(float)lengthInPixel;
			if(ratio<0) ratio = 0;
			if(ratio>1) ratio = 1;
			value = minValue+ratio*(maxValue-minValue);
			*bindData = value;
			
			sig(*bindData);

			return true;
		}
		return false;
	}
	bool JGLSeekBar::handleMouseClickEvent(int button, int action, int x, int y){
		if(button == GLUT_LEFT_BUTTON){
			if(action == GLUT_DOWN){
				if(x-xPos>=-seeker->width/2 && x-xPos <= lengthInPixel+seeker->width/2 && y-yPos >=0 && y-yPos <= seeker->height){
					float ratio = ((float)(x-xPos))/(float)lengthInPixel;
					if(ratio<0) ratio = 0;
					if(ratio>1) ratio = 1;
					value = minValue+ratio*(maxValue-minValue);
					*bindData = value;
					sig(*bindData);
					choosed = true;
					return true;
				}
			}else if(action == GLUT_UP){
				if(choosed){
					float ratio = ((float)(x-xPos))/(float)lengthInPixel;
					if(ratio<0) ratio = 0;
					if(ratio>1) ratio = 1;
					value = minValue+ratio*(maxValue-minValue);
					*bindData = value;
					sig(*bindData);
					choosed = false;
					return true;
				}
			}
		}else
			return false;
		return false;
	}

	//checkbox
	IplImage* checkImg1 = 0, *checkImg2 = 0;
	JGLCheckBox::JGLCheckBox(bool* bindData, int xPos, int yPos, std::string name){
		this->xPos = xPos; this->yPos = yPos; this->bindData = bindData;
		this->name = name; if(bindData) this->selected = *bindData;else  this->selected = false;
		if(checkImg1 == 0) checkImg1 = jgl::util::loadPNG4C(JSpriteContainer::getResourcePath()+"checkbox1.png");
		if(checkImg2 == 0) checkImg2 = jgl::util::loadPNG4C(JSpriteContainer::getResourcePath()+"checkbox2.png");
	}
	JGLCheckBox::~JGLCheckBox(void){}
	bool JGLCheckBox::isSelected(){return selected;}
	void JGLCheckBox::setSelected(bool s){selected = s; if(bindData) *bindData = selected;};
	void JGLCheckBox::draw(){
		glRasterPos2i(xPos, yPos); 
		glPixelZoom(1.0f, 1.0f);
		if(selected)
			glDrawPixels(checkImg2->width, checkImg2->height, GL_RGBA, GL_UNSIGNED_BYTE, checkImg2->imageData);
		else
			glDrawPixels(checkImg1->width, checkImg1->height, GL_RGBA, GL_UNSIGNED_BYTE, checkImg1->imageData);
		util::drawString2D(name, xPos+checkImg1->width+10, yPos+checkImg1->height/2-4);
	}
	bool JGLCheckBox::isChoosed(int x, int y){return (x-xPos>=0 && x-xPos <= checkImg1->width && y-yPos >=0 && y-yPos <= checkImg1->height);}
	bool JGLCheckBox::handleMouseClickEvent(int button, int action, int x, int y){
		if(button == GLUT_LEFT_BUTTON){
			if(action == GLUT_DOWN){
				if(x-xPos>=0 && x-xPos <= checkImg1->width && y-yPos >=0 && y-yPos <= checkImg1->height){
					this->selected = !(this->selected);
					if(bindData) *bindData = selected;
					sig(selected);
					return true;
				}
			}
		}
		return false;
	}
	bool JGLCheckBox::handleMouseMotionEvent(int x, int y){return false;}

	//JGLCombo
	int rowNum = 8;
	IplImage* combo1, *combo2, *combo3, *combo4;
	JGLCombo::JGLCombo(int xPos, int yPos, int width, int height, vector<string>& data):xPos(xPos), yPos(yPos), width(width), height(height){

		setData(data);
		if(combo1 == 0) combo1 = jgl::util::loadPNG4C(JSpriteContainer::getResourcePath()+"combo1.png");
		if(combo2 == 0) combo2 = jgl::util::loadPNG4C(JSpriteContainer::getResourcePath()+"combo2.png");
		if(combo3 == 0) combo3 = jgl::util::loadPNG4C(JSpriteContainer::getResourcePath()+"combo3.png");
		if(combo4 == 0) combo4 = jgl::util::loadPNG4C(JSpriteContainer::getResourcePath()+"combo4.png");

	}
	JGLCombo::~JGLCombo(void){}
	void JGLCombo::draw(){
		glRasterPos2i(xPos, yPos);
		if(!dropped){
			glPixelZoom(((float)width)/combo1->width, ((float)height)/combo1->height);
			if(!choosed)
				glDrawPixels(combo1->width, combo1->height, GL_RGBA, GL_UNSIGNED_BYTE, combo1->imageData);
			else
				glDrawPixels(combo2->width, combo2->height, GL_RGBA, GL_UNSIGNED_BYTE, combo2->imageData);
			util::drawString2D(value, xPos+width/2-glutBitmapLength(GLUT_BITMAP_8_BY_13, (const unsigned char*)value.c_str())/2-4, yPos+height/2-4, num::Vec4f(0,0,0,1.0));
		}else{
			glPixelZoom(((float)width)/combo1->width, ((float)height)/combo1->height);
			glDrawPixels(combo2->width, combo2->height, GL_RGBA, GL_UNSIGNED_BYTE, combo2->imageData);
			util::drawString2D(value, xPos+width/2-glutBitmapLength(GLUT_BITMAP_8_BY_13, (const unsigned char*)value.c_str())/2-4, yPos+height/2-4, num::Vec4f(0,0,0,1.0));
			for(int i = 0;i<data.size();i++){
				glRasterPos2i(xPos+i/rowNum*width, yPos-(i%rowNum+1)*height);
				glPixelZoom(((float)width)/combo3->width, ((float)height)/combo3->height);
				if(i != mousecovered) glDrawPixels(combo3->width, combo3->height, GL_RGBA, GL_UNSIGNED_BYTE, combo3->imageData);
				else glDrawPixels(combo4->width, combo4->height, GL_RGBA, GL_UNSIGNED_BYTE, combo4->imageData);
				util::drawString2D(data[i], xPos+i/rowNum*width+width/2-glutBitmapLength(GLUT_BITMAP_8_BY_13, (const unsigned char*)data[i].c_str())/2-4, yPos-(i%rowNum+1)*height+height/2-4, num::Vec4f(0,0,0,1));
			}
		}
	}
	bool JGLCombo::isChoosed(int x, int y){
		if(!dropped) return (x-xPos>=0 && x-xPos <= width && y-yPos >=0 && y-yPos <= height);
		else{
			for(int i = 0;i<data.size();i++){
				int oriX = xPos+i/rowNum*width, oriY = yPos-(i%rowNum+1)*height;
				if((x>=oriX && x <= oriX+width && y >=oriY && y <= oriY+height)) return true;
			}
			return (x-xPos>=0 && x-xPos <= width && y-yPos >=0 && y-yPos <= height);
		}
	}
	bool JGLCombo::handleMouseClickEvent(int button, int action, int x, int y){
		if(button == GLUT_LEFT_BUTTON){
			if(action == GLUT_DOWN){
				if(!dropped) dropped = true;
				else{
					if(y >= yPos){
						dropped = false;
						return true;
					}
					int index = (yPos-y)/height+(x-xPos)/width*rowNum;
					if(index >= 0 && index < data.size()){
						selectedIndex = index;
						value = data[index];
						dropped = false;
						sig(selectedIndex);
					}
				}
			}
			return true;
		}
		return false;
	}
	bool JGLCombo::handleMouseMotionEvent(int x, int y){
		if(yPos > y) mousecovered = (yPos-y)/height+(x-xPos)/width*rowNum;
		else mousecovered = -1;
		return true;
	}
	bool JGLCombo::setSelect(int index){
		if(index < 0) index = 0;
		if(index >= data.size()) index = data.size()-1;
		selectedIndex = index;
		value = data[index];
		return true;
	}
	int JGLCombo::getSelect(){ return selectedIndex;}
	bool JGLCombo::setData(vector<string>& data){
		this->data = vector<string>(data);
		dropped = false;
		choosed = false;
		mousecovered = -1;
		if(data.size() >= 1){
			selectedIndex = 0;
			value = data[0];
		}
		return true;
	}

	string JSpriteContainer::srcPath;

	string JSpriteContainer::getResourcePath() {
		return JSpriteContainer::srcPath;
	}
	bool JSpriteContainer::setResourcePath(const std::string& path){
		if(path.length()==0)
			return false;
		if(path[path.length()-1]=='\\' || path[path.length()-1]=='/') {
			JSpriteContainer::srcPath = path;
			return true;
		} else {
			if(path.find('\\')!=std::string::npos){
				JSpriteContainer::srcPath = path + "\\";
			} else if(path.find('/')!=std::string::npos){
				JSpriteContainer::srcPath = path + "/";
			} else
				return false;
			return true;
		}
	}

	JSpriteContainer::JSpriteContainer(int x,int y,int width,int height,bool bHaveBackground){				
		bgSprite = NULL;
		bUseBg = bHaveBackground;
		setPanelPosAndSize(x,y,width,height);

		pMouseDownSprite = NULL;
	}
	JSpriteContainer::~JSpriteContainer(){
		if(bgSprite!=NULL) delete bgSprite;
		for(int i = (int)sprites.size()-1;i>=0;i--){
			if(sprites[i]!=NULL)
				delete sprites[i];
		}
	}

	void JSpriteContainer::setPanelPosAndSize(int x,int y,int width,int height){
		if(width!=panelPort[2] || height!=panelPort[3]) {
			if(bUseBg){
				if(bgSprite!=NULL) delete bgSprite;
				bgSprite = new JGLImage(0,0,width,height,JSpriteContainer::getResourcePath()+"bg.png",0.5f);
			}
		}
		panelPort = num::Vec4i(x,y,width,height);
	}

	JGLSprite* JSpriteContainer::getMouseClickSprite(int x,int y){
		for(int i = (int)sprites.size()-1;i>=0;i--){
			if(sprites[i]->isChoosed(x,y)){
				return sprites[i];
			}
		}
		return NULL;
	}

	bool JSpriteContainer::MouseDown(int x,int y){
		if(x>=panelPort[0] && y >= panelPort[1] && x< panelPort[0]+panelPort[2] && y < panelPort[1] + panelPort[3]) {
			JGLSprite* sprite = getMouseClickSprite(x - panelPort[0],y - panelPort[1]);
			if(sprite!=NULL){
				pMouseDownSprite = sprite;
				pMouseDownSprite->handleMouseClickEvent(GLUT_LEFT_BUTTON, GLUT_DOWN,x - panelPort[0],y - panelPort[1]);
				return true;
			}			
		}
		return false;
	}

	bool JSpriteContainer::MouseMove(int x,int y){

		if(pMouseDownSprite){
			pMouseDownSprite->handleMouseMotionEvent(x - panelPort[0],y - panelPort[1]);
			return true;
		} 
		//else if(x>=panelPort[0] && y >= panelPort[1] && x< panelPort[0]+panelPort[2] && y < panelPort[1] + panelPort[3]) {
		//	JGLSprite* sprite = getMouseClickSprite(x - panelPort[0],y - panelPort[1]);
		//	if(sprite)
		//		sprite->handleMouseMotionEvent(x - panelPort[0],y - panelPort[1]);
		//}

		return false;
	}

	bool JSpriteContainer::MouseUp(int x,int y){
		if(pMouseDownSprite){
			pMouseDownSprite->handleMouseClickEvent(GLUT_LEFT_BUTTON, GLUT_UP,x - panelPort[0],y - panelPort[1]);
			pMouseDownSprite = NULL;
			return true;
		}
		return false;
	}

	void JSpriteContainer::draw(){
		
		glPushAttrib(GL_ALL_ATTRIB_BITS);
		glViewport(panelPort[0],panelPort[1],panelPort[2],panelPort[3]);
		glMatrixMode(GL_MODELVIEW);
		glPushMatrix();
		glLoadIdentity();
		glMatrixMode(GL_PROJECTION);
		glPushMatrix();
		glLoadIdentity();		
		glOrtho(0,panelPort[2],0,panelPort[3],-1,10);
		glDisable(GL_LIGHTING);
		glDisable(GL_TEXTURE_2D);
		glDisable(GL_DEPTH_TEST);
		glEnable(GL_BLEND);
		glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);


		if(bgSprite){
			bgSprite->draw();
		}

		for(int i = 0;i<sprites.size();i++){
			if(sprites[i] != NULL)
				sprites[i]->draw();
		}


		glMatrixMode(GL_PROJECTION);
		glPopMatrix(); 
		glMatrixMode(GL_MODELVIEW);
		glPopMatrix();
		glPopAttrib();		
	}
}