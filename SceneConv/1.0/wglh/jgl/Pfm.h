#pragma once
#include <opencv\cv.h>
#include <opencv\cxcore.h>
#include <opencv\highgui.h>

namespace jgl{
	class Pfm
	{
	public:
		Pfm(void);
		~Pfm(void);
	
		static void CopySubImageTo(IplImage* srcImg,CvRect rect, IplImage* & subImage);
		static bool Load_PFM(const char* fn,IplImage* & img);
		static bool Save_PFM(const char* fn,IplImage* img);
		static bool Save_PFM(const char* fn,const rt::Buffer<float> & data, int dim1,int dim2);
		static bool Save_PFM(const char* fn,const rt::Buffer<num::Vec3f> & data, int dim1,int dim2);
	};
}

