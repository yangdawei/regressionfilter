#pragma once

namespace jgl{
	class GLFont                                                              
	{  
	public:  
		/** 构造函数和析构函数 */  
		GLFont();  
		~GLFont(); 

		bool InitFont();  /**< 初始化字体 */  
		void PrintText(char *string, float x, float y); /**< 在(x,y)处输出string内容 */  
      
	protected:  
		HFONT m_hFont;  /**< 字体句柄 */  
          
	};
}