#pragma once

//////////////////////////////////////////////////////////////////////
// Visual Computing Foundation Classes (VCFC)
//					Jiaping Wang  2006.2
//					e_boris2002@hotmail.com
//
// (C) Copyright Jiaping Wang 2004.
//
// Permission to copy, use, modify, sell and distribute this software
// is granted provided this copyright notice appears in all copies.
// This software is provided "as is" without express or implied
// warranty, and with no claim as to its suitability for any purpose.
//
// Absolutely no warranty!!
//////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////
//  Brief Description
//
//  wglextension.h
//  GL Externsions
//
//////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////
// Revise History
//
// Inital version	2004.?.?		Jiaping
//
//////////////////////////////////////////////////////////////////////

#include "wglHelper.h"
#include <vector>
#include "wglimage.h"
#include "..\num\cubemap.h"

namespace wglh
{

namespace ext
{

//class list
class CwglMultiTextureEnv; //class alias : mt

namespace ARBvpfp
{
	class CwglGpuProgramError;

	class CwglGpuProgramSourceCode;
	class CwglGpuProgramSourceCodeProject;

	class CwglGpuProgram;
		class CwglVertexProgram; //class alias : vp
		class CwglFragmentProgram;
}

class CwglExtensions;
class CwglVertexBuffer;
	class CwglElementBuffer;


#ifndef GL_RGBA_FLOAT32_ATI
	#define GL_RGBA_FLOAT32_ATI                 0x8814
	#define GL_RGB_FLOAT32_ATI                  0x8815
	#define GL_ALPHA_FLOAT32_ATI                0x8816
	#define GL_INTENSITY_FLOAT32_ATI            0x8817
	#define GL_LUMINANCE_FLOAT32_ATI            0x8818
	#define GL_LUMINANCE_ALPHA_FLOAT32_ATI      0x8819
	#define GL_RGBA_FLOAT16_ATI                 0x881A
	#define GL_RGB_FLOAT16_ATI                  0x881B
	#define GL_ALPHA_FLOAT16_ATI                0x881C
	#define GL_INTENSITY_FLOAT16_ATI            0x881D
	#define GL_LUMINANCE_FLOAT16_ATI            0x881E
	#define GL_LUMINANCE_ALPHA_FLOAT16_ATI      0x881F
#endif

typedef CwglTexture2DBase<GL_TEXTURE_2D,GL_RGB_FLOAT16_ATI,GL_RGB,GL_FLOAT>		CwglTexture16f3c;
typedef CwglTexture2DBase<GL_TEXTURE_2D,GL_RGBA_FLOAT16_ATI,GL_RGBA,GL_FLOAT>	CwglTexture16f4c;

typedef CwglTexture2DBase<GL_TEXTURE_2D,GL_RGB_FLOAT32_ATI,GL_RGB,GL_FLOAT>		CwglTexture32f3c;
typedef CwglTexture2DBase<GL_TEXTURE_2D,GL_RGBA_FLOAT32_ATI,GL_RGBA,GL_FLOAT>	CwglTexture32f4c;

typedef CwglTexture2DBase<GL_TEXTURE_2D,GL_DEPTH_COMPONENT16_ARB,GL_DEPTH_COMPONENT,GL_UNSIGNED_BYTE>	CwglDepthTexture16f;
typedef CwglTexture2DBase<GL_TEXTURE_2D,GL_DEPTH_COMPONENT24_ARB,GL_DEPTH_COMPONENT,GL_UNSIGNED_BYTE>	CwglDepthTexture24f;
typedef CwglTexture2DBase<GL_TEXTURE_2D,GL_DEPTH_COMPONENT32_ARB,GL_DEPTH_COMPONENT,GL_UNSIGNED_BYTE>	CwglDepthTexture32f;

#ifndef GL_TEXTURE_RECTANGLE_EXT
	#define GL_TEXTURE_RECTANGLE_EXT	0x84f5
#endif

typedef CwglTexture2DBase<GL_TEXTURE_RECTANGLE_EXT,GL_RGB,GL_BGR_EXT,GL_UNSIGNED_BYTE>		CwglRectTexture8u3c;
typedef CwglTexture2DBase<GL_TEXTURE_RECTANGLE_EXT,GL_RGBA,GL_BGRA_EXT,GL_UNSIGNED_BYTE>	CwglRectTexture8u4c;

typedef CwglTexture2DBase<GL_TEXTURE_RECTANGLE_EXT,GL_DEPTH_COMPONENT16_ARB,GL_DEPTH_COMPONENT,GL_UNSIGNED_BYTE>	CwglRectDepthTexture16f;
typedef CwglTexture2DBase<GL_TEXTURE_RECTANGLE_EXT,GL_DEPTH_COMPONENT24_ARB,GL_DEPTH_COMPONENT,GL_UNSIGNED_BYTE>	CwglRectDepthTexture24f;
typedef CwglTexture2DBase<GL_TEXTURE_RECTANGLE_EXT,GL_DEPTH_COMPONENT32_ARB,GL_DEPTH_COMPONENT,GL_UNSIGNED_BYTE>	CwglRectDepthTexture32f;


namespace ARBvpfp
{

class CwglGpuProgramError
{
public:
	GLint	Position;
	const GLubyte * Desc;
};

class CwglGpuProgram
{
protected:
	//extension entry point for GPU program management
	static PFNGLPROGRAMSTRINGARBPROC				ProgramStringARB;												
	static PFNGLBINDPROGRAMARBPROC					BindProgramARB;													
	static PFNGLDELETEPROGRAMSARBPROC				DeleteProgramsARB;													
	static PFNGLGENPROGRAMSARBPROC					GenProgramsARB;		
	static PFNGLGETPROGRAMIVARBPROC					GetProgramivARB;										
	static PFNGLGETPROGRAMSTRINGARBPROC				GetProgramStringARB;											
	static PFNGLISPROGRAMARBPROC					IsProgramARB;


	//extension entry point for GPU program Env , Parameters and misc
	static PFNGLPROGRAMENVPARAMETER4DARBPROC		ProgramEnvParameter4dARB;											
	static PFNGLPROGRAMENVPARAMETER4DVARBPROC		ProgramEnvParameter4dvARB;							
	static PFNGLPROGRAMENVPARAMETER4FARBPROC		ProgramEnvParameter4fARB;									
	static PFNGLPROGRAMENVPARAMETER4FVARBPROC		ProgramEnvParameter4fvARB;										
	static PFNGLPROGRAMLOCALPARAMETER4DARBPROC		ProgramLocalParameter4dARB;									
	static PFNGLPROGRAMLOCALPARAMETER4DVARBPROC		ProgramLocalParameter4dvARB;												
	static PFNGLPROGRAMLOCALPARAMETER4FARBPROC		ProgramLocalParameter4fARB;										
	static PFNGLPROGRAMLOCALPARAMETER4FVARBPROC		ProgramLocalParameter4fvARB;											
	static PFNGLGETPROGRAMENVPARAMETERDVARBPROC		GetProgramEnvParameterdvARB;													
	static PFNGLGETPROGRAMENVPARAMETERFVARBPROC		GetProgramEnvParameterfvARB;													
	static PFNGLGETPROGRAMLOCALPARAMETERDVARBPROC	GetProgramLocalParameterdvARB;														
	static PFNGLGETPROGRAMLOCALPARAMETERFVARBPROC	GetProgramLocalParameterfvARB;											

	LPCSTR	SourceCodeHeaderTag;  //initialized by derived classes
	GLenum ProgramTarget;     //initialized by derived classes
	GLuint ProgramName;

	BOOL   CompiledSuccess;

	friend class CwglGpuProgramSourceCode;

public:
	#define ReturnProgramInteger(x) 	GLint name; \
										GetProgramivARB(Target,(GL_##x),&name); \
										_CheckErrorGL; \
										return name;

	static GLuint CurrentProgram(GLenum Target)
	{
		ReturnProgramInteger(PROGRAM_BINDING_ARB);
	}
	static GLuint MaxEnvParam(GLenum Target)
	{
		ReturnProgramInteger(MAX_PROGRAM_ENV_PARAMETERS_ARB);
	}
	static GLuint MaxLocalParam(GLenum Target)
	{
		ReturnProgramInteger(MAX_PROGRAM_LOCAL_PARAMETERS_ARB);
	}
	static GLuint MaxTempVariable(GLenum Target)
	{
		ReturnProgramInteger(MAX_PROGRAM_TEMPORARIES_ARB);
	}
	static GLuint MaxParamBinding(GLenum Target)
	{
		ReturnProgramInteger(MAX_PROGRAM_PARAMETERS_ARB);
	}
	static GLuint MaxAttribBinding(GLenum Target)
	{
		ReturnProgramInteger(MAX_PROGRAM_ATTRIBS_ARB);
	}
	static GLuint MaxInstructions(GLenum Target)
	{
		ReturnProgramInteger(MAX_PROGRAM_INSTRUCTIONS_ARB);
	}

	static GLuint MaxNativeTempVariable(GLenum Target)
	{
		ReturnProgramInteger(MAX_PROGRAM_NATIVE_TEMPORARIES_ARB);
	}
	static GLuint MaxNativeParamBinding(GLenum Target)
	{
		ReturnProgramInteger(MAX_PROGRAM_NATIVE_PARAMETERS_ARB);
	}
	static GLuint MaxNativeAttribBinding(GLenum Target)
	{
		ReturnProgramInteger(MAX_PROGRAM_NATIVE_ATTRIBS_ARB);
	}
	static GLuint MaxNativeInstructions(GLenum Target)
	{
		ReturnProgramInteger(MAX_PROGRAM_NATIVE_INSTRUCTIONS_ARB);
	}
	static GLuint MaxNativeAluInstructions(GLenum Target)
	{
		ReturnProgramInteger(MAX_PROGRAM_NATIVE_ALU_INSTRUCTIONS_ARB);
	}
	static GLuint MaxNativeTexInstructions(GLenum Target)
	{
		ReturnProgramInteger(MAX_PROGRAM_NATIVE_TEX_INSTRUCTIONS_ARB);
	}
	static GLuint MaxNativeTexIndirections(GLenum Target)
	{
		ReturnProgramInteger(MAX_PROGRAM_NATIVE_TEX_INDIRECTIONS_ARB);
	}
	#undef ReturnProgramInteger

	#define ReturnCurrentProgramInteger(x) 	GLint name; \
										GetProgramivARB(ProgramTarget,(GL_##x),&name); \
										_CheckErrorGL; \
										return name;

	GLuint CurrentAluInstructionsUsed()
	{
		ReturnCurrentProgramInteger(PROGRAM_ALU_INSTRUCTIONS_ARB);
	}

	GLuint CurrentTexInstructionsUsed()
	{
		ReturnCurrentProgramInteger(PROGRAM_TEX_INSTRUCTIONS_ARB);
	}

	GLuint CurrentTexIndirectionsUsed()
	{
		ReturnCurrentProgramInteger(PROGRAM_TEX_INDIRECTIONS_ARB);
	}

	GLuint CurrentTemporariesUsed()
	{
		ReturnCurrentProgramInteger(PROGRAM_TEMPORARIES_ARB);
	}

	GLuint CurrentParametersUsed()
	{
		ReturnCurrentProgramInteger(PROGRAM_PARAMETERS_ARB);
	}

	GLuint CurrentAttribsUsed()
	{
		ReturnCurrentProgramInteger(PROGRAM_ATTRIBS_ARB);
	}

	GLuint CurrentNativeInstructionsUsed()
	{
		ReturnCurrentProgramInteger(PROGRAM_NATIVE_INSTRUCTIONS_ARB);
	}

	GLuint CurrentNativeAluInstructionsUsed()
	{
		ReturnCurrentProgramInteger(PROGRAM_NATIVE_ALU_INSTRUCTIONS_ARB);
	}

	GLuint CurrentNativeTexInstructionsUsed()
	{
		ReturnCurrentProgramInteger(PROGRAM_NATIVE_TEX_INSTRUCTIONS_ARB);
	}

	GLuint CurrentNativeTexIndirectionsUsed()
	{
		ReturnCurrentProgramInteger(PROGRAM_NATIVE_TEX_INDIRECTIONS_ARB);
	}

	GLuint CurrentNativeTemporariesUsed()
	{
		ReturnCurrentProgramInteger(PROGRAM_NATIVE_TEMPORARIES_ARB);
	}

	GLuint CurrentNativeParametersUsed()
	{
		ReturnCurrentProgramInteger(PROGRAM_NATIVE_PARAMETERS_ARB);
	}

	GLuint CurrentNativeAttribsUsed()
	{
		ReturnCurrentProgramInteger(PROGRAM_NATIVE_ATTRIBS_ARB);
	}

	#undef ReturnCurrentProgramInteger

public:

	//static helper functions 
	static BOOL LoadExtensionEntryPoints();

	WGLH_FUNC static GLuint CurrentVertexProgram()
	{
		return CurrentProgram(GL_VERTEX_PROGRAM_ARB);
	}

	WGLH_FUNC static GLuint CurrentFragmentProgram()
	{
		return CurrentProgram(GL_FRAGMENT_PROGRAM_ARB);
	}

	static void EnableVertexProgram(BOOL Yes = TRUE)
	{
		Yes?glEnable(GL_VERTEX_PROGRAM_ARB):glDisable(GL_VERTEX_PROGRAM_ARB);
	}

	static void EnableFragmentProgram(BOOL Yes = TRUE)
	{
		Yes?glEnable(GL_FRAGMENT_PROGRAM_ARB):glDisable(GL_FRAGMENT_PROGRAM_ARB);
	}

	//Operations
	CwglGpuProgram();
	operator GLenum(){ return ProgramTarget; }

	void	SetEnvParam(GLuint index,GLfloat x,GLfloat y,GLfloat z,GLfloat w);
	void	SetEnvParam(GLuint index,GLdouble x,GLdouble y,GLdouble z,GLdouble w);
	void	SetLocalParam(GLuint index,GLfloat x,GLfloat y,GLfloat z,GLfloat w);
	void	SetLocalParam(GLuint index,GLdouble x,GLdouble y,GLdouble z,GLdouble w);

	void	SetEnvParam(GLuint index,const GLfloat *pv);
	void	SetEnvParam(GLuint index,const GLdouble *pv);
	void	SetLocalParam(GLuint index,const GLfloat *pv);
	void	SetLocalParam(GLuint index,const GLdouble *pv);

	void	GetEnvParam(GLuint index,GLfloat *pv);
	void	GetEnvParam(GLuint index,GLdouble *pv);
	void	GetLocalParam(GLuint index,GLfloat *pv);
	void	GetLocalParam(GLuint index,GLdouble *pv);

	BOOL	LoadProgramAndApply(LPCSTR prog,CwglGpuProgramError * pError = NULL);
	void	Apply();
};

class CwglVertexProgram:public CwglGpuProgram
{
public:
	//Additional entrypoints for Vertex Program
	static PFNGLVERTEXATTRIB1SARBPROC              VertexAttrib1sARB;
	static PFNGLVERTEXATTRIB1FARBPROC			   VertexAttrib1fARB;
	static PFNGLVERTEXATTRIB1DARBPROC			   VertexAttrib1dARB;
	static PFNGLVERTEXATTRIB2SARBPROC			   VertexAttrib2sARB;
	static PFNGLVERTEXATTRIB2FARBPROC			   VertexAttrib2fARB;
	static PFNGLVERTEXATTRIB2DARBPROC			   VertexAttrib2dARB;
	static PFNGLVERTEXATTRIB3SARBPROC			   VertexAttrib3sARB;
	static PFNGLVERTEXATTRIB3FARBPROC			   VertexAttrib3fARB;
	static PFNGLVERTEXATTRIB3DARBPROC			   VertexAttrib3dARB;
	static PFNGLVERTEXATTRIB4SARBPROC			   VertexAttrib4sARB;
	static PFNGLVERTEXATTRIB4FARBPROC			   VertexAttrib4fARB;
	static PFNGLVERTEXATTRIB4DARBPROC			   VertexAttrib4dARB;
	static PFNGLVERTEXATTRIB4NUBARBPROC			   VertexAttrib4NubARB;
												   
	static PFNGLVERTEXATTRIB1SVARBPROC			   VertexAttrib1svARB;
	static PFNGLVERTEXATTRIB1FVARBPROC			   VertexAttrib1fvARB;
	static PFNGLVERTEXATTRIB1DVARBPROC			   VertexAttrib1dvARB;
	static PFNGLVERTEXATTRIB2SVARBPROC			   VertexAttrib2svARB;
	static PFNGLVERTEXATTRIB2FVARBPROC			   VertexAttrib2fvARB;
	static PFNGLVERTEXATTRIB2DVARBPROC			   VertexAttrib2dvARB;
	static PFNGLVERTEXATTRIB3SVARBPROC			   VertexAttrib3svARB;
	static PFNGLVERTEXATTRIB3FVARBPROC			   VertexAttrib3fvARB;
	static PFNGLVERTEXATTRIB3DVARBPROC			   VertexAttrib3dvARB;
	static PFNGLVERTEXATTRIB4BVARBPROC			   VertexAttrib4bvARB;
	static PFNGLVERTEXATTRIB4SVARBPROC			   VertexAttrib4svARB;
	static PFNGLVERTEXATTRIB4IVARBPROC			   VertexAttrib4ivARB;
	static PFNGLVERTEXATTRIB4UBVARBPROC			   VertexAttrib4ubvARB;
	static PFNGLVERTEXATTRIB4USVARBPROC			   VertexAttrib4usvARB;
	static PFNGLVERTEXATTRIB4UIVARBPROC			   VertexAttrib4uivARB;
	static PFNGLVERTEXATTRIB4FVARBPROC			   VertexAttrib4fvARB;
	static PFNGLVERTEXATTRIB4DVARBPROC			   VertexAttrib4dvARB;
	static PFNGLVERTEXATTRIB4NBVARBPROC			   VertexAttrib4NbvARB;
	static PFNGLVERTEXATTRIB4NSVARBPROC			   VertexAttrib4NsvARB;
	static PFNGLVERTEXATTRIB4NIVARBPROC			   VertexAttrib4NivARB;
	static PFNGLVERTEXATTRIB4NUBVARBPROC		   VertexAttrib4NubvARB;
	static PFNGLVERTEXATTRIB4NUSVARBPROC		   VertexAttrib4NusvARB;
	static PFNGLVERTEXATTRIB4NUIVARBPROC		   VertexAttrib4NuivARB;
												   
	static PFNGLVERTEXATTRIBPOINTERARBPROC		   VertexAttribPointerARB;
												   
	static PFNGLENABLEVERTEXATTRIBARRAYARBPROC	   EnableVertexAttribArrayARB;
	static PFNGLDISABLEVERTEXATTRIBARRAYARBPROC	   DisableVertexAttribArrayARB;
												   
	static PFNGLGETVERTEXATTRIBDVARBPROC		   GetVertexAttribdvARB;
	static PFNGLGETVERTEXATTRIBFVARBPROC		   GetVertexAttribfvARB;
	static PFNGLGETVERTEXATTRIBIVARBPROC		   GetVertexAttribivARB;
												   
	static PFNGLGETVERTEXATTRIBPOINTERVARBPROC	   GetVertexAttribPointervARB;

public:
	CwglVertexProgram();
	static BOOL LoadExtensionEntryPoints();
};


class CwglFragmentProgram:public CwglGpuProgram
{
public:
	CwglFragmentProgram();
	static BOOL LoadExtensionEntryPoints();
};


class CwglGpuProgramSourceCode
{
protected:
	CHAR*	pSourceCode;
	HWND	hNotepadWnd;
public:
	enum
	{
		GPSC_CLIPBROAD = 0,
		GPSC_FILE,
		GPSC_RESOURCE
	};

	CwglGpuProgramSourceCode(){ ZeroMemory(this,sizeof(CwglGpuProgramSourceCode)); }
	~CwglGpuProgramSourceCode(){ _SafeDelArray(pSourceCode); }
	BOOL	Load(int Method,LPCTSTR pName1 = NULL,LPCTSTR pName2 = NULL);
	BOOL	LoadFromNotepadInstance();
	BOOL	Compile(CwglGpuProgram& ShaderOut,LPCSTR pszName = NULL,BOOL DumpResource = TRUE,BOOL ShowNotificationToNotepad = TRUE);
};

#define WM_GpuProgramSourceCodeChanged		(WM_USER+1010)

DWORD WINAPI _MonitoringThread(LPVOID pv);

class CwglGpuProgramSourceCodeProject
{
	friend DWORD WINAPI _MonitoringThread(LPVOID pv);

protected:
	TCHAR	pProjectFolder[MAX_PATH];
	UINT	ProjectFolderLen;

	HWND	hNoticationWnd;
	HANDLE	hMonitoringThread;

	typedef struct _tagProgramItem
	{
		TCHAR			Filename[MAX_PATH];
		CwglGpuProgram* ShaderObj;
		FILETIME		FileTime;
	}ProgramItem,*LPProgramItem;
	typedef const ProgramItem* LPCProgramItem;

	std::vector<ProgramItem>	m_ProgramList;

public:

	CwglGpuProgramSourceCodeProject(){ ZeroMemory(this,sizeof(CwglGpuProgramSourceCodeProject)); }
	~CwglGpuProgramSourceCodeProject();

	BOOL	LoadGpuProgramProjectFolder(LPCTSTR pFolder,HWND NotificationWnd);
	void	BindGpuProgram(CwglGpuProgram& ShaderOut,LPCTSTR pszName);
	void	ReloadAll();
	void	ReloadChanged();
};

}



class CwglMultiTextureEnv
{
protected:
	static PFNGLACTIVETEXTUREARBPROC          ActiveTextureARB;                   
public:
	static PFNGLCLIENTACTIVETEXTUREARBPROC	  ClientActiveTextureARB; 
	static PFNGLMULTITEXCOORD1DARBPROC		  MultiTexCoord1dARB; 
	static PFNGLMULTITEXCOORD1DVARBPROC		  MultiTexCoord1dvARB;
	static PFNGLMULTITEXCOORD1FARBPROC		  MultiTexCoord1fARB; 
	static PFNGLMULTITEXCOORD1FVARBPROC		  MultiTexCoord1fvARB;
	static PFNGLMULTITEXCOORD1IARBPROC		  MultiTexCoord1iARB; 
	static PFNGLMULTITEXCOORD1IVARBPROC		  MultiTexCoord1ivARB;
	static PFNGLMULTITEXCOORD1SARBPROC		  MultiTexCoord1sARB; 
	static PFNGLMULTITEXCOORD1SVARBPROC		  MultiTexCoord1svARB;
	static PFNGLMULTITEXCOORD2DARBPROC		  MultiTexCoord2dARB; 
	static PFNGLMULTITEXCOORD2DVARBPROC		  MultiTexCoord2dvARB;
	static PFNGLMULTITEXCOORD2FARBPROC		  MultiTexCoord2fARB; 
	static PFNGLMULTITEXCOORD2FVARBPROC		  MultiTexCoord2fvARB;
	static PFNGLMULTITEXCOORD2IARBPROC		  MultiTexCoord2iARB; 
	static PFNGLMULTITEXCOORD2IVARBPROC		  MultiTexCoord2ivARB;
	static PFNGLMULTITEXCOORD2SARBPROC		  MultiTexCoord2sARB; 
	static PFNGLMULTITEXCOORD2SVARBPROC		  MultiTexCoord2svARB;
	static PFNGLMULTITEXCOORD3DARBPROC		  MultiTexCoord3dARB; 
	static PFNGLMULTITEXCOORD3DVARBPROC		  MultiTexCoord3dvARB;
	static PFNGLMULTITEXCOORD3FARBPROC		  MultiTexCoord3fARB; 
	static PFNGLMULTITEXCOORD3FVARBPROC		  MultiTexCoord3fvARB;
	static PFNGLMULTITEXCOORD3IARBPROC		  MultiTexCoord3iARB; 
	static PFNGLMULTITEXCOORD3IVARBPROC		  MultiTexCoord3ivARB;
	static PFNGLMULTITEXCOORD3SARBPROC		  MultiTexCoord3sARB; 
	static PFNGLMULTITEXCOORD3SVARBPROC		  MultiTexCoord3svARB;
	static PFNGLMULTITEXCOORD4DARBPROC		  MultiTexCoord4dARB; 
	static PFNGLMULTITEXCOORD4DVARBPROC		  MultiTexCoord4dvARB;
	static PFNGLMULTITEXCOORD4FARBPROC		  MultiTexCoord4fARB; 
	static PFNGLMULTITEXCOORD4FVARBPROC		  MultiTexCoord4fvARB;
	static PFNGLMULTITEXCOORD4IARBPROC		  MultiTexCoord4iARB; 
	static PFNGLMULTITEXCOORD4IVARBPROC		  MultiTexCoord4ivARB;
	static PFNGLMULTITEXCOORD4SARBPROC		  MultiTexCoord4sARB; 
	static PFNGLMULTITEXCOORD4SVARBPROC		  MultiTexCoord4svARB;

	static BOOL LoadExtensionEntryPoints();
	WGLH_FUNC static GLuint MaxTextureStage()
	{
		GLint co;

		glGetIntegerv(GL_MAX_TEXTURE_UNITS_ARB,&co);
		_CheckErrorGL;
		return co;
	}

	static void ActiveTextureUnit(GLuint stage);

	enum
	{
		_TextureUnitBaseId = GL_TEXTURE0_ARB
	};
};

class CwglTexture3D
{
public:
	static PFNGLTEXIMAGE3DEXTPROC		TexImage3DEXT;
	static PFNGLTEXSUBIMAGE3DEXTPROC	TexSubImage3DEXT;
	static BOOL LoadExtensionEntryPoints();
};

template<UINT TexInternalFormat,UINT TexDefaultFormat,UINT TexDefaultType,UINT TexTarget = GL_TEXTURE_3D_EXT>
class CwglTexture3DBase:public CwglTextureBase<TexTarget>
{
protected:
	UINT m_WrapMode_S;	
	UINT m_WrapMode_T;
	UINT m_WrapMode_R;
	TextureFilter m_TexFilterMinify;
	TextureFilter m_TexFilterMagnify;

public:
	void Apply()
	{
		__super::Apply();
		glTexParameterf(TexTarget,GL_TEXTURE_WRAP_S,(GLfloat)m_WrapMode_S);
		glTexParameterf(TexTarget,GL_TEXTURE_WRAP_T,(GLfloat)m_WrapMode_T);
		glTexParameterf(TexTarget,GL_TEXTURE_WRAP_R_EXT,(GLfloat)m_WrapMode_R);
		_CheckErrorGL;

		glTexParameteri(TexTarget,GL_TEXTURE_MIN_FILTER,m_TexFilterMinify);
		glTexParameteri(TexTarget,GL_TEXTURE_MAG_FILTER,m_TexFilterMagnify);
		_CheckErrorGL;
	}

	CwglTexture3DBase(){ TextureWrapMode(); TextureFilterSet(); }

	void DefineTexture(UINT cx,UINT cy, UINT cz,LPCVOID pData=NULL,UINT DataFormat=TexDefaultFormat,UINT DataType=TexDefaultType)
	{
		ASSERT(wglh::ext::CwglTexture3D::TexImage3DEXT);
		Apply();

		glPixelStorei(GL_UNPACK_ALIGNMENT,4);
		_CheckErrorGL;
		wglh::ext::CwglTexture3D::TexImage3DEXT(TexTarget,0,TexInternalFormat,cx,cy,cz,0,TexDefaultFormat,TexDefaultType, pData);
		_CheckErrorGL;
	}

	void TextureWrapMode(UINT s_mode=WrapRepeat,UINT t_mode=WrapRepeat,UINT r_mode=WrapRepeat)
	{
		m_WrapMode_S = s_mode;
		m_WrapMode_T = t_mode;
		m_WrapMode_R = r_mode;
	}

	void TextureFilterSet(TextureFilter minify=tx::FilterLinear,TextureFilter magnify=tx::FilterLinear)
	{
		m_TexFilterMinify = minify;  
		m_TexFilterMagnify = magnify; 
	}

	static int GetSizeMax()
	{
		GLint sz = 0;
		glGetIntegerv(GL_MAX_3D_TEXTURE_SIZE_EXT,&sz);
		return sz>0?sz:64;
	}

	static int GetLayerMax() // for Texture Array only
	{
		GLint sz = 0;
		glGetIntegerv(GL_MAX_ARRAY_TEXTURE_LAYERS,&sz);
		return sz>0?sz:64;
	}
};

typedef CwglTexture3DBase<GL_RGBA32F_ARB, GL_RGBA, GL_FLOAT>							Cwgl3DTexture4c32f;
typedef CwglTexture3DBase<GL_RGBA32F_ARB, GL_RGBA, GL_FLOAT,GL_TEXTURE_2D_ARRAY_EXT>	CwglArrayTexture4c32f;
typedef CwglTexture3DBase<GL_RGBA16F_ARB, GL_RGBA, GL_FLOAT>							Cwgl3DTexture4c16f;
typedef CwglTexture3DBase<GL_RGBA16F_ARB, GL_RGBA, GL_FLOAT,GL_TEXTURE_2D_ARRAY_EXT>	CwglArrayTexture4c16f;

class CwglExtensions
{
public:
	enum
	{
		VertexArrayId_Vertex	= GL_VERTEX_ARRAY_EXT,
		VertexArrayId_Color		= GL_COLOR_ARRAY_EXT,
		VertexArrayId_Normal	= GL_NORMAL_ARRAY_EXT,
		VertexArrayId_Index		= GL_INDEX_ARRAY_EXT,
		VertexArrayId_TexCoord	= GL_TEXTURE_COORD_ARRAY_EXT,
		VertexArrayId_EdgeFlag	= GL_EDGE_FLAG_ARRAY_EXT
	};

	typedef struct _tagVertexArrayPointers
	{
		_tagVertexArrayPointers(){ ZeroMemory(this,sizeof(_tagVertexArrayPointers)); }

		GLsizei StaticVextexCount;

		LPCVOID	Color_Pointer;
		GLenum	Color_ValueType;
		GLsizei Color_ByteStride;
		GLint	Color_Components;

		LPCVOID	EdgeFlag_Pointer;
		GLsizei EdgeFlag_ByteStride;

		LPCVOID	ColorIndex_Pointer;
		GLenum	ColorIndex_ValueType;
		GLsizei ColorIndex_ByteStride;

		LPCVOID	Normal_Pointer;
		GLenum	Normal_ValueType;
		GLsizei Normal_ByteStride;

		LPCVOID	TexCoord_Pointer;
		GLenum	TexCoord_ValueType;
		GLsizei TexCoord_ByteStride;
		GLint	TexCoord_Components;

		LPCVOID	Vertex_Pointer;
		GLenum	Vertex_ValueType;
		GLsizei Vertex_ByteStride;
		GLint	Vertex_Components;
	}VertexArrayPointers,*LPVertexArrayPointers;
	typedef const VertexArrayPointers* LPCVertexArrayPointers;

protected:
	static LPCSTR	wglExtensionString;

public:
	static BOOL LoadExtensionEntryPoints();

	// for WGL_ARB_extensions_string and WGL_EXT_extensions_string
protected:
	static PFNWGLGETEXTENSIONSSTRINGARBPROC	GetExtensionsStringARB;
public:
	static BOOL	CheckExtensionWGL(LPCSTR	ExtName);
	static BOOL	CheckExtension(LPCSTR	ExtName){ return CwglWnd::CheckGLExtension(ExtName); }
	static LPCSTR GetExtensionStringWGL(){ return wglExtensionString; };
	static LPCSTR GetExtensionString(){ return CwglWnd::GetGLExtension(); }

	//  for GL_EXT_point_parameters
protected:
	static PFNGLPOINTPARAMETERFARBPROC		PointParameterfARB;
	static PFNGLPOINTPARAMETERFVARBPROC		PointParameterfvARB;
public:
	static void   PointParameter_MinMax(GLfloat min=-1,GLfloat max=-1);
	static void   PointParameter_FadeThreshold(GLfloat threshold);
	static void   PointParameter_DistanceAttenuation(GLfloat a=1,GLfloat b=0,GLfloat c=0);
	static BOOL	  PointParameter_IsSupportted(){ return PointParameterfARB&&PointParameterfvARB; }


	// for GL_ATI_draw_buffers
protected:
	static PFNGLDRAWBUFFERSATIPROC DrawBuffersATI;
public:
	static int GetDrawBufferCount(){ int ret; glGetIntegerv(GL_MAX_DRAW_BUFFERS_ATI,&ret); return ret; }
	static void DrawBuffers(int co,const GLenum * pBufs){ ASSERT(co); ASSERT(pBufs); DrawBuffersATI(co,pBufs); _CheckErrorGL; }

	// for GL_EXT_vertex_array
public:
	static PFNGLARRAYELEMENTEXTPROC			VertexArray_ArrayElement;
	WGLH_FUNC void							VertexArray_DrawElements( GLenum mode, GLsizei count, GLenum type, const GLvoid *indices){ glDrawElements(mode,count,type,indices); _CheckErrorGL;}
	static PFNGLDRAWARRAYSEXTPROC			VertexArray_DrawArrays;
	static PFNGLGETPOINTERVEXTPROC			VertexArray_GetPointerv;

	// for GL_EXT_compiled_vertex_array
protected:
	static PFNGLLOCKARRAYSEXTPROC			LockArraysEXT;
	static PFNGLUNLOCKARRAYSEXTPROC			UnlockArraysEXT;
	// Do not call the following enrtypoints call VertexArray_Pointers instead
	static PFNGLCOLORPOINTEREXTPROC			ColorPointerEXT;
	static PFNGLEDGEFLAGPOINTEREXTPROC		EdgeFlagPointerEXT;
	static PFNGLINDEXPOINTEREXTPROC			IndexPointerEXT;
	static PFNGLNORMALPOINTEREXTPROC		NormalPointerEXT;
	static PFNGLTEXCOORDPOINTEREXTPROC		TexCoordPointerEXT;
	static PFNGLVERTEXPOINTEREXTPROC		VertexPointerEXT;   // regard as a flag for if the VA Extension exists or not

public:
	static void   VertexArray_Pointers(CwglExtensions::LPCVertexArrayPointers pVertexArray,BOOL AutoEnableArrays = TRUE);
	WGLH_FUNC static void	  VertexArray_Enable(int VertexArrayId){ ASSERT(VertexArrayId>=GL_VERTEX_ARRAY_EXT&&VertexArrayId<=GL_EDGE_FLAG_ARRAY_EXT); glEnable(VertexArrayId); }
	WGLH_FUNC static void	  VertexArray_Disable(int VertexArrayId){ ASSERT(VertexArrayId>=GL_VERTEX_ARRAY_EXT&&VertexArrayId<=GL_EDGE_FLAG_ARRAY_EXT); glDisable(VertexArrayId); }
	WGLH_FUNC static void	  VertexArray_Lock(int Start,GLsizei Count){ if(LockArraysEXT){LockArraysEXT(Start,Count); _CheckErrorGL;}else{ _CheckDump("No GL_EXT_compiled_vertex_array supportted.\n"); } }
	WGLH_FUNC static void	  VertexArray_Unlock(int Start,GLsizei Count){ if(UnlockArraysEXT){UnlockArraysEXT(); _CheckErrorGL;}else{ _CheckDump("No GL_EXT_compiled_vertex_array supportted.\n"); } }

	// for WGL_EXT_swap_control
public:
	static PFNWGLSWAPINTERVALEXTPROC		SwapIntervalEXT;
	static PFNWGLGETSWAPINTERVALEXTPROC		GetSwapIntervalEXT;
};

//A class for short name ex::XXXXXX


class CwglVertexBufferBase
{
public:
	static PFNGLBINDBUFFERARBPROC			BindBufferARB;
	static PFNGLDELETEBUFFERSARBPROC		DeleteBuffersARB;
	static PFNGLGENBUFFERSARBPROC			GenBuffersARB;	// regard as sign
	static PFNGLISBUFFERARBPROC				IsBufferARB;
	static PFNGLBUFFERDATAARBPROC			BufferDataARB;
	static PFNGLBUFFERSUBDATAARBPROC		BufferSubDataARB;
	static PFNGLGETBUFFERSUBDATAARBPROC		GetBufferSubDataARB;
	static PFNGLMAPBUFFERARBPROC			MapBufferARB;
	static PFNGLUNMAPBUFFERARBPROC			UnmapBufferARB;
	static PFNGLGETBUFFERPARAMETERIVARBPROC	GetBufferParameterivARB;
	static PFNGLGETBUFFERPOINTERVARBPROC	GetBufferPointervARB;
protected:
	GLuint	VertexBufferName;
	LPBYTE	VertexBufferPtrBase;
	DWORD	LastStorageHint;

	GLint	BufferSize;
public:
	GLuint GetVertexBufferName()
	{
		return VertexBufferName;
	}
public:
	enum
	{
		StorageHint_NoChange = 0,

		StorageHint_OnBoard = GL_STATIC_DRAW_ARB,
		StorageHint_AGP = GL_DYNAMIC_DRAW_ARB,	
		StorageHint_System = GL_STREAM_DRAW_ARB,
        
		AccessFlag_ReadOnly = GL_READ_ONLY_ARB,
		AccessFlag_WriteOnly = GL_WRITE_ONLY_ARB,
		AccessFlag_ReadWrite = GL_READ_WRITE_ARB
	};

public:
	static BOOL	LoadExtensionEntryPoints();
    static BOOL IsSupported()
    {  return  GenBuffersARB!=NULL;}

	CwglVertexBufferBase(){ VertexBufferName = NULL; VertexBufferPtrBase = NULL; BufferSize = 0;}
	~CwglVertexBufferBase(){ Destroy(); }

	BOOL	Create(DWORD StorageHint = StorageHint_OnBoard);  // alloc buffer
	void	Destroy();
	
	WGLH_FUNC operator LPCVOID (){ return VertexBufferPtrBase; }
	WGLH_FUNC LPCVOID operator[](int index)
	{
		return VertexBufferPtrBase+index;
	}

	WGLH_FUNC BOOL IsOk() const { return VertexBufferName || VertexBufferPtrBase; }

	WGLH_FUNC DWORD	GetStorageHint(){ return LastStorageHint; }
};
   
template<UINT VB_ARRAY_TARGET>
class CwglVertexBufferBaseT:public CwglVertexBufferBase
{
public:

	WGLH_FUNC void Apply()
	{
		if( GenBuffersARB )
		{
			ASSERT(VertexBufferName);  
			BindBufferARB(VB_ARRAY_TARGET,VertexBufferName); 
			_CheckErrorGL;
		}
	}

	static WGLH_FUNC void ApplyNull()
	{
		if( GenBuffersARB )
		{
			BindBufferARB(VB_ARRAY_TARGET,NULL); 
			_CheckErrorGL;
		}
	}

	WGLH_FUNC void	SetSize(GLsizeiptrARB size_in_byte,LPCVOID pData = NULL,DWORD StorageHint = StorageHint_NoChange)  // Copy data to buffer
	{
		if(StorageHint)LastStorageHint = StorageHint;

		if( BufferSize != size_in_byte)
		{
			ASSERT(size_in_byte);

			if( GenBuffersARB )
			{
				Apply();
				BufferDataARB(VB_ARRAY_TARGET,size_in_byte,pData,LastStorageHint);
				_CheckErrorGL;
				GetBufferParameterivARB(VB_ARRAY_TARGET,GL_BUFFER_SIZE_ARB,&BufferSize);
				_CheckErrorGL; 
			}
			else
			{
				_SafeDelArray(VertexBufferPtrBase);
				VertexBufferPtrBase = new BYTE[size_in_byte];
				ASSERT(VertexBufferPtrBase);
				BufferSize = (GLint)size_in_byte;
			}
		}
	}

	WGLH_FUNC GLsizeiptrARB GetSize()
	{
		return BufferSize;
	}

	WGLH_FUNC void	SetData(LPCVOID pData,GLsizeiptrARB size_in_byte,GLsizeiptrARB offset = 0)
	{
		ASSERT(pData);
		if( GenBuffersARB )
		{
			Apply();
			BufferSubDataARB(VB_ARRAY_TARGET,offset,size_in_byte,pData);
			_CheckErrorGL;
		}
		else
		{
			ASSERT(VertexBufferPtrBase);
			memcpy(&VertexBufferPtrBase[offset],pData,size_in_byte);
		}
	}

	WGLH_FUNC void	GetData(LPVOID pData,GLsizeiptrARB size_in_byte,GLsizeiptrARB offset = 0)
	{
		ASSERT(pData);

		if( GenBuffersARB )
		{
			Apply();
			GetBufferSubDataARB(VB_ARRAY_TARGET,offset,size_in_byte,pData);
			_CheckErrorGL;
		}
		else
		{
			ASSERT(VertexBufferPtrBase);
			memcpy(pData,&VertexBufferPtrBase[offset],size_in_byte);
		}
	}

	WGLH_FUNC LPVOID	LockBuffer(DWORD AccessFlag)
	{
		if( GenBuffersARB )
		{
			Apply();
			LPVOID p = MapBufferARB(VB_ARRAY_TARGET,AccessFlag);
			_CheckErrorGL;
			return p;
		}
		else
		{
			ASSERT(VertexBufferPtrBase);
			return VertexBufferPtrBase;
		}
	}

	WGLH_FUNC void UnlockBuffer()
	{
		if( GenBuffersARB )
		{
			Apply();
			UnmapBufferARB(VB_ARRAY_TARGET); 
			_CheckErrorGL;
		}
	}
};

class CwglPixelBufferBase: public CwglVertexBufferBase
{
public:
	enum 
	{
		BIND_TARGET_RENDER = GL_ARRAY_BUFFER_ARB,
		BIND_TARGET_PACK = GL_PIXEL_PACK_BUFFER_EXT,
		BIND_TARGET_UNPACK = GL_PIXEL_UNPACK_BUFFER_EXT
	};
protected:
	UINT m_bindTarget;
public:
	CwglPixelBufferBase()
	{
		m_bindTarget = BIND_TARGET_RENDER;
	}

	WGLH_FUNC void Apply(UINT target = BIND_TARGET_RENDER)
	{
		if( GenBuffersARB )
		{
			ASSERT(VertexBufferName);  
			m_bindTarget = target;
			BindBufferARB(target,VertexBufferName); 
			_CheckErrorGL;
		}
	}
	WGLH_FUNC void ApplyNull()
	{
		if( GenBuffersARB )
		{
			BindBufferARB(m_bindTarget,NULL); 
			_CheckErrorGL;
		}
	}
	
	WGLH_FUNC void	SetSize(GLsizeiptrARB size_in_byte,LPCVOID pData = NULL)  // Copy data to buffer
	{

		if( BufferSize != size_in_byte)
		{
			ASSERT(size_in_byte);

			if( GenBuffersARB )
			{				
	
				BufferDataARB(m_bindTarget,size_in_byte,pData,GL_STREAM_COPY);
				_CheckErrorGL;
				GetBufferParameterivARB(m_bindTarget,GL_BUFFER_SIZE_ARB,&BufferSize);
				_CheckErrorGL; 
			}
		}
	}

	WGLH_FUNC GLsizeiptrARB GetSize()
	{
		return BufferSize;
	}

	WGLH_FUNC void	SetData(LPCVOID pData,GLsizeiptrARB size_in_byte,GLsizeiptrARB offset = 0)
	{
		ASSERT(pData);
		if( GenBuffersARB )
		{
			BufferSubDataARB(m_bindTarget,offset,size_in_byte,pData);
			_CheckErrorGL;
		}
	}

	WGLH_FUNC void	GetData(LPVOID pData,GLsizeiptrARB size_in_byte,GLsizeiptrARB offset = 0)
	{
		ASSERT(pData);

		if( GenBuffersARB )
		{
			GetBufferSubDataARB(m_bindTarget,offset,size_in_byte,pData);
			_CheckErrorGL;
		}
	}

	WGLH_FUNC LPVOID	LockBuffer(DWORD AccessFlag)
	{
		if( GenBuffersARB )
		{
			LPVOID p = MapBufferARB(m_bindTarget,AccessFlag);
			_CheckErrorGL;
			return p;
		}
		return NULL;
	}

	WGLH_FUNC void UnlockBuffer()
	{
		if( GenBuffersARB )
		{
			UnmapBufferARB(m_bindTarget); 
			_CheckErrorGL;
		}
	}	
	
};

typedef CwglVertexBufferBaseT<GL_ARRAY_BUFFER_ARB>	CwglVertexAttributeBuffer;
typedef CwglVertexBufferBaseT<GL_ELEMENT_ARRAY_BUFFER_ARB>	CwglVertexIndexBuffer;

//////////////////////////
// Cubemap

class CwglCubemapBase:public CwglTextureBase<GL_TEXTURE_CUBE_MAP_ARB>
{
protected:
	TextureFilter	m_TexFilterMinify;
	TextureFilter	m_TexFilterMagnify;
	UINT			m_FaceSize;

public:
	CwglCubemapBase(){ TextureFilterSet(); m_FaceSize = 0; }
	void Apply()
	{
		__super::Apply();
		_CheckErrorGL;

		TextureWrapMode(tx::WrapClampToEdge,tx::WrapClampToEdge);
		_CheckErrorGL;
		glTexParameteri(GL_TEXTURE_CUBE_MAP_ARB,GL_TEXTURE_WRAP_S,tx::WrapClampToEdge);
		glTexParameteri(GL_TEXTURE_CUBE_MAP_ARB,GL_TEXTURE_WRAP_T,tx::WrapClampToEdge);
		glTexParameteri(GL_TEXTURE_CUBE_MAP_ARB,GL_TEXTURE_WRAP_R_EXT,tx::WrapClampToEdge);
		_CheckErrorGL;

		glTexParameteri(GL_TEXTURE_CUBE_MAP_ARB,GL_TEXTURE_MIN_FILTER,m_TexFilterMinify);
		glTexParameteri(GL_TEXTURE_CUBE_MAP_ARB,GL_TEXTURE_MAG_FILTER,m_TexFilterMagnify);
		_CheckErrorGL;
	}
	static int	GetMaxFaceSize(){ int ret; glGetIntegerv(GL_MAX_CUBE_MAP_TEXTURE_SIZE_ARB,&ret); _CheckErrorGL; return ret; }
	void		TextureFilterSet(TextureFilter minify=tx::FilterNearest,TextureFilter magnify=tx::FilterNearest){ m_TexFilterMinify = minify;  m_TexFilterMagnify = magnify; }
private:
	void		TextureFilterSet(TextureFilter minify,TextureFilter magnify,GLfloat AnisotropyParam){ ASSERT(0); }
};

template<UINT TexInternalFormat,UINT TexDefaultFormat,UINT TexDefaultValue>
class CwglCubemap:public CwglCubemapBase
{
		template<UINT internal_format>
		struct _internal_type{ typedef BYTE t_Result; };
			template<> struct _internal_type<GL_RGB>{ typedef num::Vec3b t_Result; };
			template<> struct _internal_type<GL_RGB_FLOAT16_ATI>{ typedef num::Vec3f t_Result; };
			template<> struct _internal_type<GL_RGB_FLOAT32_ATI>{ typedef num::Vec3f t_Result; };
			template<> struct _internal_type<GL_RGBA>{ typedef num::Vec4b t_Result; };
			template<> struct _internal_type<GL_RGBA_FLOAT16_ATI>{ typedef num::Vec4f t_Result; };
			template<> struct _internal_type<GL_RGBA_FLOAT32_ATI>{ typedef num::Vec4f t_Result; };

protected:
		static int _Chan(){ return rt::TypeTraits<typename _internal_type<TexInternalFormat>::t_Result>::Length; }
		static int _BPV(){ return sizeof(rt::TypeTraits<typename _internal_type<TexInternalFormat>::t_Result>::t_Element); }

public:

	//template<typename T>
	//void DefineTexture_AutoMipmap(const num::CCubeMap<T>& cm)
	//{
	//	DefineTexture_AutoMipmap(cm.GetFaceSize(),cm.GetTiledImage());
	//}
	
	void DefineTexture_AutoMipmap(UINT FaceSize, LPCVOID pSixFaceTiles_in)
	{
		m_FaceSize = FaceSize;

		int step = FaceSize*_Chan()*_BPV();
		if(step%4)step = (((step>>2)+1)<<2);	// align to 4-byte

		CwglImage org_ref(rt::_CastToNonconst(pSixFaceTiles),FaceSize,FaceSize*6,step,_Chan()*_BPV(),_BPV()==1);
		CwglImage img(org_ref,TRUE);

		img.GetSub(0,FaceSize*num::_meta_::_cube_coord::FaceId_Z_Neg,FaceSize,FaceSize).Rotate_180();	//Ratote z- by 180

		Apply();
		_CheckErrorGL;
		glPixelStorei(GL_PACK_ALIGNMENT,4);
		glPixelStorei(GL_UNPACK_ALIGNMENT,4);
		_CheckErrorGL;

		gluBuild2DMipmaps(GL_TEXTURE_CUBE_MAP_POSITIVE_X_ARB,TexInternalFormat,FaceSize,FaceSize,TexDefaultFormat,TexDefaultValue,
															img.GetPixelAddress(0,FaceSize*num::_meta_::_cube_coord::FaceId_X_Pos));
		gluBuild2DMipmaps(GL_TEXTURE_CUBE_MAP_NEGATIVE_X_ARB,TexInternalFormat,FaceSize,FaceSize,TexDefaultFormat,TexDefaultValue,
															img.GetPixelAddress(0,FaceSize*num::_meta_::_cube_coord::FaceId_X_Neg));
		gluBuild2DMipmaps(GL_TEXTURE_CUBE_MAP_POSITIVE_Y_ARB,TexInternalFormat,FaceSize,FaceSize,TexDefaultFormat,TexDefaultValue,
															img.GetPixelAddress(0,FaceSize*num::_meta_::_cube_coord::FaceId_Y_Pos));
		gluBuild2DMipmaps(GL_TEXTURE_CUBE_MAP_NEGATIVE_Y_ARB,TexInternalFormat,FaceSize,FaceSize,TexDefaultFormat,TexDefaultValue,
															img.GetPixelAddress(0,FaceSize*num::_meta_::_cube_coord::FaceId_Y_Neg));
		gluBuild2DMipmaps(GL_TEXTURE_CUBE_MAP_POSITIVE_Z_ARB,TexInternalFormat,FaceSize,FaceSize,TexDefaultFormat,TexDefaultValue,
															img.GetPixelAddress(0,FaceSize*num::_meta_::_cube_coord::FaceId_Z_Pos));
		gluBuild2DMipmaps(GL_TEXTURE_CUBE_MAP_NEGATIVE_Z_ARB,TexInternalFormat,FaceSize,FaceSize,TexDefaultFormat,TexDefaultValue,
															img.GetPixelAddress(0,FaceSize*num::_meta_::_cube_coord::FaceId_Z_Neg));
		_CheckErrorGL;

		TextureFilterSet(tx::FilterTrilinearMipmap,tx::FilterLinear);
		glTexParameteri(GL_TEXTURE_CUBE_MAP_ARB,GL_TEXTURE_MIN_FILTER,m_TexFilterMinify);
		glTexParameteri(GL_TEXTURE_CUBE_MAP_ARB,GL_TEXTURE_MAG_FILTER,m_TexFilterMagnify);
		_CheckErrorGL;
	}


	template<typename T>
	void DefineTexture(int mipmap_level, const num::CCubeMap<T>& cm)
	{
		if(cm.GetTiledImage().GetPadding())
		{
			int fsize = cm.GetFaceSize();
			rt::Buffer<typename T::PixelType> data;
			data.SetSize(6 * fsize* fsize );
			int	n = 0;
			for (int face = 0; face < 6; face ++)
			for (int i = 0; i < fsize; i ++)
			for (int j = 0; j < fsize; j ++)
			{
				data[n]	= cm.GetPixel(face,j,i);
				n ++;
			}
			DefineTexture(mipmap_level,fsize,data.Begin() );
		}	
		else
		{
			DefineTexture(mipmap_level,cm.GetFaceSize(),cm.GetTiledImage());
		}
	}

	void DefineTexture(int mipmap_level, UINT FaceSize,LPCVOID pSixFaceTiles_in)
	{
		int step = FaceSize*_Chan()*_BPV();
		if(step%4)step = (((step>>2)+1)<<2);	// align to 4-byte

		CwglImage org_ref(rt::_CastToNonconst(pSixFaceTiles_in),FaceSize,FaceSize*6,step,_Chan()*_BPV(),_BPV()!=1);
		CwglImage img(org_ref,TRUE);

		img.GetSub(0,FaceSize*num::_meta_::_cube_coord::FaceId_Z_Neg,FaceSize,FaceSize).Rotate_180();	//Ratote z- by 180

		Apply();
		_CheckErrorGL;
		glPixelStorei(GL_PACK_ALIGNMENT,4);
		glPixelStorei(GL_UNPACK_ALIGNMENT,4);
		_CheckErrorGL;

		glTexImage2D(GL_TEXTURE_CUBE_MAP_POSITIVE_X_ARB,mipmap_level,TexInternalFormat,FaceSize,FaceSize,0,TexDefaultFormat,TexDefaultValue,
														img.GetPixelAddress(0,FaceSize*num::_meta_::_cube_coord::FaceId_X_Pos));
		glTexImage2D(GL_TEXTURE_CUBE_MAP_NEGATIVE_X_ARB,mipmap_level,TexInternalFormat,FaceSize,FaceSize,0,TexDefaultFormat,TexDefaultValue,
														img.GetPixelAddress(0,FaceSize*num::_meta_::_cube_coord::FaceId_X_Neg));
		glTexImage2D(GL_TEXTURE_CUBE_MAP_POSITIVE_Y_ARB,mipmap_level,TexInternalFormat,FaceSize,FaceSize,0,TexDefaultFormat,TexDefaultValue,
														img.GetPixelAddress(0,FaceSize*num::_meta_::_cube_coord::FaceId_Y_Pos));
		glTexImage2D(GL_TEXTURE_CUBE_MAP_NEGATIVE_Y_ARB,mipmap_level,TexInternalFormat,FaceSize,FaceSize,0,TexDefaultFormat,TexDefaultValue,
														img.GetPixelAddress(0,FaceSize*num::_meta_::_cube_coord::FaceId_Y_Neg));
		glTexImage2D(GL_TEXTURE_CUBE_MAP_POSITIVE_Z_ARB,mipmap_level,TexInternalFormat,FaceSize,FaceSize,0,TexDefaultFormat,TexDefaultValue,
														img.GetPixelAddress(0,FaceSize*num::_meta_::_cube_coord::FaceId_Z_Pos));
		glTexImage2D(GL_TEXTURE_CUBE_MAP_NEGATIVE_Z_ARB,mipmap_level,TexInternalFormat,FaceSize,FaceSize,0,TexDefaultFormat,TexDefaultValue,
														img.GetPixelAddress(0,FaceSize*num::_meta_::_cube_coord::FaceId_Z_Neg));
		_CheckErrorGL;

		if(!mipmap_level)
		{	m_FaceSize = FaceSize;
			TextureFilterSet(tx::FilterLinear,tx::FilterLinear);
			glTexParameteri(GL_TEXTURE_CUBE_MAP_ARB,GL_TEXTURE_MIN_FILTER,m_TexFilterMinify);
			glTexParameteri(GL_TEXTURE_CUBE_MAP_ARB,GL_TEXTURE_MAG_FILTER,m_TexFilterMagnify);	
		}
		else
		{
			TextureFilterSet(tx::FilterTrilinearMipmap,tx::FilterLinear);
			glTexParameteri(GL_TEXTURE_CUBE_MAP_ARB,GL_TEXTURE_MIN_FILTER,m_TexFilterMinify);
			glTexParameteri(GL_TEXTURE_CUBE_MAP_ARB,GL_TEXTURE_MAG_FILTER,m_TexFilterMagnify);
		}

	}
};

typedef CwglCubemap<GL_RGB8,GL_RGB,GL_UNSIGNED_BYTE>			CwglCubemapTexture8u3c;
typedef CwglCubemap<GL_RGBA8,GL_RGBA,GL_UNSIGNED_BYTE>			CwglCubemapTexture8u4c;
typedef CwglCubemap<GL_RGB8,GL_BGR_EXT,GL_UNSIGNED_BYTE>		CwglCubemapTexture8u3cw;
typedef CwglCubemap<GL_RGBA8,GL_BGRA_EXT,GL_UNSIGNED_BYTE>		CwglCubemapTexture8u4cw;

typedef CwglCubemap<GL_RGB_FLOAT16_ATI,GL_RGB,GL_FLOAT>			CwglCubemapTexture16f3c;
typedef CwglCubemap<GL_RGBA_FLOAT16_ATI,GL_RGBA,GL_FLOAT>		CwglCubemapTexture16f4c;
typedef CwglCubemap<GL_RGB_FLOAT16_ATI,GL_BGR_EXT,GL_FLOAT>		CwglCubemapTexture16f3cw;
typedef CwglCubemap<GL_RGBA_FLOAT16_ATI,GL_BGRA_EXT,GL_FLOAT>	CwglCubemapTexture16f4cw;

typedef CwglCubemap<GL_RGB_FLOAT32_ATI,GL_RGB,GL_FLOAT>			CwglCubemapTexture32f3c;
typedef CwglCubemap<GL_RGBA_FLOAT32_ATI,GL_RGBA,GL_FLOAT>		CwglCubemapTexture32f4c;
typedef CwglCubemap<GL_RGB_FLOAT32_ATI,GL_BGR_EXT,GL_FLOAT>		CwglCubemapTexture32f3cw;
typedef CwglCubemap<GL_RGBA_FLOAT32_ATI,GL_BGRA_EXT,GL_FLOAT>	CwglCubemapTexture32f4cw;

}

//A class for short name ex::XXXXXX
#ifdef _DEBUG
	class ex:public ext::CwglExtensions{};
#else
	typedef ext::CwglExtensions ex;
#endif

//A class for short name mt::XXXXXX
#ifdef _DEBUG
	class mt:public ext::CwglMultiTextureEnv{};
#else
	typedef ext::CwglMultiTextureEnv mt;
#endif

//A class for short name vp::XXXXXX
#ifdef _DEBUG
	class vp:public ext::ARBvpfp::CwglVertexProgram{};
#else
	typedef ext::ARBvpfp::CwglVertexProgram vp;
#endif

void wglLoadAllExtensionEntryPoints();

};


