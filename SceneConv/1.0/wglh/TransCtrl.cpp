#include "StdAfx.h"
#include "TransCtrl.h"

namespace wglh{

num::Vec3f _normals[3] = {
	num::Vec3f(1,0,0),
	num::Vec3f(0,1,0),
	num::Vec3f(0,0,1)
};

void CMoveCtrl::prepareDrag(){
	USING_EXPRESSION;

	if(!m_bDragging){
		m_VisualSpaceView.glGet();
		
		num::Vec2f vXYZ[3];
		num::Vec2f vPos,vDir;
		m_VisualSpaceView.TransformGradient(GetTranslation_Ref(),vXYZ[0],vXYZ[1],vXYZ[2]);
		m_VisualSpaceView.Transform(GetTranslation_Ref(),vPos);
		
		//{
		//num::Vec3f vTest[3];
		//num::Vec2f vTemp2[3];
		//float diff = 0.1f;
		//vTest[0] = GetTranslation_Ref() + num::Vec3f(diff,0,0);
		//vTest[1] = GetTranslation_Ref() + num::Vec3f(0,diff,0);
		//vTest[2] = GetTranslation_Ref() + num::Vec3f(0,0,diff);

		//m_VisualSpaceView.Transform(vTest[0],vTemp2[0]);
		//m_VisualSpaceView.Transform(vTest[1],vTemp2[1]);
		//m_VisualSpaceView.Transform(vTest[2],vTemp2[2]);
		//std::cout << "vPos = " << vPos << "\n";
		//std::cout << vTemp2[0] << vTemp2[1] << vTemp2[2] << "\n";
		//}


		m_dragStart3DPos = GetTranslation_Ref();				
		m_dragStartGradient[0] = vXYZ[0];
		m_dragStartGradient[1] = vXYZ[1];
		m_dragStartGradient[2] = vXYZ[2];
	}
}

int CMoveCtrl::getDragType(int x,int y) const{
	
	USING_EXPRESSION;

	const int _DRAG_PIXEL_DIS = 10;
	const int _indices[3][2] = {{1,2},{2,0},{0,1}};
	num::Vec2f vPos,vDir;
	m_VisualSpaceView.Transform(m_dragStart3DPos,vPos);
	vDir = num::Vec2f(x,y) - vPos;
	
	float theta[3];

	int max_dir = -1;
	float max_dot = - FLT_MAX;
	for(int i=0;i<3;i++){
		float val = vDir.Dot(m_dragStartGradient[i]);
		if(val > 1e-6){
			val /= sqrt(m_dragStartGradient[i].L2Norm_Sqr());
		}
		if( val > max_dot){
			max_dot = val;
			max_dir = i;
		}
	}

	//std::cout << m_dragStartGradient[0] << m_dragStartGradient[1] << m_dragStartGradient[2] << "\n";
	//std::cout << "dir = " << vDir << "\n";
	//std::cout << "max_dot = " << max_dot << "\n";
	//std::cout << "dot_dis = " << sqrt(vDir.L2Norm_Sqr() - max_dot*max_dot) << "\n";

	//is dragging axis
	if(vDir.L2Norm_Sqr() - max_dot*max_dot < _DRAG_PIXEL_DIS*_DRAG_PIXEL_DIS){
		
		return max_dir;
	} else {	// is dragging plane
		vDir.Normalize();
		for(int i=0;i<3;i++){
			num::Vec2f vAxis[2],vN;			
			m_dragStartGradient[_indices[i][0]].NormalizeTo(vAxis[0] );
			m_dragStartGradient[_indices[i][1]].NormalizeTo(vAxis[1] );
			vN = vAxis[0] + vAxis[1];
			vN.Normalize();
			if(vDir.Dot(vN) > vAxis[0].Dot(vN)){
				return i + 3;
			}
		}
	}

	//std::cout << "error return -1 " << "\n";

	return -1;
}

void CMoveCtrl::MouseDown(int x,int y){
	
	m_draggingType = getDragType(x,y);
	if(m_draggingType>=0){
		m_bDragging = true;
		m_dragStartScreenPos = num::Vec2i(x,y);
	}

	m_mousePos = num::Vec2i(x,y);
	
	{
	//	std::cout << "Mouse Down :" << "\n";
	//	std::cout << vXYZ[0] << vXYZ[1] << vXYZ[2] << "\n";
	}
	

}
void CMoveCtrl::MouseMove(int x,int y){

	m_mousePos = num::Vec2i(x,y);

	if(m_bDragging){
		Update(x,y);
	}
}
void CMoveCtrl::MouseUp(int x,int y){

	m_mousePos = num::Vec2i(x,y);

	if(m_bDragging){
		Update(x,y);
	}
	m_bDragging = false;
	m_draggingType = -1;

	{
	//	std::cout << "Mouse Up :" << "\n";
	//	std::cout << vXYZ[0] << vXYZ[1] << vXYZ[2] << "\n";
	}
}

void CMoveCtrl::Update(int x,int y){
	USING_EXPRESSION;
	//if(m_bDragging){
	//	num::Vec2f vDir;
	//	vDir = num::Vec2f(x,y) - num::Vec2f(m_dragStartScreenPos.x,m_dragStartScreenPos.y);
	//	float dis = vDir.Dot(m_dragStartGradient[m_draggingAxis]) / m_dragStartGradient[m_draggingAxis].L2Norm_Sqr();
	//	GetTranslation_Ref() = m_dragStart3DPos;
	//	GetTranslation_Ref()[m_draggingAxis] += dis;

	//	std::cout << "trans = " << GetTranslation_Ref() << "\n";
	//}
	if(m_bDragging){

		if(m_draggingType < 3 && m_draggingType>=0){
			num::Vec2f vDir;
			vDir = num::Vec2f(x,y) - num::Vec2f(m_dragStartScreenPos.x,m_dragStartScreenPos.y);
			float dis = vDir.Dot(m_dragStartGradient[m_draggingType]) / m_dragStartGradient[m_draggingType].L2Norm_Sqr();
			m_VisualSpaceView.Transform(m_dragStart3DPos,vDir);
			GetTranslation_Ref() = m_dragStart3DPos;
			GetTranslation_Ref()[m_draggingType] += dis;
		} else if(m_draggingType >=3 && m_draggingType <6){
			num::Vec2f vDir;
			m_VisualSpaceView.Transform(m_dragStart3DPos,vDir);
			vDir = vDir + num::Vec2f(x,y) - num::Vec2f(m_dragStartScreenPos.x,m_dragStartScreenPos.y);
			num::Vec3f newPos;
			m_VisualSpaceView.InverseTransformToPlane(vDir,_normals[m_draggingType-3],m_dragStart3DPos[m_draggingType-3],
				newPos);
			GetTranslation_Ref() = newPos;
		}

		//float dis = vDir.Dot(m_dragStartGradient[m_draggingAxis]) / m_dragStartGradient[m_draggingAxis].L2Norm_Sqr();
		//GetTranslation_Ref() = m_dragStart3DPos;
		//GetTranslation_Ref()[m_draggingAxis] += dis;

		//std::cout << "trans = " << GetTranslation_Ref() << "\n";
	}
}

void CMoveCtrl::DrawCtrl(){

	using namespace num;
	const int _AXIS_LENGTH = 40;
	const int scale_max = 1e3;
	const int _indices[3][2] = {{1,2},{2,0},{0,1}};
	Vec3f colors[4] = {Vec3f(1,0,0),Vec3f(0,1,0),Vec3f(0,0,1),Vec3f(1,1,0)};
	Vec3f axis[3] = {Vec3f(1,0,0),Vec3f(0,1,0),Vec3f(0,0,1)};
	int colorIdx[3];

	float scale = FLT_MAX;
	for(int i=0;i<3;i++){
			
			if(m_dragStartGradient[i].L2Norm_Sqr()>1e-6){
				if( _AXIS_LENGTH / sqrt(m_dragStartGradient[i].L2Norm_Sqr()) < scale){
					scale = _AXIS_LENGTH / sqrt(m_dragStartGradient[i].L2Norm_Sqr());
				}
			}
	}

	for(int i=0;i<3;i++){
		colorIdx[i] = i;
		axis[i] *= scale;
	}
	if(m_draggingType >=0 && m_draggingType < 3){
		colorIdx[m_draggingType] = 3;
	} else if(m_draggingType>=3){
		colorIdx[_indices[m_draggingType-3][0]] = 3;
		colorIdx[_indices[m_draggingType-3][1]] = 3;
	}
	
	glPushAttrib(GL_ALL_ATTRIB_BITS);
    glDisable(GL_LIGHTING);
    glDisable(GL_DEPTH_TEST);
	glPushMatrix();	
		glLineWidth(3.0f);
        glBegin(GL_LINES);
		for(int i=0;i<3;i++){
			glColor3fv(colors[colorIdx[i]].data());
			glVertex3f(0,0,0);
			glVertex3fv(axis[i].data());
		}
        glEnd();
	glPopMatrix();
	glPopAttrib();
}


void CRotationCtrl::prepareDrag(){
	USING_EXPRESSION;

	if(!m_bDragging){
		m_VisualSpaceView.glGet();
		num::Vec4f vp = m_VisualSpaceView.GetViewport();
		m_rotArcball.SetWindowSize(vp[0],vp[1],vp[2]+vp[0],vp[3]+vp[1]);
	}
}

void CRotationCtrl::MouseDown(int x,int y){
	m_rotArcball.OnMouseDown(x,y);
	m_bDragging = true;
}
void CRotationCtrl::MouseMove(int x,int y){
	if(m_bDragging){
		m_rotArcball.OnMouseMove(x,y);
	}
}
void CRotationCtrl::MouseUp(int x,int y){
	if(m_bDragging){
		m_rotArcball.OnMouseUp(x,y);
		m_bDragging =false;
	}
}

void CRotationCtrl::MouseWheel(float f)
{
	m_fScale += f;
}

}