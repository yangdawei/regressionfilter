#include "StdAfx.h"
#include ".\wglfbo.h"
#include "..\w32\debug_log.h"

PFNGLISRENDERBUFFEREXTPROC						wglh::ext::CFrameBufferObjectBase::glIsRenderbufferEXT=NULL;
PFNGLBINDRENDERBUFFEREXTPROC					wglh::ext::CFrameBufferObjectBase::glBindRenderbufferEXT=NULL;
PFNGLDELETERENDERBUFFERSEXTPROC					wglh::ext::CFrameBufferObjectBase::glDeleteRenderbuffersEXT =NULL;
PFNGLGENRENDERBUFFERSEXTPROC					wglh::ext::CFrameBufferObjectBase::glGenRenderbuffersEXT =NULL;
PFNGLRENDERBUFFERSTORAGEEXTPROC					wglh::ext::CFrameBufferObjectBase::glRenderbufferStorageEXT =NULL;
PFNGLGETRENDERBUFFERPARAMETERIVEXTPROC			wglh::ext::CFrameBufferObjectBase::glGetRenderbufferParameterivEXT =NULL;
PFNGLISFRAMEBUFFEREXTPROC						wglh::ext::CFrameBufferObjectBase::glIsFramebufferEXT =NULL;
PFNGLBINDFRAMEBUFFEREXTPROC						wglh::ext::CFrameBufferObjectBase::glBindFramebufferEXT =NULL;
PFNGLDELETEFRAMEBUFFERSEXTPROC					wglh::ext::CFrameBufferObjectBase::glDeleteFramebuffersEXT =NULL;
PFNGLGENFRAMEBUFFERSEXTPROC						wglh::ext::CFrameBufferObjectBase::glGenFramebuffersEXT =NULL;
PFNGLCHECKFRAMEBUFFERSTATUSEXTPROC				wglh::ext::CFrameBufferObjectBase::glCheckFramebufferStatusEXT =NULL;
PFNGLFRAMEBUFFERTEXTURE1DEXTPROC				wglh::ext::CFrameBufferObjectBase::glFramebufferTexture1DEXT =NULL;
PFNGLFRAMEBUFFERTEXTURE2DEXTPROC				wglh::ext::CFrameBufferObjectBase::glFramebufferTexture2DEXT =NULL;
PFNGLFRAMEBUFFERTEXTURE3DEXTPROC				wglh::ext::CFrameBufferObjectBase::glFramebufferTexture3DEXT =NULL;
PFNGLFRAMEBUFFERRENDERBUFFEREXTPROC				wglh::ext::CFrameBufferObjectBase::glFramebufferRenderbufferEXT =NULL;
PFNGLGETFRAMEBUFFERATTACHMENTPARAMETERIVEXTPROC wglh::ext::CFrameBufferObjectBase::glGetFramebufferAttachmentParameterivEXT =NULL;
PFNGLGENERATEMIPMAPEXTPROC						wglh::ext::CFrameBufferObjectBase::glGenerateMipmapEXT =NULL;
GLint wglh::ext::CFrameBufferObjectBase::iMaxColorAttachment	= 0;
GLint wglh::ext::CFrameBufferObjectBase::iMaxRenderBufferSize	= 0;

PFNGLFRAMEBUFFERTEXTUREEXTPROC					wglh::ext::CFrameBufferObjectBase::FramebufferTextureEXT;
PFNGLFRAMEBUFFERTEXTURELAYEREXTPROC				wglh::ext::CFrameBufferObjectBase::FramebufferTextureLayerEXT;
PFNGLFRAMEBUFFERTEXTUREFACEEXTPROC				wglh::ext::CFrameBufferObjectBase::FramebufferTextureFaceEXT;

BOOL wglh::ext::CFrameBufferObjectBase::LoadExtensionEntryPoint()
{
	BOOL result = TRUE;

	glIsRenderbufferEXT = (PFNGLISRENDERBUFFEREXTPROC)wglGetProcAddress("glIsRenderbufferEXT");
	if (NULL == glIsRenderbufferEXT)
		result =  FALSE;
	glBindRenderbufferEXT = (PFNGLBINDRENDERBUFFEREXTPROC)wglGetProcAddress("glBindRenderbufferEXT");
	if (NULL == glBindRenderbufferEXT)
		result =  FALSE;
	glDeleteRenderbuffersEXT = (PFNGLDELETERENDERBUFFERSEXTPROC)wglGetProcAddress("glDeleteRenderbuffersEXT");
	if (NULL == glDeleteRenderbuffersEXT)
		result =  FALSE;
	glGenRenderbuffersEXT = (PFNGLGENRENDERBUFFERSEXTPROC)wglGetProcAddress("glGenRenderbuffersEXT");
	if (NULL == glGenRenderbuffersEXT)
		result =  FALSE;
	glRenderbufferStorageEXT = (PFNGLRENDERBUFFERSTORAGEEXTPROC)wglGetProcAddress("glRenderbufferStorageEXT");
	if (NULL == glRenderbufferStorageEXT)
		result =  FALSE;
	glGetRenderbufferParameterivEXT = (PFNGLGETRENDERBUFFERPARAMETERIVEXTPROC)wglGetProcAddress("glGetRenderbufferParameterivEXT");
	if (NULL == glGetRenderbufferParameterivEXT)
		result =  FALSE;
	glIsFramebufferEXT = (PFNGLISFRAMEBUFFEREXTPROC)wglGetProcAddress("glIsFramebufferEXT");
	if (NULL == glIsFramebufferEXT)
		result =  FALSE;
	glBindFramebufferEXT = (PFNGLBINDFRAMEBUFFEREXTPROC)wglGetProcAddress("glBindFramebufferEXT");
	if (NULL == glBindFramebufferEXT)
		result =  FALSE;
	glDeleteFramebuffersEXT = (PFNGLDELETEFRAMEBUFFERSEXTPROC)wglGetProcAddress("glDeleteFramebuffersEXT");
	if (NULL == glDeleteFramebuffersEXT)
		result =  FALSE;
	glGenFramebuffersEXT = (PFNGLGENFRAMEBUFFERSEXTPROC)wglGetProcAddress("glGenFramebuffersEXT");
	if (NULL == glGenFramebuffersEXT)
		result =  FALSE;
	glCheckFramebufferStatusEXT = (PFNGLCHECKFRAMEBUFFERSTATUSEXTPROC)wglGetProcAddress("glCheckFramebufferStatusEXT");
	if (NULL == glCheckFramebufferStatusEXT)
		result =  FALSE;
	glFramebufferTexture1DEXT = (PFNGLFRAMEBUFFERTEXTURE1DEXTPROC)wglGetProcAddress("glFramebufferTexture1DEXT");
	if (NULL == glFramebufferTexture1DEXT)
		result =  FALSE;
	glFramebufferTexture2DEXT = (PFNGLFRAMEBUFFERTEXTURE2DEXTPROC)wglGetProcAddress("glFramebufferTexture2DEXT");
	if (NULL == glFramebufferTexture2DEXT)
		result =  FALSE;
	glFramebufferTexture3DEXT = (PFNGLFRAMEBUFFERTEXTURE3DEXTPROC)wglGetProcAddress("glFramebufferTexture3DEXT");
	if (NULL == glFramebufferTexture3DEXT)
		result =  FALSE;
	glFramebufferRenderbufferEXT = (PFNGLFRAMEBUFFERRENDERBUFFEREXTPROC)wglGetProcAddress("glFramebufferRenderbufferEXT");
	if (NULL == glFramebufferRenderbufferEXT)
		result =  FALSE;
	glGetFramebufferAttachmentParameterivEXT = (PFNGLGETFRAMEBUFFERATTACHMENTPARAMETERIVEXTPROC)wglGetProcAddress("glGetFramebufferAttachmentParameterivEXT");
	if (NULL == glGetFramebufferAttachmentParameterivEXT)
		result =  FALSE;
	glGenerateMipmapEXT = (PFNGLGENERATEMIPMAPEXTPROC)wglGetProcAddress("glGenerateMipmapEXT");
	if (NULL == glGenerateMipmapEXT)
		result =  FALSE;
	FramebufferTextureEXT = (PFNGLFRAMEBUFFERTEXTUREEXTPROC)wglGetProcAddress("glFramebufferTextureEXT");
	//if (NULL == FramebufferTextureEXT)
		//result =  FALSE;
	FramebufferTextureLayerEXT = (PFNGLFRAMEBUFFERTEXTURELAYEREXTPROC)wglGetProcAddress("glFramebufferTextureLayerEXT");
	//if (NULL == FramebufferTextureLayerEXT)
		//result =  FALSE;
	FramebufferTextureFaceEXT = (PFNGLFRAMEBUFFERTEXTUREFACEEXTPROC)wglGetProcAddress("glFramebufferTextureFaceEXT");
	//if (NULL == FramebufferTextureFaceEXT)
		//result =  FALSE;

	if (result) 
	{
		printf("---GL_EXT_framebuffer_object Information---\n");
		
		glGetIntegerv(GL_MAX_COLOR_ATTACHMENTS_EXT,&iMaxColorAttachment);
		glGetIntegerv(GL_MAX_RENDERBUFFER_SIZE_EXT,&iMaxRenderBufferSize);

		printf("\tMaximum number of color buffer attachments: %d\n",iMaxColorAttachment);
		printf("\tMaximum width and height of renderbuffers : %d\n",iMaxRenderBufferSize);
	}
	else
	{
		printf("Load GL_EXT_framebuffer_object FAILED!\n");
	}

	return result;
}


BOOL wglh::ext::CFrameBufferObjectBase::CheckFramebufferStatus()
{
	GLenum status;
	status = (GLenum) glCheckFramebufferStatusEXT(GL_FRAMEBUFFER_EXT);
	switch(status) {
		case GL_FRAMEBUFFER_COMPLETE_EXT:
			//printf("Framebuffer complete\n");
			return TRUE;
			break;
		case GL_FRAMEBUFFER_UNSUPPORTED_EXT:
			printf("Unsupported framebuffer format\n");
			break;
		case GL_FRAMEBUFFER_INCOMPLETE_MISSING_ATTACHMENT_EXT:
			printf("Framebuffer incomplete, missing attachment\n");
			break;
		//case GL_FRAMEBUFFER_INCOMPLETE_DUPLICATE_ATTACHMENT_EXT:
		//	printf("Framebuffer incomplete, duplicate attachment\n");
		//	break;
		case GL_FRAMEBUFFER_INCOMPLETE_DIMENSIONS_EXT:
			printf("Framebuffer incomplete, attached images must have same dimensions\n");
			break;
		case GL_FRAMEBUFFER_INCOMPLETE_FORMATS_EXT:
			printf("Framebuffer incomplete, attached images must have same format\n");
			break;
		case GL_FRAMEBUFFER_INCOMPLETE_DRAW_BUFFER_EXT:
			printf("Framebuffer incomplete, missing draw buffer\n");
			break;
		case GL_FRAMEBUFFER_INCOMPLETE_READ_BUFFER_EXT:
			printf("Framebuffer incomplete, missing read buffer\n");
			break;
		default:
			printf("Framebuffer failed\n");
	}
	_CheckDumpSrcLoc;
	return FALSE;
}



UINT wglh::ext::CFrameBufferObject::GetFrameBufferName()
{
	return m_fbName;
}

BOOL wglh::ext::CFrameBufferObject::Create()
{
	if(m_fbName)
	{
		return TRUE;
	}
	else
	{
		if(glGenFramebuffersEXT==NULL)
			return FALSE;
		else
			glGenFramebuffersEXT(1,&m_fbName);
	}
	return TRUE;
}

void wglh::ext::CFrameBufferObject::Destroy()
{
	if(m_fbName)
	{
		//ApplyNull();
		glDeleteFramebuffersEXT(1,&m_fbName);
		m_fbName = 0;
	}
}

void wglh::ext::CFrameBufferObject::Apply()
{
	if(m_fbName)
		glBindFramebufferEXT(GL_FRAMEBUFFER_EXT, m_fbName);
	else
	{
		ASSERT(0);
		printf("Framebuffer Object hasn't been initialized\n");
	}
}

void wglh::ext::CFrameBufferObject::ApplyNull()
{
	if(m_fbName)
		glBindFramebufferEXT(GL_FRAMEBUFFER_EXT, 0);
	else
	{
		ASSERT(0);
		printf("Framebuffer Object hasn't been initialized\n");
	}
}

void wglh::ext::CFrameBufferObject::AttachTexture1D(GLenum attachment,GLuint textureName,GLint level)
{
	if(m_fbName)
	{
		Apply();
		glFramebufferTexture1DEXT(GL_FRAMEBUFFER_EXT,attachment,GL_TEXTURE_1D,textureName,level);
	}
	else
	{
		ASSERT(0);
		printf("Framebuffer Object hasn't been initialized\n");
	}
}

void wglh::ext::CFrameBufferObject::AttachTexture2D(GLenum attachment,GLenum textureTarget,GLuint textureName,GLint level)
{
	if(m_fbName)
	{
		Apply();
		glFramebufferTexture2DEXT(GL_FRAMEBUFFER_EXT,attachment,textureTarget,textureName,level);
	}
	else
	{
		ASSERT(0);
		printf("Framebuffer Object hasn't been initialized\n");
	}
}

void wglh::ext::CFrameBufferObject::AttachTexture3D(GLenum attachment,GLuint textureName,GLint level,GLint zOffset)
{
	if(m_fbName)
	{
		Apply();
		glFramebufferTexture3DEXT(GL_FRAMEBUFFER_EXT,attachment,GL_TEXTURE_3D,textureName,level,zOffset);
	}
	else
	{
		ASSERT(0);
		printf("Framebuffer Object hasn't been initialized\n");
	}
}

void wglh::ext::CFrameBufferObject::AttachTexture3DExt(GLenum attachment, GLuint textureName, GLint level)
{
	if(m_fbName)
	{
		ASSERT(FramebufferTextureEXT);
		Apply();
		FramebufferTextureEXT(GL_FRAMEBUFFER_EXT,attachment,textureName,level);
	}
	else
	{
		ASSERT(0);
		printf("Framebuffer Object hasn't been initialized\n");
	}
}

void wglh::ext::CFrameBufferObject::DetachTexture(GLenum attachment)
{
	if(m_fbName)
	{
		Apply();
		glFramebufferTexture2DEXT(GL_FRAMEBUFFER_EXT,attachment,GL_TEXTURE_2D,NULL,0);
	}
	else
	{
		ASSERT(0);
		printf("Framebuffer Object hasn't been initialized\n");
	}
}

void wglh::ext::CFrameBufferObject::AttachRenderBuffer(GLenum attachment,CRenderBuffer&rb)
{
	if(m_fbName)
	{
		Apply();
		GLuint rbName = rb.GetRenderBufferName();
		ASSERT(rbName);

		glFramebufferRenderbufferEXT(GL_FRAMEBUFFER_EXT,attachment,GL_RENDERBUFFER_EXT,rbName);
	}
	else
	{
		ASSERT(0);
		printf("Framebuffer Object hasn't been initialized\n");
	}
}

void wglh::ext::CFrameBufferObject::DetachRenderBuffer(GLenum attachment)
{
	if(m_fbName)
	{
		Apply();
		glFramebufferRenderbufferEXT(GL_FRAMEBUFFER_EXT,attachment,GL_RENDERBUFFER_EXT,NULL);
	}
	else
	{
		ASSERT(0);
		printf("Framebuffer Object hasn't been initialized\n");
	}
}

void wglh::ext::CFrameBufferObject::DrawBuffer(GLenum attachment)
{
	glDrawBuffer(attachment);
}

void wglh::ext::CFrameBufferObject::ReadBuffer(GLenum attachment)
{
	glReadBuffer(attachment);
}





