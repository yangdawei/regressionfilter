#include "stdafx.h"
#include "1.0/wglh/wglLight.h"
#include "GL/GLUT.h"

void wglh::CwglSpotLight::render()
{
	glColor3f(color.r, color.g, color.b);
	glutSolidSphere(0.1, 40, 40);
}
