#include "stdafx.h"
#include "wglImage.h"
#include "wglextension.h"
#include "..\w32\w32_misc.h"

#include <atlimage.h>
#pragma comment(lib,"gdiplus.lib")


wglh::CwglImage::CwglImage()
{
	ZeroMemory(this,sizeof(wglh::CwglImage));
}

wglh::CwglImage::CwglImage(const CwglSize &size, int BPP, BOOL HDR)
{
	pImg=NULL; SetSize(size.width,size.height,BPP,HDR);
}

wglh::CwglImage::CwglImage(LPVOID pData,int w,int h,int stp,int BytePP, BOOL HDR)  //Ref bitmap
{
	pImg=NULL;
	SetRef(pData,w,h,stp,BytePP,HDR);
}

BOOL wglh::CwglImage::SetSize(int w,int h,int BPP,BOOL HDR)
{
	_IsHDR = HDR;
	if( w == GetWidth() && h == GetHeight() && BPP == GetBPP() ){}
	else
	{
		if(!IsMemoryRefered)
			_SafeFree32AL(pImg);

		BytePerPixel = BPP/8;
		width = w;
		height = h;
		Step = BytePerPixel*width;
		if(Step%4!=0)Step = ((Step>>2)+1)<<2;

		pImg = _Malloc32AL(BYTE,GetDataSize());
		IsMemoryRefered = FALSE;
	}

	return IsNotNull();
}

void wglh::CwglImage::CopyTo(CwglImage &in)const
{
	ASSERT(in.GetBPP() == GetBPP());
	ASSERT(height == in.height);
	ASSERT(width == in.width);

	int step_src = GetStep();
	int step_dst = in.GetStep();

	LPCBYTE pLine = *this;
	LPBYTE  pLineDst = in;
	for(int i=0;i<height;i++)
	{
		memcpy(pLineDst,pLine,width*BytePerPixel);
		pLine += step_src;
		pLineDst += step_dst;
	}
}

wglh::CwglImage::CwglImage(const CwglImage &in, BOOL never_refer)
{
	if(in.IsMemoryRefered && !never_refer)
	{
		memcpy(this,&in,sizeof(CwglImage)); //keep memory refered
	}
	else
	{	IsMemoryRefered = TRUE;
		SetSize(in.width,in.height,in.GetBPP(),in._IsHDR);
		in.CopyTo(*this);
		IsMemoryRefered = FALSE;
	}
}

GLenum wglh::CwglImage::GetGlPixelFormat() const
{
	if(!_IsHDR)
	{
		switch(BytePerPixel)
		{
		case 1:
			return GL_ALPHA;
		case 3:
			return GL_RGB;
		case 4:
			return GL_RGBA;
		default:ASSERT(0);
			return GL_RGB;
		}
	}
	else
	{
		switch(BytePerPixel)
		{
		case 4:
			return GL_ALPHA_FLOAT32_ATI;
		case 12:
			return GL_RGB_FLOAT32_ATI;
		case 16:
			return GL_RGBA_FLOAT32_ATI;
		default:ASSERT(0);
			return GL_RGB_FLOAT32_ATI;
		}
	}
}

GLenum wglh::CwglImage::GetGlPixelValue() const	// GL_UNSIGNED_BYTE or GL_FLOAT
{
	return _IsHDR?GL_FLOAT:GL_UNSIGNED_BYTE;
}


BOOL wglh::CwglImage::Resize(int w,int h)
{
	if(w!=width || h!=height)
	{
		wglh::CwglImage tmp(wglh::CwglSize(w,h),GetBPP(),IsHDR());
		if(tmp.IsNotNull())
		{
			if(gluScaleImage(GetGlPixelFormat(),width,height,GetGlPixelValue(),pImg,w,h,GetGlPixelValue(),tmp.pImg))
			{
				_CheckErrorGL;
				_CheckDump("WGLH: gluScaleImage Failed\n");
				_CheckDumpSrcLoc;
				return FALSE;
			}
			rt::Swap(*this,tmp);

			return TRUE;
		}
		else return FALSE;
	}
	return TRUE;
}

void wglh::CwglImage::CopyFrameBuffer(int x,int y)
{
	GLenum format;
	if(!_IsHDR)
	{
		switch(BytePerPixel)
		{
		case 3: format = GL_RGB; break;
		case 4: format = GL_RGBA; break;
		case 1: format = GL_DEPTH_COMPONENT; break;
		}
	}
	else
	{
		switch(BytePerPixel)
		{
		case 12: format = GL_RGB; break;
		case 16: format = GL_RGBA; break;
		case 4: format = GL_DEPTH_COMPONENT; break;
		}
	}

	glPixelStorei(GL_UNPACK_ALIGNMENT,4);
	glPixelStorei(GL_PACK_ALIGNMENT,4);
	_CheckErrorGL;

	glReadPixels(x,y,width,height,format,GetGlPixelValue(),pImg);
	_CheckErrorGL;
	VerticalFlip();
}

void wglh::CwglImage::CopyTexture()
{
    glPixelStorei(GL_UNPACK_ALIGNMENT, 4);
    glGetTexImage(GL_TEXTURE_2D, 0, GetGlPixelFormat(), GetGlPixelValue(), pImg);
}

HGLOBAL wglh::CwglImage::GetDIB() const
{
	ASSERT(!_IsHDR);  // support LDR only

	w32::CDIB	dib;
	if(dib.Create(GetWidth(),GetHeight(),GetBPP()))
	{
		memcpy(dib.GetBits(),pImg,GetDataSize());
		dib.VerticalFlip();
		dib.ChannelFlip();
		return dib.Detach();
	}

	return NULL;
}


BOOL wglh::CwglImage::Load(LPCTSTR Fn,_tagTextureStorage LoadFrom)
{
	ASSERT(Fn);

	LPCTSTR pext = NULL;
	if(	LoadFrom == FT_TEXTURE_FILE &&
		(pext = _tcsrchr(Fn,_T('.'))) &&
		( pext[1] == _T('p') || pext[1] == _T('P') ) &&
		( pext[2] == _T('f') || pext[2] == _T('F') ) &&
		( pext[3] == _T('m') || pext[3] == _T('M') ) )
	{
		// load HDR
		::w32::image_codec::_PFM_Header header;
		HRESULT ret = _Open_PFM(Fn,&header);

		if(	SUCCEEDED(ret) &&
			SetSize(header.width,header.height,header.ch*32,TRUE) &&
			SUCCEEDED(::w32::image_codec::_Read_PFM(&header,(LPFLOAT)pImg,header.ch,GetStep())))
		{
			return TRUE;
		}
		else return FALSE;
	}
	else
	{
		ATL::CImage img;
		switch(LoadFrom)
		{
		case FT_TEXTURE_FILE:
			img.Load(Fn);
			break;
		case FT_TEXTURE_RESID:
			img.LoadFromResource(CwglApp::GetApp()->GetInstanceHandle(),(UINT)(MAKEINTRESOURCE(Fn)));
			break;
		case FT_TEXTURE_RESSTRING:
			img.LoadFromResource(CwglApp::GetApp()->GetInstanceHandle(),Fn);
			break;
		default:ASSERT(0);
		}

		if(!img.IsNull())
		{
			LPBYTE p = (LPBYTE)img.GetBits();

			BOOL ReuseMemory =	(width == img.GetWidth()) && 
								(height == img.GetHeight()) &&
								(BytePerPixel == img.GetBPP()/8);

			width = img.GetWidth();
			height = img.GetHeight();
			BytePerPixel = img.GetBPP()/8;
			//4-byte ALIGNMENT
			Step = BytePerPixel*width;
			if(Step%4!=0)Step = ((Step>>2)+1)<<2;
			ASSERT(abs(img.GetPitch()) == Step);  
			if(img.GetPitch()<0){ p = &p[img.GetPitch()*(height-1)]; }

			if( !ReuseMemory )
			{	if(!IsMemoryRefered)_SafeDelArray(pImg);
				pImg = _Malloc32AL(BYTE,GetDataSize());
				IsMemoryRefered = FALSE;
			}
			ASSERT(pImg);
			memcpy(pImg,p,GetDataSize());
			if(img.GetPitch()<0)VerticalFlip();

			if( BytePerPixel >= 3)
			{// BGR->RGB
				for(UINT y=0;y<(UINT)height;y++)
				{	LPBYTE p = &pImg[Step*y];
					LPBYTE pend = &p[width*BytePerPixel];
					for(;p<pend;p+=BytePerPixel)
						rt::Swap(p[0],p[2]);
				}
			}

			return TRUE;
		}
		else
		{	_CheckDump("WGLH: Failed to Load :[ ");
			if(FT_TEXTURE_RESID == LoadFrom)
				_CheckDump('#'<<((unsigned short)Fn));
			else
				_CheckDump(Fn);
			_CheckDump(" ]\n");
			_CheckErrorW32;
			_CheckDumpSrcLoc;

			return FALSE;
		}
	}
}

BOOL wglh::CwglImage::Save(LPCTSTR fn) const
{
	ASSERT(pImg);

	LPCTSTR pext = _tcsrchr(fn,_T('.'));
	if(pext &&	( pext[1] == _T('p') || pext[1] == _T('P') ) &&
				( pext[2] == _T('f') || pext[2] == _T('F') ) &&
				( pext[3] == _T('m') || pext[3] == _T('M') ) )
	{
		if(!_IsHDR)return FALSE;
		return SUCCEEDED(::w32::image_codec::_Write_PFM(fn,(LPCFLOAT)pImg,BytePerPixel/4,width,height,GetStep()));
	}
	else
	{
		if(_IsHDR)return FALSE;
		ATL::CImage	img;
		if(img.Create(width,-height,BytePerPixel*8,BytePerPixel==4?ATL::CImage::createAlphaChannel:0))
		{
			ASSERT(img.GetPitch() == Step);  //no additional padding
			memcpy(img.GetBits(),pImg,Step*height);

			if( BytePerPixel >= 3)
			{// BGR->RGB
				LPBYTE pFile = (LPBYTE)img.GetBits();
				for(UINT y=0;y<(UINT)height;y++)
				{	LPBYTE p = &pFile[Step*y];
					LPBYTE pend = &p[width*BytePerPixel];
					for(;p<pend;p+=BytePerPixel)
						rt::Swap(p[0],p[2]);
				}
			}

			if( BytePerPixel == 1 )	// index-color image, setup color-table
			{	RGBQUAD	ct[256];
				for(int i=0;i<256;i++)
				{	ct[i].rgbRed = i;
					ct[i].rgbGreen = i;
					ct[i].rgbBlue = i;
					ct[i].rgbReserved = 0;
				}
				img.SetColorTable(0,256,ct);
			}

			return SUCCEEDED(img.Save(fn));
		}
	}

	return FALSE;
}

void wglh::CwglImage::VerticalFlip()
{
	ASSERT(pImg);

	LPBYTE pLine = pImg;
	LPBYTE pEnd  = &pImg[Step*((int)(height/2))];
	int lineid = 0;
	for(;pLine<pEnd;pLine+=Step,lineid++)
	{
		LPDWORD p = (LPDWORD)pLine;
		LPDWORD pM = (LPDWORD)(pImg+Step*(height-1-lineid));
		LPDWORD end = (LPDWORD)&pLine[width*BytePerPixel];
		for(;p<end;p++,pM++)
		{
			rt::Swap(*p,*pM);
		}
	}
}

void wglh::CwglImage::HorizontalFlip()
{
	ASSERT(pImg);

	LPBYTE pLine = pImg;
	LPBYTE pEnd  = &pImg[Step*height];
	
	if( BytePerPixel == 3)
	{
		for(;pLine<pEnd;pLine+=Step)
		{
			LPBYTE p = pLine;
			LPBYTE pM = &pLine[ 3*(width-1) ];
			for(;p<pM;p+=3,pM-=3)
			{
				rt::Swap(*((short*)p),*((short*)pM));
				rt::Swap(p[2],pM[2]);
			}
		}
	}
	else if( BytePerPixel == 12 )
	{
		for(;pLine<pEnd;pLine+=Step)
		{
			LPDWORD p = (LPDWORD)pLine;
			LPDWORD pM = (LPDWORD)&pLine[ 12*(width-1) ];
			for(;p<pM;p+=3,pM-=3)
			{
				rt::Swap(p[0],pM[0]);
				rt::Swap(p[1],pM[1]);
				rt::Swap(p[2],pM[2]);
			}
		}
	}
	else if( BytePerPixel == 4 )
	{
		for(;pLine<pEnd;pLine+=Step)
		{
			LPDWORD p = (LPDWORD)pLine;
			LPDWORD pM = (LPDWORD)&pLine[ 4*(width-1) ];
			for(;p<pM;p++,pM--)
			{
				rt::Swap(*p,*pM);
			}
		}
	}
	else if( BytePerPixel == 16 )
	{
		for(;pLine<pEnd;pLine+=Step)
		{
			LPDWORD p = (LPDWORD)pLine;
			LPDWORD pM = (LPDWORD)&pLine[ 16*(width-1) ];
			for(;p<pM;p+=4,pM-=4)
			{
				rt::Swap(p[0],pM[0]);
				rt::Swap(p[1],pM[1]);
				rt::Swap(p[2],pM[2]);
				rt::Swap(p[3],pM[3]);
			}
		}
	}
	else if( BytePerPixel == 1 )
	{
		for(;pLine<pEnd;pLine+=Step)
		{
			LPBYTE p = pLine;
			LPBYTE pM = &pLine[ (width-1) ];
			for(;p<pM;p++,pM--)
			{
				rt::Swap(*p,*pM);
			}
		}
	}
	else
	{
		ASSERT(0);
	}
}

void wglh::CwglImage::TransposeFlip()
{
	ASSERT(pImg);
	ASSERT(width == height);

	int y=0;
	LPVOID pEnd = &pImg[ (height-1)*Step + width*BytePerPixel ];

	if( BytePerPixel == 3)
	{
		for(;y<height-1;y++)
		{
			LPBYTE pH = &pImg[ y*Step + (y + 1)*3 ];
			LPBYTE pV = &pImg[ (y+1)*Step + 3*y ];
			for(;pV<pEnd;pH+=3,pV+=Step)
			{
				rt::Swap(*((short*)pH),*((short*)pV));
				rt::Swap(pH[2],pV[2]);
			}
		}
	}
	else if( BytePerPixel == 12)
	{
		for(;y<height-1;y++)
		{
			LPDWORD pH = (LPDWORD)&pImg[ y*Step + (y + 1)*12 ];
			LPDWORD pV = (LPDWORD)&pImg[ (y+1)*Step + 12*y ];
			for(;pV<pEnd;pH+=3,pV+=Step/4)
			{
				rt::Swap(pH[0],pV[0]);
				rt::Swap(pH[1],pV[1]);
				rt::Swap(pH[2],pV[2]);
			}
		}
	}
	else if( BytePerPixel == 4 )
	{
		ASSERT( Step % 4 == 0);

		for(;y<height-1;y++)
		{
			LPDWORD pH = (LPDWORD)&pImg[ y*Step + (y + 1)*4 ];
			LPDWORD pV = (LPDWORD)&pImg[ (y+1)*Step + 4*y ];
			for(;pV<pEnd;pH++,pV+=Step/4)
			{
				rt::Swap(*pH,*pV);
			}
		}
	}
	else if( BytePerPixel == 16 )
	{
		ASSERT( Step % 4 == 0);

		for(;y<height-1;y++)
		{
			LPDWORD pH = (LPDWORD)&pImg[ y*Step + (y + 1)*16 ];
			LPDWORD pV = (LPDWORD)&pImg[ (y+1)*Step + 16*y ];
			for(;pV<pEnd;pH+=4,pV+=Step/4)
			{
				rt::Swap(pH[0],pV[0]);
				rt::Swap(pH[1],pV[1]);
				rt::Swap(pH[2],pV[2]);
				rt::Swap(pH[3],pV[3]);
			}
		}
	}
	else if( BytePerPixel == 1 )
	{
		for(;y<height-1;y++)
		{
			LPBYTE pH = &pImg[ y*Step + y + 1 ];
			LPBYTE pV = &pImg[ y*Step + y + Step ];
			for(;pV<pEnd;pH++,pV+=Step)
			{
				rt::Swap(*pH,*pV);
			}
		}
	}
	else
	{
		ASSERT(0);
	}
}


