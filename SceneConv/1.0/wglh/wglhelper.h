#pragma once

//////////////////////////////////////////////////////////////////////
// Visual Computing Foundation Classes (VCFC)
//					Jiaping Wang  2006.2
//					e_boris2002@hotmail.com
//
// (C) Copyright Jiaping Wang 2004.
//
// Permission to copy, use, modify, sell and distribute this software
// is granted provided this copyright notice appears in all copies.
// This software is provided "as is" without express or implied
// warranty, and with no claim as to its suitability for any purpose.
//
// Absolutely no warranty!!
//////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////
//  Brief Description
//
//  wglhelper.h
//
//  basic functions of OpenGL, fix pipeline
//
//////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////
// Revise History
//
// Inital version	2003.?.?		Jiaping
//
//////////////////////////////////////////////////////////////////////


#include "..\w32\win32_ver.h"
#include "..\num\small_mat.h"
#include <mmsystem.h>

#include "inc\gl.h"
#include "inc\glu.h"

#include "inc\glext.h"
#include "inc\wglext.h"

#pragma comment(lib,"opengl32.lib")
#pragma comment(lib,"glu32.lib")
#pragma comment(lib,"winmm.lib")

#include "..\num\small_vec.h"
#include "..\w32\debug_log.h"
#include "..\w32\wnd_app.h"

#pragma warning(disable:1684)

#define WGLH_FUNC	__forceinline

#ifdef WGLH_REFRESH_SYNC
#error Remove definition of WGLH_REFRESH_SYNC, vertical synchronization is now control by wglh::CwglWnd::SetVerticalSynchronization
#endif

namespace wglh
{

void DumpOpenGLInformation(HWND	GL_Wnd = NULL);
BOOL wglCheckError();

class CwglWnd;

class CwglApp
{
	friend class CwglWnd;
private:
	static CwglApp *	g_pAppInstance;
	BOOL		_bRealtimeRenderingDriven;
protected:
	BOOL		m_bRealtimeRenderingPaused;
	BOOL		m_bInRealtimeMode;
	
	LPCTSTR		CmdLine;
	CwglWnd *	pMainWnd;
	HINSTANCE	hInstance;

public:
#ifdef _DEBUG
	static BOOL		m_bOpenGLErrorFired;
#endif

	static CwglApp * GetApp();
	CwglApp(BOOL reserved = FALSE);  //Only one instance per-process
	~CwglApp();

	HINSTANCE	GetInstanceHandle() const;
	CwglWnd *	GetMainWnd();
	BOOL		IsRealtimeActived() const;
	void		ActiveRealtimeMode(BOOL active = TRUE);
	void		Pause(BOOL pause_it);
	BOOL		IsPaused() const;
	UINT		Run(LPTSTR pCmd=NULL);
	BOOL		IsIdleDrivenRendering() const;
};
WGLH_FUNC CwglApp* GetApp(){ return CwglApp::GetApp(); }

#pragma warning(disable:1684)


#if ( !defined(W32_DISABLE_LOGGING) && defined(_DEBUG) )
	#define _CheckErrorGL	{if(wglh::wglCheckError()){w32::Logging::DumpSourceLocation(__LINE__,_T(__FILE__),_T(__FUNCTION__));}}
#else
	#define _CheckErrorGL	{while(0);}
#endif

class CwglRenderContext
{
private:
	HWND AssocWnd;
protected:
	HDC	dc;
	HGLRC rc;
public:
	CwglRenderContext(){ZeroMemory(this,sizeof(CwglRenderContext));}
	~CwglRenderContext(){if(rc)Destroy();}
	operator HGLRC ()const{return rc;}
	operator HDC   ()const{return dc;}
	BOOL MakeCurrent(BOOL DoCurrent = TRUE) const;
	BOOL Create(HWND hWnd = NULL,DWORD AddFlags=0,UINT ColorBits = 24,UINT DepthBits = 24,UINT StencilBits = 0,UINT AccumBits = 0,UINT AlphaBits = 0,UINT AuxBuf = 0,BOOL DoubleBuf = TRUE);
	BOOL Create(HDC hDc);
	BOOL IsOk() const{ return rc!=0; }
	UINT GetPixelBits()const{ ASSERT(dc); return GetDeviceCaps(dc,BITSPIXEL);}
	void Destroy();
};

class CwglSize
{
public:
	CwglSize(){width = 0; height = 0;}
	CwglSize(int cx,int cy){ width = cx; height = cy; }
	CwglSize & operator = (const CwglSize &in)
	{
		width = in.width;
		height = in.height;
		return *this;
	}
	int width;
	int height;
};

class CwglTransformation //map 3d space to 2d image space
{
protected:
	num::Vec4f m_TotalTransformMatrix[4]; // row-major matrix, different from openGL
	num::Mat3x3f m_viewRotMat;
	num::Vec4f m_viewport;
public:
	// the two method are not efficent due to matrix transpose
	void  glGet( BOOL involve_viewport = TRUE );

    void  GetMatrix4x4(num::Mat4x4f &mat);
	num::Mat3x3f  GetRotationMat() const {return m_viewRotMat;}
	num::Vec4f GetViewport()const {return m_viewport;}

	// is direction backward
	template< typename T1 >
	DYNTYPE_FUNC BOOL IsBackward(const num::Vec3<T1>& x) const
	{	float z = (x.Dot(m_TotalTransformMatrix[2].v3()) + m_TotalTransformMatrix[2][3]) / 
				  (x.Dot(m_TotalTransformMatrix[3].v3()) + m_TotalTransformMatrix[3][3]);
		float z_0 = m_TotalTransformMatrix[2][3] / m_TotalTransformMatrix[3][3];
		return z<z_0;
	};

	// transform vec4
	template< typename T1, typename T2>
	DYNTYPE_FUNC void  Transform(const num::Vec4<T1>& x, num::Vec4<T2>& y) const  //unnormalized coordinate
	{	ASSERT(((size_t)(&x))!=((size_t)(&y))); // can not transform inplace
		y[0] = x.Dot(m_TotalTransformMatrix[0]);
		y[1] = x.Dot(m_TotalTransformMatrix[1]);
		y[2] = x.Dot(m_TotalTransformMatrix[2]);
		y[3] = x.Dot(m_TotalTransformMatrix[3]);
	}
	template< typename T1, typename T2>
	DYNTYPE_FUNC void  Transform(const num::Vec4<T1>& x, num::Vec3<T2>& y) const  //normalized coordinate
	{	ASSERT(((size_t)(&x))!=((size_t)(&y))); // can not transform inplace
		float w = 1.0f/x.Dot(m_TotalTransformMatrix[3]);
		y[0] = x.Dot(m_TotalTransformMatrix[0])*w;
		y[1] = x.Dot(m_TotalTransformMatrix[1])*w;
		y[2] = x.Dot(m_TotalTransformMatrix[2])*w;
	}
	template< typename T1, typename T2>
	DYNTYPE_FUNC void  Transform(const num::Vec4<T1>& x, num::Vec2<T2>& y) const
	{	ASSERT(((size_t)(&x))!=((size_t)(&y))); // can not transform inplace
		float w = 1.0f/x.Dot(m_TotalTransformMatrix[3]);
		y[0] = x.Dot(m_TotalTransformMatrix[0])*w;
		y[1] = x.Dot(m_TotalTransformMatrix[1])*w;
	}
	// transform vec3
	template< typename T1, typename T2>
	DYNTYPE_FUNC void Transform(const num::Vec3<T1>& x, num::Vec3<T2>& y) const
	{	ASSERT(((size_t)(&x))!=((size_t)(&y))); // can not transform inplace
		float w = 1.0f/(x.Dot(m_TotalTransformMatrix[3].v3()) + m_TotalTransformMatrix[3][3]);
		y[0] = (x.Dot(m_TotalTransformMatrix[0].v3()) + m_TotalTransformMatrix[0][3])*w;
		y[1] = (x.Dot(m_TotalTransformMatrix[1].v3()) + m_TotalTransformMatrix[1][3])*w;
		y[2] = (x.Dot(m_TotalTransformMatrix[2].v3()) + m_TotalTransformMatrix[2][3])*w;
	}
	template< typename T1, typename T2>
	DYNTYPE_FUNC void Transform(const num::Vec3<T1>& x, num::Vec2<T2>& y) const
	{	ASSERT(((size_t)(&x))!=((size_t)(&y))); // can not transform inplace
		float w = 1.0f/(x.Dot(m_TotalTransformMatrix[3].v3()) + m_TotalTransformMatrix[3][3]);
		y[0] = (x.Dot(m_TotalTransformMatrix[0].v3()) + m_TotalTransformMatrix[0][3])*w;
		y[1] = (x.Dot(m_TotalTransformMatrix[1].v3()) + m_TotalTransformMatrix[1][3])*w;
	}
	
	DYNTYPE_FUNC void InverseTransformToPlane(const num::Vec2f& y, const num::Vec3f& planeNormal, float planeVal, num::Vec3f& x) const
	{	ASSERT(((size_t)(&x))!=((size_t)(&y))); // can not transform inplace

		USING_EXPRESSION;

		num::Mat3x3f f,fInv;
		num::Vec3f row[3],b;
		row[0] = m_TotalTransformMatrix[3].v3() * y[0] - m_TotalTransformMatrix[0].v3();
		row[1] = m_TotalTransformMatrix[3].v3() * y[1] - m_TotalTransformMatrix[1].v3();
		row[2] = planeNormal;
		f.SetRow(0,row[0]);		f.SetRow(1,row[1]); 		f.SetRow(2,row[2]);
		b[0] =  m_TotalTransformMatrix[0][3] - m_TotalTransformMatrix[3][3] * y[0];
		b[1] =  m_TotalTransformMatrix[1][3] - m_TotalTransformMatrix[3][3] * y[1];
		b[2] = planeVal;
		f.Inverse(fInv);

		x = fInv.Product(b);
	}

	template< typename T1, typename T2>
	DYNTYPE_FUNC void TransformGradient(const num::Vec3<T1>& x, num::Vec2<T2>& xGradient,num::Vec2<T2>& yGradient,num::Vec2<T2>& zGradient) const
	{
		USING_EXPRESSION;

		//num::Vec2<T2> y[4];
		//num::Vec3<T1> xDiff[3];
		//float diff = 0.1;
		//xDiff[0] = x + num::Vec3<T1>(diff,0,0);
		//xDiff[1] = x + num::Vec3<T1>(0,diff,0);
		//xDiff[2] = x + num::Vec3<T1>(0,0,diff);
		//Transform(x,y[0]);
		//Transform(xDiff[0],y[1]); Transform(xDiff[1],y[2]);Transform(xDiff[2],y[3]);
		//xGradient = (y[1] - y[0])/diff;
		//yGradient = (y[2] - y[0])/diff;
		//zGradient = (y[3] - y[0])/diff;

		num::Vec2<T2> y;	

		float w = 1.0f/(x.Dot(m_TotalTransformMatrix[3].v3()) + m_TotalTransformMatrix[3][3]);
		y[0] = (x.Dot(m_TotalTransformMatrix[0].v3()) + m_TotalTransformMatrix[0][3])*w;
		y[1] = (x.Dot(m_TotalTransformMatrix[1].v3()) + m_TotalTransformMatrix[1][3])*w;

		USING_EXPRESSION;

		num::Vec3<T2> g1,g2;
		g1 = m_TotalTransformMatrix[0].v3() * w - m_TotalTransformMatrix[3].v3() * y[0] * w * w;
		g2 = m_TotalTransformMatrix[1].v3() * w - m_TotalTransformMatrix[3].v3() * y[1] * w * w;
	
		xGradient = num::Vec2<T2> (g1[0],g2[0]);
		yGradient = num::Vec2<T2> (g1[1],g2[1]);
		zGradient = num::Vec2<T2> (g1[2],g2[2]);

	}
};

class CwglTimer
{
protected:
	HANDLE  hEvent;
	UINT	Interval;
	BOOL	AutoReset;
	UINT	Resolution;

	UINT	TimeID;
public:
	CwglTimer();
	~CwglTimer();
	void SetTimer(UINT Interval,BOOL UseEventPulse = TRUE,UINT Resolution = 1);
	BOOL Start();
	void Stop();
	WGLH_FUNC void Wait()
	{
		if(TimeID)
		{
			if(WaitForSingleObject(hEvent,INFINITE) == WAIT_OBJECT_0)
			{
				if(!AutoReset)ResetEvent(hEvent);
			}
			else
			{
				_CheckErrorW32;
				Stop();
			}
		}
		else
			Sleep(0);
	}
	operator BOOL(){return (BOOL)TimeID;}
};

namespace ext
{ class _SetOpenGLExtensionEntrypointLoader; }

class CwglWnd:public w32::CWndBase,
			  public CwglSize
{
	friend class CwglApp;
	friend class wglh::ext::_SetOpenGLExtensionEntrypointLoader;
	static void (*_OpenGLExtensionEntrypointLoader)();
	PROC				_SwapIntervalExt;
private:
	BOOL				_bVerticalSynchronized;
	BOOL				_bInitialized;
	static LPCSTR		pOpenGLExtensionsString;
	BOOL				_bSizeChanged;
	BOOL				_bSizeMINIMIZED;

	// state for returning from screen coordinate mode
	BOOL				_Z_test_enabled;
	BOOL				_Culling_enabled;

protected:
	float				_DepthMin;
	float				_DepthMax;
	float				_FOV;

protected:
	CwglRenderContext	glRc;
	double				m_fRenderFPS;

	virtual void OnRender(){};
	virtual void OnInitScene();
	virtual void OnUninitScene(){};
	virtual void OnViewportChanged(int w,int h){};
	BOOL OnInitWnd();

protected:
	/////////////////////////////////
	//All changable settings:
	float			m_RenderAreaAspectRatio;
	void			UpdateViewportLayout();


public:
	const	CwglRenderContext& GetRC()const{ return glRc; }
	void	Render(){ Apply(); OnRender(); }
	void	DrawUnitBox();

	CwglWnd(void);
	~CwglWnd(void){ UninitGL(); }  //delayed for derived class to clear up opengl stuffs
	virtual BOOL InitGL(DWORD AddFlags=0,UINT ColorBits = 24,UINT DepthBits = 24,UINT StencilBits = 0,UINT AccumBits = 0,UINT AlphaBits = 0,UINT AuxBuf = 0,BOOL DoubleBuf = TRUE);
	void UninitGL();
	BOOL IsGLInitialized(){ return glRc.IsOk(); }
	void SetWindowAspect(float Aspect = -1);
	BOOL IsInitialized(){ return _bInitialized; }
	void SetPolygonMode(BOOL ForceWireframe,BOOL Cullback,BOOL All_Filled = FALSE);
	void ScreenCoordinate_Begin(float DepthMin = 1.0f, float DepthMax = 20.0f);
	void ScreenCoordinate_End();
	void SetAsMainWnd();
	void Apply(){ if(glRc.IsOk()){ glRc.MakeCurrent(); _CheckErrorGL; } }
	BOOL IsVerticalSynchronized();
	void SetVerticalSynchronization(BOOL Synchronized = TRUE);

//////////////////////////////////////
//Static helper functions
public:
	static int  GetRefreshFreq();
	static BOOL CheckGLExtension(LPCSTR ExtName)
	{	ASSERT(ExtName);
		return (pOpenGLExtensionsString && strstr(pOpenGLExtensionsString,ExtName));
	}

	static LPCSTR GetGLExtension(){ return pOpenGLExtensionsString; }

	void SetGLView(int left,int top, int right, int bottom,float DepthMin, float DepthMax, float Fov);
	void SetGLView(int left,int top, int right, int bottom);
	void SetFOV(float Fov = 90.0f);
	void GetFrameBuffer(LPBYTE pPixel,GLenum format = GL_RGB);

protected:
	LRESULT WndProc(UINT message, WPARAM wParam, LPARAM lParam);
};


class tx
{
protected:
	//////////////////////////////////
	GLuint TexName;

	void	_GenTextureName()
	{
	    if(!TexName)glGenTextures(1, &TexName);
		_CheckErrorGL;
		ASSERT(TexName);
	}

public:
	enum TextureFilter {      
		FilterNearest			= GL_NEAREST,  //  GL_NEAREST 
		FilterLinear			= GL_LINEAR,  //  GL_LINEAR  
		FilterNearestMipmap		= GL_NEAREST_MIPMAP_NEAREST,  //GL_NEAREST_MIPMAP_NEAREST
		FilterLinearMipmap		= GL_NEAREST_MIPMAP_LINEAR,   //GL_NEAREST_MIPMAP_LINEAR
		FilterBilinearMipmap	= GL_LINEAR_MIPMAP_NEAREST, //  GL_LINEAR_MIPMAP_NEAREST
		FilterTrilinearMipmap	= GL_LINEAR_MIPMAP_LINEAR //  GL_LINEAR_MIPMAP_LINEAR
	};

	enum TextureWrapMode {
		WrapRepeat = GL_REPEAT,
		WrapClamp  = GL_CLAMP,
		WrapClampToEdge	= GL_CLAMP_TO_EDGE
	};

	enum TextureEnvMode {
		EnvModulate	= GL_MODULATE,
		EnvDecal	= GL_DECAL,
		EnvBlend	= GL_BLEND,
		EnvReplace	= GL_REPLACE,
		EnvAdd		= GL_ADD
	};

	tx(){ TexName = 0; }
	~tx(){ Destroy(); }
	void	Destroy()
	{
		if(TexName)
		{
			glDeleteTextures(1,&TexName);
			_CheckErrorGL;
			TexName = 0;
		}
	}

	BOOL IsTexture(){ return TexName; }
	GLuint GetTextureName(){ return TexName;}
	static void TextureEnvMode(UINT mode = EnvModulate)  //GL_MODULATE GL_DECAL GL_BLEND GL_REPLACE GL_ADD
	{
		glTexEnvf(GL_TEXTURE_ENV,GL_TEXTURE_ENV_MODE,(GLfloat)mode); 
		_CheckErrorGL;
	}
};

template<UINT tTexTarget>
class CwglTextureBase:public tx
{
protected:

public:
	static	void Enable()
	{
		glEnable(tTexTarget);
	}

	static	void Disable()
	{
		glDisable(tTexTarget);
	}

	void Apply()
	{
		_GenTextureName();
		glBindTexture(tTexTarget, TexName);
		_CheckErrorGL;
	}

	static void TextureFilterSet(TextureFilter minify=CwglTextureBase::FilterLinear,TextureFilter magnify=CwglTextureBase::FilterLinear,GLfloat AnisotropyParam = 1.0f)
	{
		glTexParameteri(tTexTarget,GL_TEXTURE_MIN_FILTER,minify);
		glTexParameteri(tTexTarget,GL_TEXTURE_MAG_FILTER,magnify);
		_CheckErrorGL;

	#ifdef WGLH_ENABLE_ANISOTROPY_FILTER
		glTexParameterf(tTexTarget,GL_TEXTURE_MAX_ANISOTROPY_EXT, AnisotropyParam);
		_CheckErrorGL;
	#endif
	}

	static void TextureWrapMode(TextureWrapMode s_axis = tx::WrapClamp,TextureWrapMode t_axis = tx::WrapClamp)
	{
		glTexParameteri(tTexTarget,GL_TEXTURE_WRAP_S,s_axis);
		glTexParameteri(tTexTarget,GL_TEXTURE_WRAP_T,t_axis);
		_CheckErrorGL;
	}

	static void TextureBorderColor(num::Vec4f &color)
	{
		glTexParameterfv(tTexTarget,GL_TEXTURE_BORDER_COLOR,color);
		_CheckErrorGL;
	}

	GLint GetWidth()
	{
		Apply();

		GLint	x;
		glGetTexLevelParameteriv(tTexTarget,0,GL_TEXTURE_WIDTH,&x);
		_CheckErrorGL;
		return x;
	}

	GLint GetHeight()
	{
		Apply();

		GLint	x;
		glGetTexLevelParameteriv(tTexTarget,0,GL_TEXTURE_HEIGHT,&x);
		_CheckErrorGL;
		return x;
	}

	GLuint GetComponent()
	{
		Apply();

		GLint	x;
		glGetTexLevelParameteriv(tTexTarget,0,GL_TEXTURE_COMPONENTS,&x);

		if(x<=4){}
		else
		{
			if( x == GL_ALPHA || x == GL_ALPHA8 )
			{
				return 1;
			}
			else if( x == GL_RGB8 || x == GL_RGB )
			{
				return 3;
			}
			else if( x == GL_RGBA8 || x == GL_RGBA )
			{
				return 4;
			}
		}

		_CheckErrorGL;
		return x;
	}
};



template<UINT TexTarget,UINT TexInternalFormat,UINT TexDefaultFormat,UINT TexDefaultType>
class CwglTexture2DBase:public CwglTextureBase<TexTarget>
{
protected:
	UINT m_WrapMode_S;	
	UINT m_WrapMode_T;	
	TextureFilter m_TexFilterMinify;
	TextureFilter m_TexFilterMagnify;

public:
	CwglTexture2DBase(){ TextureWrapMode(); TextureFilterSet(); }
	void TextureFilterSet(TextureFilter minify=tx::FilterLinear,TextureFilter magnify=tx::FilterLinear,GLfloat AnisotropyParam = 1.0f)
	{
		m_TexFilterMinify = minify;  
		m_TexFilterMagnify = magnify; 
	}

	void TextureWrapMode(tx::TextureWrapMode s_mode = tx::WrapClamp,tx::TextureWrapMode t_mode = tx::WrapClamp)
	{
		m_WrapMode_S = s_mode;
		m_WrapMode_T = t_mode;
	}

	void Apply()
	{
		__super::Apply();

		glTexParameteri(TexTarget,GL_TEXTURE_WRAP_S,m_WrapMode_S);
		glTexParameteri(TexTarget,GL_TEXTURE_WRAP_T,m_WrapMode_T);
		_CheckErrorGL;

		glTexParameteri(TexTarget,GL_TEXTURE_MIN_FILTER,m_TexFilterMinify);
		glTexParameteri(TexTarget,GL_TEXTURE_MAG_FILTER,m_TexFilterMagnify);
		_CheckErrorGL;
	}

	void	DefineTexture(UINT cx,UINT cy,LPCVOID pData=NULL,UINT DataFormat=TexDefaultFormat,UINT DataType=TexDefaultType)
	{
		Apply();

		glPixelStorei(GL_PACK_ALIGNMENT,4);
		_CheckErrorGL;

		glTexImage2D(TexTarget,0,TexInternalFormat,cx,cy,0,DataFormat,DataType,pData);
		_CheckErrorGL;
	}

	void	DefineTexture_Border(UINT cx,UINT cy,LPCVOID pData=NULL,UINT DataFormat=TexDefaultFormat,UINT DataType=TexDefaultType)
	{
		Apply();
		glPixelStorei(GL_PACK_ALIGNMENT,1);
        glPixelStorei(GL_UNPACK_ALIGNMENT,1);
		_CheckErrorGL;

		glTexImage2D(TexTarget,0,TexInternalFormat,cx,cy,1,DataFormat,DataType,pData);
		_CheckErrorGL;
	}

	void	DefineTextureMipmapped(UINT cx,UINT cy,LPCVOID pData,UINT DataFormat=TexDefaultFormat,UINT DataType=TexDefaultType)
	{
		ASSERT(pData);

		Apply();

		glPixelStorei(GL_PACK_ALIGNMENT,4);
		_CheckErrorGL;

		gluBuild2DMipmaps(TexTarget,TexInternalFormat,cx,cy,DataFormat,DataType,pData);
		_CheckErrorGL;

		TextureWrapMode();
		TextureFilterSet();
	}

	template<class wglImage>
	void DefineTexture(const wglImage& img)
	{	DefineTexture(img.GetWidth(),img.GetHeight(),img,img.GetGlPixelFormat(),GL_UNSIGNED_BYTE);
		_CheckErrorGL;
	}

	template<class wglImage>
	void DefineTextureMipmapped(const wglImage& img)
	{	DefineTextureMipmapped(img.GetWidth(),img.GetHeight(),img,img.GetGlPixelFormat(),GL_UNSIGNED_BYTE);
		_CheckErrorGL;
	}


	void TextureSubImage(int srcX,int srcY,int cx,int cy, LPCVOID pData,UINT DataFormat=TexDefaultFormat,UINT DataType=TexDefaultType)
	{
		Apply();
		glTexSubImage2D(TexTarget,0,srcX,srcY,cx,cy,DataFormat,DataType,pData);
		_CheckErrorGL;
	}

	void	CopyTextureImage(GLint width,GLint height,GLint src_x=0,GLint src_y=0)
	{
		glCopyTexImage2D(TexTarget,0,TexInternalFormat,src_x,src_y,width,height,0);
		_CheckErrorGL;
	}

	void	CopyTextureSubImage(GLint width,GLint height,GLint src_x=0,GLint src_y=0,GLint dst_x=0,GLint dst_y=0)
	{
		glCopyTexSubImage2D(TexTarget,0,dst_x,dst_y,src_x,src_y,width,height);
		_CheckErrorGL;
	}

	void	DumpToFile(LPCTSTR fn)
	{
		GLint	comp = GetComponent();
		if(comp == 1 || comp == 3 )
		{
			wglh::CwglImage	bitmap(CwglSize(GetWidth(),GetHeight()),comp*8);

			if(bitmap.GetPixelAddress())
			{
				glGetTexImage(TexTarget,0,comp==3?GL_BGR_EXT:GL_LUMINANCE,GL_UNSIGNED_BYTE,bitmap.GetPixelAddress());
				_CheckErrorGL;

				bitmap.Save(fn);
			}
			else
			{
				_CheckDumpSrcLoc;
			}
		}
		else if(comp == 4)
		{
			int cx = GetWidth();
			int cy = GetHeight();

			int co = cx*cy*4;

			LPBYTE	pImg = new BYTE[co];
			if(pImg)
			{
				glGetTexImage(TexTarget,0,GL_BGRA_EXT,GL_UNSIGNED_BYTE,pImg);
				_CheckErrorGL;

				wglh::CwglImage	bgr(CwglSize(cx,cy),24);
				wglh::CwglImage	a(CwglSize(cx,cy),8);

				LPBYTE	pBGR = bgr.GetPixelAddress();
				LPBYTE	pA = a.GetPixelAddress();
				int step_bgr = cx*3;
				int step_a = cx;

				if( step_bgr%4 )step_bgr = ((step_bgr>>2)+1)<<2;
				if( step_a%4 )step_a = ((step_a>>2)+1)<<2;

				LPBYTE pSrc = pImg;
				for(int y=0;y<cy;y++,pSrc+=cx*4,pBGR+=step_bgr,pA+=step_a)
				{
					LPBYTE pLineSrc = pSrc;
					LPBYTE pLineBGR = pBGR;
					LPBYTE pLineA = pA;
					for(int x=0;x<cx;x++,pLineA++,pLineBGR+=3,pLineSrc+=4)
					{
						pLineBGR[0] = pLineSrc[0];
						pLineBGR[1] = pLineSrc[1];
						pLineBGR[2] = pLineSrc[2];
						pLineA[0] = pLineSrc[3];
					}
				}

				_SafeDelArray(pImg);

				bgr.Save(fn);

				TCHAR	buffer[MAX_PATH];
				_tcscpy(buffer,fn);

				LPTSTR p = _tcsrchr(buffer,_T('.'));
				ASSERT(p);
				memmove(p+2,p,_tcslen(p)+1);
				p[0] = _T('_');
				p[1] = _T('a');
				
				a.Save(buffer);
			}
			else
			{
				_CheckDumpSrcLoc;
			}
		}
	}
};


typedef CwglTexture2DBase<GL_TEXTURE_2D,GL_RGB,GL_BGR_EXT,GL_UNSIGNED_BYTE>		CwglTexture8u3cw;
typedef CwglTexture2DBase<GL_TEXTURE_2D,GL_RGBA,GL_BGRA_EXT,GL_UNSIGNED_BYTE>	CwglTexture8u4cw;
typedef CwglTexture2DBase<GL_TEXTURE_2D,GL_RGB,GL_RGB,GL_UNSIGNED_BYTE>			CwglTexture8u3c;
typedef CwglTexture2DBase<GL_TEXTURE_2D,GL_RGBA,GL_RGBA,GL_UNSIGNED_BYTE>		CwglTexture8u4c;
typedef CwglTexture2DBase<GL_TEXTURE_2D,GL_RGB32F_ARB,GL_RGB,GL_FLOAT>			CwglTexture32f3c;
typedef CwglTexture2DBase<GL_TEXTURE_2D,GL_RGBA32F_ARB,GL_RGBA,GL_FLOAT>				CwglTexture32f4c;

class CwglPrimitive
{
protected:
	GLUquadricObj*	m_pQuadricObj;

public:
	CwglPrimitive(){ m_pQuadricObj = NULL; }
	~CwglPrimitive(){ ASSERT(m_pQuadricObj==NULL); }  
	void Init();
	void Term(){ if(m_pQuadricObj)gluDeleteQuadric(m_pQuadricObj); m_pQuadricObj = NULL; }

	// Primitives
	void Cylinder(GLdouble baseRadius,GLdouble topRadius,GLdouble height,GLint slices,GLint stacks)
		{ ASSERT(m_pQuadricObj); gluCylinder(m_pQuadricObj,baseRadius,topRadius,height,slices,stacks); }
	void Disk(GLdouble innerRadius,GLdouble outerRadius,GLint slices,GLint loops)
		{ ASSERT(m_pQuadricObj); gluDisk(m_pQuadricObj,innerRadius,outerRadius,slices,loops); }
	void PartialDisk(GLdouble innerRadius,GLdouble outerRadius,GLint slices,GLint loops,GLdouble startAngle,GLdouble sweepAngle)
		{ ASSERT(m_pQuadricObj); gluPartialDisk(m_pQuadricObj,innerRadius,outerRadius,slices,loops,startAngle,sweepAngle); }
	void Sphere(GLdouble radius,GLint slices,GLint stacks)
		{ ASSERT(m_pQuadricObj); gluSphere(m_pQuadricObj,radius,slices,stacks);}

	// behavior
	void SetDrawStyle(GLenum mode = GL_FILL){ ASSERT(m_pQuadricObj); gluQuadricDrawStyle(m_pQuadricObj,mode); }
	void SetNormalMode(GLenum mode = GLU_SMOOTH){ ASSERT(m_pQuadricObj); gluQuadricNormals(m_pQuadricObj,mode); }
	void SetOrientation(GLenum mode = GLU_OUTSIDE){ ASSERT(m_pQuadricObj); gluQuadricOrientation(m_pQuadricObj,mode); }
	void SetTexture(BOOL use = TRUE){ ASSERT(m_pQuadricObj); gluQuadricOrientation(m_pQuadricObj,use); }
};

}


