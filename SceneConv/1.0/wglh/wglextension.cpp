#include "stdafx.h"
#include "wglextension.h"
#include "..\w32\file_64.h"
#include "wglImage.h"
#include "wglshader.h"


//////////////////////////////////////////////////////
//extension entry point of Gpu program management

PFNGLPROGRAMSTRINGARBPROC				wglh::ext::ARBvpfp::CwglGpuProgram::ProgramStringARB = NULL;
PFNGLBINDPROGRAMARBPROC					wglh::ext::ARBvpfp::CwglGpuProgram::BindProgramARB = NULL;
PFNGLDELETEPROGRAMSARBPROC				wglh::ext::ARBvpfp::CwglGpuProgram::DeleteProgramsARB = NULL;
PFNGLGENPROGRAMSARBPROC					wglh::ext::ARBvpfp::CwglGpuProgram::GenProgramsARB = NULL;
PFNGLPROGRAMENVPARAMETER4DARBPROC		wglh::ext::ARBvpfp::CwglGpuProgram::ProgramEnvParameter4dARB = NULL;
PFNGLPROGRAMENVPARAMETER4DVARBPROC		wglh::ext::ARBvpfp::CwglGpuProgram::ProgramEnvParameter4dvARB = NULL;
PFNGLPROGRAMENVPARAMETER4FARBPROC		wglh::ext::ARBvpfp::CwglGpuProgram::ProgramEnvParameter4fARB = NULL;
PFNGLPROGRAMENVPARAMETER4FVARBPROC		wglh::ext::ARBvpfp::CwglGpuProgram::ProgramEnvParameter4fvARB = NULL;
PFNGLPROGRAMLOCALPARAMETER4DARBPROC		wglh::ext::ARBvpfp::CwglGpuProgram::ProgramLocalParameter4dARB = NULL;
PFNGLPROGRAMLOCALPARAMETER4DVARBPROC	wglh::ext::ARBvpfp::CwglGpuProgram::ProgramLocalParameter4dvARB = NULL;
PFNGLPROGRAMLOCALPARAMETER4FARBPROC		wglh::ext::ARBvpfp::CwglGpuProgram::ProgramLocalParameter4fARB = NULL;
PFNGLPROGRAMLOCALPARAMETER4FVARBPROC	wglh::ext::ARBvpfp::CwglGpuProgram::ProgramLocalParameter4fvARB = NULL;
PFNGLGETPROGRAMENVPARAMETERDVARBPROC	wglh::ext::ARBvpfp::CwglGpuProgram::GetProgramEnvParameterdvARB = NULL;
PFNGLGETPROGRAMENVPARAMETERFVARBPROC	wglh::ext::ARBvpfp::CwglGpuProgram::GetProgramEnvParameterfvARB = NULL;
PFNGLGETPROGRAMLOCALPARAMETERDVARBPROC	wglh::ext::ARBvpfp::CwglGpuProgram::GetProgramLocalParameterdvARB = NULL;
PFNGLGETPROGRAMLOCALPARAMETERFVARBPROC	wglh::ext::ARBvpfp::CwglGpuProgram::GetProgramLocalParameterfvARB = NULL;
PFNGLGETPROGRAMIVARBPROC				wglh::ext::ARBvpfp::CwglGpuProgram::GetProgramivARB = NULL;
PFNGLGETPROGRAMSTRINGARBPROC			wglh::ext::ARBvpfp::CwglGpuProgram::GetProgramStringARB = NULL;
PFNGLISPROGRAMARBPROC					wglh::ext::ARBvpfp::CwglGpuProgram::IsProgramARB = NULL;

BOOL wglh::ext::ARBvpfp::CwglGpuProgram::LoadExtensionEntryPoints()
{
	if(wglh::ext::ARBvpfp::CwglGpuProgram::ProgramStringARB)
	{
		//All entry point should be available
		ASSERT(wglh::ext::ARBvpfp::CwglGpuProgram::ProgramStringARB);
		ASSERT(wglh::ext::ARBvpfp::CwglGpuProgram::BindProgramARB);
		ASSERT(wglh::ext::ARBvpfp::CwglGpuProgram::DeleteProgramsARB);
		ASSERT(wglh::ext::ARBvpfp::CwglGpuProgram::GenProgramsARB);
		ASSERT(wglh::ext::ARBvpfp::CwglGpuProgram::ProgramEnvParameter4dARB);
		ASSERT(wglh::ext::ARBvpfp::CwglGpuProgram::ProgramEnvParameter4dvARB);
		ASSERT(wglh::ext::ARBvpfp::CwglGpuProgram::ProgramEnvParameter4fARB);
		ASSERT(wglh::ext::ARBvpfp::CwglGpuProgram::ProgramEnvParameter4fvARB);
		ASSERT(wglh::ext::ARBvpfp::CwglGpuProgram::ProgramLocalParameter4dARB);
		ASSERT(wglh::ext::ARBvpfp::CwglGpuProgram::ProgramLocalParameter4dvARB);
		ASSERT(wglh::ext::ARBvpfp::CwglGpuProgram::ProgramLocalParameter4fARB);
		ASSERT(wglh::ext::ARBvpfp::CwglGpuProgram::ProgramLocalParameter4fvARB);
		ASSERT(wglh::ext::ARBvpfp::CwglGpuProgram::GetProgramEnvParameterdvARB);
		ASSERT(wglh::ext::ARBvpfp::CwglGpuProgram::GetProgramEnvParameterfvARB);
		ASSERT(wglh::ext::ARBvpfp::CwglGpuProgram::GetProgramLocalParameterdvARB);
		ASSERT(wglh::ext::ARBvpfp::CwglGpuProgram::GetProgramLocalParameterfvARB);
		ASSERT(wglh::ext::ARBvpfp::CwglGpuProgram::GetProgramivARB);
		ASSERT(wglh::ext::ARBvpfp::CwglGpuProgram::GetProgramStringARB);
		ASSERT(wglh::ext::ARBvpfp::CwglGpuProgram::IsProgramARB);

		return TRUE;
	}
	else
	{
		//Load the extension
		if(!(ProgramStringARB				= (PFNGLPROGRAMSTRINGARBPROC				)wglGetProcAddress("glProgramStringARB"))){ goto LEEP_LoadFailed; }
		if(!(BindProgramARB					= (PFNGLBINDPROGRAMARBPROC					)wglGetProcAddress("glBindProgramARB"))){ goto LEEP_LoadFailed; }
		if(!(DeleteProgramsARB				= (PFNGLDELETEPROGRAMSARBPROC				)wglGetProcAddress("glDeleteProgramsARB"))){ goto LEEP_LoadFailed; }
		if(!(GenProgramsARB					= (PFNGLGENPROGRAMSARBPROC					)wglGetProcAddress("glGenProgramsARB"))){ goto LEEP_LoadFailed; }
		if(!(ProgramEnvParameter4dARB		= (PFNGLPROGRAMENVPARAMETER4DARBPROC		)wglGetProcAddress("glProgramEnvParameter4dARB"))){ goto LEEP_LoadFailed; }
		if(!(ProgramEnvParameter4dvARB		= (PFNGLPROGRAMENVPARAMETER4DVARBPROC		)wglGetProcAddress("glProgramEnvParameter4dvARB"))){ goto LEEP_LoadFailed; }
		if(!(ProgramEnvParameter4fARB		= (PFNGLPROGRAMENVPARAMETER4FARBPROC		)wglGetProcAddress("glProgramEnvParameter4fARB"))){ goto LEEP_LoadFailed; }
		if(!(ProgramEnvParameter4fvARB		= (PFNGLPROGRAMENVPARAMETER4FVARBPROC		)wglGetProcAddress("glProgramEnvParameter4fvARB"))){ goto LEEP_LoadFailed; }
		if(!(ProgramLocalParameter4dARB		= (PFNGLPROGRAMLOCALPARAMETER4DARBPROC		)wglGetProcAddress("glProgramLocalParameter4dARB"))){ goto LEEP_LoadFailed; }
		if(!(ProgramLocalParameter4dvARB	= (PFNGLPROGRAMLOCALPARAMETER4DVARBPROC		)wglGetProcAddress("glProgramLocalParameter4dvARB"))){ goto LEEP_LoadFailed; }
		if(!(ProgramLocalParameter4fARB		= (PFNGLPROGRAMLOCALPARAMETER4FARBPROC		)wglGetProcAddress("glProgramLocalParameter4fARB"))){ goto LEEP_LoadFailed; }
		if(!(ProgramLocalParameter4fvARB	= (PFNGLPROGRAMLOCALPARAMETER4FVARBPROC		)wglGetProcAddress("glProgramLocalParameter4fvARB"))){ goto LEEP_LoadFailed; }
		if(!(GetProgramEnvParameterdvARB	= (PFNGLGETPROGRAMENVPARAMETERDVARBPROC		)wglGetProcAddress("glGetProgramEnvParameterdvARB"))){ goto LEEP_LoadFailed; }
		if(!(GetProgramEnvParameterfvARB	= (PFNGLGETPROGRAMENVPARAMETERFVARBPROC		)wglGetProcAddress("glGetProgramEnvParameterfvARB"))){ goto LEEP_LoadFailed; }
		if(!(GetProgramLocalParameterdvARB	= (PFNGLGETPROGRAMLOCALPARAMETERDVARBPROC	)wglGetProcAddress("glGetProgramLocalParameterdvARB"))){ goto LEEP_LoadFailed; }
		if(!(GetProgramLocalParameterfvARB	= (PFNGLGETPROGRAMLOCALPARAMETERFVARBPROC	)wglGetProcAddress("glGetProgramLocalParameterfvARB"))){ goto LEEP_LoadFailed; }
		if(!(GetProgramivARB				= (PFNGLGETPROGRAMIVARBPROC					)wglGetProcAddress("glGetProgramivARB"))){ goto LEEP_LoadFailed; }
		if(!(GetProgramStringARB			= (PFNGLGETPROGRAMSTRINGARBPROC				)wglGetProcAddress("glGetProgramStringARB"))){ goto LEEP_LoadFailed; }
		if(!(IsProgramARB					= (PFNGLISPROGRAMARBPROC					)wglGetProcAddress("glIsProgramARB"))){ goto LEEP_LoadFailed; }

		return TRUE;

LEEP_LoadFailed:
		
		_CheckErrorW32;
		wglh::ext::ARBvpfp::CwglGpuProgram::ProgramStringARB = NULL;
		return FALSE;
	}

	return FALSE;
}

wglh::ext::ARBvpfp::CwglGpuProgram::CwglGpuProgram()
{
	ZeroMemory(this,sizeof(CwglGpuProgram));
}

BOOL wglh::ext::ARBvpfp::CwglGpuProgram::LoadProgramAndApply(LPCSTR prog,CwglGpuProgramError * pError)
{
	if(!ProgramName)
	{
		GenProgramsARB(1,&ProgramName);
		_CheckErrorGL;
	}

	if(!ProgramName)return FALSE;

	BindProgramARB(ProgramTarget,ProgramName);

	_CheckErrorGL;
	ASSERT(prog);
	ProgramStringARB(ProgramTarget,GL_PROGRAM_FORMAT_ASCII_ARB,(GLsizei)strlen(prog),prog);
	if(GL_INVALID_OPERATION == glGetError())
	{
		CwglGpuProgramError		err;
		if(!pError)pError = &err;

		CompiledSuccess = FALSE;

		//Dump error
		{
			glGetIntegerv(GL_PROGRAM_ERROR_POSITION_ARB,&pError->Position);
			pError->Desc = glGetString(GL_PROGRAM_ERROR_STRING_ARB);
		}
		return FALSE;
	}
	else
	{
		CompiledSuccess = TRUE;
		return TRUE;
	}
}

void wglh::ext::ARBvpfp::CwglGpuProgram::Apply()
{
	//ASSERT(ProgramName);
	if(ProgramName && CompiledSuccess)
	{
		BindProgramARB(ProgramTarget,ProgramName);
		_CheckErrorGL;
	}
	else
	{
		glDisable(ProgramTarget);
	}
}


//////////////////Setup Running Parameters
void	wglh::ext::ARBvpfp::CwglGpuProgram::SetEnvParam(GLuint index,GLfloat x,GLfloat y,GLfloat z,GLfloat w)
{
	ProgramEnvParameter4fARB(ProgramTarget,index,x,y,z,w);
	_CheckErrorGL;
}

void	wglh::ext::ARBvpfp::CwglGpuProgram::SetEnvParam(GLuint index,GLdouble x,GLdouble y,GLdouble z,GLdouble w)
{
	ProgramEnvParameter4dARB(ProgramTarget,index,x,y,z,w);
	_CheckErrorGL;
}

void	wglh::ext::ARBvpfp::CwglGpuProgram::SetLocalParam(GLuint index,GLfloat x,GLfloat y,GLfloat z,GLfloat w)
{
	ProgramLocalParameter4fARB(ProgramTarget,index,x,y,z,w);
	_CheckErrorGL;
}

void	wglh::ext::ARBvpfp::CwglGpuProgram::SetLocalParam(GLuint index,GLdouble x,GLdouble y,GLdouble z,GLdouble w)
{
	ProgramLocalParameter4dARB(ProgramTarget,index,x,y,z,w);
	_CheckErrorGL;
}

void	wglh::ext::ARBvpfp::CwglGpuProgram::SetEnvParam(GLuint index,const GLfloat *pv)
{
	ASSERT(pv);
	ProgramEnvParameter4fvARB(ProgramTarget,index,pv);
	_CheckErrorGL;
}

void	wglh::ext::ARBvpfp::CwglGpuProgram::SetEnvParam(GLuint index,const GLdouble *pv)
{
	ASSERT(pv);
	ProgramEnvParameter4dvARB(ProgramTarget,index,pv);
	_CheckErrorGL;
}

void	wglh::ext::ARBvpfp::CwglGpuProgram::SetLocalParam(GLuint index,const GLfloat *pv)
{
	ASSERT(pv);
	ProgramLocalParameter4fvARB(ProgramTarget,index,pv);
	_CheckErrorGL;
}

void	wglh::ext::ARBvpfp::CwglGpuProgram::SetLocalParam(GLuint index,const GLdouble *pv)
{
	ASSERT(pv);
	ProgramLocalParameter4dvARB(ProgramTarget,index,pv);
	_CheckErrorGL;
}

void	wglh::ext::ARBvpfp::CwglGpuProgram::GetEnvParam(GLuint index,GLfloat *pv)
{
	ASSERT(pv);
	GetProgramEnvParameterfvARB(ProgramTarget,index,pv);
	_CheckErrorGL;
}

void	wglh::ext::ARBvpfp::CwglGpuProgram::GetEnvParam(GLuint index,GLdouble *pv)
{
	ASSERT(pv);
	GetProgramEnvParameterdvARB(ProgramTarget,index,pv);
	_CheckErrorGL;
}

void	wglh::ext::ARBvpfp::CwglGpuProgram::GetLocalParam(GLuint index,GLfloat *pv)
{
	ASSERT(pv);
	GetProgramLocalParameterfvARB(ProgramTarget,index,pv);
	_CheckErrorGL;
}

void	wglh::ext::ARBvpfp::CwglGpuProgram::GetLocalParam(GLuint index,GLdouble *pv)
{
	ASSERT(pv);
	GetProgramLocalParameterdvARB(ProgramTarget,index,pv);
	_CheckErrorGL;
}



///////////////////////////////////////////////////////
//CwglGpuProgramSourceCode
BOOL wglh::ext::ARBvpfp::CwglGpuProgramSourceCode::Load(int Method,LPCTSTR pName1,LPCTSTR pName2)
{
	switch(Method)
	{
	case GPSC_CLIPBROAD:
		{
			if(::OpenClipboard(NULL))
			{
				LPSTR Text = (LPSTR)GetClipboardData(CF_TEXT);
				//I am not sure, I should release it or not
				if(Text)
				{
					UINT len = (UINT)strlen(Text);
					_SafeDelArray(pSourceCode);
					pSourceCode = new char[len+1];
					memcpy(pSourceCode,Text,len+1);
					::CloseClipboard();
					return TRUE;
				}
			}
		}
		break;
	case GPSC_FILE:
		{
			w32::CFile64	file;
			if(file.Open(pName1))
			{
				DWORD	len = (DWORD)file.GetLength();
				if(len)
				{
					_SafeDelArray(pSourceCode);
					pSourceCode = new char[len+1];
					ASSERT(pSourceCode);

					VERIFY(file.Read(pSourceCode,len) == len);
					pSourceCode[len] = 0;

					return TRUE;
				}
				else
				{
					_CheckDump("Empty file\n");
					_CheckDumpSrcLoc;
					return FALSE;
				}
			}
		}
		break;
	case GPSC_RESOURCE:
		//Not Implemented yet :P
		ASSERT(0);
	default:
		ASSERT(0);
	}

	_CheckErrorW32;

	return FALSE;
}

BOOL wglh::ext::ARBvpfp::CwglGpuProgramSourceCode::LoadFromNotepadInstance()
{
	if(!hNotepadWnd || !IsWindow(hNotepadWnd))
	{
		hNotepadWnd = ::FindWindow(_T("Notepad"),NULL);
		if(!hNotepadWnd)return FALSE;
	}

	SetWindowText(hNotepadWnd,_T("Notepad ( Associated with WGL ARBvpfp Library )"));

	HWND edit = ::GetDlgItem(hNotepadWnd,0xf);
	if(edit)
	{
		int p;
		p = (int)::SendMessage(edit, EM_GETSEL, 0, 0);
		::SendMessage(edit, EM_SETSEL, 0, -1);
		::SendMessage(edit, WM_COPY, 0, 0);
		::SendMessage(edit, EM_SETSEL, p, p);

		return Load(GPSC_CLIPBROAD);
	}

	return FALSE;
}

BOOL wglh::ext::ARBvpfp::CwglGpuProgramSourceCode::Compile(CwglGpuProgram& ShaderOut,LPCSTR pszName,BOOL DumpResource,BOOL ShowNotificationToNotepad)
{
	LPSTR	pCode = pSourceCode;

	if(pszName)
	{
		//Seek to specific section
		ASSERT(strlen(pszName)< MAX_PATH - 3);
		CHAR t[MAX_PATH+1];
		sprintf(t,"[%s]",pszName);
		pCode = strstr(pCode,t);
		if(!pCode)return FALSE;
		pCode+= 4+strlen(pszName);
	}

	ASSERT(ShaderOut.SourceCodeHeaderTag); 
	// do not use CwglGpuProgram, use CwglFragmentProgram/CwglVertexProgram instead

	pCode = strstr(pCode,ShaderOut.SourceCodeHeaderTag);
	if(pCode)
	{
		LPSTR pEnd = strstr(pCode+9,"\15\12END");
		if(pEnd)
		{
			pEnd += 6;
			CHAR Temp = *pEnd;
			*pEnd = 0;

			CwglGpuProgramError er;
			if(ShaderOut.LoadProgramAndApply(pCode,&er))
			{
				*pEnd = Temp;
				
				if(DumpResource)
				{
					///////// Dump info  ///////////////////////
					printf("=======  Shader: %s (%s) =========\n",pszName,&ShaderOut.SourceCodeHeaderTag[2]);
					printf("Temporaries        : %d\n"
						"Total Instructions : %d\n"
						"ALU Instructions   : %d\n"
						"Texture Fetch      : %d\n"
						"Texture Dependence : %d\n\n",
						ShaderOut.CurrentNativeTemporariesUsed(),
						ShaderOut.CurrentNativeInstructionsUsed(),
						ShaderOut.CurrentNativeAluInstructionsUsed(),
						ShaderOut.CurrentNativeTexInstructionsUsed(),
						ShaderOut.CurrentNativeTexIndirectionsUsed());
				}

				return TRUE;
			}

			if(DumpResource)
			{
				_CheckDump("Error: "<<((LPCSTR)er.Desc)<<"\n  Pos: "<<er.Position<<'\n');
			}

			*pEnd = Temp;

			if(ShowNotificationToNotepad && hNotepadWnd)
			{
				HWND edit = ::GetDlgItem(hNotepadWnd,0xf);
				if(edit)
				{
					::SendMessage(edit, EM_SETSEL, er.Position+(pCode-pSourceCode), er.Position+3+(pCode-pSourceCode));
					::BringWindowToTop(hNotepadWnd);

					CHAR report[512];
					sprintf(report,"(%s) ERR:%s",pszName?pszName:"Default",er.Desc);
					::SetWindowTextA(hNotepadWnd,report);
				}
			}
		}
	}

	return FALSE;
}


///////////////////////////////////////////////////////////
// CwglGpuProgramSourceCodeProject
DWORD WINAPI wglh::ext::ARBvpfp::_MonitoringThread(LPVOID pv)
{
	ASSERT(pv);
	HWND	wnd = ((CwglGpuProgramSourceCodeProject*)pv)->hNoticationWnd;

	HANDLE event = FindFirstChangeNotification(	((CwglGpuProgramSourceCodeProject*)pv)->pProjectFolder,
												FALSE,FILE_NOTIFY_CHANGE_LAST_WRITE);
	ASSERT(event != INVALID_HANDLE_VALUE);

	for(;;)
	{
		DWORD ret;
		ret = WaitForSingleObject(event,INFINITE);
		VERIFY(FindNextChangeNotification(event));

		if(ret == WAIT_OBJECT_0)
		{
			::SendMessage(wnd,WM_GpuProgramSourceCodeChanged,0,0);
		}
	}

	return 0;
}


BOOL wglh::ext::ARBvpfp::CwglGpuProgramSourceCodeProject::LoadGpuProgramProjectFolder(LPCTSTR pn,HWND NotificationWnd)
{
	ASSERT(hMonitoringThread == NULL);  //Project has been setup !! not load twice

	ASSERT(pn);

	ProjectFolderLen = 0;
	
	if(GetFileAttributes(pn) != INVALID_FILE_ATTRIBUTES)
	{
		_tcscpy(pProjectFolder,pn);
	}
	else
	{
		_tcscpy(pProjectFolder,_T("..\\"));
		_tcscat(pProjectFolder,pn);
		if(GetFileAttributes(pn) == INVALID_FILE_ATTRIBUTES)
		{
			_CheckErrorW32;
			_CheckDumpSrcLoc;
			return FALSE;
		}
	}

	ProjectFolderLen = (UINT)_tcslen(pProjectFolder);

	hNoticationWnd = NotificationWnd;
	hMonitoringThread = CreateThread(NULL,0,_MonitoringThread,this,0,NULL);
	ASSERT(hMonitoringThread);

	return TRUE;
}


wglh::ext::ARBvpfp::CwglGpuProgramSourceCodeProject::~CwglGpuProgramSourceCodeProject()
{
	if(hMonitoringThread)
	{
		TerminateThread(hMonitoringThread,0);
	}
}

void wglh::ext::ARBvpfp::CwglGpuProgramSourceCodeProject::BindGpuProgram(CwglGpuProgram& ShaderOut,LPCTSTR pszName)
{
	ProgramItem item;

	wsprintf(item.Filename,_T("%s\\%s.sh"),pProjectFolder,pszName);
	item.FileTime.dwHighDateTime = 0;
	item.FileTime.dwLowDateTime = 0;
	item.ShaderObj = &ShaderOut;

	m_ProgramList.push_back(item);
}

void wglh::ext::ARBvpfp::CwglGpuProgramSourceCodeProject::ReloadAll()
{
	UINT co = (UINT)m_ProgramList.size();
	for(UINT i=0;i<co;i++)
	{
		m_ProgramList[i].FileTime.dwHighDateTime = 0;
		m_ProgramList[i].FileTime.dwLowDateTime = 0;
	}

	ReloadChanged();
}

void wglh::ext::ARBvpfp::CwglGpuProgramSourceCodeProject::ReloadChanged()
{
	UINT co = (UINT)m_ProgramList.size();
	for(UINT i=0;i<co;i++)
	{
		w32::CFile64	file;
		if(file.Open(m_ProgramList[i].Filename))
		{
			FILETIME t;
			file.GetTime_LastModify(&t);
			file.Close();

			if(	t.dwHighDateTime != m_ProgramList[i].FileTime.dwHighDateTime ||
				t.dwLowDateTime != m_ProgramList[i].FileTime.dwLowDateTime )
			{
				_CheckDump("Update shader "<<m_ProgramList[i].Filename<<'\n');
				wglh::ext::ARBvpfp::CwglGpuProgramSourceCode shader;

				if(shader.Load(CwglGpuProgramSourceCode::GPSC_FILE,m_ProgramList[i].Filename))
				{
					m_ProgramList[i].FileTime.dwHighDateTime = t.dwHighDateTime;
					m_ProgramList[i].FileTime.dwLowDateTime = t.dwLowDateTime;

					ASSERT(m_ProgramList[i].ShaderObj);
					shader.Compile(*m_ProgramList[i].ShaderObj,0,TRUE,FALSE);
					_CheckDump('\n');
				}
				else
				{
					_CheckDumpSrcLoc;
				}
			}
		}
	}
}



//////////////////////////////////////////////////////////
//Vertex Program
wglh::ext::ARBvpfp::CwglVertexProgram::CwglVertexProgram()
{
	ProgramTarget = GL_VERTEX_PROGRAM_ARB;
	SourceCodeHeaderTag = "!!ARBvp1.0";
}

//Additional entrypoints for Vertex Program
PFNGLVERTEXATTRIB1SARBPROC             wglh::ext::ARBvpfp::CwglVertexProgram::VertexAttrib1sARB = NULL;
PFNGLVERTEXATTRIB1FARBPROC			   wglh::ext::ARBvpfp::CwglVertexProgram::VertexAttrib1fARB = NULL;
PFNGLVERTEXATTRIB1DARBPROC			   wglh::ext::ARBvpfp::CwglVertexProgram::VertexAttrib1dARB = NULL;
PFNGLVERTEXATTRIB2SARBPROC			   wglh::ext::ARBvpfp::CwglVertexProgram::VertexAttrib2sARB = NULL;
PFNGLVERTEXATTRIB2FARBPROC			   wglh::ext::ARBvpfp::CwglVertexProgram::VertexAttrib2fARB = NULL;
PFNGLVERTEXATTRIB2DARBPROC			   wglh::ext::ARBvpfp::CwglVertexProgram::VertexAttrib2dARB = NULL;
PFNGLVERTEXATTRIB3SARBPROC			   wglh::ext::ARBvpfp::CwglVertexProgram::VertexAttrib3sARB = NULL;
PFNGLVERTEXATTRIB3FARBPROC			   wglh::ext::ARBvpfp::CwglVertexProgram::VertexAttrib3fARB = NULL;
PFNGLVERTEXATTRIB3DARBPROC			   wglh::ext::ARBvpfp::CwglVertexProgram::VertexAttrib3dARB = NULL;
PFNGLVERTEXATTRIB4SARBPROC			   wglh::ext::ARBvpfp::CwglVertexProgram::VertexAttrib4sARB = NULL;
PFNGLVERTEXATTRIB4FARBPROC			   wglh::ext::ARBvpfp::CwglVertexProgram::VertexAttrib4fARB = NULL;
PFNGLVERTEXATTRIB4DARBPROC			   wglh::ext::ARBvpfp::CwglVertexProgram::VertexAttrib4dARB = NULL;
PFNGLVERTEXATTRIB4NUBARBPROC		   wglh::ext::ARBvpfp::CwglVertexProgram::VertexAttrib4NubARB = NULL;
										 
PFNGLVERTEXATTRIB1SVARBPROC			   wglh::ext::ARBvpfp::CwglVertexProgram::VertexAttrib1svARB = NULL;
PFNGLVERTEXATTRIB1FVARBPROC			   wglh::ext::ARBvpfp::CwglVertexProgram::VertexAttrib1fvARB = NULL;
PFNGLVERTEXATTRIB1DVARBPROC			   wglh::ext::ARBvpfp::CwglVertexProgram::VertexAttrib1dvARB = NULL;
PFNGLVERTEXATTRIB2SVARBPROC			   wglh::ext::ARBvpfp::CwglVertexProgram::VertexAttrib2svARB = NULL;
PFNGLVERTEXATTRIB2FVARBPROC			   wglh::ext::ARBvpfp::CwglVertexProgram::VertexAttrib2fvARB = NULL;
PFNGLVERTEXATTRIB2DVARBPROC			   wglh::ext::ARBvpfp::CwglVertexProgram::VertexAttrib2dvARB = NULL;
PFNGLVERTEXATTRIB3SVARBPROC			   wglh::ext::ARBvpfp::CwglVertexProgram::VertexAttrib3svARB = NULL;
PFNGLVERTEXATTRIB3FVARBPROC			   wglh::ext::ARBvpfp::CwglVertexProgram::VertexAttrib3fvARB = NULL;
PFNGLVERTEXATTRIB3DVARBPROC			   wglh::ext::ARBvpfp::CwglVertexProgram::VertexAttrib3dvARB = NULL;
PFNGLVERTEXATTRIB4BVARBPROC			   wglh::ext::ARBvpfp::CwglVertexProgram::VertexAttrib4bvARB = NULL;
PFNGLVERTEXATTRIB4SVARBPROC			   wglh::ext::ARBvpfp::CwglVertexProgram::VertexAttrib4svARB = NULL;
PFNGLVERTEXATTRIB4IVARBPROC			   wglh::ext::ARBvpfp::CwglVertexProgram::VertexAttrib4ivARB = NULL;
PFNGLVERTEXATTRIB4UBVARBPROC		   wglh::ext::ARBvpfp::CwglVertexProgram::VertexAttrib4ubvARB = NULL;
PFNGLVERTEXATTRIB4USVARBPROC		   wglh::ext::ARBvpfp::CwglVertexProgram::VertexAttrib4usvARB = NULL;
PFNGLVERTEXATTRIB4UIVARBPROC		   wglh::ext::ARBvpfp::CwglVertexProgram::VertexAttrib4uivARB = NULL;
PFNGLVERTEXATTRIB4FVARBPROC			   wglh::ext::ARBvpfp::CwglVertexProgram::VertexAttrib4fvARB = NULL;
PFNGLVERTEXATTRIB4DVARBPROC			   wglh::ext::ARBvpfp::CwglVertexProgram::VertexAttrib4dvARB = NULL;
PFNGLVERTEXATTRIB4NBVARBPROC		   wglh::ext::ARBvpfp::CwglVertexProgram::VertexAttrib4NbvARB = NULL;
PFNGLVERTEXATTRIB4NSVARBPROC		   wglh::ext::ARBvpfp::CwglVertexProgram::VertexAttrib4NsvARB = NULL;
PFNGLVERTEXATTRIB4NIVARBPROC		   wglh::ext::ARBvpfp::CwglVertexProgram::VertexAttrib4NivARB = NULL;
PFNGLVERTEXATTRIB4NUBVARBPROC		   wglh::ext::ARBvpfp::CwglVertexProgram::VertexAttrib4NubvARB = NULL;
PFNGLVERTEXATTRIB4NUSVARBPROC		   wglh::ext::ARBvpfp::CwglVertexProgram::VertexAttrib4NusvARB = NULL;
PFNGLVERTEXATTRIB4NUIVARBPROC		   wglh::ext::ARBvpfp::CwglVertexProgram::VertexAttrib4NuivARB = NULL;
											 
PFNGLVERTEXATTRIBPOINTERARBPROC		   wglh::ext::ARBvpfp::CwglVertexProgram::VertexAttribPointerARB = NULL;
											 
PFNGLENABLEVERTEXATTRIBARRAYARBPROC	   wglh::ext::ARBvpfp::CwglVertexProgram::EnableVertexAttribArrayARB = NULL;
PFNGLDISABLEVERTEXATTRIBARRAYARBPROC   wglh::ext::ARBvpfp::CwglVertexProgram::DisableVertexAttribArrayARB = NULL;
											 
PFNGLGETVERTEXATTRIBDVARBPROC		   wglh::ext::ARBvpfp::CwglVertexProgram::GetVertexAttribdvARB = NULL;
PFNGLGETVERTEXATTRIBFVARBPROC		   wglh::ext::ARBvpfp::CwglVertexProgram::GetVertexAttribfvARB = NULL;
PFNGLGETVERTEXATTRIBIVARBPROC		   wglh::ext::ARBvpfp::CwglVertexProgram::GetVertexAttribivARB = NULL;
											 
PFNGLGETVERTEXATTRIBPOINTERVARBPROC	   wglh::ext::ARBvpfp::CwglVertexProgram::GetVertexAttribPointervARB = NULL;
											   
											   
BOOL wglh::ext::ARBvpfp::CwglVertexProgram::LoadExtensionEntryPoints()
{											   
	if(GetVertexAttribPointervARB)			   
	{	
		ASSERT(VertexAttrib1sARB);
		ASSERT(VertexAttrib1fARB);
		ASSERT(VertexAttrib1dARB);
		ASSERT(VertexAttrib2sARB);
		ASSERT(VertexAttrib2fARB);
		ASSERT(VertexAttrib2dARB);
		ASSERT(VertexAttrib3sARB);
		ASSERT(VertexAttrib3fARB);
		ASSERT(VertexAttrib3dARB);
		ASSERT(VertexAttrib4sARB);
		ASSERT(VertexAttrib4fARB);
		ASSERT(VertexAttrib4dARB);
		ASSERT(VertexAttrib4NubARB);
		ASSERT(VertexAttrib1svARB);
		ASSERT(VertexAttrib1fvARB);
		ASSERT(VertexAttrib1dvARB);
		ASSERT(VertexAttrib2svARB);
		ASSERT(VertexAttrib2fvARB);
		ASSERT(VertexAttrib2dvARB);
		ASSERT(VertexAttrib3svARB);
		ASSERT(VertexAttrib3fvARB);
		ASSERT(VertexAttrib3dvARB);
		ASSERT(VertexAttrib4bvARB);
		ASSERT(VertexAttrib4svARB);
		ASSERT(VertexAttrib4ivARB);
		ASSERT(VertexAttrib4ubvARB);
		ASSERT(VertexAttrib4usvARB);
		ASSERT(VertexAttrib4uivARB);
		ASSERT(VertexAttrib4fvARB);
		ASSERT(VertexAttrib4dvARB);
		ASSERT(VertexAttrib4NbvARB);
		ASSERT(VertexAttrib4NsvARB);
		ASSERT(VertexAttrib4NivARB);
		ASSERT(VertexAttrib4NubvARB);
		ASSERT(VertexAttrib4NusvARB);
		ASSERT(VertexAttrib4NuivARB);
		ASSERT(VertexAttribPointerARB);
		ASSERT(EnableVertexAttribArrayARB);
		ASSERT(DisableVertexAttribArrayARB);
		ASSERT(GetVertexAttribdvARB);
		ASSERT(GetVertexAttribfvARB);
		ASSERT(GetVertexAttribivARB);
		ASSERT(GetVertexAttribPointervARB);
		return TRUE;
	}	

#define VPLEEP_Load(x) if(!( *((PROC*)&x) = wglGetProcAddress("gl"#x) ))goto VPLEEP_LoadFailed
		VPLEEP_Load(VertexAttrib1sARB);
		VPLEEP_Load(VertexAttrib1fARB);
		VPLEEP_Load(VertexAttrib1dARB);
		VPLEEP_Load(VertexAttrib2sARB);
		VPLEEP_Load(VertexAttrib2fARB);
		VPLEEP_Load(VertexAttrib2dARB);
		VPLEEP_Load(VertexAttrib3sARB);
		VPLEEP_Load(VertexAttrib3fARB);
		VPLEEP_Load(VertexAttrib3dARB);
		VPLEEP_Load(VertexAttrib4sARB);
		VPLEEP_Load(VertexAttrib4fARB);
		VPLEEP_Load(VertexAttrib4dARB);
		VPLEEP_Load(VertexAttrib4NubARB);
		VPLEEP_Load(VertexAttrib1svARB);
		VPLEEP_Load(VertexAttrib1fvARB);
		VPLEEP_Load(VertexAttrib1dvARB);
		VPLEEP_Load(VertexAttrib2svARB);
		VPLEEP_Load(VertexAttrib2fvARB);
		VPLEEP_Load(VertexAttrib2dvARB);
		VPLEEP_Load(VertexAttrib3svARB);
		VPLEEP_Load(VertexAttrib3fvARB);
		VPLEEP_Load(VertexAttrib3dvARB);
		VPLEEP_Load(VertexAttrib4bvARB);
		VPLEEP_Load(VertexAttrib4svARB);
		VPLEEP_Load(VertexAttrib4ivARB);
		VPLEEP_Load(VertexAttrib4ubvARB);
		VPLEEP_Load(VertexAttrib4usvARB);
		VPLEEP_Load(VertexAttrib4uivARB);
		VPLEEP_Load(VertexAttrib4fvARB);
		VPLEEP_Load(VertexAttrib4dvARB);
		VPLEEP_Load(VertexAttrib4NbvARB);
		VPLEEP_Load(VertexAttrib4NsvARB);
		VPLEEP_Load(VertexAttrib4NivARB);
		VPLEEP_Load(VertexAttrib4NubvARB);
		VPLEEP_Load(VertexAttrib4NusvARB);
		VPLEEP_Load(VertexAttrib4NuivARB);
		VPLEEP_Load(VertexAttribPointerARB);
		VPLEEP_Load(EnableVertexAttribArrayARB);
		VPLEEP_Load(DisableVertexAttribArrayARB);
		VPLEEP_Load(GetVertexAttribdvARB);
		VPLEEP_Load(GetVertexAttribfvARB);
		VPLEEP_Load(GetVertexAttribivARB);
		VPLEEP_Load(GetVertexAttribPointervARB);

#undef VPLEEP_Load

	return TRUE;

VPLEEP_LoadFailed:
		
	_CheckErrorW32;
	GetVertexAttribPointervARB = NULL;
	return FALSE;
}


//Fragment Program
wglh::ext::ARBvpfp::CwglFragmentProgram::CwglFragmentProgram()
{
	ProgramTarget = GL_FRAGMENT_PROGRAM_ARB;
	SourceCodeHeaderTag = "!!ARBfp1.0";
}

BOOL wglh::ext::ARBvpfp::CwglFragmentProgram::LoadExtensionEntryPoints()
{
	//I did not found any fragment specific functions
	//however I keep this function for further use
	return TRUE;
}



//////////////////////////////////////////////////
//entrypoints for ARB_multitexture
PFNGLACTIVETEXTUREARBPROC         wglh::ext::CwglMultiTextureEnv::ActiveTextureARB = NULL;                   
PFNGLCLIENTACTIVETEXTUREARBPROC	  wglh::ext::CwglMultiTextureEnv::ClientActiveTextureARB = NULL; 
PFNGLMULTITEXCOORD1DARBPROC		  wglh::ext::CwglMultiTextureEnv::MultiTexCoord1dARB = NULL; 
PFNGLMULTITEXCOORD1DVARBPROC	  wglh::ext::CwglMultiTextureEnv::MultiTexCoord1dvARB = NULL;
PFNGLMULTITEXCOORD1FARBPROC		  wglh::ext::CwglMultiTextureEnv::MultiTexCoord1fARB = NULL; 
PFNGLMULTITEXCOORD1FVARBPROC	  wglh::ext::CwglMultiTextureEnv::MultiTexCoord1fvARB = NULL;
PFNGLMULTITEXCOORD1IARBPROC		  wglh::ext::CwglMultiTextureEnv::MultiTexCoord1iARB = NULL; 
PFNGLMULTITEXCOORD1IVARBPROC	  wglh::ext::CwglMultiTextureEnv::MultiTexCoord1ivARB = NULL;
PFNGLMULTITEXCOORD1SARBPROC		  wglh::ext::CwglMultiTextureEnv::MultiTexCoord1sARB = NULL; 
PFNGLMULTITEXCOORD1SVARBPROC	  wglh::ext::CwglMultiTextureEnv::MultiTexCoord1svARB = NULL;
PFNGLMULTITEXCOORD2DARBPROC		  wglh::ext::CwglMultiTextureEnv::MultiTexCoord2dARB = NULL; 
PFNGLMULTITEXCOORD2DVARBPROC	  wglh::ext::CwglMultiTextureEnv::MultiTexCoord2dvARB = NULL;
PFNGLMULTITEXCOORD2FARBPROC		  wglh::ext::CwglMultiTextureEnv::MultiTexCoord2fARB = NULL; 
PFNGLMULTITEXCOORD2FVARBPROC	  wglh::ext::CwglMultiTextureEnv::MultiTexCoord2fvARB = NULL;
PFNGLMULTITEXCOORD2IARBPROC		  wglh::ext::CwglMultiTextureEnv::MultiTexCoord2iARB = NULL; 
PFNGLMULTITEXCOORD2IVARBPROC	  wglh::ext::CwglMultiTextureEnv::MultiTexCoord2ivARB = NULL;
PFNGLMULTITEXCOORD2SARBPROC		  wglh::ext::CwglMultiTextureEnv::MultiTexCoord2sARB = NULL; 
PFNGLMULTITEXCOORD2SVARBPROC	  wglh::ext::CwglMultiTextureEnv::MultiTexCoord2svARB = NULL;
PFNGLMULTITEXCOORD3DARBPROC		  wglh::ext::CwglMultiTextureEnv::MultiTexCoord3dARB = NULL; 
PFNGLMULTITEXCOORD3DVARBPROC	  wglh::ext::CwglMultiTextureEnv::MultiTexCoord3dvARB = NULL;
PFNGLMULTITEXCOORD3FARBPROC		  wglh::ext::CwglMultiTextureEnv::MultiTexCoord3fARB = NULL; 
PFNGLMULTITEXCOORD3FVARBPROC	  wglh::ext::CwglMultiTextureEnv::MultiTexCoord3fvARB = NULL;
PFNGLMULTITEXCOORD3IARBPROC		  wglh::ext::CwglMultiTextureEnv::MultiTexCoord3iARB = NULL; 
PFNGLMULTITEXCOORD3IVARBPROC	  wglh::ext::CwglMultiTextureEnv::MultiTexCoord3ivARB = NULL;
PFNGLMULTITEXCOORD3SARBPROC		  wglh::ext::CwglMultiTextureEnv::MultiTexCoord3sARB = NULL; 
PFNGLMULTITEXCOORD3SVARBPROC	  wglh::ext::CwglMultiTextureEnv::MultiTexCoord3svARB = NULL;
PFNGLMULTITEXCOORD4DARBPROC		  wglh::ext::CwglMultiTextureEnv::MultiTexCoord4dARB = NULL; 
PFNGLMULTITEXCOORD4DVARBPROC	  wglh::ext::CwglMultiTextureEnv::MultiTexCoord4dvARB = NULL;
PFNGLMULTITEXCOORD4FARBPROC		  wglh::ext::CwglMultiTextureEnv::MultiTexCoord4fARB = NULL; 
PFNGLMULTITEXCOORD4FVARBPROC	  wglh::ext::CwglMultiTextureEnv::MultiTexCoord4fvARB = NULL;
PFNGLMULTITEXCOORD4IARBPROC		  wglh::ext::CwglMultiTextureEnv::MultiTexCoord4iARB = NULL; 
PFNGLMULTITEXCOORD4IVARBPROC	  wglh::ext::CwglMultiTextureEnv::MultiTexCoord4ivARB = NULL;
PFNGLMULTITEXCOORD4SARBPROC		  wglh::ext::CwglMultiTextureEnv::MultiTexCoord4sARB = NULL; 
PFNGLMULTITEXCOORD4SVARBPROC	  wglh::ext::CwglMultiTextureEnv::MultiTexCoord4svARB = NULL; 


BOOL wglh::ext::CwglMultiTextureEnv::LoadExtensionEntryPoints()
{
	if(ActiveTextureARB)			   
	{	
		ASSERT(ActiveTextureARB);
		ASSERT(ClientActiveTextureARB );
		ASSERT(MultiTexCoord1dARB );
		ASSERT(MultiTexCoord1dvARB);
		ASSERT(MultiTexCoord1fARB );
		ASSERT(MultiTexCoord1fvARB);
		ASSERT(MultiTexCoord1iARB );
		ASSERT(MultiTexCoord1ivARB);
		ASSERT(MultiTexCoord1sARB );
		ASSERT(MultiTexCoord1svARB);
		ASSERT(MultiTexCoord2dARB );
		ASSERT(MultiTexCoord2dvARB);
		ASSERT(MultiTexCoord2fARB );
		ASSERT(MultiTexCoord2fvARB);
		ASSERT(MultiTexCoord2iARB );
		ASSERT(MultiTexCoord2ivARB);
		ASSERT(MultiTexCoord2sARB );
		ASSERT(MultiTexCoord2svARB);
		ASSERT(MultiTexCoord3dARB );
		ASSERT(MultiTexCoord3dvARB);
		ASSERT(MultiTexCoord3fARB );
		ASSERT(MultiTexCoord3fvARB);
		ASSERT(MultiTexCoord3iARB );
		ASSERT(MultiTexCoord3ivARB);
		ASSERT(MultiTexCoord3sARB );
		ASSERT(MultiTexCoord3svARB);
		ASSERT(MultiTexCoord4dARB );
		ASSERT(MultiTexCoord4dvARB);
		ASSERT(MultiTexCoord4fARB );
		ASSERT(MultiTexCoord4fvARB);
		ASSERT(MultiTexCoord4iARB );
		ASSERT(MultiTexCoord4ivARB);
		ASSERT(MultiTexCoord4sARB );
		ASSERT(MultiTexCoord4svARB);

		return TRUE;
	}	

#define MTLEEP_Load(x) if(!( *((PROC*)&x) = wglGetProcAddress("gl"#x) ))goto MTLEEP_LoadFailed
		MTLEEP_Load(ActiveTextureARB);
		MTLEEP_Load(ClientActiveTextureARB );
		MTLEEP_Load(MultiTexCoord1dARB );
		MTLEEP_Load(MultiTexCoord1dvARB);
		MTLEEP_Load(MultiTexCoord1fARB );
		MTLEEP_Load(MultiTexCoord1fvARB);
		MTLEEP_Load(MultiTexCoord1iARB );
		MTLEEP_Load(MultiTexCoord1ivARB);
		MTLEEP_Load(MultiTexCoord1sARB );
		MTLEEP_Load(MultiTexCoord1svARB);
		MTLEEP_Load(MultiTexCoord2dARB );
		MTLEEP_Load(MultiTexCoord2dvARB);
		MTLEEP_Load(MultiTexCoord2fARB );
		MTLEEP_Load(MultiTexCoord2fvARB);
		MTLEEP_Load(MultiTexCoord2iARB );
		MTLEEP_Load(MultiTexCoord2ivARB);
		MTLEEP_Load(MultiTexCoord2sARB );
		MTLEEP_Load(MultiTexCoord2svARB);
		MTLEEP_Load(MultiTexCoord3dARB );
		MTLEEP_Load(MultiTexCoord3dvARB);
		MTLEEP_Load(MultiTexCoord3fARB );
		MTLEEP_Load(MultiTexCoord3fvARB);
		MTLEEP_Load(MultiTexCoord3iARB );
		MTLEEP_Load(MultiTexCoord3ivARB);
		MTLEEP_Load(MultiTexCoord3sARB );
		MTLEEP_Load(MultiTexCoord3svARB);
		MTLEEP_Load(MultiTexCoord4dARB );
		MTLEEP_Load(MultiTexCoord4dvARB);
		MTLEEP_Load(MultiTexCoord4fARB );
		MTLEEP_Load(MultiTexCoord4fvARB);
		MTLEEP_Load(MultiTexCoord4iARB );
		MTLEEP_Load(MultiTexCoord4ivARB);
		MTLEEP_Load(MultiTexCoord4sARB );
		MTLEEP_Load(MultiTexCoord4svARB);

#undef MTLEEP_Load

	return TRUE;

MTLEEP_LoadFailed:
		
	ActiveTextureARB = NULL;
	return FALSE;
}

void wglh::ext::CwglMultiTextureEnv::ActiveTextureUnit(GLuint stage)
{
	//In fact, the max texture unit is not max texture stage. 
	//According to fragment program ARB. So this ASSERT should be disable
	//ASSERT(stage<wglh::ext::CwglMultiTextureEnv::MaxTextureStage());

	ActiveTextureARB(stage+GL_TEXTURE0_ARB);
	_CheckErrorGL;
}



PFNGLTEXIMAGE3DEXTPROC		wglh::ext::CwglTexture3D::TexImage3DEXT = NULL;
//PFNGLTEXSUBIMAGE3DEXTPROC	wglh::ext::CwglTexture3D::TexSubImage3DEXT = NULL;

BOOL wglh::ext::CwglTexture3D::LoadExtensionEntryPoints()
{
#define T3DLEEP_Load(x) if(!( *((PROC*)&x) = wglGetProcAddress("gl"#x) ))goto T3DLEEP_LoadFailed

	T3DLEEP_Load(TexImage3DEXT);
	//T3DLEEP_Load(TexSubImage3DEXT);

#undef  T3DLEEP_Load

	return TRUE;

T3DLEEP_LoadFailed:
	TexImage3DEXT = NULL;

	return FALSE;
}


LPCSTR wglh::ext::CwglExtensions::wglExtensionString = NULL;
PFNGLPOINTPARAMETERFEXTPROC		wglh::ext::CwglExtensions::PointParameterfARB = NULL;
PFNGLPOINTPARAMETERFVEXTPROC	wglh::ext::CwglExtensions::PointParameterfvARB = NULL;

PFNWGLGETEXTENSIONSSTRINGARBPROC	wglh::ext::CwglExtensions::GetExtensionsStringARB = NULL;
PFNGLCOLORPOINTEREXTPROC			wglh::ext::CwglExtensions::ColorPointerEXT = NULL;
PFNGLEDGEFLAGPOINTEREXTPROC			wglh::ext::CwglExtensions::EdgeFlagPointerEXT = NULL;
PFNGLINDEXPOINTEREXTPROC			wglh::ext::CwglExtensions::IndexPointerEXT = NULL;
PFNGLNORMALPOINTEREXTPROC			wglh::ext::CwglExtensions::NormalPointerEXT = NULL;
PFNGLTEXCOORDPOINTEREXTPROC			wglh::ext::CwglExtensions::TexCoordPointerEXT = NULL;
PFNGLVERTEXPOINTEREXTPROC			wglh::ext::CwglExtensions::VertexPointerEXT = NULL;  

PFNGLARRAYELEMENTEXTPROC		wglh::ext::CwglExtensions::VertexArray_ArrayElement = NULL;
PFNGLDRAWARRAYSEXTPROC			wglh::ext::CwglExtensions::VertexArray_DrawArrays = NULL;
PFNGLGETPOINTERVEXTPROC			wglh::ext::CwglExtensions::VertexArray_GetPointerv = NULL;

PFNGLLOCKARRAYSEXTPROC			wglh::ext::CwglExtensions::LockArraysEXT = NULL;
PFNGLUNLOCKARRAYSEXTPROC		wglh::ext::CwglExtensions::UnlockArraysEXT = NULL;
PFNGLDRAWBUFFERSATIPROC			wglh::ext::CwglExtensions::DrawBuffersATI = NULL;

PFNWGLSWAPINTERVALEXTPROC		wglh::ext::CwglExtensions::SwapIntervalEXT = NULL;
PFNWGLGETSWAPINTERVALEXTPROC	wglh::ext::CwglExtensions::GetSwapIntervalEXT = NULL;



BOOL wglh::ext::CwglExtensions::LoadExtensionEntryPoints()
{
	BOOL Ret = FALSE;
	// For WGL_EXT_extensions_string
	PFNWGLGETEXTENSIONSSTRINGEXTPROC wglGetExtension = (PFNWGLGETEXTENSIONSSTRINGEXTPROC)wglGetProcAddress("wglGetExtensionsStringEXT");
	if( wglGetExtension )
	{
		wglExtensionString = wglGetExtension();
		Ret = TRUE;
	}

#define NAME_GLELEEP_Load(x,name)	if(( *((PROC*)&x) = wglGetProcAddress(name) )){Ret = TRUE;}
#define GLELEEP_Load(x)				NAME_GLELEEP_Load(x,"gl"#x)
#define WGLELEEP_Load(x)			NAME_GLELEEP_Load(x,"wgl"#x)

	// GL_EXT_point_parameters
	GLELEEP_Load(PointParameterfARB);
	if( PointParameterfARB ){}
	else{ if(( *((PROC*)&PointParameterfARB) = wglGetProcAddress("glPointParameterfEXT") )){Ret = TRUE;} }
	GLELEEP_Load(PointParameterfvARB);
	if( PointParameterfvARB ){}
	else{ if(( *((PROC*)&PointParameterfvARB) = wglGetProcAddress("glPointParameterfvEXT") )){Ret = TRUE;} }

	// for WGL_ARB_extensions_string and WGL_EXT_extensions_string
	WGLELEEP_Load(GetExtensionsStringARB);

	// for GL_EXT_vertex_array
	NAME_GLELEEP_Load(VertexArray_ArrayElement,"glArrayElementEXT");
	NAME_GLELEEP_Load(VertexArray_DrawArrays,"glDrawArraysEXT");
	NAME_GLELEEP_Load(VertexArray_GetPointerv,"glGetPointervEXT");

	GLELEEP_Load(ColorPointerEXT);
	GLELEEP_Load(EdgeFlagPointerEXT);
	GLELEEP_Load(IndexPointerEXT);
	GLELEEP_Load(NormalPointerEXT);
	GLELEEP_Load(TexCoordPointerEXT);
	GLELEEP_Load(VertexPointerEXT);  
	// if GL_EXT_vertex_array dosenot exists, route callings to GL1.1's VA callings
	if( VertexPointerEXT )
	{
		ASSERT( VertexArray_ArrayElement && VertexArray_DrawArrays && VertexArray_GetPointerv );
		ASSERT( ColorPointerEXT && EdgeFlagPointerEXT && IndexPointerEXT && NormalPointerEXT && TexCoordPointerEXT );
	}
	else
	{
		ColorPointerEXT = NULL;
		EdgeFlagPointerEXT = NULL;
		IndexPointerEXT = NULL;
		NormalPointerEXT = NULL;
		TexCoordPointerEXT = NULL;
		VertexArray_ArrayElement = glArrayElement;
		VertexArray_DrawArrays = glDrawArrays;
		VertexArray_GetPointerv = glGetPointerv;
	}

	// GL_EXT_compiled_vertex_array
	GLELEEP_Load(LockArraysEXT);
	GLELEEP_Load(UnlockArraysEXT);

	// for GL_ATI_draw_buffers
	GLELEEP_Load(DrawBuffersATI);

	// for WGL_EXT_swap_control
	WGLELEEP_Load(SwapIntervalEXT);
	WGLELEEP_Load(GetSwapIntervalEXT);
    
#undef NAME_GLELEEP_Load
#undef WGLELEEP_Load
#undef GLELEEP_Load

	return Ret;
}

BOOL wglh::ext::CwglExtensions::CheckExtensionWGL(LPCSTR pExtName)
{
	ASSERT( pExtName );
	return wglExtensionString?((BOOL)strstr(wglExtensionString,pExtName)):FALSE;
}

void   wglh::ext::CwglExtensions::PointParameter_MinMax(GLfloat min,GLfloat max)
{
	if(min>=0)
	{
		PointParameterfARB(GL_POINT_SIZE_MIN_ARB,min);
		_CheckErrorGL;
	}

	if(max>=0)
	{
		PointParameterfARB(GL_POINT_SIZE_MAX_ARB,max);
		_CheckErrorGL;
	}
}

void   wglh::ext::CwglExtensions::PointParameter_FadeThreshold(GLfloat threshold)
{
	ASSERT(threshold>=0);
	PointParameterfARB(GL_POINT_FADE_THRESHOLD_SIZE_ARB,threshold);
	_CheckErrorGL;
}

void   wglh::ext::CwglExtensions::PointParameter_DistanceAttenuation(GLfloat a,GLfloat b,GLfloat c)
{
	GLfloat	v3[3];
	v3[0] = a;
	v3[1] = b;
	v3[2] = c;

	ASSERT( a!=0 || b!=0 || c!=0 );

    PointParameterfvARB(GL_POINT_DISTANCE_ATTENUATION_ARB,v3);
	_CheckErrorGL;
}

void wglh::ext::CwglExtensions::VertexArray_Pointers(CwglExtensions::LPCVertexArrayPointers pVA,BOOL AutoEnableArrays)
{
	ASSERT(pVA);

	// if GL_EXT_vertex_array is available
	if( VertexPointerEXT )
	{
		if( pVA->Color_Pointer )
		{
			ColorPointerEXT(pVA->Color_Components, pVA->Color_ValueType, pVA->Color_ByteStride,pVA->StaticVextexCount, pVA->Color_Pointer);
			_CheckErrorGL;
			if( AutoEnableArrays )glEnable( CwglExtensions::VertexArrayId_Color );
		}
		else
		{
			if( AutoEnableArrays )glDisable( CwglExtensions::VertexArrayId_Color );
		}

		if( !pVA->ColorIndex_Pointer )
		{
			if( AutoEnableArrays )glDisable( CwglExtensions::VertexArrayId_Index );
		}
		else
		{
			IndexPointerEXT(pVA->ColorIndex_ValueType,pVA->ColorIndex_ByteStride,pVA->StaticVextexCount,pVA->ColorIndex_Pointer);
			_CheckErrorGL;
			if( AutoEnableArrays )glEnable( CwglExtensions::VertexArrayId_Index );
		}

		if( !pVA->EdgeFlag_Pointer )
		{
			if( AutoEnableArrays )glDisable( CwglExtensions::VertexArrayId_EdgeFlag );
		}
		else
		{
			EdgeFlagPointerEXT(pVA->EdgeFlag_ByteStride,pVA->StaticVextexCount,(const GLboolean*)pVA->EdgeFlag_Pointer);
			_CheckErrorGL;
			if( AutoEnableArrays )glEnable( CwglExtensions::VertexArrayId_EdgeFlag );
		}

		if( pVA->Normal_Pointer )
		{
			NormalPointerEXT(pVA->Normal_ValueType,pVA->Normal_ByteStride,pVA->StaticVextexCount,pVA->Normal_Pointer);
			_CheckErrorGL;
			if( AutoEnableArrays )glEnable( CwglExtensions::VertexArrayId_Normal );
		}
		else
		{
			if( AutoEnableArrays )glDisable( CwglExtensions::VertexArrayId_Normal );
		}

		if( pVA->TexCoord_Pointer )
		{
			TexCoordPointerEXT(	pVA->TexCoord_Components,pVA->Color_ValueType,pVA->TexCoord_ByteStride,
								pVA->StaticVextexCount,pVA->TexCoord_Pointer);
			_CheckErrorGL;
			if( AutoEnableArrays )glEnable( CwglExtensions::VertexArrayId_TexCoord );
		}
		else
		{
			if( AutoEnableArrays )glDisable( CwglExtensions::VertexArrayId_TexCoord );
		}

		if( pVA->Vertex_Pointer )
		{
			VertexPointerEXT(	pVA->Vertex_Components,pVA->Vertex_ValueType,pVA->Vertex_ByteStride,
								pVA->StaticVextexCount,pVA->Vertex_Pointer);
			_CheckErrorGL;
			if( AutoEnableArrays )glEnable( CwglExtensions::VertexArrayId_Vertex );
		}
		else
		{
			if( AutoEnableArrays )glDisable( CwglExtensions::VertexArrayId_Vertex );
		}
	}
	else
	{
		if( pVA->Color_Pointer )
		{
			glColorPointer(pVA->Color_Components, pVA->Color_ValueType, pVA->Color_ByteStride, pVA->Color_Pointer);
			_CheckErrorGL;
			if( AutoEnableArrays )glEnable( CwglExtensions::VertexArrayId_Color );
		}
		else
		{
			if( AutoEnableArrays )glDisable( CwglExtensions::VertexArrayId_Color );
		}

		if( !pVA->ColorIndex_Pointer )
		{
			if( AutoEnableArrays )glDisable( CwglExtensions::VertexArrayId_Index );
		}
		else
		{
			glIndexPointer(pVA->ColorIndex_ValueType,pVA->ColorIndex_ByteStride,pVA->ColorIndex_Pointer);
			_CheckErrorGL;
			if( AutoEnableArrays )glEnable( CwglExtensions::VertexArrayId_Index );
		}

		if( !pVA->EdgeFlag_Pointer )
		{
			if( AutoEnableArrays )glDisable( CwglExtensions::VertexArrayId_EdgeFlag );
		}
		else
		{
			glEdgeFlagPointer(pVA->EdgeFlag_ByteStride,pVA->EdgeFlag_Pointer);
			_CheckErrorGL;
			if( AutoEnableArrays )glEnable( CwglExtensions::VertexArrayId_EdgeFlag );
		}

		if( pVA->Normal_Pointer )
		{
			glNormalPointer(pVA->Normal_ValueType,pVA->Normal_ByteStride,pVA->Normal_Pointer);
			_CheckErrorGL;
			if( AutoEnableArrays )glEnable( CwglExtensions::VertexArrayId_Normal );
		}
		else
		{
			if( AutoEnableArrays )glDisable( CwglExtensions::VertexArrayId_Normal );
		}

		if( pVA->TexCoord_Pointer )
		{
			glTexCoordPointer(	pVA->TexCoord_Components,pVA->Color_ValueType,pVA->TexCoord_ByteStride,
								pVA->TexCoord_Pointer);
			_CheckErrorGL;
			if( AutoEnableArrays )glEnable( CwglExtensions::VertexArrayId_TexCoord );
		}
		else
		{
			if( AutoEnableArrays )glDisable( CwglExtensions::VertexArrayId_TexCoord );
		}

		if( pVA->Vertex_Pointer )
		{
			glVertexPointer(	pVA->Vertex_Components,pVA->Vertex_ValueType,pVA->Vertex_ByteStride,
								pVA->Vertex_Pointer);
			_CheckErrorGL;
			if( AutoEnableArrays )glEnable( CwglExtensions::VertexArrayId_Vertex );
		}
		else
		{
			if( AutoEnableArrays )glDisable( CwglExtensions::VertexArrayId_Vertex );
		}
	}
}

PFNGLBINDBUFFERARBPROC				wglh::ext::CwglVertexBufferBase::BindBufferARB = NULL;
PFNGLDELETEBUFFERSARBPROC			wglh::ext::CwglVertexBufferBase::DeleteBuffersARB = NULL;
PFNGLGENBUFFERSARBPROC				wglh::ext::CwglVertexBufferBase::GenBuffersARB = NULL;
PFNGLISBUFFERARBPROC				wglh::ext::CwglVertexBufferBase::IsBufferARB = NULL;
PFNGLBUFFERDATAARBPROC				wglh::ext::CwglVertexBufferBase::BufferDataARB = NULL;
PFNGLBUFFERSUBDATAARBPROC			wglh::ext::CwglVertexBufferBase::BufferSubDataARB = NULL;
PFNGLGETBUFFERSUBDATAARBPROC		wglh::ext::CwglVertexBufferBase::GetBufferSubDataARB = NULL;
PFNGLMAPBUFFERARBPROC				wglh::ext::CwglVertexBufferBase::MapBufferARB = NULL;
PFNGLUNMAPBUFFERARBPROC				wglh::ext::CwglVertexBufferBase::UnmapBufferARB = NULL;
PFNGLGETBUFFERPARAMETERIVARBPROC	wglh::ext::CwglVertexBufferBase::GetBufferParameterivARB = NULL;
PFNGLGETBUFFERPOINTERVARBPROC		wglh::ext::CwglVertexBufferBase::GetBufferPointervARB = NULL;

BOOL wglh::ext::CwglVertexBufferBase::LoadExtensionEntryPoints()
{
#define VBLEEP_Load(x) if(!( *((PROC*)&x) = wglGetProcAddress("gl"#x) ))goto VBLEEP_LoadFailed
	
	VBLEEP_Load(BindBufferARB);
	VBLEEP_Load(DeleteBuffersARB);
	VBLEEP_Load(GenBuffersARB);
	VBLEEP_Load(IsBufferARB);
	VBLEEP_Load(BufferDataARB);
	VBLEEP_Load(BufferSubDataARB);
	VBLEEP_Load(GetBufferSubDataARB);
	VBLEEP_Load(MapBufferARB);
	VBLEEP_Load(UnmapBufferARB);
	VBLEEP_Load(GetBufferParameterivARB);
	VBLEEP_Load(GetBufferPointervARB);

	return TRUE;

#undef VBLEEP_Load

VBLEEP_LoadFailed:
	GenBuffersARB = NULL;
	printf("Load Vertex_Buffer_Object Extension FAILED!\n");
	return FALSE;
}

BOOL wglh::ext::CwglVertexBufferBase::Create(DWORD StorageHint)
{
	if(GenBuffersARB)
	{
		if(!VertexBufferName)
		{
			GenBuffersARB(1,&VertexBufferName);
			_CheckErrorGL;
		}

		ASSERT( StorageHint != StorageHint_NoChange );
		LastStorageHint = StorageHint;

		return VertexBufferName;
	}
	else
	{
		return TRUE;
	}
}

void wglh::ext::CwglVertexBufferBase::Destroy()
{
	if( GenBuffersARB )
	{
		if(VertexBufferName)
		{
			DeleteBuffersARB(1,&VertexBufferName); 
			_CheckErrorGL; 
		}
	}
	else
	{
		_SafeDelArray(VertexBufferPtrBase);
	}

	VertexBufferName = NULL;
	BufferSize = 0;
}

namespace wglh
{	namespace ext
	{	class _SetOpenGLExtensionEntrypointLoader
		{
		public:
			_SetOpenGLExtensionEntrypointLoader()
			{ wglh::CwglWnd::_OpenGLExtensionEntrypointLoader = wglh::wglLoadAllExtensionEntryPoints; }
		};
		static _SetOpenGLExtensionEntrypointLoader	_SOGEEL;
}	} // namespace wglh::ext

void wglh::wglLoadAllExtensionEntryPoints()
{
	static BOOL _is_loaded = FALSE;
	if(_is_loaded)return;

	_is_loaded = TRUE;

	if(	wglh::ext::ARBvpfp::CwglGpuProgram::LoadExtensionEntryPoints() &&
		wglh::ext::ARBvpfp::CwglVertexProgram::LoadExtensionEntryPoints() )
	{
		_CheckDump(_T("OpenGL Vertex Program Extension Loaded\n"));
		if(wglh::ext::ARBvpfp::CwglFragmentProgram::LoadExtensionEntryPoints())
		{
			_CheckDump(_T("OpenGL Fragment Program Extension Loaded\n"));
		}
		else
		{
			_CheckDump(_T("  Failed to load Fragment Program Extension (ARB_fragment_program)\n"));
		}
	}
	else
	{
		_CheckDump(_T("  Failed to load Vertex Program Extension (ARB_vertex_program)\n"));
	}

	if(	wglh::glsl::CwglLanguageObject::LoadExtensionEntryPoints())
	{
		_CheckDump(_T("OpenGL Shading Language Extension (glsl) Loaded\n"));
	}
	else
	{
		_CheckDump(_T("  Failed to load GLSL Extension (GL_ARB_shading_language_1xx)\n"));
	}

	if(wglh::ext::CwglMultiTextureEnv::LoadExtensionEntryPoints())
	{
		_CheckDump(_T("OpenGL MultiTexture Extension Loaded\n"));
	}
	else
	{
		_CheckDump(_T("  Failed to load MultiTexture Extension (ARB_multitexture)\n"));
	}

	if(wglh::ext::CwglTexture3D::LoadExtensionEntryPoints())
	{
		_CheckDump(_T("OpenGL 3D Texture Extension Loaded\n"));
	}
	else
	{
		_CheckDump(_T("  Failed to load 3D Texture Extension (EXT_texture3D)\n"));
	}

	if(wglh::ext::CwglVertexBufferBase::LoadExtensionEntryPoints())
	{
		_CheckDump(_T("OpenGL Vertex Buffer Object Extension Loaded\n"));
	}
	else
	{
		_CheckDump(_T("  Failed to load Vertex Buffer Object Extension (ARB_vertex_buffer_object)\n"));
	}

	wglh::ext::CwglExtensions::LoadExtensionEntryPoints();
}