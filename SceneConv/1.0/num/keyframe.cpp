#include "stdafx.h"
#include "keyframe.h"

namespace num
{


KeyframeControl::KeyframePtr::KeyframePtr()
{
    parent = NULL;
}


KeyframeControl::KeyframePtr::KeyframePtr(const KeyframePtr& kfp)
{
    parent = kfp.parent;
    value = kfp.value;
}


float KeyframeControl::KeyframePtr::GetTime() const
{
    ASSERT( parent != NULL );
    return value->pos->first;
}


Frame& KeyframeControl::KeyframePtr::GetFrame()
{
	ASSERT( parent != NULL );
	return value->data;
}


void KeyframeControl::KeyframePtr::GetFrame(Frame &frame) const
{
    ASSERT( parent != NULL );
    frame = value->data;
}


const KeyframeControl::KeyframePtr& KeyframeControl::KeyframePtr::operator =(const KeyframeControl::KeyframePtr &kfp)
{
    parent = kfp.parent;
    value = kfp.value;
    return *this;
}


bool KeyframeControl::KeyframePtr::operator ==(const KeyframeControl::KeyframePtr &kfp) const
{
    return ( parent == kfp.parent && value == kfp.value );
}


const KeyframeControl::KeyframePtr& KeyframeControl::KeyframePtr::operator ++()
{
    ASSERT( parent != NULL );
    MAP::iterator it = value->pos;
    ++it;
    value = it->second;
    return *this;
}


KeyframeControl::KeyframePtr KeyframeControl::KeyframePtr::operator ++(int)
{
    ASSERT( parent != NULL );
    KeyframePtr tmp(*this);
    MAP::iterator it = value->pos;
    ++it;
    value = it->second;
    return tmp;
}


const KeyframeControl::KeyframePtr& KeyframeControl::KeyframePtr::operator --()
{
    ASSERT( parent != NULL );
    MAP::iterator it = value->pos;
    --it;
    value = it->second;
    return *this;
}


KeyframeControl::KeyframePtr KeyframeControl::KeyframePtr::operator --(int)
{
    ASSERT( parent != NULL );
    KeyframePtr tmp(*this);
    MAP::iterator it = value->pos;
    --it;
    value = it->second;
    return tmp;
}


KeyframeControl::KeyframePtr::operator WPARAM() const
{
    WPARAM res;
    memcpy(&res, &value, sizeof(WPARAM));
    return res;
}


bool KeyframeControl::KeyframePtr::IsFirst() const
{
    ASSERT( parent != NULL );
    return ( parent->m_keyframe.begin()->second == value );
}


bool KeyframeControl::KeyframePtr::IsLast() const
{
    ASSERT( parent != NULL );
    MAP::const_iterator it = parent->m_keyframe.end();
    return ( (--it)->second == value );
}


KeyframeControl::KeyframeControl()
{
    SetKeyframeFormat(0, 0, 0, false);
}


KeyframeControl::~KeyframeControl()
{
    SetKeyframeFormat(0, 0, 0, false);
}


KeyframeControl::KeyframeControl(const KeyframeControl *kfc)
{
	// do not allow copy construction
	ASSERT(0);
}


const KeyframeControl& KeyframeControl::operator = (const KeyframeControl& kfc)
{
	// do not allow copy construction
	ASSERT(0);
	return kfc;
}


void KeyframeControl::SetKeyframeFormat(int Mat4x4_Count, int Vec3_Count, int Value_Count, bool fill)
{
    m_Mat4x4_Count = Mat4x4_Count;
    m_Vec3_Count = Vec3_Count;
    m_Value_Count = Value_Count;

    m_Buffered_Mat4x4_IntMethod.SetSize(Mat4x4_Count);
    m_Buffered_Vec3_IntMethod.SetSize(Vec3_Count);
    m_Buffered_Value_IntMethod.SetSize(Value_Count);

    if ( fill )
    {
        m_Buffered_Mat4x4_IntMethod.Set(INT_MAT4x4_QUATERNION);
        m_Buffered_Vec3_IntMethod.Set(INT_LINEAR);
        m_Buffered_Value_IntMethod.Set(INT_LINEAR);
    }

    m_Buffered_Mat4x4_Offset.SetSize(Mat4x4_Count);
    m_Buffered_Vec3_Offset.SetSize(Vec3_Count);
    m_Buffered_Value_Offset.SetSize(Value_Count);

    int total_linear = 0, total_cubic = 0;
    int i;

    for ( i = 0; i < (int)m_Buffered_Mat4x4_IntMethod.GetSize(); ++i )
        if ( m_Buffered_Mat4x4_IntMethod[i] == INT_MAT4x4_QUATERNION )
        {
            m_Buffered_Mat4x4_Offset[i].x = INT_MAT4x4_QUATERNION;
            m_Buffered_Mat4x4_Offset[i].y = i;
        }
        else
        {
            ASSERT(0);
        }

    for ( i = 0; i < (int)m_Buffered_Vec3_IntMethod.GetSize(); ++i )
        if ( m_Buffered_Vec3_IntMethod[i] == INT_LINEAR )
        {
            m_Buffered_Vec3_Offset[i].x = INT_LINEAR;
            m_Buffered_Vec3_Offset[i].y = total_linear;
            total_linear += 3;
        }
        else if ( m_Buffered_Vec3_IntMethod[i] == INT_CUBIC )
        {
            m_Buffered_Vec3_Offset[i].x = INT_CUBIC;
            m_Buffered_Vec3_Offset[i].y = total_cubic;
            total_cubic += 3;
        }
        else
        {
            ASSERT(0);
        }

    for ( i = 0; i < (int)m_Buffered_Value_IntMethod.GetSize(); ++i )
        if ( m_Buffered_Value_IntMethod[i] == INT_LINEAR )
        {
            m_Buffered_Value_Offset[i].x = INT_LINEAR;
            m_Buffered_Value_Offset[i].y = total_linear++;
        }
        else if ( m_Buffered_Value_IntMethod[i] == INT_CUBIC )
        {
            m_Buffered_Value_Offset[i].x = INT_CUBIC;
            m_Buffered_Value_Offset[i].y = total_cubic++;
        }
        else
        {
            ASSERT(0);
        }

    m_Buffered_Mat4x4Int.SetSize(Mat4x4_Count);
    for ( i = 0; i < Mat4x4_Count; ++i ) m_Buffered_Mat4x4Int[i].Clear();
    m_Buffered_LinearInt.SetSize(total_linear);
    for ( i = 0; i < total_linear; ++i ) m_Buffered_LinearInt[i].Clear();
    m_Buffered_CubicInt.SetSize(total_cubic);
    for ( i = 0; i < total_cubic; ++i ) m_Buffered_CubicInt[i].Clear();

    MAP::iterator it;
    for ( it = m_keyframe.begin(); it != m_keyframe.end(); ++it )
	{
		delete it->second;
		it->second = NULL;
	}
    m_keyframe.clear();

    m_FirstTime = m_LastTime = 0.0f;
}


void KeyframeControl::SetInterpolateMethod(int trg, int pos, int method)
{
    switch ( trg )
    {
    case TRG_MAT4x4:
        m_Buffered_Mat4x4_IntMethod[pos] = method;
        break;

    case TRG_VEC3:
        m_Buffered_Vec3_IntMethod[pos] = method;
        break;

    case TRG_VALUE:
        m_Buffered_Value_IntMethod[pos] = method;
        break;

    default:
        ASSERT(0);
    }

    SetKeyframeFormat(m_Mat4x4_Count, m_Vec3_Count, m_Value_Count, false);
}


bool KeyframeControl::add(const Frame& keyframe, KeyframePtr &ptr, Keyframe_Data *data)
{
    Frame kf(keyframe);
    int i;

    ASSERT( keyframe.m_Buffered_Mat4x4.GetSize() == m_Mat4x4_Count );
    ASSERT( keyframe.m_Buffered_Vec3.GetSize() == m_Vec3_Count );
    ASSERT( keyframe.m_Buffered_Value.GetSize() == m_Value_Count );

    if ( m_keyframe.find(kf.m_time) != m_keyframe.end() ) return false;

    for ( i = 0; i < m_Mat4x4_Count; ++i )
        if ( m_Buffered_Mat4x4_Offset[i].x == INT_MAT4x4_QUATERNION )
        {
            m_Buffered_Mat4x4Int[m_Buffered_Mat4x4_Offset[i].y].Set(kf.m_time, kf.m_Buffered_Mat4x4[i]);
        }
        else
        {
            ASSERT(0);
        }

    for ( i = 0; i < m_Vec3_Count; ++i )
    {
        int pos = m_Buffered_Vec3_Offset[i].y;

        if ( m_Buffered_Vec3_Offset[i].x == INT_LINEAR )
        {
            m_Buffered_LinearInt[pos].Set(kf.m_time, kf.m_Buffered_Vec3[i].x);
            m_Buffered_LinearInt[pos + 1].Set(kf.m_time, kf.m_Buffered_Vec3[i].y);
            m_Buffered_LinearInt[pos + 2].Set(kf.m_time, kf.m_Buffered_Vec3[i].z);
        }
        else if ( m_Buffered_Vec3_Offset[i].x == INT_CUBIC )
        {
            m_Buffered_CubicInt[pos].Set(kf.m_time, kf.m_Buffered_Vec3[i].x);
            m_Buffered_CubicInt[pos + 1].Set(kf.m_time, kf.m_Buffered_Vec3[i].y);
            m_Buffered_CubicInt[pos + 2].Set(kf.m_time, kf.m_Buffered_Vec3[i].z);
        }
        else
        {
            ASSERT(0);
        }
    }

    for ( i = 0; i < m_Value_Count; ++i )
    {
        int pos = m_Buffered_Value_Offset[i].y;

        if ( m_Buffered_Value_Offset[i].x == INT_LINEAR )
        {
            m_Buffered_LinearInt[pos].Set(kf.m_time, kf.m_Buffered_Value[i]);
        }
        else if ( m_Buffered_Value_Offset[i].x == INT_CUBIC )
        {
            m_Buffered_CubicInt[pos].Set(kf.m_time, kf.m_Buffered_Value[i]);
        }
        else
        {
            ASSERT(0);
        }
    }

    MAP::iterator it = m_keyframe.insert(MAP::value_type(kf.m_time, reinterpret_cast<Keyframe_Data*>(NULL))).first;
    ptr.parent = this;
    if ( data == NULL )
    {
        ptr.value = new Keyframe_Data(kf, it);
    }
    else
    {
        ptr.value = data;
        data->pos = it;
    }
    it->second = ptr.value;

    m_FirstTime = m_keyframe.begin()->first;
    m_LastTime = m_keyframe.rbegin()->first;

    return true;
}


bool KeyframeControl::AddKeyframe(const Frame &keyframe)
{
	KeyframePtr ptr;
    return add(keyframe, ptr, NULL);
}


bool KeyframeControl::AddKeyframe(const Frame &keyframe, KeyframePtr &ptr)
{
    return add(keyframe, ptr, NULL);
}


//bool KeyframeControl::GetKeyframe(const KeyframePtr &ptr, Frame &kf) const
//{
//    ASSERT( ptr.parent == this );
//    MAP::const_iterator it = m_keyframe.find(ptr.value->first);
//    if ( it != m_keyframe.end() )
//    {
//        kf = *(it->second);
//        return true;
//    }
//    else
//        return false;
//}

void KeyframeControl::remove(const KeyframePtr &ptr, bool remove_data)
{
    ASSERT( ptr.parent == this );

    MAP::iterator it = ptr.value->pos;
    float t = it->first;
    int i;

    for ( i = 0; i < m_Mat4x4_Count; ++i )
        if ( m_Buffered_Mat4x4_Offset[i].x == INT_MAT4x4_QUATERNION )
        {
            m_Buffered_Mat4x4Int[m_Buffered_Mat4x4_Offset[i].y].Remove(t);
        }
        else
        {
            ASSERT(0);
        }

    for ( i = 0; i < m_Vec3_Count; ++i )
    {
        int pos = m_Buffered_Vec3_Offset[i].y;

        if ( m_Buffered_Vec3_Offset[i].x == INT_LINEAR )
        {
            m_Buffered_LinearInt[pos].Remove(t);
            m_Buffered_LinearInt[pos + 1].Remove(t);
            m_Buffered_LinearInt[pos + 2].Remove(t);
        }
        else if ( m_Buffered_Vec3_Offset[i].x == INT_CUBIC )
        {
            m_Buffered_CubicInt[pos].Remove(t);
            m_Buffered_CubicInt[pos + 1].Remove(t);
            m_Buffered_CubicInt[pos + 2].Remove(t);
        }
        else
        {
            ASSERT(0);
        }
    }

    for ( i = 0; i < m_Value_Count; ++i )
    {
        int pos = m_Buffered_Value_Offset[i].y;

        if ( m_Buffered_Value_Offset[i].x == INT_LINEAR )
        {
            m_Buffered_LinearInt[pos].Remove(t);
        }
        else if ( m_Buffered_Value_Offset[i].x == INT_CUBIC )
        {
            m_Buffered_CubicInt[pos].Remove(t);
        }
        else
        {
            ASSERT(0);
        }
    }

    if ( remove_data ) delete it->second;
    it->second = NULL;
    m_keyframe.erase(it);

    if ( !m_keyframe.empty() )
    {
        m_FirstTime = m_keyframe.begin()->first;
        m_LastTime = m_keyframe.rbegin()->first;
    }
    else
    {
        m_FirstTime = m_LastTime = 0.0f;
    }
}


void KeyframeControl::RemoveKeyframe(const KeyframePtr &ptr)
{
    remove(ptr, true);
}


bool KeyframeControl::GetFrame(float time, Frame &frame) const
{
    if ( m_keyframe.empty() )
        return false;

    //if ( less(time, m_FirstTime) ) return false;
    //if ( less(m_LastTime, time) ) return false;

    frame.m_time = time;
    frame.SetSize(m_Mat4x4_Count, m_Vec3_Count, m_Value_Count);

    int i;
    for ( i = 0; i < m_Mat4x4_Count; ++i )
        if ( m_Buffered_Mat4x4_Offset[i].x == INT_MAT4x4_QUATERNION )
        {
            m_Buffered_Mat4x4Int[m_Buffered_Mat4x4_Offset[i].y].Query(time, frame.m_Buffered_Mat4x4[i]);
        }
        else
        {
            ASSERT(0);
        }

    for ( i = 0; i < m_Vec3_Count; ++i )
    {
        int pos = m_Buffered_Vec3_Offset[i].y;

        if ( m_Buffered_Vec3_Offset[i].x == INT_LINEAR )
        {
            m_Buffered_LinearInt[pos].Query(time, frame.m_Buffered_Vec3[i].x);
            m_Buffered_LinearInt[pos + 1].Query(time, frame.m_Buffered_Vec3[i].y);
            m_Buffered_LinearInt[pos + 2].Query(time, frame.m_Buffered_Vec3[i].z);
        }
        else if ( m_Buffered_Vec3_Offset[i].x == INT_CUBIC )
        {
            m_Buffered_CubicInt[pos].Query(time, frame.m_Buffered_Vec3[i].x);
            m_Buffered_CubicInt[pos + 1].Query(time, frame.m_Buffered_Vec3[i].y);
            m_Buffered_CubicInt[pos + 2].Query(time, frame.m_Buffered_Vec3[i].z);
        }
        else
        {
            ASSERT(0);
        }
    }

    for ( i = 0; i < m_Value_Count; ++i )
    {
        int pos = m_Buffered_Value_Offset[i].y;

        if ( m_Buffered_Value_Offset[i].x == INT_LINEAR )
        {
            m_Buffered_LinearInt[pos].Query(time, frame.m_Buffered_Value[i]);
        }
        else if ( m_Buffered_Value_Offset[i].x == INT_CUBIC )
        {
            m_Buffered_CubicInt[pos].Query(time, frame.m_Buffered_Value[i]);
        }
        else
        {
            ASSERT(0);
        }
    }

    return true;
}

void KeyframeControl::UpdateKeyframe(const KeyframePtr &ptr, const Frame &keyframe)
{
    bool ret;

    ASSERT( ptr.parent == this );
    Keyframe_Data *data = ptr.value;
    remove(ptr, false);
    KeyframePtr tmp;
    ret = add(keyframe, tmp, data);
    ASSERT( tmp == ptr );
    ASSERT( ret );
}


KeyframeControl::KeyframePtr KeyframeControl::GetFirstKeyframe()
{
    KeyframePtr res;
    res.parent = this;
    res.value = m_keyframe.begin()->second;
    return res;
}


KeyframeControl::KeyframePtr KeyframeControl::GetLastKeyframe()
{
    KeyframePtr res;
    res.parent = this;
    MAP::iterator it = m_keyframe.end();
    --it;
    res.value = it->second;
    return res;
}


KeyframeControl::KeyframePtr KeyframeControl::GetKeyframe(WPARAM handle)
{
    KeyframePtr res;
    res.parent = this;
    memcpy(&res.value, &handle, sizeof(WPARAM));
    //res.value = (Keyframe_Data *)(handle);
    return res;
}


}
