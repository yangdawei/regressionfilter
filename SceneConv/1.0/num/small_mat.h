#pragma once

//////////////////////////////////////////////////////////////////////
// Visual Computing Foundation Classes (VCFC)
//					Jiaping Wang  2006.2
//					e_boris2002@hotmail.com
//
// (C) Copyright Jiaping Wang 2004.
//
// Permission to copy, use, modify, sell and distribute this software
// is granted provided this copyright notice appears in all copies.
// This software is provided "as is" without express or implied
// warranty, and with no claim as to its suitability for any purpose.
//
// Absolutely no warranty!!
//////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////
//  Brief Description
//
//  small_mat.h
//
//  small matrix
//
//////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////
// Revise History
//
// Inital version	2006.6.10		Jiaping Wang
// Fix bug			2006.11.10		Shuang Zhao
// Fix bug			2010.11.02		Shuang Zhao
//
//////////////////////////////////////////////////////////////////////

#include "..\rt\runtime_base.h"
#include "..\rt\member_visitor.h"
#include "small_vec.h"

namespace num
{

namespace _meta_
{
	__forceinline LPCTSTR _text_Mat4x4_(){ return _T("Mat4x4<"); }
	__forceinline LPCTSTR _text_Mat3x3_(){ return _T("Mat3x3<"); }
}

#define DYNTYPE_FUNC __forceinline

#pragma pack(1)

/////////////////////////////////////////////////////////////
// small matrix
template<typename t_Val>
class Mat4x4;

template<typename t_Val>
class Mat3x3
{	typedef rt::TypeTraits<t_Val>	TE;
public:
	operator t_Val*(){ return m[0]; }
	operator const t_Val*()const{ return m[0]; }

	TYPETRAITS_DECL_TYPEID(rt::_typeid_codelib)
	TYPETRAITS_DECL_EXTENDTYPEID((rt::_typeid_codelib<<16)+113)
	TYPETRAITS_DECL_ACCUM_TYPE(Mat3x3<typename TE::t_Accum>)		
	TYPETRAITS_DECL_VALUE_TYPE(Mat3x3<typename TE::t_Val>)		
	TYPETRAITS_DECL_ELEMENT_TYPE(num::Vec3<t_Val>)		
	TYPETRAITS_DECL_SIGNED_TYPE(Mat3x3<typename TE::t_Signed>)	
	TYPETRAITS_DECL_IS_AGGREGATE(true)	
	TYPETRAITS_DECL_IS_NUMERIC(false)		
	TYPETRAITS_DECL_IS_ARRAY(true)		
	TYPETRAITS_DECL_LENGTH(3)

	num::Vec3<t_Val>& operator [](UINT i){ return *((num::Vec3<t_Val>*)m[i]); }
	const num::Vec3<t_Val>& operator [](UINT i)const{ return *((const num::Vec3<t_Val>*)m[i]); }

	template<class ostream>
	static void __TypeName(ostream & outstr)
	{	outstr<<_meta_::_text_Mat3x3_(); rt::TypeTraits<t_Val>::TypeName(outstr);
		outstr<<_T('>');
	}


	union{
        t_Val m[3][3];
		struct {
		t_Val	m00,	m10,	m20;
		t_Val	m01,	m11,	m21;
		t_Val	m02,	m12,	m22;
 		};
	};

    DYNTYPE_FUNC Mat3x3() {}
	DYNTYPE_FUNC t_Val & operator () (int x, int y) {return m[y][x];}
	DYNTYPE_FUNC const t_Val & operator () (int x, int y) const { return m[y][x]; }		

	DYNTYPE_FUNC rt::Vec<t_Val,9>& GetVec(){ return *((rt::Vec<t_Val,9>*)&m); }
	DYNTYPE_FUNC const rt::Vec<t_Val,9>& GetVec()const{ return *((const rt::Vec<t_Val,9>*)&m); }
	void TextDump() const{ _STD_OUT<<*this; }

	DYNTYPE_FUNC void Transpose()
	{
		rt::Swap(m01,m10);
		rt::Swap(m02,m20);
		rt::Swap(m21,m12);
	}

	//Column Major
	DYNTYPE_FUNC num::Vec3<t_Val> GetRow(unsigned int i) const {
		ASSERT(i>=0&&i<3);
		return num::Vec3<t_Val>(m[i][0],m[i][1],m[i][2]);
	}
	//Column Major
	DYNTYPE_FUNC void SetRow(unsigned int i,const num::Vec3<t_Val>& val) {
		ASSERT(i>=0&&i<3);
		m[i][0] = val[0];m[i][1] = val[1];m[i][2] = val[2];
	}
	//Column Major
	DYNTYPE_FUNC num::Vec3<t_Val> GetColumn(unsigned int i) const {
		ASSERT(i>=0&&i<3);
		return num::Vec3<t_Val>(m[0][i],m[1][i],m[2][i]);
	}
	//Column Major
	DYNTYPE_FUNC void SetColumn(unsigned int i,const num::Vec3<t_Val>& val) {
		ASSERT(i>=0&&i<3);
		m[0][i] = val[0];m[1][i] = val[1];m[2][i] = val[2];
	}


    DYNTYPE_FUNC num::Vec3<t_Val> Product(const num::Vec3<t_Val> &v) const
	{
        num::Vec3<t_Val> res;
        res[0] = m00 * v[0] + m01 * v[1] + m02 * v[2];
        res[1] = m10 * v[0] + m11 * v[1] + m12 * v[2];
        res[2] = m20 * v[0] + m21 * v[1] + m22 * v[2];
        return res;
	}
	
	DYNTYPE_FUNC void Product(const Mat3x3 &m2,const Mat3x3 &m1)
	{
		m00 = m1.m00*m2.m00 + m1.m10*m2.m01 + m1.m20*m2.m02;
		m01 = m1.m01*m2.m00 + m1.m11*m2.m01 + m1.m21*m2.m02;
		m02 = m1.m02*m2.m00 + m1.m12*m2.m01 + m1.m22*m2.m02;

		m10 = m1.m00*m2.m10 + m1.m10*m2.m11 + m1.m20*m2.m12;
		m11 = m1.m01*m2.m10 + m1.m11*m2.m11 + m1.m21*m2.m12;
		m12 = m1.m02*m2.m10 + m1.m12*m2.m11 + m1.m22*m2.m12;

		m20 = m1.m00*m2.m20 + m1.m10*m2.m21 + m1.m20*m2.m22;
		m21 = m1.m01*m2.m20 + m1.m11*m2.m21 + m1.m21*m2.m22;
		m22 = m1.m02*m2.m20 + m1.m12*m2.m21 + m1.m22*m2.m22;
	}

	DYNTYPE_FUNC void SetMat(const t_Val *p)
	{
		memcpy(m, p, sizeof(m));
	}

	DYNTYPE_FUNC void Zero()
	{
		memset(m, 0, sizeof(m));
	}

	//DYNTYPE_FUNC void Transform(Vec3<t_Val> &out,const Vec3<t_Val> &in) const
	//{
	//	out.x = in.x*m00+in.y*m01+in.z*m02;
	//	out.y = in.x*m10+in.y*m11+in.z*m12;
	//	out.z = in.x*m20+in.y*m21+in.z*m22;
	//}

	DYNTYPE_FUNC void SetIdentity()
	{
		m00 = m11 = m22 = 1.0f;
		m01 = m02 = m10 = m12 = m20 = m21 = 0.0f;
	};

	//DYNTYPE_FUNC void SetScale(t_Val x,t_Val y,t_Val z)
	//{
	//	m00 = x;
	//	m11 = y;
	//	m22 = z;
	//	m01 = m02 = m10 = m12 = m20 = m21 = 0.0f;	
	//};

	//template<typename T>
	//DYNTYPE_FUNC void SetRotate(t_Val angle,Vec3<T> axis)
	//{
	//	float s = sin(angle);
	//	float c = cos(angle);

	//	m00 = (1-c)*axis.x*axis.x + c;			m01 = (1-c)*axis.x*axis.y + s*axis.z;	m02 = (1-c)*axis.x*axis.z - s*axis.y;
	//	m10 = (1-c)*axis.x*axis.y-s*axis.z;		m11 = (1-c)*axis.y*axis.y + c;			m12 = (1-c)*axis.y*axis.z + s*axis.x;
	//	m20 = (1-c)*axis.x*axis.z + s*axis.y;	m21 = (1-c)*axis.y*axis.z - s*axis.x;	m22 = (1-c)*axis.z*axis.z + c;
	//}

	//DYNTYPE_FUNC void SetRotate(t_Val Theta,t_Val Phi)
	//{
	//	Mat3x3<t_Val>	m1,m2;
	//	m1.SetRotate(Theta,Vec3<t_Val>(0,1,0));
	//	m2.SetRotate(Phi,Vec3<t_Val>(0,0,1));

	//	Product(m1,m2);
	//}

    template<class t_Mat>
    Mat3x3(const t_Mat &m)
    {
        m00 = (t_Val)m(0, 0);
        m01 = (t_Val)m(0, 1);
        m02 = (t_Val)m(0, 2);
        m10 = (t_Val)m(1, 0);
        m11 = (t_Val)m(1, 1);
        m12 = (t_Val)m(1, 2);
        m20 = (t_Val)m(2, 0);
        m21 = (t_Val)m(2, 1);
        m22 = (t_Val)m(2, 2);
    }

	template<class t_Mat>
	DYNTYPE_FUNC const Mat3x3<t_Val> & operator = (const t_Mat &m)
	{
        m00 = (t_Val)m(0, 0);
        m01 = (t_Val)m(0, 1);
        m02 = (t_Val)m(0, 2);
        m10 = (t_Val)m(1, 0);
        m11 = (t_Val)m(1, 1);
        m12 = (t_Val)m(1, 2);
        m20 = (t_Val)m(2, 0);
        m21 = (t_Val)m(2, 1);
        m22 = (t_Val)m(2, 2);

		return *this;
	}

	template<class t_Mat>
	DYNTYPE_FUNC const Mat3x3<t_Val> & operator += (const t_Mat &m)
	{
        m00 += (t_Val)m(0, 0);
        m01 += (t_Val)m(0, 1);
        m02 += (t_Val)m(0, 2);
        m10 += (t_Val)m(1, 0);
        m11 += (t_Val)m(1, 1);
        m12 += (t_Val)m(1, 2);
        m20 += (t_Val)m(2, 0);
        m21 += (t_Val)m(2, 1);
        m22 += (t_Val)m(2, 2);

		return *this;
	}

	DYNTYPE_FUNC void Multiply(t_Val x)
	{
        m00 *= x; m01 *= x; m02 *= x;
        m10 *= x; m11 *= x; m12 *= x;
        m20 *= x; m21 *= x; m22 *= x;
	};

	//Add by Yanxiang, 2010,10
	DYNTYPE_FUNC t_Val Determinant()
	{
		t_Val ret = 0;
		ret += m00 * (m11 * m22 - m12 * m21);
		ret += -m01 * (m10 * m22 - m12 * m20);
		ret += m02 * (m10 * m21 - m11 * m20);
		return ret;
	}

	DYNTYPE_FUNC void Inverse(Mat3x3& result)
	{
		t_Val invdet = 1.0/Determinant();
		result(0,0) =  ((*this)(1,1)*(*this)(2,2)-(*this)(2,1)*(*this)(1,2))*invdet;
		result(1,0) = -((*this)(0,1)*(*this)(2,2)-(*this)(0,2)*(*this)(2,1))*invdet;
		result(2,0) =  ((*this)(0,1)*(*this)(1,2)-(*this)(0,2)*(*this)(1,1))*invdet;
		result(0,1) = -((*this)(1,0)*(*this)(2,2)-(*this)(1,2)*(*this)(2,0))*invdet;
		result(1,1) =  ((*this)(0,0)*(*this)(2,2)-(*this)(0,2)*(*this)(2,0))*invdet;
		result(2,1) = -((*this)(0,0)*(*this)(1,2)-(*this)(1,0)*(*this)(0,2))*invdet;
		result(0,2) =  ((*this)(1,0)*(*this)(2,1)-(*this)(2,0)*(*this)(1,1))*invdet;
		result(1,2) = -((*this)(0,0)*(*this)(2,1)-(*this)(2,0)*(*this)(0,1))*invdet;
		result(2,2) =  ((*this)(0,0)*(*this)(1,1)-(*this)(1,0)*(*this)(0,1))*invdet;
		
	}

};

template<typename t_Val>
class Mat4x4
{	typedef rt::TypeTraits<t_Val>	TE;
public :
	operator t_Val*(){ return m[0]; }
	operator const t_Val*()const{ return m[0]; }

	TYPETRAITS_DECL_TYPEID(rt::_typeid_codelib)
	TYPETRAITS_DECL_EXTENDTYPEID((rt::_typeid_codelib<<16)+114)
	TYPETRAITS_DECL_ACCUM_TYPE(Mat4x4<typename TE::t_Accum>)		
	TYPETRAITS_DECL_VALUE_TYPE(Mat4x4<typename TE::t_Val>)		
	TYPETRAITS_DECL_ELEMENT_TYPE(num::Vec4<t_Val>)		
	TYPETRAITS_DECL_SIGNED_TYPE(Mat4x4<typename TE::t_Signed>)	
	TYPETRAITS_DECL_IS_AGGREGATE(true)	
	TYPETRAITS_DECL_IS_NUMERIC(false)		
	TYPETRAITS_DECL_IS_ARRAY(true)		
	TYPETRAITS_DECL_LENGTH(4)

	num::Vec4<t_Val>& operator [](UINT i){ return *((num::Vec4<t_Val>*)m[i]); }
	const num::Vec4<t_Val>& operator [](UINT i)const{ return *((const num::Vec4<t_Val>*)m[i]); }

	template<class ostream>
	static void __TypeName(ostream & outstr)
	{	outstr<<_meta_::_text_Mat4x4_(); rt::TypeTraits<t_Val>::TypeName(outstr);
		outstr<<_T('>');
	}

    //Column Major
	union{
		t_Val m[4][4];
		struct {
		t_Val	m00,	m10,	m20,	m30;
		t_Val	m01,	m11,	m21,	m31;
		t_Val	m02,	m12,	m22,	m32;
		t_Val	m03,	m13,	m23,	m33;
		};
	};

	DYNTYPE_FUNC Mat4x4(){ }
	DYNTYPE_FUNC Mat4x4(const t_Val * p ) { SetMat(p); }
	DYNTYPE_FUNC t_Val & operator () (int x, int y) {return m[y][x];}
	DYNTYPE_FUNC const t_Val & operator () (int x, int y) const {return m[y][x]; }	

	DYNTYPE_FUNC rt::Vec<t_Val,16>& GetVec(){ return *((rt::Vec<t_Val,16>*)&m); }
	DYNTYPE_FUNC const rt::Vec<t_Val,16>& GetVec()const{ return *((const rt::Vec<t_Val,16>*)&m); }
	void TextDump() const{ _STD_OUT<<*this; }


    template<typename T>
    DYNTYPE_FUNC Mat4x4(const Mat3x3<T> & in)
    {
		m00 = (t_Val)in.m00;
		m01 = (t_Val)in.m01;
		m02 = (t_Val)in.m02;
				 
		m10 = (t_Val)in.m10;
		m11 = (t_Val)in.m11;
		m12 = (t_Val)in.m12;
				 
		m20 = (t_Val)in.m20;
		m21 = (t_Val)in.m21;
		m22 = (t_Val)in.m22;

        m03 = m13 = m23 = m30 = m31 = m32 = (t_Val)0.0;
        m33 = (t_Val)1.0;
    }

	template<typename T>
	DYNTYPE_FUNC const Mat4x4<t_Val> & operator = (const Mat3x3<T> & in)
	{
		m00 = (t_Val)in.m00;
		m01 = (t_Val)in.m01;
		m02 = (t_Val)in.m02;
				 
		m10 = (t_Val)in.m10;
		m11 = (t_Val)in.m11;
		m12 = (t_Val)in.m12;
				 
		m20 = (t_Val)in.m20;
		m21 = (t_Val)in.m21;
		m22 = (t_Val)in.m22;

        m03 = m13 = m23 = m30 = m31 = m32 = (t_Val)0.0;
        m33 = (t_Val)1.0;

		return *this;
	}

    template<class t_Mat>
    Mat4x4(const t_Mat &m)
    {
		m00 = (t_Val)m(0, 0);
		m01 = (t_Val)m(0, 1);
		m02 = (t_Val)m(0, 2);
		m03 = (t_Val)m(0, 3);
				 
		m10 = (t_Val)m(1, 0);
		m11 = (t_Val)m(1, 1);
		m12 = (t_Val)m(1, 2);
		m13 = (t_Val)m(1, 3);

		m20 = (t_Val)m(2, 0);
		m21 = (t_Val)m(2, 1);
		m22 = (t_Val)m(2, 2);
		m23 = (t_Val)m(2, 3);

		m30 = (t_Val)m(3, 0);
		m31 = (t_Val)m(3, 1);
		m32 = (t_Val)m(3, 2);
		m33 = (t_Val)m(3, 3);
    }
    
    template<class t_Mat>
	DYNTYPE_FUNC const Mat4x4<t_Val> & operator = (const t_Mat& m)
	{
		m00 = (t_Val)m(0, 0);
		m01 = (t_Val)m(0, 1);
		m02 = (t_Val)m(0, 2);
		m03 = (t_Val)m(0, 3);
				 
		m10 = (t_Val)m(1, 0);
		m11 = (t_Val)m(1, 1);
		m12 = (t_Val)m(1, 2);
		m13 = (t_Val)m(1, 3);

		m20 = (t_Val)m(2, 0);
		m21 = (t_Val)m(2, 1);
		m22 = (t_Val)m(2, 2);
		m23 = (t_Val)m(2, 3);

		m30 = (t_Val)m(3, 0);
		m31 = (t_Val)m(3, 1);
		m32 = (t_Val)m(3, 2);
		m33 = (t_Val)m(3, 3);

		return *this;
	}

    template<class t_Mat>
	DYNTYPE_FUNC const Mat4x4<t_Val> & operator += (const t_Mat& m)
	{
		m00 += (t_Val)m(0, 0);
		m01 += (t_Val)m(0, 1);
		m02 += (t_Val)m(0, 2);
		m03 += (t_Val)m(0, 3);
				 
		m10 += (t_Val)m(1, 0);
		m11 += (t_Val)m(1, 1);
		m12 += (t_Val)m(1, 2);
		m13 += (t_Val)m(1, 3);

		m20 += (t_Val)m(2, 0);
		m21 += (t_Val)m(2, 1);
		m22 += (t_Val)m(2, 2);
		m23 += (t_Val)m(2, 3);

		m30 += (t_Val)m(3, 0);
		m31 += (t_Val)m(3, 1);
		m32 += (t_Val)m(3, 2);
		m33 += (t_Val)m(3, 3);

		return *this;
	}

	// Transformation
	//
	DYNTYPE_FUNC void SetIdentity()
	{
		m00 = m11 = m22 = m33 = 1.0f;
		m01 = m02 = m03 = m10 = m12 = m13 = m20 = m21 = m23 = m30 = m31 = m32 = 0.0f;
	};

	DYNTYPE_FUNC void Zero()
	{
		memset(m, 0, sizeof(m));
	}

	//DYNTYPE_FUNC void Transform(const Vec4<t_Val> &in,Vec4<t_Val> &out) const
	//{
	//	out.x = in.x*m00+in.y*m01+in.z*m02+in.w*m03;
	//	out.y = in.x*m10+in.y*m11+in.z*m12+in.w*m13;
	//	out.z = in.x*m20+in.y*m21+in.z*m22+in.w*m23;
	//	out.w = in.x*m30+in.y*m31+in.z*m32+in.w*m33;
	//}

	//DYNTYPE_FUNC void Transform(const Vec3<t_Val> &in,Vec3<t_Val> &out) const
	//{
	//	out.x = in.x*m00+in.y*m01+in.z*m02+m03;
	//	out.y = in.x*m10+in.y*m11+in.z*m12+m13;
	//	out.z = in.x*m20+in.y*m21+in.z*m22+m23;
	//}

 //   DYNTYPE_FUNC void TransformHomoCoordDir(Vec3<t_Val> &out,const Vec3<t_Val> &in) const
 //   {
 //       Vec4<t_Val> in4 = Vec4<t_Val> (in.x,in.y,in.z,0.0f);
 //       Vec4<t_Val> out4;
 //       Transform(out4,in4);
 //       out.x = out4.x/out4.w;
 //       out.y = out4.y/out4.w;
 //       out.z = out4.z/out4.w;
 //   }
 //   DYNTYPE_FUNC void TransformHomoCoordPos(Vec3<t_Val> &out,const Vec3<t_Val> &in) const
 //   {
 //       Vec4<t_Val> in4 = Vec4<t_Val> (in.x,in.y,in.z,1.0f);
 //       Vec4<t_Val> out4;
 //       Transform(out4,in4);
 //       out.x = out4.x/out4.w;
 //       out.y = out4.y/out4.w;
 //       out.z = out4.z/out4.w;
 //   }
 //   DYNTYPE_FUNC void TransformTranspose(Vec3<t_Val> &out,const Vec3<t_Val> &in) const
 //   {
 //       out.x = in.x*m00+in.y*m10+in.z*m20;
 //       out.y = in.x*m01+in.y*m11+in.z*m21;
 //       out.z = in.x*m02+in.y*m12+in.z*m22;
 //   }    
	DYNTYPE_FUNC void SetMat(const t_Val *p)
	{
		memcpy(m, p, sizeof(m));
	}

    DYNTYPE_FUNC num::Vec4<t_Val> Product(const num::Vec4<t_Val> &v) const
	{
        num::Vec4<t_Val> res;
        res[0] = m00 * v[0] + m01 * v[1] + m02 * v[2] + m03 * v[3];
        res[1] = m10 * v[0] + m11 * v[1] + m12 * v[2] + m13 * v[3];
        res[2] = m20 * v[0] + m21 * v[1] + m22 * v[2] + m23 * v[3];
        res[3] = m30 * v[0] + m31 * v[1] + m32 * v[2] + m33 * v[3];
        return res;
	}

    DYNTYPE_FUNC num::Vec3<t_Val> Product(const num::Vec3<t_Val> &v) const
	{
        num::Vec3<t_Val> res;
        res[0] = m00 * v[0] + m01 * v[1] + m02 * v[2] + m03;
        res[1] = m10 * v[0] + m11 * v[1] + m12 * v[2] + m13;
        res[2] = m20 * v[0] + m21 * v[1] + m22 * v[2] + m23;
        return res;
	}

	DYNTYPE_FUNC void Product(const Mat4x4 &m2,const Mat4x4 &m1)
	{
		m00 = m1.m00*m2.m00 + m1.m10*m2.m01 + m1.m20*m2.m02 + m1.m30*m2.m03;
		m01 = m1.m01*m2.m00 + m1.m11*m2.m01 + m1.m21*m2.m02 + m1.m31*m2.m03;
		m02 = m1.m02*m2.m00 + m1.m12*m2.m01 + m1.m22*m2.m02 + m1.m32*m2.m03;
		m03 = m1.m03*m2.m00 + m1.m13*m2.m01 + m1.m23*m2.m02 + m1.m33*m2.m03;
														   
		m10 = m1.m00*m2.m10 + m1.m10*m2.m11 + m1.m20*m2.m12 + m1.m30*m2.m13;
		m11 = m1.m01*m2.m10 + m1.m11*m2.m11 + m1.m21*m2.m12 + m1.m31*m2.m13;
		m12 = m1.m02*m2.m10 + m1.m12*m2.m11 + m1.m22*m2.m12 + m1.m32*m2.m13;
		m13 = m1.m03*m2.m10 + m1.m13*m2.m11 + m1.m23*m2.m12 + m1.m33*m2.m13;
														   
		m20 = m1.m00*m2.m20 + m1.m10*m2.m21 + m1.m20*m2.m22 + m1.m30*m2.m23;
		m21 = m1.m01*m2.m20 + m1.m11*m2.m21 + m1.m21*m2.m22 + m1.m31*m2.m23;
		m22 = m1.m02*m2.m20 + m1.m12*m2.m21 + m1.m22*m2.m22 + m1.m32*m2.m23;
		m23 = m1.m03*m2.m20 + m1.m13*m2.m21 + m1.m23*m2.m22 + m1.m33*m2.m23;

		m30 = m1.m00*m2.m30 + m1.m10*m2.m31 + m1.m20*m2.m32 + m1.m30*m2.m33;
		m31 = m1.m01*m2.m30 + m1.m11*m2.m31 + m1.m21*m2.m32 + m1.m31*m2.m33;
		m32 = m1.m02*m2.m30 + m1.m12*m2.m31 + m1.m22*m2.m32 + m1.m32*m2.m33;
		m33 = m1.m03*m2.m30 + m1.m13*m2.m31 + m1.m23*m2.m32 + m1.m33*m2.m33;
	}

	DYNTYPE_FUNC void Transpose ()
	{
		rt::Swap(m01 ,m10);
		rt::Swap(m02 ,m20);
		rt::Swap(m03 ,m30);
		rt::Swap(m12 ,m21);
		rt::Swap(m13 ,m31);
		rt::Swap(m23 ,m32);
	}

	DYNTYPE_FUNC void Multiply(t_Val x)
	{
        m00 *= x; m01 *= x; m02 *= x; m03 *= x;
        m10 *= x; m11 *= x; m12 *= x; m13 *= x;
        m20 *= x; m21 *= x; m22 *= x; m23 *= x;
        m30 *= x; m31 *= x; m32 *= x; m33 *= x;
	};

	//algorithm from http://www.geometrictools.com//LibFoundation/Mathematics/Wm4Matrix4.inl
	DYNTYPE_FUNC void Inverse(Mat4x4<t_Val>& kInv) const
	{
		t_Val fA0 = m00*m11 - m01*m10;
		t_Val fA1 = m00*m12 - m02*m10;
		t_Val fA2 = m00*m13 - m03*m10;
		t_Val fA3 = m01*m12 - m02*m11;
		t_Val fA4 = m01*m13 - m03*m11;
		t_Val fA5 = m02*m13 - m03*m12;
		t_Val fB0 = m20*m31 - m21*m30;
		t_Val fB1 = m20*m32 - m22*m30;
		t_Val fB2 = m20*m33 - m23*m30;
		t_Val fB3 = m21*m32 - m22*m31;
		t_Val fB4 = m21*m33 - m23*m31;
		t_Val fB5 = m22*m33 - m23*m32;

		t_Val fDet = fA0*fB5-fA1*fB4+fA2*fB3+fA3*fB2-fA4*fB1+fA5*fB0;
		if (abs(fDet) <= 0.00001)
		{
			kInv.Zero();
		}

		kInv.m00 =
			+ m11*fB5 - m12*fB4 + m13*fB3;
		kInv.m10 =
			- m10*fB5 + m12*fB2 - m13*fB1;
		kInv.m20 =
			+ m10*fB4 - m11*fB2 + m13*fB0;
		kInv.m30 =
			- m10*fB3 + m11*fB1 - m12*fB0;
		kInv.m01 =
			- m01*fB5 + m02*fB4 - m03*fB3;
		kInv.m11 =
			+ m00*fB5 - m02*fB2 + m03*fB1;
		kInv.m21 =
			- m00*fB4 + m01*fB2 - m03*fB0;
		kInv.m31 =
			+ m00*fB3 - m01*fB1 + m02*fB0;
		kInv.m02 =
			+ m31*fA5 - m32*fA4 + m33*fA3;
		kInv.m12 =
			- m30*fA5 + m32*fA2 - m33*fA1;
		kInv.m22 =
			+ m30*fA4 - m31*fA2 + m33*fA0;
		kInv.m32 =
			- m30*fA3 + m31*fA1 - m32*fA0;
		kInv.m03 =
			- m21*fA5 + m22*fA4 - m23*fA3;
		kInv.m13 =
			+ m20*fA5 - m22*fA2 + m23*fA1;
		kInv.m23 =
			- m20*fA4 + m21*fA2 - m23*fA0;
		kInv.m33 =
			+ m20*fA3 - m21*fA1 + m22*fA0;

		t_Val fInvDet = ((t_Val)1.0)/fDet;
		kInv.m00 *= fInvDet;
		kInv.m01 *= fInvDet;
		kInv.m02 *= fInvDet;
		kInv.m03 *= fInvDet;
		kInv.m10 *= fInvDet;
		kInv.m11 *= fInvDet;
		kInv.m12 *= fInvDet;
		kInv.m13 *= fInvDet;
		kInv.m20 *= fInvDet;
		kInv.m21 *= fInvDet;
		kInv.m22 *= fInvDet;
		kInv.m23 *= fInvDet;
		kInv.m30 *= fInvDet;
		kInv.m31 *= fInvDet;
		kInv.m32 *= fInvDet;
		kInv.m33 *= fInvDet;
	};


	//DYNTYPE_FUNC void SetRotation(t_Val Theta,t_Val Phi)
	//{
	//	float sp = sin(Phi);
	//	float cp = cos(Phi);
	//	float st = sin(Theta);
	//	float ct = cos(Theta);

	//	float sp2 = sp*sp;
	//	float cp2 = 1-sp2;

	//	m00 = ct;
	//	m01 = sp;

	//	m00 = cp*ct;
	//	m01 = sp*(ct+sp2+(1-ct)*cp2-ct*sp2);
	//	m02 = -cp*st;

	//	m10 = -ct*sp;
	//	m11 = cp*(ct+sp2+(1-ct)*cp2-ct*sp2);
	//	m12 = sp*st;

	//	m20 = sp2*st+cp2*(sp-ct*sp+st);
	//	m21 = sp2*cp*(1-ct);
	//	m22 = ct;

	//	m03 = m13 = m23 = m30 = m31 = m32 = 0;
	//	m33 = 1;
	//}

	//DYNTYPE_FUNC void SetRotate(t_Val Theta,t_Val Phi)
	//{
	//	Mat3x3<t_Val>	m1,m2,q;
	//	m1.SetRotate(Theta,Vec3<t_Val>(0,1,0));

	//	m2.SetRotate(Phi,Vec3<t_Val>(0,0,1));

	//	q.Product(m1,m2);

	//	m00 = q.m00;
	//	m01 = q.m01;
	//	m02 = q.m02;
	//			
	//	m10 = q.m10;
	//	m11 = q.m11;
	//	m12 = q.m12;
	//			
	//	m20 = q.m20;
	//	m21 = q.m21;
	//	m22 = q.m22;

	//	m03 = m13 = m23 = m30 = m31 = m32 = 0;
	//	m33 = 1;
	//}

	//DYNTYPE_FUNC void SetColumnVec3(int i,const Vec3<t_Val> &v)
	//{
	//	ASSERT(i>=0&&i<4);
	//	m[i][0] = v[0];
	//	m[i][1] = v[1];
	//	m[i][2] = v[2];
	//}
	//DYNTYPE_FUNC void SetRowVec3(int i,const Vec3<t_Val> &v) const
	//{
	//	ASSERT(i>=0&&i<4);
	//	m[0][i] = v[0];
	//	m[1][i] = v[1];
	//	m[2][i] = v[2];
	//}

 //   DYNTYPE_FUNC Vec3<t_Val> GetColumnVec3(int i) const
 //   {
 //       ASSERT(i>=0&&i<4);
 //       return Vec3<t_Val>(m[i][0],m[i][1],m[i][2]);
 //   }
 //   DYNTYPE_FUNC Vec3<t_Val> GetRowVec3(int i) const
 //   {
 //       ASSERT(i>=0&&i<4);
 //       return Vec3<t_Val> (m[0][i],m[1][i],m[2][i]);
 //   }
 //   DYNTYPE_FUNC Vec4<t_Val> GetColumn(int i) const
 //   {
 //       ASSERT(i>=0&&i<4);
 //       return Vec4<t_Val>(m[i][0],m[i][1],m[i][2],m[i][3]);
 //   }
 //   DYNTYPE_FUNC Vec4<t_Val> GetRow(int i) const
 //   {
 //       ASSERT(i>=0&&i<4);
 //       return Vec4<t_Val>(m[0][i],m[1][i],m[2][i],m[3][i]);
 //   }
};

typedef Mat3x3<float> Mat3x3f;
typedef Mat4x4<float> Mat4x4f;

#pragma pack()

template<class t_Ostream, typename t_Ele>
t_Ostream& operator<<(t_Ostream& Ostream, const num::Mat3x3<t_Ele> & vec)
{	rt::_meta_::TextDump(Ostream,*((num::Vec3<num::Vec3<t_Ele>>*)&vec));
	Ostream<<_T('\n');
	return Ostream;
}
template<class t_Ostream, typename t_Ele>
t_Ostream& operator<<(t_Ostream& Ostream, const num::Mat4x4<t_Ele> & vec)
{	return rt::_meta_::TextDump(Ostream,*((num::Vec4<num::Vec4<t_Ele>>*)&vec));
	Ostream<<_T('\n');
	return Ostream;
}


};



