#pragma once

//////////////////////////////////////////////////////////////////////
// Visual Computing Foundation Classes (VCFC)
//					Jiaping Wang  2006.2
//					e_boris2002@hotmail.com
//
// (C) Copyright Jiaping Wang 2004.
//
// Permission to copy, use, modify, sell and distribute this software
// is granted provided this copyright notice appears in all copies.
// This software is provided "as is" without express or implied
// warranty, and with no claim as to its suitability for any purpose.
//
// Absolutly no garanty!!
//////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////
//  Brief Description
//
//  quaternion.h
//
//  quaternion operations
//
//////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////
// Revise History
//
// Inital version	2006.11.10		Shuang Zhao
//
//////////////////////////////////////////////////////////////////////

#include <math.h>
#include "small_vec.h"
#include "small_mat.h"
#include "irreg_func.h"

namespace num
{

template <typename t_Val, typename t_Comp = double>
class Quaternion : public num::Vec4<t_Val>
{
public:
    DEFAULT_STUFFS(Quaternion)

    template <typename T>
    void Multiply(const num::Quaternion<T>& q)
    {
        t_Comp w0, x0, y0, z0;
        w0 = (t_Comp)(w * q.w - (x * q.x + y * q.y + z * q.z)); 
        x0 = (t_Comp)(w * q.x +  q.w * x + y * q.z - z * q.y);  
        y0 = (t_Comp)(w * q.y -  q.z * x + y * q.w + z * q.x); 
        z0 = (t_Comp)(w * q.z +  q.y * x - y * q.x + z * q.w);

        w = (t_Val)w0;
        x = (t_Val)x0;
        y = (t_Val)y0;
        z = (t_Val)z0;
    }

    // The matrix m is assumed to be already normalized.
    template <class t_Mat>
    void ImportRotationMatrix(const t_Mat &m)
    {
        num::Mat3x3<t_Comp> mat(m);

#ifdef _DEBUG
        num::Vec3<t_Comp> v;
        t_Comp L;
        
        v.x = mat(0, 0); v.y = mat(0, 1); v.z = mat(0, 2);
        L = (t_Comp)v.L2Norm_Sqr();
        ASSERT( (t_Val)fabs( L - 1.0 ) < 1e-4 );

        v.x = mat(1, 0); v.y = mat(1, 1); v.z = mat(1, 2);
        L = (t_Comp)sqrt(v.L2Norm_Sqr());
        ASSERT( (t_Val)fabs( L - 1.0 ) < 1e-4 );

        v.x = mat(2, 0); v.y = mat(2, 1); v.z = mat(2, 2);
        L = (t_Comp)sqrt(v.L2Norm_Sqr());
        ASSERT( (t_Val)fabs( L - 1.0 ) < 1e-4 );
#endif

        // Calculation
		t_Comp s;
		t_Comp tr = mat(0, 0) + mat(1, 1) + mat(2, 2);

		// check the diagonal
        if ( tr > rt::TypeTraits<t_Comp>::Epsilon() )
		{
			s = (t_Comp)sqrt(tr + 1);
			w = (t_Val)(0.5 * s);
			s = (t_Comp)(0.5 / s);
			x = (t_Val)((mat(2, 1) - mat(1, 2)) * s);
			y = (t_Val)((mat(0, 2) - mat(2, 0)) * s);
			z = (t_Val)((mat(1, 0) - mat(0, 1)) * s);
		}
		else
		{
			if ( mat(1, 1) - mat(0, 0) > rt::TypeTraits<t_Comp>::Epsilon() &&
                 mat(1, 1) - mat(2, 2) > -rt::TypeTraits<t_Comp>::Epsilon() )
			{
				s = (t_Comp)sqrt((mat(1, 1) - (mat(2, 2) + mat(0, 0))) + 1);

				y = (t_Val)(0.5 * s);
				if ( fabs(s) > rt::TypeTraits<t_Comp>::Epsilon() )
				{
					s = (t_Comp)(0.5 / s);
				}

				w = (t_Val)((mat(0, 2) - mat(2, 0)) * s);
				z = (t_Val)((mat(1, 2) + mat(2, 1)) * s);
				x = (t_Val)((mat(1, 0) + mat(0, 1)) * s);
			}
			else
			{
				if ( mat(0, 0) - mat(1, 1) > -rt::TypeTraits<t_Val>::Epsilon() && 
                     mat(2, 2) - mat(0, 0) > rt::TypeTraits<t_Val>::Epsilon() ||
                     mat(2, 2) - mat(1, 1) > rt::TypeTraits<t_Val>::Epsilon() )
				{
					s = (t_Comp)sqrt((mat(2, 2) - (mat(0, 0) + mat(1, 1))) + 1);

					z = (t_Val)(0.5 * s);

					if ( fabs(s) > rt::TypeTraits<t_Comp>::Epsilon() )
					{
						s = (t_Comp)(0.5 / s);
					}

					w = (t_Val)((mat(1, 0) - mat(0, 1)) * s);
					x = (t_Val)((mat(2, 0) + mat(0, 2)) * s);
					y = (t_Val)((mat(2, 1) + mat(1, 2)) * s);
				}
				else
				{
					s = (t_Comp)sqrt((mat(0, 0) - (mat(1, 1) + mat(2, 2))) + 1);

					x = (t_Val)(0.5 * s);

					if ( fabs(s) > rt::TypeTraits<t_Comp>::Epsilon() )
					{
						s = (t_Comp)(0.5 / s);
					}

					w = (t_Val)((mat(2, 1) - mat(1, 2)) * s);
					y = (t_Val)((mat(0, 1) + mat(1, 0)) * s);
					z = (t_Val)((mat(0, 2) + mat(2, 0)) * s);
				}
			}
        }

        Normalize();
    }

    template <class t_Mat>
    void ImportRotationMatrix(const t_Mat &m, num::Vec3<t_Val> &scale)
    {
        num::Mat3x3<t_Comp> mat(m);

        // Normalization
        num::Vec3<t_Comp> v;
        t_Comp L;
        
        v.x = mat(0, 0); v.y = mat(0, 1); v.z = mat(0, 2);
        L = (t_Comp)sqrt(v.L2Norm_Sqr());
        if ( L > rt::TypeTraits<t_Comp>::Epsilon() )
            scale.x = (t_Val)L;
        else
            scale.x = (t_Val)1.0;
        v.Normalize();
        mat(0, 0) = (t_Comp)v.x; mat(0, 1) = v.y; mat(0, 2) = v.z;

        v.x = mat(1, 0); v.y = mat(1, 1); v.z = mat(1, 2);
        L = (t_Comp)sqrt(v.L2Norm_Sqr());
        if ( L > rt::TypeTraits<t_Comp>::Epsilon() )
            scale.y = (t_Val)L;
        else
            scale.y = (t_Val)1.0;
        v.Normalize();
        mat(1, 0) = v.x; mat(1, 1) = v.y; mat(1, 2) = v.z;

        v.x = mat(2, 0); v.y = mat(2, 1); v.z = mat(2, 2);
        L = (t_Comp)sqrt(v.L2Norm_Sqr());
        if ( L > rt::TypeTraits<t_Comp>::Epsilon() )
            scale.z = (t_Val)L;
        else
            scale.z = (t_Val)1.0;
        v.Normalize();
        mat(2, 0) = v.x; mat(2, 1) = v.y; mat(2, 2) = v.z;

        ImportRotationMatrix(mat);
    }

    template <class t_Mat>
    void ExportRotationMatrix(t_Mat& mat) const
    {
		t_Comp x2 = (x + x);
		t_Comp y2 = (y + y);
        t_Comp z2 = (z + z);

        t_Comp xx2 = x * x2, xy2 = x * y2, xz2 = x * z2;
        t_Comp yy2 = y * y2, yz2 = y * z2, zz2 = z * z2;
        t_Comp wx2 = w * x2, wy2 = w * y2, wz2 = w * z2;

        mat(0, 0) = (t_Val)(1 - yy2 - zz2);
        mat(1, 0) = (t_Val)(xy2 + wz2);
        mat(2, 0) = (t_Val)(xz2 - wy2);
        mat(0, 1) = (t_Val)(xy2 - wz2);
        mat(1, 1) = (t_Val)(1 - xx2 - zz2); 
        mat(2, 1) = (t_Val)(yz2 + wx2);
        mat(0, 2) = (t_Val)(xz2 + wy2);
        mat(1, 2) = (t_Val)(yz2 - wx2);
        mat(2, 2) = (t_Val)(1 - xx2 - yy2);
    }

    template <class t_Mat>
    void ExportRotationMatrix(t_Mat& mat, const num::Vec3<t_Val> &scale) const
    {
		t_Comp x2 = (x + x);
		t_Comp y2 = (y + y);
        t_Comp z2 = (z + z);

        t_Comp xx2 = x * x2, xy2 = x * y2, xz2 = x * z2;
        t_Comp yy2 = y * y2, yz2 = y * z2, zz2 = z * z2;
        t_Comp wx2 = w * x2, wy2 = w * y2, wz2 = w * z2;

        mat(0, 0) = (t_Val)(1 - yy2 - zz2) * scale.x;
        mat(1, 0) = (t_Val)(xy2 + wz2) * scale.y;
        mat(2, 0) = (t_Val)(xz2 - wy2) * scale.z;
        mat(0, 1) = (t_Val)(xy2 - wz2) * scale.x;
        mat(1, 1) = (t_Val)(1 - xx2 - zz2) * scale.y; 
        mat(2, 1) = (t_Val)(yz2 + wx2) * scale.z;
        mat(0, 2) = (t_Val)(xz2 + wy2) * scale.x;
        mat(1, 2) = (t_Val)(yz2 - wx2) * scale.y;
        mat(2, 2) = (t_Val)(1 - xx2 - yy2) * scale.z;
    }

	void Interpolate(const Quaternion<t_Val> &lhs, const Quaternion<t_Val> &rhs, const t_Val &t)
	{
        /*
		t_Comp scale0, scale1;
        Quaternion<t_Comp> q2;

		// DOT the quats to get the cosine of the angle between them
		const t_Comp cosom = (t_Comp)lhs.Dot(rhs);

		// Two special cases:
		// Quats are exactly opposite, within DELTA?
        if ( cosom > rt::TypeTraits<t_Comp>::Epsilon() - t_Comp(1.0) )
		{
			// make sure they are different enough to avoid a divide by 0
			if ( cosom < t_Comp(1.0) - rt::TypeTraits<t_Comp>::Epsilon() )
			{
				// SLERP away
				t_Comp omega = (t_Comp)(acos(cosom));
				t_Comp isinom = (t_Comp)(1.0 / sin(omega));

				scale0 = (t_Comp)(sin((1.0 - t) * omega) * isinom);
				scale1 = (t_Comp)(sin(t * omega) * isinom);
			}
			else
			{
				// LERP is good enough at this distance
				scale0 = (t_Comp)(1.0 - t);
				scale1 = (t_Comp)t;
			}

			q2 = rhs;
            q2 *= scale1;
		}
		else
		{
			// SLERP towards a perpendicular quat
			// Set slerp parameters
			scale0 = (t_Comp)sin((1.0 - t) * acos(0.0));
			scale1 = (t_Comp)sin(t * acos(0.0));

			q2.x = -rhs.y * scale1;
			q2.y = +rhs.x * scale1;
			q2.z = -rhs.w * scale1;
			q2.w = +rhs.z * scale1;
		}

		// Compute the result
		x = (t_Val)(q2.x + lhs.x*scale0);
		y = (t_Val)(q2.y + lhs.y*scale0);
		z = (t_Val)(q2.z + lhs.z*scale0);
		w = (t_Val)(q2.w + lhs.w*scale0);
        */

        Quaternion<t_Val> aa, bb;

        t_Comp c = lhs.Dot(rhs);
        if ( c > -rt::TypeTraits<t_Comp>::Epsilon() )
        {
		    aa = lhs;
		    bb = rhs;
	    }
        else
        {
		    aa = lhs;
		    bb = rhs;
            bb *= (t_Val)(-1.0);
		    c = -c;
	    }


	    float theta = (float)acos( c );
	    float sinom = (float)sin( theta );

	    if( sinom>EPSILON )
        {
            aa *= (t_Val)sin((t_Comp)(1-t)*theta);
            bb *= (t_Val)((t_Comp)sin(t*theta));
	        *this = aa;
            *this += bb;
            *this /= sinom;
            Normalize();
	    }
        else
        {
            aa *= (t_Val)((t_Comp)1.0 - t);
            bb *= (t_Val)t;
            *this = aa;
            *this += bb;
            Normalize();
        }
	}

    DYNTYPE_FUNC void Identity()                     { w = (t_Val)1.0; x = y = z = (t_Val)0.0; }
    DYNTYPE_FUNC const t_Val& Quat_Scal() const      { return w; }
    DYNTYPE_FUNC t_Val& Quat_Scal()                  { return w; }
    DYNTYPE_FUNC const Vec3<t_Val>& Quat_Vec() const { return v3(); }
    DYNTYPE_FUNC Vec3<t_Val>& Quat_Vec()             { return v3(); }
};

typedef Quaternion<float> Quaternionf;
typedef Quaternion<double> Quaterniond;


template <typename t_Key, typename t_Value, typename t_Comp = t_Key>
class QuaternionInterpolator : public _meta_::Interpolator_Base<0, t_Key, Quaternion<t_Value>, t_Comp>
{
public:
    void Query(const t_Key &x, Quaternion<t_Value> &y) const
    {
        ASSERT( map.size() > 0 );

        MAP::const_iterator it, it2;

        it = map.begin();
        if ( less(x, it->first) )
        {
            y = (it->second)[0];
            return;
        }

        it = map.end();
        --it;
        if ( less(it->first, x) )
        {
            y = (it->second)[0];
            return;
        }

        it = map.lower_bound(x);
        if ( equal(x, it->first) )
            y = (it->second)[0];
        else
        {
            it2 = it;
            --it2;

            t_Key x0 = it2->first;
            t_Key x1 = it->first;

            Quaternion<t_Comp> y0, y1, res;
            y0 = (it2->second)[0];
            y1 = (it->second)[0];

            t_Comp A = (t_Comp)( (x - x0) / (x1 - x0) );

            res.Interpolate(y0, y1, A);
            y = res;
        }
    }
};

typedef QuaternionInterpolator<float, float> QuaternionInterpolatorf;
typedef QuaternionInterpolator<double, double> QuaternionInterpolatord;

} // namespace num

