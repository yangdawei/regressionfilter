#pragma once
//////////////////////////////////////////////////////////////////////
// Visual Computing Foundation Classes (VCFC)
//					Jiaping Wang  2006.2
//					e_boris2002@hotmail.com
//
// (C) Copyright Jiaping Wang 2004.
//
// Permission to copy, use, modify, sell and distribute this software
// is granted provided this copyright notice appears in all copies.
// This software is provided "as is" without express or implied
// warranty, and with no claim as to its suitability for any purpose.
//
// Absolutly no garanty!!
//////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////
//  Brief Description
//
//  irreg_func.h
//
//  irregular sampled 1D function
//
//////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////
// Revise History
//
// Inital version	2006.11.10		Shuang Zhao
//
//////////////////////////////////////////////////////////////////////

#include <functional>
#include <vector>
#include <map>
#include "small_vec.h"

namespace num
{

namespace _meta_
{

// extra : Extra number of elements used to calculate the interpolation.
// For instance, in linear interpolation, there is no such need, so extra = 0.
// In cubic interpolation, for we have to calculate y''_i as well as y_i for each x_i,
// namely one extra element is needed. Thus extra = 1 when we do cubic interpolation.
template <UINT extra, typename t_Key, typename t_Value, typename t_Comp = double>
class Interpolator_Base
{
protected:
    template <typename T>
    struct Less : public std::binary_function<T, T, bool>
    {
        bool operator()(const T& _Left, const T& _Right) const
        {
            return ( _Right - _Left > rt::TypeTraits<T>::Epsilon() );
        }
    };
    
    Less<t_Key> less;
    Less<t_Comp> less_comp;

    bool equal(const t_Comp& _Left, const t_Comp& _Right) const
    {
        return ( !less_comp(_Left, _Right) && !less_comp(_Right, _Left) );
    }


    typedef std::map<t_Key, rt::Vec<t_Value, extra + 1>, Less<t_Key> > MAP;
    MAP map;
    
public:
    void Clear()
    {
        map.clear();
    }

    void Set(const t_Key &x, const t_Value &y)
    {
        MAP::iterator it = map.find(x);
        if ( it == map.end() )
        {
            rt::Vec<t_Value, extra + 1> buf;
            buf[0] = y;

            map.insert(MAP::value_type(x, buf));
        }
        else
            (it->second)[0] = y;
    }

    void Remove(const t_Key &x)
    {
        if ( map.find(x) != map.end() )
            map.erase(x);
    }
};

} // namespace num::_meta_


// Class for doing linear interpolation
template <typename t_Key, typename t_Value, typename t_Comp = double>
class LinearInterpolator : public _meta_::Interpolator_Base<0, t_Key, t_Value, t_Comp>
{
public:
    LinearInterpolator()
    {
    }

    void Query(const t_Key &x, t_Value &y) const
    {
        ASSERT( map.size() > 0 );

        MAP::const_iterator it, it2;

        it = map.begin();
        if ( less(x, it->first) )
        {
            y = (it->second)[0];
            return;
        }

        it = map.end();
        --it;
        if ( less(it->first, x) )
        {
            y = (it->second)[0];
            return;
        }

        it = map.lower_bound(x);
        if ( equal(x, it->first) )
            y = (it->second)[0];
        else
        {
            it2 = it;
            --it2;

            t_Key x0 = it2->first;
            t_Key x1 = it->first;

            t_Comp A = (t_Comp)( (x1 - x) / (x1 - x0) );
            t_Comp B = (t_Comp)( 1.0 - A );

            y = (t_Value)( A * (it2->second)[0] + B * (it->second)[0] );
        }
    }
};


// Class for doing cubic interpolation
template <typename t_Key, typename t_Value, typename t_Comp = double>
class CubicInterpolator : public _meta_::Interpolator_Base<1, t_Key, t_Value, t_Comp>
{
protected:
    void _YY_again()
    {
        int size = (int)map.size();
        int i;

        if ( size <= 2 )
        {
            MAP::iterator it;

            for ( it = map.begin(); it != map.end(); ++it )
                (it->second)[1] = (t_Value)(0.0);
        }
        else
        {
            std::vector<num::Vec2<t_Comp> > f;
            std::vector<num::Vec3<t_Comp> > A;
            std::vector<t_Comp> b;
            MAP::iterator it;

            for ( it = map.begin(); it != map.end(); ++it )
                f.push_back(num::Vec2<t_Comp>(it->first, (it->second)[0]));

            for ( i = 0; i < size - 2; ++i )
            {
                num::Vec3<t_Comp> now;
                now.x = (t_Comp)( (f[i + 1].x - f[i].x) / 6.0 );
                now.y = (t_Comp)( (f[i + 2].x - f[i].x) / 3.0 );
                now.z = (t_Comp)( (f[i + 2].x - f[i + 1].x) / 6.0 );
                A.push_back(now);

                t_Comp v1 = (t_Comp)( (f[i + 2].y - f[i + 1].y) / (f[i + 2].x - f[i + 1].x) );
                t_Comp v2 = (t_Comp)( (f[i + 1].y - f[i].y) / (f[i + 1].x - f[i].x) );
                b.push_back(v1 - v2);
            }

            A[0].x = (t_Comp)0.0;
            for ( i = 0; i < size - 3; ++i )
            {
                ASSERT( !equal(A[i].y, (t_Comp)0.0) );
                t_Comp rat = (t_Comp)(A[i + 1].x / A[i].y);

                A[i + 1].x = (t_Comp)0.0;
                A[i + 1].y -= rat * (t_Comp)A[i].z;
                b[i + 1] -= rat * b[i];
            }

            rt::Buffer<t_Comp> buf_yy;
            buf_yy.SetSize(size);
            buf_yy[0] = buf_yy[size - 1] = (t_Comp)0.0;

            for ( i = size - 3; i >= 0; --i )
            {
                ASSERT( !equal(A[i].y, (t_Comp)0.0) );
                t_Comp v = (t_Comp)( (b[i] - A[i].z * buf_yy[i + 2]) / A[i].y );
                buf_yy[i + 1] = v;
            }

            i = 0;
            for ( it = map.begin(); it != map.end(); ++it )
                (it->second)[1] = (t_Value)buf_yy[i++];
        }
    }

public:
    CubicInterpolator()
    {
    }

    void Set(const t_Key &x, const t_Value &y)
    {
        __super::Set(x, y);
        _YY_again();
    }

    void Remove(const t_Key &x)
    {
        __super::Remove(x);
        _YY_again();
    }

    void Query(const t_Key &x, t_Value &y) const
    {
        ASSERT( map.size() > 0 );

        MAP::const_iterator it, it2;

        it = map.begin();
        if ( less(x, it->first) )
        {
            y = (it->second)[0];
            return;
        }

        it = map.end();
        --it;
        if ( less(it->first, x) )
        {
            y = (it->second)[0];
            return;
        }

        it = map.lower_bound(x);
        if ( equal(x, it->first) )
            y = (it->second)[0];
        else
        {
            it2 = it;
            --it2;

            t_Key x0 = it2->first;
            t_Key x1 = it->first;

            t_Comp A = (t_Comp)( (x1 - x) / (x1 - x0) );
            t_Comp B = (t_Comp)( 1.0 - A );
            t_Comp C = (t_Comp)( (A * A * A - A) * (x1 - x0) * (x1 - x0) / 6.0 );
            t_Comp D = (t_Comp)( (B * B * B - B) * (x1 - x0) * (x1 - x0) / 6.0 );

            y = (t_Value)( A * (it2->second)[0] + B * (it->second)[0] + C * (it2->second)[1] + D * (it->second)[1] );
        }
    }
};

typedef LinearInterpolator<float, float> LinearInterpolatorf;
typedef LinearInterpolator<double, double> LinearInterpolatord;
typedef CubicInterpolator<float, float> CubicInterpolatorf;
typedef CubicInterpolator<double, double> CubicInterpolatord;

} // namespace num