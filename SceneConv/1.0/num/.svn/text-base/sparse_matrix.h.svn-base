#pragma once

//////////////////////////////////////////////////////////////////////
// Visual Computing Foundation Classes (VCFC)
//					Jiaping Wang  2006.2
//					e_boris2002@hotmail.com
//
// (C) Copyright Jiaping Wang 2004.
//
// Permission to copy, use, modify, sell and distribute this software
// is granted provided this copyright notice appears in all copies.
// This software is provided "as is" without express or implied
// warranty, and with no claim as to its suitability for any purpose.
//
// Absolutly no warranty!!
//////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////
//  Brief Description
//
//  sparse_matrix.h
//
//  compressed repersentation for sparse matrix
//
//  USE_BOOST_HASH_FOR_SPARSE_MATRIX
//  	Use boost::unordered_map instead of stdext::hash_map,
//  	boost::unordered_map is faster than std::map, 
//  	stdext::hash_map and std::tr1::unordered_map in MSVC.
//
//////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////
// ChangeLog
//
// Inital version	2006.07.24		Jiaping
// Add IsZero		2006.09.17		Jiaping
//
// Ver 1            2008.11.07		Yiming
// 		* Provide a macro to use boost::unordered_map instead of 
// 		  the ugly implementation of stdext::hash_map in MSVC;
// 		* Fix a typo in the copyright section;
// 		* Change "Revise History" to "ChangeLog".
//
//////////////////////////////////////////////////////////////////////

#include "..\rt\compact_vector.h"
#include "..\rt\image_type.h"
#include "..\num\small_vec.h"
#include "..\w32\file_comp.h"

#include "math.h"

#if defined(USE_BOOST_HASH_FOR_SPARSE_MATRIX)
	#include <boost/unordered_map.hpp>
#elif defined(USE_GOOGLE_HASH_FOR_SPARSE_MATRIX)
	#pragma message("Using google hash map")
	#include <google/dense_hash_map>
#else
	#include <hash_map>
#endif

namespace num
{

namespace _meta_
{
	template<typename T>
	DYNTYPE_FUNC BOOL IsZero(const T& x)
	{	__if_exists(T::IsZero){ return x.IsZero(); }
		typedef ::rt::NumericTraits<T>	_nt;
		__static_if(_nt::IsBuiltInNumeric)
		{	__static_if(!_nt::IsFloat){ return x==0; }
			__static_if(_nt::IsFloat)
			{ return x<(::rt::TypeTraits<T>::Epsilon()) && x>(-::rt::TypeTraits<T>::Epsilon()); }
			ASSERT(0); //avoid both static if fails
		}
		ASSERT(0); //I don't know how to determine a zero
	}

	__forceinline LPCTSTR _text_null(){ return _T("{ -NULL- }"); }
	__forceinline LPCTSTR _text_left_empty(){ return _T("{ - "); }
	__forceinline LPCTSTR _text_right_empty(){ return _T(" empty matrix - }"); }
	__forceinline LPCTSTR _text_left(){ return _T("{ // "); }
	__forceinline LPCTSTR _text_sparse(){ return _T(" sparse matrix"); }
	__forceinline LPCTSTR _text_transpose(){ return _T(" (transposed)"); }
	__forceinline LPCTSTR _text_nz(){ return _T(" nonzero entries\n"); }
	__forceinline LPCTSTR _text_seperator(){ return _T("  ... ...\n"); }
	__forceinline LPCTSTR _text_at(){ return _T("    @"); }
	__forceinline LPCTSTR _text_equ(){ return _T(" = "); }

}	// namespace _meta_
}	// namespace num

namespace num
{
namespace _meta_
{

template<typename t_Val,class t_SparseMatrix>
class _ValueBinded
{	typedef t_SparseMatrix t_Associated;
	typedef typename t_SparseMatrix::t_LinearizedType t_LinearizedPos;

	t_LinearizedPos pos;
	t_Associated&	matrix_associated;
public:
	TYPETRAITS_FOLLOW_VALUE_TYPE(t_Val)
	TYPETRAITS_FOLLOW_ACCUM_TYPE(t_Val)
	TYPETRAITS_FOLLOW_SIGNED_TYPE(t_Val)
	TYPETRAITS_FOLLOW_ELEMENT_TYPE(t_Val)

	TYPETRAITS_DECL_IS_AGGREGATE(true)
	TYPETRAITS_DECL_IS_NUMERIC(::rt::NumericTraits<t_Val>::IsBuiltInNumeric)
public:
	DYNTYPE_FUNC _ValueBinded(t_LinearizedPos p,t_Associated& i):pos(p),matrix_associated(i){}
	DYNTYPE_FUNC void Random(){ ::rt::_meta_::_Random(value()); }

public:
	template<typename T>
	DYNTYPE_FUNC const t_Val& operator =(const T& x)
	{	
#pragma warning(disable:4244)
		t_Associated::t_EntryMap::iterator p = matrix_associated.m_EntryMap.find(pos);
		if(p!=matrix_associated.m_EntryMap.end())
		{	return matrix_associated.GetVec()[p->second] = x; }
		else
		{	if(!::num::_meta_::IsZero(x))
			{	matrix_associated.m_EntryMap.insert(std::pair<t_LinearizedPos,UINT>(pos,matrix_associated.GetVec().GetSize()));
				return matrix_associated.GetVec().push_back(x);
			}else{ return matrix_associated.m_ZeroValue; }
		}
#pragma warning(default:4244)
	}
	DYNTYPE_FUNC void Zero()
	{	t_Associated::t_EntryMap::iterator p = matrix_associated.m_EntryMap.find(pos);
		if(p!=matrix_associated.m_EntryMap.end()){ matrix_associated.m_EntryMap.erase(p); }
	}
	DYNTYPE_FUNC t_Val& value()
	{	t_Associated::t_EntryMap::iterator p = matrix_associated.m_EntryMap.find(pos);
		if(p!=matrix_associated.m_EntryMap.end())
		{	return matrix_associated.GetVec()[p->second]; }
		else
		{	matrix_associated.m_EntryMap.insert(std::pair<t_LinearizedPos,UINT>(pos,matrix_associated.GetVec().GetSize()));
			return matrix_associated.GetVec().push_back(matrix_associated.m_ZeroValue);
		}
	}
	DYNTYPE_FUNC const t_Val& const_value() const
	{	t_Associated::t_EntryMap::iterator p = matrix_associated.m_EntryMap.find(pos);
		if(p!=matrix_associated.m_EntryMap.end())
		{	return matrix_associated.GetVec()[p->second]; }
		else
		{	return matrix_associated.m_ZeroValue; }
	}
	DYNTYPE_FUNC operator const t_Val& () const { return const_value(); }
};
} // namespace _meta_
} // namespace num

namespace num
{
namespace _meta_
{
template<class t_Ostream, typename t_Ele,class t_SparseMatrix>
t_Ostream& operator<<(t_Ostream& Ostream, const _ValueBinded<t_Ele,t_SparseMatrix> & pxb)
{	Ostream<<pxb.const_value(); return Ostream; }

} // namespace _meta_
} // namespace num


namespace num
{

namespace _meta_
{

// in-place vector row
template<typename t_Val,class t_SparseMatrix,int _vec_type> //_vec_type: 0,Row 1,Col 2,Diag
class _BaseSparseVecRoot
{	ASSERT_STATIC(_vec_type>=0 && _vec_type<=2);
	// friend class t_SparseMatrix;   // problem in VS2008
	UINT	i;
	t_SparseMatrix*	matrix_associated;
protected:
	DYNTYPE_FUNC _BaseSparseVecRoot(){}
	void SetAssociation(UINT index,t_SparseMatrix &mat)
	{	__static_if(_vec_type==0){ ASSERT(index<mat.GetSize_Row()); i=index; }
		__static_if(_vec_type==1){ ASSERT(index<mat.GetSize_Row()); i=index; }
		__static_if(_vec_type==2){ ASSERT(index<mat.GetSize_Row()); i=min(mat.GetSize_Row(),mat.GetSize_Col()); }
		matrix_associated = &mat;
	}
public:
	static const bool IsRef = true; 
	DYNTYPE_FUNC explicit _BaseSparseVecRoot(const _BaseSparseVecRoot &x){ ASSERT_STATIC(0); }	//copy ctor should be avoided, use reference for function parameters
	DYNTYPE_FUNC BOOL SetSize(UINT co=0){ return co==GetSize(); }
	DYNTYPE_FUNC BOOL ChangeSize(UINT new_size){ return new_size==GetSize(); }
	//TypeTraits
	TYPETRAITS_DECL_LENGTH(TYPETRAITS_SIZE_UNKNOWN)
	TYPETRAITS_DECL_IS_AGGREGATE(false)
	typedef UINT t_SizeType;

	template<typename T>
	DYNTYPE_FUNC _ValueBinded<t_Val,t_SparseMatrix> At(const T index)
	{	__static_if(_vec_type==0){ return matrix_associated->At(i,index); }
		__static_if(_vec_type==1){ return matrix_associated->At(index,i); }
		__static_if(_vec_type==2){ return matrix_associated->At(index,index); }
	}
	template<typename T>
	DYNTYPE_FUNC const t_Val & At(const T index) const
	{	__static_if(_vec_type==0){ return ((const t_SparseMatrix*)matrix_associated)->At(i,index); }
		__static_if(_vec_type==1){ return ((const t_SparseMatrix*)matrix_associated)->At(index,i); }
		__static_if(_vec_type==2){ return ((const t_SparseMatrix*)matrix_associated)->At(index,index); }
	}
	DYNTYPE_FUNC UINT GetSize() const
	{	__static_if(_vec_type==0){ return ((const t_SparseMatrix*)matrix_associated)->GetSize_Col(); }
		__static_if(_vec_type==1){ return ((const t_SparseMatrix*)matrix_associated)->GetSize_Row(); }
		__static_if(_vec_type==2){ return i; }
	}
};

template< class t_SparseMatrix,
		  int vec_type,
		  template<typename,class,int> class t_BaseRoot
		>
class _BaseSparseVec
{public:	template<typename t_Ele> class SpecifyItemType
			{public: typedef t_BaseRoot<t_Ele,t_SparseMatrix,vec_type> t_BaseVec; };
			static const bool IsCompactLinear = false;
};
} // namespace _meta_

template<	typename t_Val,
			class t_SparseMatrix,
			int vec_type,
			template<typename,class,int> class t_BaseRoot>
class Sparse_VectorInMatrix:public ::rt::_TypedVector<t_Val,::num::_meta_::_BaseSparseVec<t_SparseMatrix,vec_type,t_BaseRoot > >
{	
public:
	Sparse_VectorInMatrix(UINT index, t_SparseMatrix& mat){	__super::SetAssociation(index,mat); }
	Sparse_VectorInMatrix(const Sparse_VectorInMatrix& x){ memcpy(this,x,sizeof(Sparse_VectorInMatrix)); }
	DYNTYPE_FUNC ::num::_meta_::_ValueBinded<t_Val,t_SparseMatrix> operator [](UINT index)
	{ ASSERT((UINT)index<GetSize()); return At(index); }
	DYNTYPE_FUNC const t_Val & operator [](UINT index)const { ASSERT((UINT)index<GetSize()); return At(index); }

	TYPETRAITS_DECL_EXTENDTYPEID((rt::_typeid_codelib<<16)+11)
#pragma warning(disable:4244)
	ASSIGN_OPERATOR_VEC(Sparse_VectorInMatrix)
#pragma warning(default:4244)
	DEFAULT_TYPENAME(Sparse_VectorInMatrix)
private: // disabled _TypedVector functions due to use of rt::Swap
	// TODO: enable those function by place a specializated rt::Swap for _ValueBinded
	DYNTYPE_FUNC void Flip();
	DYNTYPE_FUNC void Shuffle();
};

//////////////////////////////////////////////////////////////////////
// storage type for sparse matrix
namespace _meta_
{
template<typename T>	// t_LinearizedPos can only be ULONGLONG or UINT
struct _DeduceLinearizeType;
	template<> struct _DeduceLinearizeType<ULONGLONG>{ typedef UINT IndexType; };
	template<> struct _DeduceLinearizeType<UINT>{ typedef WORD IndexType; };


template<typename t_Val,bool transposed = false,typename t_LinearizedPos = UINT>
class _BaseSparseMatrixRoot // Column major
{	//Element requirment: float/double
protected:
	DYNTYPE_FUNC _BaseSparseMatrixRoot(const _BaseSparseMatrixRoot &x){ ASSERT_STATIC(0); }

protected:
	template<typename t_Val, class t_SM>
	friend class ::num::_meta_::_ValueBinded;

	typedef union
	{	t_LinearizedPos Linearized;
		struct{
		typename _DeduceLinearizeType<t_LinearizedPos>::IndexType
			Low,High;
		};
	}t_LinearizedPosConv;

public:
	DYNTYPE_FUNC t_LinearizedPos _Linearize(UINT i,UINT j) const
	{	t_LinearizedPosConv ret;
		__static_if(!transposed){ ret.Low = j; ret.High = i; }
		__static_if(transposed) { ret.Low = i; ret.High = j; }
		return ret.Linearized;
	}
	DYNTYPE_FUNC void _ParseLinearized(t_LinearizedPos l,UINT& i,UINT& j) const
	{	__static_if(!transposed){
			j = ((t_LinearizedPosConv&)l).Low;
			i = ((t_LinearizedPosConv&)l).High;
		}
		__static_if(transposed){
			i = ((t_LinearizedPosConv&)l).Low;
			j = ((t_LinearizedPosConv&)l).High;
		}
	}
public:
	static const bool IsRef = false;
	typedef t_LinearizedPos t_LinearizedType;
	DYNTYPE_FUNC _BaseSparseMatrixRoot()
	{	m_ColCount = m_RowCount = 0;
		__static_if(::rt::TypeTraits<t_Val>::IsAggregate){
			ZeroMemory(&m_ZeroValue,sizeof(m_ZeroValue));}
#if defined(USE_GOOGLE_HASH_FOR_SPARSE_MATRIX)
		m_EntryMap.set_empty_key(t_EntryMap::key_type(-1));
#endif
	}
	DYNTYPE_FUNC ~_BaseSparseMatrixRoot(){}
	DYNTYPE_FUNC BOOL SetSize(UINT co){ return (co==GetSize())?TRUE:FALSE; }
	DYNTYPE_FUNC BOOL ChangeSize(UINT new_size){ return (new_size==GetSize())?TRUE:FALSE; }

	//TypeTraits
	TYPETRAITS_DECL_LENGTH(TYPETRAITS_SIZE_UNKNOWN)
	TYPETRAITS_DECL_IS_AGGREGATE(false)

	typedef ::num::_meta_::_ValueBinded<t_Val,_BaseSparseMatrixRoot> _BindedValue;

	template<typename T,typename T2>
	DYNTYPE_FUNC _BindedValue At(const T i,const T2 j)
	{	ASSERT(i<GetSize_Row() && j<GetSize_Col());
		return _BindedValue(_Linearize(i,j),*this);
	}
	template<typename T,typename T2>
	DYNTYPE_FUNC const t_Val & At(const T i,const T2 j) const
	{	ASSERT(i<GetSize_Row() && j<GetSize_Col());
		t_EntryMap::const_iterator p = m_EntryMap.find(_Linearize((UINT)i,(UINT)j));
		if(p!=m_EntryMap.end()){ return m_Values[p->second]; }
		else{ return m_ZeroValue; }
	}
	DYNTYPE_FUNC UINT GetSize() const{ return (UINT)(m_ColCount*m_RowCount); }

	typedef Sparse_VectorInMatrix<t_Val,_BaseSparseMatrixRoot,1,::num::_meta_::_BaseSparseVecRoot> t_ColumnRef;
	t_ColumnRef GetLine(UINT y){ return t_ColumnRef(y,*this); }
	const t_ColumnRef GetLine(UINT y) const{ return ::rt::_CastToNonconst(this)->GetLine(y); }

protected:	//sparse structure
	/* map linearized position to index of nonzero list */
#if defined(USE_BOOST_HASH_FOR_SPARSE_MATRIX)
	typedef ::boost::unordered_map<t_LinearizedPos,UINT>  t_EntryMap;
#elif defined(USE_GOOGLE_HASH_FOR_SPARSE_MATRIX)
	typedef ::google::dense_hash_map<t_LinearizedPos,UINT> t_EntryMap;
#else
	typedef ::stdext::hash_map<t_LinearizedPos,UINT>      t_EntryMap;
#endif
	t_Val						m_ZeroValue;	// value for empty
	UINT						m_ColCount,m_RowCount;
	t_EntryMap					m_EntryMap;
	::rt::BufferEx_32BIT<t_Val>	m_Values;

public:
	typedef typename t_EntryMap::const_iterator t_pNonzeroEntry;
	DYNTYPE_FUNC t_pNonzeroEntry GetNonzeroEntryBegin() const{ return m_EntryMap.begin(); }
	DYNTYPE_FUNC t_pNonzeroEntry GetNonzeroEntryEnd() const{ return m_EntryMap.end(); }
	DYNTYPE_FUNC UINT			 GetNonzeroEntryCount() const{ return (UINT)m_EntryMap.size(); }

	DYNTYPE_FUNC UINT		GetSize_Col() const{ return transposed?m_RowCount:m_ColCount; }
	DYNTYPE_FUNC UINT		GetSize_Row() const{ return transposed?m_ColCount:m_RowCount; }
	::rt::BufferEx_32BIT<t_Val>& GetVec(){ return m_Values; }
	const ::rt::BufferEx_32BIT<t_Val>& GetVec() const{ return m_Values; }

	DYNTYPE_FUNC BOOL SetSize_2D(UINT row=0,UINT col=0)
	{	m_EntryMap.clear();
		if(row==m_RowCount && col==m_ColCount){ return TRUE; }
		else
		{	if( row < ::rt::TypeTraits<typename _DeduceLinearizeType<t_LinearizedPos>::IndexType>::MaxVal() &&
				col < ::rt::TypeTraits<typename _DeduceLinearizeType<t_LinearizedPos>::IndexType>::MaxVal() )
			{	//If row/col will large than 65535, use ULONGLONG for t_LinearizedPos
				m_Values.SetSize(0);
				__static_if(!transposed){ m_ColCount = col;	m_RowCount = row; }
				__static_if(transposed){ m_ColCount = row;	m_RowCount = col; }
				return TRUE;
			}
		}
		return FALSE;
	}
	DYNTYPE_FUNC UINT		GetWidth() const{ return m_RowCount;}
	DYNTYPE_FUNC UINT		GetHeight() const{ return m_ColCount;}
};
template<bool transposed = false,typename t_LinearizedPos = UINT>
class _BaseSparseMatrix
{public:	template<typename t_Ele> class SpecifyItemType
			{public: typedef ::num::_meta_::_BaseSparseMatrixRoot<t_Ele,transposed,t_LinearizedPos> t_BaseVec; };
			static const bool IsCompactLinear = false;
			static const bool IsColumnMajor = true;
};

}//namespace _meta_


#define COMMON_SPARSE_MATRIX_STUFFS(clsname)	DEFAULT_TYPENAME(clsname)				\
		void TextDump() const{ _STD_OUT<<*this; _STD_OUT<<_T('\n');}							\
		template<typename t_Val2,class t_BaseVec2>												\
		DYNTYPE_FUNC const clsname& operator = (const rt::_TypedImage<t_Val2,t_BaseVec2>& x)\
		{	CopyFrom(x); return *this; }														\
		__static_if(!IsItemTypePlanar){															\
		DYNTYPE_FUNC const clsname& operator = (const FundamentalItemType & x)					\
		{	GetVec()=x; return *this; }															\
		}																						\
		DYNTYPE_FUNC const clsname& operator = (const ItemType & x)								\
		{ SetItem(x); return *this;}															\
		DYNTYPE_FUNC const clsname& operator = (const clsname & x)								\
		{ ASSERT(GetSize()==x.GetSize()); CopyFrom(x); return *this; }							\
		template<typename t_Val2> DYNTYPE_FUNC const clsname& operator =						\
		(const Sparse_Matrix<t_Val2,t_Transposed,t_LinearizedPos> & x)							\
		{ ASSERT(GetSize()==x.GetSize()); CopyFrom(x); return *this; }							\
		template<typename T, typename BaseVec>													\
		DYNTYPE_FUNC const clsname& operator = (const rt::_TypedVector<T,BaseVec> & x)			\
		{ ASSERT(GetSize()==x.GetSize()); CopyFrom(x); return *this; }							\
		template<typename T,int sz>																\
		DYNTYPE_FUNC const clsname& operator = (const T x[sz])									\
		{	ASSERT(GetSize()==sz);																\
			CopyFrom((const rt::_TypedVector<T,rt::_BaseVecFix<sz> >&)x); return *this;			\
		}																						\


template<typename t_Val,bool t_Transposed = false,typename t_LinearizedPos = UINT>
class Sparse_Matrix:public ::rt::_TypedImage<t_Val,::num::_meta_::_BaseSparseMatrix<t_Transposed,t_LinearizedPos> >
{	template<typename t_Val2,bool transposed2,typename t_LinearizedPos2>
	friend class Sparse_Matrix;

public: //override buffer's methods
	TYPETRAITS_DECL_EXTENDTYPEID((rt::_typeid_codelib<<16)+104)
	typedef t_LinearizedPos t_LinearizedType;
	::rt::BufferEx_32BIT<t_Val>& GetVec(){ return m_Values; }
	const ::rt::BufferEx_32BIT<t_Val>& GetVec() const{ return m_Values; }

	template<typename T>
	DYNTYPE_FUNC BOOL SetSizeAs(const T& x){ return SetSize_2D(x.GetSize_Row(),x.GetSize_Col()); }
	DYNTYPE_FUNC BOOL SetSize(UINT row=0,UINT col=0){ return SetSize_2D(row,col); }

	template<typename t_Val2>
	DYNTYPE_FUNC void CopyFrom(const Sparse_Matrix<t_Val2,t_Transposed,t_LinearizedPos> & x)
	{	m_EntryMap = x.m_EntryMap;
		GetVec().SetSizeAs(x.GetVec());
		GetVec() = x.GetVec();
	}
	template<typename t_Val2,class t_BaseVec2>
	void CopyFrom( const _TypedImage<t_Val2,t_BaseVec2>& v)
	{	ASSERT(GetWidth() == v.GetWidth());
		ASSERT(GetHeight() == v.GetHeight());
		Zero();
		for(UINT y=0;y<GetHeight();y++)
			for(UINT x=0;x<GetWidth();x++)
		{	const t_Val2& val = v.At(x,y);
			if(::num::_meta_::IsZero(val)){}
			else{ At(x,y) = val; }
		}
	}

	typedef ::num::_meta_::_BaseSparseMatrixRoot<t_Val,t_Transposed,t_LinearizedPos> t_MatrixRoot;

	typedef Sparse_VectorInMatrix<t_Val,t_MatrixRoot,0,::num::_meta_::_BaseSparseVecRoot> t_RowRef;
	t_RowRef GetRow(UINT i){ return t_RowRef(i,*this); }
	const t_RowRef GetRow(UINT i) const{ return ::rt::_CastToNonconst(this)->GetRow(i); }

	typedef Sparse_VectorInMatrix<t_Val,t_MatrixRoot,1,::num::_meta_::_BaseSparseVecRoot> t_ColumnRef;
	t_ColumnRef GetCol(UINT i){ return t_ColumnRef(i,*this); }
	const t_ColumnRef GetCol(UINT i) const{ return ::rt::_CastToNonconst(this)->GetCol(i); }

	typedef Sparse_VectorInMatrix<t_Val,t_MatrixRoot,2,::num::_meta_::_BaseSparseVecRoot> t_DiagVec;
	t_DiagVec GetDiag(){ return t_DiagVec(0,*this); }
	const t_DiagVec GetDiag() const{ return ::rt::_CastToNonconst(this)->GetRow(i); }

	BOOL	ReserveNonzeroEntry(UINT count){ return GetVec().reserve(count); }

public:
	Sparse_Matrix<t_Val,!t_Transposed,t_LinearizedPos> & operator !()
	{	return *((Sparse_Matrix<t_Val,!t_Transposed,t_LinearizedPos>*)this); }
	const Sparse_Matrix<t_Val,!t_Transposed,t_LinearizedPos> & operator !() const
	{	return ::rt::_CastToNonconst(this)->operator !(); }

	DYNTYPE_FUNC void		SetZeroValue(t_Val x){ ::rt::Swap(x,m_ZeroValue); }
	
public:
	template<typename t_Ele2> class SpecifyItemType
	{public: typedef Sparse_Matrix<t_Ele2> t_Result; };

	typedef ::num::_meta_::_ValueBinded<t_Val,::num::_meta_::_BaseSparseMatrixRoot<t_Val,t_Transposed,t_LinearizedPos> > _BindedValue;
	DYNTYPE_FUNC _BindedValue operator()(UINT i,UINT j){ return At(i,j); }
	DYNTYPE_FUNC const t_Val& operator()(UINT i,UINT j) const { return At(i,j); }
	DYNTYPE_FUNC void Zero(){ m_Values.SetSize(0); m_EntryMap.clear(); }

public:
	COMMON_CONSTRUCTOR_IMG(Sparse_Matrix)
	COMMON_SPARSE_MATRIX_STUFFS(Sparse_Matrix)

	////////////////////////////////////////////////////////////////////////////////////////
	// 3.1 Unary operators
	//
	// 3.1.1 Assignment
	static const bool IsItemTypePlanar = __super::IsItemTypePlanar;
#define ASSIGN_OP(op)	template<typename T, class BaseVec>														\
						DYNTYPE_FUNC void operator op (const rt::_TypedVector<T,BaseVec> & x){ GetVec() op x; } \
						DYNTYPE_FUNC void operator op (const ItemType & x){ GetVec() op x; }					\
						__static_if(!IsItemTypePlanar){															\
						DYNTYPE_FUNC void operator op (const FundamentalItemType & x){ GetVec() op x; }			\
						}																						\
						template<typename T> DYNTYPE_FUNC void operator op (const T* x){ GetVec() op x; }		\
						template<typename T> DYNTYPE_FUNC void operator op (T* x){ GetVec() op x; }				\

		ASSIGN_OP(+=)
		ASSIGN_OP(-=)
		ASSIGN_OP(*=)
		ASSIGN_OP(/=)
		ASSIGN_OP(%=)
		ASSIGN_OP(<<=)
		ASSIGN_OP(>>=)
		ASSIGN_OP(&=)
		ASSIGN_OP(|=)
		ASSIGN_OP(^=)
#undef  ASSIGN_OP

	// 3.1.2 Prefix
#define UNARY_OP_Prefix(op)	DYNTYPE_FUNC const Sparse_Matrix& operator op (){ op GetVec(); return *this; }
		UNARY_OP_Prefix(++)
		UNARY_OP_Prefix(--)
#undef  UNARY_OP_Prefix

	// 3.1.3 Postfix, functions same as Prefix
#define UNARY_OP_Postfix(op)	DYNTYPE_FUNC const Sparse_Matrix& operator op (int){ GetVec() op; return *this; }
		UNARY_OP_Postfix(++)
		UNARY_OP_Postfix(--)
#undef UNARY_OP_Postfix

public:
	template<typename t_ItemType2, typename BaseVec2>
	DYNTYPE_FUNC void CopyTo(::rt::_TypedImage<t_ItemType2,BaseVec2> & x) const
	{	x.Zero();
		t_pNonzeroEntry p=GetNonzeroEntryBegin();
		t_pNonzeroEntry end=GetNonzeroEntryEnd();
		for(;p!=end;p++)
		{	UINT i,j;
			_ParseLinearized(p->first,i,j);
			x.At(i,j) = GetVec()[p->second];
		}
	}

	////////////////////////////////////////////////////////////////////////////////////////
	// 3.3 Set all item with given value
	DYNTYPE_FUNC void SetItem(const ItemType & x){ GetVec().SetItem(x); }
	DYNTYPE_FUNC void Set(const FundamentalItemType & x){ GetVec().Set(x); }	

	////////////////////////////////////////////////////////////////////////////////////////
	// 3.8 Dot product/Sum/L2Norm_Sqr
	// 3.8.1 Dot product
	template< class t_Vec2 >
	DYNTYPE_FUNC _ItemValueType Dot(const t_Vec2 & y ) const 
	{	ASSERT(GetSize() == rt::GetSize(y));
		typedef _ItemValueType t_Accum;
		t_Accum tot=0;
		t_pNonzeroEntry p=GetNonzeroEntryBegin();
		t_pNonzeroEntry end=GetNonzeroEntryEnd();
		for(;p!=end;p++)
		{	UINT i,j;
			_ParseLinearized(p->first,i,j);
			tot+=GetVec()[p->second]*y.At(i,j);
		}
		return tot;
	}


	// 3.8.2 Sum
	DYNTYPE_FUNC _ItemValueType Sum() const { return GetVec().Sum(); }
	DYNTYPE_FUNC _ItemValueType L2Norm_Sqr() const { return GetVec().L2Norm_Sqr(); }

	// 3.8.4 L2Distance_Sqr
	template< class t_Vec2 >
	DYNTYPE_FUNC _ItemValueType Distance_Sqr(const t_Vec2 & y) const
	{	ASSERT(GetSize() == rt::GetSize(y));
		typedef _ItemValueType t_Accum;
		t_Accum tot=0;
		t_pNonzeroEntry p=GetNonzeroEntryBegin();
		t_pNonzeroEntry end=GetNonzeroEntryEnd();
		for(;p!=end;p++)
		{	UINT i,j;
			_ParseLinearized(p->first,i,j);
			tot+=::rt::Sqr(GetVec()[p->second] - y[i*GetSize_Row()+j]);
		}
		return tot;
	}

	////////////////////////////////////////////////////////////////////////////////////////
	// 3.10 Randomize elements
	DYNTYPE_FUNC void Random(){ GetVec().Random(); }

	template<typename t_ItemType2, typename BaseVec2>
	DYNTYPE_FUNC void CopyTo(::rt::_TypedVector<t_ItemType2,BaseVec2> & x) const
	{	ASSERT(GetSize() == x.GetSize());
		x.Zero();
		t_pNonzeroEntry p=GetNonzeroEntryBegin();
		t_pNonzeroEntry end=GetNonzeroEntryEnd();
		for(;p!=end;p++)
		{	UINT i,j;
			_ParseLinearized(p->first,i,j);
			x[i*GetSize_Row()+j] = GetVec()[p->second];
		}
		return tot;
	}
	template< class t_ItemType2 >
	DYNTYPE_FUNC void CopyTo( t_ItemType2 * p) const
	{	ASSERT_ARRAY(p,GetSize());
		ZeroMemory(p,sizeof(t_ItemType2)*GetSize());
		t_pNonzeroEntry p=GetNonzeroEntryBegin();
		t_pNonzeroEntry end=GetNonzeroEntryEnd();
		for(;p!=end;p++)
		{	UINT i,j;
			_ParseLinearized(p->first,i,j);
			p[i*GetSize_Row()+j] = GetVec()[p->second];
		}
		return tot;
	}
	DYNTYPE_FUNC void Flip(){ GetVec().Flip(); }
	DYNTYPE_FUNC void Shuffle(){ GetVec().Shuffle(); }
	DYNTYPE_FUNC int SearchItem( const ItemType& x ) const { return GetVec().SearchItem(x); }
	DYNTYPE_FUNC ItemType Min() const { return GetVec().Min(); }
	DYNTYPE_FUNC UINT MinIndex() const{ return GetVec().MinIndex(); }
	DYNTYPE_FUNC ItemType Max() const { return GetVec().Max(); }
	DYNTYPE_FUNC UINT MaxIndex() const{ return GetVec().MaxIndex(); }

public:
	// Persistence
	template<class t_w32_file>
	HRESULT Save(t_w32_file & file) const
	{
		ASSERT(file.IsOpen());
		ULONGLONG org_pos = file.GetCurrentPosition();

		DWORD dw = 0x534d4d53;		//signature "SMMS"
		file.Write(&dw,sizeof(DWORD));

		dw = sizeof(ItemType);	//byte-per-element, element size
		file.Write(&dw,sizeof(DWORD));
		
		dw = 0;
		__static_if( rt::TypeTraits<ItemType>::IsAggregate )
		{ dw = rt::TypeTraits<FundamentalItemType>::Typeid; /*Fundamental type id*/	}
		dw |= (rt::TypeTraits<ItemType>::Typeid)<<16; //element type id
		file.Write(&dw,sizeof(DWORD));

		dw = GetSize_Row();
		file.Write(&dw,sizeof(DWORD)); // row count
		dw = GetSize_Col();
		file.Write(&dw,sizeof(DWORD)); // col count
		dw = (DWORD)m_EntryMap.size();
		file.Write(&dw,sizeof(DWORD)); //nonzero element count

		HRESULT hr = S_OK;

		__static_if(!rt::TypeTraits<ItemType>::IsAggregate)
		{// non-Aggregate elements, t_w32_file should be CFile64_Composition
			TCHAR SectionName[20];
			t_EntryMap::const_iterator p = m_EntryMap.begin();
			for(;p!=m_EntryMap.end();p++)
			{
				_stprintf(SectionName,_T("#%d"),i);
				if(file.EnterSection(SectionName))
				{	UINT pos[2];
					_ParseLinearized(p->first,pos[0],pos[1]);
					file.Write(pos,sizeof(pos));
					hr = At(p->second).Save(file);
					file.ExitSection();
				}else{ hr = E_INVALIDARG; }
				if(FAILED(hr))break;
			}
		}
		__static_if(rt::TypeTraits<ItemType>::IsAggregate)
		{// Aggregate elements
			t_EntryMap::const_iterator p = m_EntryMap.begin();
			for(;p!=m_EntryMap.end();p++)
			{	UINT pos[2];
				_ParseLinearized(p->first,pos[0],pos[1]);
				file.Write(pos,sizeof(pos));
				if(file.Write(&At(p->second),sizeof(t_Val)) == sizeof(t_Val)){}
				else
				{	hr = file.HresultFromLastError();
					break;
				}
			}
		}

		if(FAILED(hr)){ file.Seek(org_pos); }
		return hr;
	}

	template<class t_w32_file>
	HRESULT	Load(t_w32_file & file)	//Load from storage
	{	if(!file.IsOpen())return E_INVALIDARG;
		ULONGLONG org_pos = file.GetCurrentPosition();
		HRESULT hr = E_INVALIDARG;

		BOOL TypeMismatch;
		DWORD dw=0;
		file.Read(&dw,sizeof(DWORD));
		if( dw == 0x534d4d53 )
		{
			file.Read(&dw,sizeof(DWORD));
			if( sizeof(ItemType) == dw )
			{
				file.Read(&dw,sizeof(DWORD)); //Typeid

				DWORD type_sign=0;
				{	__static_if( rt::TypeTraits<ItemType>::IsAggregate )
					{	type_sign = rt::TypeTraits<FundamentalItemType>::Typeid; /*Fundamental type id*/	}
					type_sign |= (rt::TypeTraits<ItemType>::Typeid)<<16; //element type id
				}
				TypeMismatch = ( dw != type_sign );

				DWORD r,c;
				file.Read(&r,sizeof(DWORD));
				file.Read(&c,sizeof(DWORD));
				if(SetSize(r,c))
				{
					file.Read(&dw,sizeof(DWORD)); //nonzero element count
					if(dw)
					{
						if(__super::SetSize(dw))
						{	
							__static_if(!rt::TypeTraits<ItemType>::IsAggregate)
							{// non-Aggregate elements, t_w32_file should be CFile64_Composition
								TCHAR SectionName[20];
								for(UINT i=0;i<__super::GetSize();i++)
								{	_stprintf(SectionName,_T("#%d"),i);
									if(file.EnterSection(SectionName))
									{	UINT pos[2];
										file.Read(pos,sizeof(pos));
										if(SUCCEEDED(hr = At(i).Load(file)))
											m_EntryMap.insert(std::pair<t_LinearizedPos,UINT>(_Linearize(pos[0],pos[1]),i));
										file.ExitSection();
									}else{ hr = file.HresultFromLastError(); }
									if(FAILED(hr))break;
								}
							}
							__static_if(rt::TypeTraits<ItemType>::IsAggregate)
							{// Aggregate elements
								hr = S_OK;
								for(UINT i=0;i<__super::GetSize();i++)
								{	UINT pos[2];
									file.Read(pos,sizeof(pos));
									if(file.Read(&At(i),sizeof(t_Val)) == sizeof(t_Val))
									{	m_EntryMap.insert(std::pair<t_LinearizedPos,UINT>(_Linearize(pos[0],pos[1]),i)); }
									else{ hr = file.HresultFromLastError(); break; }
								}
							}
						}
						else{ hr = E_OUTOFMEMORY; }
					}
					else
					{	__super::SetSize(0);
						hr = S_OK; 
					}
				}
				else{ hr = E_OUTOFMEMORY; }
			}
		}

		if(FAILED(hr)){ file.Seek(org_pos); SetSize(); }
		return hr;
	}
};

template<class t_Ostream, typename t_Val,bool transposed,typename t_LinearizedPos>
t_Ostream& operator<<(t_Ostream& Ostream, const ::num::Sparse_Matrix<t_Val,transposed,t_LinearizedPos> & km)
{
	if(km.GetSize_Row()==0 || km.GetSize_Col()==0)
	{	Ostream<<_meta_::_text_null(); return Ostream; }
	else if(km.GetNonzeroEntryCount()){}
	else
	{	Ostream<<_meta_::_text_left_empty()<<km.GetSize_Row()<<_T('x')<<km.GetSize_Col()<<_meta_::_text_right_empty();
		return Ostream;
	}
	Ostream<<_meta_::_text_left()<<km.GetSize_Row()<<_T('x')<<km.GetSize_Col()<<_meta_::_text_sparse();
	__static_if(transposed){ Ostream<<_meta_::_text_transpose(); }
	Ostream<<_T(',')<<_T(' ')<<km.GetNonzeroEntryCount()<<_meta_::_text_nz();

	if(::rt::_meta_::IsStreamStandard(Ostream))
	{
		UINT ele_limite = 8;

		if(km.GetSize_Row()<=ele_limite)
		{//print all
			for(UINT i=0;i<km.GetSize_Row()-1;i++)
			{
				Ostream<<km.GetRow(i)<<_T(',')<<_T('\n');
			}
			Ostream<<km.GetRow(km.GetSize_Row()-1)<<_T('\n');
		}
		else
		{//print head and tail
			for(UINT i=0;i<=4;i++)
				Ostream<<km.GetRow(i)<<_T(',')<<_T('\n');
			Ostream<<_meta_::_text_seperator();
			Ostream<<km.GetRow(km.GetSize_Row()-2)<<_T(',')<<_T('\n');
			Ostream<<km.GetRow(km.GetSize_Row()-1)<<_T('\n');
		}
		Ostream<<_T('}')<<_T('\n');
	}
	else
	{	::num::Sparse_Matrix<t_Val,transposed,t_LinearizedPos>::t_pNonzeroEntry p = km.GetNonzeroEntryBegin();
		for(;p!=km.GetNonzeroEntryEnd();p++)
		{	
			num::Vec2u pos;
			km._ParseLinearized(p->first,pos.x,pos.y);
			Ostream<<_meta_::_text_at()<<pos<<_meta_::_text_equ()<<km.At(pos.x,pos.y)<<_T('\n');
		}
	}

	return Ostream;
}




}
