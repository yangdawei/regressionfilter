#pragma once

//////////////////////////////////////////////////////////////////////
// Visual Computing Foundation Classes (VCFC)
//					Jiaping Wang  2006.2
//					e_boris2002@hotmail.com
//
// (C) Copyright Jiaping Wang 2004.
//
// Permission to copy, use, modify, sell and distribute this software
// is granted provided this copyright notice appears in all copies.
// This software is provided "as is" without express or implied
// warranty, and with no claim as to its suitability for any purpose.
//
// Absolutely no warranty!!
//////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////
//  Brief Description
//
//  tabulate_func.h
//
//  tabulated functions
//
//////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////
// Revise History
//
// Inital version	2006.6.10		Jiaping
//
//////////////////////////////////////////////////////////////////////

#include "..\rt\compact_vector.h"
#include <float.h>

#ifndef NUMERIC_FUNC
#	define NUMERIC_FUNC __forceinline
#endif

namespace num
{

template<typename t_Val>
class Tabulated_Function:public rt::Buffer<t_Val>
{
public:
	COMMON_CONSTRUCTOR_VEC(Tabulated_Function)
#pragma warning(disable:4244)
	ASSIGN_OPERATOR_VEC(Tabulated_Function)
#pragma warning(default:4244)
	DEFAULT_TYPENAME(Tabulated_Function)

public:
	template<typename t_Val2>
	NUMERIC_FUNC ItemType GetInterpolatedValue(t_Val2 u) const //0<=u<=1
	{	ASSERT( u>=-EPSILON && u<=1.0+EPSILON );
		u*=(GetSize()-1);
				
		UINT idx = (UINT)(int)u;

		if(idx<GetSize()-1)
		{
			return (At(idx))*(idx+1-u) + (At(idx+1))*(u-idx);
		}
		else{ return _p[GetSize()-1]; }
	}

	template<typename t_Val2>
	void SetSortedIrregluarSamples(const num::Vec2<t_Val2>* p, UINT count)
	{
		if(!GetSize())SetSize(count*2);
		double x_scale = 1.0/p[count-1].x;
		double y_scale = 1.0/p[count-1].y;

		UINT	last_idx = 0;
		t_Val2	last_val = 0;
		
		for(UINT i=0;i<count;i++)
		{
			UINT	idx = (UINT)(int)(p[i].x*x_scale*(GetSize()-1));
			t_Val2	val = p[i].y*y_scale;
			for(UINT d=last_idx;d<idx;d++)
			{	typedef typename rt::TypeResult<t_Val,t_Val2>::t_Result RET;
				RET a = (d-last_idx)/(RET)(idx-last_idx);
				At(d) = (last_val*(1-a)) + (val*a);
			}

			last_idx = idx;
			last_val = val;
		}
		At(GetSize()-1) = 1;
	}

	template<typename t_Val2, typename BaseVector2>
	void InverseTo( rt::_TypedVector<t_Val2,BaseVector2>& Inversed ) const //Apply to Mono function [0,1]<->[0,1]
	{	ASSERT( InvserseFunctionTable.GetSize() );
		if( At(0) <= At(GetSize()-1) )	
		{//Increasing
			int org = 0;
			Inversed[0] = 0;
			for(int i=1;i<Inversed.GetSize()-1;i++)
			{
				double goal = i/(double)(InvserseFunctionTable.GetSize()-1);

				while(At(org) < goal)org++;
				ASSERT(org < GetSize() );
				ASSERT(org);

				Inversed[i] = (	(goal - (*this)[org-1]) /
								(At(org)-At(org-1))
								+ org-1 )/(double)(GetSize()-1);
			}
			Inversed[Inversed.GetSize()-1] = (t_Val2)1;
		}
		else
		{//Decreasing
			int org = GetSize()-1;
			Inversed[0] = (t_Val2)1;
			for(int i=1;i<Inversed.GetSize()-1;i++)
			{
				double goal = i/(double)(Inversed.GetSize()-1);

				while((*this)[org] < goal)org--;
				ASSERT(org < (*this).GetSize());
				ASSERT(org>=0);

				Inversed[i] =	(	(goal - (*this)[org+1]) /
									((*this)[org]-(*this)[org+1])
									+ org )/(double)(GetSize()-1);
 			}
			Inversed[Inversed.GetSize()-1] = 0;
		}
	}

	_ItemValueType GaussKernel(float scale=4.0f)  //return sum
	{
		_ItemValueType tot = 0;
		UINT half = (UINT)((GetSize()+1)>>1);
		float center = (GetSize()-1)*0.5f;
		scale /= (float)(GetSize()-1);

		float level = (rt::TypeTraits<ItemType>::MaxVal()/GetSize())/256.0f + 1.0f;

        UINT i=0;
		for(;i<=half;i++)
		{	
			if( rt::NumericTraits<ItemType>::IsInteger )
				_p[i] = level*exp(-rt::Sqr(scale*(center-i))) + 0.5f;
			else
				_p[i] = exp(-rt::Sqr(scale*(center-i)));

			tot += _p[i];
		}

		for(;i<GetSize();i++)
		{
			tot += (_p[i] = _p[GetSize()-i-1]);
		}

		if( rt::NumericTraits<ItemType>::IsInteger )
			return tot;
		else
		{
			*this *= ((_ItemValueType)1)/tot;
			return (_ItemValueType)1;
		}
	}

	_ItemValueType GaussKernelHalf(float scale=4.0f)  //return sum
	{
		_ItemValueType tot = 0;
		scale /= (float)(GetSize()-1);

		float level = (rt::TypeTraits<ItemType>::MaxVal()/GetSize())/256.0f + 1.0f;

        UINT i=0;
		for(;i<GetSize();i++)
		{	
			if( rt::NumericTraits<ItemType>::IsInteger )
				_p[i] = level*exp(-rt::Sqr(scale*i)) + 0.5f;
			else
				_p[i] = exp(-rt::Sqr(scale*i));

			tot += _p[i];
		}

		if( rt::NumericTraits<ItemType>::IsInteger )
			return tot;
		else
		{
			*this *= ((_ItemValueType)1)/tot;
			return (_ItemValueType)1;
		}
	}

	void ConformValue(ItemType value, UINT idx)  //return sum
	{	
		ASSERT( idx<GetSize() );
		ItemType offset; 
		offset = value - At(idx);
		At(idx) = value;

		for(UINT i=0;i<idx;i++)
		{
			(*this)[i] = (*this)[i] + offset*(i/(float)idx);
		}
		for(UINT i=idx+1;i<GetSize();i++)
		{
			(*this)[i] = (*this)[i] + offset*((GetSize()-i)/(float)(GetSize()-idx));
		}
	}
};

} // namespace num
