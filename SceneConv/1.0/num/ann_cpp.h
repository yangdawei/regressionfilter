#pragma once

//////////////////////////////////////////////////////////////////////
// Visual Computing Foundation Classes (VCFC)
//					Jiaping Wang  2006.2
//					e_boris2002@hotmail.com
//
// (C) Copyright Jiaping Wang 2004.
//
// Permission to copy, use, modify, sell and distribute this software
// is granted provided this copyright notice appears in all copies.
// This software is provided "as is" without express or implied
// warranty, and with no claim as to its suitability for any purpose.
//
// Absolutely no warranty!!
//////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////
//  Brief Description
//
//  ann_cpp.h
//
//  ANN wrapper class
//
//////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////
// Revise History
//
// Inital version	2005.10		Jiaping
//
//////////////////////////////////////////////////////////////////////

#include "..\rt\compact_vector.h"
#include <ANN_32f.h>

#ifdef _WIN64
	#pragma message("\n>> Linking with EM64t version of ANN\n")
    #pragma comment(lib,"ann_32f_vc05_x64.lib")
#else
	#pragma message("\n>> Linking with IA32 version of ANN\n")
    #pragma comment(lib,"ANN_32f_intel.lib")
#endif


namespace num
{

class ANN_Tree
{
public:
	ANNkd_tree*				m_pTree;
	rt::Buffer<LPFLOAT>		m_pPointArray;

	rt::Buffer<ANNidx>		m_ResultIndex;
	rt::Buffer<ANNdist>		m_ResultDistance;
	UINT					m_AvailableResults;

	UINT					m_Dimen;

public:
	DYNTYPE_FUNC ANN_Tree(){ m_pTree = NULL; }
	DYNTYPE_FUNC ~ANN_Tree(){ _SafeDel(m_pTree); }

	DYNTYPE_FUNC void Clear()
	{
		_SafeDel(m_pTree);
		m_pPointArray.SetSize();
		m_ResultIndex.SetSize();
		m_ResultDistance.SetSize();
	}

	DYNTYPE_FUNC BOOL IsNull(){ return m_pTree==NULL; }

	DYNTYPE_FUNC void SetupSearchSpace(float* pPoints,int dimen,int co)
	{
		m_Dimen = dimen;
		m_pPointArray.SetSize(co);
		for(int i=0;i<co;i++)
			m_pPointArray[i] = &pPoints[dimen*i];

		Rebuild();
	}

	DYNTYPE_FUNC LPFLOAT& operator [](UINT i){ return m_pPointArray[i]; }

	DYNTYPE_FUNC void SetSearchSpaceSize(UINT co = 0,UINT dimen = 0) //Zero for clear
	{
		_SafeDel(m_pTree);

		m_pPointArray.SetSize(co);
		m_Dimen = dimen;
	}

	DYNTYPE_FUNC void Rebuild()
	{
		_SafeDel(m_pTree);

#ifdef _DEBUG //Check all pointers
		BOOL failed = FALSE;
		for(UINT i=0;i<m_pPointArray.GetSize();i++)
		{
			if(w32::IsValidAddress_Read(m_pPointArray[i],sizeof(float)*m_Dimen)){}
			else
			{	_CheckDump("KD-TREE: point "<<i<<" is invalid (0x"<<m_pPointArray[i]<<")\n"); 
				failed = TRUE;
			}
		}
		ASSERT(!failed);
#endif

		m_pTree = new ANNkd_tree(m_pPointArray, m_pPointArray.GetSize(), m_Dimen);
		ASSERT( m_pTree );
		
		SetResultSlots(1);
	}

	DYNTYPE_FUNC void SetResultSlots(UINT co)
	{	ASSERT(m_pTree);
		ASSERT(co<=m_pPointArray.GetSize());

		m_ResultIndex.SetSize(co);
		m_ResultDistance.SetSize(co);
		m_AvailableResults = 0;
	}

	DYNTYPE_FUNC void  Query(const float* pPoint)
	{	ASSERT(m_pTree);
		m_pTree->annkSearch(	rt::_CastToNonconst(pPoint),
								m_ResultIndex.GetSize(),
								m_ResultIndex,
								m_ResultDistance
								);
		m_AvailableResults = m_ResultIndex.GetSize();
	}

	DYNTYPE_FUNC UINT  Query(const float* pt,float MaxDistanceSqr)
	{	ASSERT(m_pTree);
		m_AvailableResults = m_pTree->annkFRSearch(	rt::_CastToNonconst(pt),
													MaxDistanceSqr,
													m_ResultIndex.GetSize(),
													m_ResultIndex,
													m_ResultDistance
													);

		return min(m_AvailableResults, m_ResultIndex.GetSize());
	}

	DYNTYPE_FUNC UINT	GetResultIndex(UINT i=0){ ASSERT( m_AvailableResults>i ); return m_ResultIndex[i]; }

	DYNTYPE_FUNC float*	GetResultVector(UINT i=0)
	{	UINT idx = GetResultIndex(i);
		ASSERT(idx<m_pPointArray.GetSize());

		return m_pPointArray[idx];
	}

	DYNTYPE_FUNC float	GetResultDistance(UINT i=0)
	{	ASSERT( m_AvailableResults>i );
		return m_ResultDistance[i];
	}

	DYNTYPE_FUNC UINT GetSize(){ return m_pPointArray.GetSize(); }
	static void FinalCleanUp(){ annClose(); }
};




}


















