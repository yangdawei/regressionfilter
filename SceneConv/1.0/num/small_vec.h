#pragma once

//////////////////////////////////////////////////////////////////////
// Visual Computing Foundation Classes (VCFC)
//					Jiaping Wang  2006.2
//					e_boris2002@hotmail.com
//
// (C) Copyright Jiaping Wang 2004.
//
// Permission to copy, use, modify, sell and distribute this software
// is granted provided this copyright notice appears in all copies.
// This software is provided "as is" without express or implied
// warranty, and with no claim as to its suitability for any purpose.
//
// Absolutely no warranty!!
//////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////
//  Brief Description
//
//  small_vec.h
//
//  instantiations of rt::Vec for small vectors
//
//  define NUM_SMALLVEC_POLAR_Z to use z+ for polar axis
//
//////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////
// Revise History
//
// Inital version	2006.6.7		Jiaping
//
//////////////////////////////////////////////////////////////////////

#include <math.h>
#include "fpu_abs.h"
#include "..\rt\fixvec_type.h"

#define DEFAULT_STUFFS(clsname)									\
		COMMON_FIXVEC_CONSTRUCTOR(clsname)						\
		ASSIGN_OPERATOR_VEC(clsname)							\
		DEFAULT_TYPENAME(clsname)								\
		template<typename t_Ele2> class SpecifyItemType			\
		{public: typedef ::num::clsname<t_Ele2> t_Result; };	\
		TYPETRAITS_DECL_ACCUM_TYPE(typename SpecifyItemType< typename rt::TypeTraits<t_Val>::t_Accum >::t_Result)	\
		TYPETRAITS_DECL_VALUE_TYPE(typename SpecifyItemType< typename rt::TypeTraits<t_Val>::t_Val >::t_Result)		\
		TYPETRAITS_DECL_SIGNED_TYPE(typename SpecifyItemType<typename rt::TypeTraits<t_Val>::t_Signed>::t_Result)	\
		TYPETRAITS_DECL_IS_NUMERIC(true)																			\


namespace num
{

//pre-declaration
template< typename t_val >class Vec1;
template< typename t_val >class Vec2;
template< typename t_val >class Vec3;
template< typename t_val >class Vec4;

template< typename t_Val >
class Vec1:public rt::Vec<t_Val,1>
{	static const int LEN=1;
public:
	TYPETRAITS_DECL_EXTENDTYPEID((rt::_typeid_codelib<<16)+6)
	//All default ctor
	Vec1(){}
	explicit DYNTYPE_FUNC Vec1(const Vec1& x){ *this=x; }
	template<typename T,class BaseVec>
	explicit DYNTYPE_FUNC Vec1(const rt::_TypedVector<T,BaseVec>& x){ *this=x; }
	template<typename T>
	DYNTYPE_FUNC Vec1(const rt::Vec<T,LEN>& x){ *this = x; }

	template<typename t_Ele2> class SpecifyItemType{public: typedef ::num::Vec1<t_Ele2> t_Result; };
	TYPETRAITS_DECL_ACCUM_TYPE(typename SpecifyItemType< typename rt::TypeTraits<t_Val>::t_Accum >::t_Result)
	TYPETRAITS_DECL_VALUE_TYPE(typename SpecifyItemType< typename rt::TypeTraits<t_Val>::t_Val >::t_Result)
	TYPETRAITS_DECL_SIGNED_TYPE(typename SpecifyItemType<typename rt::TypeTraits<t_Val>::t_Signed>::t_Result)
	TYPETRAITS_DECL_IS_NUMERIC(true)
	DYNTYPE_FUNC operator t_Val ()const{ return x; }

	ASSIGN_OPERATOR_VEC(Vec1)
	DEFAULT_TYPENAME(Vec1)

	template<typename t_Val2>
	DYNTYPE_FUNC const Vec1& operator =(const Vec2<t_Val2>& i)
	{ x=(t_Val)i.GetBrightness(); return *this; }
	template<typename t_Val2>
	DYNTYPE_FUNC const Vec1& operator =(const Vec3<t_Val2>& i)
	{ x=(t_Val)i.GetBrightness(); return *this; }
	template<typename t_Val2>
	DYNTYPE_FUNC const Vec1& operator =(const Vec4<t_Val2>& i)
	{ x=(t_Val)i.GetBrightness(); return *this; }

	DYNTYPE_FUNC Vec1(t_Val i){ x = i; } 
	//this non-explicit ctor conflict with the explicit one defined in COMMON_CONSTRUCTOR_VEC
	//so I have to expand all default ctor marcos
	DYNTYPE_FUNC t_Val GetBrightness()const { return x; }
};

template< typename t_Val >
class Vec2:public rt::Vec< t_Val , 2 >
{	static const int LEN=4;
public:
	TYPETRAITS_DECL_EXTENDTYPEID((rt::_typeid_codelib<<16)+7)
	DEFAULT_STUFFS(Vec2)

	template<typename t_Val2>
	const Vec2& operator =(const Vec1<t_Val2>& i){ x=y=(t_Val)i.x; return *this; }
	template<typename t_Val2>
	const Vec2& operator =(const Vec3<t_Val2>& i){ *this = i.v2(); return *this; }
	template<typename t_Val2>
	const Vec2& operator =(const Vec4<t_Val2>& i){ x=(t_Val)i.GetBrightness(); y=(t_Val)i.a; return *this; }

	DYNTYPE_FUNC Vec1<t_Val>& v1(){ return (Vec1<t_Val>&)*this; }
	DYNTYPE_FUNC const Vec1<t_Val>& v1()const { return (Vec1<t_Val>&)*this; }

public:
	DYNTYPE_FUNC Vec2(t_Val x_theta,t_Val y_phi){ x = x_theta; y = y_phi; }

	template<typename t_Val2> //+y is the pole-asix
	DYNTYPE_FUNC void Sph2Vec(Vec3<t_Val2>& v) const
	{
		if(NumericTraits_Item::IsInteger)
		{	
#ifdef NUM_SMALLVEC_POLAR_Z
			v.z = cos(theta*PI/180.0);
			v.y = sin(theta*PI/180.0);
			  
			v.x = v.y*cos(phi*PI/180.0);
			v.y = v.y*sin(phi*PI/180.0);
#else
			v.y = cos(theta*PI/180.0);
			v.z = sin(theta*PI/180.0);
			  
			v.x = v.z*cos(phi*PI/180.0);
			v.z = v.z*sin(phi*PI/180.0);
#endif
		}
		else
		{
#ifdef NUM_SMALLVEC_POLAR_Z
			v.z = cos((double)theta);
			v.y = sin((double)theta);
			  
			v.x = v.y*cos((double)phi);
			v.y = v.y*sin((double)phi);
#else
			v.y = cos((double)theta);
			v.z = sin((double)theta);
			  
			v.x = v.z*cos((double)phi);
			v.z = v.z*sin((double)phi);
#endif
		}
	}

	DYNTYPE_FUNC t_Val GetBrightness()const { return (x+y)/2; }
};



template< typename t_Val >
class Vec3:public rt::Vec< t_Val , 3 >
{	static const int LEN=3;
public:
	TYPETRAITS_DECL_EXTENDTYPEID((rt::_typeid_codelib<<16)+8)
	DEFAULT_STUFFS(Vec3)

	template<typename t_Val2>
	const Vec3& operator =(const Vec1<t_Val2>& i){ x=y=z=(t_Val)i.x; return *this; }
	template<typename t_Val2>
	const Vec3& operator =(const Vec2<t_Val2>& i){ v2() = i; return *this; }
	template<typename t_Val2>
	const Vec3& operator =(const Vec4<t_Val2>& i){ *this = i.v3(); return *this; }

	DYNTYPE_FUNC Vec1<t_Val>& v1(){ return (Vec1<t_Val>&)*this; }
	DYNTYPE_FUNC const Vec1<t_Val>& v1()const { return (Vec1<t_Val>&)*this; }
	DYNTYPE_FUNC Vec2<t_Val>& v2(){ return (Vec2<t_Val>&)*this; }
	DYNTYPE_FUNC const Vec2<t_Val>& v2()const { return (Vec2<t_Val>&)*this; }

public:
	DYNTYPE_FUNC Vec3(t_Val x_r,t_Val y_g,t_Val z_b)
	{ x = x_r; y = y_g; z = z_b; }

	template<typename t_Val2,typename t_Val3>   // a x b -> This
	DYNTYPE_FUNC void CrossProduct(const Vec3<t_Val2>& in1,const Vec3<t_Val3>& in2)
	{
		x =  in1.y*in2.z - in1.z*in2.y;
		y = -in1.x*in2.z + in1.z*in2.x;
		z =  in1.x*in2.y - in1.y*in2.x;
	}

	template<typename t_Val2,typename t_Val3,typename t_Val4>	// (a-org) x (b-org) -> This
	DYNTYPE_FUNC void CrossProduct(const Vec3<t_Val2>& a,const Vec3<t_Val3>& b,const Vec3<t_Val4>& c)
	{
		x =  (a.y-c.y)*(b.z-c.z) - (a.z-c.z)*(b.y-c.y);
		y = -(a.x-c.x)*(b.z-c.z) + (a.z-c.z)*(b.x-c.x);
		z =  (a.x-c.x)*(b.y-c.y) - (a.y-c.y)*(b.x-c.x);
	}
    
    void GetAxisDirection(Vec3<t_Val> *pVS,Vec3<t_Val> *pVT)
    {
        Vec3<t_Val> vN;
        Vec3<t_Val>& vS = *pVS;
        Vec3<t_Val>& vT = *pVT;
        NormalizeTo(vN);
        if(fabs(vN.z)>1.0f - 1e-6)
        {
            if(vN.z>0)
            {
                vS = Vec3<t_Val>(1,0,0);
                vT = Vec3<t_Val>(0,1,0);
            }
            else
            {
                vS = Vec3<t_Val>(1,0,0);
                vT = Vec3<t_Val>(0,-1,0);
            }
        }
        else
        {
            vS.CrossProduct(vN,Vec3<t_Val>(0,0,1));
            vS.Normalize();
            vT.CrossProduct(vN,vS);
            vT.Normalize();
        }
    }

	template<typename t_Val2,typename t_Val3>	//The Area of the Triangle (this,in,in2) in 3D
	DYNTYPE_FUNC t_Val TriangleArea(const Vec3<t_Val2>& in,const Vec3<t_Val3>& in2) const
	{	return sqrt(	rt::Sqr( (in2.y-y)*(in.z-z) - (in2.z-z)*(in.y-y) ) + 
						rt::Sqr( (in2.x-x)*(in.z-z) - (in2.z-z)*(in.x-x) ) +
						rt::Sqr( (in2.x-x)*(in.y-y) - (in2.y-y)*(in.x-x) ) )/2;
	}

	template<typename t_Val2> //+y is the pole-asix
	DYNTYPE_FUNC void Vec2Sph(Vec2<t_Val2>& sc) const
	{
		if(rt::NumericTraits<t_Val2> ::IsInteger)
		{
#ifdef NUM_SMALLVEC_POLAR_Z
			sc.x = (t_Val2)(180*atan2(sqrt(x*x + y*y),z)/PI + 0.5f);
			sc.y = (t_Val2)(180*atan2(y, x)/PI + 0.5f);
			if(sc.y < 0)sc.y += 360;
#else
			sc.x = (t_Val2)(180*atan2(sqrt(x*x + z*z),y)/PI + 0.5f);
			sc.y = (t_Val2)(180*atan2(z, x)/PI + 0.5f);
			if(sc.y < 0)sc.y += 360;
#endif
		}
		else
		{
#ifdef NUM_SMALLVEC_POLAR_Z
			sc.x = (t_Val2)atan2(sqrt(x*x + y*y),z);
			sc.y = (t_Val2)atan2(y, x);
			if(sc.y < 0)sc.y += (t_Val2)(PI * 2.0);
#else
			sc.x = (t_Val2)atan2(sqrt(x*x + z*z),y);
			sc.y = (t_Val2)atan2(z, x);
			if(sc.y < 0)sc.y += (t_Val2)(PI * 2.0);
#endif
		}
	}

	DYNTYPE_FUNC t_Val GetBrightness() const
	{	if(NumericTraits_Item::IsInteger)
		{ return (t_Val)(((int)(r+g+g+b))>>2); }
		else
		{ return (t_Val)(rt::sc_Brightness_RGB[0]*r+rt::sc_Brightness_RGB[1]*g+rt::sc_Brightness_RGB[2]*b); }
	}

	bool operator <(const Vec3&p) const
	{	
		__static_if(NumericTraits_Item::IsFloat)
		{
			if( p.x - x > ::rt::TypeTraits<t_Val>::Epsilon() )
				return true;
			else if( p.x - x < -::rt::TypeTraits<t_Val>::Epsilon() )
				return false;
			if( p.y - y > ::rt::TypeTraits<t_Val>::Epsilon() )
				return true;
			else if( p.y - y < -::rt::TypeTraits<t_Val>::Epsilon() )
				return false;

			return p.z - z > ::rt::TypeTraits<t_Val>::Epsilon();
		}
		__static_if(!NumericTraits_Item::IsFloat)
		{
			if( p.x > x ) return true;
			else if( p.x < x ) return false;
			if( p.y > y) return true;
			else if( p.y < y ) return false;

			return p.z > z;
		}
	}
};

}


namespace num
{

template< typename t_Val >
class Vec4:public rt::Vec< t_Val , 4 >
{	static const int LEN=4;
public:
	TYPETRAITS_DECL_EXTENDTYPEID((rt::_typeid_codelib<<16)+9)
	DEFAULT_STUFFS(Vec4)

	template<typename t_Val2>
	const Vec4& operator =(const Vec1<t_Val2>& i){ x=y=z=(t_Val)i.x; return *this; }
	template<typename t_Val2>
	const Vec4& operator =(const Vec2<t_Val2>& i){ x=y=z=(t_Val)i.x; a=(t_Val)i.y; return *this; }
	template<typename t_Val2>
	const Vec4& operator =(const Vec3<t_Val2>& i){ v3() = i; return *this; }

	DYNTYPE_FUNC Vec1<t_Val>& v1(){ return (Vec1<t_Val>&)*this; }
	DYNTYPE_FUNC const Vec1<t_Val>& v1()const { return (Vec1<t_Val>&)*this; }
	DYNTYPE_FUNC Vec2<t_Val>& v2(){ return (Vec2<t_Val>&)*this; }
	DYNTYPE_FUNC const Vec2<t_Val>& v2()const { return (Vec2<t_Val>&)*this; }
	DYNTYPE_FUNC Vec3<t_Val>& v3(){ return (Vec3<t_Val>&)*this; }
	DYNTYPE_FUNC const Vec3<t_Val>& v3()const { return (Vec3<t_Val>&)*this; }

public:
	DYNTYPE_FUNC Vec4(t_Val x_r,t_Val y_g,t_Val z_b,t_Val w_a)
	{ r = x_r; g = y_g; b = z_b; a = w_a;}

public:
	DYNTYPE_FUNC t_Val GetBrightness() const{ return v3().GetBrightness(); }
};

typedef Vec1<float> Vec1f;
typedef Vec2<float> Vec2f;
typedef Vec3<float> Vec3f;
typedef Vec4<float> Vec4f;

typedef Vec1<double> Vec1d;
typedef Vec2<double> Vec2d;
typedef Vec3<double> Vec3d;
typedef Vec4<double> Vec4d;

typedef Vec1<int> Vec1i;
typedef Vec2<int> Vec2i;
typedef Vec3<int> Vec3i;
typedef Vec4<int> Vec4i;

typedef Vec1<short> Vec1s;
typedef Vec2<short> Vec2s;
typedef Vec3<short> Vec3s;
typedef Vec4<short> Vec4s;

typedef Vec1<UINT> Vec1u;
typedef Vec2<UINT> Vec2u;
typedef Vec3<UINT> Vec3u;
typedef Vec4<UINT> Vec4u;

typedef Vec1<BYTE> Vec1b;
typedef Vec2<BYTE> Vec2b;
typedef Vec3<BYTE> Vec3b;
typedef Vec4<BYTE> Vec4b;

typedef Vec1<signed char> Vec1c;
typedef Vec2<signed char> Vec2c;
typedef Vec3<signed char> Vec3c;
typedef Vec4<signed char> Vec4c;

template<typename t_value,unsigned int t_len>
struct SpecifySmallVec
{ typedef ::rt::Vec<t_value,t_len> t_Result; };
	template<typename t_value> struct SpecifySmallVec<t_value,1>
	{ typedef num::Vec1<t_value> t_Result; };
	template<typename t_value> struct SpecifySmallVec<t_value,2>
	{ typedef num::Vec2<t_value> t_Result; };
	template<typename t_value> struct SpecifySmallVec<t_value,3>
	{ typedef num::Vec3<t_value> t_Result; };
	template<typename t_value> struct SpecifySmallVec<t_value,4>
	{ typedef num::Vec4<t_value> t_Result; };

} // namespace num

#include "..\rt\compact_vector.h"
#include "..\rt\expr_templ.h"

namespace num{

	template<typename T> T clamp(const T&value, const T&minValue, const T&maxValue){

		if(value<minValue) 
			return minValue;
		else if (value>maxValue) 
			return maxValue;
		else
			return value;
	}
	template<typename T> float step(const T& x, const T& y){
		return x<=y?1:0;
	}

	template<typename T> T texture2D(int length1, int length2, float coor1, float coor2, const rt::Buffer<T>* values){
		USING_EXPRESSION;
		coor1 = coor1*length1-0.5;
		int coor1a = floor(coor1);
		int coor1b = coor1a+1;
		float coor_fac1 = coor1-coor1a;
		if(coor1b>=length1) coor1b = length1-1;
		coor2 = coor2*length2-0.5;
		int coor2a = floor(coor2);
		int coor2b = coor2a+1;
		float coor_fac2 = coor2-coor2a;
		if(coor2b>=length1) coor2b = length2-1;
		T value1 = (*values)[coor2a*length1+coor1a];
		T value2 = (*values)[coor2a*length1+coor1b];
		T value3 = (*values)[coor2b*length1+coor1a];
		T value4 = (*values)[coor2b*length1+coor1b];

		T res;
		res = value1*(1-coor_fac1)*(1-coor_fac2)+value2*coor_fac1*(1-coor_fac2)+value3*(1-coor_fac1)*coor_fac2+value4*coor_fac1*coor_fac2;

		return res;
	}

	__forceinline float calerf2(float x){
		float z = fabs(x);
		float t = 1.0/(1.0+0.47047*z);
		float tsq = t*t;
		float tcb = tsq*t;
		float ans = (0.3480242 * t -0.0958798 * tsq + 0.7478556 * tcb ) * exp(-z*z);
		if(x>0)
			return (2.0-ans)-1;
		else
			return ans-1;
	}
}