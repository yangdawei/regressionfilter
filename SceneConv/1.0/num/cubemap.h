#pragma once

//////////////////////////////////////////////////////////////////////
// Visual Computing Foundation Classes (VCFC)
//					Jiaping Wang  2006.2
//					e_boris2002@hotmail.com
//
// (C) Copyright Jiaping Wang 2004.
//
// Permission to copy, use, modify, sell and distribute this software
// is granted provided this copyright notice appears in all copies.
// This software is provided "as is" without express or implied
// warranty, and with no claim as to its suitability for any purpose.
//
// Absolutely no warranty!!
//////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////
//  Brief Description
//
//  cubemap.h
//  representation for cubemap domain
//	cubemap coordinate conversion
//
//////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////
// Revise History
//
// Inital version	2005.11		Jiaping
// Revised			2008.10		Jiaping
// Merge CubeCoord	2008.11		Jiaping
//
//////////////////////////////////////////////////////////////////////

#include "..\w32\file_64.h"
#include "..\num\small_vec.h"


//////////////////////////////////////////////////////////////////////////////////
/*  Right-Hand coordinate

     ------------------                         			+-------+
    /                 /|           y						|       |
   /                 / |           ^						|  0Y+  |
  /       0Y+       /  |           |						|       |
 /                 /   |           |			    +-------+-------+-------+
/                 /    |           |			    |       |       |       |
------------------     |           o ----->x	    |  1X-  |  2Z+  |  3X+  |
|                | 3X+ |          /				    |       |       |       |
|                |     /         /				    +-------+-------+-------+
|                |    /		    /							|       | 
|      2Z+       |   /		   z							|  4Y-  | 
|                |  /										|       | 
|                | /										+-------+
|                |/											|       | 
------------------											|  5Z-  | 
															|       | 
the cube size is 2*2*2										+-------+           
*/
///////////////////////////////////////////////////////////////////////////////////



namespace num
{

namespace _meta_
{
namespace _cube_coord
{
	// Y+,X-,Z+,X+,Y-,Z-
	enum
	{
		FaceId_Y_Pos = 0,
		FaceId_X_Neg,
		FaceId_Z_Pos,
		FaceId_X_Pos,
		FaceId_Y_Neg,
		FaceId_Z_Neg
	};

	static const int   FaceIdIndex_InOpenGL[6] = { FaceId_X_Pos,FaceId_X_Neg,FaceId_Y_Pos,FaceId_Y_Neg,FaceId_Z_Pos,FaceId_Z_Neg};
					
	static const num::Vec3f _pos3D[6] =	{ 
												num::Vec3f(-1, 1,-1),
												num::Vec3f(-1, 1,-1),
												num::Vec3f(-1, 1, 1),
												num::Vec3f( 1, 1, 1),
												num::Vec3f(-1,-1, 1),
												num::Vec3f(-1,-1,-1)
											};
	static const num::Vec3f _dirX[6] =	{	
												num::Vec3f( 1, 0, 0),
												num::Vec3f( 0, 0, 1),
												num::Vec3f( 1, 0, 0),
												num::Vec3f( 0, 0,-1),
												num::Vec3f( 1, 0, 0),
												num::Vec3f( 1, 0, 0)
											};
	static const num::Vec3f _dirY[6] =	{
												num::Vec3f( 0, 0, 1),
												num::Vec3f( 0,-1, 0),
												num::Vec3f( 0,-1, 0),
												num::Vec3f( 0,-1, 0),
												num::Vec3f( 0, 0,-1),
												num::Vec3f( 0, 1, 0)
											};
	static const num::Vec3f _normal[6] = {
												num::Vec3f( 0, 1, 0),
												num::Vec3f(-1, 0, 0),
												num::Vec3f( 0, 0, 1),
												num::Vec3f( 1, 0, 0),
												num::Vec3f( 0,-1, 0),
												num::Vec3f( 0, 0,-1)
											 };
} // namespace _cube_coord
} // namespace _meta_



template< typename t_Val >  // float or double
class CubeCoord:public Vec2< t_Val >
{
public:
	int	FaceId;	// Y+,X-,Z+,X+,Y-,Z-
//		0Y+
//1X-	2Z+		3X+
//		4Y-
//		5Z-

public:
	NUMERIC_FUNC CubeCoord(){}
	NUMERIC_FUNC CubeCoord(int face_id, t_Val x, t_Val y)
		:FaceId(face_id),Vec2< t_Val >(x,y){}
	NUMERIC_FUNC CubeCoord(const CubeCoord& i)
		:FaceId(i.FaceId),Vec2< t_Val >(i.x,i.y){}

	template< typename t_Val2 >
	NUMERIC_FUNC void Cube2Vec(Vec3<t_Val2> &dir_not_normalized) const //not normalized
	{
		dir_not_normalized.x =	num::_meta_::_cube_coord::_pos3D[FaceId].x		+ 
								num::_meta_::_cube_coord::_dirX[FaceId].x*2*x	+ 
								num::_meta_::_cube_coord::_dirY[FaceId].x*2*y;
		dir_not_normalized.y =	num::_meta_::_cube_coord::_pos3D[FaceId].y		+ 
								num::_meta_::_cube_coord::_dirX[FaceId].y*2*x	+ 
								num::_meta_::_cube_coord::_dirY[FaceId].y*2*y;
		dir_not_normalized.z =	num::_meta_::_cube_coord::_pos3D[FaceId].z		+ 
								num::_meta_::_cube_coord::_dirX[FaceId].z*2*x	+ 
								num::_meta_::_cube_coord::_dirY[FaceId].z*2*y;
	}

	template< typename t_Val2 >
	NUMERIC_FUNC void Vec2Cube(const Vec3<t_Val2> &dir_normalized)  //should be normalized
	{
		//float q = fabs(dir.L2Norm_Sqr() - 1);
		float a = rt::TypeTraits<t_Val2>::Epsilon();
		ASSERT(fabs(dir_normalized.L2Norm_Sqr() - 1) < (rt::TypeTraits<t_Val2>::Epsilon()));

		float abs_x,abs_y,abs_z;
		abs_x = fabs(dir_normalized.x);
		abs_y = fabs(dir_normalized.y);
		abs_z = fabs(dir_normalized.z);

		if( abs_x > abs_y )
		{
			if( abs_x > abs_z )
			{// x is max
				FaceId = dir_normalized.x>0?3:1;
			}
			else
			{// z is max
				FaceId = dir_normalized.z>0?2:5;
			}
		}
		else
		{
			if( abs_y > abs_z )
			{// y is max
				FaceId = dir_normalized.y>0?0:4;
			}
			else
			{// z is max
				FaceId = dir_normalized.z>0?2:5;
			}
		}

		t_Val FaceDotInv = 1/dir_normalized.Dot(num::_meta_::_cube_coord::_normal[FaceId]);
		num::Vec3<t_Val> dirOnFace;

		dirOnFace.x = dir_normalized.x*FaceDotInv - num::_meta_::_cube_coord::_pos3D[FaceId].x;
		dirOnFace.y = dir_normalized.y*FaceDotInv - num::_meta_::_cube_coord::_pos3D[FaceId].y;
		dirOnFace.z = dir_normalized.z*FaceDotInv - num::_meta_::_cube_coord::_pos3D[FaceId].z;

		x = dirOnFace.Dot(num::_meta_::_cube_coord::_dirX[FaceId])/2;
		y = dirOnFace.Dot(num::_meta_::_cube_coord::_dirY[FaceId])/2;
	}

	template< typename t_Val2 >
	NUMERIC_FUNC void Vec2Cube(const Vec3<t_Val2> &dir_normalized,int FaceId)  //should be normalized
	{
		t_Val FaceDotInv = 1/dir_normalized.Dot(num::_meta_::_cube_coord::_normal[FaceId]);

		num::Vec3<t_Val> dirOnFace;
		dirOnFace.x = dir.x*FaceDotInv - num::_meta_::_cube_coord::_pos3D[FaceId].x;
		dirOnFace.y = dir.y*FaceDotInv - num::_meta_::_cube_coord::_pos3D[FaceId].y;
		dirOnFace.z = dir.z*FaceDotInv - num::_meta_::_cube_coord::_pos3D[FaceId].z;

		x = dirOnFace.Dot(num::_meta_::_cube_coord::_dirX[FaceId])/2;
		y = dirOnFace.Dot(num::_meta_::_cube_coord::_dirY[FaceId])/2;
	}

	static const num::Vec3f& GetFaceNormal(){ return num::_meta_::_cube_coord::_normal[FaceId]; }
};
template<class t_Ostream, typename t_Val>
t_Ostream& operator<<(t_Ostream& Ostream, const ::num::CubeCoord<t_Val> & c)
{
	Ostream<<"(F"<<c.FaceId<<'|'<<std::setw(5)<<c.x<<','<<std::setw(5)<<c.y<<')';
	return Ostream;
}


template<class t_Image = ipp::CImage_3c32f>
class CCubeMap
{
	typedef typename t_Image::Ref			t_ImageRef;
	typedef typename t_Image::PixelType		t_Pixel;

protected:
	t_Image				m_CubeImage;

public:
	CCubeMap(){}
	BOOL				SetFaceSize(UINT FaceSize){ return m_CubeImage.SetSize(FaceSize,FaceSize*6); }
	int					GetFaceSize()const{ return m_CubeImage.GetWidth(); }
	
	t_ImageRef&			GetTiledImage(){ return m_CubeImage; }
	const t_ImageRef&	GetTiledImage()const{ return m_CubeImage; }
	t_ImageRef			GetFace(UINT face_id){ ASSERT(face_id<6); return m_CubeImage.GetSub(0,m_CubeImage.GetHeight()/6*face_id, m_CubeImage.GetWidth(), m_CubeImage.GetWidth()); }
	const t_ImageRef	GetFace(UINT face_id)const { ASSERT(face_id<6); return m_CubeImage.GetSub(0,m_CubeImage.GetHeight()/6*face_id, m_CubeImage.GetWidth(), m_CubeImage.GetWidth()); }
	t_Pixel&			GetPixel(int face, int x, int y){ return m_CubeImage(x,face*m_CubeImage.GetWidth()+y); }
	const t_Pixel&		GetPixel(int face, int x, int y)const { return m_CubeImage(x,face*m_CubeImage.GetWidth()+y); }

	void				PropogateFaceEdge(const int maxIter = 20)
	{
		ASSERT(maxIter>=0);
		int	fsize = int(m_CubeImage.GetWidth() );
		if(fsize>1)
		{
			// get cube with areraged edge and corner
			CCubeMap<t_Image>	cube;
			cube	= *this;
			// fix 8-corner
#define	average_corner(x1,y1,x2,y2,x3,y3)	cube.m_CubeImage(x1,y1)+=cube.m_CubeImage(x2,y2);cube.m_CubeImage(x1,y1)+=cube.m_CubeImage(x3,y3);cube.m_CubeImage(x1,y1)/=3;cube.m_CubeImage(x2,y2)=cube.m_CubeImage(x1,y1);cube.m_CubeImage(x3,y3)=cube.m_CubeImage(x1,y1)
			// Face 0, UpLeft:		Face 1, UpLeft:		Face 5, DownLeft
			average_corner(0,0,0,fsize,0,fsize*6-1);
			// Face 0, DownLeft:	Face 1, UpRight:	Face 2, UpLeft
			average_corner(0,fsize-1,fsize-1,fsize,0,fsize*2);
			// Face 0, DownRight:	Face 2, UpRight:	Face 3, UpLeft
			average_corner(fsize-1,fsize-1,fsize-1,fsize*2,0,fsize*3);
			// Face 0, UpRight:		Face 3, UpRight:	Face 5, DownRight
			average_corner(fsize-1,0,fsize-1,fsize*3,fsize-1,fsize*6-1);
			// Face 4, UpLeft:		Face 1, DownRight:	Face 2, DownLeft
			average_corner(0,fsize*4,fsize-1,fsize*2-1,0,fsize*3-1);
			// Face 4, DownLeft:	Face 1, DownLeft:	Face 5, UpRight
			average_corner(0,fsize*5-1,0,fsize*2-1,0,fsize*5);
			// Face 4, DownRight:	Face 3, DownRight:	Face 5, UpRight
			average_corner(fsize-1,fsize*5-1,fsize-1,fsize*4-1,fsize-1,fsize*5);
			// Face 4, UpRight:		Face 2, DownRight:	Face 3, DownLeft
			average_corner(fsize-1,fsize*4,fsize-1,fsize*3-1,0,fsize*4-1);
#undef	average_corner
			if(fsize>2)
			{
				// fix 12-edge
#define	average_edge(x1,y1,x2,y2)	cube.m_CubeImage(x1,y1)+=cube.m_CubeImage(x2,y2);cube.m_CubeImage(x1,y1)/=2;cube.m_CubeImage(x2,y2)=cube.m_CubeImage(x1,y1)
				for (int i = 1; i < fsize-1; i ++)
				{
					// Face 0, Y-: Face 5, Y+
					average_edge(i,0,i,fsize*6-1);
					// Face 0, X-: Face 1, Y-
					average_edge(0,i,i,fsize);
					// Face 0, Y+: Face 2, Y-
					average_edge(i,fsize-1,i,fsize*2);
					// Face 0, X+: Face 3, Y-
					average_edge(fsize-1,i,fsize-1-i,fsize*3);
					// Face 4, Y-: Face 2, Y+
					average_edge(i,fsize*4,i,fsize*3-1);
					// Face 4, X-: Face 1, Y+
					average_edge(0,fsize*4+i,fsize-1-i,fsize*2-1);
					// Face 4, Y+: Face 5, Y-
					average_edge(i,fsize*5-1,i,fsize*5);
					// Face 4, X+: Face 3, Y+
					average_edge(fsize-1,fsize*4+i,i,fsize*4-1);
					// Face 1, X-: Face 5, X-
					average_edge(0,fsize+i,0,fsize*6-1-i);
					// Face 1, X+: Face 2, X-
					average_edge(fsize-1,fsize+i,0,fsize*2+i);
					// Face 3, X-: Face 2, X+
					average_edge(0,fsize*3+i,fsize-1,fsize*2+i);
					// Face 3, X+: Face 4, X+
					average_edge(fsize-1,fsize*3+i,fsize-1,fsize*6-1-i);
				}
#undef	average_edge
			}
			CCubeMap<t_Image>	cubeError;
			cubeError = cube;
			for (int face = 0; face < 6; face ++)
			for (int y = 0; y < fsize; y ++)
			for (int x = 0; x < fsize; x ++)
				cubeError.GetPixel(face,x,y) -= (*this).GetPixel(face,x,y);
			CCubeMap<t_Image>	cubeErrorList[2];
			cubeErrorList[0].SetFaceSize(fsize);
			cubeErrorList[1].SetFaceSize(fsize);
			for (int face = 0; face < 6; face ++)
			for (int y = 0; y < fsize; y ++)
			for (int x = 0; x < fsize; x ++)
				cubeErrorList[0].GetPixel(face,x,y) = 0;
			// propogate error
			for (int face = 0; face < 6; face ++)
			{
				const double	epsiron = 0.000000001;
				num::Vec3d	edgeErrorSum;
				edgeErrorSum.r = 0;
				edgeErrorSum.g = 0;
				edgeErrorSum.b = 0;
				int		index = 0;
				int		iter = 0;
				do
				{
					for (int i = 0; i < fsize; i ++)
					{
						num::Vec3d tmp;

						tmp = cubeErrorList[index].GetFace(face)(i,0);
						tmp -= cubeError.GetFace(face)(i,0);
						tmp *= tmp;
						edgeErrorSum += tmp;
						tmp = cubeErrorList[index].GetFace(face)(i,fsize-1);
						tmp -= cubeError.GetFace(face)(i,fsize-1);
						tmp *= tmp;
						edgeErrorSum += tmp;
						tmp = cubeErrorList[index].GetFace(face)(0,i);
						tmp -= cubeError.GetFace(face)(0,i);
						tmp *= tmp;
						edgeErrorSum += tmp;
						tmp = cubeErrorList[index].GetFace(face)(fsize-1,i);
						tmp -= cubeError.GetFace(face)(fsize-1,i);
						tmp *= tmp;
						edgeErrorSum += tmp;
					}
					edgeErrorSum /= 4*fsize;
					
					for (int i = 0; i < fsize; i ++)
					{
						cubeErrorList[index].GetFace(face)(i,0) = cubeError.GetFace(face)(i,0);
						cubeErrorList[index].GetFace(face)(i,fsize-1) = cubeError.GetFace(face)(i,fsize-1);
						cubeErrorList[index].GetFace(face)(0,i) = cubeError.GetFace(face)(0,i);
						cubeErrorList[index].GetFace(face)(fsize-1,i) = cubeError.GetFace(face)(fsize-1,i);
					}

					if (edgeErrorSum.r > epsiron || edgeErrorSum.g > epsiron || edgeErrorSum.b > epsiron)
					{
						
						cubeErrorList[index].GetFace(face).Gauss_3x3(cubeErrorList[(index+1)%2].GetFace(face) );
						index = (index+1)%2;
						iter ++;
					}
				} while((edgeErrorSum.r > epsiron || edgeErrorSum.g > epsiron || edgeErrorSum.b > epsiron) && iter < maxIter);
				for (int y = 0; y < fsize; y ++)
				for (int x = 0; x < fsize; x ++)
					(*this).GetPixel(face,x,y) += cubeErrorList[index].GetPixel(face,x,y);
			}
		}
		else
		{// sum all
			for (int i = 1; i < 6; i ++)
				m_CubeImage.GetPixel(0,0) += m_CubeImage.GetPixel(0,i);
			m_CubeImage.GetPixel(0,0) /= 6;
			for (int i = 1; i < 6; i ++)
				m_CubeImage.GetPixel(0,i) = m_CubeImage.GetPixel(0,0);
		}
	}

	void				MergeFaceEdge()// arerage edge and corner
	{	
		int	fsize = int(m_CubeImage.GetWidth() );
		if(fsize>1)
		{
			// fix 8-corner
#define	average_corner(x1,y1,x2,y2,x3,y3)	m_CubeImage(x1,y1)+=m_CubeImage(x2,y2);m_CubeImage(x1,y1)+=m_CubeImage(x3,y3);m_CubeImage(x1,y1)/=3;m_CubeImage(x2,y2)=m_CubeImage(x1,y1);m_CubeImage(x3,y3)=m_CubeImage(x1,y1)
			// Face 0, UpLeft:		Face 1, UpLeft:		Face 5, DownLeft
			average_corner(0,0,0,fsize,0,fsize*6-1);
			// Face 0, DownLeft:	Face 1, UpRight:	Face 2, UpLeft
			average_corner(0,fsize-1,fsize-1,fsize,0,fsize*2);
			// Face 0, DownRight:	Face 2, UpRight:	Face 3, UpLeft
			average_corner(fsize-1,fsize-1,fsize-1,fsize*2,0,fsize*3);
			// Face 0, UpRight:		Face 3, UpRight:	Face 5, DownRight
			average_corner(fsize-1,0,fsize-1,fsize*3,fsize-1,fsize*6-1);
			// Face 4, UpLeft:		Face 1, DownRight:	Face 2, DownLeft
			average_corner(0,fsize*4,fsize-1,fsize*2-1,0,fsize*3-1);
			// Face 4, DownLeft:	Face 1, DownLeft:	Face 5, UpRight
			average_corner(0,fsize*5-1,0,fsize*2-1,0,fsize*5);
			// Face 4, DownRight:	Face 3, DownRight:	Face 5, UpRight
			average_corner(fsize-1,fsize*5-1,fsize-1,fsize*4-1,fsize-1,fsize*5);
			// Face 4, UpRight:		Face 2, DownRight:	Face 3, DownLeft
			average_corner(fsize-1,fsize*4,fsize-1,fsize*3-1,0,fsize*4-1);			
#undef	average_corner
			if(fsize>2)
			{
				// fix 12-edge
#define	average_edge(x1,y1,x2,y2)	m_CubeImage(x1,y1)+=m_CubeImage(x2,y2);m_CubeImage(x1,y1)/=2;m_CubeImage(x2,y2)=m_CubeImage(x1,y1)
	
				for (int i = 1; i < fsize-1; i ++)
				{
					// Face 0, Y-: Face 5, Y+
					average_edge(i,0,i,fsize*6-1);
					// Face 0, X-: Face 1, Y-
					average_edge(0,i,i,fsize);
					// Face 0, Y+: Face 2, Y-
					average_edge(i,fsize-1,i,fsize*2);
					// Face 0, X+: Face 3, Y-
					average_edge(fsize-1,i,fsize-1-i,fsize*3);
					// Face 4, Y-: Face 2, Y+
					average_edge(i,fsize*4,i,fsize*3-1);
					// Face 4, X-: Face 1, Y+
					average_edge(0,fsize*4+i,fsize-1-i,fsize*2-1);
					// Face 4, Y+: Face 5, Y-
					average_edge(i,fsize*5-1,i,fsize*5);
					// Face 4, X+: Face 3, Y+
					average_edge(fsize-1,fsize*4+i,i,fsize*4-1);
					// Face 1, X-: Face 5, X-
					average_edge(0,fsize+i,0,fsize*6-1-i);
					// Face 1, X+: Face 2, X-
					average_edge(fsize-1,fsize+i,0,fsize*2+i);
					// Face 3, X-: Face 2, X+
					average_edge(0,fsize*3+i,fsize-1,fsize*2+i);
					// Face 3, X+: Face 4, X+
					average_edge(fsize-1,fsize*3+i,fsize-1,fsize*6-1-i);
				}
#undef	average_edge
			}
		}
		else
		{// sum all
			for (int i = 1; i < 6; i ++)
				m_CubeImage.GetPixel(0,0) += m_CubeImage.GetPixel(0,i);
			m_CubeImage.GetPixel(0,0) /= 6;
			for (int i = 1; i < 6; i ++)
				m_CubeImage.GetPixel(0,i) = m_CubeImage.GetPixel(0,0);
		}
	}

	template<typename t_Val>
	t_Pixel				GetInterpolatedPixel(const Vec3<t_Val> & direction_normalized) const
	{	t_Pixel v;
		num::CubeCoord<float>	cube;
		cube.Vec2Cube(direction_normalized);
		cube.x = min(cube.x,1-EPSILON);
		cube.y = min(cube.y,1-EPSILON);
		GetFace(cube.FaceId).GetInterpolatedPixel(cube.x,cube.y,v);
		return v;
	}

	template<class t_Buf>
	HRESULT Save(t_Buf& data, UINT format /*ipp::ImageCodec_PNG*/) const{ return m_CubeImage.Save(data,format); }
	HRESULT Load(LPCVOID data, UINT len_byte){ return m_CubeImage.Load(data,len_byte); }


	template<class t_w32_file>
	HRESULT	Load(t_w32_file & file){ return m_CubeImage.Load(file); }

	template<class t_w32_file>
	HRESULT	Save(t_w32_file & file) const { return m_CubeImage.Save(file);	}

	BOOL	Load(LPCTSTR fn)
	{	t_Image raw;
		int fs;
		if(	SUCCEEDED(raw.Load(fn)) &&
			raw.GetWidth()/3 == (fs = raw.GetHeight()/4) &&
			SetFaceSize(fs) )
		{
			raw.GetSub(fs,  0   ,fs,fs).CopyTo(m_CubeImage.GetSub(0,0,   fs,fs));
			raw.GetSub(0 ,  fs  ,fs,fs).CopyTo(m_CubeImage.GetSub(0,fs,  fs,fs));
			raw.GetSub(fs*1,fs  ,fs,fs).CopyTo(m_CubeImage.GetSub(0,fs*2,fs,fs));
			raw.GetSub(fs*2,fs  ,fs,fs).CopyTo(m_CubeImage.GetSub(0,fs*3,fs,fs));
			raw.GetSub(fs,  fs*2,fs,fs).CopyTo(m_CubeImage.GetSub(0,fs*4,fs,fs));
			raw.GetSub(fs,  fs*3,fs,fs).CopyTo(m_CubeImage.GetSub(0,fs*5,fs,fs));

			return TRUE;
		}
		return FALSE;
	}

	BOOL Save(LPCTSTR fn)
	{	t_Image raw;
		raw.Set(t_Image::PixelType(0));
		int fs = GetFaceSize();
		if(raw.SetSize(fs*3,fs*4))
		{
			m_CubeImage.GetSub(0,0,   fs,fs).CopyTo(raw.GetSub(fs,  0   ,fs,fs));
			m_CubeImage.GetSub(0,fs,  fs,fs).CopyTo(raw.GetSub(0 ,  fs  ,fs,fs));
			m_CubeImage.GetSub(0,fs*2,fs,fs).CopyTo(raw.GetSub(fs*1,fs  ,fs,fs));
			m_CubeImage.GetSub(0,fs*3,fs,fs).CopyTo(raw.GetSub(fs*2,fs  ,fs,fs));
			m_CubeImage.GetSub(0,fs*4,fs,fs).CopyTo(raw.GetSub(fs,  fs*2,fs,fs));
			m_CubeImage.GetSub(0,fs*5,fs,fs).CopyTo(raw.GetSub(fs,  fs*3,fs,fs));
			return SUCCEEDED(raw.Save(fn));
		}
		return FALSE;
	}

	void SphereVisualize(LPCTSTR fn, UINT size)
	{
		ASSERT(size);
		t_Image raw;
		raw.SetSize(size, size);
		raw.Set(t_Image::PixelType(0));

		num::Vec3f dir;
		float r = size / 2.0f;
		float r0;
		for(UINT i=0; i<size; ++i)
		for(UINT j=0; j<size; ++j)
		{
			dir.x = (i - r)/r;
			dir.y = (j - r)/r;
			r0 = rt::Sqr(dir.x) + rt::Sqr(dir.y);
			if( r0 > 0.99)
			{
				raw(i,j) = 50*(min(1.01, r0) - 0.99);
			}
			else
			{
    			dir.z = sqrt( 1.0f - rt::Sqr(dir.x) - rt::Sqr(dir.y) );
    			raw(i,j) = GetInterpolatedPixel(dir);
			}
		}
		raw.Save(fn);
	}

	const CCubeMap& operator = (const CCubeMap& in)
	{
		return operator = <t_Image>(in);
	}

	template<class t_Image2>
	const CCubeMap<t_Image2>& operator = (const CCubeMap<t_Image2>& in)
	{	
		VERIFY(SetFaceSize(in.GetFaceSize()));
		m_CubeImage = in.GetTiledImage();
		return in;
	}

	template<class t_Image2>
	CCubeMap(const CCubeMap<t_Image2>& in){ *this = in; }
	CCubeMap(const CCubeMap& in){ *this = in; }
};


}

