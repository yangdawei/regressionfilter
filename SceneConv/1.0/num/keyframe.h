#pragma once
//////////////////////////////////////////////////////////////////////
// Visual Computing Foundation Classes (VCFC)
//					Jiaping Wang  2006.2
//					e_boris2002@hotmail.com
//
// (C) Copyright Jiaping Wang 2004.
//
// Permission to copy, use, modify, sell and distribute this software
// is granted provided this copyright notice appears in all copies.
// This software is provided "as is" without express or implied
// warranty, and with no claim as to its suitability for any purpose.
//
// Absolutly no garanty!!
//////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////
//  Brief Description
//
//  keyframe.h
//
//  interpolation of scene parameters: matrix and vectors
//
//////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////
// Revise History
//
// Inital version	2007.11		Shuang Zhao
// Revised			2010.11		Shuang Zhao
//
//////////////////////////////////////////////////////////////////////


#include <functional>
#include <set>
#include <map>
#include "small_vec.h"
#include "small_mat.h"
#include "quaternion.h"
#include "irreg_func.h"
#include "..\rt\compact_vector.h"

namespace num
{

namespace _meta_
{

template <typename t_Key, typename t_Value, typename t_Comp = double>
class Mat4x4Interpolator
{
protected:
    num::QuaternionInterpolator<t_Key, t_Value> m_qi;
    num::LinearInterpolator<t_Key, t_Value> m_scale[3];
    num::LinearInterpolator<t_Key, t_Value> m_translate[3];

public:
    Mat4x4Interpolator()
    {
    }

    void Clear()
    {
        int i;

        m_qi.Clear();
        for ( i = 0; i < 3; ++i )
        {
            m_scale[i].Clear();
            m_translate[i].Clear();
        }
    }

    void Remove(float x)
    {
        int i;

        m_qi.Remove(x);
        for ( i = 0; i < 3; ++i )
        {
            m_scale[i].Remove(x);
            m_translate[i].Remove(x);
        }
    }

    void Set(t_Key x, const num::Mat4x4<t_Value> &m0)
    {
        num::Mat4x4<t_Comp> m;
        int i, j;

        m = m0;
        t_Comp div = (t_Comp)m.m33;
        ASSERT( div > rt::TypeTraits<t_Comp>::Epsilon() );
        for ( i = 0; i < 4; ++i )
            for ( j = 0; j < 4; ++j )
                m.m[i][j] /= div;

        num::Mat3x3<t_Value> mat;
        num::Vec3<t_Value> scale;
        num::Quaternion<t_Value> q;

        mat = m;
        q.ImportRotationMatrix(mat, scale);

        m_qi.Set(x, q);
        m_scale[0].Set(x, scale.x);
        m_scale[1].Set(x, scale.y);
        m_scale[2].Set(x, scale.z);
        m_translate[0].Set(x, (t_Value)m.m03);
        m_translate[1].Set(x, (t_Value)m.m13);
        m_translate[2].Set(x, (t_Value)m.m23);
    }

    void Query(t_Key x, num::Mat4x4<t_Value> &m) const
    {
        num::Mat3x3<t_Value> mat;
        num::Vec3<t_Value> scale;
        num::Quaternion<t_Value> q;

        m_qi.Query(x, q);
        m_scale[0].Query(x, scale.x);
        m_scale[1].Query(x, scale.y);
        m_scale[2].Query(x, scale.z);
        q.ExportRotationMatrix(mat, scale);

        memset(m.m, 0, sizeof(m.m));
        m = mat;
        m_translate[0].Query(x, m.m03);
        m_translate[1].Query(x, m.m13);
        m_translate[2].Query(x, m.m23);

        m.m33 = (t_Value)1.0;
    }
};

typedef Mat4x4Interpolator<float, float> Mat4x4Interpolatorf;

}  // namespace _meta_
}  // namespace num

namespace num
{

template <typename t_Value>
struct Frame_Data
{
    Frame_Data()
    {
        m_time = (t_Value)0.0;
        SetSize(0, 0, 0);
    }

    Frame_Data(t_Value time, int Mat4x4_Count, int Vec3_Count, int Value_Count)
    {
        m_time = time;
        SetSize(Mat4x4_Count, Vec3_Count, Value_Count);
    }

    Frame_Data(const Frame_Data<t_Value> &k)
    {
        m_time = k.m_time;
        m_Buffered_Mat4x4.SetSizeAs(k.m_Buffered_Mat4x4);
        m_Buffered_Mat4x4.CopyFrom(k.m_Buffered_Mat4x4);
        m_Buffered_Vec3.SetSizeAs(k.m_Buffered_Vec3);
        m_Buffered_Vec3.CopyFrom(k.m_Buffered_Vec3);
        m_Buffered_Value.SetSizeAs(k.m_Buffered_Value);
        m_Buffered_Value.CopyFrom(k.m_Buffered_Value);
    }

    void SetSize(int Mat4x4_Count, int Vec3_Count, int Value_Count)
    {
        m_Buffered_Mat4x4.SetSize(Mat4x4_Count);
        m_Buffered_Vec3.SetSize(Vec3_Count);
        m_Buffered_Value.SetSize(Value_Count);
    }

    const Frame_Data<t_Value>& operator = (const Frame_Data<t_Value> &k)
    {
        m_time = k.m_time;
        m_Buffered_Mat4x4.SetSizeAs(k.m_Buffered_Mat4x4);
        m_Buffered_Mat4x4.CopyFrom(k.m_Buffered_Mat4x4);
        m_Buffered_Vec3.SetSizeAs(k.m_Buffered_Vec3);
        m_Buffered_Vec3.CopyFrom(k.m_Buffered_Vec3);
        m_Buffered_Value.SetSizeAs(k.m_Buffered_Value);
        m_Buffered_Value.CopyFrom(k.m_Buffered_Value);

        return *this;
    }

    t_Value m_time;

    rt::Buffer<num::Mat4x4<t_Value> > m_Buffered_Mat4x4;
    rt::Buffer<num::Vec3<t_Value> > m_Buffered_Vec3;
    rt::Buffer<t_Value> m_Buffered_Value;
};

typedef Frame_Data<float> Frame;


class KeyframeControl
{
public:
    static const int TRG_MAT4x4                 = 0;
    static const int TRG_VEC3                   = 1;
    static const int TRG_VALUE                  = 2;
    static const int INT_MAT4x4_QUATERNION      = 100;
    static const int INT_LINEAR                 = 201;
    static const int INT_CUBIC                  = 202;

protected:
    struct Less : public std::binary_function<float, float, bool>
    {
        bool operator()(const float& _Left, const float& _Right) const
        {
            return ( _Right - _Left > rt::TypeTraits<float>::Epsilon() );
        }
    } less;

	//struct LessEqu : public std::binary_function<float, float, bool>
	//{
 //       bool operator()(const float& _Left, const float& _Right) const
 //       {
 //           return ( _Right - _Left > -rt::TypeTraits<float>::Epsilon() );
 //       }
	//};
	//static LessEqu lessequ;

    struct Keyframe_Data;
    typedef std::map<float, Keyframe_Data*, Less> MAP;

    struct Keyframe_Data
    {
        Keyframe_Data(const Frame &kf, MAP::iterator &it)
        {
            data = kf;
            pos = it;
        }

        Frame data;
        MAP::iterator pos;
    };

public:
    class KeyframePtr
    {
        friend class KeyframeControl;

    protected:
        KeyframeControl *parent;
        Keyframe_Data *value;

    public:
        KeyframePtr();
        KeyframePtr(const KeyframePtr& kfp);
        float GetTime() const;
		Frame& GetFrame();
        void GetFrame(Frame &frame) const;

        const KeyframePtr& operator = (const KeyframePtr &kfp);
        const KeyframePtr& operator++ ();
        KeyframePtr operator++ (int);
        const KeyframePtr& operator-- ();
        KeyframePtr operator-- (int);
        bool operator == (const KeyframePtr &kfp) const;
        operator WPARAM() const;

        bool IsFirst() const;
        bool IsLast() const;
    };

protected:
	// do not allow copy construction
	KeyframeControl(const KeyframeControl *kfc);
	const KeyframeControl& operator = (const KeyframeControl& kfc);

    bool add(const Frame &keyframe, KeyframePtr &ptr, Keyframe_Data *data);
    void remove(const KeyframePtr &ptr, bool remove_data);

    MAP m_keyframe;

    rt::Buffer<int> m_Buffered_Mat4x4_IntMethod;
    rt::Buffer<int> m_Buffered_Vec3_IntMethod;
    rt::Buffer<int> m_Buffered_Value_IntMethod;

    rt::Buffer<num::Vec2i> m_Buffered_Mat4x4_Offset;
    rt::Buffer<num::Vec2i> m_Buffered_Vec3_Offset;
    rt::Buffer<num::Vec2i> m_Buffered_Value_Offset;

    rt::Buffer<_meta_::Mat4x4Interpolatorf> m_Buffered_Mat4x4Int;
    rt::Buffer<num::CubicInterpolatorf> m_Buffered_CubicInt;
    rt::Buffer<num::LinearInterpolatorf> m_Buffered_LinearInt;

    int m_Mat4x4_Count, m_Vec3_Count, m_Value_Count;
    float m_FirstTime, m_LastTime;

public:
    KeyframeControl();
    ~KeyframeControl();
    void SetKeyframeFormat(int Mat4x4_Count, int Vec3_Count, int Value_Count, bool fill = true);
    void SetInterpolateMethod(int trg, int pos, int method);
    //bool GetKeyframe(const KeyframePtr &ptr, Frame &kf) const;
	bool AddKeyframe(const Frame &keyframe);
    bool AddKeyframe(const Frame &keyframe, KeyframePtr &ptr);
    void RemoveKeyframe(const KeyframePtr &ptr);
    void UpdateKeyframe(const KeyframePtr &ptr, const Frame &keyframe);
    bool GetFrame(float time, Frame &frame) const;

    KeyframePtr GetFirstKeyframe();
    KeyframePtr GetLastKeyframe();
    KeyframePtr GetKeyframe(WPARAM handle);		// Get a keyframe by a handle generated by KeyframePtr::WPARAM().

    __inline UINT GetKeyframeCount() const { return static_cast<UINT>(m_keyframe.size());	}
    __inline UINT GetMat4x4Count() const   { return static_cast<UINT>(m_Mat4x4_Count);		}
    __inline UINT GetVec3Count() const     { return static_cast<UINT>(m_Vec3_Count);		}
    __inline UINT GetValueCount() const    { return static_cast<UINT>(m_Value_Count);		}
};

} // namespace num