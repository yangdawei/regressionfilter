#pragma once

//////////////////////////////////////////////////////////////////////
// Visual Computing Foundation Classes (VCFC)
//					Jiaping Wang  2006.2
//					e_boris2002@hotmail.com
//
// (C) Copyright Jiaping Wang 2004.
//
// Permission to copy, use, modify, sell and distribute this software
// is granted provided this copyright notice appears in all copies.
// This software is provided "as is" without express or implied
// warranty, and with no claim as to its suitability for any purpose.
//
// Absolutely no warranty!!
//////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////
//  Brief Description
//
//  fpu_abs.h
//
//  instantiations of rt::Vec for small vectors
//
//////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////
// Revise History
//
// Inital version	2006.6.8		Jiaping
//
//////////////////////////////////////////////////////////////////////

#include <float.h>
#include "..\w32\win32_ver.h"
#include "..\rt\runtime_base.h"

#ifndef NUMERIC_FUNC
#	define NUMERIC_FUNC __forceinline
#endif

namespace num
{

////////////////////////////////////////////////////
// Special float values
NUMERIC_FUNC BOOL IsPositiveInfinity	(float val){ return ((DWORD&)val)==0x7f800000; }
NUMERIC_FUNC BOOL IsNegativeInfinity	(float val){ return ((DWORD&)val)==0xff800000; }
NUMERIC_FUNC BOOL IsNotANumber			(float val){ return ((DWORD&)val)==0x7fffffff; }
NUMERIC_FUNC BOOL IsNumberOk			(float val){ return ::rt::IsInRange_CC(_fpclass(val),_FPCLASS_NN,_FPCLASS_PN); }
NUMERIC_FUNC BOOL IsPositiveInfinity	(double val){ return ((ULONGLONG&)val)==0x7ff0000000000000; }
NUMERIC_FUNC BOOL IsNegativeInfinity	(double val){ return ((ULONGLONG&)val)==0xfff0000000000000; }
NUMERIC_FUNC BOOL IsNotANumber			(double val){ return ((ULONGLONG&)val)==0x7fffffffffffffff; }
NUMERIC_FUNC BOOL IsNumberOk			(double val){ return ::rt::IsInRange_CC(_fpclass(val),_FPCLASS_NN,_FPCLASS_PN); }
NUMERIC_FUNC void MakePositiveInfinity	(float& val){ ((DWORD&)val)=0x7f800000; }
NUMERIC_FUNC void MakeNegativeInfinity	(float& val){ ((DWORD&)val)=0xff800000; }
NUMERIC_FUNC void MakeNotANumber		(float& val){ ((DWORD&)val)=0x7fffffff; }
NUMERIC_FUNC void MakePositiveInfinity	(double& val){ ((ULONGLONG&)val)=0x7ff0000000000000; }
NUMERIC_FUNC void MakeNegativeInfinity	(double& val){ ((ULONGLONG&)val)=0xfff0000000000000; }
NUMERIC_FUNC void MakeNotANumber		(double& val){ ((ULONGLONG&)val)=0x7fffffffffffffff; }


////////////////////////////////////////////////////
// copy sign of floating-point
NUMERIC_FUNC void CopySign(double&x,double val_sign){ x = ::_copysign(x,val_sign); }
NUMERIC_FUNC void CopySign(float&x,float val_sign){ x = (float)::_copysign(x,val_sign); }

////////////////////////////////////////////////////
// rounding control of fpu
NUMERIC_FUNC void SetRounding_Near(){ _controlfp(_RC_NEAR,_MCW_RC); }
NUMERIC_FUNC void SetRounding_Chop(){ _controlfp(_RC_CHOP,_MCW_RC); }

////////////////////////////////////////////////////
// precision control of fpu
NUMERIC_FUNC void SetPrecision_24bit(){ _controlfp(_PC_24,_MCW_PC); }
NUMERIC_FUNC void SetPrecision_53bit(){ _controlfp(_PC_53,_MCW_PC); }
NUMERIC_FUNC void SetPrecision_64bit(){ _controlfp(_PC_64,_MCW_PC); }


}

