#pragma once

//////////////////////////////////////////////////////////////////////
// Visual Computing Foundation Classes (VCFC)
//					Jiaping Wang  2006.2
//					e_boris2002@hotmail.com
//
// (C) Copyright Jiaping Wang 2004.
//
// Permission to copy, use, modify, sell and distribute this software
// is granted provided this copyright notice appears in all copies.
// This software is provided "as is" without express or implied
// warranty, and with no claim as to its suitability for any purpose.
//
// Absolutly no garanty!!
//////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////
//  Brief Description
//
//  ArcBall.h
//
//  Quaternion-based ArcBall
//
//////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////
// Revise History
//
// Inital version	2006.12		Shuang Zhao
// Revised			2008.10		Jiaping Wang
//
//////////////////////////////////////////////////////////////////////

#include "quaternion.h"
#include "small_mat.h"

namespace num
{

template <typename t_Val>
class ArcBall
{
    num::Vec3<t_Val>		sphereStart, sphereEnd;
    num::Quaternion<t_Val>	cubeQuat, cubeQuatStart;
    num::Quaternion<t_Val>	mouseQuat;

protected:

    int m_lx, m_ly, m_ux, m_uy;

    num::Vec2<t_Val> m_Center;
	num::Mat4x4<t_Val> m_Rotation;
    BOOL m_bDrag;

    void transCoord(const num::Vec2i &p, num::Vec2<t_Val> &res)
    {
        res.x = (t_Val)((p.x - m_Center.x) / (0.5 * (m_ux - m_lx)));
        res.y = -(t_Val)((p.y - m_Center.y) / (0.5 * (m_uy - m_ly)));
    }

    void getSpherePoint(const num::Vec2<t_Val> &p, num::Vec3<t_Val> &res)
    {
        t_Val r = p.x * p.x + p.y * p.y;
        res.x = p.x;
        res.y = p.y;

        if ( r - (t_Val)1.0 > -rt::TypeTraits<t_Val>::Epsilon() )
        {
            t_Val sr = (t_Val)sqrt(r);
            res.x /= sr;
            res.y /= sr;
            res.z = (t_Val)0.0;
        }
        else
            res.z = (t_Val)sqrt(1.0 - r);
    }

    void getQuat(const num::Vec3<t_Val> &p, const num::Vec3<t_Val> &q, num::Quaternion<t_Val> &res)
    {
        res.Quat_Scal() = p.Dot(q);
        res.Quat_Vec().CrossProduct(p, q);
    }

public:

	const num::Quaternion<t_Val>& operator = (const num::Quaternion<t_Val>& x)
	{
		cubeQuat = x;
		cubeQuat.ExportRotationMatrix(m_Rotation);
		return x;
	}

	operator const num::Quaternion<t_Val> () const
	{
		return cubeQuat;
	}

	operator num::Quaternion<t_Val> ()
	{
		return cubeQuat;
	}

    ArcBall()
    {
        m_lx = m_ly = m_ux = m_uy = 0;
        m_Center.Zero();
        m_bDrag = false;

        Initialize();
    }

    void SetWindowSize(int lx, int ly, int ux, int uy)
    {
		m_bDrag = FALSE;  // cancel dragging
        ASSERT( lx <= ux && ly <= uy );

        m_lx = lx;
        m_ly = ly;
        m_ux = ( lx == ux ? ux + 1 : ux );
        m_uy = ( ly == uy ? uy + 1 : uy );

        m_Center.x = (t_Val)(0.5 * (lx + ux));
        m_Center.y = (t_Val)(0.5 * (ly + uy));
    }

    void Initialize()
    {
        mouseQuat.Identity();
        cubeQuat.Identity();
        cubeQuatStart.Identity();
        sphereStart.Zero();
        sphereEnd.Zero();

		m_Rotation.Zero();
        m_Rotation(3, 3) = (t_Val)1.0;
        cubeQuat.ExportRotationMatrix(m_Rotation);
    }

    void OnMouseDown(const num::Vec2i &p)
    {
        num::Vec2<t_Val> q;
        transCoord(p, q);
        getSpherePoint(q, sphereStart);
        cubeQuatStart = cubeQuat;
        mouseQuat.Identity();

        m_bDrag = true;
    }

    void OnMouseDown(int x, int y)
    {
        OnMouseDown(num::Vec2i(x, y));
    }

    void OnMouseMove(const num::Vec2i &p)
    {
        if ( m_bDrag )
        {
            num::Vec2<t_Val> q;
            transCoord(p, q);
            getSpherePoint(q, sphereEnd);
            getQuat(sphereStart, sphereEnd, mouseQuat);

            cubeQuat = mouseQuat;
            cubeQuat.Multiply(cubeQuatStart);
            cubeQuat.Normalize();
            cubeQuat.ExportRotationMatrix(m_Rotation);
        }
    }

    void OnMouseMove(int x, int y)
    {
        OnMouseMove(num::Vec2i(x, y));
    }

    void OnMouseUp(const num::Vec2i &p)
    {
        OnMouseMove(p);
        m_bDrag = false;
    }

    void OnMouseUp(int x, int y)
    {
        OnMouseUp(num::Vec2i(x, y));
    }

    template <class t_Mat>
    void GetMatrix3x3(t_Mat &m)
    {
        m(0, 0) = m_Rotation(0, 0);
        m(0, 1) = m_Rotation(0, 1);
        m(0, 2) = m_Rotation(0, 2);
        m(1, 0) = m_Rotation(1, 0);
        m(1, 1) = m_Rotation(1, 1);
        m(1, 2) = m_Rotation(1, 2);
        m(2, 0) = m_Rotation(2, 0);
        m(2, 1) = m_Rotation(2, 1);
        m(2, 2) = m_Rotation(2, 2);
    }

    template <class t_Mat>
    void GetMatrix4x4(t_Mat &m)
    {	
        m(0, 0) = m_Rotation(0, 0);
        m(0, 1) = m_Rotation(0, 1);
        m(0, 2) = m_Rotation(0, 2);
        m(0, 3) = m_Rotation(0, 3);
        m(1, 0) = m_Rotation(1, 0);
        m(1, 1) = m_Rotation(1, 1);
        m(1, 2) = m_Rotation(1, 2);
        m(1, 3) = m_Rotation(1, 3);
        m(2, 0) = m_Rotation(2, 0);
        m(2, 1) = m_Rotation(2, 1);
        m(2, 2) = m_Rotation(2, 2);
        m(2, 3) = m_Rotation(2, 3);
        m(3, 0) = m_Rotation(3, 0);
        m(3, 1) = m_Rotation(3, 1);
        m(3, 2) = m_Rotation(3, 2);
        m(3, 3) = m_Rotation(3, 3);
    }

    template <class t_Mat>
    void GetInverseMatrix3x3(t_Mat &m)
    {
        m(0, 0) = m_Rotation(0, 0);
        m(0, 1) = m_Rotation(1, 0);
        m(0, 2) = m_Rotation(2, 0);
        m(1, 0) = m_Rotation(0, 1);
        m(1, 1) = m_Rotation(1, 1);
        m(1, 2) = m_Rotation(2, 1);
        m(2, 0) = m_Rotation(0, 2);
        m(2, 1) = m_Rotation(1, 2);
        m(2, 2) = m_Rotation(2, 2);
    }

    template <class t_Mat>
    void GetInverseMatrix4x4(t_Mat &m)
    {
        m(0, 0) = m_Rotation(0, 0);
        m(0, 1) = m_Rotation(1, 0); 
        m(0, 2) = m_Rotation(2, 0);
        m(0, 3) = m_Rotation(3, 0);
        m(1, 0) = m_Rotation(0, 1);
        m(1, 1) = m_Rotation(1, 1);
        m(1, 2) = m_Rotation(2, 1);
        m(1, 3) = m_Rotation(3, 1);
        m(2, 0) = m_Rotation(0, 2);
        m(2, 1) = m_Rotation(1, 2);
        m(2, 2) = m_Rotation(2, 2);
        m(2, 3) = m_Rotation(3, 2);
        m(3, 0) = m_Rotation(0, 3);
        m(3, 1) = m_Rotation(1, 3);
        m(3, 2) = m_Rotation(2, 3);
        m(3, 3) = m_Rotation(3, 3);
    }

    template <class t_Mat>
    void SetMatrix(t_Mat &m)
    {
        if(!m_bDrag)
        {
            Initialize();
            cubeQuat.ImportRotationMatrix(m);

            memset(m_Rotation.m, 0, sizeof(m_Rotation.m));
            m_Rotation.m33 = (t_Val)1.0;
            m_Rotation(0, 0) = m(0, 0);
            m_Rotation(0, 1) = m(0, 1);
            m_Rotation(0, 2) = m(0, 2);
            m_Rotation(1, 0) = m(1, 0);
            m_Rotation(1, 1) = m(1, 1);
            m_Rotation(1, 2) = m(1, 2);
            m_Rotation(2, 0) = m(2, 0);
            m_Rotation(2, 1) = m(2, 1);
            m_Rotation(2, 2) = m(2, 2);
        }
    }

	void makeOrientationUpside()
	{
		num::Mat4x4f mat = GetMatrix();
			
		num::Vec3f vX,vY,vZ;
		num::Vec3f vAxisZ = Vec3f(0,0,1);
		//vX = Vec3f(mat.m[0][0],mat.m[0][1],mat.m[0][2]);
		//vY = Vec3f(mat.m[1][0],mat.m[1][1],mat.m[1][2]);
		vZ = Vec3f(mat.m[0][2],mat.m[1][2],mat.m[2][2]);

		//_CheckDump( " vX = " << vX << "\n vY =  " << vY << "\n vZ = " << vZ << "\n");

		if(fabs(vZ.Dot(vAxisZ)) < 1 - 1e-6 ){

			vX.CrossProduct(vAxisZ,vZ);
			vX.Normalize();
			vY.CrossProduct(vZ,vX);
			vY.Normalize();

			mat.m[0][0] = vX[0]; mat.m[1][0] = vX[1]; mat.m[2][0] = vX[2];
			mat.m[0][1] = vY[0]; mat.m[1][1] = vY[1]; mat.m[2][1] = vY[2];
			SetMatrix(mat);
		}
	}


	const num::Mat4x4<t_Val>& GetMatrix() const{ return m_Rotation; }
};

typedef ArcBall<float>	ArcBallf;
typedef ArcBall<double> ArcBalld;

} // namespace num