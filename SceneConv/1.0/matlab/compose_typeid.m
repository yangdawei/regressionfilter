%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Convert Matlab class_name to ::rt::TypeIds
% 
% return type id, 0 for unsupported type
%
% Version   2006.9.1    Jiaping

function [type_id,ele_size] = compose_typeid(typeid)
type_id = 0;
ele_size = 0;
switch typeid 
    case 'uchar'    
        type_id = 3;
        ele_size = 1;
    case 'uint16'   
        type_id = 4;
        ele_size = 2;
    case 'uint32'   
        type_id = 5;
        ele_size = 4;
    case 'uint64'   
        type_id = 6;
        ele_size = 8;
    case 'schar'    
        type_id = 7;
        ele_size = 1;
    case 'int16'    
        type_id = 9;
        ele_size = 2;
    case 'int32'    
        type_id = 10;
        ele_size = 4;
    case 'int64'    
        type_id = 11;
        ele_size = 8;
    case 'single'   
        type_id = 12;
        ele_size = 4;
    case 'double'   
        type_id = 13;
        ele_size = 8;
end
