%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Save Matrix/Image data file
% compatible with ::rt::_TypedVector_2D::Save/Load
%
% return 0 on succeeded
% Error Code
% -1 can not open the file
% -2 unsupported data type
% -3 error occured in data i/o
%
% Version   2006.8.31    Jiaping

function [ret] = save_matrix(mat,filename,class_name_desired)
ret = 0;
file_id = fopen(filename,'w');
if(file_id ~=-1 )
    if(nargin==2)
        class_name_desired = 'single';
    end
    [type_id,ele_sz] = compose_typeid(class_name_desired);
    if(ele_sz~=0)
        if(issparse(mat))
            fwrite(file_id,1397574995,'uint32');   %sign of SMMS
            fwrite(file_id,ele_sz,'uint32');
            fwrite(file_id,type_id,'uint32');
            [width,height] = size(mat);
            fwrite(file_id,width,'uint32');
            fwrite(file_id,height,'uint32');
            [i,j,s] = find(mat);
            i = int32(i-1);
            j = int32(j-1);
            [nonzero_co,dim] = size(i);
            fwrite(file_id,nonzero_co,'uint32');
            %fclose(file_id);
            %return;
            for x = 1:nonzero_co
                co1 = fwrite(file_id,i(x),'uint32');
                co2 = fwrite(file_id,j(x),'uint32');
                co3 = fwrite(file_id,s(x),class_name_desired);
                if(co1+co2+co3 ~= 3)
                    disp(['ERROR: in saving ' int2str(x) ' nonzero entry']);
                    ret = -3;
                end
            end
        else
            fwrite(file_id,1296911699,'uint32');   %sign of SMMM
            fwrite(file_id,ele_sz,'uint32');
            fwrite(file_id,type_id,'uint32');
            [width,height] = size(mat);
            fwrite(file_id,width,'uint32');
            fwrite(file_id,height,'uint32');
            if( width >0 && height >0 )
                for x = 1:width
                    co = fwrite(file_id,mat(x,:),class_name_desired);
                    if(co ~= height)
                        disp(['ERROR: in saving ' int2str(x) ' row']);
                        ret = -3;
                        break;
                    end
                end
            end
        end
    else
        disp(['ERROR: unsupported data type' class_name_desired]);
        ret = -2;
    end
    fclose(file_id);
else
    disp(['ERROR: Can not open file ' filename]);
    ret = -1;
end;

