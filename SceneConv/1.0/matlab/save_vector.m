%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Save Vector data file
% compatible with ::rt::_TypedVector::Save/Load
%
% return 0 on succeeded
% Error Code
% -1 can not open the file
% -2 unsupported data type
% -3 error occured in data i/o
%
% Version   2006.8.31    Jiaping

function [ret] = save_vector(vec,filename,class_name_desired)
ret = 0;
[length,dim] = size(vec);
if(dim~=1)
    error('input is not a vector');
end
file_id = fopen(filename,'w');
if(file_id ~=-1 )
    if(nargin==2)
        class_name_desired = 'single';
    end
    [type_id,ele_sz] = compose_typeid(class_name_desired);
    if(ele_sz~=0)
        fwrite(file_id,1447906643,'uint32');   %sign of SMMV
        fwrite(file_id,ele_sz,'uint32');
        fwrite(file_id,type_id,'uint32');
        fwrite(file_id,length,'uint32');
        if( length >0 )
            co = fwrite(file_id,vec,class_name_desired);
            if(co ~= length)
                disp('ERROR: in saving');
                ret = -3;
            end
        end
    else
        disp(['ERROR: unsupported data type' class_name_desired]);
        ret = -2;
    end
    fclose(file_id);
else
    disp(['ERROR: Can not open file ' filename]);
    ret = -1;
end;

