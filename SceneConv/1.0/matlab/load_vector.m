%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Load Vector data file
% compatible with ::rt::_TypedVector::Save/Load
%
% return the vector on succeeded
% Error Code
% -1 can not open the file
% -2 the file is in bad format
% -3 error occured in data i/o
%
% Version   2006.9.1    Jiaping

function [ret] = load_vector(filename)
ret = -2;
file_id = fopen(filename,'r');
if(file_id ~=-1 )
    header = fread(file_id,4,'uint32');
    if (header(1) == 1447906643) %sign for SMMV
        [ele_type_name,ele_size] = parse_typeid(header(3));
        if(ele_size==header(2))
            length = header(4);
            if( length >0 )
                [ret,co] = fread(file_id,length,ele_type_name);
                if(co~=length)
                    disp('ERROR: in loading');
                    ret = -3;
                end
            end
        end
    else
        disp('ERROR: data file is corrupt.');
    end;
    fclose(file_id);
else
    disp(['ERROR: Can not open file ' filename]);
    ret = -1;
end;

