%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Load Matrix/Image data file
% compatible with ::rt::_TypedVector_2D::Save/Load
% and ::num::Sparse_Matrix::Save/Load
%
% return the matrix on succeeded
% Error Code
% -1 can not open the file
% -2 the file is in bad format
% -3 error occured in data i/o
%
% Version   2006.8.31    Jiaping

function [ret] = load_matrix(filename)
ret = -2;
file_id = fopen(filename,'r');
if(file_id ~=-1 )
    header = fread(file_id,5,'uint32');
    [ele_type_name,ele_size] = parse_typeid(header(3));
    if (header(1) == 1296911699) %sign for SMMM
        % load normal matrix
        if(ele_size==header(2))
            width=header(4);
            height=header(5);
            if( width >0 && height >0 )
                ret = zeros(width,height,ele_type_name);
                for x = 1:width
                    [ret(x,:),co] = fread(file_id,height,ele_type_name);
                    if(co ~= height)
                        disp(['ERROR: in loading ' int2str(x) ' row']);
                        ret = -3;
                        break;
                    end
                end
            end
        end
    else 
        if(header(1) == 1397574995) %sign for SMMS
            % load sparse matrix
            if(ele_size==header(2))
                width=header(4);
                height=header(5);
                nonzero_co=fread(file_id,1,'uint32');
                if( width>0 && height>0 && nonzero_co>0 )
                    nonzero_pos = zeros(nonzero_co,2,'uint32');
                    nonzero_value = zeros(nonzero_co,1,ele_type_name);
                    for x = 1:nonzero_co
                        [nonzero_pos(x,:),co2] = fread(file_id,2,'uint32');
                        [nonzero_value(x),co1] = fread(file_id,1,ele_type_name);
                        if(co2~=2 || co1~=1)
                            disp(['ERROR: in loading ' int2str(x) ' nonzero entry']);
                            ret = -3;
                            break;
                        end
                    end
                    if(ret ~= -3)
                        i = double(nonzero_pos(:,1)+1);
                        j = double(nonzero_pos(:,2)+1);
                        s = double(nonzero_value);
                        ret = sparse(i,j,s,width,height);
                    end
                end
            end
        end    
    end
    fclose(file_id);
    if(ret == -2)
        disp('ERROR: data file is corrupt.');
    end
else
    disp(['ERROR: Can not open file ' filename]);
    ret = -1;
end;

