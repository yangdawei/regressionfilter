%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Convert ::rt::TypeIds to Matlab class_name
% 
% return Matlab class_name and byte-per-element
% byte-per-element is returned 0 if the typeid
% not supported by Matlab
%
% Version   2006.9.1    Jiaping

function [class_name,byte_per_element] = parse_typeid(typeid)
fund_type_id = int32(mod(typeid,65536));
class_name = -1;
byte_per_element = 0;
switch fund_type_id 
    case 3 %_typeid_8u
        class_name = 'uchar';
        byte_per_element = 1;
    case 4 %_typeid_16u
        class_name = 'uint16';
        byte_per_element = 2;
    case 5 %_typeid_32u
        class_name = 'uint32';
        byte_per_element = 4;
    case 6 %_typeid_64u
        class_name = 'uint64';
        byte_per_element = 8;
    case 7 %_typeid_8s
        class_name = 'schar';
        byte_per_element = 1;
    case 9 %_typeid_16s
        class_name = 'int16';
        byte_per_element = 2;
    case 10 %_typeid_32s
        class_name = 'int32';
        byte_per_element = 4;
    case 11 %_typeid_64s
        class_name = 'int64';
        byte_per_element = 8;
    case 12 %_typeid_32f
        class_name = 'single';
        byte_per_element = 4;
    case 13 %_typeid_64f
        class_name = 'double';
        byte_per_element = 8;
end
