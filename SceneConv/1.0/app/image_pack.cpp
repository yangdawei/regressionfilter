#include "StdAfx.h"
#include "image_pack.h"
#include "..\w32\debug_log.h"

/////////////////////////////////////////
// class CImagePackage_FileHeader
ipp::file::CImagePackage_FileHeader::CImagePackage_FileHeader()
{
	ZeroMemory(this,sizeof(CImagePackage_FileHeader));

	HeadSign = IMPK_FileHeaderSignature;
	FileType = IMPK_FileType_Undefined;
	Version = IMPK_Version;
	ZeroPadding = 0;

	CompressionOption = IMPK_Compression_None;

	Dimension = 0;

	ZeroMemory(DimensionResolution,sizeof(DimensionResolution));
	ZeroMemory(DimensionResolutionValueTable,sizeof(DimensionResolutionValueTable));
	ZeroMemory(Reserved,sizeof(Reserved));
}

UINT ipp::file::CImagePackage_FileHeader::GetTotalImageCount() const
{
	UINT tot = 1;
	for(UINT i=0;i<Dimension;i++)
	{
		tot *= DimensionResolution[i];
	}
	return tot;
}


/////////////////////////////////////////
// class CHdrBtfPackage_Reader
ipp::file::CImagePackage::CImagePackage()
{
	_ClearHeader();
}

void ipp::file::CImagePackage::_ClearHeader()
{
	ZeroMemory(m_pDimensionResolutionValueTable,sizeof(m_pDimensionResolutionValueTable));
	ZeroMemory(m_DimenSteps,sizeof(m_DimenSteps));
	m_SingleImageDataSize = 0;
	m_EffectiveDimension = 0;
}

ipp::file::CImagePackage::~CImagePackage()
{
	Close();
}

HRESULT	ipp::file::CImagePackage::Open(LPCTSTR fn,BOOL bSimluteFullDimension,UINT openFlag)	// read only
{
	ASSERT(!m_DataStream.IsOpen());
	if(m_DataStream.Open(fn,openFlag))
	{
		if(m_DataStream.Read(this,sizeof(CImagePackage_FileHeader)) == sizeof(CImagePackage_FileHeader))
		{
			if( HeadSign == IMPK_FileHeaderSignature && 
				Version <= IMPK_Version &&
				CompressionOption == IMPK_Compression_None &&
				BytePerScanline >= ImageWidth*Channel*BytePerChannel
				)
			{
				_CheckDump("Open image package:\n  "<<ImageWidth<<"x"<<ImageHeight<<" ");
				_CheckDump(Channel<<"c");

				if( BytePerChannel == 1 )
				{
					_CheckDump("8u");
				}
				else if( BytePerChannel == 2 )
				{
					_CheckDump("16s");
				}
				else if( BytePerChannel == 4 )
				{
					_CheckDump("32f");
				}

				_CheckDump(" image of "<<Dimension<<"D array (");
				for(UINT i=0;i<Dimension;i++)
				{
					_CheckDump(DimensionResolution[i]);
					if(i != Dimension-1 )_CheckDump(" x ");
				}
				_CheckDump(")\n");

				if(Version > 0x00010000)
				{
					_CheckDump("Dimensions are ");
					for(UINT i=0;i<Dimension;i++)
					{
						_CheckDump(DimensionName[i]);
						if(i != Dimension-1 )_CheckDump(" . ");
					}
					_CheckDump("\n");
				}
				_CheckDump("\n");

				// Load dimen value
				for(UINT i=0;i<Dimension;i++)
				{
					_SafeDelArray(m_pDimensionResolutionValueTable[i]);
					if( DimensionResolution[i] )
					{
						m_pDimensionResolutionValueTable[i] = new float[DimensionResolution[i]];
						ASSERT(m_pDimensionResolutionValueTable[i]);

						if(m_DataStream.Seek(DimensionResolutionValueTable[i]) == DimensionResolutionValueTable[i])
							if(	m_DataStream.Read(m_pDimensionResolutionValueTable[i],sizeof(float)*DimensionResolution[i])
								== sizeof(float)*DimensionResolution[i] )
							{
								continue;
							}

						_ClearHeader();
						return w32::HresultFromLastError();
					}
				}

				// Calculate steps
				m_SingleImageDataSize = ImageHeight*BytePerScanline;

				if( Dimension )
				{
					m_DimenSteps[Dimension-1] = m_SingleImageDataSize;

					for(int i=Dimension-1;i>0;i--)
					{
						m_DimenSteps[i-1] = m_DimenSteps[i]*DimensionResolution[i];
					}
				}

				_SimluteFullDimension();
				if(bSimluteFullDimension)Dimension = 6;

				for(m_EffectiveDimension = Dimension;m_EffectiveDimension>0;m_EffectiveDimension--)
					if(DimensionResolution[m_EffectiveDimension-1]>1)
						break;

				return S_OK;
			}
			else
			{
				_ClearHeader();
				return E_ABORT;
			}
		}
	}

	_ClearHeader();
	return w32::HresultFromLastError();
}

void ipp::file::CImagePackage::_SimluteFullDimension()
{
	for(int i=Dimension;i<6;i++)
	{
		m_DimenSteps[i] = 0;
		_SafeDelArray(m_pDimensionResolutionValueTable[i]);
		DimensionResolution[i] = 1;
	}
}

HRESULT	ipp::file::CImagePackage::Create(LPCTSTR fn,const CImagePackage_FileHeader * header,const float * pResTable0,const float * pResTable1,
									const float * pResTable2,const float * pResTable3,const float * pResTable4,const float * pResTable5) // write only
{
	ASSERT(header);
	ASSERT(!m_DataStream.IsOpen());
	if(m_DataStream.Open(fn,w32::CFile64::Normal_Write))
	{
		memcpy(this,header,sizeof(CImagePackage_FileHeader));

		if(	CompressionOption == IMPK_Compression_None && 
			Version <= IMPK_Version && 
			HeadSign == IMPK_FileHeaderSignature &&
			BytePerScanline >= ImageWidth*Channel*BytePerChannel
			)
		{

			ULONGLONG base=sizeof(CImagePackage_FileHeader);
			{
				ZeroMemory(DimensionResolutionValueTable,sizeof(DimensionResolutionValueTable));
				if( pResTable0 && Dimension>0)
				{
					DimensionResolutionValueTable[0] = base;
					base += sizeof(float)*DimensionResolution[0];

					_SafeDelArray(m_pDimensionResolutionValueTable[0]);
					m_pDimensionResolutionValueTable[0] = new float[DimensionResolution[0]];
					ASSERT( m_pDimensionResolutionValueTable[0] );
					memcpy(m_pDimensionResolutionValueTable[0],pResTable0,DimensionResolution[0]*sizeof(float));
				}
				if( pResTable1 && Dimension>1)
				{
					DimensionResolutionValueTable[1] = base;
					base += sizeof(float)*DimensionResolution[1];

					_SafeDelArray(m_pDimensionResolutionValueTable[1]);
					m_pDimensionResolutionValueTable[1] = new float[DimensionResolution[1]];
					ASSERT( m_pDimensionResolutionValueTable[1] );
					memcpy(m_pDimensionResolutionValueTable[1],pResTable1,DimensionResolution[1]*sizeof(float));
				}
				if( pResTable2 && Dimension>2)
				{
					DimensionResolutionValueTable[2] = base;
					base += sizeof(float)*DimensionResolution[2];

					_SafeDelArray(m_pDimensionResolutionValueTable[2]);
					m_pDimensionResolutionValueTable[2] = new float[DimensionResolution[2]];
					ASSERT( m_pDimensionResolutionValueTable[2] );
					memcpy(m_pDimensionResolutionValueTable[2],pResTable2,DimensionResolution[2]*sizeof(float));
				}
				if( pResTable3 && Dimension>3)
				{
					DimensionResolutionValueTable[3] = base;
					base += sizeof(float)*DimensionResolution[3];

					_SafeDelArray(m_pDimensionResolutionValueTable[3]);
					m_pDimensionResolutionValueTable[3] = new float[DimensionResolution[3]];
					ASSERT( m_pDimensionResolutionValueTable[3] );
					memcpy(m_pDimensionResolutionValueTable[3],pResTable3,DimensionResolution[3]*sizeof(float));
				}
				if( pResTable4 && Dimension>4)
				{
					DimensionResolutionValueTable[4] = base;
					base += sizeof(float)*DimensionResolution[4];

					_SafeDelArray(m_pDimensionResolutionValueTable[4]);
					m_pDimensionResolutionValueTable[4] = new float[DimensionResolution[4]];
					ASSERT( m_pDimensionResolutionValueTable[4] );
					memcpy(m_pDimensionResolutionValueTable[4],pResTable4,DimensionResolution[4]*sizeof(float));
				}
				if( pResTable5 && Dimension>5)
				{
					DimensionResolutionValueTable[5] = base;
					base += sizeof(float)*DimensionResolution[5];

					_SafeDelArray(m_pDimensionResolutionValueTable[5]);
					m_pDimensionResolutionValueTable[4] = new float[DimensionResolution[5]];
					ASSERT( m_pDimensionResolutionValueTable[5] );
					memcpy(m_pDimensionResolutionValueTable[5],pResTable5,DimensionResolution[5]*sizeof(float));
				}
			}

            ImageArrayStart = base;

			m_SingleImageDataSize = ImageHeight*BytePerScanline;

			if( Dimension )
			{
				m_DimenSteps[Dimension-1] = m_SingleImageDataSize;
                UINT i=Dimension-1;
				for(;i>0;i--)
				{
					m_DimenSteps[i-1] = m_DimenSteps[i]*DimensionResolution[i];
				}

				m_DataStream.SetLength(ImageArrayStart + m_DimenSteps[0]*DimensionResolution[0]);
				m_DataStream.Seek(0);

				m_DataStream.Write(this,sizeof(CImagePackage_FileHeader));
				for(i=0;i<Dimension;i++)
				{
					if( DimensionResolutionValueTable[i] )
					{
						m_DataStream.Write(m_pDimensionResolutionValueTable[i],sizeof(float)*DimensionResolution[i]);
					}
				}
			}
			else
			{
				m_DataStream.SetLength(ImageArrayStart + m_DimenSteps[0]*DimensionResolution[0]);
				m_DataStream.Seek(0);

				m_DataStream.Write(this,sizeof(CImagePackage_FileHeader));
			}

			if( m_DataStream.GetCurrentPosition() == ImageArrayStart )
			{
				_SimluteFullDimension();
				return S_OK;
			}
		}
	}

	return w32::HresultFromLastError();
}

void ipp::file::CImagePackage::Close()
{
	_SafeDelArray(m_pDimensionResolutionValueTable[0]);
	_SafeDelArray(m_pDimensionResolutionValueTable[1]);
	_SafeDelArray(m_pDimensionResolutionValueTable[2]);
	_SafeDelArray(m_pDimensionResolutionValueTable[3]);
	_SafeDelArray(m_pDimensionResolutionValueTable[4]);
	_SafeDelArray(m_pDimensionResolutionValueTable[5]);

	if(m_DataStream.IsOpen())m_DataStream.Close();

	ZeroMemory(m_DimenSteps,sizeof(m_DimenSteps));
	ZeroMemory(m_pDimensionResolutionValueTable,sizeof(m_pDimensionResolutionValueTable));
}


