#pragma once

//////////////////////////////////////////////////////////////////////
// Visual Computing Foundation Classes (VCFC)
//					Jiaping Wang  2006.2
//					e_boris2002@hotmail.com
//
// (C) Copyright Jiaping Wang 2004.
//
// Permission to copy, use, modify, sell and distribute this software
// is granted provided this copyright notice appears in all copies.
// This software is provided "as is" without express or implied
// warranty, and with no claim as to its suitability for any purpose.
//
// Absolutely no warranty!!
//////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////
//  Brief Description
//
//  graph_plot.h
//	Plot graph for visualization
//
//////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////
// Revise History
//
// Inital version	2007.10		Jiaping
//
//////////////////////////////////////////////////////////////////////

#include "..\wglh\wglMesh.h"
#include "..\wglh\wglArcball.h"

namespace wglh
{

class CGraphVisWnd:public wglh::CwglArcballWnd
{
public:
	struct PickEvent
	{	virtual void OnPick(wglh::CGraphVisWnd* Wnd, UINT vertex_id) = 0;
	};
	void SetPickEventHandler(PickEvent* p=NULL){ m_pPickEventHandler = p; }

	CGraphVisWnd()
    {   
        m_LastSelected = INFINITE;
        m_PointSize = 3;
        m_bNormalize=FALSE;
        m_bDrawEdge=TRUE;
        m_bDrawBox=TRUE;
    }
	void SetVertex(const num::Vec3f* p, UINT len, UINT step_in_byte = sizeof(float)*3)
	{
		m_LastSelected = INFINITE;
		Apply();
		if(len!=m_Graph.GetVertexCount())m_Graph.SetMeshRenderMode(GL_POINTS);
		m_Graph.SetSize_Vertex(len);
		num::Vec3f* d = m_Graph.GetBuffer_VertexPosition();
		for(UINT i=0;i<len;i++)
			d[i] = (num::Vec3f&)(((LPBYTE)p)[i*step_in_byte]);

		if(m_bNormalize)
			m_Graph.NormalizePosition();

		m_Vertex.SetSize(len);
		m_Vertex.CopyFrom(d);
		m_Graph.SubmitBuffers();
	}
	void SetVertex(const num::Vec3f* pos, const num::Vec4b* color, UINT len, UINT pos_step = sizeof(float)*3, UINT color_step = sizeof(num::Vec4b))
	{
		m_LastSelected = INFINITE;
		Apply();
		if(len!=m_Graph.GetVertexCount())m_Graph.SetMeshRenderMode(GL_POINTS);
		m_Graph.SetSize_Vertex(len);
		num::Vec3f* d = m_Graph.GetBuffer_VertexPosition();
		for(UINT i=0;i<len;i++)
			d[i] = (num::Vec3f&)(((LPBYTE)pos)[i*pos_step]);

		num::Vec4b* c = m_Graph.GetBuffer_VertexColor();
		for(UINT i=0;i<len;i++)
			c[i] = (num::Vec4b&)(((LPBYTE)color)[i*color_step]);

		if(m_bNormalize)
			m_Graph.NormalizePosition();

		m_Vertex.SetSize(len);
		m_Vertex.CopyFrom(d);
		m_Graph.SubmitBuffers();
	}
	void SetEdge(const num::Vec2u* p, UINT len, UINT step_in_byte = sizeof(UINT)*2)
	{
		Apply();
		m_Graph.SetSize_MeshElementIndic(len*2);
		num::Vec2u* d = (num::Vec2u*)m_Graph.GetBuffer_MeshElementIndic();
		for(UINT i=0;i<len;i++)
			d[i] = (num::Vec2u&)(((LPBYTE)p)[i*step_in_byte]);
		m_Graph.SubmitBuffers();
		m_Graph.SetMeshRenderMode(GL_LINES);
	}
	static CGraphVisWnd* CreatePopup(LPCTSTR title = NULL, BOOL auto_del = TRUE)
	{
		CGraphVisWnd* wnd = new CGraphVisWnd;
		ASSERT(wnd);
		if(wnd->Create() && wnd->InitGL())
		{
			if(auto_del)wnd->EnableDeleteThisOnDestroy();
			wglh::wglLoadAllExtensionEntryPoints();
	
			wnd->SetWindowText(title?title:_T("Graph Plot"));
			wnd->ResizeWindowByClientArea(400,300);
			wnd->ShowWindow();
			return wnd;
		}
		wnd->DestroyWindow();
		delete wnd;
		return NULL;
	}
	void SetLastSelection(UINT id = INFINITE){ m_LastSelected = id; Render(); }
	void SetPointSize(UINT pt_sz){ m_PointSize = pt_sz; Render(); }
	void EnableAutoNormalize(BOOL is_auto){ m_bNormalize = is_auto; }
	CwglMesh*	GetMesh(){ return &m_Graph; }

protected:
	wglh::CwglMesh				m_Graph;
	BOOL						m_bDrawBox;
    BOOL                        m_bDrawEdge;
	::rt::Buffer<num::Vec3f>	m_Vertex;
	UINT						m_LastSelected;
	wglh::CwglPrimitive			m_Primitive;
	PickEvent*					m_pPickEventHandler;
	UINT						m_PointSize;
	BOOL						m_bNormalize;
	
protected:
	void OnInitScene(){ __super::OnInitScene(); m_Primitive.Init();	}
	void OnUninitScene(){ m_Graph.Destroy(); m_Primitive.Term(); }
	void OnRender()
	{
		glClear(GL_COLOR_BUFFER_BIT|GL_DEPTH_BUFFER_BIT);
		glMatrixMode(GL_MODELVIEW);
		glLoadIdentity();
		glTranslated(0,0,-5);
		TransformScene();

		if(m_bDrawBox)DrawUnitBox();

		if(m_Graph.IsLoaded())
		{
			glColor3f(1,1,1);
			glPointSize((GLfloat)m_PointSize);

			m_Graph.Render(wglh::CwglMesh::RENDERING_FLAG_POINT_CLOUD);

			if(m_Graph.GetMeshRenderMode()==GL_LINES)
                m_Graph.Render(m_bDrawEdge?0:wglh::CwglMesh::RENDERING_FLAG_POINT_CLOUD);

			if(m_LastSelected < m_Vertex.GetSize())
			{	
				glColor3f(0.2f,0.2f,0.9f);
				glPointSize(10);

				glPushMatrix();
					glTranslatef(	m_Vertex[m_LastSelected].x,
									m_Vertex[m_LastSelected].y,
									m_Vertex[m_LastSelected].z	);
					m_Primitive.Sphere(0.04f*320/height,8,8);
				glPopMatrix();
			}
		}
		
		glFinish();
		SwapBuffers(wglGetCurrentDC());
		_CheckErrorGL;
	}
	LRESULT WndProc(UINT message, WPARAM wParam, LPARAM lParam)
	{
		if(message == WM_KEYDOWN)
		{
			if('B' == wParam){ m_bDrawBox = !m_bDrawBox; Render(); }
			if('W' == wParam){ m_bDrawEdge = !m_bDrawEdge; Render(); }
		}
		if(message == WM_LBUTTONDOWN && (wParam&MK_CONTROL))
		{
			if(m_Graph.IsLoaded())
			{
				glClear(GL_COLOR_BUFFER_BIT|GL_DEPTH_BUFFER_BIT);
				glMatrixMode(GL_MODELVIEW);
				glLoadIdentity();
				glTranslated(0,0,-5);
				TransformScene();
				glPointSize(6);

				Render();

				if(m_pPickEventHandler)
					m_pPickEventHandler->OnPick(this,m_LastSelected);
			}
		}

		return __super::WndProc(message,wParam,lParam);
	}
};




} // namespace wglh
