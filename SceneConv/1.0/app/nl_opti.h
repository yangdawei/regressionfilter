#pragma once

//////////////////////////////////////////////////////////////////////
// Visual Computing Foundation Classes (VCFC)
//					Jiaping Wang  2006.2
//					e_boris2002@hotmail.com
//
// (C) Copyright Jiaping Wang 2004.
//
// Permission to copy, use, modify, sell and distribute this software
// is granted provided this copyright notice appears in all copies.
// This software is provided "as is" without express or implied
// warranty, and with no claim as to its suitability for any purpose.
//
// Absolutly no garanty!!
//////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////
//  Brief Description
//
//  nl_opti.h
//
//  Nonlinear optimization algorithms
//
//////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////
// Revise History
//
// Inital version	2006.12.3		Jiaping
//
//////////////////////////////////////////////////////////////////////


#include "..\mkl\mkl_matrix.h"

//////////////////////////////////////////////////////////////////////
// Levenberg-Marquardt algorithm (LMA):
// Solve parameters P_0 of an nonlinear function y = F(x;P) based on 
// observed measurements of F(x,P_0): <y_i,x_i>
//
// F is a function of "d" dimension
// P is the parameter of "k" dimension
// x is the variable of F, we do not care about its dimension 
// <y_i,x_i> are observed measurements of "m" different x_i
// 
// To determine P, y = F_x(P+dP) is approximated by its linearizations:
//                F_x(P+ΔP) = F_x(P) + J_x(P)		                  (1)
// where J is the Jacobian of F_x at P.
//              (!J.J + λ.I)Δq = !J.Δy			                      (2)
// Start from initial guess of P', P_0 is approximated by P'+Δp. the 
// the Δp is determined by equations (2), in which:
//
//	  /	 "a matrix of (d*m,k)"				   									\
//	  |  δF_0(x_0,P')/δP_0	δF_0(x_0,P')/δP_1  		...		δF_0(x_0,P')/δP_k-1	|
//	  |  δF_1(x_0,P')/δP_0	δF_1(x_0,P')/δP_1  		...		δF_1(x_0,P')/δP_k-1	|
//	  |          :                     :         	 :  	          :			|
//	  |          :                     :         	 :  	          :			|
//J = |  δF_0(x_1,P')/δP_0	δF_0(x_1,P')/δP_1  		...		δF_0(x_1,P')/δP_k-1	|
//	  |          :                     :         	 :  	          :			|
//	  |          :                     :         	 :  	          :			|
//	  |          :                     :         	 :  	          :			|  
//	  |δF_d-1(x_m-1,P')/δP_0 δF_d-1(x_m-1,P')/δP_1	...	 δF_d-1(x_m-1,P')/δP_k-1|
//	  \		 Δy																	/
//
//		/  "a colume of d*m"				\
//		|  F_0(x_0,P')	 -	  F_0(x_0,P') 	|
//		|  F_1(x_0,P')	 -	  F_1(x_0,P') 	|
//		|      :			      :		 	|		
//		|      :			      :		 	|
// Δy =	|  F_0(x_1,P')	 -	  F_0(x_1,P') 	|
//		|      :			      :		 	|
//		|      :			      :		 	|
//		|      :			      :			| 
//		|F_d-1(x_m-1,P') -	F_d-1(x_m-1,P')	|
//		\									/
//
// then P' is iteratively updated by P'+Δp.



namespace app
{

template<typename t_Val = float>
class CLevenbergMarquardt
{
protected:
	UINT						_func_dimen;	//d
	UINT						_param_dimen;	//k
	UINT						_sample_num;	//m
	UINT						_equ_co;		// = _func_dimen*_sample_num
	t_Val						_lamda;

	mkl::CVector<t_Val>			_right_hand;	//size is _param_dimen
	mkl::CMatrix<t_Val>			_left_hand;		//_param_dimen x _param_dimen

public:
	UINT						GetValueDimen(){ return _func_dimen; }
	UINT						GetSolutionDimen(){ return _param_dimen; }
	UINT						GetSampleNumber(){ return _sample_num; }

public:
	mkl::CVector<t_Val>			solution;		//size is _param_dimen
	mkl::CMatrix<t_Val>			jacobian;		//size is _func_dimen*_sample_num x _param_dimen

	BOOL SetupProblem(UINT func_dim, UINT param_dim, UINT sample_num)
	{	_func_dimen = func_dim;
		_param_dimen = param_dim;
		_sample_num = sample_num;
		_equ_co = _func_dimen*_sample_num;

		return		_right_hand.SetSize(_param_dimen) 
				&&	_left_hand.SetSize(_param_dimen,_param_dimen) 
				&&	solution.SetSize(_param_dimen)
				&&	jacobian.SetSize(_equ_co,_param_dimen);
	}

	void SetInitialSolution(const t_Val* pP)
	{	ASSERT_ARRAY(pP,_param_dimen);
		solution.CopyFrom(pP);
		memcpy(solution.Begin(),pP,sizeof(t_Val)*_param_dimen);
		_lamda = 1;
	}

	mkl::CVector_RefCompact<t_Val> GetJacobian(UINT param_index, UINT sample_index)
	{	ASSERT( param_index < _param_dimen );
		ASSERT( sample_index < _sample_num );
		return jacobian.GetCol(param_index).GetRef(sample_index*_func_dimen,_func_dimen);
	}

	HRESULT IterateOnce(const t_Val* pResidual, t_Val delta = 0.05) /// (F(x_i,P_0) - F(x_i,P')) size is _func_dimen*_sample_num
	{	HRESULT hr;
		ASSERT_ARRAY(pResidual,_equ_co);

		t_Val step_limit = delta*_param_dimen;
		t_Val step = FLT_MAX;
		mkl::CVector_RefCompact<t_Val> Residual(::rt::_CastToNonconst(pResidual),_equ_co);

		for(;;)
		{
			// solve equation
			(!jacobian).VectorProduct( Residual, _right_hand );
			_left_hand.Product(!jacobian,!!jacobian);
			_left_hand.GetDiag() += _lamda;

			hr = _left_hand.SolveLinearLeastSquares_LQR(_right_hand);
			if(FAILED(hr))return hr;

			// check step
			step = _right_hand.L2Norm();
			if( step > delta )
			{	_lamda *= 2;
				//_CheckDump('+');
				continue;
			}
			else
			{	if(step<delta/2)_lamda /= 2;
				//_CheckDump('-');
				break;
			}
		}

		solution += _right_hand;

		return S_OK;				
	}
};



//////////////////////////////////////////////////////////////////////
// Nonlinear Conjugate-Gradient (NCG):
// minimize an nonlinear equation F(x) by moving toward gradient
// direction. 

template<typename t_Val = float>
class CNonlinearConjugateGradient
{	
public:
	class _EquationCls
	{public:
		virtual void  GetGradient(const t_Val* pX, t_Val* pGrad) = 0;	// ΔF(x)
		virtual t_Val GetFunction(t_Val* pX) = 0;						// F(x), change pX's value to apply clamp for making solution legal
	};

public:
	class LinearSearch
	{	friend class CNonlinearConjugateGradient;
	protected:
		mkl::CVector<t_Val>			_SolutionTemp;
		mkl::CVector<t_Val>			_CurrentDirection;	// d_k
		t_Val _ResidualOfForward(t_Val step_len)
		{	for(UINT i=0;i<_SolutionTemp.GetSize();i++)
				_SolutionTemp[i] = solution[i] + step_len*_CurrentDirection[i];
			return _Equation->GetFunction(_SolutionTemp);
		}
	public:
		mkl::CVector<t_Val>			solution;
		_EquationCls*				_Equation;

		LinearSearch(){ _Equation = NULL; }
		BOOL SetSize(UINT solution_dimen)
		{	return	_SolutionTemp.SetSize(solution_dimen)		&& 
					solution.SetSize(solution_dimen)			&& 
					_CurrentDirection.SetSize(solution_dimen); 
		}

		void SetSearchDirection(const t_Val* p){ _CurrentDirection.CopyFrom(p); }

		void Search(t_Val max_step_length)
		{	t_Val eps = max_step_length/1000;
			t_Val a = 0;
			t_Val b = max_step_length;
			if((b-a>eps))
			{
				t_Val s1 = max_step_length*((t_Val)0.382);
				t_Val s2 = max_step_length*((t_Val)0.618);
				t_Val e1 = _ResidualOfForward(s1);
				t_Val e2 = _ResidualOfForward(s2);
				t_Val ret;

				for(UINT i=1; (b-a>eps); i++)
				{	if(e1>e2)
					{	a = s1;
						if((b-a)>eps)
						{
							s1 = s2; e1 = e2;
							s2 = a + ((t_Val)0.618)*(b-a);
							ret = e2 = _ResidualOfForward(s2);
						}else break;
					}
					else
					{	b = s2;
						if((b-a)>eps)
						{
							s2 = s1; e2 = e1;
							s1 = a + ((t_Val)0.382)*(b-a);
							ret = e1 = _ResidualOfForward(s1);
						}else break;
					}
				}

				::rt::Swap(_SolutionTemp,solution);
			}
		}

	};

protected:
	_EquationCls*				_Equation;
	mkl::CVector<t_Val>			_CurrentGradient;	// g_k
	mkl::CVector<t_Val>			_PreviousGradient;	// g_k-1
	LinearSearch				_Stepper;

public:
	mkl::CVector_RefCompact<t_Val>			solution;

	CNonlinearConjugateGradient():solution(NULL,0){}
	BOOL SetupProblem(UINT param_dimen, _EquationCls* pEqu)
	{	_Equation = _Stepper._Equation = pEqu;
		if(_Stepper.SetSize(param_dimen) && _PreviousGradient.SetSize(param_dimen))
		{	solution.SetRef(_Stepper.solution);
			return TRUE;
		}
		return FALSE;
	}

	void SetInitialSolution(const t_Val* pP = NULL)
	{	_CurrentGradient.SetSize(0);
		if(pP)_Stepper.solution.CopyFrom(pP);
	}

	::rt::Buffer_Ref<t_Val> GetGradient()
	{	return ::rt::Buffer_Ref<t_Val>(_CurrentGradient.Begin(),_CurrentGradient.GetSize());
	}

	t_Val IterateOnce(t_Val max_step_length = 0.1f) // return Residual
	{	
		ASSERT(_Equation);
		if(	_CurrentGradient.GetSize() == solution.GetSize()){}
		else
		{	//first Iteration
			VERIFY(_CurrentGradient.SetSize(solution.GetSize()));
			_Equation->GetGradient(solution,_CurrentGradient);
			for(UINT i=0;i<solution.GetSize();i++)
				_Stepper._CurrentDirection[i] = -_CurrentGradient[i];
		}

		// find best step length by linear search
		_Stepper.Search(max_step_length);
		solution.SetRef(_Stepper.solution);

		// preparing for next
		_PreviousGradient = _CurrentGradient; // keep preivous gradient
		_Equation->GetGradient(solution,_CurrentGradient);

		// calculate beta
		t_Val g_m = _CurrentGradient.L2Norm_Sqr();
		t_Val beta = 0;
		{	for(UINT i=0;i<_CurrentGradient.GetSize();i++)
				beta += _CurrentGradient[i]*(_CurrentGradient[i] - _PreviousGradient[i]);
			if(beta < 0)
			{	beta = 0; }
			else
			{	beta /= g_m; }
		}

		// new moving direction
		for(UINT i=0;i<_CurrentGradient.GetSize();i++)
			_Stepper._CurrentDirection[i] = _Stepper._CurrentDirection[i]*beta - _CurrentGradient[i];

		return g_m;
	}
};



} // namespace app
