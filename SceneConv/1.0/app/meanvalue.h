#pragma once

//////////////////////////////////////////////////////////////////////
// Visual Computing Foundation Classes (VCFC)
//					Jiaping Wang  2006.2
//					e_boris2002@hotmail.com
//
// (C) Copyright Jiaping Wang 2004.
//
// Permission to copy, use, modify, sell and distribute this software
// is granted provided this copyright notice appears in all copies.
// This software is provided "as is" without express or implied
// warranty, and with no claim as to its suitability for any purpose.
//
// Absolutly no garanty!!
//////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////
//  Brief Description
//
//  Meanvalue.h
//
//////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////
// Revise History
//
// Inital version	2006.9.?		Jiaping
//
//////////////////////////////////////////////////////////////////////

#include "..\num\small_vec.h"

namespace num
{

template<typename t_Val>
class MeanValueSpace_2d
{
protected:
	::rt::Buffer<num::Vec2<t_Val> >	m_SupportVertex;

	__forceinline float _meanvalue_coord(const num::Vec2<t_Val>& v,UINT prev,UINT curr,UINT next) const
	{
		Vec2<t_Val> p,c,n;
		p.x = m_SupportVertex[prev].x - v.x;
		p.y = m_SupportVertex[prev].y - v.y;
		c.x = m_SupportVertex[curr].x - v.x;
		c.y = m_SupportVertex[curr].y - v.y;
		n.x = m_SupportVertex[next].x - v.x;
		n.y = m_SupportVertex[next].y - v.y;

		p.Normalize();
		n.Normalize();
		t_Val len = sqrt(c.L2Norm_Sqr());
		c /= len;

		t_Val cos1,cos2;
		cos1 = p.Dot(c); cos2 = n.Dot(c);

		return (sqrt(1-cos1*cos1)/(1+cos1) + sqrt(1-cos2*cos2)/(1+cos2)) / len;
	}

public:
	template<typename T>
	void SetupSpace(const T* pVertex, UINT count)
	{	
		m_SupportVertex.SetSize(count);
		m_SupportVertex.CopyFrom(pVertex);
	}
	UINT GetDimension()const{ return m_SupportVertex.GetSize(); }
	const num::Vec2<t_Val>& GetSupportVertex(UINT i)const{ return m_SupportVertex[i]; }

	void Project(const num::Vec2<t_Val>& v, t_Val* pCoord) const
	{	UINT dim = m_SupportVertex.GetSize();
		ASSERT_ARRAY(pCoord,dim);

		t_Val tot = pCoord[0] = _meanvalue_coord(v,dim-1,0,1);
		UINT i=1;
		for(;i<dim-1;i++)
			tot += (pCoord[i] = _meanvalue_coord(v,i-1,i,i+1));
		tot += (pCoord[i] = _meanvalue_coord(v,i-1,i,0));

		for(UINT i=0;i<dim;i++)
			pCoord[i] /= tot;
	}

	void Unproject(const t_Val* pCoord, num::Vec2<t_Val>& v) const
	{	UINT dim = m_SupportVertex.GetSize();
		ASSERT_ARRAY(pCoord,dim);
		m_SupportVertex[0].ScaleTo(pCoord[0],v);
		for(UINT i=1;i<dim;i++)
		{
			v.x += m_SupportVertex[i].x*pCoord[i];
			v.y += m_SupportVertex[i].y*pCoord[i];
		}
	}

	template<class atl_cimage> // for ATL::CImage in <ATLImage.h>
	void PlotPolygon(atl_cimage& img,UINT size = 200) const
	{	//Calculate bounding box
		Vec2<t_Val> min_v,max_v;
		{
			min_v = max_v = m_SupportVertex[0];
			for(UINT i=1;i<m_SupportVertex.GetSize();i++)
			{
				max_v.Max(max_v,m_SupportVertex[i]);
				min_v.Min(min_v,m_SupportVertex[i]);
			}
		}
		float scale = (size-20)/(max(max_v.x-min_v.x,max_v.y-min_v.y));

		if(!img.IsNull())
		{	if(img.GetWidth()==size && img.GetHeight()==size && img.GetPitch()>0){}
			else
			{	img.Destroy();
				img.Create(size,-((int)size),24);
			}
		}
		else{ img.Create(size,-((int)size),24); }
		
		HDC hdc = img.GetDC();
		memset(img.GetBits(),0xff,img.GetPitch()*img.GetHeight());
		UINT vco = m_SupportVertex.GetSize();
		for(UINT i=0;i<vco;i++)
		{	Vec2i pos;
			pos.x = (UINT)((m_SupportVertex[i].x - min_v.x)*scale + 10.5f);
			pos.y = (UINT)((m_SupportVertex[i].y - min_v.y)*scale + 10.5f);
			::Rectangle(hdc,pos.x-2,pos.y-2,pos.x+3,pos.y+3);
		}
		::MoveToEx(hdc,	(UINT)((m_SupportVertex[vco-1].x - min_v.x)*scale + 10.5f),
						(UINT)((m_SupportVertex[vco-1].y - min_v.y)*scale + 10.5f),NULL);
		for(UINT i=0;i<vco;i++)
		{	Vec2i pos;
			pos.x = (UINT)((m_SupportVertex[i].x - min_v.x)*scale + 10.5f);
			pos.y = (UINT)((m_SupportVertex[i].y - min_v.y)*scale + 10.5f);
			::LineTo(hdc,pos.x,pos.y);
		}
		img.ReleaseDC();
	}
};


}