#pragma once

//////////////////////////////////////////////////////////////////////
// Visual Computing Foundation Classes (VCFC)
//					Jiaping Wang  2006.2
//					e_boris2002@hotmail.com
//
// (C) Copyright Jiaping Wang 2004.
//
// Permission to copy, use, modify, sell and distribute this software
// is granted provided this copyright notice appears in all copies.
// This software is provided "as is" without express or implied
// warranty, and with no claim as to its suitability for any purpose.
//
// Absolutely no warranty!!
//////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////
//  Brief Description
//
//  dimen_reduc.h
//	MultiDimensional Scaling
//	Principal Component Analysis
//
//////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////
// Revise History
//
// Inital version	2007.10		Jiaping
//					2008.10		Jiaping		add PCA
//
//////////////////////////////////////////////////////////////////////


#include "..\mkl\mkl_matrix.h"
#include "..\w32\prog_idct.h"
#include "manifold.h"

namespace nldr
{

////////////////////////////////////////////////////////////
// Input: vector_in_col: Vectors in each column
// 
// Output:	vector_in_col is convert to coordinates vectors in column vector
//			center, the center vector of the original space
//			eigenvalue, the variance of each dimension of the resulting space
//			eigenvector, the eigenvectors in column vector ( eigenvector' is the projection matrix)
template<typename t_Val>
HRESULT PrincipalComponentAnalysis(::mkl::CMatrix<t_Val>& vector_in_col, ::mkl::CVector<t_Val>& center, ::mkl::CVector<t_Val>& eigenvalue, ::mkl::CMatrix<t_Val>& eigenvector, UINT max_dimen = UINT_MAX, float enerage_ratio = 0.99f)
{
	UINT count = vector_in_col.GetSize_Col();
	UINT dimen_org = vector_in_col.GetSize_Row();

	max_dimen = min(max_dimen,dimen_org);

	{	// centering the original space
		_CheckDump(_T("\tCentering: "));
		center.SetSize(dimen_org);
		t_Val divid = 1/(t_Val)count;
		for(UINT i=0;i<dimen_org;i++)
			center[i] = vector_in_col.GetRow(i).Sum() * divid;

		for(UINT i=0;i<count;i++)
			vector_in_col.GetCol(i) -= center;
		_CheckDump(center<<_T("\n"));
	}

	HRESULT ret = S_OK;

	if(count >= dimen_org)
	{
		_CheckDump(_T("\tCalculate covariance "<<vector_in_col.GetSize_Row()<<_T('x')<<vector_in_col.GetSize_Col()<<_T('\n')));

		if(!eigenvector.SetSize(dimen_org,dimen_org))return E_OUTOFMEMORY;
		eigenvector.Product(!!vector_in_col,!vector_in_col);

		_CheckDump(_T("\tSVD on ")<<eigenvector.GetSize_Row()<<_T('x')<<eigenvector.GetSize_Col()<<_T(" matrix, "));
		ret = eigenvector.SolveEigen_ByEnerge(eigenvalue,enerage_ratio,max_dimen);
		if(SUCCEEDED(ret))
		{	
			_CheckDump(_T("results in ")<<eigenvalue.GetSize()<<_T("D subspace.\n"));

			::mkl::CMatrix<t_Val>	coeff;
			if(!coeff.SetSize(eigenvalue.GetSize(),count))return E_OUTOFMEMORY;
			coeff.Product(!eigenvector,!!vector_in_col);

			rt::Swap(coeff,vector_in_col);
		}
	}
	else
	{
		_CheckDump(_T("\tGeneral SVD on ")<<vector_in_col.GetSize_Row()<<_T('x')<<vector_in_col.GetSize_Col()<<_T(" matrix, "));
		ret = vector_in_col.GeneralSVD(eigenvalue,mkl::Replace_U_in_This,NULL,&eigenvector);
		if(SUCCEEDED(ret))
		{	
			// reduce dimension
			t_Val tot_energe = eigenvalue.Sum()*enerage_ratio;
			UINT i=0;
			for(;i<eigenvalue.GetSize();i++)
				if(	eigenvalue[i] < tot_energe && 
					eigenvalue[i] > ::rt::TypeTraits<t_Val>::Epsilon() )
				{	tot_energe -= eigenvalue[i]; }
				else{ break; }
			max_dimen = min(max_dimen,i);

			// trancate eigenvalues
			{	::mkl::CVector<t_Val> tmp;
				tmp.SetSize(max_dimen);
				tmp = eigenvalue.GetRef(0,max_dimen);
				rt::Swap(tmp,eigenvalue);
			}
			// trancate eigenvector and coefficients
			{	::mkl::CMatrix<t_Val> coefficients;
				coefficients.SetSize(max_dimen,count);
				coefficients = eigenvector.GetSub(0,0,max_dimen,count);

				eigenvector.SetSize(dimen_org,max_dimen);
				eigenvector = vector_in_col.GetSub(0,0,dimen_org,max_dimen);
				rt::Swap(coefficients,vector_in_col);
			}

			_CheckDump(_T("results in ")<<eigenvalue.GetSize()<<_T("D subspace.\n"));

			for(UINT i=0;i<vector_in_col.GetSize_Row();i++)
				vector_in_col.GetRow(i) *= eigenvalue[i];
		}
	}

	if(SUCCEEDED(ret))
		_CheckDump(_T("\tEigen Values: ")<<eigenvalue<<_T('\n'));

	return ret;
}


////////////////////////////////////////////////////////////
// Input: dist_sqr_matrix(i,j) = ||Vi-Vj||^2, matrix pairwise square distance
// 
// Output:	dist_sqr_matrix is convert to coordinates vectors in column
//			eigenvalue, the variance of each dimension in the resulting space
template<typename t_Val>
HRESULT MultidimensionalScaling(::mkl::CMatrix<t_Val>& dist_sqr_matrix, ::mkl::CVector<t_Val>& eigenvalue_out, UINT max_dimen = UINT_MAX, float enerage_ratio = 0.99f)
{
	UINT order = dist_sqr_matrix.GetSize_Col();
	max_dimen = min(max_dimen,order);

	::mkl::CVector<t_Val>	_AveDis;

	_CheckDump(_T("\tBuild inner-product space.\n"));
	{	
		// Average Distances to others of i
		t_Val div = 1/(t_Val)order;
		_AveDis.SetSize(order);
		for(UINT i=0;i<order;i++)
			_AveDis[i] = dist_sqr_matrix.GetCol(i).Sum()*div;

		//centering columns and rows
		for(UINT i=0;i<order;i++)
			dist_sqr_matrix.GetRow(i) -= _AveDis[i];
		t_Val scale = -1/(t_Val)order;
		for(UINT i=0;i<order;i++)
		{	mkl::CVector_RefCompact<t_Val>& r = dist_sqr_matrix.GetCol(i);
			r += r.Sum()*scale;
		}

		// B = -0.5f*D
		for(UINT i=0;i<order;i++)
			dist_sqr_matrix.GetCol(i) *= -0.5f;
	}

	_CheckDump(_T("\tSVD on ")<<order<<_T('x')<<order<<_T(" matrix, "));
	{	
		::mkl::CVector<t_Val>	eigenvalue;
		
		{	HRESULT ret;
			ret = dist_sqr_matrix.SolveEigen_ByEnerge(eigenvalue,enerage_ratio,max_dimen);
			if(FAILED(ret))return ret;

			_CheckDump(_T("results in ")<<eigenvalue.GetSize()<<_T("D subspace.\n"));	
		}

		eigenvalue_out.SetSizeAs(eigenvalue);
		eigenvalue_out = eigenvalue;
		_CheckDump(_T("\tEigen Values: ")<<eigenvalue<<_T('\n'));

		eigenvalue.vml_SquareRoot();

		for(UINT i=0;i<eigenvalue.GetSize();i++)
			dist_sqr_matrix.GetCol(i) *= eigenvalue[i];

		dist_sqr_matrix = !dist_sqr_matrix;
	}
	return S_OK;
}


////////////////////////////////////////////////////////////
// Input: vector_in_col: Vectors in each column
// 
// Output:	vector_in_col is convert to coordinates vectors in column
//			eigenvalue, the variance of each dimension in the resulting space
template<typename t_Val>
HRESULT Isomap(::mkl::CMatrix<t_Val>& vector_in_col, ::mkl::CVector<t_Val>& eigenvalue, UINT k_nearest, BOOL bRemoveDuplicate, UINT max_dimen = UINT_MAX, float enerage_ratio = 0.99f)
{
	UINT count = vector_in_col.GetSize_Col();
	UINT dimen_org = vector_in_col.GetSize_Row();

	UINT subgraph;
	CManifoldSpace<float>		mani_space;
	{
		CEuclideanSpace<float>		org_space;
		if(!org_space.SetPointCount(count,dimen_org))return E_OUTOFMEMORY;
		for(UINT i=0;i<count;i++)
			vector_in_col.GetCol(i).CopyTo(org_space.GetPoint(i));
		org_space.Finalize(bRemoveDuplicate);

		mani_space.BuildNeighborhoodGraph(org_space,k_nearest);
		subgraph = mani_space.DetectConnentedComponents();
		if(subgraph>1)_CheckDump(_T('\t')<<subgraph<<_T(" unconnected Subgraphs are detected.\n"));
	}

	::mkl::CMatrix<t_Val>& dist = vector_in_col;
	if(!dist.SetSize(count,count))return E_OUTOFMEMORY;
	dist.Zero();

	_CheckDump(_T("\tCalculate approximated geodesic distance.\n"));
	mani_space.CalculateDistanceMatrix(dist);

	if(subgraph)	// fix infinit distance
	{	t_Val* p = dist;
		UINT len = dist.GetVec().GetSize();
		t_Val dist_max = 0;
		for(UINT i=0;i<len;i++)
			if(p[i]<mani_space.InfinitDistance() - EPSILON)
				dist_max = max(p[i],dist_max);
		dist_max *= 10;
		ASSERT_FLOAT(dist_max);
		for(UINT i=0;i<len;i++)
			if(p[i]>=mani_space.InfinitDistance() - EPSILON)
				p[i] = dist_max;
	}

	return MultidimensionalScaling(dist,eigenvalue,max_dimen,enerage_ratio);
}

////////////////////////////////////////////////////////////
// Input:	vector_in_col: Vectors in each column
//			k: the number of cluster centers
//			iteration_times: limit of times of iteration
// 
// Output:	center_in_col: Vectors of centers in each column
//			pLabels: cluster id for each vector
template<typename t_Val>
void k_Mean(const ::mkl::CMatrix<t_Val>& vector_in_col, UINT k, ::mkl::CMatrix<t_Val>& center_in_col, UINT * pLabels = NULL, UINT iteration_times = UINT_MAX)
{
	UINT dim = vector_in_col.GetSize_Row();
	UINT count = vector_in_col.GetSize_Col();

	rt::Buffer<UINT> id;
	id.SetSize(count);
	for(UINT i=0;i<count;i++)id[i] = i;
	id.Shuffle();

	rt::Buffer<UINT> pop;
	pop.SetSize(k);

	// init centers
	center_in_col.SetSize(dim,k);
	for(UINT i=0;i<k;i++)
		center_in_col.GetCol(i) = vector_in_col.GetCol(id[i]);

	double last_error = FLT_MAX;
	double err_interval = -1;
	double tot_err = 0;
	UINT t = 0;
	do
	{	
		last_error = tot_err;
		tot_err = 0;
		// clustering
		{	UINT i=0;
			w32::CTimeMeasureDisplay<> tm(&i,count);
			for(;i<count;i++)
			{
				const ::mkl::CMatrix<t_Val>::t_ColumnRef& x = vector_in_col.GetCol(i);
				double dist_min = FLT_MAX;
				for(UINT j=0;j<k;j++)
				{
					t_Val dist = x.Distance_Sqr(center_in_col.GetCol(j));
					if(dist<dist_min)
					{	dist_min = dist;
						id[i] = j;
					}
				}
				tot_err += dist_min;
			}
		}
		if(err_interval>0){}
		else{ err_interval = tot_err/10000; }
		_CheckDump(t<<"/"<<iteration_times<<"\t"<<tot_err<<"\n");
		// updating centers
		center_in_col.Zero();
		pop.Zero();
		for(UINT i=0;i<count;i++)
		{
			center_in_col.GetCol(id[i]) += vector_in_col.GetCol(i);
			pop[id[i]]++;
		}
		for(UINT i=0;i<k;i++)
		{
			center_in_col.GetCol(i)/=(t_Val)pop[i];
		}
	}while(t++ < iteration_times && fabs(last_error-tot_err)>err_interval);

	if(pLabels)
	{
		ASSERT_ARRAY(pLabels,count);
		id.CopyTo(pLabels);
	}
}


} // namespace nldr
