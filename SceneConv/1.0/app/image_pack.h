#pragma once

//////////////////////////////////////////////////////////////////////
// Visual Computing Foundation Classes (VCFC)
//					Jiaping Wang  2006.2
//					e_boris2002@hotmail.com
//
// (C) Copyright Jiaping Wang 2004.
//
// Permission to copy, use, modify, sell and distribute this software
// is granted provided this copyright notice appears in all copies.
// This software is provided "as is" without express or implied
// warranty, and with no claim as to its suitability for any purpose.
//
// Absolutely no warranty!!
//////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////
//  Brief Description
//
//  imagee_pack.h
//
//  image package for high dimension image sequence 
//
//////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////
// Revise History
//
// Inital version	2004.11		Jiaping
//                2009.07     Yanxiang: Fix the data out of range bugs in set/get method of CImagePackage class
//
//////////////////////////////////////////////////////////////////////

#include "..\rt\runtime_base.h"
#include "..\w32\file_64.h"

namespace ipp
{
namespace file
{

#define IMPK_Version				0x00010001	// v1.1
#define	IMPK_FileHeaderSignature	0x4b504d49	// "IMPK"

#define IMPK_FileType_Undefined		0x46444e55	// "UNDF" Type not defined
#define IMPK_FileType_BTFSEQ		0x53465442	// "BTFS" 7D	BTF scan data (5D image array)
#define IMPK_FileType_BTF			0x20465442	// "BTF " 6D	BTF data (4D image array)
#define IMPK_FileType_BRDFSEQ		0x53445242	// "BRDS" 5D	View-dependent image scan data (3D image array)
#define IMPK_FileType_BRDF			0x46445242	// "BRDF" 4D	View-dependent image data (2D image array)
#define IMPK_FileType_FRAMESEQ		0x4e414353	// "SCAN" 3D	image sequence data (1D image array)
#define IMPK_FileType_FRAME			0x4c474e53	// "SNGL" 2D	image data (0D image array)

#define IMPK_Compression_None		0x454e4f4e	// "NONE" Uncompressed
#define IMPK_Compression_Jpeg		0x4745504a	// "JPEG" Each single image is jpeg compressed  //supported in the future
//#define IMPK_Compression_Lz		


#include "errno.h"

class CImagePackage_FileHeader
{
	// v1.0 file header
public:
	DWORD	HeadSign;
	DWORD	Version;			//current is 0x00010001
	DWORD	FileType;			//IMPK_FileType_xxx
protected:
	DWORD	ZeroPadding;		//always zero

public:
	UINT	ImageWidth;
	UINT	ImageHeight;
	UINT	Channel;			//1-4
	UINT	BytePerChannel;		//1,2 or 4
	UINT	BytePerScanline;	//step in IPP
	DWORD	CompressionOption;	//IMPK_Compression_xxx, current support IMPK_Compression_None only

	UINT	Dimension;
	UINT	DimensionResolution[6];

public:
	ULONGLONG	DimensionResolutionValueTable[6];	// offset to every table from file begining, zero for N/A
	ULONGLONG	ImageArrayStart;					// offset to image from file begining	

	// v1.1 file header
	char	DimensionName[6][9];					// name of the dimension 8 charactors is limited

	// Padding header to 512 bytes
	BYTE	Reserved[512 - 4*(33) - 6*9];

public:
	CImagePackage_FileHeader();
	void	SetDimensionName(UINT i, LPCSTR name){ ASSERT(i<sizeofArray(DimensionName)); memcpy(DimensionName[i],name,min(strlen(name),8)); DimensionName[i][8]=0; } // 8 charactors is limited
	UINT	GetImageWidth()const{ return ImageWidth; }
	UINT	GetImageHeight()const{ return ImageHeight; }
	LPCSTR	GetFileType()const{ return (LPCSTR)&FileType; }
	UINT	GetTotalImageCount()const;
};

class CImagePackage:public CImagePackage_FileHeader
{
protected:
	w32::CFile64	m_DataStream;

protected:
	float*			m_pDimensionResolutionValueTable[6];

	UINT			m_EffectiveDimension;
	UINT			m_DimenSteps[6];
	UINT			m_SingleImageDataSize;		// in byte	
	void			_SimluteFullDimension();
	void			_ClearHeader();

public:
	CImagePackage();
	~CImagePackage();
    HRESULT	Open(LPCTSTR fn,BOOL bSimluteFullDimension = FALSE,UINT openFlag = w32::CFile64::Normal_Read);	// read only
	HRESULT Create(	LPCTSTR fn,const CImagePackage_FileHeader * header,
					LPCFLOAT pResTable0=NULL,LPCFLOAT pResTable1=NULL,
					LPCFLOAT pResTable2=NULL,LPCFLOAT pResTable3=NULL,
					LPCFLOAT pResTable4=NULL,LPCFLOAT pResTable5=NULL); // write only
	UINT	GetEffectiveDimension()const{ return m_EffectiveDimension; }
	UINT	GetImageDataSize()const{ return m_SingleImageDataSize; }
	void	Close();
	BOOL	IsOpen(){ return m_DataStream.IsOpen(); }
	__forceinline LPCFLOAT GetResolutionValueTable(int index)const{ return m_pDimensionResolutionValueTable[index]; }
	

	__forceinline BOOL	GetImage_Linear(UINT index,LPVOID pImageOut)
	{
		ASSERT(pImageOut);
		ULONGLONG offset = (ULONGLONG)ImageArrayStart + (ULONGLONG)index*(ULONGLONG)m_SingleImageDataSize;
		if(m_DataStream.Seek(offset) == offset)
		{
			return m_DataStream.Read(pImageOut,m_SingleImageDataSize) == m_SingleImageDataSize;
		}
		else
			return FALSE;
	}
	__forceinline BOOL	SetImage_Linear(UINT index,LPCVOID pImageOut)
	{
		ASSERT(pImageOut);
		ULONGLONG offset = (ULONGLONG)ImageArrayStart + (ULONGLONG)index*(ULONGLONG)m_SingleImageDataSize;
		if(m_DataStream.Seek(offset) == offset)
		{
			return m_DataStream.Write(pImageOut,m_SingleImageDataSize) == m_SingleImageDataSize;
		}
		else
			return FALSE;
	}
	__forceinline BOOL	GetImage_6D(UINT index0,UINT index1,UINT index2,UINT index3,UINT index4,UINT index5,LPVOID pImageOut)
	{
		ASSERT(pImageOut);
		ULONGLONG offset =	(ULONGLONG)index0*(ULONGLONG)m_DimenSteps[0] + 
							(ULONGLONG)index1*(ULONGLONG)m_DimenSteps[1] + 
							(ULONGLONG)index2*(ULONGLONG)m_DimenSteps[2] + 
							(ULONGLONG)index3*(ULONGLONG)m_DimenSteps[3] + 
							(ULONGLONG)index4*(ULONGLONG)m_DimenSteps[4] + 
							(ULONGLONG)index5*(ULONGLONG)m_DimenSteps[5] + 
							(ULONGLONG)ImageArrayStart;
		if(m_DataStream.Seek(offset) == offset)
		{
			return m_DataStream.Read(pImageOut,m_SingleImageDataSize) == m_SingleImageDataSize;
		}
		else
			return FALSE;
	}
	__forceinline BOOL	GetImage_5D(UINT index0,UINT index1,UINT index2,UINT index3,UINT index4,LPVOID pImageOut)
	{
		ASSERT(pImageOut);
		ULONGLONG offset =(ULONGLONG)index0*(ULONGLONG)m_DimenSteps[0] + 
							(ULONGLONG)index1*(ULONGLONG)m_DimenSteps[1] + 
							(ULONGLONG)index2*(ULONGLONG)m_DimenSteps[2] + 
							(ULONGLONG)index3*(ULONGLONG)m_DimenSteps[3] + 
							(ULONGLONG)index4*(ULONGLONG)m_DimenSteps[4] + 
							(ULONGLONG)ImageArrayStart;
		if(m_DataStream.Seek(offset) == offset)
		{
			return m_DataStream.Read(pImageOut,m_SingleImageDataSize) == m_SingleImageDataSize;
		}
		else
			return FALSE;
	}
	__forceinline BOOL	GetImage_4D(UINT index0,UINT index1,UINT index2,UINT index3,LPVOID pImageOut)
	{
		ASSERT(pImageOut);
		ULONGLONG offset =	(ULONGLONG)index0*(ULONGLONG)m_DimenSteps[0] + 
							(ULONGLONG)index1*(ULONGLONG)m_DimenSteps[1] + 
							(ULONGLONG)index2*(ULONGLONG)m_DimenSteps[2] + 
							(ULONGLONG)index3*(ULONGLONG)m_DimenSteps[3] + 
							(ULONGLONG)ImageArrayStart;
		if(m_DataStream.Seek(offset) == offset)
		{
			return m_DataStream.Read(pImageOut,m_SingleImageDataSize) == m_SingleImageDataSize;
		}
		else
			return FALSE;
	}
	__forceinline BOOL	GetImage_3D(UINT index0,UINT index1,UINT index2,LPVOID pImageOut)
	{
		ASSERT(pImageOut);
		ULONGLONG offset =	(ULONGLONG)index0*(ULONGLONG)m_DimenSteps[0] + 
							(ULONGLONG)index1*(ULONGLONG)m_DimenSteps[1] + 
							(ULONGLONG)index2*(ULONGLONG)m_DimenSteps[2] + 
							(ULONGLONG)ImageArrayStart;
		if(m_DataStream.Seek(offset) == offset)
		{
			return m_DataStream.Read(pImageOut,m_SingleImageDataSize) == m_SingleImageDataSize;
		}
		else
			return FALSE;
	}
	__forceinline BOOL	GetImage_2D(UINT index0,UINT index1,LPVOID pImageOut)
	{
		ASSERT(pImageOut);
		ULONGLONG offset =	(ULONGLONG)index0*(ULONGLONG)m_DimenSteps[0] + 
							(ULONGLONG)index1*(ULONGLONG)m_DimenSteps[1] + 
							(ULONGLONG)ImageArrayStart;
		if(m_DataStream.Seek(offset) == offset)
		{
			return m_DataStream.Read(pImageOut,m_SingleImageDataSize) == m_SingleImageDataSize;
		}
		else
			return FALSE;
	}
	__forceinline BOOL	GetImage_1D(UINT index0,LPVOID pImageOut)
	{
		ASSERT(pImageOut);
		ULONGLONG offset =	(ULONGLONG)index0*(ULONGLONG)m_DimenSteps[0] + 
							(ULONGLONG)ImageArrayStart;
		if(m_DataStream.Seek(offset) == offset)
		{
			return m_DataStream.Read(pImageOut,m_SingleImageDataSize) == m_SingleImageDataSize;
		}
		else
			return FALSE;
	}
	__forceinline BOOL	GetImage_0D(LPVOID pImageOut)
	{
		ASSERT(pImageOut);
		if(m_DataStream.Seek(ImageArrayStart) == ImageArrayStart)
		{
			return m_DataStream.Read(pImageOut,m_SingleImageDataSize) == m_SingleImageDataSize;
		}
		else
			return FALSE;
	}
	__forceinline BOOL	SetImage_6D(UINT index0,UINT index1,UINT index2,UINT index3,UINT index4,UINT index5,LPCVOID pImageOut)
	{
		ASSERT(pImageOut);
		ULONGLONG offset =	(ULONGLONG)index0*(ULONGLONG)m_DimenSteps[0] + 
							(ULONGLONG)index1*(ULONGLONG)m_DimenSteps[1] + 
							(ULONGLONG)index2*(ULONGLONG)m_DimenSteps[2] + 
							(ULONGLONG)index3*(ULONGLONG)m_DimenSteps[3] + 
							(ULONGLONG)index4*(ULONGLONG)m_DimenSteps[4] + 
							(ULONGLONG)index5*(ULONGLONG)m_DimenSteps[5] + 
						(ULONGLONG)ImageArrayStart;
		if(m_DataStream.Seek(offset) == offset)
		{
			return m_DataStream.Write(pImageOut,m_SingleImageDataSize) == m_SingleImageDataSize;
		}
		else
			return FALSE;
	}
	__forceinline BOOL	SetImage_5D(UINT index0,UINT index1,UINT index2,UINT index3,UINT index4,LPCVOID pImageOut)
	{
		ASSERT(pImageOut);
		ULONGLONG offset =	(ULONGLONG)index0*(ULONGLONG)m_DimenSteps[0] + 
							(ULONGLONG)index1*(ULONGLONG)m_DimenSteps[1] + 
							(ULONGLONG)index2*(ULONGLONG)m_DimenSteps[2] + 
							(ULONGLONG)index3*(ULONGLONG)m_DimenSteps[3] + 
							(ULONGLONG)index4*(ULONGLONG)m_DimenSteps[4] + 
							(ULONGLONG)ImageArrayStart;
		if(m_DataStream.Seek(offset) == offset)
		{
			return m_DataStream.Write(pImageOut,m_SingleImageDataSize) == m_SingleImageDataSize;
		}
		else
			return FALSE;
	}
	__forceinline BOOL	SetImage_4D(UINT index0,UINT index1,UINT index2,UINT index3,LPCVOID pImageOut)
	{
		ASSERT(pImageOut);
		ULONGLONG offset =	(ULONGLONG)index0*(ULONGLONG)m_DimenSteps[0] + 
							(ULONGLONG)index1*(ULONGLONG)m_DimenSteps[1] + 
							(ULONGLONG)index2*(ULONGLONG)m_DimenSteps[2] + 
							(ULONGLONG)index3*(ULONGLONG)m_DimenSteps[3] + 
							(ULONGLONG)ImageArrayStart;
		if(m_DataStream.Seek(offset) == offset)
		{
			return m_DataStream.Write(pImageOut,m_SingleImageDataSize) == m_SingleImageDataSize;
		}
		else
			return FALSE;
	}
	__forceinline BOOL	SetImage_3D(UINT index0,UINT index1,UINT index2,LPCVOID pImageOut)
	{
		ASSERT(pImageOut);
		ULONGLONG offset =	(ULONGLONG)index0*(ULONGLONG)m_DimenSteps[0] + 
							(ULONGLONG)index1*(ULONGLONG)m_DimenSteps[1] + 
							(ULONGLONG)index2*(ULONGLONG)m_DimenSteps[2] + 
							(ULONGLONG)ImageArrayStart;
		if(m_DataStream.Seek(offset) == offset)
		{
			return m_DataStream.Write(pImageOut,m_SingleImageDataSize) == m_SingleImageDataSize;
		}
		else
			return FALSE;
	}
	__forceinline BOOL	SetImage_2D(UINT index0,UINT index1,LPCVOID pImageOut)
	{
		ASSERT(pImageOut);
		ULONGLONG offset =	(ULONGLONG)index0*(ULONGLONG)m_DimenSteps[0] + 
							(ULONGLONG)index1*(ULONGLONG)m_DimenSteps[1] + 
							(ULONGLONG)ImageArrayStart;
		if(m_DataStream.Seek(offset) == offset)
		{
			return m_DataStream.Write(pImageOut,m_SingleImageDataSize) == m_SingleImageDataSize;
		}
		else
			return FALSE;
	}
	__forceinline BOOL	SetImage_1D(UINT index0,LPCVOID pImageOut)
	{
		ASSERT(pImageOut);
		ULONGLONG offset =	(ULONGLONG)index0*(ULONGLONG)m_DimenSteps[0] + 
							(ULONGLONG)ImageArrayStart;
		if(m_DataStream.Seek(offset) == offset)
		{
			return m_DataStream.Write(pImageOut,m_SingleImageDataSize) == m_SingleImageDataSize;
		}
		else
			return FALSE;
	}
	__forceinline BOOL	SetImage_0D(LPCVOID pImageOut)
	{
		ASSERT(pImageOut);
		if(m_DataStream.Seek(ImageArrayStart) == ImageArrayStart)
		{
			return m_DataStream.Write(pImageOut,m_SingleImageDataSize) == m_SingleImageDataSize;
		}
		else
			return FALSE;
	}
};


#define DefineHelperFunctions(dimen_name,value_name,value_param,index)	\
	__forceinline UINT Get##dimen_name() const{ return DimensionResolution[index]; } \
	__forceinline BOOL IsExist_##value_name() const{ return m_pDimensionResolutionValueTable[index]; } \
	__forceinline float Get##value_name(UINT value_param) const{ ASSERT(m_pDimensionResolutionValueTable[index]); return m_pDimensionResolutionValueTable[index][value_param]; } \
	__forceinline LPCFLOAT GetTable##value_name() const{return m_pDimensionResolutionValueTable[index]; }
		

//////////////////////////////////////////
/// class for Single image 
/// "SNGL" 2D	image data (0D image array)
template<typename tValueType,class ImagePackBase=ipp::file::CImagePackage>
class CSingleImage:public ImagePackBase
{
public:
	HRESULT	Open(LPCTSTR fn)
	{
		HRESULT re = __super::Open(fn);
		if(SUCCEEDED(re))
		{
			if(m_SingleImageDataSize > 0)
				return re;
			else
				return E_ABORT;
		}

		return re;
	}
	template<class t_Image>
	HRESULT Create(LPCTSTR fn,const t_Image& img)
	{
		return Create(	fn,img.GetWidth(),img.GetHeight(),img.GetStep(),img.GetChannels());
	}
	HRESULT Create(LPCTSTR fn,UINT width,UINT height,UINT BytePerScan,UINT channel)
	{
		CImagePackage_FileHeader h;
		h.ImageWidth = width;
		h.ImageHeight = height;
		h.Channel = channel;
		h.BytePerChannel = sizeof(tValueType);
		h.BytePerScanline = BytePerScan;
		h.FileType = IMPK_FileType_FRAME;

		HRESULT re = __super::Create(fn,&h);
		return re;
	}
	
	__forceinline BOOL SetImage(const tValueType * pImgData)
	{
		return SetImage_0D(pImgData);
	}

	__forceinline BOOL GetImage(tValueType * pImgData)
	{
		return GetImage_0D(pImgData);
	}
};

typedef CSingleImage<BYTE>	CSingleImage_LDR;
typedef CSingleImage<float>	CSingleImage_HDR;

//////////////////////////////////////////
/// class for image Sequence
/// "SCAN" 3D	image sequence data (1D image array)
template<typename tValueType,class ImagePackBase=ipp::file::CImagePackage>
class CSequenceImage:public ImagePackBase
{
public:
	HRESULT	Open(LPCTSTR fn)
	{
		HRESULT re = __super::Open(fn);
		if(SUCCEEDED(re))
		{
			if(m_SingleImageDataSize > 0 && Dimension>=1)
				return re;
			else
				return E_ABORT;
		}

		return re;
	}
	template<class t_Image>
	HRESULT Create(LPCTSTR fn,const t_Image& img,UINT FrameCount,const float* Timestamp = NULL)
	{
		return Create(	fn,img.GetWidth(),img.GetHeight(),img.GetStep(),img.GetChannels(),
						FrameCount,Timestamp);
	}
	HRESULT Create(LPCTSTR fn,UINT width,UINT height,UINT BytePerScan,UINT channel,UINT FrameCount,const float* Timestamp = NULL)
	{
		CImagePackage_FileHeader h;
		h.ImageWidth = width;
		h.ImageHeight = height;
		h.Channel = channel;
		h.BytePerChannel = sizeof(tValueType);
		h.BytePerScanline = BytePerScan;
		h.FileType = IMPK_FileType_FRAMESEQ;

		h.Dimension = 1;
		h.DimensionResolution[0] = FrameCount;

		HRESULT re = __super::Create(fn,&h,Timestamp);
		return re;
	}
	
	__forceinline BOOL SetImage(const tValueType * pImgData,UINT frameid)
	{
		return SetImage_1D(frameid,pImgData);
	}

	__forceinline BOOL GetImage(tValueType * pImgData,UINT frameid)
	{
		return GetImage_1D(frameid,pImgData);
	}

	DefineHelperFunctions(FrameCount,Timestamp,FrameId,0)
};

typedef CSequenceImage<BYTE>	CSequenceImage_LDR;
typedef CSequenceImage<float>	CSequenceImage_HDR;

//////////////////////////////////////////
/// class for View-dependent image
/// "VDM " 4D	View-dependent image data (2D image array)
template<typename tValueType,class ImagePackBase=ipp::file::CImagePackage>
class CViewDependentImage:public ImagePackBase
{
public:
	HRESULT	Open(LPCTSTR fn)
	{
		HRESULT re = __super::Open(fn);
		if(SUCCEEDED(re))
		{
			if(m_SingleImageDataSize > 0 && Dimension>=2)
				return re;
			else
				return E_ABORT;
		}

		return re;
	}
	template<class t_Image>
	HRESULT Create(LPCTSTR fn,const t_Image& img,UINT PolarRes,UINT AzimuthRes,const float* pPolarAngles = NULL,const float* pAzimuthAngles = NULL)
	{
		return Create(	fn,img.GetWidth(),img.GetHeight(),img.GetStep(),img.GetChannels(),
						PolarRes,AzimuthRes,pPolarAngles,pAzimuthAngles);
	}
	HRESULT Create(LPCTSTR fn,UINT width,UINT height,UINT BytePerScan,UINT channel,UINT PolarRes,UINT AzimuthRes,const float* pPolarAngles = NULL,const float* pAzimuthAngles = NULL)
	{
		CImagePackage_FileHeader h;
		h.ImageWidth = width;
		h.ImageHeight = height;
		h.Channel = channel;
		h.BytePerChannel = sizeof(tValueType);
		h.BytePerScanline = BytePerScan;
		h.FileType = IMPK_FileType_BRDF;

		h.Dimension = 2;
		h.DimensionResolution[0] = PolarRes;
		h.DimensionResolution[1] = AzimuthRes;
 
		HRESULT re = __super::Create(fn,&h,pPolarAngles,pAzimuthAngles);
		return re;
	}
	
	__forceinline BOOL SetImage(const tValueType * pImgData,UINT PolarId,UINT AzimuthId)
	{
		return SetImage_2D(PolarId,AzimuthId,pImgData);
	}

	__forceinline BOOL GetImage(tValueType * pImgData,UINT PolarId,UINT AzimuthId)
	{
		return GetImage_2D(PolarId,AzimuthId,pImgData);
	}

	DefineHelperFunctions(PolarRes,PolarAngle,PolarId,0)
	DefineHelperFunctions(AzimuthRes,AzimuthAngle,AzimuthId,1)
};

typedef CViewDependentImage<BYTE>	CViewDependentImage_LDR;
typedef CViewDependentImage<float>	CViewDependentImage_HDR;


//////////////////////////////////////////
/// class for BTF
/// "BTF " 6D	BTF data (4D image array)
/// Light x View
template<typename tValueType,class ImagePackBase=ipp::file::CImagePackage>
class CBtfImage_LV:public ImagePackBase
{
public:
	HRESULT	Open(LPCTSTR fn)
	{
		HRESULT re = __super::Open(fn);
		if(SUCCEEDED(re))
		{
			if(m_SingleImageDataSize > 0 && Dimension>=4)
				return re;
			else
				return E_ABORT;
		}

		return re;
	}
	template<class t_Image>
	HRESULT Create(LPCTSTR fn,const t_Image& img,
					UINT LightPolarRes,UINT LightAzimuthRes,UINT ViewPolarRes,UINT ViewAzimuthRes,
					const float* pLightPolarAngles = NULL,const float* pLightAzimuthAngles = NULL,
					const float* pViewPolarAngles = NULL,const float* pViewAzimuthAngles = NULL
					)
	{
		return Create(	fn,img.GetWidth(),img.GetHeight(),img.GetStep(),img.GetChannels(),
						LightPolarRes,LightAzimuthRes,ViewPolarRes,ViewAzimuthRes,
						pLightPolarAngles,pLightAzimuthAngles,
						pViewPolarAngles,pViewAzimuthAngles);
	}
	HRESULT Create(LPCTSTR fn,UINT width,UINT height,UINT BytePerScan,UINT channel,
					UINT LightPolarRes,UINT LightAzimuthRes,UINT ViewPolarRes,UINT ViewAzimuthRes,
					const float* pLightPolarAngles = NULL,const float* pLightAzimuthAngles = NULL,
					const float* pViewPolarAngles = NULL,const float* pViewAzimuthAngles = NULL
					)
	{
		CImagePackage_FileHeader h;
		h.ImageWidth = width;
		h.ImageHeight = height;
		h.Channel = channel;
		h.BytePerChannel = sizeof(tValueType);
		h.BytePerScanline = BytePerScan;
		h.FileType = IMPK_FileType_BTF;

		h.Dimension = 4;
		h.DimensionResolution[0] = LightPolarRes;
		h.DimensionResolution[1] = LightAzimuthRes;
		h.DimensionResolution[2] = ViewPolarRes;
		h.DimensionResolution[3] = ViewAzimuthRes;

		HRESULT re = __super::Create(fn,&h,pLightPolarAngles,pLightAzimuthAngles,pViewPolarAngles,pViewAzimuthAngles);
		return re;
	}
	
	__forceinline BOOL SetImage(const tValueType * pImgData,UINT LightPolarId,UINT LightAzimuthId,
															UINT ViewPolarId,UINT ViewAzimuthId)
	{
		return SetImage_4D(LightPolarId,LightAzimuthId,ViewPolarId,ViewAzimuthId,pImgData);
	}

	__forceinline BOOL GetImage(tValueType * pImgData,UINT LightPolarId,UINT LightAzimuthId,
													UINT ViewPolarId,UINT ViewAzimuthId)
	{
		return GetImage_4D(LightPolarId,LightAzimuthId,ViewPolarId,ViewAzimuthId,pImgData);
	}

	DefineHelperFunctions(LightPolarRes,LightPolarAngle,LightPolarId,0)
	DefineHelperFunctions(LightAzimuthRes,LightAzimuthAngle,LightAzimuthId,1)
	DefineHelperFunctions(ViewPolarRes,ViewPolarAngle,ViewPolarId,2)
	DefineHelperFunctions(ViewAzimuthRes,ViewAzimuthAngle,ViewAzimuthId,3)
};

typedef CBtfImage_LV<BYTE>		CBtfImage_LV_LDR;
typedef CBtfImage_LV<float>		CBtfImage_LV_HDR;
typedef CBtfImage_LV<BYTE>		CBtfImage_LDR;	//compatible with quasi code
typedef CBtfImage_LV<float>		CBtfImage_HDR;	//compatible with quasi code

//////////////////////////////////////////
/// class for BTF
/// "BTF " 6D	BTF data (4D image array)
/// View x Light
template<typename tValueType,class ImagePackBase=ipp::file::CImagePackage>
class CBtfImage_VL:public ImagePackBase
{
public:
    HRESULT	Open(LPCTSTR fn,UINT openFlag = w32::CFile64::Normal_Read)
	{
		HRESULT re = __super::Open(fn,openFlag);
		if(SUCCEEDED(re))
		{
			if(m_SingleImageDataSize > 0 && Dimension>=4)
				return re;
			else
				return E_ABORT;
		}

		return re;
	}
	template<class t_Image>
	HRESULT Create(LPCTSTR fn,const t_Image& img,
					UINT LightPolarRes,UINT LightAzimuthRes,UINT ViewPolarRes,UINT ViewAzimuthRes,
					const float* pLightPolarAngles = NULL,const float* pLightAzimuthAngles = NULL,
					const float* pViewPolarAngles = NULL,const float* pViewAzimuthAngles = NULL
					)
	{
		return Create(	fn,img.GetWidth(),img.GetHeight(),img.GetStep(),img.GetChannels(),
						LightPolarRes,LightAzimuthRes,ViewPolarRes,ViewAzimuthRes,
						pLightPolarAngles,pLightAzimuthAngles,
						pViewPolarAngles,pViewAzimuthAngles);
	}
	HRESULT Create(LPCTSTR fn,UINT width,UINT height,UINT BytePerScan,UINT channel,
					UINT LightPolarRes,UINT LightAzimuthRes,UINT ViewPolarRes,UINT ViewAzimuthRes,
					const float* pLightPolarAngles = NULL,const float* pLightAzimuthAngles = NULL,
					const float* pViewPolarAngles = NULL,const float* pViewAzimuthAngles = NULL
					)
	{
		CImagePackage_FileHeader h;
		h.ImageWidth = width;
		h.ImageHeight = height;
		h.Channel = channel;
		h.BytePerChannel = sizeof(tValueType);
		h.BytePerScanline = BytePerScan;
		h.FileType = IMPK_FileType_BTF;

		h.Dimension = 4;
		h.DimensionResolution[0] = ViewPolarRes;
		h.DimensionResolution[1] = ViewAzimuthRes;
		h.DimensionResolution[2] = LightPolarRes;
		h.DimensionResolution[3] = LightAzimuthRes;

		HRESULT re = __super::Create(fn,&h,pLightPolarAngles,pLightAzimuthAngles,pViewPolarAngles,pViewAzimuthAngles);
		return re;
	}
	
	__forceinline BOOL SetImage(const tValueType * pImgData,UINT ViewPolarId,UINT ViewAzimuthId,
															UINT LightPolarId,UINT LightAzimuthId)
	{
		return SetImage_4D(ViewPolarId,ViewAzimuthId,LightPolarId,LightAzimuthId,pImgData);
	}

	__forceinline BOOL GetImage(tValueType * pImgData,	UINT ViewPolarId,UINT ViewAzimuthId,
														UINT LightPolarId,UINT LightAzimuthId)
	{
		return GetImage_4D(ViewPolarId,ViewAzimuthId,LightPolarId,LightAzimuthId,pImgData);
	}

	DefineHelperFunctions(ViewPolarRes,ViewPolarAngle,ViewPolarId,0)
	DefineHelperFunctions(ViewAzimuthRes,ViewAzimuthAngle,ViewAzimuthId,1)
	DefineHelperFunctions(LightPolarRes,LightPolarAngle,LightPolarId,2)
	DefineHelperFunctions(LightAzimuthRes,LightAzimuthAngle,LightAzimuthId,3)
};

typedef CBtfImage_VL<BYTE>		CBtfImage_VL_LDR;
typedef CBtfImage_VL<float>		CBtfImage_VL_HDR;




/////////////////////////////////////////////
/// class for BTF Image sequence
/// "BTFS" 7D	BTF scan data (5D image array)
template<typename tValueType,class ImagePackBase=ipp::file::CImagePackage>
class CBtfSequenceImage:public ImagePackBase
{
public:
	HRESULT	Open(LPCTSTR fn)
	{
		HRESULT re = __super::Open(fn);
		if(SUCCEEDED(re))
		{
			if(m_SingleImageDataSize > 0 && Dimension>=5){}
			else
				return E_ABORT;
		}

		return re;
	}
	template<class t_Image>
	HRESULT Create(LPCTSTR fn,const t_Image& img,
					UINT LightPolarRes,UINT LightAzimuthRes,UINT ViewPolarRes,UINT ViewAzimuthRes,UINT FrameCount,
					const float* pLightPolarAngles = NULL,const float* pLightAzimuthAngles = NULL,
					const float* pViewPolarAngles = NULL,const float* pViewAzimuthAngles = NULL,const float* Timestemp = NULL
					)
	{
		return Create(	fn,img.GetWidth(),img.GetHeight(),img.GetStep(),img.GetChannels(),
						LightPolarRes,LightAzimuthRes,ViewPolarRes,ViewAzimuthRes,FrameCount,
						pLightPolarAngles,pLightAzimuthAngles,
						pViewPolarAngles,pViewAzimuthAngles,Timestemp);
	}
	HRESULT Create(LPCTSTR fn,UINT width,UINT height,UINT BytePerScan,UINT channel,
					UINT LightPolarRes,UINT LightAzimuthRes,UINT ViewPolarRes,UINT ViewAzimuthRes,UINT FrameCount,
					const float* pLightPolarAngles = NULL,const float* pLightAzimuthAngles = NULL,
					const float* pViewPolarAngles = NULL,const float* pViewAzimuthAngles = NULL,const float* Timestemp = NULL
					)
	{
		CImagePackage_FileHeader h;

		h.ImageWidth = width;
		h.ImageHeight = height;
		h.Channel = channel;
		h.BytePerChannel = sizeof(tValueType);
		h.BytePerScanline = BytePerScan;
		h.FileType = IMPK_FileType_BTFSEQ;

		h.Dimension = 5;
		h.DimensionResolution[0] = LightPolarRes;
		h.DimensionResolution[1] = LightAzimuthRes;
		h.DimensionResolution[2] = ViewPolarRes;
		h.DimensionResolution[3] = ViewAzimuthRes;
		h.DimensionResolution[4] = FrameCount;

		HRESULT re = __super::Create(fn,&h,pLightPolarAngles,pLightAzimuthAngles,pViewPolarAngles,pViewAzimuthAngles,Timestemp);
		return re;
	}
	
	__forceinline BOOL SetImage(const tValueType * pImgData,UINT LightPolarId,UINT LightAzimuthId,
															UINT ViewPolarId,UINT ViewAzimuthId,UINT FrameIndex)
	{
		return SetImage_5D(LightPolarId,LightAzimuthId,ViewPolarId,ViewAzimuthId,FrameIndex,pImgData);
	}

	__forceinline BOOL GetImage(tValueType * pImgData,UINT LightPolarId,UINT LightAzimuthId,
													UINT ViewPolarId,UINT ViewAzimuthId,UINT FrameIndex)
	{
		return GetImage_5D(LightPolarId,LightAzimuthId,ViewPolarId,ViewAzimuthId,FrameIndex,pImgData);
	}

	DefineHelperFunctions(LightPolarRes,LightPolarAngle,LightPolarId,0)
	DefineHelperFunctions(LightAzimuthRes,LightAzimuthAngle,LightAzimuthId,1)
	DefineHelperFunctions(ViewPolarRes,ViewPolarAngle,ViewPolarId,2)
	DefineHelperFunctions(ViewAzimuthRes,ViewAzimuthAngle,ViewAzimuthId,3)
	DefineHelperFunctions(FrameCount,Timestamp,FrameId,4)
};

typedef CBtfSequenceImage<BYTE>		CBtfSequenceImage_LDR;
typedef CBtfSequenceImage<float>	CBtfSequenceImage_HDR;



#undef DefineHelperFunctions

} // namespace file
} // namespace ipp


