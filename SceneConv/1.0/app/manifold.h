#pragma once

//////////////////////////////////////////////////////////////////////
// Visual Computing Foundation Classes (VCFC)
//					Jiaping Wang  2006.2
//					e_boris2002@hotmail.com
//
// (C) Copyright Jiaping Wang 2004.
//
// Permission to copy, use, modify, sell and distribute this software
// is granted provided this copyright notice appears in all copies.
// This software is provided "as is" without express or implied
// warranty, and with no claim as to its suitability for any purpose.
//
// Absolutely no warranty!!
//////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////
//  Brief Description
//
//  manifold.h
//
//////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////
// Revise History
//
// Inital version	2006.8.12		Jiaping
//
//////////////////////////////////////////////////////////////////////

#include "..\rt\compact_vector.h"
#include "..\num\geodesic.h"
#include "..\num\ann_cpp.h"
#include "..\num\small_vec.h"
#include <list>
#include <vector>
#include <hash_map>
#include <algorithm>
#include "..\w32\prog_idct.h"
#include "..\mkl\mkl_matrix.h"


namespace nldr
{

template<typename t_Value = float, typename t_Distance = t_Value>
class CEuclideanSpace
{	ASSERT_STATIC( ::rt::TypeTraits<t_Value>::Typeid == ::rt::_typeid_32f ); // support float type only
protected:
	typedef t_Distance			DISTANCE_TYPE;

	::rt::Buffer<t_Value>		m_NodeList;
	UINT						m_Dimen;
	
	num::ANN_Tree				m_SearchTree;
	
protected:
	DYNTYPE_FUNC t_Distance Distance_Sqr(const t_Value* v1, const t_Value* v2) const
	{	ASSERT(m_Dimen);
		t_Distance tot_dist = ::rt::Sqr(v1[0] - v2[0]);
		for(UINT q=1;q<m_Dimen;q++)tot_dist += ::rt::Sqr(v1[q] - v2[q]);
		return tot_dist;
	}
public:
	CEuclideanSpace(){ m_Dimen = 0; }
	DYNTYPE_FUNC BOOL SetPointCount(UINT co,UINT dimen){ m_Dimen = dimen; return m_NodeList.SetSize(co*m_Dimen); }
	DYNTYPE_FUNC UINT GetPointCount() const{ return m_NodeList.GetSize()/m_Dimen; }
	DYNTYPE_FUNC t_Value* GetPoint(UINT i){ return &m_NodeList[i*m_Dimen]; }
	DYNTYPE_FUNC const t_Value* GetPoint(UINT i) const{ return &m_NodeList[i*m_Dimen]; }

	DYNTYPE_FUNC void SetSearchNeighborhoodCount(UINT x){ m_SearchTree.SetResultSlots(x); }
	DYNTYPE_FUNC void Search(const t_Value* x){ m_SearchTree.Query(x); }
	DYNTYPE_FUNC UINT GetSearchResult_Index(UINT i){ return m_SearchTree.GetResultIndex(i); }
	DYNTYPE_FUNC t_Distance GetSearchResult_DistanceSqr(UINT i){ return m_SearchTree.GetResultDistance(i); }

	BOOL Finalize(BOOL Resample = TRUE, UINT sample_count_max = UINT_MAX, t_Distance distorion_max = EPSILON)
	{	UINT node_count = GetPointCount();
		if(Resample)
		{	
			_CheckDump(_T("\tRemove duplication.\n"));
			::std::vector<t_Value*> gathered;
			{	::rt::Buffer<UINT> shuffle_index;
				shuffle_index.SetSize(node_count);
				UINT i=0;
				for(;i<node_count;i++)shuffle_index[i] = i;
				shuffle_index.Shuffle();

				t_Distance distorion_sqr = distorion_max*distorion_max;
				i=0;
				w32::CTimeMeasureDisplay<UINT> tm(&i,node_count);
				for(;i<node_count;)
				{	t_Value* new_pt = GetPoint(shuffle_index[i]);
					for(UINT j=0;j<gathered.size();j++) //check unique
						if( Distance_Sqr(new_pt,gathered[j]) > distorion_sqr ){}
						else{ goto NextPoint; }
					gathered.push_back(new_pt);
					if( sample_count_max == gathered.size() )break;
NextPoint:			i++;
				}
			}
			_CheckDump(_T("\t\tDuplication removed, ")<<GetPointCount()<<_T("->")<<gathered.size()<<_T('\n'));
			::rt::Buffer<t_Value>	old_node_list;
			::rt::Swap(old_node_list,m_NodeList);
			SetPointCount((UINT)gathered.size(),m_Dimen);
			for(UINT i=0;i<gathered.size();i++)
				memcpy(GetPoint(i),gathered[i],sizeof(t_Value)*m_Dimen);
		}
		m_SearchTree.SetupSearchSpace(m_NodeList,m_Dimen,GetPointCount());
		return TRUE;
	}
};

template<typename t_Distance = float>
class CManifoldSpace
{
private:
	struct _Peer{ UINT index; t_Distance distance_sqr; };

public:
	class Link:public num::Dijsktra<t_Distance>::DirectionalLink
	{	public:
		DYNTYPE_FUNC BOOL operator <(const Link& x){ return vertexFrom<x.vertexFrom; }
	};
	UINT		GetLinkCount() const{ return m_LinkList.GetSize(); }
	Link&		GetLink(UINT i){ return m_LinkList[i]; }
	const Link&	GetLink(UINT i) const{ return m_LinkList[i]; }

	UINT GetPointLinkCount(UINT i) const{ return m_Geodesic.GetNodeEdgeCount(i); }
	const Link* GetPointLinks(UINT i) const{ return (const Link*)m_Geodesic.GetNodeEdges(i); }
	Link* GetPointLinks(UINT i){ return (Link*)m_Geodesic.GetNodeEdges(i); }

protected:
	rt::Buffer<Link>			m_LinkList;
	num::Dijsktra<t_Distance>	m_Geodesic;

protected:
	void PackLinkList(const std::list<_Peer> *pLinkLists,UINT node_co,UINT tot_link_co)
	{	ASSERT( (tot_link_co&1) == 0);
		UINT eid = 0;
		m_LinkList.SetSize(tot_link_co);
        for(UINT i=0;i<node_co;i++)
		{	std::list<_Peer>::const_iterator p = pLinkLists[i].begin();
			std::list<_Peer>::const_iterator end = pLinkLists[i].end();
			for(;p!=end;p++,eid++)
			{	m_LinkList[eid].vertexFrom = i;
				m_LinkList[eid].vertexTo = p->index;
				m_LinkList[eid].weight = sqrt(p->distance_sqr);
		}	}
		ASSERT(eid == tot_link_co);
		m_Geodesic.SetGraph(m_LinkList,m_LinkList.GetSize(),node_co);
	}

	void SortAndPackLinkList(std::vector<Link>& link_list,UINT node_co)
	{
		std::sort(link_list.begin(),link_list.end());
		m_LinkList.SetSize((UINT)link_list.size());
		for(UINT i=0;i<link_list.size();i++)
		{
			m_LinkList[i] = link_list[i];
		}
		m_Geodesic.SetGraph(m_LinkList,m_LinkList.GetSize(),node_co);
	}
	
public:
	UINT GetPointCount() const{ return m_Geodesic.GetNodeCount(); }

	void BuildUndirectionalGraph(const num::Vec2i* pEdges,const t_Distance* pWeights, UINT edges_co, UINT node_co )
	{
		std::vector<Link> links;
		links.reserve(edges_co*2);
		for(UINT i=0;i<edges_co;i++)
		{
			Link a;
			a.vertexFrom = pEdges[i].x;
			a.vertexTo = pEdges[i].y;
			a.weight = pWeights[i];
			links.push_back(a);
			::rt::Swap(a.vertexFrom,a.vertexTo);
			links.push_back(a);
		}
		SortAndPackLinkList(links,node_co);
	}


	template<typename t_value>
	void BuildNeighborhoodGraph(nldr::CEuclideanSpace<t_value,t_Distance>& space, UINT k_Nearest)
	{	UINT NodeCount = space.GetPointCount();
		::rt::Buffer<std::list<_Peer> > AllEdges;
		AllEdges.SetSize(NodeCount);
		//K-nearest connection (k-isomap)
		::rt::TypeTraits<t_Distance>::t_Accum total_dist_sqr = 0;
		UINT EdgeCount = 0;
		{	UINT Init_K = (UINT)(k_Nearest*3.0f + 1.5f);
			space.SetSearchNeighborhoodCount(Init_K+1);
			for(UINT i=0;i<NodeCount;i++)
			{	space.Search(space.GetPoint(i));	//search neighborhood of node i
				ASSERT(space.GetSearchResult_Index(0) == i); //nearest node should be itself, 
				// otherwise the feature space contains identical points which is not allowed
				for(UINT j=1;j<=Init_K;j++)
				{	int idx = space.GetSearchResult_Index(j);
					_Peer pp;
					pp.index = idx;
					pp.distance_sqr = space.GetSearchResult_DistanceSqr(j);
					total_dist_sqr += pp.distance_sqr;

					{/// descending distance 
						std::list<_Peer>::iterator p = AllEdges[i].begin();
						for(;p!=AllEdges[i].end();p++)
						{	if( p->index != idx )  // check if already added
								if( p->distance_sqr < pp.distance_sqr )break;
							else
								goto ProcessNextPeer;
						}
						AllEdges[i].insert(p,pp);
					}
					pp.index = i; //also add to its peer's list
					{/// descending distance 
						std::list<_Peer>::iterator p = AllEdges[idx].begin();
						for(;p!=AllEdges[idx].end();p++)
						{
							if( p->distance_sqr < pp.distance_sqr )break;
						}
						AllEdges[idx].insert(p,pp);
					}
					EdgeCount++;
ProcessNextPeer:
					continue;
		}	}	}
		_CheckDump(_T("\tInitial ")<<k_Nearest<<_T("-nearest connection create ")<<EdgeCount<<_T(" edges.\n"));
		t_Distance E_dist_sqr = (t_Distance)(total_dist_sqr/(EdgeCount*2));
		//Remove long-length edges (e-Isomap)
		{	int Link_Min = (int)max(2,k_Nearest*0.8f);
			for(UINT i=0;i<NodeCount;i++)
			{	std::list<_Peer>& edges = AllEdges[i];
				int Total_Link = (int)edges.size();
				std::list<_Peer>::iterator p = edges.begin();
				while(	Total_Link > Link_Min && 
						p != edges.end() &&
						p->distance_sqr > E_dist_sqr )
				{	// try to remove p
					if( (int)AllEdges[p->index].size() > Link_Min )
					{	std::list<_Peer>& peer_edges = AllEdges[p->index];
						std::list<_Peer>::iterator pr = peer_edges.begin();
						for(;pr!=peer_edges.end();pr++)
						{	if(pr->index!=i){}
							else // remove this link
							{	peer_edges.erase(pr);
								std::list<_Peer>::iterator p_org = p;  p++;
								edges.erase(p_org);
								EdgeCount--;
								Total_Link--;
								break;
							}
						}
					}
					else{ p++; }
		}	}	}
		_CheckDump(_T('\t')<<EdgeCount<<_T(" edges remain after removing long-distance edges. K'=")<<EdgeCount*2.0f/NodeCount<<_T('\n'));
		PackLinkList(AllEdges,NodeCount,EdgeCount*2);
	}

	__forceinline static t_Distance InfinitDistance(){ return num::Dijsktra<t_Distance>::InfinitDistance(); }
	UINT DetectConnentedComponents(UINT* pLabels = NULL) const // return number of Connected Components, pLabels[i] = surgraph id
	
	{	ASSERT(m_Geodesic.GetNodeCount());
		UINT nodeco = GetPointCount();

		::rt::Buffer<UINT> label;
		if( pLabels ){ ASSERT_ARRAY(pLabels,nodeco); }
		else
		{	VERIFY(label.SetSize(nodeco));
			pLabels = label;
		}
		memset(pLabels,0xff,sizeof(UINT)*nodeco);

		::rt::Buffer<t_Distance>	dist_list;
		dist_list.SetSize( GetPointCount() );

		UINT seed = 0;
		UINT subgraph_id = 0;
		for(;seed != UINT_MAX;subgraph_id++)
		{
			dist_list.Set(m_Geodesic.InfinitDistance());
			::rt::_CastToNonconst(this)->m_Geodesic.ProcessSingleSource(seed,dist_list);
			seed = UINT_MAX;

			for(UINT i=0;i<nodeco;i++)
			{	if( dist_list[i] < m_Geodesic.InfinitDistance() )
					pLabels[i] = subgraph_id;
				else if( pLabels[i] != 0xffffffff ){}
					 else{ seed = i; }
			}
		}

		return subgraph_id;
	}

	template<class t_Matrix>
	void CalculateDistanceMatrix(t_Matrix & matrix) const
	{	UINT nodeco = GetPointCount();
		VERIFY(matrix.SetSize(nodeco,nodeco));
		::rt::_CastToNonconst(this)->m_Geodesic.ProcessAllPairs(matrix);
	}

	template<class t_Vector>
	void CalculateDistanceVector(UINT from_id, t_Vector & vector) const
	{	UINT nodeco = GetPointCount();
		VERIFY(vector.SetSize(nodeco));
		::rt::_CastToNonconst(this)->m_Geodesic.ProcessSingleSource(from_id,vector);
	}

	template<class t_Matrix, class t_JumpTable>
	void CalculateDistanceMatrix(t_Matrix & matrix,t_JumpTable & jump_table) const
	{	
		CalculateDistanceMatrix(matrix);
		//// Setup Jump table
		{
			UINT pt_co = GetPointCount();
			VERIFY(jump_table.SetSize(pt_co,pt_co));
			UINT i=0;
			w32::CTimeMeasureDisplay<UINT> tm(&i,pt_co-1);
			for(;i<pt_co;i++)
			{	::rt::Buffer_Ref<float> distance(matrix.GetLine(i),pt_co);
				ASSERT( distance[i] < EPSILON );
				ASSERT( distance.Max() < num::Dijsktra<float>::InfinitDistance() );
				for(UINT j=0;j<pt_co;j++)
				{
					if(i!=j)
					{	const Link* p = GetPointLinks(j);
						const Link* end = &p[GetPointLinkCount(j)];

						UINT nearest_id;
						float  Dist_Min = FLT_MAX;
						for(UINT e=0;p<end;p++,e++)
						{
							ASSERT(p->vertexFrom == j);
							float dist = distance[p->vertexTo] + p->weight;
							if(dist < Dist_Min)
							{	nearest_id = e;
								Dist_Min = dist;
							}
						}

						ASSERT( ::rt::IsInRange_OO(distance[j]-Dist_Min,-EPSILON,EPSILON) );
						ASSERT( nearest_id>=0 && nearest_id<255 );
						jump_table.At(j,i) = nearest_id;
					}
					else
					{
						jump_table.At(i,j) = i;
					}
				}
			}
		}
	}
};

template<typename t_Distance = float>
class CGeodesicSpace:public CManifoldSpace<t_Distance>
{	
protected:
public:
	::rt::Image<t_Distance>				m_DistanceMatrix;
	::rt::Image<BYTE>					m_JumpTable;

public:
	void UpdateGeodesicSpace()
	{	
		CalculateDistanceMatrix(m_DistanceMatrix,m_JumpTable);
	}

	template<class T>
	void MarkGeodesicLine(UINT from,UINT to,T& pLabel) const
	{	ASSERT(from<GetPointCount());
		ASSERT(to<GetPointCount());
		do
		{	pLabel[from] = TRUE;
			const nldr::CManifoldSpace<>::Link& Link = GetPointLinks(from)[m_JumpTable(from,to)];
			ASSERT(Link.vertexFrom == from);
			from = Link.vertexTo;
		}while(from != to);
		pLabel[to] = TRUE;
	}

	void TraceGeodesicLine(UINT from,UINT to,::rt::BufferEx<UINT> &path) const
	{	ASSERT(from<GetPointCount());
		ASSERT(to<GetPointCount());path.reserve(2);
		path.ChangeSize(1);
		path[0] = from;
		do
		{	const nldr::CManifoldSpace<>::Link& Link = GetPointLinks(from)[m_JumpTable(from,to)];
			ASSERT(Link.vertexFrom == from);
			from = Link.vertexTo;
			path.push_back(from);
		}while(from != to);
	}

	t_Distance Distance(UINT i,UINT j) const{ return m_DistanceMatrix(i,j); }
};



}

