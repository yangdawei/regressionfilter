#pragma once

//////////////////////////////////////////////////////////////////////
// Visual Computing Foundation Classes (VCFC)
//					Jiaping Wang  2006.2
//					e_boris2002@hotmail.com
//
// (C) Copyright Jiaping Wang 2004.
//
// Permission to copy, use, modify, sell and distribute this software
// is granted provided this copyright notice appears in all copies.
// This software is provided "as is" without express or implied
// warranty, and with no claim as to its suitability for any purpose.
//
// Absolutely no warranty!!
//////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////
//  Brief Description
//
//  compact_image.h
//
//  instantiations of _typedimage for linear and compact image
//
//
//////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////
// Revise History
//
// Inital version	2009.3.27		Jiaping
//
//////////////////////////////////////////////////////////////////////


#include "image_type.h"
#include "compact_vector.h"

namespace rt
{

namespace _meta_
{
//////////////////////////////////////////////////////////////////////
// storage type for images
template<typename t_Val>
class _BaseImageRoot
{
	static const bool IsElementNotAggregate = !rt::TypeTraits<t_Val>::IsAggregate ;
protected:
	DYNTYPE_FUNC _BaseImageRoot(const _BaseImageRoot &x){ ASSERT_STATIC(0); }
public:
	DYNTYPE_FUNC _BaseImageRoot(){	lpData=NULL; width=0; height=0; Pad_Bytes=0; }
	DYNTYPE_FUNC ~_BaseImageRoot(){ __SafeFree(); }
	DYNTYPE_FUNC BOOL SetSize(UINT co=0){ return (co==GetSize())?TRUE:FALSE; }
	DYNTYPE_FUNC BOOL ChangeSize(UINT new_size){ return (new_size==GetSize())?TRUE:FALSE; }

	//TypeTraits
	TYPETRAITS_DECL_LENGTH(TYPETRAITS_SIZE_UNKNOWN)
	TYPETRAITS_DECL_IS_AGGREGATE(false)

	template<typename T,typename T2>
	DYNTYPE_FUNC t_Val & At(const T x,const T2 y)
	{ return *(t_Val*)&((LPBYTE)lpData)[y*Step_Bytes+x*sizeof(t_Val)]; }
	template<typename T,typename T2>
	DYNTYPE_FUNC const t_Val & At(const T x,const T2 y)const
	{ return *(const t_Val*)&((LPCBYTE)lpData)[y*Step_Bytes+x*sizeof(t_Val)]; }

	DYNTYPE_FUNC UINT GetSize() const{ return (UINT)(width*height); }

	::rt::Buffer_Ref<t_Val> 
	GetLine(UINT y){ return ::rt::Buffer_Ref<t_Val>(&At(0,y),GetWidth()); }
	const ::rt::Buffer_Ref<t_Val> 
	GetLine(UINT y) const{ return ::rt::_CastToNonconst(this)->GetLine(y); }

	DYNTYPE_FUNC rt::Buffer_Ref<t_Val> GetVec(){ return rt::Buffer_Ref<t_Val>(lpData,Step_Bytes*height/sizeof(t_Val)); }
	DYNTYPE_FUNC const rt::Buffer_Ref<t_Val> GetVec() const{ return _CastToNonconst(this)->GetVec(); }

protected:
	UINT	width;
	UINT	height;
	UINT	Pad_Bytes;	//Pad_Bytes = Step - width*sizeof(t_Val)
	UINT	Step_Bytes; //Step = width*sizeof(t_Val) + Pad_Bytes
	t_Val*	lpData;

	void	__SafeFree()
	{	__static_if(IsElementNotAggregate) //call dtor
		{
			for(UINT y=0;y<GetHeight();y++)
			for(UINT x=0;x<GetWidth();x++)
				At(x,y).~t_Val(); 
		}
		_SafeFree32AL(lpData);
	}

public:
	DYNTYPE_FUNC BOOL SetSize_2D(UINT w=0,UINT h=0, UINT step_byte=0)
	{	if(w==width && h==height){ return TRUE; }
		else
		{	__SafeFree();
			width=w; height=h;
			Step_Bytes = max(width*sizeof(t_Val), step_byte);
			Pad_Bytes = width*sizeof(t_Val) - Step_Bytes;
			if(w>=0 && h>=0)
			{
				lpData = (t_Val*)_Malloc32AL(BYTE,Step_Bytes*h);
				if(lpData)
				{	__static_if(IsElementNotAggregate) //call ctor
					{
						for(UINT y=0;y<GetHeight();y++)
						for(UINT x=0;x<GetWidth();x++)
							new (&At(x,y)) t_Val();
					}
					return TRUE;
				}
				else{ width=height=0; }
			}
			else{ return TRUE; }
		}
		return FALSE;
	}
	DYNTYPE_FUNC UINT			GetWidth() const{ return width;}
	DYNTYPE_FUNC UINT			GetHeight() const{ return height;}
	DYNTYPE_FUNC static UINT	GetChannels(){ return 1;}
};
}// namespace _meta_
class _BaseImage
{public:	template<typename t_Ele> class SpecifyItemType
			{public: typedef rt::_meta_::_BaseImageRoot<t_Ele> t_BaseVec; };
			static const bool IsCompactLinear = false;
};


//pre-declaration
template<typename t_Ele>
class Image_Ref;

//////////////////////////////////////////////////////////////////////
// Basic 2D image class
template<typename t_Val,class BaseImage = _BaseImage>
class Image:public rt::_TypedImage<t_Val,BaseImage>
{
private:
	DYNTYPE_FUNC BOOL SetSize(){ ASSERT(0); }

public:
	COMMON_IMAGE_STUFFS(Image)
	TYPETRAITS_DECL_EXTENDTYPEID((rt::_typeid_codelib<<16)+102)

	DYNTYPE_FUNC BOOL SetSize(UINT w=0,UINT h=0)
	{ return __super::SetSize_2D(w,h); }

	template<typename t_Valx,class t_BaseVecx>
	DYNTYPE_FUNC BOOL SetSize(const _TypedImage<t_Valx,t_BaseVecx>& x)
	{ return __super::SetSize_2D(x.GetWidth(),x.GetHeight()); }

	template<typename t_Ele2> class SpecifyItemType
	{public: typedef rt::Image<t_Ele2,BaseImage> t_Result; };

public:
	typedef typename rt::TypeTraits<t_Val>	t_PixelTR;
	typedef typename t_PixelTR::t_Accum		t_AccumPixel;

	typedef typename FundamentalItemType	ValueType;
	typedef typename ValueType*				LPValueType;
	typedef typename const ValueType*		LPCValueType;

	typedef typename t_Val					PixelType;
	typedef typename PixelType*				LPPixelType;
	typedef typename const PixelType*		LPCPixelType;

	typedef typename t_AccumPixel			AccumPixelType;
	typedef typename t_AccumPixel*			LPAccumPixelType;
	typedef typename const t_AccumPixel*	LPCAccumPixelType;

public:
	typedef rt::Image_Ref<t_Val> Ref;

	DYNTYPE_FUNC operator Ref& (){ return *((Ref*)this); }
	DYNTYPE_FUNC operator const Ref& () const{ return *((Ref*)this); }

	DYNTYPE_FUNC Ref GetSub(UINT x,UINT y,UINT w,UINT h)
	{	ASSERT(x+w<=GetWidth());
		ASSERT(y+h<=GetHeight());
		return Ref(&At(x,y),w,h,GetStep());
	}
	DYNTYPE_FUNC const Ref GetSub(UINT x,UINT y,UINT w,UINT h) const
	{	return rt::_CastToNonconst(this)->GetSub(x,y,w,h); }

	DYNTYPE_FUNC Ref GetSub_Inside(UINT border)
	{	ASSERT(border*2<GetWidth());
		ASSERT(border*2<GetHeight());
		return GetSub(border,border,GetWidth()-border*2,GetHeight()-border*2);
	}
	DYNTYPE_FUNC const Ref GetSub_Inside(UINT border) const
	{	return rt::_CastToNonconst(this)->GetSub_Inside(border); }

	DYNTYPE_FUNC Ref GetSub_Inside(UINT border_x,UINT border_y)
	{	ASSERT(border_x*2<GetWidth());
		ASSERT(border_y*2<GetHeight());
		return GetSub(border_x,border_y,GetWidth()-border_x*2,GetHeight()-border_y*2);
	}
	DYNTYPE_FUNC const Ref GetSub_Inside(UINT border_x,UINT border_y) const
	{	return rt::_CastToNonconst(this)->GetSub_Inside(border_x,border_y); }

public:
	DYNTYPE_FUNC UINT		GetStep() const{ return Step_Bytes; }
	DYNTYPE_FUNC UINT		GetDataSize() const{ return height*GetStep(); }
	DYNTYPE_FUNC UINT		GetPadding() const{ return Pad_Bytes; }
	DYNTYPE_FUNC UINT		GetBPP() const{ return sizeof(t_Val)<<3; }

	DYNTYPE_FUNC LPVOID		GetBits(){ return lpData; }
	DYNTYPE_FUNC LPCVOID	GetBits() const{ return lpData; }
	DYNTYPE_FUNC LPVOID		GetBitsAt(UINT x,UINT y){ ASSERT(x<=GetWidth()&&y<=GetHeight()); return &(((LPBYTE)lpData)[y*GetStep()+x*sizeof(t_Val)]); }
	DYNTYPE_FUNC LPCVOID	GetBitsAt(UINT x,UINT y) const{ ASSERT(x<=GetWidth()&&y<=GetHeight()); return &(((LPBYTE)lpData)[y*StepSize+x*sizeof(t_Val)]); }

	DYNTYPE_FUNC operator	LPValueType(){ return (LPValueType)GetBits(); }
	DYNTYPE_FUNC operator	LPCValueType() const{ return (LPCValueType)GetBits(); }

	DYNTYPE_FUNC LPPixelType	GetPixelAddress(UINT x=0,UINT y=0){ ASSERT(x<=GetWidth()&&y<=GetHeight()); return (LPPixelType)(&((LPBYTE)GetBits())[y*GetStep()+x*sizeof(t_Val)]); }
	DYNTYPE_FUNC LPPixelType	GetLineAddress(UINT y=0){ ASSERT(y<=GetHeight()); return (LPPixelType)(&((LPBYTE)GetBits())[y*GetStep()]); }
	DYNTYPE_FUNC LPCPixelType	GetPixelAddress(UINT x=0,UINT y=0) const{ ASSERT(x<=GetWidth()&&y<=GetHeight()); return (LPCPixelType)(&((LPBYTE)GetBits())[y*GetStep()+x*sizeof(t_Val)]); }
	DYNTYPE_FUNC LPCPixelType	GetLineAddress(UINT y=0) const{ ASSERT(y<=GetHeight()); return (LPCPixelType)(&((LPBYTE)GetBits())[y*GetStep()]); }

	template<typename T> 
	DYNTYPE_FUNC T*	Scanline_Next(T* ptr) const{ return (T*)rt::_CastToNonconst((&((LPBYTE)(ptr))[GetStep()])); }
	template<typename T> 
	DYNTYPE_FUNC T* Scanline_Prev(T* ptr) const{ return (T*)rt::_CastToNonconst((&((LPBYTE)(ptr))[-((int)GetStep())])); }

	DYNTYPE_FUNC LPPixelType	Scanline_Begin(){ return (LPCPixelType)GetBits(); }
	DYNTYPE_FUNC LPCPixelType	Scanline_Begin() const{ return (LPCPixelType)GetBits(); }
	DYNTYPE_FUNC LPCPixelType	Scanline_End() const{ return GetPixelAddress(width,height-1); }

	DYNTYPE_FUNC PixelType&			GetPixel(UINT x=0,UINT y=0){ ASSERT(x<=GetWidth()&&y<=GetHeight()); return *((LPPixelType)GetPixelAddress(x,y)); }
	DYNTYPE_FUNC const PixelType&	GetPixel(UINT x=0,UINT y=0) const{ ASSERT(x<=GetWidth()&&y<=GetHeight()); return *((LPCPixelType)GetPixelAddress(x,y)); }
	DYNTYPE_FUNC PixelType&			operator ()(UINT x, UINT y){ return GetPixel(x,y); }
	DYNTYPE_FUNC const PixelType&	operator ()(UINT x, UINT y) const{ return GetPixel(x,y); }

	template<typename T>
	DYNTYPE_FUNC PixelType&			GetPixel(const T& p){return *((LPPixelType)GetPixelAddress(p.x,p.y)); }
	template<typename T>
	DYNTYPE_FUNC const PixelType&	GetPixel(const T& p) const{return *((LPCPixelType)GetPixelAddress(p.x,p.y)); }
	template<typename T>
	DYNTYPE_FUNC PixelType&			operator ()(const T& p){ return GetPixel(p); }
	template<typename T>
	DYNTYPE_FUNC const PixelType&	operator ()(const T& p) const{ return GetPixel(p); }

	template<class PixelType>
	void GetInterpolatedPixel(float u,float v,typename PixelType& p) const
	{	ASSERT(u>-EPSILON && u<=1.0f+EPSILON);
		ASSERT(v>-EPSILON && v<=1.0f+EPSILON);

		float r_x = u*(GetWidth()-1);
		float r_y = v*(GetHeight()-1);
		int	i_x = (int)(r_x + FLT_EPSILON);
		int	i_y = (int)(r_y + FLT_EPSILON);
    
		LPCPixelType addr = GetPixelAddress(i_x,i_y);
        p.BilinearInterpolate(*addr,*Scanline_Next(addr),r_x-i_x,r_y-i_y);
	}

	template<typename t_ItemType2, typename BaseVec2>
	DYNTYPE_FUNC void CopyFrom(const _TypedImage<t_ItemType2,BaseVec2> & i)
	{	ASSERT_SIZE(*this,i);
		for(UINT y=0;y<GetHeight();y++)
		for(UINT x=0;x<GetWidth(); x++)
#pragma warning(disable:4244)
			At(x,y) = i.At(x,y);
#pragma warning(default:4244)
	}

	/////////////////////////////////////////////////////
	// Pixel conversion
	template<typename T,class BaseImage2>	//convert image if possible
	void CopyFrom( const Image<T,BaseImage2>& in, float FloatMax=1.0f )
	{	ASSERT_SIZE(*this,in);
		typedef ValueType	_ValueType;
		typedef typename rt::FundamentalType<T>::t_Result	_ValueType2;
		typedef rt::NumericTraits<_ValueType>	_v1_type;
		typedef rt::NumericTraits<_ValueType2>	_v2_type;
		static const int chan_num = TypeTraits_Item::Length ;
		static const int chan_num2 = rt::TypeTraits<T>::Length;
		
		__static_if(_v1_type::IsInteger && _v2_type::IsFloat)
		{	// float to int, handle Env.FloatMax and clamp
			typedef typename rt::_meta_::_TypeIndex<float,double,(sizeof(ItemType)>=4)>::t_Result t_scale_type;
			t_scale_type scale = rt::TypeTraits<_ValueType>::MaxVal()/FloatMax;

			__static_if(chan_num>chan_num2)
			{	
				ASSERT_STATIC(chan_num2==1 || chan_num2==3);
				typedef typename rt::SpecifyItemType<T,_ValueType>::t_Result t_TempValInt;

				for(UINT y=0;y<GetHeight();y++)
				for(UINT x=0;x<GetWidth();x++)
				{
					__static_if(chan_num2 == 1)
					{	// scale and clamp, then grey->color
#pragma warning(disable: 4244)
						T tmp_float(scale*in.GetPixel(x,y)); //scale
						tmp_float = __max((_ValueType2)0,__min(tmp_float,rt::TypeTraits<_ValueType>::MaxVal()-EPSILON)); //clamp
						GetPixel(x,y) = t_TempValInt(tmp_float); // float->int and grey->color
#pragma warning(default: 4244)
					}
					__static_if(chan_num2 == 3)
					{	// scale and clamp, then v3->v4
#pragma warning(disable: 4244)
						T tmp_float(in.GetPixel(x,y)); //scale
						tmp_float*=scale;
						tmp_float[0] = max((_ValueType2)0,min(tmp_float[0],rt::TypeTraits<_ValueType>::MaxVal()-EPSILON)); //clamp
						tmp_float[1] = max((_ValueType2)0,min(tmp_float[1],rt::TypeTraits<_ValueType>::MaxVal()-EPSILON)); //clamp
						tmp_float[2] = max((_ValueType2)0,min(tmp_float[2],rt::TypeTraits<_ValueType>::MaxVal()-EPSILON)); //clamp
						GetPixel(x,y) = t_TempValInt(tmp_float); // float->int and v3->v4
						GetPixel(x,y)[3] = rt::TypeTraits<_ValueType>::MaxVal()-EPSILON;
#pragma warning(default: 4244)
					}
				}
			}
			__static_if(chan_num<chan_num2)
			{	
				ASSERT_STATIC(chan_num==1 || chan_num==3);

				for(UINT y=0;y<GetHeight();y++)
				for(UINT x=0;x<GetWidth();x++)
				{
					typedef typename rt::SpecifyItemType<ItemType,_ValueType2>::t_Result t_TempVal;

					__static_if(chan_num == 1)
					{	// color->grey, then scale and clamp
						t_TempVal tmp_float(in.GetPixel(x,y).GetBrightness()); //color->grey
						tmp_float*=scale; //scale
						tmp_float = __max((_ValueType2)0,__min(tmp_float,rt::TypeTraits<_ValueType>::MaxVal()-EPSILON)); //clamp
#pragma warning(disable:4244)
						GetPixel(x,y) = tmp_float; // float to int
#pragma warning(default:4244)
					}
					__static_if(chan_num == 3)
					{	// v3->v4, then scale and clamp
						t_TempVal tmp_float(in.GetPixel(x,y).v3()); // v3->v4
						tmp_float*=scale; //scale
						tmp_float[0] = max((_ValueType2)0,min(tmp_float[0],rt::TypeTraits<_ValueType>::MaxVal()-EPSILON)); //clamp
						tmp_float[1] = max((_ValueType2)0,min(tmp_float[1],rt::TypeTraits<_ValueType>::MaxVal()-EPSILON)); //clamp
						tmp_float[2] = max((_ValueType2)0,min(tmp_float[2],rt::TypeTraits<_ValueType>::MaxVal()-EPSILON)); //clamp
#pragma warning(disable:4244)
						GetPixel(x,y) = tmp_float; // float to int
						GetPixel(x,y)[3] = rt::TypeTraits<_ValueType>::MaxVal();
#pragma warning(default:4244)
					}
				}
			}
		}
		__static_if(_v1_type::IsFloat && _v2_type::IsInteger)
		{	// int to float, handle Env.FloatMax
			typedef typename rt::_meta_::_TypeIndex<float,double,(sizeof(T)>=4)>::t_Result t_scale_type;
			t_scale_type scale = FloatMax/rt::TypeTraits<_ValueType2>::MaxVal();

			__static_if(chan_num>chan_num2)
			{	// scale then grey->color
				ASSERT_STATIC(chan_num2==1 || chan_num2==3);
				typedef typename rt::SpecifyItemType<T,_ValueType>::t_Result t_TempVal;

				for(UINT y=0;y<GetHeight();y++)
				for(UINT x=0;x<GetWidth();x++)
				{
#pragma warning(disable:4244)
					t_TempVal tmp_float(in.GetPixel(x,y)); // scale
					tmp_float*=scale;
					GetPixel(x,y) = tmp_float; //  grey->color, v3->v4
					__static_if(chan_num2==3)
					{ GetPixel(x,y)[3] = FloatMax; }
#pragma warning(default:4244)
				}
			}
			__static_if(chan_num<chan_num2)
			{	// color->grey, then scale
				ASSERT_STATIC(chan_num==1 || chan_num==3);
				typedef typename rt::SpecifyItemType<ItemType,_ValueType2>::t_Result t_TempVal;

				for(UINT y=0;y<GetHeight();y++)
				for(UINT x=0;x<GetWidth();x++)
				{	
#pragma warning(disable:4244)
					t_TempVal tmp_int(in.GetPixel(x,y).GetBrightness()); // color->grey, v3->v4
					t_Val tmpfloat(tmp_int);
					tmpfloat*=scale; //scale
					GetPixel(x,y) = tmpfloat;
#pragma warning(default:4244)
				}
			}
		}

		for(UINT y=0;y<height;y++)
		{	LPPixelType p = GetLineAddress(y);
			const T* cp = in.GetLineAddress(y);
			LPPixelType end = p + width;
			for(;p<end;p++,cp++)
			{	
#pragma warning(disable:4244)
				*p = *cp;
#pragma warning(default:4244)
			}
		}
	}

	template<typename t_Val2,class BaseImage2>	//conversion images if possible
	void CopyTo( Image<t_Val2,BaseImage2>& in ) const{ in.CopyFrom(*this); }

};
template<class t_Ostream, typename t_Ele, class BaseVector>
t_Ostream& operator<<(t_Ostream& Ostream, const rt::Image<t_Ele,BaseVector> & im)
{	
	UINT ele_limite = ::rt::_meta_::IsStreamStandard(Ostream)?8:UINT_MAX;
	if(im.IsNull())
	{	Ostream<<"{ -NULL- }";
		return Ostream;
	}

	Ostream<<"{ // "<<im.GetWidth()<<'x'<<im.GetHeight()<<" Image\n";
	if(im.GetHeight()<=ele_limite)
	{//print all
		for(UINT y=0;y<im.GetHeight()-1;y++)
		{
			Ostream<<im.GetLine(y)<<','<<'\n';
		}
		Ostream<<im.GetLine(im.GetHeight()-1)<<'\n';
	}
	else
	{//print head and tail
		for(UINT y=0;y<=4;y++)
			Ostream<<im.GetLine(y)<<','<<'\n';
		Ostream<<"  ... ...\n";
		Ostream<<im.GetLine(im.GetHeight()-2)<<','<<'\n';
		Ostream<<im.GetLine(im.GetHeight()-1)<<'\n';
	}
	Ostream<<'}'<<'\n';

	return Ostream;
}



//////////////////////////////////////////////////////////////////////
// reference type for images
namespace _meta_
{
template<typename t_Val>
class _BaseImageRefRoot
{
protected:
	DYNTYPE_FUNC _BaseImageRefRoot(const _BaseImageRefRoot &x){ ASSERT_STATIC(0); }
	DYNTYPE_FUNC _BaseImageRefRoot(){}
public:
	DYNTYPE_FUNC BOOL SetSize(UINT co=0){ return (co==GetSize())?TRUE:FALSE; }
	DYNTYPE_FUNC BOOL ChangeSize(UINT new_size){ return (new_size==GetSize())?TRUE:FALSE; }

	//TypeTraits
	TYPETRAITS_DECL_LENGTH(TYPETRAITS_SIZE_UNKNOWN)
	TYPETRAITS_DECL_IS_AGGREGATE(false)

	template<typename T,typename T2>
	DYNTYPE_FUNC t_Val & At(const T x,const T2 y)
	{ return (t_Val&)((LPBYTE)lpData)[y*Step_Bytes+x*sizeof(t_Val)]; }
	template<typename T,typename T2>
	DYNTYPE_FUNC const t_Val & At(const T x,const T2 y)const
	{ return (const t_Val&)((LPCBYTE)lpData)[y*Step_Bytes+x*sizeof(t_Val)]; }

	DYNTYPE_FUNC UINT GetSize() const{ return (UINT)(width*height); }

	::rt::Buffer_Ref<t_Val> 
	GetLine(UINT y){ return ::rt::Buffer_Ref<t_Val>(&At(0,y),GetWidth()); }
	const ::rt::Buffer_Ref<t_Val> 
	GetLine(UINT y) const{ return ::rt::_CastToNonconst(this)->GetLine(y); }

protected:
	UINT	width;
	UINT	height;
	UINT	Pad_Bytes;	//Pad_Bytes = Step - width*sizeof(t_Val)
	UINT	Step_Bytes; //Step = width*sizeof(t_Val) + Pad_Bytes
	t_Val*	lpData;
public:
	DYNTYPE_FUNC BOOL SetSize_2D(UINT w=0,UINT h=0)
	{ return w==width && h==height;	}
	DYNTYPE_FUNC UINT		GetWidth() const{ return width;}
	DYNTYPE_FUNC UINT		GetHeight() const{ return height;}
};
}// namespace _meta_
class _BaseImageRef
{public:	template<typename t_Ele> class SpecifyItemType
			{public: typedef rt::_meta_::_BaseImageRefRoot<t_Ele> t_BaseVec; };
			static const bool IsCompactLinear = false;
};


//////////////////////////////////////////////////////////////////////
// Basic 2D image class
template<typename t_Val>
class Image_Ref:public rt::Image<t_Val,_BaseImageRef>
{	
	template< typename t_Val, class BaseImage >
	friend class Image;
	template< typename t_Val, class BaseImage >
	friend class ::rt::_TypedImage;
protected:
	DYNTYPE_FUNC Image_Ref(){ ASSERT_STATIC(0); }
	DYNTYPE_FUNC Image_Ref(const Image_Ref& x){ ASSERT_STATIC(0); }	//The two ASSERTs fired when trying to construct 
	//a Ref from a Ref instance or an empty instance which is not allowed. Avoid trying to copy a Ref
	//instance, it is highly recommended to use Ref& instead  for local using and function arguments
public:
	template<class BaseVec>
	DYNTYPE_FUNC Image_Ref(rt::Image<t_Val,BaseVec>& x)
		:_BaseImageRefRoot((t_Val*)x.GetBits(),x.GetWidth(),x.GetHeight(),x.GetStep()){}
	DYNTYPE_FUNC Image_Ref(t_Val* ptr,UINT w,UINT h,UINT step_bytes) //internal use only
		{ lpData=ptr; width=w; height=h; Step_Bytes=step_bytes; Pad_Bytes=step_bytes - width*sizeof(t_Val); }


#ifdef __INTEL_COMPILER
#pragma warning(disable: 525) //warning #525: type "xxx" is an inaccessible type (allowed for compatibility)
#endif
	typedef __super::Ref Ref;
#ifdef __INTEL_COMPILER
#pragma warning(default: 525)
#endif

public:
#pragma warning(disable:4244)
	ASSIGN_OPERATOR_IMG(Image_Ref)
#pragma warning(default:4244)
	DEFAULT_TYPENAME(Image_Ref)
};




} // namespace rt