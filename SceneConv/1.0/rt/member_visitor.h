#pragma once

//////////////////////////////////////////////////////////////////////
// Visual Computing Foundation Classes (VCFC)
//					Jiaping Wang  2006.2
//					e_boris2002@hotmail.com
//
// (C) Copyright Jiaping Wang 2004.
//
// Permission to copy, use, modify, sell and distribute this software
// is granted provided this copyright notice appears in all copies.
// This software is provided "as is" without express or implied
// warranty, and with no claim as to its suitability for any purpose.
//
// Absolutely no warranty!!
//////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////
//  Brief Description
//
//  member_visitor.h
//
//  framework for visiting class members and base-classes
//	visitor for textdump, binary save/load is defined
//
//////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////
// Revise History
//
// Inital version	2006.7.24		Jiaping
//
//////////////////////////////////////////////////////////////////////

#include "compiler_abs.h"
#include "fixvec_type.h"


///////////////////////////////////////////////////////////////////
// A Visitor class should have three functions 
// and one static const int Visitor_Id = x 
// 
// template< typename T >
// HRESULT Visit(T& member,DWORD flag,LPCTSTR pMemberName=NULL)
// Called each time for visiting a member variable
//
// HRESULT Begin(LPCTSTR pClassName)
// Called each time before visiting all members of a class
//
// HRESULT End(LPCTSTR pClassName)
// Called each time after all members of a class have been visited
//
///////////////////////////////////////////////////////////////////

			
///////////////////////////////////////////////////////////////////
// Member Table Definition in class scope of clsname definition
#define _MEMBER_TABLE_BEGIN(clsname)										\
		public: static LPCTSTR __GetClassName(){ return _T(#clsname); }		\
		static const int HAS_MEMBER_VISITOR ;								\
		template<typename t_MemberVisitor>									\
		HRESULT VisitMembers(t_MemberVisitor & __visitor)					\
		{	HRESULT hr = S_OK;		DWORD flag = 0;							\
			hr=__visitor.Begin(__GetClassName());							\
			if(FAILED(hr))return hr;										\

#define _MEMBER_TABLE_END()													\
			return __visitor.End(__GetClassName());							\
		}																	\




///////////////////////////////////////////////////////////////////
// Member Table Definition in global scope based on TypeAgent
#define _MEMBER_TABLE_BEGIN_EXTERNAL(cls_name)						\
		namespace rt{namespace _meta_{								\
		template<> struct _TypeAgent<cls_name>: public cls_name		\
		{	static const int __IsDefined = true ;					\
			_MEMBER_TABLE_BEGIN(cls_name)							\

#define _MEMBER_TABLE_END_EXTERNAL()								\
			_MEMBER_TABLE_END()										\
		}; }} /* _meta_/rt */										\

// use within _MEMBER_TABLE_BEGIN_EXTERNAL/_MEMBER_TABLE_END_EXTERNAL
// and after all _MEMBER_ENTRY items
#define _MEMBER_TABLE_TYPETRAITS_DECL_BEGIN()						\
			_MEMBER_TABLE_END()										\

#define _MEMBER_TABLE_TYPETRAITS_DECL_END()							\
			__static_if(0){											\



///////////////////////////////////////////////////////////////////
// Entry for members used within 
// _MEMBER_TABLE_BEGIN(_EXTERNAL)/_MEMBER_TABLE_END(_EXTERNAL) pair
#define _MEMBER_ENTRY(x)																\
		{ hr=rt::_MemberVisitor::_meta_::_Call_Member_Visitor(x,__visitor,flag,_T(#x));	\
		  if(FAILED(hr))return hr; }													\

#define _MEMBER_ENTRY_INCLUDE(x,include_mv)												\
		if(t_MemberVisitor::Visitor_Id & include_mv)_MEMBER_ENTRY(x)					\

#define _MEMBER_ENTRY_EXCLUDE(x,exclude_mv)												\
		if((t_MemberVisitor::Visitor_Id & include_mv)==0)_MEMBER_ENTRY(x)				\

// Entry for base classes
// Entry for members used within 
// _MEMBER_TABLE_BEGIN(_EXTERNAL)/_MEMBER_TABLE_END(_EXTERNAL) pair
#define _MEMBER_ENTRY_BASECLASS(t_base)												\
		{	hr=rt::_MemberVisitor::_meta_::_Call_Member_Visitor						\
				(*((t_base*)this),__visitor,flag,_T("[") _T(#t_base) _T("]"));		\
			if(FAILED(hr))return hr; }												\

#define _MEMBER_ENTRY_BASECLASS_INCLUDE(t_base,include_mv)							\
		if(t_MemberVisitor::Visitor_Id & include_mv)								\
			_MEMBER_ENTRY_BASECLASS(t_base)											\

#define _MEMBER_ENTRY_BASECLASS_EXCLUDE(t_base,exclude_mv)							\
		if((t_MemberVisitor::Visitor_Id & include_mv)==0)							\
			_MEMBER_ENTRY_BASECLASS(t_base)											\

#define _MEMBER_IS_VISITING_BY(mv_id)	(mv_id==t_MemberVisitor::Visitor_Id)



namespace rt
{

namespace _MemberVisitor
{
namespace _meta_
{

template<typename T,class t_Visitor>
HRESULT _Call_Member_Visitor(T& x ,t_Visitor& __visitor,DWORD flag,LPCTSTR pMemberName)
{	typedef rt::TypeTraits<T>	TT;
	__static_if(TT::Typeid == rt::_typeid_array)
	{	typedef rt::Vec<typename TT::t_Element,TT::Length>* LPVEC;
		return __visitor.Visit(*((LPVEC)rt::_CastToNonconst(&x)),flag,pMemberName);
	}
	__static_if(TT::Typeid != rt::_typeid_array)
	{
		__static_if(rt::HasTypeAgent<T>::Result)
		{	typedef rt::_meta_::_TypeAgent<T> T_mirror;
			return __visitor.Visit(*((T_mirror*)rt::_CastToNonconst(&x)),flag,pMemberName);
		}
		__static_if(!rt::HasTypeAgent<T>::Result)
		{ return __visitor.Visit(x,flag,pMemberName); }
	}
}

template<typename T>
struct _MemberListDefined
{	__if_exists(T::HAS_MEMBER_VISITOR){ static const int Result = true; };
	__if_not_exists(T::HAS_MEMBER_VISITOR){ static const int Result = false; };
};

} // namespace _meta_
} // namespace _MemberVisitor
} // namespace rt


namespace rt
{

template<class T>
struct IsMemberVisitorSupported
{	static const bool Result =	_MemberVisitor::_meta_::_MemberListDefined<T>::Result ||
								_MemberVisitor::_meta_::_MemberListDefined<rt::_meta_::_TypeAgent<T> >::Result;
};

template<typename T,class t_Visitor>
HRESULT CallMemberVisitor(T& x ,t_Visitor& visitor)
{	
	ASSERT_STATIC(IsMemberVisitorSupported<T>::Result);
	typedef rt::_meta_::_TypeAgent<T> T_mirror;

	__static_if(T_mirror::__IsDefined){	return ((T_mirror*)rt::_CastToNonconst(&x))->VisitMembers(visitor); }
	__static_if(!T_mirror::__IsDefined){return x.VisitMembers(visitor); }
}

} // namespace rt



namespace rt
{

////////////////////////////////////////////////
// TextDump Member Visitor
namespace _MemberVisitor
{

enum Id
{	// bitwise defined !! 
	MV_Dump		= 0x1,	// rt::_MemberVisitor::_TextDump
	MV_Save		= 0x2,	// w32::CFile64_Composition
	MV_Load		= 0x4,	// w32::CFile64_Composition
	MV_BindGrid = 0x8,	// ui::xt::_meta_::_BindMembers_
};

#define _MEMBER_ALLOW_TEXTDUMP()	public:													\
									static const int MEMBER_ALLOW_TEXTDUMP ;				\
									void TextDump(::rt::t_ostream* pOstream=NULL) const		\
									{	rt::_MemberVisitor::_TextDump						\
										visitor(pOstream?(*pOstream):(_STD_OUT));			\
										rt::_CastToNonconst(this)->VisitMembers(visitor);	\
									}														\
									private:												\

class _TextDump
{
	__forceinline static LPCTSTR _text_dump_tail(){ return _T(";\n"); }
	__forceinline static LPCTSTR _text_class_sp(){ return _T("class "); }
	__forceinline static LPCTSTR _text_left(){ return _T("{\n"); }
	__forceinline static LPCTSTR _text_right(){ return _T("}\n"); }

	t_ostream&	m_Ostream;
	int			m_NestedLevel;
protected:
	void			PrintIndent()
	{	static const LPCTSTR space_str= _T("  ");
		for(int i=0;i<m_NestedLevel;i++)m_Ostream<<(space_str);
	}

public:
	_TextDump(t_ostream& ostr)
		:m_Ostream(ostr){ m_NestedLevel = 0; }
public:
	static const int Visitor_Id = MV_Dump ;

	template< typename T >
	HRESULT Visit(T& member,DWORD flag,LPCTSTR pMemberName=NULL)
	{	
		PrintIndent();
		m_Ostream<<pMemberName<<_T('=');

		__if_exists(T::HAS_MEMBER_VISITOR)
		{
			m_Ostream<<_T('\n');
			m_NestedLevel++;
			member.VisitMembers(*this);
			m_NestedLevel--;
		}
		__if_not_exists(T::HAS_MEMBER_VISITOR)
		{
			m_Ostream<<member;
			m_Ostream<<_text_dump_tail();
		}
		return S_OK;
	}
 
	HRESULT Begin(LPCTSTR pClassName)
	{	PrintIndent(); m_Ostream<<_text_class_sp()<<pClassName<<_T('\n');
		PrintIndent(); m_Ostream<<_text_left();
		m_NestedLevel++;
		return S_OK;
	}
	HRESULT End(LPCTSTR pClassName)
	{	m_NestedLevel--;
		PrintIndent(); m_Ostream<<_text_right();
		return S_OK;
	}
};

} // namespace _MemberVisitor
} // namespace rt



