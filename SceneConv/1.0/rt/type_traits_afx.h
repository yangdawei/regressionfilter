#pragma once

//////////////////////////////////////////////////////////////////////
// Visual Computing Foundation Classes (VCFC)
//					Jiaping Wang  2006.2
//					e_boris2002@hotmail.com
//
// (C) Copyright Jiaping Wang 2004.
//
// Permission to copy, use, modify, sell and distribute this software
// is granted provided this copyright notice appears in all copies.
// This software is provided "as is" without express or implied
// warranty, and with no claim as to its suitability for any purpose.
//
// Absolutly no garanty!!
//////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////
//  Brief Description
//
//  type_traits_stl.h
//
//	TypeTraits for ATL/MFC classes
//
//	Optional Macros
//	TYPETRAITS_ENABLE_ALL_ATLMFC	fully support all ATL/MFC classes
//									including not frequently used
//
//////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////
// Revise History
//
// Inital version	2007.11.1		Jiaping
//
//////////////////////////////////////////////////////////////////////

#include "type_traits_ext.h"
#include <Rpc.h>
#pragma comment(lib,"Rpcrt4.lib")

//////////////////////////////////////////////////
// if TYPETRAITS_ENABLE_ALL_ATLMFC is enabled, 
// the following ...MAX macro might be redefined.
#define SUBTYPEID_MFC_MIN			1
#define SUBTYPEID_MFC_MAX			999
#define SUBTYPEID_ATL_MIN			2001
#define SUBTYPEID_ATL_MAX			2999

#define SUBTYPEID_AFX_MIN			SUBTYPEID_MFC_MIN
#define SUBTYPEID_AFX_MAX			SUBTYPEID_ATL_MAX

namespace rt{
namespace _meta_{
	__forceinline LPCTSTR _text_CMap_(){ return _T("CMap<"); }
	__forceinline LPCTSTR _text_ATL_(){ return _T("ATL::"); }
	__forceinline LPCTSTR _text_UUID_LEFT_(){ return _T(",{"); }
	__forceinline LPCTSTR _text_UUID_RIGHT_(){ return _T("}>"); }
}} // namespace _meta_ / namespace rt

//////////////////////////////////////////////////////////////////////
// TypeTraits for ATL/MFC classes
// 
#define _MFC_CLASS_DEF(cls_name, etid_minor)	/* etid_major is _typeid_afx */							\
	class cls_name;																						\
	namespace rt{																						\
		enum{ _typeid_afx_##cls_name = (_typeid_afx<<16)+etid_minor };									\
		namespace _ExtendedType{																		\
		::rt::_meta_::_ExtenedTypeId_Struct<_typeid_afx_##cls_name> _DefineExtendedTypeId(cls_name* p);	\
		template<typename T> class TypeTraitsEx<T, _typeid_afx_##cls_name>								\
		{public:typedef T t_Accum;																		\
				typedef T t_Val;																		\
				typedef void t_Element;																	\
				typedef T t_Signed;																		\
				enum{ Typeid = _typeid_afx };															\
				enum{ ExtendTypeid = _typeid_afx_##cls_name };											\
				enum{ IsAggregate = false };															\
				enum{ IsNumeric = false };																\
				enum{ IsArray = false };																\
				enum{ IsImage = false };																\
				enum{ Length = 1 };																		\
				template<class ostream>																	\
				static void TypeName(ostream & outstr){ outstr<<_T(#cls_name);  }						\
				struct text_output_spec																	\
				{	enum{ width_total = TYPETRAITS_SIZE_UNKNOWN };										\
					enum{ width_after_point = TYPETRAITS_SIZE_UNKNOWN };								\
		};		};																						\
	}} /* namespace _ExtendedTyp / namespace rt */														\

_MFC_CLASS_DEF(CObject			,	1	)		// AssertValid/Dump
_MFC_CLASS_DEF(CCmdTarget		,	2	)
_MFC_CLASS_DEF(CDC				,	3	)
_MFC_CLASS_DEF(CFile			,	4	)
_MFC_CLASS_DEF(CGdiObject		,	5	)
_MFC_CLASS_DEF(CSyncObject		,	6	)
_MFC_CLASS_DEF(CWinThread		,	7	)
_MFC_CLASS_DEF(CWnd				,	8	)
_MFC_CLASS_DEF(CDialog			,	9	)
_MFC_CLASS_DEF(CMenu			,	10	)
_MFC_CLASS_DEF(COleControl		,	11	)
_MFC_CLASS_DEF(CView			,	12	)
_MFC_CLASS_DEF(CWinApp			,	13	)
_MFC_CLASS_DEF(CBrush			,	14	)
_MFC_CLASS_DEF(CButton			,	15	)
_MFC_CLASS_DEF(CBitmap			,	16	)
_MFC_CLASS_DEF(CAsyncSocket		,	17	)
_MFC_CLASS_DEF(CClientDC		,	18	)
_MFC_CLASS_DEF(CComboBox		,	19	)
_MFC_CLASS_DEF(CComboBoxEx		,	20	)
_MFC_CLASS_DEF(CCommonDialog	,	21	)
_MFC_CLASS_DEF(CControlBar		,	22	)
_MFC_CLASS_DEF(CCriticalSection	,	23	)
_MFC_CLASS_DEF(CCtrlView		,	24	)
_MFC_CLASS_DEF(CDHtmlDialog		,	25	)
_MFC_CLASS_DEF(CDialogBar		,	26	)
_MFC_CLASS_DEF(CDocument		,	27	)
_MFC_CLASS_DEF(CEdit			,	28	)
_MFC_CLASS_DEF(CEditView		,	29	)
_MFC_CLASS_DEF(CEvent			,	30	)
_MFC_CLASS_DEF(CException		,	31	)
_MFC_CLASS_DEF(CFileDialog		,	32	)
_MFC_CLASS_DEF(CFileException	,	33	)
_MFC_CLASS_DEF(CFont			,	34	)
_MFC_CLASS_DEF(CFontDialog		,	35	)
_MFC_CLASS_DEF(CFormView		,	36	)
_MFC_CLASS_DEF(CFrameWnd		,	37	)
_MFC_CLASS_DEF(CHeaderCtrl		,	38	)
_MFC_CLASS_DEF(CHotKeyCtrl		,	39	)
_MFC_CLASS_DEF(CHtmlView		,	40	)
_MFC_CLASS_DEF(CHttpFile		,	41	)
_MFC_CLASS_DEF(CListBox			,	42	)
_MFC_CLASS_DEF(CListCtrl		,	43	)
_MFC_CLASS_DEF(CListView		,	44	)
_MFC_CLASS_DEF(CMDIChildWnd		,	45	)
_MFC_CLASS_DEF(CMDIFrameWnd		,	46	)
_MFC_CLASS_DEF(CMemoryException	,	47	)
_MFC_CLASS_DEF(CMutex			,	48	)
_MFC_CLASS_DEF(CPaintDC			,	49	)
_MFC_CLASS_DEF(CPen				,	50	)
_MFC_CLASS_DEF(CRichEditCtrl	,	51	)
_MFC_CLASS_DEF(CRichEditView	,	52	)
_MFC_CLASS_DEF(CScrollBar		,	53	)
_MFC_CLASS_DEF(CScrollView		,	54	)
_MFC_CLASS_DEF(CSliderCtrl		,	55	)
_MFC_CLASS_DEF(CSocket			,	56	)
_MFC_CLASS_DEF(CSplitterWnd		,	57	)
_MFC_CLASS_DEF(CStatic			,	58	)
_MFC_CLASS_DEF(CStatusBar		,	59	)
_MFC_CLASS_DEF(CStatusBarCtrl	,	60	)
_MFC_CLASS_DEF(CStdioFile		,	61	)
_MFC_CLASS_DEF(CTabCtrl			,	62	)
_MFC_CLASS_DEF(CToolBar			,	63	)
_MFC_CLASS_DEF(CToolBarCtrl		,	64	)
_MFC_CLASS_DEF(CTreeCtrl		,	65	)
_MFC_CLASS_DEF(CTreeView		,	66	)
_MFC_CLASS_DEF(CUserException	,	67	)
_MFC_CLASS_DEF(CWaitCursor		,	68	)
_MFC_CLASS_DEF(CWindowDC		,	69	)
//_MFC_CLASS_DEF(CPoint			,	70	)
//_MFC_CLASS_DEF(CRect			,	71	)
//_MFC_CLASS_DEF(CSize			,	72	)

_MEMBER_TABLE_BEGIN_EXTERNAL(CPoint)
	_MEMBER_ENTRY(x)
	_MEMBER_ENTRY(y)
	_MEMBER_TABLE_TYPETRAITS_DECL_BEGIN()
		TYPETRAITS_DECL_TYPEID(rt::_typeid_afx)
		TYPETRAITS_DECL_EXTENDTYPEID((rt::_typeid_afx<<16) + 70)
		TYPETRAITS_DECL_IS_AGGREGATE(true)
	_MEMBER_TABLE_TYPETRAITS_DECL_END()
_MEMBER_TABLE_END_EXTERNAL()

_MEMBER_TABLE_BEGIN_EXTERNAL(CRect)
	_MEMBER_ENTRY(left)
	_MEMBER_ENTRY(top)
	_MEMBER_ENTRY(right)
	_MEMBER_ENTRY(bottom)
	_MEMBER_TABLE_TYPETRAITS_DECL_BEGIN()
		TYPETRAITS_DECL_TYPEID(rt::_typeid_afx)
		TYPETRAITS_DECL_EXTENDTYPEID((rt::_typeid_afx<<16) + 71)
		TYPETRAITS_DECL_IS_AGGREGATE(true)
	_MEMBER_TABLE_TYPETRAITS_DECL_END()
_MEMBER_TABLE_END_EXTERNAL()

_MEMBER_TABLE_BEGIN_EXTERNAL(CSize)
	_MEMBER_ENTRY(cx)
	_MEMBER_ENTRY(cy)
	_MEMBER_TABLE_TYPETRAITS_DECL_BEGIN()
		TYPETRAITS_DECL_TYPEID(rt::_typeid_afx)
		TYPETRAITS_DECL_EXTENDTYPEID((rt::_typeid_afx<<16) + 72)
		TYPETRAITS_DECL_IS_AGGREGATE(true)
	_MEMBER_TABLE_TYPETRAITS_DECL_END()
_MEMBER_TABLE_END_EXTERNAL()




#define _MFC_CONTAINER(cls_name, is_array, etid_minor) /* etid_major is _typeid_afx */				\
	template<typename T, typename Arg>	class cls_name;													\
	namespace rt{																						\
		enum{ _typeid_afx_##cls_name = (_typeid_afx<<16)+etid_minor };									\
		template<typename t_Ele, typename Arg> class TypeTraits<cls_name<t_Ele,Arg> >					\
		{		typedef rt::TypeTraits<t_Ele> TT_ele;													\
				typedef rt::TypeTraits<t_Ele> TT_arg;													\
		public:																							\
				typedef cls_name<typename TT_ele::t_Accum, typename TT_arg::t_Accum>  t_Accum;			\
				typedef cls_name<typename TT_ele::t_Val,   typename TT_arg::t_Val>	  t_Val;			\
				typedef t_Ele t_Element;																\
				typedef cls_name<typename TT_ele::t_Signed,typename TT_arg::t_Signed> t_Signed;			\
				enum{ Typeid = _typeid_afx };															\
				enum{ ExtendTypeid = (_typeid_afx<<16)+etid_minor };									\
				enum{ IsAggregate = false };															\
				enum{ IsNumeric = false };																\
				enum{ IsArray = is_array };																\
				enum{ IsImage = false };																\
				enum{ Length = TYPETRAITS_SIZE_UNKNOWN };												\
				template<class ostream>																	\
				static void TypeName(ostream & outstr)													\
					{ outstr<<_T(#cls_name)<<_T('<'); TT_ele::TypeName(outstr); outstr<<_T('>'); }		\
				struct text_output_spec																	\
				{	enum{ width_total = TYPETRAITS_SIZE_UNKNOWN };										\
					enum{ width_after_point = TYPETRAITS_SIZE_UNKNOWN };								\
		};		};																						\
		namespace _ExtendedType{																		\
			template<typename T, typename Arg>															\
			::rt::_meta_::_ExtenedTypeId_Struct<_typeid_afx_##cls_name>									\
				_DefineExtendedTypeId(cls_name<T, Arg>* p);												\
		template<typename T> class TypeTraitsEx<T, _typeid_afx_##cls_name >								\
			:public ::rt::TypeTraits<T>{};																\
	}} /* namespace _ExtendedTyp / namespace rt */														\


#define _MFC_TYPED_CONTAINER(cls_name, t_ele, is_array, etid_minor)	/* etid_major is _typeid_afx */	\
	class cls_name;																						\
	namespace rt{																						\
		enum{ _typeid_afx_##cls_name = (_typeid_afx<<16)+etid_minor };									\
		namespace _ExtendedType{																		\
		::rt::_meta_::_ExtenedTypeId_Struct<_typeid_afx_##cls_name> _DefineExtendedTypeId(cls_name* p);	\
		template<typename T> class TypeTraitsEx<T, _typeid_afx_##cls_name>								\
		{public:typedef T t_Accum;																		\
				typedef T t_Val;																		\
				typedef t_ele t_Element;																\
				typedef T t_Signed;																		\
				enum{ Typeid = _typeid_afx };															\
				enum{ ExtendTypeid = _typeid_afx_##cls_name };											\
				enum{ IsAggregate = false };															\
				enum{ IsNumeric = false };																\
				enum{ IsArray = is_array };																\
				enum{ IsImage = false };																\
				enum{ Length = TYPETRAITS_SIZE_UNKNOWN };												\
				template<class ostream>																	\
				static void TypeName(ostream & outstr){ outstr<<_T(#cls_name);  }						\
				struct text_output_spec																	\
				{	enum{ width_total = TYPETRAITS_SIZE_UNKNOWN };										\
					enum{ width_after_point = TYPETRAITS_SIZE_UNKNOWN };								\
		};		};																						\
	}} /* namespace _ExtendedTyp / namespace rt */														\

_MFC_CONTAINER(CArray							,true,	200)
_MFC_TYPED_CONTAINER(CByteArray		,BYTE		,true,	201)
_MFC_TYPED_CONTAINER(CDWordArray	,DWORD		,true,	202)
_MFC_TYPED_CONTAINER(CObArray		,CObject*	,true,	203)
_MFC_TYPED_CONTAINER(CPtrArray		,LPVOID		,true,	204)
_MFC_TYPED_CONTAINER(CStringArray	,CString	,true,	205)
_MFC_TYPED_CONTAINER(CUIntArray		,UINT		,true,	206)
_MFC_TYPED_CONTAINER(CWordArray		,WORD		,true,	207)

_MFC_CONTAINER(CList							,false,	230)
_MFC_TYPED_CONTAINER(CObList		,CObject*	,false,	231)
_MFC_TYPED_CONTAINER(CPtrList		,LPVOID		,false,	232)
_MFC_TYPED_CONTAINER(CStringList	,CString	,false,	233)

template<class KEY, class ARG_KEY, class VALUE, class ARG_VALUE> class CMap;
namespace rt{																					
	enum{ _typeid_afx_CMap = (_typeid_afx<<16)+400 };								
	template<class KEY, class ARG_KEY, class t_Ele, class ARG_VALUE>
	class TypeTraits<CMap<KEY,ARG_KEY,t_Ele,ARG_VALUE> >				
	{		typedef rt::TypeTraits<t_Ele> TT_ele;												
			typedef rt::TypeTraits<t_Ele> TT_arg;												
	public:																						
			typedef CMap<KEY,ARG_KEY, typename TT_ele::t_Accum, typename TT_arg::t_Accum>	t_Accum;		
			typedef CMap<KEY,ARG_KEY, typename TT_ele::t_Val,   typename TT_arg::t_Val>		t_Val;		
			typedef t_Ele t_Element;															
			typedef CMap<KEY,ARG_KEY, typename TT_ele::t_Signed,typename TT_arg::t_Signed>	t_Signed;		
			enum{ Typeid = _typeid_afx };														
			enum{ ExtendTypeid = _typeid_afx_CMap };								
			enum{ IsAggregate = false };														
			enum{ IsNumeric = false };															
			enum{ IsArray = false };															
			enum{ IsImage = false };
			enum{ Length = TYPETRAITS_SIZE_UNKNOWN };											
			template<class ostream>																
			static void TypeName(ostream & outstr)												
				{ outstr<<_meta_::_text_CMap_(); TT_ele::TypeName(outstr); outstr<<_T('>'); }	
			struct text_output_spec																
			{	enum{ width_total = TYPETRAITS_SIZE_UNKNOWN };									
				enum{ width_after_point = TYPETRAITS_SIZE_UNKNOWN };							
	};		};																					
namespace _ExtendedType{																		
		template<class KEY, class ARG_KEY, class VALUE, class ARG_VALUE>
		::rt::_meta_::_ExtenedTypeId_Struct<_typeid_afx_CMap>								
			_DefineExtendedTypeId(CMap<KEY,ARG_KEY,VALUE,ARG_VALUE>* p);											
		template<typename T> class TypeTraitsEx<T, _typeid_afx_CMap >					
			:public ::rt::TypeTraits<T>{};															
}} /* namespace _ExtendedTyp / namespace rt */													


//_MFC_MAP(CMap				,	400)
_MFC_TYPED_CONTAINER(CMapPtrToPtr		,LPVOID		,false	,401)
_MFC_TYPED_CONTAINER(CMapPtrToWord		,WORD		,false	,402)
_MFC_TYPED_CONTAINER(CMapStringToOb		,CObject*	,false	,403)
_MFC_TYPED_CONTAINER(CMapStringToPtr	,LPVOID		,false	,404)
_MFC_TYPED_CONTAINER(CMapStringToString	,CString	,false	,405)
_MFC_TYPED_CONTAINER(CMapWordToOb		,CObject*	,false	,406)
_MFC_TYPED_CONTAINER(CMapWordToPtr		,LPVOID		,false	,407)

#undef _MFC_CONTAINER
#undef _MFC_TYPED_CONTAINER

namespace ATL
{ template< typename BaseType, class StringTraits > class CStringT; }
namespace rt{																							
	enum{ _typeid_afx_CStringT = (_typeid_afx<<16)+500 };												
	enum{ _typeid_afx_CString  = (_typeid_afx<<16)+500+sizeof(TCHAR) };			
	enum{ _typeid_afx_CStringA = (_typeid_afx<<16)+502 };
	enum{ _typeid_afx_CStringW = (_typeid_afx<<16)+501 };
	template<typename t_Ele, class StringTraits> 
	class TypeTraits< ::ATL::CStringT<t_Ele,StringTraits> >	
	{		typedef rt::TypeTraits<t_Ele> TT_ele;														
	public:																								
			typedef CStringT<t_Ele,StringTraits>	t_Accum;											
			typedef CStringT<t_Ele,StringTraits>	t_Val;														
			typedef t_Ele							t_Element;											
			typedef CStringT<t_Ele,StringTraits>	t_Signed;											
			enum{ Typeid = _typeid_afx };																
			enum{ ExtendTypeid = _typeid_afx_CStringT };													
			enum{ IsAggregate = false };																
			enum{ IsNumeric = false };																	
			enum{ IsArray = true };																		
			enum{ IsImage = false };
			enum{ Length = TYPETRAITS_SIZE_UNKNOWN };													
			template<class ostream>																		
			static void TypeName(ostream & outstr)														
			{	
				__static_if((rt::IsTypeSame<t_Ele,TCHAR>::Result))
				{	outstr<<_T("CString");
					return;
				}
				__static_if((rt::IsTypeSame<t_Ele,CHAR>::Result))
				{	outstr<<_T("CStringA");
					return;
				}
				__static_if((rt::IsTypeSame<t_Ele,WCHAR>::Result))
				{	outstr<<_T("CStringW");
					return;
				}
				outstr<<_T("CStringT<"); TT_ele::TypeName(outstr); outstr<<_T('>'); 
			}
			struct text_output_spec																		
			{	enum{ width_total = TYPETRAITS_SIZE_UNKNOWN };											
				enum{ width_after_point = TYPETRAITS_SIZE_UNKNOWN };									
			};		
	};
}	// namespace rt

namespace rt{																							
namespace _ExtendedType{																				
		template<typename t_Ele, typename StringTraits>	
		::rt::_meta_::_ExtenedTypeId_Struct<_typeid_afx_CStringT>
			_DefineExtendedTypeId(::ATL::CStringT<t_Ele,StringTraits>* p);	
		template<typename StringTraits>	
		::rt::_meta_::_ExtenedTypeId_Struct<_typeid_afx_CStringW>
			_DefineExtendedTypeId(::ATL::CStringT<WCHAR,StringTraits>* p);	
		template<typename StringTraits>	
		::rt::_meta_::_ExtenedTypeId_Struct<_typeid_afx_CStringA>
			_DefineExtendedTypeId(::ATL::CStringT<CHAR,StringTraits>* p);	
		template<typename T> class TypeTraitsEx<T, _typeid_afx_CStringT >									
			:public ::rt::TypeTraits<T>{};									
		template<typename T> class TypeTraitsEx<T, _typeid_afx_CStringA >									
			:public TypeTraitsEx<T, _typeid_afx_CStringT >{};										
		template<typename T> class TypeTraitsEx<T, _typeid_afx_CStringW >									
			:public TypeTraitsEx<T, _typeid_afx_CStringT >{};									
}} /* namespace _ExtendedTyp / namespace rt */															


#define PREDECLARE_ATLCLASS_T1(cls_name)	namespace ATL{ template<typename T> class cls_name; }
#define _ATL_CLASS_DEF_T1(cls_name, is_array, t_len, etid_minor)	/* etid_major is _typeid_afx */		\
	PREDECLARE_ATLCLASS_T1(cls_name)																	\
	namespace rt{																						\
		enum{ _typeid_afx_##cls_name = (_typeid_afx<<16)+etid_minor };					\
		template<typename t_Ele> class TypeTraits<cls_name<t_Ele> >										\
		{		typedef rt::TypeTraits<t_Ele> TT_ele;													\
		public:																							\
				typedef cls_name<t_Ele>  t_Accum;														\
				typedef cls_name<t_Ele>	  t_Val;														\
				typedef t_Ele t_Element;																\
				typedef cls_name<t_Ele> t_Signed;														\
				enum{ Typeid = _typeid_afx };															\
				enum{ ExtendTypeid = _typeid_afx_##cls_name };											\
				enum{ IsAggregate = false };															\
				enum{ IsNumeric = false };																\
				enum{ IsArray = is_array };																\
				enum{ IsImage = false };																\
				enum{ Length = t_len };																	\
				template<class ostream>																	\
				static void TypeName(ostream & outstr)													\
					{	outstr<<rt::_meta_::_text_ATL_()<<_T(#cls_name)<<_T('<');						\
						TT_ele::TypeName(outstr); outstr<<_T('>'); }									\
				struct text_output_spec																	\
				{	enum{ width_total = TYPETRAITS_SIZE_UNKNOWN };										\
					enum{ width_after_point = TYPETRAITS_SIZE_UNKNOWN };								\
		};		};																						\
		namespace _ExtendedType{																		\
			template<typename T> ::rt::_meta_::_ExtenedTypeId_Struct<_typeid_afx_##cls_name>			\
				_DefineExtendedTypeId(cls_name<T>* p);													\
		template<typename T> class TypeTraitsEx<T, _typeid_afx_##cls_name >								\
			:public ::rt::TypeTraits<T>{};																\
	}} /* namespace _ExtendedTyp / namespace rt */														\


_ATL_CLASS_DEF_T1(CComPtr			,false	,TYPETRAITS_SIZE_UNKNOWN	,8+2000)
_ATL_CLASS_DEF_T1(CAutoPtr			,false	,TYPETRAITS_SIZE_UNKNOWN	,18+2000)
_ATL_CLASS_DEF_T1(CAxWindowT		,false	,1							,3+2000)
_ATL_CLASS_DEF_T1(CComObject		,false	,1							,7+2000)
_ATL_CLASS_DEF_T1(CAutoVectorPtr	,false	,TYPETRAITS_SIZE_UNKNOWN	,22+2000)
_ATL_CLASS_DEF_T1(CAutoPtrArray		,true	,TYPETRAITS_SIZE_UNKNOWN	,37+2000)
_ATL_CLASS_DEF_T1(CAutoPtrList		,true	,TYPETRAITS_SIZE_UNKNOWN	,29+2000)
#undef PREDECLARE_ATLCLASS_T1
#undef _ATL_CLASS_DEF_T1


#define PREDECLARE_ATLCLASS_T2(cls_name) namespace ATL{ template<typename T, class T2> class cls_name; }
#define _ATL_CLASS_DEF_T2(cls_name, is_array, t_len, etid_minor)	/* etid_major is _typeid_afx */		\
	PREDECLARE_ATLCLASS_T2(cls_name)																	\
	namespace rt{																						\
		enum{ _typeid_afx_##cls_name = (_typeid_afx<<16)+etid_minor };									\
		template<typename t_Ele, class T2> class TypeTraits<cls_name<t_Ele,T2> >						\
		{		typedef rt::TypeTraits<t_Ele> TT_ele;													\
		public:																							\
				typedef cls_name<t_Ele,T2>	t_Accum;													\
				typedef cls_name<t_Ele,T2>	t_Val;														\
				typedef t_Ele				t_Element;													\
				typedef cls_name<t_Ele,T2>	t_Signed;													\
				enum{ Typeid = _typeid_afx };															\
				enum{ ExtendTypeid = _typeid_afx_##cls_name };									\
				enum{ IsAggregate = false };															\
				enum{ IsNumeric = false };																\
				enum{ IsArray = is_array };																\
				enum{ IsImage = false };																\
				enum{ Length = t_len };																	\
				template<class ostream>																	\
				static void TypeName(ostream & outstr)													\
					{	outstr<<rt::_meta_::_text_ATL_()<<_T(#cls_name)<<_T('<');						\
						TT_ele::TypeName(outstr); outstr<<_T('>'); }									\
				struct text_output_spec																	\
				{	enum{ width_total = TYPETRAITS_SIZE_UNKNOWN };										\
					enum{ width_after_point = TYPETRAITS_SIZE_UNKNOWN };								\
		};		};																						\
		namespace _ExtendedType{																		\
			template<typename T, class T2> ::rt::_meta_::_ExtenedTypeId_Struct<_typeid_afx_##cls_name>	\
				_DefineExtendedTypeId(cls_name<T,T2>* p);												\
		template<typename T> class TypeTraitsEx<T, (_typeid_afx<<16)+etid_minor >						\
			:public ::rt::TypeTraits<T>{};																\
	}} /* namespace _ExtendedTyp / namespace rt */														\

_ATL_CLASS_DEF_T2(CHeapPtr			,false	,TYPETRAITS_SIZE_UNKNOWN	,11+2000)
_ATL_CLASS_DEF_T2(CComControl		,false	,1							,5+2000)
_ATL_CLASS_DEF_T2(CHeapPtrList		,false	,TYPETRAITS_SIZE_UNKNOWN	,12+2000)
_ATL_CLASS_DEF_T2(CSimpleArray		,true	,TYPETRAITS_SIZE_UNKNOWN	,40+2000)
_ATL_CLASS_DEF_T2(CAtlArray			,true	,TYPETRAITS_SIZE_UNKNOWN	,35+2000)
_ATL_CLASS_DEF_T2(CAtlList			,false	,TYPETRAITS_SIZE_UNKNOWN	,17+2000)
#undef PREDECLARE_ATLCLASS_T2
#undef _ATL_CLASS_DEF_T2


#ifdef UNICODE
	#define RPC_CTSTR RPC_WSTR
#else
	#define RPC_CTSTR RPC_CSTR
#endif

#define PREDECLARE_ATLCLASS_T2_CLSID(cls_name) namespace ATL{ template<typename T, const CLSID* clsid> class cls_name; }
#define _ATL_CLASS_DEF_T2_CLSID(cls_name, is_array, t_len, etid_minor)							\
	PREDECLARE_ATLCLASS_T2_CLSID(cls_name)														\
	namespace rt{																				\
		enum{ _typeid_afx_##cls_name = (_typeid_afx<<16)+etid_minor };							\
		template<typename t_Ele, const CLSID* clsid> class TypeTraits<cls_name<t_Ele,clsid> >	\
		{		typedef rt::TypeTraits<t_Ele> TT_ele;											\
		public:																					\
				typedef cls_name<t_Ele,clsid>	t_Accum;										\
				typedef cls_name<t_Ele,clsid>	t_Val;											\
				typedef t_Ele					t_Element;										\
				typedef cls_name<t_Ele,clsid>	t_Signed;										\
				enum{ Typeid = _typeid_afx };													\
				enum{ ExtendTypeid = _typeid_afx_##cls_name };									\
				enum{ IsAggregate = false };													\
				enum{ IsNumeric = false };														\
				enum{ IsArray = is_array };														\
				enum{ IsImage = false };														\
				enum{ Length = t_len };															\
				template<class ostream>															\
				static void TypeName(ostream & outstr)											\
					{	outstr<<rt::_meta_::_text_ATL_()<<_T(#cls_name)<<_T('<');				\
						TT_ele::TypeName(outstr); outstr<<_meta_::_text_UUID_LEFT_();			\
						TCHAR* string=NULL;														\
						UUID* p = (UUID*)rt::_CastToNonconst(clsid);							\
						VERIFY(RPC_S_OK == UuidToString(p,(RPC_CTSTR *)&string));				\
						outstr<<string;															\
						if(string)RpcStringFree((RPC_CTSTR *)&string);							\
						outstr<<_meta_::_text_UUID_RIGHT_();									\
					}																			\
				struct text_output_spec															\
				{	enum{ width_total = TYPETRAITS_SIZE_UNKNOWN };								\
					enum{ width_after_point = TYPETRAITS_SIZE_UNKNOWN };						\
		};		};																				\
		namespace _ExtendedType{																\
			template<typename T, const CLSID* clsid>											\
				::rt::_meta_::_ExtenedTypeId_Struct<_typeid_afx_##cls_name>						\
					_DefineExtendedTypeId(cls_name<T,clsid>* p);								\
		template<typename T> class TypeTraitsEx<T, _typeid_afx_##cls_name >						\
			:public ::rt::TypeTraits<T>{};														\
	}} /* namespace _ExtendedTyp / namespace rt */												\


_ATL_CLASS_DEF_T2_CLSID(CComCoClass		,false	,1							,4+2000)  
_ATL_CLASS_DEF_T2_CLSID(CInterfaceArray	,true	,TYPETRAITS_SIZE_UNKNOWN	,14+2000)
_ATL_CLASS_DEF_T2_CLSID(CInterfaceList	,false	,TYPETRAITS_SIZE_UNKNOWN	,15+2000)
#undef _ATL_CLASS_DEF_T2_CLSID
#undef PREDECLARE_ATLCLASS_T2_CLSID
#undef RPC_CTSTR




#define PREDECLARE_ATLCLASS(cls_name)		namespace ATL{ class cls_name; }
#define _ATL_CLASS_DEF(cls_name, etid_minor)	/* etid_major is _typeid_afx */							\
	PREDECLARE_ATLCLASS(cls_name)																		\
	namespace rt{																						\
		enum{ _typeid_afx_##cls_name = (_typeid_afx<<16)+etid_minor };									\
		namespace _ExtendedType{																		\
		template<typename T> class TypeTraitsEx<T, _typeid_afx_##cls_name>								\
		{public:																						\
				typedef T t_Accum;																		\
				typedef T t_Val;																		\
				typedef void t_Element;																	\
				typedef T t_Signed;																		\
				enum{ Typeid = _typeid_afx };															\
				enum{ ExtendTypeid = _typeid_afx_##cls_name };											\
				enum{ IsAggregate = _atl_is_pod };														\
				enum{ IsNumeric = false };																\
				enum{ IsArray = false };																\
				enum{ IsImage = false };																\
				enum{ Length = 1 };																		\
				template<class ostream>																	\
				static void TypeName(ostream & outstr)													\
					{ outstr<<rt::_meta_::_text_ATL_()<<_T(#cls_name);  }								\
				struct text_output_spec																	\
				{	enum{ width_total = TYPETRAITS_SIZE_UNKNOWN };										\
					enum{ width_after_point = TYPETRAITS_SIZE_UNKNOWN };								\
				};																						\
		};																								\
		::rt::_meta_::_ExtenedTypeId_Struct<_typeid_afx_##cls_name>										\
			_DefineExtendedTypeId(ATL::cls_name* p);													\
	}} /* namespace _ExtendedType / namespace rt */														\

#define _atl_is_pod true
_ATL_CLASS_DEF(CFileTime		,600)
_ATL_CLASS_DEF(CTime			,601)
_ATL_CLASS_DEF(CTimeSpan		,602)
#undef _atl_is_pod

#define _atl_is_pod false
_ATL_CLASS_DEF(CComBSTR			,1+2000)
_ATL_CLASS_DEF(CWindow			,2+2000)
_ATL_CLASS_DEF(CComControlBase	,6+2000)
_ATL_CLASS_DEF(CComVariant		,9+2000)
_ATL_CLASS_DEF(CHandle			,10+2000)
_ATL_CLASS_DEF(CImage			,13+2000)
_ATL_CLASS_DEF(CRegKey			,19+2000)


#ifdef TYPETRAITS_ENABLE_ALL_ATLMFC
	#define INCLUDE_AFX_TYPETRAITS_EX
	#include "type_traits_afx_more.h"
	#undef INCLUDE_AFX_TYPETRAITS_EX
#endif

#undef _atl_is_pod
#undef _MFC_CLASS_DEF
#undef _ATL_CLASS_DEF
#undef PREDECLARE_ATLCLASS


TYPETRAITS_DECL_STRING_CLASS(rt::_typeid_afx_CStringT)
TYPETRAITS_DECL_STRING_CLASS(rt::_typeid_afx_CStringW)
TYPETRAITS_DECL_STRING_CLASS(rt::_typeid_afx_CStringA)

