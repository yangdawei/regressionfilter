#pragma once

//////////////////////////////////////////////////////////////////////
// Visual Computing Foundation Classes (VCFC)
//					Jiaping Wang  2006.2
//					e_boris2002@hotmail.com
//
// (C) Copyright Jiaping Wang 2004.
//
// Permission to copy, use, modify, sell and distribute this software
// is granted provided this copyright notice appears in all copies.
// This software is provided "as is" without express or implied
// warranty, and with no claim as to its suitability for any purpose.
//
// Absolutly no garanty!!
//////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////
//  Brief Description
//
//  type_traits_stl.h
//
//	TypeTraits for win32 structures
//
//
//////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////
// Revise History
//
// Inital version	2009.4.22		Jiaping
//
//////////////////////////////////////////////////////////////////////

#include "type_traits_ext.h"


_MEMBER_TABLE_BEGIN_EXTERNAL(GUID)
	_MEMBER_ENTRY(Data1)
	_MEMBER_ENTRY(Data2)
	_MEMBER_ENTRY(Data3)
	_MEMBER_ENTRY(Data4)
	_MEMBER_TABLE_TYPETRAITS_DECL_BEGIN()
		TYPETRAITS_DECL_TYPEID(rt::_typeid_w32)
		TYPETRAITS_DECL_EXTENDTYPEID((rt::_typeid_w32<<16) + 1)
		TYPETRAITS_DECL_IS_AGGREGATE(true)
	_MEMBER_TABLE_TYPETRAITS_DECL_END()
_MEMBER_TABLE_END_EXTERNAL()
