#pragma once

//////////////////////////////////////////////////////////////////////
// Visual Computing Foundation Classes (VCFC)
//					Jiaping Wang  2006.2
//					e_boris2002@hotmail.com
//
// (C) Copyright Jiaping Wang 2004.
//
// Permission to copy, use, modify, sell and distribute this software
// is granted provided this copyright notice appears in all copies.
// This software is provided "as is" without express or implied
// warranty, and with no claim as to its suitability for any purpose.
//
// Absolutely no warranty!!
//////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////
//  Brief Description
//
//  compact_vector.h
//
//  instantiations of _typedvector for linear and compact types
//
//
//////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////
// Revise History
//
// Inital version	2006.6.7		Jiaping
//
//////////////////////////////////////////////////////////////////////


#include "vector_type.h"

#pragma warning(disable: 4616)
#pragma warning(disable: 4522 4311 1684)

namespace rt
{

//////////////////////////////////////////////////////////////////////
//Dynamic length linear compact vector storage
namespace _meta_
{
// BaseVectorRoot
template<typename t_Val, typename SIZETYPE>
class _BaseVecDynRoot
{
private:
	DYNTYPE_FUNC void __SafeFree()
	{	if(!_p)return;
		__static_if(IsElementNotAggregate)
		{ for(UINT i=0;i<_len;i++)_p[i].~t_Val(); }
		_SafeFree32AL(_p);
	}
public:
	typedef SIZETYPE t_SizeType;
	static const bool IsElementNotAggregate = !rt::TypeTraits<t_Val>::IsAggregate ;
	static const bool IsRef = false;
	DYNTYPE_FUNC _BaseVecDynRoot(){ _p=NULL; _len=0; }
	DYNTYPE_FUNC ~_BaseVecDynRoot(){ __SafeFree(); }
	DYNTYPE_FUNC explicit _BaseVecDynRoot(const _BaseVecDynRoot &x){ ASSERT_STATIC(0); }	//copy ctor should be avoided, use reference for function parameters
	DYNTYPE_FUNC BOOL SetSize(SIZETYPE co=0) //zero for clear
	{	ASSERT(co>=0);
		if(co == _len){ return TRUE; }
		else
		{	__SafeFree();
			_len = co;
			if(co)
			{	_p = _Malloc32AL(t_Val,co);
				if( _p )
				{	__static_if(IsElementNotAggregate)
					{ for(UINT i=0;i<_len;i++)new (&_p[i]) t_Val(); }	//call ctor
				}
				else
				{ _len = 0; return FALSE; }
			}
		}
		return TRUE;
	}
	DYNTYPE_FUNC BOOL ChangeSize(SIZETYPE new_size) //Orignal data at front is preserved
	{	
		if( new_size == _len )return TRUE;
		if( new_size<_len )
		{	
			__static_if(IsElementNotAggregate)		//call dtor for unwanted instances at back
			{ for(SIZETYPE i=new_size;i<_len;i++)_p[i].~t_Val(); }

			_len = new_size;
			return TRUE;
		}
		else	//expand buffer
		{	t_Val* new_p = _Malloc32AL(t_Val,new_size);
			if(new_p)
			{	memcpy(new_p,_p,_len*sizeof(t_Val));
				_SafeFree32AL(_p);
				_p = new_p;
			
				__static_if(IsElementNotAggregate)	//call ctor for additional instances at back
				{ for(SIZETYPE i=_len;i<new_size;i++)new (&_p[i]) t_Val(); }

				_len = new_size;
				return TRUE;
			}else{ return FALSE; }
		}
		
		return FALSE;
	}

	//TypeTraits
	TYPETRAITS_DECL_LENGTH(TYPETRAITS_SIZE_UNKNOWN)	
	TYPETRAITS_DECL_IS_AGGREGATE(false)

	template<typename T>
	DYNTYPE_FUNC t_Val & At(const T index){ return _p[(int)index]; }
	template<typename T>
	DYNTYPE_FUNC const t_Val & At(const T index)const { return _p[(int)index]; }
	DYNTYPE_FUNC SIZETYPE GetSize() const{ return _len; }
	static const bool IsCompactLinear = true;
protected:
	t_Val*	_p;
	SIZETYPE	_len;
};

} // namespace _meta_
template<typename SIZETYPE>
class _BaseVecDyn
{public:	template<typename t_Ele> class SpecifyItemType
			{public: typedef rt::_meta_::_BaseVecDynRoot<t_Ele, SIZETYPE> t_BaseVec; };
			static const bool IsCompactLinear = true;
};

namespace _meta_
{
// _BaseVecDynExRoot an alternative for std::vector
template<typename t_Val, typename SIZETYPE>
class _BaseVecDynExRoot:public rt::_meta_::_BaseVecDynRoot<t_Val, SIZETYPE>
{
	DYNTYPE_FUNC BOOL _add_one_entry() // the added entry's ctor is not called !!
	{	if(_len+1 <= _len_reserved){} // expand elements only
		else // expand buffer
		{	SIZETYPE new_buf_resv = __max(__max(4,_len+1),_len_reserved*2);
			t_Val* pNewBuffer = (t_Val*)rt::Malloc32AL(sizeof(t_Val)*new_buf_resv,TRUE); // no ask if failure
			if(pNewBuffer){}else
			{	new_buf_resv = _len+1;
				pNewBuffer = (t_Val*)_Malloc32AL(t_Val,new_buf_resv);
				if(pNewBuffer){}else{ return FALSE; }
			}
			// copy old elements
			memcpy(pNewBuffer,_p,sizeof(t_Val)*_len);
			_SafeFree32AL(_p);
			_p = (t_Val*)pNewBuffer;
			_len_reserved = new_buf_resv;
		}
		_len++;
		return TRUE;
	}

protected:
	SIZETYPE	_len_reserved;
public:
	DYNTYPE_FUNC _BaseVecDynExRoot(){ _len_reserved=0; }
	DYNTYPE_FUNC explicit _BaseVecDynExRoot(const _BaseVecDynExRoot &x){ ASSERT_STATIC(0); }	//copy ctor should be avoided, use reference for function parameters
	DYNTYPE_FUNC BOOL SetSize(SIZETYPE co=0) //zero for clear
	{	if(__super::SetSize(co)){ _len_reserved=_len; return TRUE; }
		else{ _len_reserved=0; return FALSE; }
	}
	DYNTYPE_FUNC BOOL ChangeSize(SIZETYPE new_size) //Orignal data at front is preserved
	{	if( new_size == _len )return TRUE;
		if( new_size<_len ){ return __super::ChangeSize(new_size); }
		else 
		{	if(new_size <= _len_reserved){} // expand elements only
			else // expand buffer
			{	_len_reserved = __max(__max(16,new_size),_len_reserved*2);
				LPBYTE pNewBuffer = (LPBYTE)rt::Malloc32AL(sizeof(t_Val)*_len_reserved,TRUE); // no ask if failure
				if(pNewBuffer){}else
				{	_len_reserved = new_size;
					pNewBuffer = (LPBYTE)_Malloc32AL(t_Val,_len_reserved);
					if(pNewBuffer){}else{ return FALSE; }
				}
				// copy old elements
				memcpy(pNewBuffer,_p,sizeof(t_Val)*_len);
				_SafeFree32AL(_p);
				_p = (t_Val*)pNewBuffer;
			}
			// call ctor for additional instances at back
			__static_if(IsElementNotAggregate)	
			{ for(SIZETYPE i=_len;i<new_size;i++)new (&_p[i]) t_Val(); }
			_len = new_size;
			return TRUE;
		}
	}
	DYNTYPE_FUNC SIZETYPE GetReservedSize() const { return _len_reserved; }
	DYNTYPE_FUNC t_Val& push_front()
	{	VERIFY(_add_one_entry());
		memmove(&_p[1],&_p[0],sizeof(t_Val)*(_len-1));
		__static_if(IsElementNotAggregate){ new (&_p[0]) t_Val(); }
		return _p[0];
	}
	template<typename T>
	DYNTYPE_FUNC t_Val& push_front(const T& x)
	{	VERIFY(_add_one_entry());
		memmove(&_p[1],&_p[0],sizeof(t_Val)*(_len-1));
#pragma warning(disable:4244)
		__static_if(IsElementNotAggregate){ new (&_p[0]) t_Val(x); }
		__static_if(!IsElementNotAggregate){ _p[0] = x; }
#pragma warning(default:4244)
		return _p[0];
	}
	DYNTYPE_FUNC t_Val& push_back()
	{	VERIFY(_add_one_entry());
		__static_if(IsElementNotAggregate){ new (&_p[_len-1]) t_Val(); }
		return _p[_len-1];
	}
	template<typename T>
	DYNTYPE_FUNC t_Val& push_back(const T& x)
	{	VERIFY(_add_one_entry());
#pragma warning(disable:4244)
		__static_if(IsElementNotAggregate){ new (&_p[_len-1]) t_Val(x); }
		__static_if(!IsElementNotAggregate){ _p[_len-1] = x; }
#pragma warning(default:4244)
		return _p[_len-1];
	}
	template<typename T>
	DYNTYPE_FUNC void push_back(const T* x, UINT count)
	{	
		SIZETYPE sz = GetSize();
		VERIFY(ChangeSize(sz + count));
#pragma warning(disable:4244)
		static const bool _is_type_same = rt::IsTypeSame<T,t_Val>::Result;
		__static_if(IsElementNotAggregate){ for(;sz<_len;sz++){ new (&_p[sz]) t_Val(*x++); } }
		__static_if(!IsElementNotAggregate && _is_type_same){ memcpy(&_p[sz],x,sizeof(t_Val)*count); }
		__static_if(!IsElementNotAggregate &&!_is_type_same){ for(;sz<_len;sz++){ _p[sz] = *x++; } }
#pragma warning(default:4244)
	}
	DYNTYPE_FUNC void erase(SIZETYPE index)
	{	ASSERT(index<_len);
		// call dtor for removed items
		__static_if(IsElementNotAggregate){ _p[index].~t_Val(); }
		_len--;
		memmove(&_p[index],&_p[index+1],sizeof(t_Val)*(_len-index));
	}
	DYNTYPE_FUNC void erase(SIZETYPE index_begin,SIZETYPE index_end)
	{	ASSERT(index_begin<index_end);
		ASSERT(index_end<=_len);
		// call dtor for removed items
		__static_if(IsElementNotAggregate)
		{ for(SIZETYPE i=index_begin;i<index_end;i++)_p[i].~t_Val(); }
		memmove(&_p[index_begin],&_p[index_end],sizeof(t_Val)*(_len-index_end));
		_len-=(index_end-index_begin);
	}
	DYNTYPE_FUNC void pop_back()
	{	ASSERT(_len); 	_len--;
		__static_if(IsElementNotAggregate){ _p[_len].~t_Val(); }
	}
	DYNTYPE_FUNC void pop_front()
	{	ASSERT(_len); 	_len--;
		__static_if(IsElementNotAggregate){ _p[0].~t_Val(); }
		memmove(&_p[0],&_p[1],sizeof(t_Val)*_len);
	}
	DYNTYPE_FUNC void compact_memory()
	{	if(_len < _len_reserved)
		{	LPBYTE pNew = (LPBYTE)rt::Malloc32AL(sizeof(t_Val)*_len,TRUE);
			if(pNew)
			{	memcpy(pNew,_p,sizeof(t_Val)*_len);
				_SafeFree32AL(_p);
				_p = pNew;
				_len_reserved = _len;
			}
		}
	};
	DYNTYPE_FUNC BOOL reserve(SIZETYPE co)
	{	if(co>_len_reserved)
		{	LPBYTE pNew = (LPBYTE)::rt::Malloc32AL(sizeof(t_Val)*co,TRUE);
			if(pNew)
			{	memcpy(pNew,_p,sizeof(t_Val)*_len);
				_SafeFree32AL(_p);
				_p = (t_Val*)pNew;
				_len_reserved = co;
			}
			return (BOOL)pNew;
		}
		return TRUE;
	}

	DYNTYPE_FUNC t_Val& insert(SIZETYPE index)
	{	VERIFY(_add_one_entry());
		memmove(&At(index+1),&At(index),(_len-index-1)*sizeof(t_Val));
		__static_if(IsElementNotAggregate){ new (&_p[index]) t_Val(); }
		return _p[index];
	}

	DYNTYPE_FUNC t_Val& first(){ ASSERT(_len); return _p[0]; }
	DYNTYPE_FUNC const t_Val& first()const{  ASSERT(_len); return _p[0]; }
	DYNTYPE_FUNC t_Val& last(){  ASSERT(_len); return _p[_len-1]; }
	DYNTYPE_FUNC const t_Val& last()const{  ASSERT(_len); return _p[_len-1]; }


	//not implemented yet, :P. complete them whenever needed	DYNTYPE_FUNC BOOL Insert(UINT index,const t_Val& x);
	DYNTYPE_FUNC void insert(SIZETYPE index,SIZETYPE count);
	DYNTYPE_FUNC void insert(SIZETYPE index,SIZETYPE count,const t_Val& x);
};

} // namespace _meta_
template<typename SIZETYPE>
class _BaseVecDynEx
{public:	template<typename t_Ele> class SpecifyItemType
			{public: typedef rt::_meta_::_BaseVecDynExRoot<t_Ele, SIZETYPE> t_BaseVec; };
			static const bool IsCompactLinear = true;
};



//pre-declaration
template<typename t_Ele>
class Buffer_Ref;

/////////////////////////////////////////////////////////////
// Class for dynamic/fix linear and compact vectors
template< typename t_Val, class t_BaseVec=rt::_BaseVecDyn<SIZE_T> >
class Buffer:public rt::_TypedVector<t_Val,t_BaseVec>
{	
public:
	//////////////////////////////////////////////////////
	//obtain linear memory
	//I try to use __static_if to hide these functions if storage
	//is not linear and compact, but Compiler of VC'05 crashes here
	//
	//__static_if(t_BaseVec::IsCompactLinear){ 
	DYNTYPE_FUNC operator LPItemType(){ return &At(0); }
	DYNTYPE_FUNC operator LPCItemType()const { return &At(0); }
	DYNTYPE_FUNC operator LPVOID(){ return &At(0); }
	DYNTYPE_FUNC operator LPCVOID()const { return &At(0); }
	
	DYNTYPE_FUNC LPCItemType Begin()const { return &At(0); }
	DYNTYPE_FUNC LPItemType  Begin(){ return &At(0); }
	DYNTYPE_FUNC LPCItemType End()const { return &At(GetSize()); }
	DYNTYPE_FUNC LPItemType  End(){ return &At(GetSize()); }
	//}

public:
	typedef rt::Buffer_Ref<t_Val> Ref;

	DYNTYPE_FUNC Ref GetRef(){ return Ref(*this); }
	DYNTYPE_FUNC Ref GetRef(SIZETYPE x,SIZETYPE len){ return Ref(&At(x),len);	}

	DYNTYPE_FUNC const Ref GetRef() const
	{	return ::rt::_CastToNonconst(this)->GetRef(); }
	DYNTYPE_FUNC const Ref GetRef(UINT x,UINT len) const
	{	return ::rt::_CastToNonconst(this)->GetRef(x,len); }

	DYNTYPE_FUNC operator Ref& ()
	{ return *((Ref*)this); }
	DYNTYPE_FUNC operator const Ref& () const 
	{ return ::rt::_CastToNonconst(this)->operator Ref& (); }

public:
	TYPETRAITS_DECL_EXTENDTYPEID((rt::_typeid_codelib<<16)+2)
	COMMON_CONSTRUCTOR_VEC(Buffer)
#pragma warning(disable:4244)
	ASSIGN_OPERATOR_VEC(Buffer)
#pragma warning(default:4244)
	DEFAULT_TYPENAME(Buffer)
};

template<typename t_Val>
class BufferEx:public Buffer<t_Val,rt::_BaseVecDynEx<SIZE_T>>
{
public:
	TYPETRAITS_DECL_EXTENDTYPEID((rt::_typeid_codelib<<16)+3)
	COMMON_CONSTRUCTOR_VEC(BufferEx)
#pragma warning(disable:4244)
	ASSIGN_OPERATOR_VEC(BufferEx)
#pragma warning(default:4244)
	DEFAULT_TYPENAME(BufferEx)
};


//////////////////////////////////////////////////////////////////////
//Memory Stream

class MemoryFile: public rt::_stream 
{
protected:
	rt::BufferEx<BYTE>	m_Buffer;
	UINT				m_Pos;

public:
	MemoryFile(){ m_Pos = 0; }
	virtual UINT		Read(LPVOID pBuf, UINT co)
	{	ASSERT(m_Pos <= m_Buffer.GetSize());
		co = (UINT)__min(co,m_Buffer.GetSize() - m_Pos);
		memcpy(pBuf,&m_Buffer[m_Pos],co);
		m_Pos += co;
		return co;
	}
	virtual UINT		Write(LPCVOID pBuf, UINT co)
	{	m_Buffer.ChangeSize(__max(m_Buffer.GetSize(),m_Pos+co));
		memcpy(&m_Buffer[m_Pos],pBuf,co);
		m_Pos += co;
		return co;
	}
	virtual LONGLONG	Seek(LONGLONG offset, UINT nFrom = rt::_stream::Seek_Begin)
	{	int p;
		if(nFrom == rt::_stream::Seek_Begin){	p = (int)offset; }
		else if(nFrom == rt::_stream::Seek_Current){ p = m_Pos+(int)offset; }
		else if(nFrom == rt::_stream::Seek_End){ p = ((int)m_Buffer.GetSize()) + (int)offset; }
		else { ASSERT(0); }

		if(p>=0 && p<=(int)m_Buffer.GetSize())m_Pos = p;
		return m_Pos;
	}

	virtual BOOL		SetLength(ULONGLONG sz){ m_Pos = 0; return m_Buffer.SetSize((UINT)sz); }
	virtual LONGLONG	GetLength() const { return m_Buffer.GetSize(); }
	LPBYTE				GetInteralBuffer(){ return m_Buffer; }
	LPCBYTE				GetInteralBuffer() const { return m_Buffer; }
};



//////////////////////////////////////////////////////////////////////
//Vector reference type for all linear and compact vectors
namespace _meta_
{
template<typename t_Val, typename SIZETYPE>
class _BaseVecRefRoot
{
protected:
	DYNTYPE_FUNC _BaseVecRefRoot(const _BaseVecRefRoot &x){ ASSERT_STATIC(0); }
	DYNTYPE_FUNC _BaseVecRefRoot(){}
public:
	DYNTYPE_FUNC BOOL SetSize(SIZETYPE co=0){ return (co==_len)?TRUE:FALSE; }
	DYNTYPE_FUNC BOOL ChangeSize(SIZETYPE new_size){ if(_len >= new_size){ _len=new_size; return TRUE; } return FALSE; }
	static const bool IsRef = true;
	typedef SIZETYPE t_SizeType;

	//TypeTraits
	TYPETRAITS_DECL_LENGTH(TYPETRAITS_SIZE_UNKNOWN)	
	TYPETRAITS_DECL_IS_AGGREGATE(false)

	template<typename T>
	DYNTYPE_FUNC t_Val & At(const T index){ return _p[(int)index]; }
	template<typename T>
	DYNTYPE_FUNC const t_Val & At(const T index)const { return _p[(int)index]; }
	DYNTYPE_FUNC SIZETYPE GetSize() const{ return _len; }
protected:
	t_Val*	_p;
	SIZETYPE	_len;
};
}// namespace _meta_
template<typename SIZETYPE>
class _BaseVecRef
{public:	template<typename t_Ele> class SpecifyItemType
			{public: typedef rt::_meta_::_BaseVecRefRoot<t_Ele, SIZETYPE> t_BaseVec; };
			static const bool IsCompactLinear = true;
};


/////////////////////////////////////////////////////////////
// Class for linear and compact vector reference/argument
template< typename t_Val >
class Buffer_Ref:public rt::Buffer<t_Val,rt::_BaseVecRef<SIZE_T>>
{
	template< typename t_Val, class t_BaseVec >
	friend class Buffer;
protected:
	Buffer_Ref(){ ASSERT_STATIC(0); }
public:
	DYNTYPE_FUNC Buffer_Ref(t_Val* ptr,SIZETYPE len){ _p=ptr; _len=len; }
	Buffer_Ref(const Buffer_Ref& x){ ASSERT_STATIC(0); }	//The two ASSERTs fired when trying to construct 
	//a Ref from a Ref instance or an empty instance which is not allowed. Avoid trying to copy a Ref
	//instance, it is highly recommended to use Ref& instead  for local using and function arguments
	template<class BaseVec>
	DYNTYPE_FUNC Buffer_Ref(rt::_TypedVector<t_Val,BaseVec>& x):_BaseVecRefRoot(x.Begin(),x.GetSize()){}

#ifdef __INTEL_COMPILER
#pragma warning(disable: 525) //warning #525: type "xxx" is an inaccessible type (allowed for compatibility)
#endif
	typedef __super::Ref Ref;
#ifdef __INTEL_COMPILER
#pragma warning(default: 525)
#endif
	
public:
#pragma warning(disable:4244)
	ASSIGN_OPERATOR_VEC(Buffer_Ref)
#pragma warning(default:4244)
	DEFAULT_TYPENAME(Buffer_Ref)
};


template<typename t_Val>
class Buffer_32BIT:public Buffer<t_Val,rt::_BaseVecDyn<UINT>>
{
public:
	TYPETRAITS_DECL_EXTENDTYPEID((rt::_typeid_codelib<<16)+2)
	COMMON_CONSTRUCTOR_VEC(Buffer_32BIT)
#pragma warning(disable:4244)
	ASSIGN_OPERATOR_VEC(Buffer_32BIT)
#pragma warning(default:4244)
	DEFAULT_TYPENAME(Buffer_32BIT)
};

template<typename t_Val>
class BufferEx_32BIT:public Buffer<t_Val,rt::_BaseVecDynEx<UINT>>
{
public:
	TYPETRAITS_DECL_EXTENDTYPEID((rt::_typeid_codelib<<16)+3)
	COMMON_CONSTRUCTOR_VEC(BufferEx_32BIT)
#pragma warning(disable:4244)
	ASSIGN_OPERATOR_VEC(BufferEx_32BIT)
#pragma warning(default:4244)
	DEFAULT_TYPENAME(BufferEx_32BIT)
};


template< typename t_Val >
class Buffer_Ref_32BIT:public rt::Buffer<t_Val,rt::_BaseVecRef<UINT>>
{
	template< typename t_Val, class t_BaseVec >
	friend class Buffer;
protected:
	Buffer_Ref_32BIT(){ ASSERT_STATIC(0); }
public:
	DYNTYPE_FUNC Buffer_Ref_32BIT(t_Val* ptr,SIZETYPE len){ _p=ptr; _len=len; }
	Buffer_Ref_32BIT(const Buffer_Ref_32BIT& x){ ASSERT_STATIC(0); }	//The two ASSERTs fired when trying to construct 
	//a Ref from a Ref instance or an empty instance which is not allowed. Avoid trying to copy a Ref
	//instance, it is highly recommended to use Ref& instead  for local using and function arguments
	template<class BaseVec>
	DYNTYPE_FUNC Buffer_Ref_32BIT(rt::_TypedVector<t_Val,BaseVec>& x):_BaseVecRefRoot(x.Begin(),x.GetSize()){}

#ifdef __INTEL_COMPILER
#pragma warning(disable: 525) //warning #525: type "xxx" is an inaccessible type (allowed for compatibility)
#endif
	typedef __super::Ref Ref;
#ifdef __INTEL_COMPILER
#pragma warning(default: 525)
#endif
	
public:
#pragma warning(disable:4244)
	ASSIGN_OPERATOR_VEC(Buffer_Ref_32BIT)
#pragma warning(default:4244)
	DEFAULT_TYPENAME(Buffer_Ref_32BIT)
};
} // namespace rt