#pragma once

//////////////////////////////////////////////////////////////////////
// Visual Computing Foundation Classes (VCFC)
//					Jiaping Wang  2004.3
//					e_boris2002@hotmail.com
//
// (C) Copyright Jiaping Wang 2004.
//
// Permission to copy, use, modify, sell and distribute this software
// is granted provided this copyright notice appears in all copies.
// This software is provided "as is" without express or implied
// warranty, and with no claim as to its suitability for any purpose.
//
// Absolutely no warranty!!
//////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////
//  Brief Description
//
//  vec_type.h
//
//  small vector with loop unroll
//
//////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////
// Revise History
//
// Inital version	2006.6.7		Jiaping
//
//////////////////////////////////////////////////////////////////////

#include "vector_type.h"

namespace rt
{	


//////////////////////////////////////////////////////////////////////
//Fixed length buffer container 
namespace _meta_
{
#pragma pack(1)
	template<typename t_Val,SIZE_T LEN >
	class _BaseVecFixStorage
	{public:	union
				{	struct{ t_Val _p[LEN];	};
	};			};
	//specialization: size==1/2/3/4
	template<typename t_Val>
	class _BaseVecFixStorage<t_Val,1>
	{public:	union
				{	struct{ t_Val _p[1];	};
					struct{ t_Val x;		};
	};			};
	template<typename t_Val>
	class _BaseVecFixStorage<t_Val,2>
	{public:	union
				{	struct{ t_Val _p[2];	};
					struct{ t_Val x, y;		};
					struct{ t_Val theta,phi;};	//spherical direction 
					struct{ t_Val real,imag;};	//complex number
	};			};
	template<typename t_Val>
	class _BaseVecFixStorage<t_Val,3>
	{public:	union
				{	struct{ t_Val _p[3]; };
					struct{ t_Val x,y,z; };
					struct{ t_Val r,g,b; };	//triple color
	};			};
	template<typename t_Val>
	class _BaseVecFixStorage<t_Val,4>
	{public:	union
				{	struct{ t_Val _p[4]; };
					struct{ t_Val x,y,z,w; };
					struct{ t_Val r,g,b,a; }; //quadruple color
	};			};

	//BaseVectorRoot
	template<typename t_Val,UINT LEN >
	class _BaseVecFixRoot
		:public _BaseVecFixStorage<t_Val,LEN>
	{public: typedef t_Val t_Ele;
		DYNTYPE_FUNC BOOL SetSize(SIZE_T co = 0){ if(co==_len){return TRUE;} ASSERT(0); return FALSE; }
		DYNTYPE_FUNC BOOL ChangeSize(SIZE_T co = 0){ return SetSize(co); }
		//TypeTraits
		TYPETRAITS_DECL_LENGTH(LEN)	
		TYPETRAITS_FOLLOW_IS_AGGREGATE(t_Val)
		typedef UINT t_SizeType;

		template<typename T>
		DYNTYPE_FUNC t_Val & At(const T index){ return _p[(int)index]; }
		template<typename T>
		DYNTYPE_FUNC const t_Val & At(const T index)const { return _p[(int)index]; }
		DYNTYPE_FUNC SIZE_T GetSize() const{ return _len; }
	protected:	static const SIZE_T _len = LEN;
	};
#pragma pack()
} // namespace _meta_
template<SIZE_T Len>
class _BaseVecFix
{public:	template<typename t_Ele> class SpecifyItemType
			{public: typedef rt::_meta_::_BaseVecFixRoot<t_Ele,Len> t_BaseVec; };
			static const bool IsCompactLinear = true;
};
//////////////////////////////////////////////////////////////////////




#pragma pack(1)

#define COMMON_FIXVEC_CONSTRUCTOR(clsname)	clsname(){}																\
											DYNTYPE_FUNC clsname(const clsname& x)									\
											{ SetSizeAs(x); *this=x; }												\
											template<typename T,class BaseVec>										\
											DYNTYPE_FUNC clsname(const rt::_TypedVector<T,BaseVec>& x)				\
											{ SetSizeAs(x); *this=x; }												\
											__static_if(!IsItemTypePlanar){											\
											explicit DYNTYPE_FUNC clsname(const FundamentalItemType & x)			\
											{	*this = x; } 														\
											}																		\
											explicit DYNTYPE_FUNC clsname(const ItemType & x)						\
											{ *this = x; }															\


//////////////////////////////////////////////////////////////////////
// class for small num vectors
template< typename t_Val, SIZE_T LEN, class BaseVec = rt::_BaseVecFix<LEN> >
class _TypedFixVec:public rt::_TypedVector<t_Val,BaseVec >
{	
protected:
	typedef rt::NumericTraits<t_Val>	NumericTraits_Item;

public:
	TYPETRAITS_DECL_EXTENDTYPEID((rt::_typeid_codelib<<16)+4)
#pragma warning(disable:4244)
	ASSIGN_OPERATOR_VEC(_TypedFixVec)
#pragma warning(default:4244)
	template<class ostream> static void __TypeName(ostream & outstr)
	{ outstr<<"rt::_TypedFixVec<"; TypeTraits_Item::TypeName(outstr); outstr<<','<<LEN<<'>'; }

public:
	template<typename t_Ele2> class SpecifyItemType
	{public: typedef rt::_TypedFixVec<t_Ele2,LEN> t_Result; };

	TYPETRAITS_DECL_ACCUM_TYPE(typename SpecifyItemType< typename rt::TypeTraits<t_Val>::t_Accum >::t_Result)	
	TYPETRAITS_DECL_VALUE_TYPE(typename SpecifyItemType< typename rt::TypeTraits<t_Val>::t_Val >::t_Result)
	TYPETRAITS_DECL_SIGNED_TYPE(typename SpecifyItemType<typename rt::TypeTraits<t_Val>::t_Signed>::t_Result)
	TYPETRAITS_DECL_IS_NUMERIC(false)

public:
	DYNTYPE_FUNC SIZE_T GetSize()const { return LEN; }
	DYNTYPE_FUNC BOOL SetSize(SIZE_T sz){ return sz==LEN; }

	__static_if(LEN==1){
	DYNTYPE_FUNC operator t_Val ()const{ return At(0); }
	}

	template<SIZE_T LEN2>
	DYNTYPE_FUNC operator rt::_TypedFixVec<t_Val,LEN2>&()
	{ return *((rt::_TypedFixVec<t_Val,LEN2>*)this); }
	template<SIZE_T LEN2>
	DYNTYPE_FUNC operator const rt::_TypedFixVec<t_Val,LEN2>&() const
	{ return *((rt::_TypedFixVec<t_Val,LEN2>*)this); }

public:
	//operators
#define ASSIGN_OP(op)	DYNTYPE_FUNC void operator op (FundamentalItemType x)				\
						{	__static_if(LEN>1)												\
							{ ((rt::_TypedFixVec<t_Val,LEN-1,BaseVec>&)*this) op x; }		\
							At(LEN-1) op x;													\
						}																	\
						template<typename T, class BV>										\
						DYNTYPE_FUNC void operator op (const rt::_TypedVector<T,BV> & x)	\
						{	__static_if(LEN>1)												\
							{ ((rt::_TypedFixVec<t_Val,LEN-1,BaseVec>&)*this) op x; }		\
							At(LEN-1) op x.At(LEN-1);										\
						}																	\
						template<typename T>												\
						DYNTYPE_FUNC void operator op (const T* x)							\
						{	__static_if(LEN>1)												\
							{ ((rt::_TypedFixVec<t_Val,LEN-1,BaseVec>&)*this) op x; }		\
							At(LEN-1) op x[LEN-1];											\
						}																	\
						//template<typename T>												\
						//DYNTYPE_FUNC void operator op (T* x)								\
						//{	__static_if(LEN>1)												\
						//	{ ((rt::_TypedFixVec<t_Val,LEN-1,BaseVec>&)*this) op x; }		\
						//	At(LEN-1) op x[LEN-1];											\
						//}																	\

		#pragma warning(disable:4244)
		ASSIGN_OP(+=)
		ASSIGN_OP(-=)
		ASSIGN_OP(*=)
		ASSIGN_OP(/=)
		ASSIGN_OP(%=)
		ASSIGN_OP(<<=)
		ASSIGN_OP(>>=)
		ASSIGN_OP(&=)
		ASSIGN_OP(|=)
		ASSIGN_OP(^=)
		#pragma warning(default:4244)
#undef  ASSIGN_OP


	// 3.1.2 Prefix
#define UNARY_OP_Prefix(op)	DYNTYPE_FUNC const _TypedFixVec& operator op ()					\
							{	__static_if(LEN>1)											\
								{ op((rt::_TypedFixVec<t_Val,LEN-1,BaseVec>&)*this); }		\
								op(At(LEN-1));												\
								return *this; }
		UNARY_OP_Prefix(++)
		UNARY_OP_Prefix(--)
#undef  UNARY_OP_Prefix

	// 3.1.3 Postfix, functions same as Prefix
#define UNARY_OP_Postfix(op) DYNTYPE_FUNC const _TypedFixVec& operator op (int)				\
							 {	__static_if(LEN>1)											\
								{ ((rt::_TypedFixVec<t_Val,LEN-1,BaseVec>&)*this)op; }		\
								(At(LEN-1))op;												\
								return *this; }
		UNARY_OP_Postfix(++)
		UNARY_OP_Postfix(--)
#undef UNARY_OP_Postfix

	////////////////////////////////////////////////////////////////////////////////////////
	// 3.2 Set all elements with machine zero
	DYNTYPE_FUNC void Zero()
	{	__static_if(LEN>1){ ((rt::_TypedFixVec<t_Val,LEN-1,BaseVec>&)*this).Zero(); } 
		At(LEN-1)=0; 
	}

	////////////////////////////////////////////////////////////////////////////////////////
	// 3.3 Set all elements with given value
	DYNTYPE_FUNC void SetItem(const ItemType & x)
	{	__static_if(LEN>1){ ((rt::_TypedFixVec<t_Val,LEN-1,BaseVec>&)*this).SetItem(x); }
		At(LEN-1)=x; 
	}

	DYNTYPE_FUNC void Set(const FundamentalItemType & x)					
	{	__if_not_exists(ItemType::Set){ SetItem(x); }
		__if_exists(ItemType::Set)
		{	__static_if(LEN>1){ ((rt::_TypedFixVec<t_Val,LEN-1,BaseVec>&)*this).Set(x); }
			At(LEN-1).Set(x);
		}
	}																		

	////////////////////////////////////////////////////////////////////////////////////////
	// 3.8 Dot product/Sum/L2Norm_Sqr
	// 3.8.1 Dot product
	template< typename T, class BV>
	DYNTYPE_FUNC _ItemValueType Dot(const _TypedVector<T,BV> & y ) const
	{	USING_EXPRESSION;
		ASSERT_STATIC(::rt::TypeTraits<_ItemValueType>::IsAggregate);
		_ItemValueType value;
	
		__static_if(LEN>1)
		{	value = ((const rt::_TypedFixVec<t_Val,LEN-1,BaseVec>&)(*this)).Dot(y) + At(LEN-1)*y.At(LEN-1);
		}
		__static_if(LEN==1)
		{	value = At(0)*y[0]; }
		return value;
	}

	// 3.8.2 Sum
	DYNTYPE_FUNC _ItemValueType Sum() const
	{	USING_EXPRESSION;
		ASSERT_STATIC(::rt::TypeTraits<_ItemValueType>::IsAggregate);
		_ItemValueType value;
		
		__static_if(LEN>1)
		{ value = ((const rt::_TypedFixVec<t_Val,LEN-1,BaseVec>&)(*this)).Sum() + At(LEN-1); }
		__static_if(LEN==1)
		{ value = At(0); }

		return value;
	}

	// 3.8.3 L2Norm_Sqr
	DYNTYPE_FUNC _ItemValueType L2Norm_Sqr() const
	{	USING_EXPRESSION;
		ASSERT_STATIC(::rt::TypeTraits<_ItemValueType>::IsAggregate);
		_ItemValueType value;
		
		__static_if(LEN>1)
		{ value =  ((const rt::_TypedFixVec<t_Val,LEN-1,BaseVec>&)(*this)).L2Norm_Sqr() + Sqr(At(LEN-1)); }
		__static_if(LEN==1)
		{ value = Sqr(At(LEN-1)); }

		return value;
	}

	// 3.8.4 L2Distance_Sqr
	template< typename T, class BV >
	DYNTYPE_FUNC _ItemValueType Distance_Sqr(const _TypedVector<T,BV> & y) const
	{	USING_EXPRESSION;
		ASSERT_STATIC(::rt::TypeTraits<_ItemValueType>::IsAggregate);
		_ItemValueType value;
		
		__static_if(LEN>1)
		{ value = ((const rt::_TypedFixVec<t_Val,LEN-1,BaseVec>&)(*this)).Distance_Sqr(y) + Sqr(At(LEN-1)-y[LEN-1]); }
		__static_if(LEN==1)
		{ value = Sqr(At(0)-y[0]); }

		return value;
	}

    // 3.8.5 Product
    DYNTYPE_FUNC _ItemValueType Product(void) const
    {	USING_EXPRESSION;
		ASSERT_STATIC(::rt::TypeTraits<_ItemValueType>::IsAggregate);
		_ItemValueType value;

        __static_if(LEN>1) {
			value = ((const rt::_TypedFixVec<t_Val,LEN-1,BaseVec>&)(*this)).Product() * At(LEN-1);
        }
        __static_if(LEN==1) {
            value = At(0);
        }
		return value;
    }

	////////////////////////////////////////////////////////////////////////////////////////
	// 3.10 Randomize elements
	DYNTYPE_FUNC void Random()
	{	__static_if(LEN>1){ ((rt::_TypedFixVec<t_Val,LEN-1,BaseVec>&)*this).Random(); }
		rt::_meta_::_Random(At(LEN-1)); 
	}

	////////////////////////////////////////////////////////////////////////////////////////
	// 4.1 Assignment
	template< class t_ItemType2 >
	DYNTYPE_FUNC void CopyTo( t_ItemType2 * p) const
	{	__static_if(LEN>1){ ((const rt::_TypedFixVec<t_Val,LEN-1,BaseVec>&)*this).CopyTo(p); }
#pragma warning(disable:4244)
		p[LEN-1] = At(LEN-1); 
	}

	template< class t_ItemType2 >
	DYNTYPE_FUNC void CopyFrom(const t_ItemType2 * p)
	{	__static_if(LEN>1){ ((rt::_TypedFixVec<t_Val,LEN-1,BaseVec>&)*this).CopyFrom(p); }
#pragma warning(disable:4244)
		At(LEN-1) = p[LEN-1]; 
#pragma warning(default:4244)
	}

	template<typename T, typename BV>
	DYNTYPE_FUNC void CopyFrom(const rt::_TypedVector<T,BV> & x)
	{	__static_if(LEN>1){ ((rt::_TypedFixVec<t_Val,LEN-1,BaseVec>&)(*this)).CopyFrom(x); }
#pragma warning(disable:4244)
		At(LEN-1)=x.At(LEN-1);
#pragma warning(default:4244)
	}

	////////////////////////////////////////////////////////////////////////////////////////
	// 4.1 Inverse element order
	DYNTYPE_FUNC void Flip()
	{	__static_if(LEN>1 && LEN<100)
		{ Swap(At(0),At(LEN-1)); ((rt::_TypedFixVec<t_Val,LEN-2,BaseVec>&)At(1)).Flip(); }
	}

	////////////////////////////////////////////////////////////////////////////////////////
	// 4.2 Randomize element order
	DYNTYPE_FUNC void Shuffle()
	{	__static_if(LEN>1)
		{ ((_TypedFixVec<t_Val,LEN-1,BaseVec>&)*this).Shuffle(); Swap(At(LEN-1),At(::rand()%LEN)); }	
	}


	////////////////////////////////////////////////////////////////////////////////////////
	// Normalize
	DYNTYPE_FUNC void Normalize()
	{	_ItemValueType L2=L2Norm_Sqr();
		typedef rt::IsTypeSame<FundamentalItemType,double> is_double;

		__static_if(!is_double::Result)
		{ if(L2>TypeTraits_Item::Epsilon())(*this) *= 1.0f/(float)::sqrt((double)L2); }
		__static_if(is_double::Result)
		{ if(L2>TypeTraits_Item::Epsilon())(*this) *= 1.0/::sqrt((double)L2); }
	}

	template< typename t_Val2, class BV >
	DYNTYPE_FUNC void NormalizeTo(_TypedVector<t_Val2,BV> & x) const
	{	USING_EXPRESSION;
		
		_ItemValueType L2=L2Norm_Sqr();
		typedef rt::IsTypeSame<_TypedVector<t_Val2,BV>::FundamentalItemType,double> is_double;

		__static_if(!is_double::Result)
		{	if(L2>TypeTraits_Item::Epsilon())x = (*this)*1.0f/(float)sqrt((double)L2); }
		__static_if(is_double::Result)
		{	if(L2>TypeTraits_Item::Epsilon())x = (*this)*1.0/sqrt((double)L2); }
	}


	////////////////////////////////////////////////////////////////////////////////////////
	// Max/Min element
	DYNTYPE_FUNC _ItemValueType Min() const
	{	USING_EXPRESSION;
		ASSERT_STATIC(::rt::TypeTraits<_ItemValueType>::IsAggregate);
		_ItemValueType value;
			
		__static_if(LEN>1)
		{	_ItemValueType m = ((rt::_TypedFixVec<t_Val,LEN-1,BaseVec>*)this)->Min(); 
			_ItemValueType cur = At(LEN-1);
			value = cur>=m?m:cur;
		}
		__static_if(LEN==1)
		{	value = At(0); }

		return value;
	}

	DYNTYPE_FUNC _ItemValueType Max() const
	{	USING_EXPRESSION;
		ASSERT_STATIC(::rt::TypeTraits<_ItemValueType>::IsAggregate);
		_ItemValueType value;
		
		__static_if(LEN>1)
		{	_ItemValueType m = ((rt::_TypedFixVec<t_Val,LEN-1,BaseVec>*)this)->Max(); 
			_ItemValueType cur = At(LEN-1);
			value = cur<=m?m:cur;
		}
		__static_if(LEN==1)
		{	value = At(0); }

		return value;
	}

	////////////////////////////////////////////////////////////////////////////////////////
	// element-wise Max/Min 
	template<typename t_Val1,typename t_Val2,class BV1,class BV2>
	DYNTYPE_FUNC void Max(const _TypedVector<t_Val1,BV1>& v1,const _TypedVector<t_Val2,BV2>& v2)
	{	USING_EXPRESSION;
		__static_if(LEN>1)
		{ ((rt::_TypedFixVec<t_Val,LEN-1,BaseVec>&)*this).Max(v1,v2); }

		At(LEN-1) = max(v1.At(LEN-1),v2.At(LEN-1));
	}
	template<typename t_Val1,typename t_Val2,class BV1,class BV2>
	DYNTYPE_FUNC void Min(const _TypedVector<t_Val1,BV1>& v1,const _TypedVector<t_Val2,BV2>& v2)
	{	USING_EXPRESSION;
		__static_if(LEN>1)
		{ ((rt::_TypedFixVec<t_Val,LEN-1,BaseVec>&)*this).Min(v1,v2); }

		At(LEN-1) = min(v1.At(LEN-1),v2.At(LEN-1));
	}

	////////////////////////////////////////////////////////////////////////////////////////
	// clamp 
	DYNTYPE_FUNC void Saturate(	FundamentalItemType ClampMin = (FundamentalItemType)0, 
								FundamentalItemType ClampMax = (FundamentalItemType)1 )
	{	__static_if(LEN>1)
		{ ((rt::_TypedFixVec<t_Val,LEN-1,BaseVec>&)*this).Saturate(ClampMin,ClampMax); }

		__if_exists(ItemType::Saturate)
		{ At(LEN-1).Saturate(ClampMin,ClampMax); }
		__if_not_exists(ItemType::Saturate)
		{ At(LEN-1) = min(ClampMax,max(ClampMin,At(LEN-1))); }
	}

	////////////////////////////////////////////////////////////////////////////////////////
	// equal 
	template<typename t_Val2, class BV>
	DYNTYPE_FUNC BOOL operator == (const rt::_TypedVector<t_Val2,BV> & in) const
	{	USING_EXPRESSION;
		__static_if(LEN>1)
		{	BOOL ise = ((rt::_TypedFixVec<t_Val,LEN-1,BaseVec>&)*this)==in; }
		
		typedef rt::NumericTraits<ItemType> NumericTraits_Item;
		__static_if(NumericTraits_Item::IsFloat)
		{	BOOL ise_cur = rt::IsInRange_CC(At(LEN-1)-in.At(LEN-1),-TypeTraits_Item::Epsilon(),TypeTraits_Item::Epsilon()); }
		__static_if(!NumericTraits_Item::IsFloat)
		{	BOOL ise_cur = (At(LEN-1)==in.At(LEN-1)); }
		
		__static_if(LEN>1) { return ise && ise_cur; }
		__static_if(LEN==1){ return ise_cur; }
	}
	template<typename t_Val2, class BV>
	DYNTYPE_FUNC BOOL operator != (const rt::_TypedVector<t_Val2,BV> & in) const 
	{	return !((*this)==in);	}

};

}	// namespace rt



namespace rt
{

template< typename t_Val, unsigned int LEN >
class Vec:public _TypedFixVec<t_Val,LEN>
{
public:
	COMMON_FIXVEC_CONSTRUCTOR(Vec)
	template<class ostream> static void __TypeName(ostream & outstr)
	{ outstr<<"rt::Vec<"; TypeTraits_Item::TypeName(outstr); outstr<<','<<LEN<<'>'; }
#pragma warning(disable:4244)
	ASSIGN_OPERATOR_VEC(_TypedFixVec)
#pragma warning(default:4244)

public:
	t_Val* data(){ return _p; }
	const t_Val* data() const{ return _p; }

	template<int ret_len>
	Vec<t_Val,ret_len>& GetRef(UINT index)
	{ ASSERT(ret_len+index<=LEN); return *((Vec<t_Val,ret_len>*)&_p[index]); }
	template<int ret_len>
	const Vec<t_Val,ret_len>& GetRef(UINT index) const
	{ return rt::_CastToNonconst(this)->GetRef<ret_len>(index); }

	////////////////////////////////////////////////////////////////////////////////////////
	// BilinearInterpolation
	//	| a | a'|
	//	| b | b'|     
	template< typename T1,typename T2,SIZE_T sz1,SIZE_T sz2 >
	DYNTYPE_FUNC void BilinearInterpolate(const rt::Vec<T1,sz1>& v1,const rt::Vec<T2,sz2>& v2,float u,float v)
	{	USING_EXPRESSION;
		ASSERT_STATIC(rt::TypeTraits<T1>::IsAggregate);
		ASSERT_STATIC(rt::TypeTraits<T2>::IsAggregate);
		__static_if(LEN>1)
		{ ((rt::Vec<t_Val,LEN-1>&)*this).BilinearInterpolate(v1,v2,u,v); }

		At(LEN-1) =	(t_Val)(  (v1.At(LEN-1)*(1.0f-u) + v1.At(LEN+sz1-1)*u)*(1.0f-v)
							 +(v2.At(LEN-1)*(1.0f-u) + v2.At(LEN+sz2-1)*u)*v );
	}
	template< typename T1,typename T2,SIZE_T sz1,SIZE_T sz2 >
	DYNTYPE_FUNC void BilinearInterpolate(const rt::Vec<T1,sz1>& v1,const rt::Vec<T2,sz2>& v2)
	{	USING_EXPRESSION;
		ASSERT_STATIC(rt::TypeTraits<T1>::IsAggregate);
		ASSERT_STATIC(rt::TypeTraits<T2>::IsAggregate);
		__static_if(LEN>1)
		{ ((rt::Vec<t_Val,LEN-1>&)*this).BilinearInterpolate(v1,v2); }

		__static_if(::rt::NumericTraits<T1>::IsInteger && ::rt::NumericTraits<T2>::IsInteger)
		{
			At(LEN-1) =	(t_Val)(( (v1.At(LEN-1) + v1.At(LEN+sz1-1))
								 +(v2.At(LEN-1) + v2.At(LEN+sz2-1)) )>>2);
		}
		__static_if(!(::rt::NumericTraits<T1>::IsInteger && ::rt::NumericTraits<T2>::IsInteger))
		{
			At(LEN-1) =	(t_Val)(( (v1.At(LEN-1) + v1.At(LEN+sz1-1))
								 +(v2.At(LEN-1) + v2.At(LEN+sz2-1)) )/4);
		}
	}


};


#pragma pack()
};


