#pragma once

//////////////////////////////////////////////////////////////////////
// Visual Computing Foundation Classes (VCFC)
//					Jiaping Wang  2006.2
//					e_boris2002@hotmail.com
//
// (C) Copyright Jiaping Wang 2004.
//
// Permission to copy, use, modify, sell and distribute this software
// is granted provided this copyright notice appears in all copies.
// This software is provided "as is" without express or implied
// warranty, and with no claim as to its suitability for any purpose.
//
// Absolutely no warranty!!
//////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////
//  Brief Description
//
//  string_type.h
//
//  string class based on Buffer
//
//
//////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////
// Revise History
//
// Inital version	2006.10.7		Jiaping
//
//////////////////////////////////////////////////////////////////////
#include "runtime_base.h"
#include "type_traits.h"

#include <atlbase.h>
#include <atlconv.h>

#ifndef CP_UTF16
	#define CP_UTF16	66000
#endif

#ifndef DYNTYPE_FUNC
	#define DYNTYPE_FUNC __forceinline
#endif

namespace rt
{

namespace _meta_
{
	static DYNTYPE_FUNC int		xxxcmp(LPCSTR p1,LPCSTR p2){ return strcmp(p1,p2); }
	static DYNTYPE_FUNC int		xxxcmp(LPCWSTR p1,LPCWSTR p2){ return wcscmp(p1,p2); }
	static DYNTYPE_FUNC size_t	xxxlen(LPCSTR p1){ return strlen(p1); }
	static DYNTYPE_FUNC size_t	xxxlen(LPCWSTR p1){ return wcslen(p1); }
	static DYNTYPE_FUNC LPSTR	xxxcpy(LPSTR p1,LPCSTR p2){ return strcpy(p1,p2); }
	static DYNTYPE_FUNC LPWSTR	xxxcpy(LPWSTR p1,LPCWSTR p2){ return wcscpy(p1,p2); }
	static DYNTYPE_FUNC LPSTR	xxxncpy(LPSTR p1,LPCSTR p2,size_t n){ return strncpy(p1,p2,n); }
	static DYNTYPE_FUNC LPWSTR	xxxncpy(LPWSTR p1,LPCWSTR p2,size_t n){ return wcsncpy(p1,p2,n); }
	static DYNTYPE_FUNC LPSTR	xxxupr(LPSTR p){ return _strupr(p); }
	static DYNTYPE_FUNC LPWSTR	xxxupr(LPWSTR p){ return _wcsupr(p); }
	static DYNTYPE_FUNC LPSTR	xxxlwr(LPSTR p){ return _strlwr(p); }
	static DYNTYPE_FUNC LPWSTR	xxxlwr(LPWSTR p){ return _wcslwr(p); }
	static DYNTYPE_FUNC LPCSTR	xxxstr(LPCSTR p, LPCSTR sub){ return strstr(p,sub); }
	static DYNTYPE_FUNC LPCWSTR	xxxstr(LPCWSTR p, LPCWSTR sub){ return wcsstr(p,sub); }
	static DYNTYPE_FUNC LPCSTR	xxxchr(LPCSTR p, CHAR sub){ return strchr(p,sub); }
	static DYNTYPE_FUNC LPCWSTR	xxxchr(LPCWSTR p, WCHAR sub){ return wcschr(p,sub); }
	static DYNTYPE_FUNC LPCSTR	xxxrchr(LPCSTR p, CHAR sub){ return strrchr(p,sub); }
	static DYNTYPE_FUNC LPCWSTR	xxxrchr(LPCWSTR p, WCHAR sub){ return wcsrchr(p,sub); }
	static DYNTYPE_FUNC void	itox(int v, LPSTR p, int radix){ itoa(v,p,radix); }
	static DYNTYPE_FUNC void	itox(int v, LPWSTR p, int radix){ _itow(v,p,radix); }
	static DYNTYPE_FUNC void	_i64tox(__int64 v, LPSTR p, int radix){ _i64toa(v,p,radix); }
	static DYNTYPE_FUNC void	_i64tox(__int64 v, LPWSTR p, int radix){ _i64tow(v,p,radix); }
	static DYNTYPE_FUNC void	_ui64tox(ULONGLONG v, LPSTR p, int radix){ _ui64toa(v,p,radix); }
	static DYNTYPE_FUNC void	_ui64tox(ULONGLONG v, LPWSTR p, int radix){ _ui64tow(v,p,radix); }

	template<typename t_Val, typename t_Left, typename t_Right>
	class StringT_AddExpr;

	//// support stl's hash_map, stdext::hash_map<rt::StringA, int, rt::StringA::hash_compare>
	template<typename T>
	struct hash_compare
	{	// traits class for hash containers
		enum // parameters for hash table
		{	bucket_size = 4,	// 0 < bucket_size
			min_buckets = 8		// min_buckets = 2 ^^ N, 0 < N
		};
		size_t operator()(const T& key) const
		{	return stdext::_Hash_value(key.Begin(),key.End());	}
		bool operator()(const T& _Keyval1, const T& _Keyval2) const
		{	return _Keyval1 < _Keyval2;		}
		//struct _comp
		//{	// functor for operator==
		//	typedef const T&	first_argument_type;
		//	typedef const T&	second_argument_type;
		//	typedef bool		result_type;
		//	bool operator()(const T& _Left, const T& _Right) const
		//	{	return _Left < _Right;	}
		//};
		//_comp comp;
	};
}	

template<typename t_Val>
class StringT_Ref
{	
	TYPETRAITS_DECL_ELEMENT_TYPE(t_Val)
	TYPETRAITS_DECL_LENGTH(TYPETRAITS_SIZE_UNKNOWN)	
	TYPETRAITS_DECL_IS_AGGREGATE(true)
	TYPETRAITS_DECL_IS_ARRAY(true)

	template<typename t_Val, typename t_Left, typename t_Right>
	friend class _meta_::StringT_AddExpr;
	template<typename t_Val, class storage>
	friend class StringT;

	DYNTYPE_FUNC SIZE_T _ExprFill(t_Val* p) const	// return number of char filled
	{	if(!IsEmpty())
		{	memcpy(p,_p,GetLength()*sizeof(t_Val));
			return GetLength();
		}
		else return 0;
	}

protected:
	t_Val*	_p;
	UINT	_len;

public:
	typedef t_Val t_Char;
	typedef _meta_::hash_compare<StringT_Ref<t_Val>> hash_compare;
	typedef StringT_Ref<t_Val> t_Ref;

public:
	DYNTYPE_FUNC StringT_Ref(){ _p = NULL; _len = 0; }
	DYNTYPE_FUNC StringT_Ref(const StringT_Ref& x){ *this = x; }
	DYNTYPE_FUNC StringT_Ref(const t_Val* x){ *this = x; }
	DYNTYPE_FUNC explicit StringT_Ref(const t_Val* p, SIZE_T len):_p((t_Val*)p),_len(len+1) { }
	DYNTYPE_FUNC explicit StringT_Ref(const t_Val* p, const t_Val* end):_p((t_Val*)p)
	{	if(end){ ASSERT(end>=p); _len = (int)(end-p+1); }
		else{ _len = 1 + _meta_::xxxlen(p);	}
	}
	template<class T>
	DYNTYPE_FUNC explicit StringT_Ref(const T& x){ *this = x; }
	//template<size_t size_>
	//DYNTYPE_FUNC explicit StringT_Ref(const t_Val p[size_]):_p((t_Val*)p),_len(size_){}
	//DYNTYPE_FUNC explicit StringT_Ref(const t_Val* p){ *this = p; }

	template<class T>
	DYNTYPE_FUNC const StringT_Ref& operator = (const T& x)
	{	typedef rt::TypeTraits<T>::t_Element t_Ele;
		__if_exists(T::Begin){ _p=(t_Val*)x.Begin(); }
		__if_not_exists(T::Begin){ _p=(t_Val*)((const t_Val*)x); }
		__if_exists(T::GetLength){ UINT l = x.GetLength(); _len = l>0?(l+1):0; }
		__if_not_exists(T::GetLength)
		{	if(_p){ UINT l = _meta_::xxxlen(_p); _len = l>0?(l+1):0; }
			else{ _len = 0; } return *this; 
		}
		return *this; 
	}

	//template<size_t sz>
	//DYNTYPE_FUNC const StringT_Ref& operator = (const t_Val p[sz]){ _p = (t_Val*)p; _len = sz; return *this; }
	//DYNTYPE_FUNC const StringT_Ref& operator = (const t_Val* p){ _p = (t_Val*)p; if(p){ _len = 1+static_cast<UINT>(_meta_::xxxlen(_p)); }else{ _len = 0; } return *this; }

	DYNTYPE_FUNC BOOL	IsEmpty() const { return _p==NULL || GetLength()<1 || _p[0] == 0; }
	DYNTYPE_FUNC void	Empty(){ _p = NULL; _len = 0; }
	DYNTYPE_FUNC SIZE_T	GetLength() const { return _len?_len-1:0; }
	DYNTYPE_FUNC SIZE_T CopyTo(t_Val* p) const { memcpy(p,_p,GetLength()*sizeof(t_Val)); return GetLength(); } // terminate-zero is not copied

	DYNTYPE_FUNC const t_Val*	Begin() const { return _p; }	// may NOT terminated by ZERO !!
	DYNTYPE_FUNC const t_Val*	GetBuffer() const { return _p; }	// may NOT terminated by ZERO !!
	DYNTYPE_FUNC	   t_Val*	Begin() { return _p; }	// may NOT terminated by ZERO !!
	DYNTYPE_FUNC       t_Val*	GetBuffer() { return _p; }	// may NOT terminated by ZERO !!
	DYNTYPE_FUNC const t_Val*	End() const { return _p+GetLength(); }
	DYNTYPE_FUNC t_Val&			Last(){ return _p[GetLength()-1]; }
	DYNTYPE_FUNC const t_Val&	Last() const { return _p[GetLength()-1]; }

	DYNTYPE_FUNC void		 Trim(){ TrimLeft(); TrimRight(); }
	DYNTYPE_FUNC void		 TrimLeft(){ if(_p && GetLength()){ while(GetLength()>0 && *_p<=0x20 && *_p>0){ _p++; _len--; } } }
	DYNTYPE_FUNC void		 TrimRight(){ if(_p && GetLength()){ while(GetLength()>0 && _p[GetLength()-1]<=0x20 && _p[GetLength()-1]>0)_len--; } }
	DYNTYPE_FUNC void		 Truncate(UINT start, SIZE_T length){ ASSERT(length+start <= GetLength()); _p+=start; _len=length+1; }
	DYNTYPE_FUNC void		 TruncateLeft(SIZE_T length){ ASSERT(_p); ASSERT(length <= GetLength()); _p+=length; _len-=length; }
	DYNTYPE_FUNC void		 TruncateRight(SIZE_T length){ ASSERT(_p); ASSERT(length <= GetLength()); _len-=length; }

	DYNTYPE_FUNC StringT_Ref<t_Val> GetSub_Right(SIZE_T len) const { ASSERT(_p); ASSERT(GetLength()>=len); return StringT_Ref<t_Val>(_p+GetLength()-len,End()); }
	DYNTYPE_FUNC StringT_Ref<t_Val> GetSub_Left(SIZE_T len) const { ASSERT(_p); ASSERT(GetLength()>=len); return StringT_Ref<t_Val>(_p,len); }
	DYNTYPE_FUNC StringT_Ref<t_Val> GetSub(SIZE_T start, SIZE_T len) const { ASSERT(_p); ASSERT(GetLength()>=len+start); return StringT_Ref<t_Val>(_p+start,len); }

	DYNTYPE_FUNC operator const t_Val* () const { ASSERT(!_p || !_p[GetLength()]); return _p; }
	DYNTYPE_FUNC operator t_Val* () { if(_p){ ASSERT(!_p[GetLength()]); } return _p; }
	DYNTYPE_FUNC t_Val	operator [](SIZE_T i) const { return _p[i]; }
	DYNTYPE_FUNC t_Val	operator [](int i) const { return _p[i]; }
	DYNTYPE_FUNC t_Val	operator [](UINT i) const { return _p[i]; }
	DYNTYPE_FUNC t_Val&	operator [](SIZE_T i){ return _p[i]; }
	DYNTYPE_FUNC t_Val&	operator [](int i){ return _p[i]; }
	DYNTYPE_FUNC t_Val&	operator [](UINT i){ return _p[i]; }
	DYNTYPE_FUNC bool	operator == (const t_Val* in) const { return (::memcmp(_p,in,GetLength()*sizeof(t_Val))==0) && in[GetLength()] == 0x0; }
	template< class StrT >
	DYNTYPE_FUNC bool operator == (const StrT & in) const 
	{	static const int issamechar = rt::IsTypeSame<StrT::t_Char,t_Val>::Result;
		ASSERT_STATIC(issamechar);
		return GetLength() == in.GetLength() && (::memcmp(_p,in.Begin(),GetLength()*sizeof(t_Val))==0); 
	}
	template< class StrT >
	DYNTYPE_FUNC bool	operator != (const StrT & in) const { return !(*this == in); }
	template< class StrT >
	DYNTYPE_FUNC bool	operator < (const StrT& x) const
	{	int ret = memcmp(_p,x.Begin(),min(GetLength(),x.GetLength())*sizeof(t_Val));
		return (ret < 0) || (ret==0 && (GetLength() < x.GetLength()));
	}

public:
	DYNTYPE_FUNC void	MakeUpper()
	{	for(SIZE_T i=0;i<GetLength();i++)
			if(_p[i] >='a' && _p[i] <='z') _p[i] -= ('a' - 'A');
	}
	DYNTYPE_FUNC void	MakeLower()
	{	for(SIZE_T i=0;i<GetLength();i++)
			if(_p[i] >='A' && _p[i] <='Z') _p[i] += ('a' - 'A');
	}
	DYNTYPE_FUNC SIZE_T	FindCharactor(t_Val ch, SIZE_T skip_first = 0) const
	{	for(;skip_first<GetLength();skip_first++)
			if(_p[skip_first] == ch)return skip_first;
		return -1;
	}
	DYNTYPE_FUNC SIZE_T	FindCharactorReverse(t_Val ch, int skip_first = 0) const
	{	int i = GetLength()-1;
		for(;i>=skip_first;i--)
			if(_p[i] == ch)return i;
		return -1;
	}
	DYNTYPE_FUNC void	Replace(t_Val a, t_Val b) // replace all a with b in the current string
	{	for(SIZE_T i=0; i<_len; ++i)
			if(_p[i] == a)_p[i] = b;
	}
	DYNTYPE_FUNC BOOL ParseNextNumber(UINT& out)	// parsed text will be removed
	{	SIZE_T start = 0;
		for(;start < GetLength();start++)
			if(_p[start]>=(t_Val)'0' && _p[start]<=(t_Val)'9')goto PARSE_UINT;
		return FALSE;
	PARSE_UINT:
		UINT ret = 0;
		for(;start < GetLength();start++)
			if(_p[start]>=(t_Val)'0' && _p[start]<=(t_Val)'9')
				ret = ret*10 + (_p[start] - (t_Val)'0');
			else break;
		_p += start; _len -= start;	out = ret;
		return TRUE;
	}
	DYNTYPE_FUNC BOOL RemoveExtName()
	{	int i=GetLength();
		for(;i>=0;i--)
		{	if(_p[i] == (t_Val)'.')goto FIND_EXT;
			if(_p[i] == (t_Val)'\\' || _p[i] == (t_Val)'/')return FALSE;
		}
		return FALSE;
	FIND_EXT:
		Truncate(0, i);
		return TRUE;
	}
	DYNTYPE_FUNC rt::StringT_Ref<t_Val> GetExtName() const // return ".xxx"
	{	int i=GetLength()-1;
		for(;i>=0;i--)
		{	if(_p[i] == '.')return rt::StringT_Ref<t_Val>(_p+i, End());
			if(_p[i] == '\\' || _p[i] == '/')return rt::StringT_Ref<t_Val>();
		}
		return rt::StringT_Ref<t_Val>();
	}
	template<typename T>
	UINT ToNumber(T& x) const {	return ToNumber<T,10>(x); }
	template<typename T, int BASE>
	UINT ToNumber(T& x) const  // [+/-/ ]12.34, return the offset where parsing stopped
	{	t_Val* p = _p;
		t_Val* end = _p + GetLength();
		if(p < end)
		{	BOOL neg = (*p == (t_Val)'-');
			if((*p<(t_Val)'0' || *p>(t_Val)('0'+BASE-1)) && p<end)p++;
			x = 0;
			while(*p>=(t_Val)'0' && *p<=(t_Val)('0'+BASE-1) && p<end)
			{	x = x*BASE + (*p - (t_Val)'0');
				p++;
			}
			if(*p == (t_Val)'.' && p<end) // 
			{	p++;
				enum{ is_float = rt::IsTypeSame<T,float>::Result };
				enum{ is_double = rt::IsTypeSame<T,double>::Result };
				__static_if(is_float)
				{	double frag = 0;		double div = 1;
					while(*p>=(t_Val)'0' && *p<=(t_Val)('0'+BASE-1) && p<end)
					{	div *= 1.0f/BASE;
						frag += div*(*p - (t_Val)'0');
						p++;
					}
					x = (T)(x + frag);
				}
				__static_if(is_double)
				{
					T div = 1;
					while(*p>=(t_Val)'0' && *p<=(t_Val)('0'+BASE-1) && p<end)
					{	div *= 1/(T)BASE;
						x += div*(*p - (t_Val)'0');
						p++;
					}
				}
				__static_if(!rt::NumericTraits<T>::IsFloat)
				{
					if(*p >= (t_Val)('0'+BASE/2))x++;
				}
			}
			__static_if(rt::NumericTraits<T>::IsSigned || rt::NumericTraits<T>::IsFloat)
			{ if(neg)x = -x; }
		}
		return p - _p;
	}
	DYNTYPE_FUNC BOOL GetNextLine(StringT_Ref<t_Val>& x)
	{	t_Val* start = _p;		const t_Val* tail = End();
		if(x.End() >= _p && x.End() <= tail)start = (t_Val*)x.End();
		while(start<tail && (*start==0xd || *start==0xa))start++;
		if(start>=tail)return FALSE;
		t_Val* end = start+1;
		while(end<tail && *end!=0xa && *end!=0xd)end++;
		x = StringT_Ref<t_Val>(start,end);
		return TRUE;
	}
	DYNTYPE_FUNC UINT ParseFields(StringT_Ref<t_Val>* fields, UINT fields_count, t_Val sep)	const // return count of field actually parsed
	{	UINT i=0;	const t_Val* start = _p;	const t_Val* tail = End();
		const t_Val* p = start;
		for(;;)
		{	
			if(*p!=sep && p<tail){ p++; }
			else
			{	rt::StringT_Ref<t_Val> v(start,p);
				v.Trim();
				if(v[0] == v.End()[-1] && (v[0] == (t_Val)'"' || v[0] == (t_Val)'\''))
					v.Truncate(1,v.GetLength()-2);
				fields[i++] = v;
				if(i>=fields_count || p==tail)
				{
					for(UINT j=i+1;j<fields_count;j++)
						fields[j].Empty();
					return i;
				}
				p++;
				start = p;
			}
		}
	}
};
template<class t_Ostream, typename t_Val>
t_Ostream& operator<<(t_Ostream& Ostream, const rt::StringT_Ref<t_Val> & vec)
{	if(vec.GetLength())
	{	if(vec[vec.GetLength()])
		{	for(SIZE_T i=0;i<vec.GetLength();i++)Ostream<<vec[i];	}
		else
			Ostream<<vec.Begin();
	}
	return Ostream;
}

namespace _meta_
{
	template<typename t_Val, int fixed_len> class _Fixed:public rt::StringT_Ref<t_Val>
	{	protected:	t_Val string[fixed_len];
		DYNTYPE_FUNC _Fixed(){ _p = string; }
		DYNTYPE_FUNC _Fixed(const _Fixed& x)
		{	_p = string; _len = x._len; if(_len>1)memcpy(_p,x._p,sizeof(t_Val)*(_len-1)); }
	};
	template<typename t_Val>
	class toS:public _Fixed<t_Val, 65>
	{	template<typename t_Val, typename t_Left, typename t_Right>
		friend class StringT_AddExpr;
		TYPETRAITS_DECL_ELEMENT_TYPE(t_Val)
		TYPETRAITS_DECL_LENGTH(TYPETRAITS_SIZE_UNKNOWN)	
		TYPETRAITS_DECL_IS_AGGREGATE(false)
		TYPETRAITS_DECL_IS_ARRAY(true)
	protected:
		toS(){}
		toS(const toS& x):_Fixed(x){}
	public:
		DYNTYPE_FUNC void	RightAlign(UINT w, t_Val pad)
		{	ASSERT(w <= sizeofArray(string));
			if(w > GetLength())
			{	memmove(&string[w - GetLength()], string, GetLength());
				for(UINT i=0; i<w - GetLength(); i++)string[i] = pad;
				_len = w + 1;
		}	}
		DYNTYPE_FUNC void	LeftAlign(UINT w, t_Val pad)
		{	ASSERT(w <= sizeofArray(string));
			if(w > GetLength())
			{	for(UINT i=GetLength(); i<w; i++)string[i] = pad;
				_len = w + 1;
		}	}
		DYNTYPE_FUNC toS(t_Val x){ _len = 2; string[0] = x; string[1] = (t_Val)'\0'; }
		DYNTYPE_FUNC toS(int x){ itox(x,string,10); _len = (UINT)xxxlen(string)+1; }
		DYNTYPE_FUNC toS(UINT x){ _ui64tox(x,string,10); _len = (UINT)xxxlen(string)+1; }
		DYNTYPE_FUNC toS(DWORD x){ _ui64tox(x,string,10); _len = (UINT)xxxlen(string)+1; }
		DYNTYPE_FUNC toS(__int64 x){ _i64tox(x,string,10); _len = (UINT)xxxlen(string)+1; }
		DYNTYPE_FUNC toS(ULONGLONG x){ _ui64tox(x,string,10); _len = (UINT)(xxxlen(string))+1; }
		DYNTYPE_FUNC toS(LPCVOID x)
		{	string[0] = (t_Val)'0';
			string[1] = (t_Val)'x';
#ifdef _WIN64
			_ui64tox((ULONGLONG)x,&string[2],16);
#else
			itox((int)x,&string[2],16);
#endif
			_len = (UINT)(xxxlen(string))+1;
		}
		DYNTYPE_FUNC toS(float x)
		{	__static_if(sizeof(t_Val) == sizeof(CHAR)){ _len = 1+sprintf(string,"%.2f",x); return; }
			__static_if(sizeof(t_Val) == sizeof(WCHAR)){ _len = 1+swprintf(string,L"%.2f",x); return; }
			ASSERT(0);
		}
		DYNTYPE_FUNC toS(double x)
		{	__static_if(sizeof(t_Val) == sizeof(CHAR)){ _len = 1+sprintf(string,"%.4f",x); return; }
			__static_if(sizeof(t_Val) == sizeof(WCHAR)){ _len = 1+swprintf(string,L"%.4f",x); return; }
			ASSERT(0);
		}
	};
} // namespace _meta_


namespace _meta_
{
	template<typename t_Val, int iFixLen>
	class StorageFixed
	{	
	protected:
		t_Val*	_p;
		UINT	_len;
		static const UINT _len_reserved = iFixLen;
		StorageFixed(){ _p = _pStorage; _len = 0; }
	private:
		t_Val _pStorage[iFixLen+1];
	public:
		BOOL ChangeSize(UINT new_size)
		{	if(new_size <= iFixLen)
			{	_len = new_size;
				return TRUE;
			}
			else return FALSE;
		}
	};
	template<typename t_Val>
	class StorageDyn
	{	
	protected:
		t_Val*	_p;
		UINT	_len;
		UINT	_len_reserved;
		StorageDyn(){ _p = NULL; _len_reserved = _len = 0; }
		~StorageDyn(){ _SafeFree32AL(_p); }
	public:
		DYNTYPE_FUNC BOOL ChangeSize(UINT new_size) //Orignal data at front is preserved
		{	if( new_size <= _len_reserved){ _len = new_size; return TRUE; }
			_len_reserved = __max(__max(16,new_size),_len_reserved*2);
			LPBYTE pNewBuffer = (LPBYTE)rt::Malloc32AL(sizeof(t_Val)*_len_reserved,TRUE); // no ask if failure
			if(pNewBuffer){}else{ return FALSE; }
			// copy old elements
			memcpy(pNewBuffer,_p,sizeof(t_Val)*_len);
			_SafeFree32AL(_p);
			_p = (t_Val*)pNewBuffer;
			_len = new_size;
			return TRUE;
		}
	};
}

template<typename t_Val, class t_Storage = ::rt::_meta_::StorageDyn<t_Val>>
class StringT:protected t_Storage
{
	TYPETRAITS_DECL_ELEMENT_TYPE(t_Val)
	TYPETRAITS_DECL_LENGTH(TYPETRAITS_SIZE_UNKNOWN)	
	TYPETRAITS_DECL_IS_AGGREGATE(false)
	TYPETRAITS_DECL_IS_ARRAY(true)
private:
	BOOL ChangeSize(SIZE_T){ ASSERT(0); return FALSE; }	// use SetLength instead
	BOOL SetSize(SIZE_T){ ASSERT(0); return FALSE; }	// use SetLength instead

protected:
	static const int IsWideChar = (sizeof(t_Val) == 2) ;
	typedef typename _meta_::_TypeIndex<WCHAR,CHAR,IsWideChar>::t_Result t_OtherChar;

public:
	typedef t_Val t_Char;
	typedef _meta_::hash_compare<StringT<t_Val>> hash_compare;
	typedef StringT_Ref<t_Val> t_Ref;

	TYPETRAITS_DECL_EXTENDTYPEID((rt::_typeid_codelib<<16)+5)
	// constructors
	DYNTYPE_FUNC StringT(){}
	DYNTYPE_FUNC StringT(const StringT& x){ *this = x; }
	DYNTYPE_FUNC StringT(const t_Val* x){ *this = x; }
	DYNTYPE_FUNC explicit StringT(const t_Char* p, int len){ *this = rt::StringT_Ref<t_Val>(p,len); }
	DYNTYPE_FUNC explicit StringT(const t_OtherChar* p, int len){ *this = rt::StringT_Ref<t_OtherChar>(p,len); }
	DYNTYPE_FUNC explicit StringT(const t_OtherChar* p){ *this = rt::StringT_Ref<t_OtherChar>(p); }
	DYNTYPE_FUNC explicit StringT(const rt::StringT_Ref<t_Char>& x){ *this = x; }
	DYNTYPE_FUNC explicit StringT(const rt::StringT_Ref<t_OtherChar>& x){ *this = x; }
	template<typename T, SIZE_T sz>
	DYNTYPE_FUNC explicit StringT(const T x[sz]){ *this = rt::StringT_Ref<T>(x,sz);}
	template<typename t_Left, typename t_Right>
	DYNTYPE_FUNC explicit StringT(const _meta_::StringT_AddExpr<t_Char,t_Left,t_Right>& str_exp){ *this = str_exp; }
	
	DEFAULT_TYPENAME(StringT)
	void TextDump() const{ _STD_OUT<<*this; _STD_OUT<<_T('\n'); }
	DYNTYPE_FUNC t_Val&	operator [](SIZE_T i){ return _p[i]; }
	DYNTYPE_FUNC t_Val&	operator [](int i){ return _p[i]; }
	DYNTYPE_FUNC t_Val&	operator [](UINT i){ return _p[i]; }
	DYNTYPE_FUNC t_Val	operator [](SIZE_T i) const { return _p[i]; }
	DYNTYPE_FUNC t_Val	operator [](int i) const { return _p[i]; }
	DYNTYPE_FUNC t_Val	operator [](UINT i) const { return _p[i]; }

	// Assignments
	DYNTYPE_FUNC const StringT& operator = (const StringT& x){	*this = x.GetRef(); return *this; }
	DYNTYPE_FUNC t_Char operator = (const t_Char x){ *this = rt::StringT_Ref<t_Char>(&x,1); return x; }
	DYNTYPE_FUNC const StringT& operator = (const t_Char* p){ *this = rt::StringT_Ref<t_Val>(p); return *this; }
	DYNTYPE_FUNC const StringT& operator = (const t_OtherChar* p){ *this = rt::StringT_Ref<t_OtherChar>(p); return *this; }
	DYNTYPE_FUNC const StringT& operator = (const StringT_Ref<t_Char>& in)
	{	if(!in.IsEmpty())
		{	VERIFY(__super::ChangeSize(in.GetLength()+1));
			memcpy(_p,in._p,in.GetLength()*sizeof(t_Val));
			_p[in.GetLength()] = (t_Val)'\0';	// in case [in] is partially referring [*this]
		}
		else Empty();
		return *this;
	}
	DYNTYPE_FUNC const StringT& operator = (const StringT_Ref<t_OtherChar>& in)
	{	if(!in.IsEmpty()){ VERIFY(ConvertCodePage(in)); }
		else Empty();
		return *this;
	}
	template< typename t_Left, typename t_right >
	DYNTYPE_FUNC StringT& operator = (const  _meta_::StringT_AddExpr<t_Val, t_Left, t_right>& e)
	{	SIZE_T len = e.GetLength();
		if(len)
		{	StringT tmp;
			tmp.SetLength(len);
			e.CopyTo(tmp._p);
			rt::Swap(tmp,*this);
		}else{ Empty(); }
		return *this;
	}
	template<class t_String>
	BOOL ConvertCodePage(const t_String& x, UINT codepage = CP_ACP)
	{	typedef typename t_String::t_Char T;
		__static_if(sizeof(t_Char) == sizeof(T)){ *this = x; return TRUE; }
		__static_if(sizeof(t_Char) > sizeof(T))		// A2W
		{	if(x.GetLength())
			{	int len = ::MultiByteToWideChar(codepage,0, x.Begin(),x.GetLength(), NULL,0);
				if(SetLength(len) && ::MultiByteToWideChar(codepage,0, x.Begin(),x.GetLength(), _p,len))
					return TRUE;
				else
				{	Empty(); return FALSE; }
			}
			Empty(); return TRUE;
		}
		__static_if(sizeof(t_Char) < sizeof(T))		// W2A
		{	if(x.GetLength())
			{	int len = ::WideCharToMultiByte(codepage,0, x.Begin(),x.GetLength(), NULL,0, NULL,NULL);
				if(SetLength(len) && ::WideCharToMultiByte(codepage,0, x.Begin(),x.GetLength(), _p,len, NULL, NULL))
					return TRUE;
				else
				{	Empty(); return FALSE; }
			}
			Empty(); return TRUE;
		}
	}

public:
	static const t_Val* EmptyString(){ return (const t_Val*)("\0\0"); }
	DYNTYPE_FUNC const t_Val* GetSafeString() const { return _p?_p:EmptyString(); }
	DYNTYPE_FUNC operator const t_Val* () const { return GetSafeString(); }
	DYNTYPE_FUNC t_Val* GetBuffer() { ASSERT(_p); return _p; }
	DYNTYPE_FUNC t_Val* Begin() { ASSERT(_p); return _p; }
	DYNTYPE_FUNC const t_Val* GetBuffer() const { ASSERT(_p); return _p; }
	DYNTYPE_FUNC const t_Val* Begin() const { return _p; }
	DYNTYPE_FUNC const t_Val* End() const { return _p + GetLength(); }
	DYNTYPE_FUNC t_Val&		  Last(){ return _p[GetLength()-1]; }
	DYNTYPE_FUNC const t_Val& Last() const { return _p[GetLength()-1]; }
	//operator t_Val* () { ASSERT(_p); return _p; }

	DYNTYPE_FUNC operator const StringT_Ref<t_Val>& () const { return *((const StringT_Ref<t_Val>*)this); }
	DYNTYPE_FUNC operator StringT_Ref<t_Val>& () { return *((StringT_Ref<t_Val>*)this); }
	DYNTYPE_FUNC StringT_Ref<t_Val>& GetRef(){ return *((StringT_Ref<t_Val>*)this); }
	DYNTYPE_FUNC const StringT_Ref<t_Val>& GetRef() const { return rt::_CastToNonconst(this)->GetRef(); }

	DYNTYPE_FUNC BOOL IsEmpty() const { return _p==NULL || _len<=1 || _p[0] == 0; }
	DYNTYPE_FUNC SIZE_T GetLength() const { return _len?_len-1:0; }
	DYNTYPE_FUNC SIZE_T CopyTo(t_Val* p) const { memcpy(p,_p,GetLength()*sizeof(t_Val)); return GetLength(); } // terminate-zero is not copied
	DYNTYPE_FUNC BOOL SetLength(SIZE_T sz = 0)
	{	if(__super::ChangeSize(sz+1)){ _p[sz] = 0; return TRUE; }
		else return FALSE;
	}
	DYNTYPE_FUNC void RecalculateLength(){ if(_p){ _len = _meta_::xxxlen(_p) + 1; }else{ _len = 0; } }

	template<typename T>
	DYNTYPE_FUNC bool operator != (const T& x) const {	return !(this->GetRef()==x); }
	template<typename T>
	DYNTYPE_FUNC bool operator == (const T& x) const {	return this->GetRef()==x; }
	template< class StrT >
	DYNTYPE_FUNC bool operator < (const StrT& in) const { return GetRef() < in; }

	template<typename T>
	DYNTYPE_FUNC void operator +=(const T& x){ *this = *this + x; }
	DYNTYPE_FUNC void operator +=(t_Char x){ int pos = GetLength(); SetLength(pos+1); _p[pos] = x; }
	DYNTYPE_FUNC const StringT& operator += (const StringT_Ref<t_Val>& e)
	{	SIZE_T len = e.GetLength();
		if(len)
		{	if(_len + len < _len_reserved)
			{	memcpy(Begin()+GetLength(),e.Begin(),len*sizeof(t_Val)); SetLength(len + GetLength()); }
			else{ *this = *this + e; }
		}
		return *this;
	}
	template< typename t_Left, typename t_right >
	DYNTYPE_FUNC StringT& operator += (const  _meta_::StringT_AddExpr<t_Val, t_Left, t_right>& e)
	{	SIZE_T len = e.GetLength();
		if(len)
		{	*this = *this + e;
		}
		return *this;
	}

	DYNTYPE_FUNC StringT_Ref<t_Val>	Trim() const { StringT_Ref<t_Val> ref(_p,GetLength()); ref.Trim(); return ref; }
	DYNTYPE_FUNC StringT_Ref<t_Val>	TrimLeft() const { StringT_Ref<t_Val> ref(_p,GetLength()); ref.TrimLeft(); return ref; }
	DYNTYPE_FUNC StringT_Ref<t_Val>	TrimRight() const { StringT_Ref<t_Val> ref(_p,GetLength()); ref.TrimRight(); return ref; }
	DYNTYPE_FUNC StringT_Ref<t_Val>	Truncate(UINT start, SIZE_T length) const { StringT_Ref<t_Val> ref(_p,GetLength()); ref.Truncate(start,length); return ref; }
	DYNTYPE_FUNC StringT_Ref<t_Val>	TruncateLeft(SIZE_T length) const { StringT_Ref<t_Val> ref(_p,GetLength()); ref.TruncateLeft(length); return ref; }
	DYNTYPE_FUNC StringT_Ref<t_Val>	TruncateRight(SIZE_T length) const { StringT_Ref<t_Val> ref(_p,GetLength()); ref.TruncateRight(length); return ref; }

public:
	// opertations
	DYNTYPE_FUNC void	Zero(){ if(_p && _len>0)ZeroMemory(_p,_len*sizeof(t_Val)); }
	DYNTYPE_FUNC void	Empty(){ SetLength(0); }
	DYNTYPE_FUNC void	MakeUpper(){ _meta_::xxxupr(Begin()); }
	DYNTYPE_FUNC void	MakeLower(){ _meta_::xxxlwr(Begin()); }
	DYNTYPE_FUNC int	FindString(const t_Val* pSub, SIZE_T skip_first = 0) const
	{	ASSERT(skip_first<GetLength());
		return (pSub=_meta_::xxxstr(Begin()+skip_first,pSub))?(int)(pSub-Begin()):-1; 
	}
	DYNTYPE_FUNC int	FindCharactor(t_Val ch, SIZE_T skip_first = 0) const
	{	ASSERT(skip_first<GetLength());
		const t_Val* pSub = _meta_::xxxchr(Begin()+skip_first,ch);
		return pSub?(int)(pSub-Begin()):-1; 
	}
	DYNTYPE_FUNC int	FindCharactorReverse(t_Val ch, SIZE_T skip_first = 0) const
	{	ASSERT(skip_first<GetLength());
		const t_Val* pSub = _meta_::xxxrchr(Begin()+skip_first,ch);
		return pSub?(int)(pSub-Begin()):-1;
	}
	DYNTYPE_FUNC void	Replace(t_Val a, t_Val with_b) // replace all a with b in the current string
	{
		for(int i=0; i<((int)_len)-1; ++i)
			if(_p[i] == a)_p[i] = with_b;
	}
	void	FromTextFile(LPCSTR pData, UINT codepage = CP_ACP) // codepage will be override if UTF-x header is detected
	{	
		WORD hdr1 = ((LPCWORD)pData)[0];
		BYTE hdr2 = ((LPCBYTE)pData)[2];
		if(hdr1 == 0xfeff)
		{	// CP_UTF16;
			__static_if(IsWideChar) { *this = &((LPCWSTR)pData)[1]; }
			__static_if(!IsWideChar){ *this = ATL::CW2A(&((LPCWSTR)pData)[1],codepage); }
		}
		else if(hdr1 == 0xbbef && hdr2 == 0xbf)
		{	// CP_UTF8;
			__static_if(IsWideChar) { ConvertCodePage(rt::StringA_Ref(&pData[3]),CP_UTF8); }
			__static_if(!IsWideChar)
			{	rt::StringW	utf16;	utf16.ConvertCodePage(rt::StringA_Ref(&pData[3]),CP_UTF8);
				ConvertCodePage(utf16,codepage);
			}
		}
		else if(hdr1 == 0xfffe)
			return; // UTF16 big endian, not supported
		else
		{
			__static_if(IsWideChar) { *this = ATL::CA2W(pData,codepage); }
			__static_if(!IsWideChar){ *this = pData; }
		}
	}

	void	ToTextFile(rt::StringT<CHAR>& data, UINT target_codepage = CP_UTF8, UINT default_codepage = CP_ACP) // UTF header will be inserted
	{
		data.Empty();

		switch(target_codepage)
		{
		case CP_ACP:	// plain
			{
				__static_if(IsWideChar) { data = ATL::CW2A(_p,default_codepage); }
				__static_if(!IsWideChar){ data = *this; }
				break;
			}
		case CP_UTF16:
			{
				__static_if(IsWideChar)
				{	if(data.SetLength((1 + GetLength())*sizeof(WCHAR)))
					{
						WCHAR * p = (WCHAR *)data.Begin();
						p[0] = 0xfeff;
						memcpy(&p[1],_p,GetLength()*sizeof(WCHAR));
					}
				}
				__static_if(!IsWideChar)
				{	ATL::CA2W a(_p,default_codepage);
					UINT len = (UINT)wcslen(a) + 1;
					if(data.SetLength((1 + len)*sizeof(WCHAR)))
					{
						WCHAR * p = (WCHAR *)data.Begin();
						p[0] = 0xfeff;
						memcpy(&p[1],a,len*sizeof(WCHAR));
					}
				}
			}
			break;
		case CP_UTF8:
			{
				__static_if(IsWideChar)
				{	ATL::CW2A utf8(_p,CP_UTF8); 
					UINT len = (UINT)strlen(utf8) + 1;
					if(!data.SetLength(3 + len))return;
					memcpy(&data[3],utf8,len);
				}
				__static_if(!IsWideChar)
				{	ATL::CA2W utf16(_p,default_codepage);
					ATL::CW2A utf8(utf16,CP_UTF8);
					UINT len = (UINT)strlen(utf8) + 1;
					if(!data.SetLength(3 + len))return;
					memcpy(&data[3],utf8,len);
				}
				data[0] = (char)0xef;
				data[1] = (char)0xbb;
				data[2] = (char)0xbf;
			}
			break;
		}
	}
};
template<class t_Ostream, typename t_Val>
t_Ostream& operator<<(t_Ostream& Ostream, const rt::StringT<t_Val> & vec)
{
	if(!vec.IsEmpty())
		Ostream<<vec.Begin();
	return Ostream;
}

namespace tos
{

template<typename t_Val>
struct Number:public ::rt::_meta_::toS<t_Val>
{	template<typename T>
	Number(T x): ::rt::_meta_::toS<t_Val>(x){}
};

template<typename t_Val>
struct WhiteSpace:public ::rt::_meta_::toS<t_Val>
{	WhiteSpace(int len)
	{	ASSERT(len+1 < sizeof(string));
		for(int i=0;i<len;i++)_p[i] = (t_Val)' ';
		_p[len] = (t_Val)'\0';
		_len = len+1;
	}
};

template<typename t_Val>
struct Charactor:public ::rt::_meta_::toS<t_Val>
{	Charactor(t_Val c)
	{	_p[0] = c;	_p[1] = (t_Val)'\0';
		_len = 2;
	}
};

template<typename t_Val, bool upper_case = true, bool compact = false>
struct FileSize:public ::rt::_meta_::toS<t_Val>
{	
	FileSize(ULONGLONG filesize)
	{
		__static_if(upper_case && !compact)
		{
			if(filesize < 1024)
				SetString(rt::StringT_Ref<t_Val>() + (UINT)(filesize) + (t_Val)' ' + (t_Val)'B');
			else if(filesize < 1024*1024)
				SetString(rt::StringT_Ref<t_Val>() + (UINT)(filesize/1024) + (t_Val)' ' + (t_Val)'K' + (t_Val)'B');
			else if(filesize < 1024*1024*1024)
				SetString(rt::StringT_Ref<t_Val>() + (float)(filesize/1000/1024.f) + (t_Val)' ' + (t_Val)'M' + (t_Val)'B');
			else
				SetString(rt::StringT_Ref<t_Val>() + (float)(filesize/1000/1000/1024.f) + (t_Val)' ' + (t_Val)'G' + (t_Val)'B');
		}
		__static_if(!upper_case && !compact)
		{
			if(filesize < 1024)
				SetString(rt::StringT_Ref<t_Val>() + (UINT)(filesize) + (t_Val)' ' + (t_Val)'b');
			else if(filesize < 1024*1024)
				SetString(rt::StringT_Ref<t_Val>() + (UINT)(filesize/1024) + (t_Val)' ' + (t_Val)'k' + (t_Val)'b');
			else if(filesize < 1024*1024*1024)
				SetString(rt::StringT_Ref<t_Val>() + (float)(filesize/1000/1024.f) + (t_Val)' ' + (t_Val)'m' + (t_Val)'b');
			else
				SetString(rt::StringT_Ref<t_Val>() + (float)(filesize/1000/1000/1024.f) + (t_Val)' ' + (t_Val)'g' + (t_Val)'b');
		}
		__static_if(upper_case && compact)
		{
			if(filesize < 1024)
				SetString(rt::StringT_Ref<t_Val>() + (UINT)(filesize) + (t_Val)'B');
			else if(filesize < 1024*1024)
				SetString(rt::StringT_Ref<t_Val>() + (UINT)(filesize/1024) + (t_Val)'K' + (t_Val)'B');
			else if(filesize < 1024*1024*1024)
				SetString(rt::StringT_Ref<t_Val>() + (float)(filesize/1000/1024.f) + (t_Val)'M' + (t_Val)'B');
			else
				SetString(rt::StringT_Ref<t_Val>() + (float)(filesize/1000/1000/1024.f) + (t_Val)'G' + (t_Val)'B');
		}
		__static_if(!upper_case && compact)
		{
			if(filesize < 1024)
				SetString(rt::StringT_Ref<t_Val>() + (UINT)(filesize) + (t_Val)'b');
			else if(filesize < 1024*1024)
				SetString(rt::StringT_Ref<t_Val>() + (UINT)(filesize/1024) + (t_Val)'k' + (t_Val)'b');
			else if(filesize < 1024*1024*1024)
				SetString(rt::StringT_Ref<t_Val>() + (float)(filesize/1000/1024.f) + (t_Val)'m' + (t_Val)'b');
			else
				SetString(rt::StringT_Ref<t_Val>() + (float)(filesize/1000/1000/1024.f) + (t_Val)'g' + (t_Val)'b');
		}
	}
private:
	template<class T>
	void SetString(const T& x)
	{	ASSERT(x.GetLength() < sizeofArray(string)-1);
		_len = x.CopyTo(string);
		string[_len] = (t_Val)'\0';
		_len++;
	}
};

template<typename t_Val, bool _smart = true>
struct TimeSpan:public ::rt::_meta_::toS<t_Val>
{	TimeSpan(int msec)
	{	t_Val* p = string;
		if(msec < 0){ p[0]='-'; p++; msec = -msec; }
		if(msec >= 10000)
		{	msec = (int)(msec/1000.0f + 0.5f);
			if(msec >= 3600*24)
			{	_meta_::itox(msec/(3600*24),p,10);	p+=_meta_::xxxlen(p);
				p[0]=(t_Val)'d';	p++;
				if(_smart && (msec%(3600*24)) == 0)
				{	p[-1] = (t_Val)' '; p[0] = (t_Val)'d';	p[1] = (t_Val)'a';	p[2] = (t_Val)'y';	p+=3;
					if(msec >= 3600*48){p[0] = (t_Val)'s';	p++;  }
					goto END_OF_PRINT;
				}
				p[0]=(t_Val)':';	p++;
			}
			if(msec >= 3600)
			{	_meta_::itox((msec%(3600*24))/3600,p,10);	p+=_meta_::xxxlen(p);
				p[0]=(t_Val)'h';	p++;
				if(_smart && (msec%3600) == 0)
				{	if(msec < 3600*24)
					{	p[-1] = (t_Val)' '; p[0] = (t_Val)'h';	p[1] = (t_Val)'o';	p[2] = (t_Val)'u';	p[3] = (t_Val)'r';	p+=4;
						if(msec >= 7200){   p[0] = (t_Val)'s';	p++; }
					}
					goto END_OF_PRINT;
				}
				p[0]=(t_Val)':';	p++;
			}
			if(msec > 60)
			{	_meta_::itox((msec%(3600))/60,p,10);	p+=_meta_::xxxlen(p);
				p[0]=(t_Val)'m';	p++;
				if(_smart && (msec%60) == 0)
				{	if(msec < 3600)
					{	p[-1] = (t_Val)' '; p[0] = (t_Val)'m';	p[1] = (t_Val)'i';	p[2] = (t_Val)'n';
						p[3] = (t_Val)'u';	p[4] = (t_Val)'t';	p[5] = (t_Val)'e';	p+=6;
						if(msec >= 120){ p[0] = (t_Val)'s'; p++; }
					}
					goto END_OF_PRINT;
				}
				p[0]=(t_Val)':';	p++;
			}
			_meta_::itox(msec%60,p,10);		p+=_meta_::xxxlen(p);
			p[0]=(t_Val)'s';				p++;
			if(_smart && msec < 60)
			{	p[-1]=(t_Val)' '; p[0]=(t_Val)'s'; p[1]=(t_Val)'e'; p[2]=(t_Val)'c'; p[3]=(t_Val)'o'; p[4]=(t_Val)'n'; p[5]=(t_Val)'d'; p+=6;
				if(msec > 1){ p[0]=(t_Val)'s'; p++; }
			}
		}
		else
		{	
			p[0] = (t_Val)('0' + msec/1000);	p++;
			int frac = msec%1000;
			if(frac)
			{	p[0] = (t_Val)'.';	p++;
				p[0] = (t_Val)('0' + (msec%1000)/100);
				p[1] = (t_Val)('0' + (msec%100)/10);
				p[2] = (t_Val)('0' + (msec%10));
				p+=3;
			}
			if(!_smart){ p[0] = 's'; p++; }
			else
			{	p[0]=(t_Val)' '; p[1]=(t_Val)'s'; p[2]=(t_Val)'e'; p[3]=(t_Val)'c'; p[4]=(t_Val)'o'; p[5]=(t_Val)'n'; p[6]=(t_Val)'d'; p+=7;
				if(msec >= 2000){ p[0]=(t_Val)'s'; p++; }
			}
		}
END_OF_PRINT:
		p[0] = '\0';
		_len = 1+(p-string);
	}
};

template<typename t_Val, int SEP = '/',bool no_year = false>
struct Date:public ::rt::_meta_::toS<t_Val>
{
	template<typename t_w32_CDate32>
	Date(const t_w32_CDate32& x)
	{	
		__static_if(!no_year)
		{
			__static_if(sizeof(t_Val)==1)
			{	_len = 1+sprintf(string,print_template(), x.GetYear(), x.GetMonth(), x.GetDay());
			}
			__static_if(sizeof(t_Val)==2)
			{	_len = 1+swprintf(string,print_template(), x.GetYear(), x.GetMonth(), x.GetDay());
			}
			__static_if(SEP != '/')
			{	string[4] = string[7] = (t_Val)SEP;
			}
		}
		__static_if(no_year)
		{
			__static_if(sizeof(t_Val)==1)
			{	_len = 1+sprintf(string,print_template()+5, x.GetMonth(), x.GetDay());
			}
			__static_if(sizeof(t_Val)==2)
			{	_len = 1+swprintf(string,print_template()+5, x.GetMonth(), x.GetDay());
			}
			__static_if(SEP != '/')
			{	string[2] = (t_Val)SEP;
			}
		}
	}
protected:
	const t_Val* print_template() const 
	{	
		__static_if(sizeof(t_Val)==1){ return "%04d/%02d/%02d"; }
		__static_if(sizeof(t_Val)==2){ return L"%04d/%02d/%02d"; }
	}
};

template<typename t_Val>
struct Binary:public ::rt::_meta_::toS<t_Val>
{
	Binary(LPCVOID pbyte, int len)	
	{	
		len = min(len,sizeofArray(string)/2);
		LPSTR p = string;
		for(int i=0;i<len;i++,p+=2)
		{
			BYTE v = ((LPCBYTE)pbyte)[i];
			p[0] = (t_Val)(v>=0xa0?'A'+((v-0xa0)>>4):'0'+(v>>4));
			v = v&0xf;
			p[1] = (t_Val)(v>=0xa?'A'+(v-0xa):'0'+v);
		}
		_len = len*2;
	}
};

template<typename t_Val, bool compact = false>
struct DateTime:public ::rt::_meta_::toS<t_Val>
{
	template<typename t_w32_CTime64>
	DateTime(const t_w32_CTime64& x)
	{
		__static_if(compact)
		{
			__static_if(sizeof(t_Val)==1)
			{	_len = 1+sprintf(string,print_template(), x.GetYear(), x.GetMonth(), x.GetDayOfMonth(), x.GetHour(), x.GetMinute(), x.GetSecond());
			}
			__static_if(sizeof(t_Val)==2)
			{	_len = 1+swprintf(string,print_template(), x.GetYear(), x.GetMonth(), x.GetDayOfMonth(), x.GetHour(), x.GetMinute(), x.GetSecond());
			}
		}

		__static_if(!compact)
		{
			__static_if(sizeof(t_Val)==1)
			{	_len = 1+sprintf(string,print_template(), x.GetDayOfWeekName(), x.GetDayOfMonth(),
							  x.GetMonthName(), x.GetYear(), x.GetHour(), x.GetMinute(), x.GetSecond());
			}
			__static_if(sizeof(t_Val)==2)
			{	_len = 1+swprintf(string,print_template(), x.GetDayOfWeekName(), x.GetDayOfMonth(),
							  x.GetMonthName(), x.GetYear(), x.GetHour(), x.GetMinute(), x.GetSecond());
			}
		}
	}
protected:
	const t_Val* print_template() const 
	{	__static_if(compact)
		{
			__static_if(sizeof(t_Val)==1){ return "%04d/%02d/%02d %02d:%02d:%02d"; }
			__static_if(sizeof(t_Val)==2){ return L"%04d/%02d/%02d %02d:%02d:%02d"; }
		}
		__static_if(!compact)
		{
			__static_if(sizeof(t_Val)==1){ return "%s, %02d %s %04d %02d:%02d:%02d"; }
			__static_if(sizeof(t_Val)==2){ return L"%s, %02d %s %04d %02d:%02d:%02d"; }
		}
	}
};

template<typename t_Val>
struct GUID
{
	DYNTYPE_FUNC explicit GUID(const ::GUID& guid)
	{
		__static_if(sizeof(t_Val)==1)
		{	_len = 1+sprintf(string,print_template(), guid.Data1,guid.Data2,guid.Data3,
						  guid.Data4[0],guid.Data4[1],guid.Data4[2],guid.Data4[3],
						  guid.Data4[4],guid.Data4[5],guid.Data4[6],guid.Data4[7]);
		}
		__static_if(sizeof(t_Val)==2)
		{	_len = 1+swprintf(string,print_template(), guid.Data1,guid.Data2,guid.Data3,
						  guid.Data4[0],guid.Data4[1],guid.Data4[2],guid.Data4[3],
						  guid.Data4[4],guid.Data4[5],guid.Data4[6],guid.Data4[7]);
		}
	}
	const t_Val* print_template() const 
	{	
		__static_if(sizeof(t_Val)==1){ return "%08x-%04x-%04x-%02x%02x-%02x%02x%02x%02x%02x%02x"; }
		__static_if(sizeof(t_Val)==2){ return L"%08x-%04x-%04x-%02x%02x-%02x%02x%02x%02x%02x%02x"; }
	}
};


} // tos


//////////////////////////////////////////////////////////
// StringT_AddExpr
namespace _meta_
{

template<typename t_Val, typename t_Left, typename t_Right>
class StringT_AddExpr
{	TYPETRAITS_DECL_ELEMENT_TYPE(t_Val)
	TYPETRAITS_DECL_LENGTH(TYPETRAITS_SIZE_UNKNOWN)	
	TYPETRAITS_DECL_IS_AGGREGATE(false)
	TYPETRAITS_DECL_IS_ARRAY(true)
	template<typename t_Val, typename t_Left, typename t_Right>
	friend class StringT_AddExpr;
	t_Val*		_pString;
	DYNTYPE_FUNC SIZE_T _ExprFill(t_Val* p) const	// return number of char filled
	{	SIZE_T len = _left._ExprFill(p);
		return len + _right._ExprFill(p+len);
	}

public:
	typedef t_Val t_Char;
	t_Left		_left;
	t_Right		_right;

public:
	template<typename T1, typename T2>
	DYNTYPE_FUNC StringT_AddExpr(T1& l, T2& r)
		:_left(l),_right(r),_pString(NULL)
	{}
	DYNTYPE_FUNC ~StringT_AddExpr(){ _SafeFree32AL(_pString); }
	DYNTYPE_FUNC const t_Val* Begin() const { return rt::_CastToNonconst(this)->GetSafeString(); }
	DYNTYPE_FUNC const t_Val* GetSafeString() 
	{	if(_pString == NULL)
		{	UINT len = (UINT)GetLength();
			_pString = _Malloc32AL(t_Val,len+1);
			ASSERT(_pString);
			VERIFY(len == _ExprFill(_pString));
			_pString[len] = '\0';
		}
		return _pString;
	}
	DYNTYPE_FUNC operator const t_Val* () const { return ((StringT_AddExpr*)this)->GetSafeString(); }
	DYNTYPE_FUNC SIZE_T GetLength() const
	{	
		return _left.GetLength() + _right.GetLength();
	}
	DYNTYPE_FUNC SIZE_T CopyTo(t_Val* p) const { return _ExprFill(p); }  // terminate-zero is not copied
};
template<class t_Ostream, typename t_Val, typename t_Left, typename t_Right>
DYNTYPE_FUNC t_Ostream& operator<<(t_Ostream& Ostream, const rt::_meta_::StringT_AddExpr<t_Val,t_Left,t_Right> & str_expr)
{
	return Ostream<<str_expr._left<<str_expr._right;
}

} // namespace _meta_

template<typename t_Val, typename t_Left, typename t_Right>
struct _SE:public rt::_meta_::StringT_AddExpr<t_Val,t_Left,t_Right>
{	template<typename T1, typename T2>
	DYNTYPE_FUNC _SE(T1& l, T2& r):StringT_AddExpr(l,r){}
};

#define STRING_CONNECT_OP_(t_Left, t_Right, t_Left_in, t_Right_in)							\
template<typename t_Val> DYNTYPE_FUNC														\
_SE<t_Val, t_Left, t_Right >  operator + (const t_Left_in left, const t_Right_in right)		\
{ return _SE<t_Val, t_Left, t_Right > ( (left), (right) ); }								\

#define STRING_CONNECT_OP(t_Left, t_Right, t_Left_in, t_Right_in)							\
		STRING_CONNECT_OP_(t_Right, t_Left, t_Right_in, t_Left_in)							\
		STRING_CONNECT_OP_(t_Left, t_Right, t_Left_in, t_Right_in)							\

STRING_CONNECT_OP_(StringT_Ref<t_Val>,StringT_Ref<t_Val>, StringT_Ref<t_Val>&, StringT_Ref<t_Val>&	)
STRING_CONNECT_OP(StringT_Ref<t_Val>, StringT_Ref<t_Val>, StringT_Ref<t_Val>&, StringT<t_Val>&		)
STRING_CONNECT_OP(StringT_Ref<t_Val>, _meta_::toS<t_Val>, StringT_Ref<t_Val>&, _meta_::toS<t_Val>&	)
STRING_CONNECT_OP(StringT_Ref<t_Val>, StringT_Ref<t_Val>, StringT_Ref<t_Val>&, t_Val*				)
STRING_CONNECT_OP(StringT_Ref<t_Val>, _meta_::toS<t_Val>, StringT_Ref<t_Val>&, t_Val				)
STRING_CONNECT_OP(StringT_Ref<t_Val>, _meta_::toS<t_Val>, StringT_Ref<t_Val>&, INT					)
STRING_CONNECT_OP(StringT_Ref<t_Val>, _meta_::toS<t_Val>, StringT_Ref<t_Val>&, UINT					)
STRING_CONNECT_OP(StringT_Ref<t_Val>, _meta_::toS<t_Val>, StringT_Ref<t_Val>&, DWORD				)
STRING_CONNECT_OP(StringT_Ref<t_Val>, _meta_::toS<t_Val>, StringT_Ref<t_Val>&, LONGLONG				)
STRING_CONNECT_OP(StringT_Ref<t_Val>, _meta_::toS<t_Val>, StringT_Ref<t_Val>&, ULONGLONG			)
STRING_CONNECT_OP(StringT_Ref<t_Val>, _meta_::toS<t_Val>, StringT_Ref<t_Val>&, float				)
STRING_CONNECT_OP(StringT_Ref<t_Val>, _meta_::toS<t_Val>, StringT_Ref<t_Val>&, double				)

STRING_CONNECT_OP_(StringT_Ref<t_Val>,StringT_Ref<t_Val>, StringT<t_Val>&	 , StringT<t_Val>&		)
STRING_CONNECT_OP(StringT_Ref<t_Val>, _meta_::toS<t_Val>, StringT<t_Val>&	 , _meta_::toS<t_Val>&	)
STRING_CONNECT_OP(StringT_Ref<t_Val>, StringT_Ref<t_Val>, StringT<t_Val>&	 , t_Val*				)
STRING_CONNECT_OP(StringT_Ref<t_Val>, _meta_::toS<t_Val>, StringT<t_Val>&	 , t_Val				)
STRING_CONNECT_OP(StringT_Ref<t_Val>, _meta_::toS<t_Val>, StringT<t_Val>&	 , INT					)
STRING_CONNECT_OP(StringT_Ref<t_Val>, _meta_::toS<t_Val>, StringT<t_Val>&	 , UINT					)
STRING_CONNECT_OP(StringT_Ref<t_Val>, _meta_::toS<t_Val>, StringT<t_Val>&	 , DWORD				)
STRING_CONNECT_OP(StringT_Ref<t_Val>, _meta_::toS<t_Val>, StringT<t_Val>&	 , LONGLONG				)
STRING_CONNECT_OP(StringT_Ref<t_Val>, _meta_::toS<t_Val>, StringT<t_Val>&	 , ULONGLONG			)
STRING_CONNECT_OP(StringT_Ref<t_Val>, _meta_::toS<t_Val>, StringT<t_Val>&	 , float				)
STRING_CONNECT_OP(StringT_Ref<t_Val>, _meta_::toS<t_Val>, StringT<t_Val>&	 , double				)


#undef STRING_CONNECT_OP
#undef STRING_CONNECT_OP_


#define STRING_EXPR_CONNECT_OP(type, type_in)										\
template<typename t_Val, typename t_Left, typename t_Right> DYNTYPE_FUNC			\
_SE<t_Val, type, _SE<t_Val,t_Left,t_Right> >										\
operator + (const type_in p, const _SE<t_Val,t_Left,t_Right>& x)					\
{ return _SE<t_Val, type, _SE<t_Val,t_Left,t_Right>>((p),x); }						\
template<typename t_Val, typename t_Left, typename t_Right> DYNTYPE_FUNC			\
_SE<t_Val, _SE<t_Val,t_Left,t_Right> , type>										\
operator + (const _SE<t_Val,t_Left,t_Right>& x, const type_in p)					\
{ return _SE<t_Val, _SE<t_Val,t_Left,t_Right>, type>(x, (p)); }						\


STRING_EXPR_CONNECT_OP(StringT_Ref<t_Val>, StringT_Ref<t_Val>&	)
STRING_EXPR_CONNECT_OP(StringT_Ref<t_Val>, StringT<t_Val>&		)
STRING_EXPR_CONNECT_OP(_meta_::toS<t_Val>, _meta_::toS<t_Val>&	)
STRING_EXPR_CONNECT_OP(StringT_Ref<t_Val>, t_Val*				)
STRING_EXPR_CONNECT_OP(_meta_::toS<t_Val>, t_Val				)
STRING_EXPR_CONNECT_OP(_meta_::toS<t_Val>, INT					)
STRING_EXPR_CONNECT_OP(_meta_::toS<t_Val>, UINT					)
STRING_EXPR_CONNECT_OP(_meta_::toS<t_Val>, DWORD				)
STRING_EXPR_CONNECT_OP(_meta_::toS<t_Val>, LONGLONG				)
STRING_EXPR_CONNECT_OP(_meta_::toS<t_Val>, ULONGLONG			)
STRING_EXPR_CONNECT_OP(_meta_::toS<t_Val>, float				)
STRING_EXPR_CONNECT_OP(_meta_::toS<t_Val>, double				)


template<typename t_Val, typename t_LL, typename t_LR, typename t_RL, typename t_RR > DYNTYPE_FUNC 
_SE<t_Val, _SE<t_Val,t_LL,t_LR>, _SE<t_Val,t_RL,t_RR> >	
operator + (const _SE<t_Val,t_LL,t_LR>& xl, const _SE<t_Val,t_RL,t_RR>& xr)					
{ return _SE<t_Val, _SE<t_Val,t_LL,t_LR>, _SE<t_Val,t_RL,t_RR> >(xl,xr); }

///////////////////////////////////////////////
// shortcuts
typedef StringT<TCHAR> String;
typedef StringT_Ref<TCHAR> String_Ref;

typedef StringT<CHAR> StringA;
typedef StringT_Ref<CHAR> StringA_Ref;

typedef StringT<WCHAR> StringW;
typedef StringT_Ref<WCHAR> StringW_Ref;

} // namespace rt

