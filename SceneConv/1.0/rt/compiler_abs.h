#pragma once

//////////////////////////////////////////////////////////////////////
// Visual Computing Foundation Classes (VCFC)
//					Jiaping Wang  2006.2
//					e_boris2002@hotmail.com
//
// (C) Copyright Jiaping Wang 2004.
//
// Permission to copy, use, modify, sell and distribute this software
// is granted provided this copyright notice appears in all copies.
// This software is provided "as is" without express or implied
// warranty, and with no claim as to its suitability for any purpose.
//
// Absolutely no warranty!!
//////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////
//  Brief Description
//
//  compiler_abs.h
//
//  Feature abstraction for compiler functionality.
//
//
//////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////
// Revise History
//
// Inital version	2006.6.4		Jiaping
//
//////////////////////////////////////////////////////////////////////

#include "Windows.h"

#ifndef _MSC_EXTENSIONS
#	error Microsoft C++ extensions required. ( compiler option /Ze )
#endif 

#pragma inline_depth(255)
#pragma inline_recursion(on)

#ifndef MARCO_JOIN
#	define MARCO_JOIN( X, Y ) _MARCO_DO_JOIN( X, Y )
#	define _MARCO_DO_JOIN( X, Y ) _MARCO_DO_JOIN2(X,Y)
#	define _MARCO_DO_JOIN2( X, Y ) X##Y
#endif


////////////////////////////////////////////////////
// meta types
namespace rt
{	namespace _meta_
	{	class	NullType;
		struct	EmptyType{};
}	}

///////////////////////////////////////////////////////////
// Compile-time ASSERT
namespace rt
{	namespace _meta_
	{ template<int i>class _Compile_Time_Assertion{}; }
	template<bool>class Compile_Time_Assertion_Fired;
	template<>class Compile_Time_Assertion_Fired<true>{};

#define ASSERT_STATIC(x)	typedef	rt::_meta_::_Compile_Time_Assertion \
									<sizeof(::rt::Compile_Time_Assertion_Fired<(x)>)> \
									MARCO_JOIN(__ct_assert_def_,__COUNTER__);


////////////////////////////////////////////////////
// __static_if (num bool to symbol existence)
namespace _meta_
{	//Mapping bool to Symbol existence
	template<bool bool_value> struct _bool2Symbol;
		template<> struct _bool2Symbol<true> { static const bool Yes=true ; };
		template<> struct _bool2Symbol<false>{ static const bool No=false  ; };
	typedef rt::_meta_::_bool2Symbol<true>  _bool2Symbol_true;
	typedef rt::_meta_::_bool2Symbol<false> _bool2Symbol_false;
}
#ifdef _DEBUG
#define __static_if(const_bool_value)	typedef rt::_meta_::_bool2Symbol< (const_bool_value) >  \
										MARCO_JOIN(_value_check_,__COUNTER__); \
										__if_exists(rt::_meta_::_bool2Symbol< (const_bool_value) >::Yes)
#else
#define __static_if(const_bool_value)	__if_exists(rt::_meta_::_bool2Symbol< (const_bool_value) >::Yes)
#endif


////////////////////////////////////////////////////
// Min/Max
namespace _meta_
{	
	template<int a,int b,bool a_is_greater=(a>b) >
	struct Max_Value;
		template<int a,int b>
		struct Max_Value<a,b,true>{ static const int Result = a ; };
		template<int a,int b>
		struct Max_Value<a,b,false>{ static const int Result = b ; };

	template<int a,int b,bool a_is_greater=(a>b) >
	struct Min_Value;
		template<UINT a,UINT b>
		struct Min_Value<a,b,true>{ static const int Result = b ; };
		template<UINT a,UINT b>
		struct Min_Value<a,b,false>{ static const int Result = a ; };
}

////////////////////////////////////////////////////
// true if T1 T2 is the same type
template<typename T1, typename T2>
struct IsTypeSame{ static const bool Result = false; };
	template<typename T>
	struct IsTypeSame<T,T>{ static const bool Result = true; };

//////////////////////////////////////////////////
// Remove reference
template<class T>
struct Remove_Reference{ typedef T t_Result; };
	template<class T>
	struct Remove_Reference<T&>{ typedef T t_Result; };

//////////////////////////////////////////////////
// Remove qualifers
template<class T>
struct Remove_Qualifer{ typedef T t_Result; };
	template<class T>
	struct Remove_Qualifer<const T>{ typedef T t_Result; };
	template<class T>
	struct Remove_Qualifer<volatile T>{ typedef T t_Result; };

//////////////////////////////////////////////////
// Remove qualifers and reference
template<class T>
class Remove_QualiferAndRef
{protected:	typedef typename Remove_Reference<T>::t_Result		_NoRef;
 public:	typedef typename Remove_Qualifer<_NoRef>::t_Result	t_Result;
};

} // namespace rt


///////////////////////////////////////////////////////////
// Compiler intrinsics 
#if defined(_MSC_VER) && defined(_MSC_FULL_VER) && (_MSC_FULL_VER >=140050215)
	#define COMPILER_INTRINSICS_IS_POD(T)	__is_pod(T)
	//true if the type is a class or union with no constructor or private or protected 
	//non-static members, no base classes, and no virtual functions
	#define COMPILER_INTRINSICS_HAS_TRIVIAL_CONSTRUCTOR(T) __has_trivial_constructor(T)
	//true if the type has a trivial, compiler-generated constructor, which takes no effect
	#define COMPILER_INTRINSICS_HAS_TRIVIAL_COPY(T) __has_trivial_copy(T)
	//true if the type has a trivial, compiler-generated copy constructor, which equal to memcpy
	#define COMPILER_INTRINSICS_HAS_TRIVIAL_ASSIGN(T) __has_trivial_assign(T)
	//true if the type has a trivial, compiler-generated assignment operator, which equal to memcpy
	#define COMPILER_INTRINSICS_HAS_TRIVIAL_DESTRUCTOR(T) __has_trivial_destructor(T)
	//true if the type has a trivial, compiler-generated destructor, which takes no effect
	#define COMPILER_INTRINSICS_IS_EMPTY(T) __is_empty(T)
	//true if the type has no instance data members

	#define COMPILER_INTRINSICS_IS_CONVERTIBLE(from,to)	__is_convertible_to(from, to)
	//true if the first type can be converted to the second type
	#define COMPILER_INTRINSICS_IS_BASE_OF(base,derived)	__is_base_of(base,derived)
	//true if 'derived' type inherits 'base' type
#else
	#pragma message(">> TypeTraits intrinsics is not supported by current compiler.")
	#pragma message(">>    Optimization for unknown aggregate class is disabled.")
	#define COMPILER_INTRINSICS_NOT_AVAILABLE
	#define COMPILER_INTRINSICS_IS_POD(T)	false
	#define COMPILER_INTRINSICS_HAS_TRIVIAL_CONSTRUCTOR(T) false
	#define COMPILER_INTRINSICS_HAS_TRIVIAL_COPY(T) false
	#define COMPILER_INTRINSICS_HAS_TRIVIAL_ASSIGN(T) false
	#define COMPILER_INTRINSICS_HAS_TRIVIAL_DESTRUCTOR(T) false
	#define COMPILER_INTRINSICS_IS_EMPTY(T) (sizeof(T)==0)

	#define COMPILER_INTRINSICS_IS_CONVERTIBLE(from,to)	(rt::_meta_::_IsConvertible<from, to>::Result)
	#define COMPILER_INTRINSICS_IS_BASE_OF(base,derived)	(rt::_meta_::_IsBaseOf<base, derived>::Result)

	namespace rt
	{	namespace _meta_
		{	////////////////////////////////////////////////////
			// true if convertible
			template<typename t_From, typename t_To>
			class _IsConvertible // t_From is assumed that can be used as function reture type
			{	class t_big{ char __no_name[2]; }; 
				typedef char t_small; 
		#ifdef __INTEL_COMPILER
		#	pragma warning(disable: 1595) //warning by intel compiler
		#endif //warning #1595: non-POD (Plain Old Data) class type passed through ellipsis
				static t_small	MatchType(t_To);
				static t_big	MatchType(...);
				static t_From	from_obj;
			public:	
				static const bool Result = sizeof(MatchType(from_obj))==sizeof(t_small) ;
		#ifdef __INTEL_COMPILER
		#	pragma warning(default: 1684) //warning by intel compiler
		#endif //warning #1595: non-POD (Plain Old Data) class type passed through ellipsis
			};

			template<typename t_Base, typename t_Derived>
			class _IsBaseOf
			{	typedef typename rt::Remove_QualiferAndRef<t_Base>::t_Result t_BaseRaw;
				typedef typename rt::Remove_QualiferAndRef<t_Derived>::t_Result t_DerivedRaw;
			public:
				static const int Result =	rt::_meta_::_IsConvertible<const t_DerivedRaw*,const t_BaseRaw* >::Result && //pointer can convert
								 (!rt::IsTypeSame<t_BaseRaw,t_DerivedRaw >::Result) &&				 //not the same type
								 (!rt::IsTypeSame<const t_Base*,const void * >::Result);			 //t_BaseRaw is not void
			};
		} //namespace _meta_
	} //namespace rt
#endif

