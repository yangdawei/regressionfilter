#pragma once

//////////////////////////////////////////////////////////////////////
// Visual Computing Foundation Classes (VCFC)
//					Jiaping Wang  2006.2
//					e_boris2002@hotmail.com
//
// (C) Copyright Jiaping Wang 2004.
//
// Permission to copy, use, modify, sell and distribute this software
// is granted provided this copyright notice appears in all copies.
// This software is provided "as is" without express or implied
// warranty, and with no claim as to its suitability for any purpose.
//
// Absolutely no warranty!!
//////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////
//  Brief Description
//
//  algorithm.h
//
//  shortest path on graph
//  PrefectStringHash
//
//////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////
// Revise History
//
// Inital version		2005.9		Kunxu
// Using ::rt::Buffer	2006.8.14	Jiaping
//						Replace all naked C++ new with ::rt::Buffer 
// PrefectStringHash	2011.5.23	Jiaping
//
//////////////////////////////////////////////////////////////////////

#include "..\rt\runtime_base.h"
#include "..\rt\compact_vector.h"
#include "..\rt\string_type.h"
#include "..\w32\prog_idct.h"

namespace rt
{

template <typename t_Type>
void FloydWarshallAlgorithm(int nVertices,t_Type* pMat,int step=0)
//   N-to-N shortest path, time complexity O(n^3)
//
//nVertices:    number of vertices of the Graph
//pMat:			Weighted connectivity matrix (logically, nVertices*nVertices)
//step:			memory layout, t_Types per-line (physically, step*nVertices)
//Use Floyd-Warshall Algorithm to compute all pairwise shortest paths of the graph
//Inplace operation, time:O(n^3) space:O(n^2), just for input
{
	if(!step)step=nVertices;
	ASSERT( pMat );
	ASSERT( nVertices<=step );

	int k=0;

	__if_exists(w32::CTimeMeasureDisplay){
	w32::CTimeMeasureDisplay<int> tm(&k,nVertices);
	}

    for(;k<nVertices;k++)
    {
        t_Type  *pKAddr = &pMat[k*step];
        t_Type  *pIKAddr,*pKJAddr,*pIJAddr;
        for(int i=0;i<nVertices;i++)
        {
            pIKAddr = &pMat[i*step+k];
            pKJAddr = pKAddr;
            pIJAddr = &pMat[i*step];

            for(int j=0;j<nVertices;j++)
            {
				t_Type t = *pIKAddr + *pKJAddr;
                *pIJAddr = min(*pIJAddr,t);

                pKJAddr++;
                pIJAddr++;
            }
        }
    }
}



template <typename t_Type>
class Dijsktra
{
public:
	struct DirectionalLink
	{	int vertexFrom;
		int vertexTo;
		t_Type weight;    // MUST >=0

		TYPETRAITS_DECL_IS_AGGREGATE(true)
	};

protected:
	/////////////////////////////////////
	//MinHeap class
	template<typename Type> 
	class HeapNode
	{
	public:
		Type m_data;
		int m_heapIndex;

	public:
		HeapNode():m_heapIndex(-1){}
		HeapNode(Type& t):m_data(t),m_heapIndex(-1){}
		bool operator <= ( HeapNode<Type>& node2 ){ return this->m_data<=node2.m_data; }
		bool operator < ( HeapNode<Type>& node2 ){ return this->m_data<node2.m_data; }
	};

	template<typename Type> class MinHeap
	{
	public:
		typedef typename HeapNode<Type> MinHeadNode;
	public:
		MinHeap(){ m_maxSize = 0; m_currentSize = 0; }
		void SetSizeMax(int sz){ m_maxSize = sz; m_currentSize = 0; m_pDataPointer.SetSize(sz); }
		void Reset(){ m_currentSize = 0; }

		void Insert(MinHeadNode* node)
		{
			ASSERT(m_currentSize<m_maxSize);
			m_pDataPointer[m_currentSize] = node;
			FilterUp(m_currentSize);
			m_currentSize++;
		}

		MinHeadNode* GetMin()
		{
			ASSERT(m_currentSize>0);
			return m_pDataPointer[0];
		}

		MinHeadNode* ExtractMin()
		{
			ASSERT(m_currentSize>0);
			MinHeadNode* rNode = m_pDataPointer[0];
			rNode->m_heapIndex = -1;
			m_pDataPointer[0] = m_pDataPointer[m_currentSize-1];
			m_currentSize--;
			FilterDown(0);
			return rNode;
		}

		void DecreaseKey(MinHeadNode* node)
		{
			ASSERT(node->m_heapIndex!=-1);
			int heapIndex = node->m_heapIndex;

			FilterUp(heapIndex);
		}

	public:
		::rt::Buffer<MinHeadNode*> m_pDataPointer;

		int     m_maxSize;
		int     m_currentSize;

		void FilterUp(int start)
		{
			int j = start;
			int i = (j-1)/2;
			MinHeadNode* temp = m_pDataPointer[j];
			while(j>0)
			{
				if( (*m_pDataPointer[i])<=(*temp))
					break;
				else
				{
					m_pDataPointer[j] = m_pDataPointer[i];
					m_pDataPointer[j]->m_heapIndex = j;

					j = i;
					i = (i-1)/2;
				}
			}
			m_pDataPointer[j] = temp;
			m_pDataPointer[j]->m_heapIndex = j;
		}

		void FilterDown(int start)
		{
			int i = start;
			int j = 2*i+1;
			MinHeadNode* temp = m_pDataPointer[i];
			while(j<m_currentSize)
			{
				if(j<m_currentSize-1&&(*m_pDataPointer[j+1])<(*m_pDataPointer[j]))
					j++;
				if( ((*temp)<=*m_pDataPointer[j]))
					break;
				else
				{
					m_pDataPointer[i] = m_pDataPointer[j];
					m_pDataPointer[i]->m_heapIndex = i;

					i = j;
					j = 2*j+1;
				}
			}
			m_pDataPointer[i] = temp;
			m_pDataPointer[i]->m_heapIndex = i;
		}
	};
	//End of MinHeap class
	/////////////////////////////////////

protected:
    const DirectionalLink*	m_edgeList;     //MUST be sorted by vertexFrom
    int						m_nEdge;
    int						m_nVertex;
	::rt::Buffer<int>		m_EdgeBeginPosOfVertex;
	::rt::Buffer<int>		m_EdgeCountVertex;

    MinHeap<t_Type>			m_minHeap;
	::rt::Buffer<HeapNode<t_Type> > m_heapNode;

public:
	__forceinline int GetNodeEdgeCount(int i) const{ return m_EdgeCountVertex[i]; }
	__forceinline const DirectionalLink* GetNodeEdges(int i) const{ return &m_edgeList[m_EdgeBeginPosOfVertex[i]]; }
	__forceinline int GetNodeCount() const{ return m_nVertex; }
    void SetGraph(const DirectionalLink* list,int edge_count,int vertex_count)	//list MUST be sorted by vertexFrom
	{
		m_minHeap.SetSizeMax(vertex_count);

		m_nVertex = vertex_count;
		VERIFY(m_EdgeBeginPosOfVertex.SetSize(m_nVertex));
		VERIFY(m_EdgeCountVertex.SetSize(m_nVertex));
		VERIFY(m_heapNode.SetSize(m_nVertex));

		m_EdgeBeginPosOfVertex.Zero();
		m_EdgeCountVertex.Zero();

		m_edgeList = list;
		m_nEdge = edge_count;
	    
		int edgeBegin;
		int vertexIndex;

		ASSERT(list[0].vertexFrom==0);
	    
		vertexIndex = 0;
		edgeBegin = 0;
		for(int i=0;i<m_nEdge;i++)
		{	
			ASSERT(list[i].vertexFrom<vertex_count);
			ASSERT(list[i].vertexTo<vertex_count);
			ASSERT(list[i].weight>0);

			if(list[i].vertexFrom==vertexIndex){}
			else
			{
				m_EdgeBeginPosOfVertex[vertexIndex] = edgeBegin;
				m_EdgeCountVertex[vertexIndex] = i - edgeBegin;

				edgeBegin = i;
				ASSERT(vertexIndex < list[i].vertexFrom);
				vertexIndex = list[i].vertexFrom;
			}
		}
		m_EdgeBeginPosOfVertex[vertexIndex] = edgeBegin;
		m_EdgeCountVertex[vertexIndex] = m_nEdge - edgeBegin;
	}

	__forceinline static t_Type InfinitDistance(){ return rt::TypeTraits<t_Type>::MaxVal()/3; }

	void ProcessSingleSource(int iStartVertex,t_Type* outDistance)
	{
		m_minHeap.Reset();
		for(int i=0;i<m_nVertex;i++)
		{
			if(i!=iStartVertex)
				m_heapNode[i].m_data = rt::TypeTraits<t_Type>::MaxVal()/2;
			else
				m_heapNode[i].m_data = 0;
			m_heapNode[i].m_heapIndex = -1;
		}

		for(int i=0;i<m_nVertex;i++)
		{
			m_minHeap.Insert(&m_heapNode[i]);
		}
		for(int i=0;i<m_nVertex;i++)
		{
			HeapNode<t_Type> * node =  m_minHeap.ExtractMin();
			int iVertex = (int)(node - m_heapNode);
			outDistance[iVertex] = node->m_data;
			for(int j=m_EdgeBeginPosOfVertex[iVertex];j<m_EdgeBeginPosOfVertex[iVertex]+m_EdgeCountVertex[iVertex];j++)
			{
				const DirectionalLink& edgeinfo = m_edgeList[j];

				ASSERT(edgeinfo.vertexFrom==iVertex);

				t_Type newDistance = m_heapNode[iVertex].m_data + edgeinfo.weight;
				if( m_heapNode[edgeinfo.vertexTo].m_data > newDistance)
				{
					m_heapNode[edgeinfo.vertexTo].m_data = newDistance;
					m_minHeap.DecreaseKey(&m_heapNode[edgeinfo.vertexTo]);
				}
			}
		}
	}

    void ProcessAllPairs(t_Type* DistanceMatrix,int step=0)
	{
		if(step){}else{ step=m_nVertex; }

		int i=0;
		w32::CTimeMeasureDisplay<int> tm(&i,m_nVertex);
		for(;i<m_nVertex;i++)
		{
			ProcessSingleSource(i,DistanceMatrix+i*step);
		}
	}

	template<class T>
	void ProcessAllPairs(T& DistanceMatrix)
	{	ASSERT(DistanceMatrix.GetWidth() == m_nVertex);
		ASSERT(DistanceMatrix.GetHeight() == m_nVertex);
		int i=0;
		w32::CTimeMeasureDisplay<int> tm(&i,m_nVertex);
		for(;i<m_nVertex;i++)
		{
			ProcessSingleSource(i,DistanceMatrix.GetLine(i));
		}
	}
};



////////////////////////////////////////////////////////////////
// StringPrefectHashMap.
// for massive time-critial query only, updating is very slow
// if ignore_nonmapped_key == false, the query return null_value for nonmapped key
// if ignore_nonmapped_key == true, the return is undefined for nonmapped key
//    the query is more fast and the hash space will be more compact.
template<typename t_Char, typename t_Value, bool ignore_nonmapped_key = true>		
class StringPrefectHashMap
{	struct _Entry
	{	__static_if(!ignore_nonmapped_key){	rt::StringT<t_Char>	key; }
		t_Value		value;
	};
protected:
	DYNTYPE_FUNC int	Hash(const rt::StringT_Ref<t_Char>& key) const
						{	int hash = 0xdeadbeef;
							for(UINT p=0;p<key.GetLength();p++)
								hash = ((hash ^ key[p]) * m_HashMagic);
							return ((UINT)hash) % m_HashSpaceSize;
						}
	DYNTYPE_FUNC BOOL	IsKeyEqual(const rt::StringT_Ref<t_Char> &key, const rt::StringT_Ref<t_Char>& x) const
						{	return key == x;
						}
protected:
	rt::Buffer<_Entry>	m_HashSpace;
	int					m_HashSpaceSize;
	int					m_HashMagic;
	t_Value				m_NullValue;
	BOOL	_CheckHashSpace(rt::Buffer<int> &hash_space, UINT count, const rt::StringT_Ref<t_Char>* keys, const t_Value* values)
	{	ASSERT(hash_space.GetSize() == m_HashSpaceSize);
		hash_space.Set(-1);
		for(UINT i=0;i<count;i++)
		{	int hash = Hash(keys[i]);
			BOOL no_conflict;
			__static_if(ignore_nonmapped_key){ no_conflict = hash_space[hash] <0 || values[i] == values[hash_space[hash]]; }
			__static_if(!ignore_nonmapped_key){ no_conflict = hash_space[hash] <0; }
			if(no_conflict){ hash_space[hash] = i; }
			else
			{	BOOL duplicated;	// conflict !!
				__static_if(ignore_nonmapped_key){ duplicated = (keys[i] == keys[hash_space[hash]]); }
				__static_if(!ignore_nonmapped_key){ duplicated = (keys[i] == keys[hash_space[hash]]) && !(values[i] == values[hash_space[hash]]); }
				if(duplicated)		// duplicated key
				{	m_HashSpace.SetSize(); m_HashSpaceSize = 0;
					DuplicatedKeyIndex1 = i;	DuplicatedKeyIndex2 = hash_space[hash];
					hash_space.SetSize(0);
					return FALSE;
				}
				return FALSE;
			}	
		}
		return TRUE;
	}
public:
	int					DuplicatedKeyIndex1;
	int					DuplicatedKeyIndex2;
	int					GetHashMagic() const { return m_HashMagic; }
	int					GetHashSpaceSize() const { return m_HashSpaceSize; }
public:
	BOOL	Build(t_Value null_value, UINT count = 0, const rt::StringT_Ref<t_Char>* keys = NULL, const t_Value* values = NULL, int proposal_hashsize = 0, int proposal_magic = 0)
	{	ASSERT_STATIC(sizeof(t_Char) < 3);
		COMPILER_INTRINSICS_IS_POD(t_Value);
		DuplicatedKeyIndex1 = DuplicatedKeyIndex2 = -1;
		m_NullValue = null_value;
		m_HashSpace.SetSize();
		if(count == 0){	m_HashSpaceSize = 0; return TRUE; }
		rt::Buffer<int> hash_space;
		if(proposal_hashsize)
		{	// Try proposal
			m_HashSpaceSize = proposal_hashsize;
			VERIFY(hash_space.SetSize(m_HashSpaceSize));
			if(_CheckHashSpace(hash_space, count, keys, values))
			{	m_HashSpace.SetSize(m_HashSpaceSize);	// done !!
				goto TRIES_DONE;
			}
		}
		int hashspace_max = max(64,count*10);
		for(m_HashSpaceSize = count; m_HashSpaceSize < hashspace_max; m_HashSpaceSize++)
		{	// try different sizes of hash space
			VERIFY(hash_space.SetSize(m_HashSpaceSize));
			for(int mg = 1; mg < 256; mg++)
			{	// try different hash magic
				__static_if(sizeof(t_Char) == 1){ m_HashMagic = mg; }
				__static_if(sizeof(t_Char) == 2){ m_HashMagic = mg + (mg<<8); }
				if(!_CheckHashSpace(hash_space, count, keys, values))
				{	if(hash_space.GetSize()){ goto TryNextMagic; }
					else { return FALSE; }// duplicated key 
				}
				m_HashSpace.SetSize(m_HashSpaceSize);	// done !!
				goto TRIES_DONE;
TryNextMagic:	continue;
		}	}
TRIES_DONE:
		if(m_HashSpace.GetSize())
		{
			for(int i=0;i<m_HashSpaceSize;i++)
			{	if(hash_space[i]>=0)
				{	m_HashSpace[i].value = values[hash_space[i]];
					__static_if(!ignore_nonmapped_key){ m_HashSpace[i].key = keys[hash_space[i]]; }
				}
				else m_HashSpace[i].value = m_NullValue;
			}
			return TRUE;
		}
		else return FALSE;
	}
	DYNTYPE_FUNC t_Value operator [](const rt::StringT_Ref<t_Char>& x) const 
	{	if(m_HashSpaceSize)
		{	__static_if(ignore_nonmapped_key){ return m_HashSpace[Hash(x)].value; }
			__static_if(!ignore_nonmapped_key)
			{	int hash = Hash(x);
				if(	m_HashSpace[hash].value != m_NullValue &&
					IsKeyEqual(m_HashSpace[hash].key, x)
				)return m_HashSpace[hash].value;
			}
		}
		return m_NullValue;
	}
};

template<typename t_Char, typename t_Value>
class StringPrefixPrefectHashMap:protected StringPrefectHashMap<t_Char,t_Value,false>
{	BOOL	_Prefix_Lengths[256];
	int		_LengthMax;
public:
	int		GetHashMagic() const { return m_HashMagic; }
	int		GetHashSpaceSize() const { return m_HashSpaceSize; }
	BOOL	Build(t_Value null_value, UINT count = 0, const rt::StringT_Ref<t_Char>* keys = NULL, const t_Value* values = NULL, int proposal_hashsize = 0, int proposal_magic = 0)
	{	ZeroMemory(_Prefix_Lengths,sizeof(_Prefix_Lengths));
		_LengthMax = -1;
		for(UINT i=0;i<count;i++)
		{	if(keys[i].GetLength() >= 256)return FALSE;
			_LengthMax = max(_LengthMax,(int)keys[i].GetLength());
			_Prefix_Lengths[keys[i].GetLength()] = TRUE;
		}
		return __super::Build(null_value, count, keys, values,proposal_hashsize,proposal_magic);
	}
	DYNTYPE_FUNC t_Value operator [](const rt::StringT_Ref<t_Char>& x) const 
	{	if(m_HashSpaceSize)
		{	int hash = 0xdeadbeef;
			int len = max((int)x.GetLength(),_LengthMax);
			t_Value ret = m_NullValue;
			for(int i=0;i<=len;i++)
			{	int hh = ((UINT)hash) % m_HashSpaceSize;
				if(	_Prefix_Lengths[i] && 
					m_HashSpace[hh].value != m_NullValue &&
					IsKeyEqual(m_HashSpace[hh].key, rt::StringT_Ref<t_Char>(x.Begin(),i))
				)
				{	ret = m_HashSpace[hh].value;
				}
				hash = ((hash ^ x[i]) * m_HashMagic);
			}
			return ret;
		}
		return m_NullValue;
	}
};

} // namespace rt
