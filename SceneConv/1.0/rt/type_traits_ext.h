#pragma once

//////////////////////////////////////////////////////////////////////
// Visual Computing Foundation Classes (VCFC)
//					Jiaping Wang  2006.2
//					e_boris2002@hotmail.com
//
// (C) Copyright Jiaping Wang 2004.
//
// Permission to copy, use, modify, sell and distribute this software
// is granted provided this copyright notice appears in all copies.
// This software is provided "as is" without express or implied
// warranty, and with no claim as to its suitability for any purpose.
//
// Absolutly no garanty!!
//////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////
//  Brief Description
//
//  type_traits.h
//
//  The type traits for basic types and customizable type traits framework
//
//  Provides: 
//	TypeTraits: Basic Types, Array, pointer, const
//
//  Option Macros:
//	#define EPSLON_RELAXATION num
//			The relaxation scale for float-point number comparison
//			EPSILON=EPSLON_RELAXATION*FLT_EPSLON ( num=10 by default)
//
//////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////
// Revise History
//
// Inital version	2006.2.23		Jiaping
//					Type traits and ResultType traits were removed
//                  from smallmath.h and placed in this file.
// Output format	2006.8.15		Jiaping
//					Add output format spec for built-in numeric. Affect
//					most TextDump functions
// Add IsZero		2006.9.17		Jiaping
//
//////////////////////////////////////////////////////////////////////


#include "type_traits.h"
#include "member_visitor.h"

// use in global scope
#define TYPETRAITS_DECL_STRING_CLASS(_extended_typeid)			\
		namespace rt{namespace _meta_{							\
			template<typename T>								\
			struct _IsStringClass_Internal<T,_extended_typeid>	\
			{ static const bool Result = true; };				\
		}} /* _meta_/rt */										\



namespace rt
{

namespace _ExtendedType
{

template<typename T, UINT _extended_type_id>
	class TypeTraitsEx;

} //namespace _ExtendedType

namespace _meta_
{

template<typename T, UINT _extended_typeid>			// has both extended TypeTraits and type agent
class _TypeTraits_Default<T,_extended_typeid,true>
	:public rt::TypeTraits<_meta_::_TypeAgent<T> >{};

template<typename T, UINT _extended_typeid>
class _TypeTraits_Default<T,_extended_typeid,false>	// has extended TypeTraits and has't type agent
	:public _ExtendedType::TypeTraitsEx<T, _extended_typeid>{};

template<typename T, UINT _extended_type_id>		// Agent of TypeTraitsEx class is not allowed
struct _TypeAgent<_ExtendedType::TypeTraitsEx<T,_extended_type_id>>{ ASSERT_STATIC(0); };

template<typename T, UINT extend_typeid = rt::TypeTraits<T>::ExtendTypeid>
struct _IsStringClass_Internal{ static const bool Result = false; };
	
template<UINT extened_type_id>class _ExtenedTypeId_Struct{ DWORD a[extened_type_id]; };
} // namespace _meta_

template<typename T, bool is_extendTT = ((rt::TypeTraits<T>::Typeid)>rt::_typeid_extend_type_min) >
struct IsStringClass{ static const bool Result = false ; };
	template<typename T> struct IsStringClass<T,true>
	{ static const bool Result = _meta_::_IsStringClass_Internal<T>::Result ; };

} // namespace rt
