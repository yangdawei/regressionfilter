#pragma once

//////////////////////////////////////////////////////////////////////
// Visual Computing Foundation Classes (VCFC)
//					Jiaping Wang  2006.2
//					e_boris2002@hotmail.com
//
// (C) Copyright Jiaping Wang 2004.
//
// Permission to copy, use, modify, sell and distribute this software
// is granted provided this copyright notice appears in all copies.
// This software is provided "as is" without express or implied
// warranty, and with no claim as to its suitability for any purpose.
//
// Absolutly no garanty!!
//////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////
//  Brief Description
//
//  type_traits_stl.h
//
//  TypeTraits for stl classes
//
//////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////
// Revise History
//
// Inital version	2007.10.30		Jiaping
//
//////////////////////////////////////////////////////////////////////

#include "type_traits_ext.h"


namespace rt{
namespace _meta_{
	__forceinline LPCTSTR _text_std_(){ return _T("std::"); }
}} // namespace _meta_ / namespace rt

//////////////////////////////////////////////////////////////////////
// TypeTraits for STL containers
// 
#define __STL_CONTAINER_TYPENAME(container_name)							\
		template<class ostream>												\
		static void TypeName(ostream & outstr)								\
			{	outstr<<_meta_::_text_std_()<<_T(#container_name)<<_T('<');	\
				rt::TypeTraits<t_Ele>::TypeName(outstr); outstr<<_T('>'); 	\
			}																\

#define __STL_CONTAINER_BODY(container_name,is_array)						\
		typedef t_Ele										t_Element;		\
		static const int Typeid = _typeid_stl;								\
		static const int ExtendTypeid = _typeid_stl_##container_name;		\
		static const bool IsAggregate = false;								\
		static const bool IsNumeric = false;								\
		static const bool IsArray = is_array;								\
		static const bool IsImage = false;									\
		static const int Length = TYPETRAITS_SIZE_UNKNOWN;					\
	public:																	\
		struct text_output_spec												\
		{	static const int width_total = TYPETRAITS_SIZE_UNKNOWN;			\
			static const int width_after_point = TYPETRAITS_SIZE_UNKNOWN;	\
	};	};																	\
	namespace _ExtendedType{												\
	template<typename T> class TypeTraitsEx<T, _typeid_stl_##container_name >\
		:public ::rt::TypeTraits<T>{};										\
	}} /* namespace _ExtendedType/rt */										\


#define __STL_CONTAINER1(container_name,is_array,etid_minor)										\
	namespace std{ template<class,class> class container_name; }									\
	namespace rt{																					\
	static const int _typeid_stl_##container_name = (_typeid_stl<<16)+etid_minor;					\
	namespace _ExtendedType{	template<class T, class T2>											\
								::rt::_meta_::_ExtenedTypeId_Struct<_typeid_stl_##container_name>	\
									_DefineExtendedTypeId(std::container_name<T, T2>* p);			\
	} /* _ExtendedType */																			\
	template<class t_Ele, class t_Allocator>														\
	class TypeTraits< std::container_name<t_Ele,t_Allocator> >										\
	{public:																						\
		typedef std::container_name<typename TypeTraits<t_Ele>::t_Accum	,t_Allocator>	t_Accum;	\
		typedef std::container_name<typename TypeTraits<t_Ele>::t_Val	,t_Allocator>	t_Val;		\
		typedef std::container_name<typename TypeTraits<t_Ele>::t_Signed,t_Allocator>	t_Signed;	\
	__STL_CONTAINER_TYPENAME(container_name)														\
	__STL_CONTAINER_BODY(container_name,is_array)													\

#define __STL_CONTAINER2(container_name,is_array,etid_minor)												\
	namespace std{ template<class,class,class> class container_name; }										\
	namespace rt{																							\
	static const int _typeid_stl_##container_name = (_typeid_stl<<16)+etid_minor;							\
	namespace _ExtendedType{	template<class T, class T2, class T3>										\
								::rt::_meta_::_ExtenedTypeId_Struct<_typeid_stl_##container_name>			\
									_DefineExtendedTypeId(std::container_name<T, T2, T3>* p);				\
	} /* _ExtendedType */																					\
	template<class t_Ele, class Traits, class t_Allocator>													\
	class TypeTraits< std::container_name<t_Ele,Traits,t_Allocator> >										\
	{public:																								\
		typedef std::container_name<typename TypeTraits<t_Ele>::t_Accum	,Traits,t_Allocator>	t_Accum;	\
		typedef std::container_name<typename TypeTraits<t_Ele>::t_Val	,Traits,t_Allocator>	t_Val;		\
		typedef std::container_name<typename TypeTraits<t_Ele>::t_Signed,Traits,t_Allocator>	t_Signed;	\
	__STL_CONTAINER_TYPENAME(container_name)																\
	__STL_CONTAINER_BODY(container_name,is_array)															\

#define __STL_CONTAINER3(container_name,is_array,_namespace,etid_minor)													\
	namespace _namespace{ template<class,class,class,class> class container_name; }										\
	namespace rt{																										\
	static const int _typeid_stl_##container_name = (_typeid_stl<<16)+etid_minor;										\
	namespace _ExtendedType{	template<class T, class T2, class T3, class T4>											\
								::rt::_meta_::_ExtenedTypeId_Struct<_typeid_stl_##container_name>						\
									_DefineExtendedTypeId(_namespace::container_name<T, T2, T3, T4>* p);				\
	} /* _ExtendedType */																								\
	template<class t_Key, class t_Ele, class Traits, class t_Allocator>													\
	class TypeTraits< _namespace::container_name<t_Key,t_Ele,Traits,t_Allocator> >										\
	{public:																											\
		typedef _namespace::container_name<t_Key,typename TypeTraits<t_Ele>::t_Accum  ,Traits,t_Allocator>	t_Accum;	\
		typedef _namespace::container_name<t_Key,typename TypeTraits<t_Ele>::t_Val    ,Traits,t_Allocator>	t_Val;		\
		typedef _namespace::container_name<t_Key,typename TypeTraits<t_Ele>::t_Signed ,Traits,t_Allocator>	t_Signed;	\
		template<class ostream>																							\
		static void TypeName(ostream & outstr)																			\
			{	outstr<<_T(#_namespace)<<_T(':')<<_T(':')<<_T(#container_name)<<_T('<');								\
				rt::TypeTraits<t_Key>::TypeName(outstr); outstr<<_T(',')<<_T(' ');										\
				rt::TypeTraits<t_Ele>::TypeName(outstr); outstr<<_T('>'); 												\
			}																											\
	__STL_CONTAINER_BODY(container_name,is_array)																		\


__STL_CONTAINER1(vector					,true		,1)
__STL_CONTAINER1(list					,false		,2)
__STL_CONTAINER1(deque					,true		,3)
__STL_CONTAINER1(stack					,false		,4)
__STL_CONTAINER1(queue					,false		,5)

__STL_CONTAINER2(priority_queue			,false		,6)
__STL_CONTAINER2(set					,false		,7)
__STL_CONTAINER2(multiset				,false		,8)
__STL_CONTAINER2(hash_set				,false		,9)
__STL_CONTAINER2(hash_multiset			,false		,10)
__STL_CONTAINER2(basic_string			,true		,11)

__STL_CONTAINER3(hash_map				,false,	stdext	,12)
__STL_CONTAINER3(hash_multimap			,false,	stdext	,13)
__STL_CONTAINER3(map					,false,	std		,14)
__STL_CONTAINER3(multimap				,false,	std		,15)

#undef __STL_CONTAINER_BODY
#undef __STL_CONTAINER1
#undef __STL_CONTAINER2
#undef __STL_CONTAINER3


TYPETRAITS_DECL_STRING_CLASS(_typeid_stl_basic_string)