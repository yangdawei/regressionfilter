#pragma once

//////////////////////////////////////////////////////////////////////
// Visual Computing Foundation Classes (VCFC)
//					Jiaping Wang  2006.2
//					e_boris2002@hotmail.com
//
// (C) Copyright Jiaping Wang 2004.
//
// Permission to copy, use, modify, sell and distribute this software
// is granted provided this copyright notice appears in all copies.
// This software is provided "as is" without express or implied
// warranty, and with no claim as to its suitability for any purpose.
//
// Absolutely no warranty!!
//////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////
//  Brief Description
//
//  runtime_base.h
//
//  The basic underlying runtime support for VCFC library for both win32
//  and win64 platform
//
//  Provides: 
//	1. Basic Types: LPCBYTE LPCWORD LPCDWORD QWORD LPQWORD LPCQWORD
//	2. Constants:	EPSILON PI FLT_PINF FLT_NINF
//	3. Misc macro:	sizeofArray _CheckFireBreak
//	4. ASSERTs:		ASSERT VERIFY ASSERTH VERIFYH
//	5. Safe delete: _SafeDelX
//	6. Aligned allocation: 
//					_Malloc32AL _SafeFree32AL
//					rt::Malloc32AL rt::Free32AL rt::EnlargeTo32AL
//  7. Misc template functions: 
//					Sqr Cubic IsInRange_xx MaxPower2Within
//	8. Optimized Swap: 
//					directly memory exchange. no temporary object, 
//					no ctor and "=" needed
//	9. Aligned object new: 
//					_New32AL _SafeDel32AL 
//					rt::New32AL  rt::Del32AL
// 10. reflection class
//					
//
//  Option Macros:
//	#define RTB_REPORT_MEMORY_ACTIVITY
//			Enable text logging for every memory allocation and free
//  #define RTB_FORCE_BUFFER_OVERRUN_DETECTION
//			Enable buffer overrun detection of _Free32AL in release
//			version. Take no effect in debug version, always enabled.
//	#define RTB_MEMORY_LEAK_ADDRESS (0x12345678)
//			Enable address alert when allocation, help to locate memory
//			leak bugs. 
//	#define EPSLON_RELAXATION num
//			The relaxation scale for float-point num comparison
//			EPSILON=EPSLON_RELAXATION*FLT_EPSILON ( num=10 by default)
//////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////
// Revise History
//
// Inital version	2006.2.19		Jiaping
//                  Add: Basic Types,Constants,Misc macro,ASSERTs,
//					Safe delete,Aligned allocation,Optimized Swap
//					Misc template functions
// v0.1				2006.3.15		Jiaping
//					Add: 32byte Aligned Object new and delete
// v0.2				2006.8.29		Jiaping
//					Add: reflection class
//
//////////////////////////////////////////////////////////////////////


#include <iostream>
#include <tchar.h>
#include <stdio.h>
#include <time.h>


#include "..\w32\win32_ver.h"

#ifndef _SIZE_T_DEFINED
	#include <stddef.h>		// definition of size_t
#endif

#ifdef _DEBUG
	#include <crtdbg.h>
#endif

//#if (defined(_DEBUG)) && !(defined(RTB_FORCE_BUFFER_OVERRUN_DETECTION))
//	#define RTB_FORCE_BUFFER_OVERRUN_DETECTION
//#endif

#define DYNTYPE_FUNC __forceinline

//////////////////////////////////////////////////////////////////
#if (!defined _NATIVE_WCHAR_T_DEFINED) && (defined UNICODE)
#error	built-in wchar_t type is disabled, please enable as following: \
		project's Property Pages dialog->[C/C++]->[Language]->[Treat wchar_t as Built-in Type]
#endif

				
///////////////////////////////////////////////////////
// Warning style
//#pragma warning(disable: 4244 4522 4311 4312 4996)
#pragma warning(disable: 4819) //warning C4819: The file contains a character that cannot be represented in the current code page (936)
#pragma warning(disable: 4996) //warning C4996: 'xxx' was declared deprecated
#pragma warning(disable: 4616) //warning C4996: 'xxx' was declared deprecated
#pragma warning(disable: 4311) //warning C4311: 'type cast' : pointer truncation
#pragma warning(error : 4715 4700) //not all control paths return a value


///////////////////////////////////////////////////////
// Enable frequently used intrinsic function
#pragma intrinsic(abs,labs)
#pragma intrinsic(strcmp,strcpy,strcat,strlen)
#pragma intrinsic(memcmp,memcpy,memset)


///////////////////////////////////////////////////////
// Basic data types
typedef const BYTE*			LPCBYTE;
typedef const WORD*			LPCWORD;
typedef const DWORD*		LPCDWORD;
typedef ULONGLONG			QWORD;
typedef QWORD*				LPQWORD;
typedef const QWORD*		LPCQWORD;
typedef const float *		LPCFLOAT;
typedef float *				LPFLOAT;
///////////////////////////////////////////////////////


///////////////////////////////////////////////////////
// Freqently used constants
#ifndef EPSLON_RELAXATION
	#define EPSLON_RELAXATION 20
#endif

#ifndef EPSILON
	#define EPSILON	((float)(FLT_EPSILON*EPSLON_RELAXATION))
#endif

#ifndef PI
	#define PI (3.14159265358979323846)
#endif

#ifndef MAKE_FOURCC
	#define MAKE_FOURCC(a,b,c,d)		(#@a|(#@b<<8)|(#@c<<16)|(#@d<<24))
	#define MAKE_FOURCC3(a,b,c)			(#@a|(#@b<<8)|(#@c<<16))
	#define MAKE_FOURCC2(a,b)			(#@a|(#@b<<8))
#endif
///////////////////////////////////////////////////////


///////////////////////////////////////////////////////
// Misc Helper macro
#define sizeofArray(array_name) (sizeof(array_name)/sizeof(array_name[0]))
#ifdef _WIN64
	#define _CheckFireBreak __debugbreak()
#else
	#define _CheckFireBreak __asm{ int 3 }
#endif

#ifndef MARCO_JOIN
#	define MARCO_JOIN( X, Y ) _MACRO_DO_JOIN( X, Y )
#	define _MACRO_DO_JOIN( X, Y ) _MACRO_DO_JOIN2(X,Y)
#	define _MACRO_DO_JOIN2( X, Y ) X##Y
#endif

///////////////////////////////////////////////////////


///////////////////////////////////////////////////////
// Debugging and error handling
#ifndef ASSERT
	#ifdef _DEBUG
		#define ASSERT(x) _ASSERT(x)
	#else
		#define ASSERT(x) {}
	#endif
#endif

#ifndef VERIFY
	#ifdef _DEBUG
		#define VERIFY(x) ASSERT(x)
	#else
		#define VERIFY(x) (x)
	#endif
#endif

#define ASSERTH(re) ASSERT(SUCCEEDED(re))
#define VERIFYH(re) VERIFY(SUCCEEDED(re))
///////////////////////////////////////////////////////


///////////////////////////////////////////////////////
// Runtime diagnosis
#ifdef _DEBUG
	#define _CheckHeap	ASSERT(_CrtCheckMemory())
#else
	#define _CheckHeap	{}
#endif

#ifdef _DEBUG
	#include <float.h>
	#define ASSERT_FLOAT(x)	{if(_fpclass(x) <=_FPCLASS_NINF || _fpclass(x)==_FPCLASS_PINF)ASSERT(0);}
	#define VERIFY_FLOAT(x)	ASSERT_FLOAT(x)
#else
	#define ASSERT_FLOAT(x)	while(0)
	#define VERIFY_FLOAT(x)	(x)
#endif

#define ASSERT_PTR(p,size_in_byte)			ASSERT(!::IsBadReadPtr(p,size_in_byte));
#define ASSERT_ARRAY(p,element_count)		ASSERT(!::IsBadReadPtr(p,(element_count)*sizeof(p[0])));
///////////////////////////////////////////////////////


///////////////////////////////////////////////////////
// Some constant 
namespace rt
{
#ifdef UNICODE
	#define _STD_OUT		(std::wcout)
	typedef std::wostream	t_ostream;
#else
	#define _STD_OUT		(std::cout)
	typedef std::ostream	t_ostream;
#endif

static const double sc_Exponent	= 2.71828182845904523536;
static const double sc_Ln10		= 2.30258509299404568402;
static const double sc_Ln2		= 0.69314718055994530942;
static const double sc_Sqrt2	= 1.41421356237309504880;
static const double sc_Sqrt3	= 1.44224957030740838232;
static const double sc_Pi		= 3.14159265358979323846;

static const float	sc_Brightness_RGB[3] = {0.299f,0.587f,0.114f};
} // namespace rt

///////////////////////////////////////////////////////
// Reflect Model
namespace rt
{
struct _reflection{ virtual UINT OnReflect(LPVOID param = 0, UINT msg = 0) = 0; };
struct _stream
{
	static const int Seek_Begin			= FILE_BEGIN;
	static const int Seek_Current		= FILE_CURRENT;
	static const int Seek_End			= FILE_END;

	virtual UINT		Read(LPVOID lpBuf,UINT nCount) = 0;
	virtual UINT		Write(LPCVOID lpBuf,UINT nCount) = 0;
	virtual LONGLONG	Seek(LONGLONG offset,UINT nFrom = Seek_Begin) = 0; // return ULLONG_MAX for failure.
	virtual LONGLONG	GetLength() const = 0;
	virtual BOOL		SetLength(ULONGLONG sz) = 0;
};
}


///////////////////////////////////////////////////////
// Safe delete Objects
#define _SafeDel(x)			{ if(x){delete x; x=0;} }
#define _SafeDelArray(x)	{ if(x){delete [] x; x=0;} }
#define _SafeRelease(x)		{ if(x){(x)->Release(); x=0;} }
#define _SafeCoFree(x)		{ if(x){::CoTaskMemFree(x); x=0;} }
#define _SafeDelGDI(x)		{ if(x){::DeleteObject(x); x=0;} }
#define _SafeFreeBSTR(x)	{ if(x){::SysFreeString(x); x=0;} }
#ifdef _WINBASE_
#define _SafeCloseHandle(x) { if((x)!=INVALID_HANDLE_VALUE){ ::CloseHandle(x); x=INVALID_HANDLE_VALUE; } }
#endif
///////////////////////////////////////////////////////

///////////////////////////////////////////////////////
//CoTaskMem
#define _CoNewStringA(from,to) (to = CoTaskMemAlloc(strlen(from)+1),strcpy(to,from))
#define _CoNewStringW(from,to) (to = CoTaskMemAlloc(strlen(from)+2),wcscpy(to,from))
#define _CoNew(x)	CoTaskMemAlloc(sizeof(x))
#define _CoDelete(x) CoTaskMemFree(x)

//////////////////////////////////////////////////////////
// 32byte Aligned Memory allocation and free 
//
// 1. use 32byte-Aligned Memory is optimized for cache hitting rate on CPUs of PIII and above
// 2. Add prefix and suffix bytes to allocated memory block to detect buffer overflow

///////////////////////////////////////////////////////////////////////////////////////////////////
//// 32byte Aligned Memory allocation layout
//// additional 32+1+4 bytes (or 32+1+8 bytes in Win64) is added to every memory block
//// 
////                 |----  Offset  ----|
//// /-- LEN-BYTE --\/-- Prefix bytes -\#/---------- User block -----------\/-- Suffix bytes --\
//// UUUUUUUUUUUUUUUUU ......... UUUUUUUUUUUUUUUUUUUUUUUUUU .... UUUUUUUUUUUUUUUUUUUUUUUUUUUUUUU
//// ^                                  ^^
//// |Original                          ||
//// \allocated             Saved Offset/\First aligned address after LEN-BYTE, the return 
////
//// 1. LEN-BYTE (size_t) indicates the size of User block
//// 
//// 2. If the p is returned by malloc and subtracted by 8, then &p[32-p%32] is the output address
////    and the offset=32-p%32 is saved at p[offset-1] as one byte the legal range of offset is from
////    1 to 32, and this value will be checked when free and used to calculate the original address
//// 
//// 3. The Prefix bytes (size=offset-1) and Suffix bytes (size=33-offset) will be check to ensure 
////    no boundary memory written occured (buffer overflow)
/////////////////////////////////////////////////////////////////////////////////////////////////////

#define _Malloc32AL(type,co)	((type*)rt::Malloc32AL(sizeof(type)*(co)))
#define _SafeFree32AL(ptr)		{ rt::Free32AL(ptr); ptr=0; }

namespace rt
{
#ifdef __INTEL_COMPILER
#	pragma warning(disable: 1684) //warning by intel compiler
#endif //warning #1684: conversion from pointer to same-sized integral type (potential portability problem)

template<typename T>
__forceinline T EnlargeTo32AL(T num)
{ return (((num) + 0x7)&(~((T)0x7))); }

__forceinline LPVOID Malloc32AL(size_t size,BOOL allow_fail = FALSE)   //size in byte
{
#pragma warning(disable:4244 4127)	// conversion from 'int' to 'BYTE', possible loss of data
									// conditional expression is constant
RetryAlloc:
	LPBYTE p;

#ifdef RTB_FORCE_BUFFER_OVERRUN_DETECTION
	//32 for alignment, 1 for suffix, 4(or 8) for LEN-BYTE
	ASSERT(size < size+32+1+sizeof(size_t));
	p = new(std::nothrow) BYTE[size+32+1+sizeof(size_t)]; 
#else
	ASSERT(size < size+32);
	p = new(std::nothrow) BYTE[size+32]; 
#endif

#if defined RTB_MEMORY_LEAK_ADDRESS
	if( p == (LPBYTE)RTB_MEMORY_LEAK_ADDRESS )
	{	_CheckFireBreak;
		//Leaked memory block is located, check [Call Stack] for high-level code
	}
#endif

	if(p)
	{	
#ifdef RTB_FORCE_BUFFER_OVERRUN_DETECTION
		//Record user block size for determining the high boundry of the memory block
		*((size_t*)p) = size; //size of User block
		p += sizeof(size_t);
#endif

		int offset;
		offset = 32 - (int)( ((UINT)p) & 0x1f);	// align to 32
		p[offset-1] = offset;	//Store offset

#if defined RTB_MEMORY_LEAK_ADDRESS
	if( &p[offset] == (LPBYTE)RTB_MEMORY_LEAK_ADDRESS )
	{	_CheckFireBreak;
		//abnormal used memory block is located, check [Call Stack] for high-level code
	}
#endif

#ifdef RTB_FORCE_BUFFER_OVERRUN_DETECTION
		//Addtional guard bytes is set for detecting broundry-overwriting, 
		//which will be checked when memory free. 
		int i;
		for(i=0;i<offset-1;i++)p[i] = 0x61+i;				//Set prefix bytes to 0x61+i = "abcde..."
		for(i=0;i<33-offset;i++)p[offset+size+i] = 0x41+i;	//Set suffix bytes to 0x41+i = "ABCDE..."
#endif

#ifdef RTB_REPORT_MEMORY_ACTIVITY
		_CheckDump("User block [0x"<<((LPVOID)&p[offset])<<"] allocated.\n");
#endif

		return &p[offset];
	}
	else  //Allocation failed
	{
		printf("\n\nOut of memory: Failed to allocate %dKB memory block!\7\n",(size+41)/1024);
		ASSERT(0);

#ifdef _WINUSER_
		{
			TCHAR msg[200];
			wsprintf(msg,_T("Failed to allocate %dKB memory block!\15\12\15\12 Abort: Launch debugger\15\12 Retry: Try allocation again\15\12 Ignore: Stop allocation"),(size+41)/1024);
			if(!allow_fail)
			{	int idret = ::MessageBox(NULL,msg,_T("Out of memory"),MB_ABORTRETRYIGNORE|MB_ICONSTOP);
				if( idret == IDRETRY )
					goto RetryAlloc;
				else if(idret == IDABORT )
					DebugBreak();
			}
		}
#endif
		return NULL;
	}
}

__forceinline void Free32AL(LPVOID ptr_in)
{	if( ptr_in ) //NULL pointer is allowed but take no effect
	{	
#ifdef RTB_REPORT_MEMORY_ACTIVITY
		_CheckDump(_T("User block [0x")<<ptr_in<<_T("] try to free.\n"));
#endif

		LPBYTE ptr = reinterpret_cast<LPBYTE>(ptr_in);
		ASSERT_PTR(&ptr[-1],1); //invalid pointer provided

		bool Prefix_Err_Detected = FALSE; 
#ifdef RTB_FORCE_BUFFER_OVERRUN_DETECTION
		bool Suffix_Err_Detected = FALSE;
		size_t user_block_size = ((size_t)(-1));
#endif
		int offset = ptr[-1];
		if( offset <=32 && offset>0 )
		{
#ifdef RTB_FORCE_BUFFER_OVERRUN_DETECTION
			//Detect buffer overrun
			ASSERT_PTR(&ptr[-offset-sizeof(size_t)],1+offset+sizeof(size_t));
			user_block_size = *((size_t*)&ptr[-offset-sizeof(size_t)]);
			if( !::IsBadReadPtr( &ptr[-offset-sizeof(size_t)], 
								 user_block_size+sizeof(size_t)+33 ) )
			{	int i; 
				for(i=0;i<offset-1;i++)
				{// check prefix bytes
					if( ptr[-(offset-i)] == 0x61+i ){}
					else{ Prefix_Err_Detected = TRUE; break; }
				}
				for(i=0;i<33-offset;i++)
				{// check suffix bytes
					if( ptr[user_block_size+i] == 0x41+i ){}
					else{ Suffix_Err_Detected = TRUE; break; }
				}

				if( !Prefix_Err_Detected && !Suffix_Err_Detected )
				{	
					//ensure heap integration 
					_CheckHeap;
					
					delete [] (&ptr[-offset-sizeof(size_t)]);
					return;
				}
			}
			else{ Prefix_Err_Detected = TRUE; }
#else
			//ensure heap integration 
#ifdef RTB_FORCE_BUFFER_OVERRUN_DETECTION
			_CheckHeap;
#endif

			delete [] (&ptr[-offset]);
			return;
#endif
		}
		else{ Prefix_Err_Detected = TRUE; }

	#ifdef _WIN64
		printf("Abnormal block at 0x%016x", (size_t)ptr);
	#else
		printf("Abnormal block at 0x%08x", (size_t)ptr);
	#endif

#ifdef RTB_FORCE_BUFFER_OVERRUN_DETECTION
		if( user_block_size != ((size_t)(-1)) )
			printf(" (%d bytes)",user_block_size);

		printf(", ");

		if( Prefix_Err_Detected && Suffix_Err_Detected)
		{	printf("Both low and high");	}
		else if( Prefix_Err_Detected )
		{	printf("Low");	}
		else if( Suffix_Err_Detected )
		{	printf("High");	}
		else{ ASSERT(0); }

		printf(" boundry was overwritten.\n");
#else
		if( Prefix_Err_Detected )printf(", Low boundry was overwritten.\n");
#endif
		ASSERT(0);

	}
}
#pragma warning(default:4244 4127)	

#ifdef __INTEL_COMPILER
#	pragma warning(default: 1684)
#endif
} // namespace rt


///////////////////////////////////////////////////////
// Misc inline templates
namespace rt
{

__forceinline void Randomize( DWORD seed = time(NULL) )
{ srand(seed); }

template<typename T, typename T_R>
__forceinline bool IsInRange_OO(T var,T_R min_v,T_R max_v)
{
	return (var > min_v) && (var < max_v);
}

template<typename T, typename T_R>
__forceinline bool IsInRange_CC(T var,T_R min_v,T_R max_v)
{
	return (var >= min_v) && (var <= max_v);
}

template<typename T, typename T_R>
__forceinline bool IsInRange_CO(T var,T_R min_v,T_R max_v)
{
	return (var >= min_v) && (var < max_v);
}

template<typename T, typename T_R>
__forceinline bool IsInRange_OC(T var,T_R min_v,T_R max_v)
{
	return (var > min_v) && (var <= max_v);
}

__forceinline UINT MaxPower2Within(UINT within)
{
	if(within)
	{
		UINT ret=1;
		while(within!=1)
		{
			ret<<=1;
			within>>=1;
		}

		return ret;
	}
	else
		return 0;
}

template<typename T>
__forceinline T* _CastToNonconst(const T * p)
{
#ifdef __INTEL_COMPILER
#	pragma warning(disable: 1684) //warning by intel compiler
#endif //warning #1684: conversion from pointer to same-sized integral type (potential portability problem)

	return (T*)((size_t)p);

#ifdef __INTEL_COMPILER
#	pragma warning(default: 1684)
#endif
}
} // namespace rt



/////////////////////////////////////////////////////
// switch little-endian/big-endian values, inplace
namespace rt
{
namespace _meta_
{	
	template<int sz>
	__forceinline void _SwitchByteOrder(LPBYTE p)
	{
		BYTE t = p[0];
		p[0] = p[sz-1];
		p[sz-1] = t;
		_SwitchByteOrder<sz-2>(p+1);
	}
	template<>
	__forceinline void _SwitchByteOrder<1>(LPBYTE p){}
	template<>
	__forceinline void _SwitchByteOrder<0>(LPBYTE p){}
} // namespace _meta_
} // namespace rt

namespace rt
{
	template<typename T>
	__forceinline void SwitchByteOrder(T& x)
	{	_meta_::_SwitchByteOrder<sizeof(T)>((LPBYTE)&x);
	}
	template<typename T>
	__forceinline T ConvertByteOrder(const T& x)
	{	T out = x;
		_meta_::_SwitchByteOrder<sizeof(T)>((LPBYTE)&out);
		return out;
	}
} // namespace rt



#ifdef UNICODE
#	define RTB_TextDumpStdOut	std::wcout
#else
#	define RTB_TextDumpStdOut	std::cout
#endif


////////////////////////////////////////////////////////////////////////////
// Optimized Object Swap, UNSAFE if the object has self or member pointed pointers
// No addtional temporary object, no ctor/dtor called
namespace rt
{
	namespace _meta_
	{
		template<UINT Len>
		__forceinline void _Exchange_32AL(LPDWORD obj_a,LPDWORD obj_b)
		{
			DWORD t = *obj_a;
			*obj_a = *obj_b;
			*obj_b = t;

			rt::_meta_::_Exchange_32AL<Len-1>(obj_a+1,obj_b+1);
		}
		template<>
		__forceinline void _Exchange_32AL<0>(LPDWORD,LPDWORD){} // dummy
	} // namespace _meta_
} // namespace rt

namespace rt
{
#pragma warning(disable: 4307) //warning C4307: '*' : integral constant overflow
template<typename T>
__forceinline void Swap(T& a, T& b)
{	
	rt::_meta_::_Exchange_32AL<sizeof(T)/4> ((LPDWORD)&a, (LPDWORD)&b);
	if( (sizeof(T)&0x1) == 0x1 )
	{	DWORD t;
		t = ((LPBYTE)&a)[sizeof(T)-1];
		((LPBYTE)&a)[sizeof(T)-1] = ((LPBYTE)&b)[sizeof(T)-1];
		((LPBYTE)&b)[sizeof(T)-1] = (BYTE)t;
	}
	if( (sizeof(T)&0x2) == 0x2 )
	{	DWORD t;
		t = ((LPWORD)&a)[sizeof(T)/2-1];
		((LPWORD)&a)[sizeof(T)/2-1] = ((LPWORD)&b)[sizeof(T)/2-1];
		((LPWORD)&b)[sizeof(T)/2-1] = (WORD)t;
	}
}
#pragma warning(default: 4307) //warning C4307: '*' : integral constant overflow
} // namespace rt
////////////////////////////////////////////////////////////////////////////

