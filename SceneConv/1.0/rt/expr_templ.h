#pragma once

//////////////////////////////////////////////////////////////////////
// Visual Computing Foundation Classes (VCFC)
//					Jiaping Wang  2006.2
//					e_boris2002@hotmail.com
//
// (C) Copyright Jiaping Wang 2004.
//
// Permission to copy, use, modify, sell and distribute this software
// is granted provided this copyright notice appears in all copies.
// This software is provided "as is" without express or implied
// warranty, and with no claim as to its suitability for any purpose.
//
// Absolutely no warranty!!
//////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////
//  Brief Description
//
//  expr_templ.h
//  expression template, expression support for vectors and images
//
//	{	USING_EXPRESSION;
//		a = b * c + d;
//	}
//
//////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////
// Revise History
//
// Inital version	2009.3.25		Jiaping
//
//////////////////////////////////////////////////////////////////////

#include "type_traits.h"
#include "fixvec_type.h"
#include "image_type.h"

#pragma warning(disable:4503) // warning C4503: decorated name length exceeded, name was truncated 
// From MSDN: "The correctness of the program, however, is unaffected by the truncated name"

namespace rt
{

#define	BINARY_OP_SYMBOL(op,op_name,op_id)	template<typename P1,typename P2,typename Ret> class op_name	\
											{public:														\
												DYNTYPE_FUNC static Ret Func(const P1& x1, const P2& x2)	\
												{	USING_EXPRESSION;	return x1 op x2;	}				\
												static const int id = op_id ;								\
											};																\

		BINARY_OP_SYMBOL(*	,_MUL,	1)
		BINARY_OP_SYMBOL(/	,_DIV,	2)
		BINARY_OP_SYMBOL(%	,_RMD,	3)
		BINARY_OP_SYMBOL(+	,_ADD,	4)
		BINARY_OP_SYMBOL(-	,_SUB,	5)
		BINARY_OP_SYMBOL(>>	,_RSH,	6)
		BINARY_OP_SYMBOL(<<	,_LSH,	7)
		BINARY_OP_SYMBOL(>	,_GT ,	8)
		BINARY_OP_SYMBOL(<	,_LT ,	9)
		BINARY_OP_SYMBOL(<=	,_GTE,	10)
		BINARY_OP_SYMBOL(>=	,_LTE,	11)
		BINARY_OP_SYMBOL(==	,_EQU,	12)
		BINARY_OP_SYMBOL(!=	,_NEQ,	13)
		BINARY_OP_SYMBOL(&	,_BAD,	14)
		BINARY_OP_SYMBOL(|	,_BOR,	15)
		BINARY_OP_SYMBOL(^	,_BXR,	16)
		BINARY_OP_SYMBOL(&&	,_AND,	17)
		BINARY_OP_SYMBOL(||	,_OR ,	18)
#undef	BINARY_OP_SYMBOL


#define UNARY_OP_SYMBOL(op,op_name,op_id)	template<typename P,typename Ret> class op_name		\
											{public:											\
												DYNTYPE_FUNC static Ret Func(const P& x)		\
												{	USING_EXPRESSION;	return op x;	}		\
												static const int id = op_id ;					\
											};													\

		UNARY_OP_SYMBOL(*,_PTR,	101)
		UNARY_OP_SYMBOL(&,_ADR,	102)
		UNARY_OP_SYMBOL(+,_POS,	103)
		UNARY_OP_SYMBOL(-,_Neg,	104)
		UNARY_OP_SYMBOL(!,_INV,	105)
		UNARY_OP_SYMBOL(~,_COM,	106)
#undef	UNARY_OP_SYMBOL

} // namesapce rt


namespace rt
{

static const SIZE_T SIZE_T_INFINITE = (SIZE_T)(-1);

template< typename BaseVec >
class _BS
{public:	template<typename t_Ele> struct SpecifyItemType
			{	typedef BaseVec t_BaseVec; };
			static const bool IsCompactLinear = false;
};

DYNTYPE_FUNC static LPCSTR _text_va_clsname(){ return "_VA<"; }

template< typename T, int dim_up >
class _VA
{	const T& val;
public:
	typedef _VA<T,dim_up-1> t_Ret;

	TYPETRAITS_DECL_TYPEID(_typeid_virtual_array)
	TYPETRAITS_DECL_ELEMENT_TYPE(t_Ret)
	TYPETRAITS_DECL_IS_ARRAY(true)
	TYPETRAITS_DECL_IS_IMAGE(true)
	TYPETRAITS_DECL_LENGTH(0)
	template<class ostream>
	DYNTYPE_FUNC static void __TypeName(ostream & outstr)
	{	outstr<<_text_va_clsname();
		rt::TypeTraits<T>::TypeName(outstr);
		outstr<<','<<dim_up<<'>';
	}

public:
	_VA(const T& x):val(x){}

	SIZE_T GetSize() const{ return SIZE_T_INFINITE; }
	SIZE_T GetWidth() const{ return SIZE_T_INFINITE; }
	SIZE_T GetHeight() const{ return SIZE_T_INFINITE; }

	DYNTYPE_FUNC const t_Ret& At(SIZE_T i) const { return *((t_Ret*)this); }
	DYNTYPE_FUNC const t_Ret& At(SIZE_T x, SIZE_T y) const { return *((t_Ret*)this); }
};
template< typename T>
	class _VA<T,1>
	{	const T& val;
	public:
		TYPETRAITS_DECL_TYPEID(_typeid_virtual_array)
		TYPETRAITS_DECL_ELEMENT_TYPE(T)
		TYPETRAITS_DECL_IS_ARRAY(true)
		TYPETRAITS_DECL_IS_IMAGE(true)
		TYPETRAITS_DECL_LENGTH(0)
		template<class ostream>
		DYNTYPE_FUNC static void __TypeName(ostream & outstr)
		{	outstr<<_text_va_clsname();
			rt::TypeTraits<T>::TypeName(outstr);
			outstr<<'>';
		}

	public:
		_VA(const T& x):val(x){}

		SIZE_T GetSize() const{ return SIZE_T_INFINITE; }
		SIZE_T GetWidth() const{ return SIZE_T_INFINITE; }
		SIZE_T GetHeight() const{ return SIZE_T_INFINITE; }

		DYNTYPE_FUNC const T& At(SIZE_T i) const{	return val; }
		DYNTYPE_FUNC const T& At(SIZE_T x, SIZE_T y) const{	return val; }
	};
} // namespace rt


namespace rt
{
////////////////////////////////////////////////////
// Type Result
namespace _meta_
{

template< typename T, 
		  bool is_vector = rt::TypeTraits<T>::IsArray && (!rt::TypeTraits<T>::IsImage),
		  bool is_VA = _typeid_virtual_array == (int)rt::TypeTraits<T>::Typeid
>
struct IsVector{ static const bool Result = false; };
	template< typename T, bool is_vec >
	struct IsVector<T,is_vec,true>{ static const bool Result = true; };
	template< typename T >
	struct IsVector<T,true,false>{ static const bool Result = true; };


template< typename T, 
		  bool is_vector = rt::TypeTraits<T>::IsArray && rt::TypeTraits<T>::IsImage,
		  bool is_VA = _typeid_virtual_array == (int)rt::TypeTraits<T>::Typeid
>
struct IsImage{ static const bool Result = false; };
	template< typename T, bool is_vec >
	struct IsImage<T,is_vec,true>{ static const bool Result = true; };
	template< typename T >
	struct IsImage<T,true,false>{ static const bool Result = true; };



template<typename T1, typename T2, int dim1_minus_dim2, bool dim1_is_large = (dim1_minus_dim2>0) >
struct _Deduce_VA;
	template<typename T1, typename T2, int dim1_minus_dim2>
	struct _Deduce_VA<T1,T2,dim1_minus_dim2,true>
	{	
		typedef T1 t_Param1;
		typedef rt::_VA<T2,dim1_minus_dim2> t_Param2;

		const t_Param1&	a_inc;
		t_Param2		b_inc;
		_Deduce_VA(const T1& a, const T2& b):a_inc(a),b_inc(b){}
	};
	template<typename T1, typename T2, int dim1_minus_dim2>
	struct _Deduce_VA<T1,T2,dim1_minus_dim2,false>
	{	
		typedef rt::_VA<T1,-dim1_minus_dim2> t_Param1;
		typedef T2 t_Param2;

		t_Param1		a_inc;
		const t_Param2&	b_inc;
		_Deduce_VA(const T1& a,const T2& b):a_inc(a),b_inc(b){ ASSERT(dim1_minus_dim2); }
	};


template< typename t_Ele, typename t_BaseVec,
		  typename T1, typename T2, 
		  bool is_vec1 = _meta_::IsVector<T1>::Result,
		  bool is_vec2 = _meta_::IsVector<T2>::Result,
		  bool is_img1 = _meta_::IsImage<T1>::Result,
		  bool is_img2 = _meta_::IsImage<T2>::Result
>
struct ResultingArray_Binary{ ASSERT_STATIC(0) }; // mixing image and vector in expression is not allowed
	template< typename t_Ele, typename t_BaseVec, typename T1, typename T2, bool is_img1, bool is_img2 >
	struct ResultingArray_Binary<t_Ele,t_BaseVec,T1,T2,true,true,is_img1,is_img2>
	{	
		static const SIZE_T static_len = rt::TypeTraits<t_BaseVec>::Length ;
		typedef rt::_TypedVector<const t_Ele, _BS<t_BaseVec>>				t_Result_Var;
		typedef rt::_TypedFixVec<const t_Ele, static_len, _BS<t_BaseVec>>	t_Result_Fix;

		typedef typename _TypeIndex< t_Result_Var,t_Result_Fix,static_len!=0 >::t_Result t_Result;
	};
	template< typename t_Ele, typename t_BaseVec, typename T1, typename T2, bool is_vec1, bool is_vec2 >
	struct ResultingArray_Binary<t_Ele,t_BaseVec,T1,T2,is_vec1,is_vec2,true,true>
	{	typedef rt::_TypedImage<const t_Ele, _BS<t_BaseVec>>	t_Result;	};


template< typename t_Ele, typename t_BaseVec, typename T,
		  bool is_vec = _meta_::IsVector<T>::Result,
		  bool is_img = _meta_::IsImage<T>::Result
>
struct ResultingArray_Unary;
	template< typename t_Ele, typename t_BaseVec, typename T, bool is_img >
	struct ResultingArray_Unary<t_Ele,t_BaseVec,T,true,is_img>
	{
		static const SIZE_T static_len = rt::TypeTraits<t_BaseVec>::Length ;
		typedef rt::_TypedVector<const t_Ele, _BS<t_BaseVec>>				t_Result_Var;
		typedef rt::_TypedFixVec<const t_Ele, static_len, _BS<t_BaseVec>>	t_Result_Fix;

		typedef typename _TypeIndex< t_Result_Var,t_Result_Fix,static_len!=0 >::t_Result t_Result;
	};
	template< typename t_Ele, typename t_BaseVec, typename T, bool is_vec >
	struct ResultingArray_Unary<t_Ele,t_BaseVec,T,is_vec,true>
	{	typedef rt::_TypedImage<const t_Ele, _BS<t_BaseVec>>	t_Result;	};


template<	typename T, 
			bool have_a_copy = ::rt::IsBuiltInType<T>::Result ||
							  (((int)::rt::TypeTraits<T>::Typeid) == _typeid_virtual_array) ||
							  (((int)::rt::TypeTraits<T>::Typeid) == _typeid_expression) 
>
struct _DeduceOpParamType;
	template<typename T>
	struct _DeduceOpParamType<T,true>{ typedef T t_Result; }; // save a copy of VA and Expr
	template<typename T>
	struct _DeduceOpParamType<T,false>{ typedef const T& t_Result; };


template<typename T1, typename T2>
class _BinaryOp_Base
{
	typedef typename Remove_QualiferAndRef<T1>::t_Result real_T1;
	typedef typename Remove_QualiferAndRef<T2>::t_Result real_T2;

public:
	DYNTYPE_FUNC _BinaryOp_Base(const T1& x1, const T2& x2):p1(x1),p2(x2)
	{	// check dimension
		ASSERT_STATIC(::rt::TypeDimension<T1>::Result == ::rt::TypeDimension<T1>::Result);
		__static_if(IsVector<T1>::Result && IsVector<T2>::Result)
		{
			ASSERT(	p1.GetSize() == p2.GetSize() || p1.GetSize()==SIZE_T_INFINITE || p2.GetSize()==SIZE_T_INFINITE );
			return;		// both are vector
		}
		__static_if(IsImage<T1>::Result && IsImage<T2>::Result)
		{
			ASSERT(p1.GetWidth() == p2.GetWidth() || p1.GetWidth()==SIZE_T_INFINITE || p2.GetWidth()==SIZE_T_INFINITE );
			ASSERT(p1.GetHeight() == p2.GetHeight() || p1.GetHeight()==SIZE_T_INFINITE || p2.GetHeight()==SIZE_T_INFINITE );
			return;		// both are image
		}
		ASSERT(0);   // not way to deal with vector * image
	}

	typename _DeduceOpParamType<real_T1>::t_Result p1;
	typename _DeduceOpParamType<real_T2>::t_Result p2;
};

template<typename T>
class _UnaryOp_Base
{
	typedef typename Remove_QualiferAndRef<T>::t_Result real_T;

public:
	DYNTYPE_FUNC _UnaryOp_Base(const T& x):p(x){}
	typename _DeduceOpParamType<real_T>::t_Result p;
};

}// namespace _meta_
}


namespace rt
{

template<typename T1, typename T2, typename t_RetEle, class OpFunc >
class _Op2;

template<typename T, typename t_RetEle, class OpFunc>
class _Op1;


// Type Result 
template< typename T1, typename T2, 
template<typename Ret,typename P1,typename P2> class OpFunc,
		  int type_dim1 = rt::TypeDimension<T1>::Result,
		  int type_dim2 = rt::TypeDimension<T2>::Result >
struct ResultingType_Binary	// dimension is not the same
{
	typedef _meta_::_Deduce_VA<T1,T2,type_dim1-type_dim2>	t_VA;
	typedef typename t_VA::t_Param1	t_P1;
	typedef typename t_VA::t_Param2	t_P2;

	static const int Target_Dim = __max(type_dim1,type_dim2) ;
	typedef typename ResultingType_Binary<t_P1,t_P2,OpFunc,Target_Dim,Target_Dim>::t_Result t_Result;
};
	// both built-in types
	template< typename T1_in, typename T2_in, template<typename Ret,typename P1,typename P2> class OpFunc >
	struct ResultingType_Binary<T1_in,T2_in,OpFunc,0,0>
	{
		typedef typename TypeTraits<T1_in>::t_Val T1;
		typedef typename TypeTraits<T2_in>::t_Val T2;
		static const int T1_Rank = NumericTraits<T1>::TypePriority;
		static const int T2_Rank = NumericTraits<T2>::TypePriority;

		typedef typename _meta_::_TypeIndex< T1,T2,(T1_Rank<T2_Rank) >::t_Result t_Result; 
	};
	// same dimension
	template< typename T1, typename T2, template<typename Ret,typename P1,typename P2> class OpFunc, int dim >
	struct ResultingType_Binary<T1,T2, OpFunc, dim, dim>
	{
		typedef typename rt::TypeTraits<T1>::t_Element	E1_raw;
		typedef typename rt::TypeTraits<T2>::t_Element	E2_raw;

		typedef typename rt::Remove_QualiferAndRef<E1_raw>::t_Result E1;
		typedef typename rt::Remove_QualiferAndRef<E2_raw>::t_Result E2;

		typedef typename ResultingType_Binary<E1,E2,OpFunc>::t_Result	RET;
		typedef typename _meta_::ResultingArray_Binary<RET, _Op2<const T1,const T2,RET,OpFunc<E1,E2,RET>>, T1, T2>::t_Result t_Result;
	};

template< typename T, template<typename Ret,typename P> class OpFunc,
		  int type_dim = rt::TypeDimension<T>::Result
>
struct ResultingType_Unary	// array
{	typedef typename TypeTraits<T>::t_Element ELE;
	typedef typename ResultingType_Unary<ELE,OpFunc>::t_Result RET;
	typedef typename _meta_::ResultingArray_Unary<RET, _Op1<const T,RET, OpFunc<ELE,RET>>, T>::t_Result t_Result;
};
	template< typename T, template<typename Ret,typename P> class OpFunc >
	struct ResultingType_Unary<T,OpFunc,0>
	{	typedef typename TypeTraits<T>::t_Val t_Result;
	};


template<typename T1, typename T2, typename t_RetEle, class OpFunc >
class _Op2:protected _meta_::_BinaryOp_Base<T1, T2>
{
public:
	typedef SIZE_T t_SizeType;
	TYPETRAITS_DECL_TYPEID(_typeid_expression)
	TYPETRAITS_DECL_IS_AGGREGATE(true)
	TYPETRAITS_DECL_LENGTH(__max(rt::TypeTraits<T1>::Length,rt::TypeTraits<T2>::Length))

	DYNTYPE_FUNC const t_RetEle At(SIZE_T i) const{	return OpFunc::Func(p1.At(i),p2.At(i)); }
	DYNTYPE_FUNC const t_RetEle At(SIZE_T x, SIZE_T y) const{	return OpFunc::Func(p1.At(x,y),p2.At(x,y)); }
	DYNTYPE_FUNC SIZE_T GetSize()const { return min(p1.GetSize(),p2.GetSize()); }
	DYNTYPE_FUNC SIZE_T GetWidth()const { return min(p1.GetWidth(),p2.GetWidth()); }
	DYNTYPE_FUNC SIZE_T GetHeight()const { return min(p1.GetHeight(),p2.GetHeight()); }

	DYNTYPE_FUNC SIZE_T SetSize(){ ASSERT(0); }
	DYNTYPE_FUNC SIZE_T SetSize(UINT,UINT){ ASSERT(0); }
};


template<typename T, typename t_RetEle, class OpFunc>
class _Op1:protected _meta_::_UnaryOp_Base<T>
{
public:
	typedef SIZE_T t_SizeType;
	TYPETRAITS_DECL_TYPEID(_typeid_expression)
	TYPETRAITS_DECL_IS_AGGREGATE(true)
	TYPETRAITS_FOLLOW_LENGTH(T)

	DYNTYPE_FUNC const t_RetEle At(SIZE_T i) const{	return OpFunc::Func(p.At(i)); }
	DYNTYPE_FUNC const t_RetEle At(SIZE_T x, SIZE_T y) const{	return OpFunc::Func(p.At(x,y)); }
	DYNTYPE_FUNC SIZE_T GetSize()const { return p.GetSize(); }
	DYNTYPE_FUNC SIZE_T GetWidth()const { return p.GetWidth(); }
	DYNTYPE_FUNC SIZE_T GetHeight()const { return p.GetHeight(); }

	DYNTYPE_FUNC SIZE_T SetSize(){ ASSERT(0); }
	DYNTYPE_FUNC SIZE_T SetSize(UINT,UINT){ ASSERT(0); }
};


template<typename T, typename t_Ret>
DYNTYPE_FUNC const t_Ret UnaryOperator(const T& a)
{
	rt::_meta_::_UnaryOp_Base<T> ret(a);
	return *(t_Ret*)&ret;
}

template<typename T1, typename T2, typename t_Ret>
DYNTYPE_FUNC const t_Ret BinaryOperator(const T1& a, const T2& b)
{
	// check type dimension
	static const int dim1 = ::rt::TypeDimension<T1>::Result;
	static const int dim2 = ::rt::TypeDimension<T2>::Result;
	
	__static_if(dim1==dim2)
	{
		__static_if(dim1>0)
		{
			rt::_meta_::_BinaryOp_Base<T1,T2> ret(a,b);
			return *(t_Ret*)&ret;
		}
		ASSERT_STATIC(dim1>0);  // T1*T2 is not array and has no operator defined.
	}
	__static_if(dim1!=dim2)
	{
		typedef ::rt::_meta_::_Deduce_VA<T1,T2,dim1-dim2>	t_VA;
		t_VA level_up(a,b);
		return BinaryOperator<t_VA::t_Param1,t_VA::t_Param2,t_Ret>(level_up.a_inc,level_up.b_inc);
	}
}


#define BINARY_OP_OVERRIDE(op,op_name)	template<typename T1, typename T2>																		\
										DYNTYPE_FUNC const typename ::rt::ResultingType_Binary<T1,T2,::rt::op_name>::t_Result					\
										operator op (const T1& a, const T2& b)																	\
										{	return																								\
											::rt::BinaryOperator<T1,T2,typename ::rt::ResultingType_Binary<T1,T2,::rt::op_name>::t_Result>(a,b);\
										}

#define UNARY_OP_OVERRIDE(op,op_name)	template<typename T>																				\
										DYNTYPE_FUNC const typename ::rt::ResultingType_Unary<T,::rt::op_name>::t_Result					\
										operator op (const T& a)																			\
										{	return ::rt::UnaryOperator<T,typename rt::ResultingType_Unary<T,::rt::op_name>::t_Result>(a);	\
										}

namespace expr
{
		BINARY_OP_OVERRIDE(*	,_MUL)
		BINARY_OP_OVERRIDE(/	,_DIV)
		BINARY_OP_OVERRIDE(%	,_RMD)
		BINARY_OP_OVERRIDE(+	,_ADD)
		BINARY_OP_OVERRIDE(-	,_SUB)
		BINARY_OP_OVERRIDE(|	,_BOR)
		BINARY_OP_OVERRIDE(^	,_BXR)
		BINARY_OP_OVERRIDE(&&	,_AND)
		BINARY_OP_OVERRIDE(||	,_OR )

		UNARY_OP_OVERRIDE(+,_POS)
		UNARY_OP_OVERRIDE(-,_Neg)
		UNARY_OP_OVERRIDE(!,_INV)
		UNARY_OP_OVERRIDE(~,_COM)

} // namespace expr


namespace expr_rare
{
		BINARY_OP_OVERRIDE(>>	,_RSH)
		BINARY_OP_OVERRIDE(<<	,_LSH)
		BINARY_OP_OVERRIDE(>	,_GT )
		BINARY_OP_OVERRIDE(<	,_LT )
		BINARY_OP_OVERRIDE(<=	,_GTE)
		BINARY_OP_OVERRIDE(>=	,_LTE)
		BINARY_OP_OVERRIDE(==	,_EQU)
		BINARY_OP_OVERRIDE(!=	,_NEQ)
		BINARY_OP_OVERRIDE(&	,_BAD)

		UNARY_OP_OVERRIDE(*,_PTR)
		UNARY_OP_OVERRIDE(&,_ADR)
} // namespace expr_rare


#undef	BINARY_OP_OVERRIDE
#undef	UNARY_OP_OVERRIDE

} // namespace rt


