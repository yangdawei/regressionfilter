#pragma once

//////////////////////////////////////////////////////////////////////
// Visual Computing Foundation Classes (VCFC)
//					Jiaping Wang  2006.2
//					e_boris2002@hotmail.com
//
// (C) Copyright Jiaping Wang 2004.
//
// Permission to copy, use, modify, sell and distribute this software
// is granted provided this copyright notice appears in all copies.
// This software is provided "as is" without express or implied
// warranty, and with no claim as to its suitability for any purpose.
//
// Absolutely no warranty!!
//////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////
//  Brief Description
//
//  image_type.h
//
//   instantiations of _typedvector for image
//
//////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////
// Revise History
//
// Inital version	2006.6.9		Jiaping
//
//////////////////////////////////////////////////////////////////////

#include "runtime_base.h"
#include "type_traits.h"
#include <iomanip>


#define ASSERT_SIZE(x1,x2) ASSERT((x1).GetWidth() == (x2).GetWidth() && (x1).GetHeight() == (x2).GetHeight())


namespace rt
{


#define ASSIGN_OPERATOR_IMG(clsname)														\
		__static_if(!IsItemTypePlanar)	{												\
		DYNTYPE_FUNC const clsname& operator = (const FundamentalItemType & i)			\
		{	for(UINT y=0;y<GetHeight();y++)												\
			for(UINT x=0;x<GetWidth(); x++)At(x,y) = i;   return *this;					\
		}								}												\
		DYNTYPE_FUNC const clsname& operator = (const ItemType & x)						\
		{	SetItem(x); return *this;	}												\
		DYNTYPE_FUNC const clsname& operator = (const clsname & x) /*default assign*/	\
		{	CopyFrom(x); return *this;	}												\
		template<typename T, typename BaseVec>											\
		DYNTYPE_FUNC const clsname& operator = (const rt::_TypedImage<T,BaseVec> & x)	\
		{	CopyFrom(x); return *this;	}												\
		DYNTYPE_FUNC void TextDump() const												\
		{	_STD_OUT<<*this; _STD_OUT<<_T('\n'); }										\

#define DEFAULT_TYPENAME(clsname)														\
		template<class ostream> static void __TypeName(ostream & outstr)				\
		{	outstr<<_T("rt::") _T(#clsname) _T("<");									\
			TypeTraits_Item::TypeName(outstr); outstr<<_T('>');							\
		}																				\
		static const int MARCO_JOIN(is_,clsname);										\

#define COMMON_CONSTRUCTOR_IMG(clsname)												\
		clsname(){}																	\
		explicit DYNTYPE_FUNC clsname(const clsname& x)								\
		{ SetSizeAs(x); *this=x; }													\
		template<typename T,class BaseVec>											\
		explicit DYNTYPE_FUNC clsname(const rt::_TypedImage<T,BaseVec>& x)			\
		{ SetSizeAs(x); *this=x; }													\



#define COMMON_IMAGE_STUFFS(clsname)	ASSIGN_OPERATOR_IMG(clsname)						\
										DEFAULT_TYPENAME(Image)								\



template<typename t_Val,class BaseVector>
class _TypedImage:public BaseVector::SpecifyItemType<t_Val>::t_BaseVec
{

////////////////////////////////////////////////////////////////////////////////////////////
// 1. Traits
public:
	typedef t_Val						ItemType;
	typedef ItemType*					LPItemType;
	typedef const ItemType*				LPCItemType;
protected:
	typedef rt::TypeTraits<ItemType>		TypeTraits_Item;
	typedef typename TypeTraits_Item::t_Val _ItemValueType;

public:
	template<typename t_Ele2> class SpecifyItemType
	{public: typedef _TypedImage<t_Ele2,BaseVector> t_Result; };

	TYPETRAITS_DECL_ACCUM_TYPE(typename SpecifyItemType< typename rt::TypeTraits<t_Val>::t_Accum >::t_Result)	
	TYPETRAITS_DECL_VALUE_TYPE(typename SpecifyItemType< typename rt::TypeTraits<t_Val>::t_Val >::t_Result)
	TYPETRAITS_DECL_ELEMENT_TYPE(ItemType)
	TYPETRAITS_DECL_SIGNED_TYPE(typename SpecifyItemType<typename rt::TypeTraits<t_Val>::t_Signed>::t_Result)
	TYPETRAITS_DECL_IS_NUMERIC(false)
	TYPETRAITS_DECL_IS_ARRAY(true)
	TYPETRAITS_DECL_IS_IMAGE(true)
	public:static const int __Typeid =	(int)(TypeTraits<typename BaseVector::SpecifyItemType<t_Val>::t_BaseVec>::Typeid!=_typeid_class?
											  TypeTraits<BaseVector::SpecifyItemType<t_Val>::t_BaseVec>::Typeid:_typeid_codelib);
	TYPETRAITS_DECL_EXTENDTYPEID((__Typeid<<16)+101)

	typedef typename rt::FundamentalType<ItemType>::t_Result	FundamentalItemType;
	static const int IsItemTypePlanar = ::rt::IsTypeSame<FundamentalItemType,typename ::rt::Remove_Qualifer<ItemType>::t_Result>::Result;

////////////////////////////////////////////////////////////////////////////////////////////
// 2. Basic States
public:
	COMMON_IMAGE_STUFFS(_TypedImage)

	DYNTYPE_FUNC ItemType&			operator ()(UINT x, UINT y){ return At(x,y); }
	DYNTYPE_FUNC const ItemType&	operator ()(UINT x, UINT y) const{ return At(x,y); }

	template<class T> 
	DYNTYPE_FUNC BOOL SetSizeAs( const T & x ){ return SetSize_2D(x.GetWidth(),x.GetHeight()); }


public:
	DYNTYPE_FUNC BOOL		IsNull() const{return !(lpData);}
	DYNTYPE_FUNC BOOL		IsNotNull() const{return (BOOL)lpData;}



////////////////////////////////////////////////////////////////////////////////////////////
// 3. per-fundamental-element manipulations
public:
	////////////////////////////////////////////////////////////////////////////////////////
	// 3.1 Unary operators
	//
	// 3.1.1 Assignment
#define ASSIGN_OP(op)	template<typename T, class BaseVec>												\
						DYNTYPE_FUNC void operator op (const rt::_TypedImage<T,BaseVec> & i)			\
						{	ASSERT_SIZE(x,*this);														\
							for(UINT y=0;y<GetHeight();y++)												\
							for(UINT x=0;x<GetWidth(); x++)At(x,y) op i(x,y);							\
						}																				\
						DYNTYPE_FUNC void operator op (const ItemType & i)								\
						{	for(UINT y=0;y<GetHeight();y++)												\
							for(UINT x=0;x<GetWidth(); x++)At(x,y) op i;								\
						}																				\
						__static_if(!IsItemTypePlanar)	{												\
						DYNTYPE_FUNC void operator op (const FundamentalItemType & i)					\
						{	for(UINT y=0;y<GetHeight();y++)												\
							for(UINT x=0;x<GetWidth(); x++)At(x,y) op i;								\
						}								}

		#pragma warning(disable:4244)
		ASSIGN_OP(+=)
		ASSIGN_OP(-=)
		ASSIGN_OP(*=)
		ASSIGN_OP(/=)
		ASSIGN_OP(%=)
		ASSIGN_OP(<<=)
		ASSIGN_OP(>>=)
		ASSIGN_OP(&=)
		ASSIGN_OP(|=)
		ASSIGN_OP(^=)
		#pragma warning(default:4244)
#undef  ASSIGN_OP

	// 3.1.2 Prefix
#define UNARY_OP_Prefix(op)	DYNTYPE_FUNC const _TypedImage& operator op ()								\
							{	for(UINT y=0;y<GetHeight();y++)											\
								for(UINT x=0;x<GetWidth(); x++)op(At(x,y)); return *this;					\
							}																			

		UNARY_OP_Prefix(++)
		UNARY_OP_Prefix(--)
		UNARY_OP_Prefix(-)
		UNARY_OP_Prefix(!)
#undef  UNARY_OP_Prefix

	// 3.1.3 Postfix, functions same as Prefix
#define UNARY_OP_Postfix(op)	DYNTYPE_FUNC const _TypedImage& operator op (int)						\
							{	for(UINT y=0;y<GetHeight();y++)											\
								for(UINT x=0;x<GetWidth(); x++) (At(x,y))op; return *this;				\
							}																			
		UNARY_OP_Postfix(++)
		UNARY_OP_Postfix(--)
#undef UNARY_OP_Postfix


	////////////////////////////////////////////////////////////////////////////////////////
	// 3.2 Set all elements with machine zero
	DYNTYPE_FUNC void Zero()
	{	
		for(UINT y=0;y<GetHeight();y++)
		for(UINT x=0;x<GetWidth(); x++)
		{
			__if_exists(ItemType::Zero)		{ At(x,y).Zero(); }
			__if_not_exists(ItemType::Zero)	{ At(x,y) = 0; }
		}
	}

	////////////////////////////////////////////////////////////////////////////////////////
	// 3.3 Set all item with given value
	DYNTYPE_FUNC void SetItem(const ItemType & i)
	{	
		for(UINT y=0;y<GetHeight();y++)
		for(UINT x=0;x<GetWidth(); x++)At(x,y) = i;
	}

	////////////////////////////////////////////////////////////////////////////////////////
	// 3.8 Dot product/Sum/L2Norm_Sqr
	// 3.8.1 Dot product
	template< class t_Vec2 >
	DYNTYPE_FUNC _ItemValueType Dot(const t_Vec2 & i ) const
	{	USING_EXPRESSION;
		ASSERT_SIZE(*this,i);
		typedef _ItemValueType t_Accum;
		t_Accum tot(0);
		for(UINT y=0;y<GetHeight();y++)
		for(UINT x=0;x<GetWidth(); x++)tot += At(x,y)*i(x,y);
		return tot;
	}

	// 3.8.2 Sum
	DYNTYPE_FUNC _ItemValueType Sum() const
	{	USING_EXPRESSION;
		typedef _ItemValueType t_Accum;
		t_Accum tot(0);
		for(UINT y=0;y<GetHeight();y++)
		for(UINT x=0;x<GetWidth(); x++)tot += At(x,y);
		return tot;
	}

	// 3.8.3 L2Norm_Sqr
	DYNTYPE_FUNC _ItemValueType L2Norm_Sqr() const
	{	USING_EXPRESSION;
		typedef _ItemValueType t_Accum;
		t_Accum tot(0);
		for(UINT y=0;y<GetHeight();y++)
		for(UINT x=0;x<GetWidth(); x++)tot += Sqr(At(x,y));
		return tot;
	}

	// 3.8.4 L2Distance_Sqr
	template< class t_Vec2 >
	DYNTYPE_FUNC _ItemValueType Distance_Sqr(const t_Vec2 & i) const
	{	USING_EXPRESSION;
		typedef _ItemValueType t_Accum;
		t_Accum tot(0);
		for(UINT y=0;y<GetHeight();y++)
		for(UINT x=0;x<GetWidth(); x++)tot += Sqr(At(x,y)-i(x,y));
		return tot;
	}

	////////////////////////////////////////////////////////////////////////////////////////
	// 3.10 Randomize elements
	DYNTYPE_FUNC void Random()
	{
		for(UINT y=0;y<GetHeight();y++)
		for(UINT x=0;x<GetWidth(); x++)rt::_meta_::_Random(At(x,y));
	}


////////////////////////////////////////////////////////////////////////////////////////////
// 4. per-element manipulations
public:
	////////////////////////////////////////////////////////////////////////////////////////
	// 4.1 Assignment
	template<typename t_ItemType2, typename BaseVec2>
	DYNTYPE_FUNC void CopyTo(_TypedImage<t_ItemType2,BaseVec2> & i) const
	{	ASSERT_SIZE(*this,i);
		for(UINT y=0;y<GetHeight();y++)
		for(UINT x=0;x<GetWidth(); x++)i(x,y) = At(x,y);
	}
	template<typename t_ItemType2, typename BaseVec2>
	DYNTYPE_FUNC void CopyFrom(const _TypedImage<t_ItemType2,BaseVec2> & i)
	{	ASSERT_SIZE(*this,i);
		for(UINT y=0;y<GetHeight();y++)
		for(UINT x=0;x<GetWidth(); x++)At(x,y) = i(x,y);
	}

////////////////////////////////////////////////////////////////////////////////////////////
// 5. basic num operations
public:
	////////////////////////////////////////////////////////////////////////////////////////
	// 5.2 Min/Max
	DYNTYPE_FUNC ItemType Min() const
	{	ASSERT(IsNotNull());
		ItemType CurMin = At(0,0);
		for(UINT y=0;y<GetHeight();y++)
		for(UINT x=0;x<GetWidth(); x++)
			if(At(x,y)<CurMin)CurMin = At(x,y);
		return CurMin;
	}
	DYNTYPE_FUNC ItemType Max() const
	{	ASSERT(IsNotNull());
		ItemType CurMax = At(0,0);
		for(UINT y=0;y<GetHeight();y++)
		for(UINT x=0;x<GetWidth(); x++)
			if(CurMax<At(x,y))CurMax = At(x,y);
		return CurMax;
	}

	////////////////////////////////////////////////////////////
	//Persisentence
	//Intergate with w32::CFile64/w32::CFile64_Composition
public:
	template<class t_w32_file>
	HRESULT	Load(t_w32_file & file)	//Load from storage
	{	if(!file.IsOpen())return E_INVALIDARG;
		ULONGLONG org_pos = file.GetCurrentPosition();
		HRESULT hr = E_INVALIDARG;

		BOOL TypeMismatch;
		DWORD dw=0,ww=0,hh=0;
		file.Read(&dw,sizeof(DWORD));
		if( dw == 0x4d4d4d53 )
		{
			file.Read(&dw,sizeof(DWORD));	//sizeof(t_Val)

			__static_if(rt::TypeTraits<ItemType>::IsAggregate)
			{
				if( sizeof(t_Val) != dw )return E_ABORT;
			}

			file.Read(&dw,sizeof(DWORD)); //Typeid

			DWORD type_sign=0;
			{	__static_if( rt::TypeTraits<t_Val>::IsAggregate )
				{	typedef typename rt::FundamentalType<t_Val>::t_Result t_Fun;
					type_sign = rt::TypeTraits<t_Fun>::Typeid; //Fundamental type id
				}
				type_sign |= (rt::TypeTraits<t_Val>::Typeid)<<16; //element type id
			}
			TypeMismatch = ( dw != type_sign );

			file.Read(&ww,sizeof(DWORD));
			file.Read(&hh,sizeof(DWORD));
			__super::SetSize_2D(ww,hh);

			hr = TypeMismatch?S_FALSE:S_OK;
			if(ww && hh)
			{	if(IsNotNull())
				{	__static_if(!rt::TypeTraits<t_Val>::IsAggregate)
					{// non-Aggregate elements, t_w32_file should be CFile64_Composition
						TCHAR SectionName[30];
						for(UINT y=0;y<GetHeight();y++)
						for(UINT x=0;x<GetWidth();x++)
						{	_stprintf(SectionName,_T("@(%d,%d)"),x,y);
							if(file.EnterSection(SectionName))
							{	hr = At(x,y).Load(file);
								file.ExitSection();
							}else{ hr = E_INVALIDARG; }
							if(FAILED(hr))break;
						}
					}
					__static_if(rt::TypeTraits<t_Val>::IsAggregate)
					{// Aggregate elements
						dw = sizeof(t_Val)*GetWidth();
						for(UINT y=0;y<GetHeight();y++)
						{
							if(dw == file.Read(&At(0,y),dw)){}
							else
							{	hr = t_w32_file::HresultFromLastError();
								break;
							}
						}
					}
				}else{ hr = E_OUTOFMEMORY; }
			}
		}

		if(FAILED(hr)){ file.Seek(org_pos); SetSize_2D(0,0); }
		return hr;
	}

	template<class t_w32_file>
	HRESULT Save(t_w32_file & file) const //save to storage
	{
		ASSERT(file.IsOpen());
		ULONGLONG org_pos = file.GetCurrentPosition();

		DWORD dw = 0x4d4d4d53;		//signature "SMMM"
		file.Write(&dw,sizeof(DWORD));

		__static_if(rt::TypeTraits<ItemType>::IsAggregate)
		{
			dw = sizeof(t_Val);	//byte-per-element, element size
		}
		__static_if(!rt::TypeTraits<ItemType>::IsAggregate)
		{
			dw = 0;
		}
		file.Write(&dw,sizeof(DWORD));
		
		dw = 0;
		__static_if( rt::TypeTraits<t_Val>::IsAggregate )
		{	typedef typename rt::FundamentalType<t_Val>::t_Result t_Fun;
			dw = rt::TypeTraits<t_Fun>::Typeid; //Fundamental type id
		}
		dw |= (rt::TypeTraits<t_Val>::Typeid)<<16; //element type id
		file.Write(&dw,sizeof(DWORD));

		dw = GetWidth();
		file.Write(&dw,sizeof(DWORD)); //width

		dw = GetHeight();
		file.Write(&dw,sizeof(DWORD)); //height

		HRESULT hr = S_OK;

		__static_if(!rt::TypeTraits<t_Val>::IsAggregate)
		{// non-Aggregate elements, t_w32_file should be CFile64_Composition
			TCHAR SectionName[30];
			for(UINT y=0;y<GetHeight();y++)
			for(UINT x=0;x<GetWidth();x++)
			{	_stprintf(SectionName,_T("@(%d,%d)"),x,y);
				if(file.EnterSection(SectionName))
				{	hr = At(x,y).Save(file);
					file.ExitSection();
				}else{ hr = E_INVALIDARG; }
				if(FAILED(hr))break;
			}
		}
		__static_if(rt::TypeTraits<t_Val>::IsAggregate)
		{// Aggregate elements
			dw *= sizeof(t_Val); //all data
			dw = sizeof(t_Val)*GetWidth();
			for(UINT y=0;y<GetHeight();y++)
			{
				if(dw == file.Write(&At(0,y),dw)){}
				else
				{	hr = t_w32_file::HresultFromLastError();
					break;
				}
			}
		}

		if(FAILED(hr)){ file.Seek(org_pos); }
		return hr;
	}
};


} // namespace rt

