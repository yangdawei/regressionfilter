#pragma once

//////////////////////////////////////////////////////////////////////
// Visual Computing Foundation Classes (VCFC)
//					Jiaping Wang  2004.3
//					e_boris2002@hotmail.com
//
// (C) Copyright Jiaping Wang 2004.
//
// Permission to copy, use, modify, sell and distribute this software
// is granted provided this copyright notice appears in all copies.
// This software is provided "as is" without express or implied
// warranty, and with no claim as to its suitability for any purpose.
//
// Absolutely no warranty!!
//////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////
//  Brief Description
//
//  vector_type.h
//
//  Templates for basic algebra for dynamic length types
//
//  Provides: 
//	1. Base Vector: rt::_BaseVecFix rt::_BaseVecDyn32AL rt::_BaseVecDynObj 
//					rt::_BaseVecRef
//  2. TypeResult: Deduce the type of T1 OP T2
//
//  Option Macros:
//	#define RTB_TEXTDUMP_LINEWIDTH_HINT (int=80)
//			Set hint for textdump function to output optimized appearance
//
//////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////
// Revise History
//
// Inital version	2006.3.3		Jiaping
//
//////////////////////////////////////////////////////////////////////


#include "runtime_base.h"
#include "type_traits.h"
#include <iomanip>

#ifndef RTB_TEXTDUMP_LINEWIDTH_HINT
#	define RTB_TEXTDUMP_LINEWIDTH_HINT 80
#endif

namespace rt
{

namespace _meta_
{

template<typename T>
DYNTYPE_FUNC void _Random(T& x)
{	typedef rt::TypeTraits<T> TypeTraits_T;
	typedef rt::NumericTraits<T> NumericTraits_T;

	__if_exists(T::Random){ x.Random(); return; }
	__if_not_exists(T::Random)
	{
		__static_if(TypeTraits_T::IsArray)
		{	for(SIZETYPE i=0;i<rt::GetSize(x);i++)_Random(x[i]);
			return;
		}
		__static_if( !TypeTraits_T::IsArray && 
					 (	NumericTraits_T::IsBuiltInNumeric ||
						TypeTraits_T::Typeid == ::rt::_typeid_pointer
				   ) )
		{
			__static_if( rt::NumericTraits<T>::IsFloat){ x=(::rand()-(RAND_MAX/2))/((T)(RAND_MAX/2)); return; }
			__static_if(!rt::NumericTraits<T>::IsFloat)
			{
	#pragma warning(disable:4312 4800)
				x=(T)::rand();
	#pragma warning(default:4312 4800)
				return;
			}
		}
	}

	ASSERT(0); //I don't know how to randomize the elements
}

}	// namespace _meta_
}	// namespace rt



////////////////////////////////////////////////////////////////////////////////////////////
// Main manipulations for vectors of dynamic length
// with BaseVector::t_Ele, target on large vector
#define ASSIGN_OPERATOR_VEC(clsname)													\
		__static_if(!IsItemTypePlanar){													\
		DYNTYPE_FUNC const clsname& operator = (const FundamentalItemType & x)			\
		{ for(SIZETYPE i=0;i<GetSize();i++)At(i) = x; return *this; } 						\
		}																				\
		DYNTYPE_FUNC const clsname& operator = (const ItemType & x)						\
		{ SetItem(x); return *this;}													\
		DYNTYPE_FUNC const clsname& operator = (const clsname & x) /*default assign*/	\
		{ ASSERT(GetSize()==x.GetSize()); CopyFrom(x); return *this; }					\
		template<typename T, typename BaseVec>											\
		DYNTYPE_FUNC const clsname& operator = (const rt::_TypedVector<T,BaseVec> & x)	\
		{ ASSERT(GetSize()==x.GetSize()); CopyFrom(x); return *this; }					\
		template<typename T, int sz>													\
		DYNTYPE_FUNC const clsname& operator = (const T x[sz])							\
		{	ASSERT(GetSize()==sz);														\
			CopyFrom((const rt::_TypedVector<T,rt::_BaseVecFix<sz> >&)x); return *this;	\
		}																				\
		DYNTYPE_FUNC void TextDump() const												\
		{	_STD_OUT<<*this; _STD_OUT<<_T('\n'); }										\



namespace rt
{

template<typename t_Ele, class BaseVector > 
class _TypedVector:public BaseVector::SpecifyItemType<t_Ele>::t_BaseVec
{
#pragma warning(disable:4244)
//warning C4244: '=' : conversion from 'xxx' to 'xxx', possible loss of data

////////////////////////////////////////////////////////////////////////////////////////////
// 1. Traits
public:
	typedef t_Ele						ItemType;
	typedef ItemType*					LPItemType;
	typedef const ItemType*				LPCItemType;
	typedef typename BaseVector::SpecifyItemType<t_Ele>::t_BaseVec::t_SizeType SIZETYPE;
protected:
	typedef rt::TypeTraits<ItemType>		TypeTraits_Item;
	typedef typename TypeTraits_Item::t_Val _ItemValueType;

public:
	template<typename t_Ele2> class SpecifyItemType
	{public: typedef rt::_TypedVector<t_Ele2,BaseVector> t_Result; };
	static const bool IsCompactVector = BaseVector::IsCompactLinear;

	//TypeTraits
	TYPETRAITS_DECL_ACCUM_TYPE(typename SpecifyItemType< typename rt::TypeTraits<t_Ele>::t_Accum >::t_Result)	
	TYPETRAITS_DECL_VALUE_TYPE(typename SpecifyItemType< typename rt::TypeTraits<t_Ele>::t_Val >::t_Result)
	TYPETRAITS_DECL_ELEMENT_TYPE(ItemType)
	TYPETRAITS_DECL_SIGNED_TYPE(typename SpecifyItemType<typename rt::TypeTraits<t_Ele>::t_Signed>::t_Result)
	TYPETRAITS_DECL_IS_NUMERIC(false)
	TYPETRAITS_DECL_IS_ARRAY(true)
	TYPETRAITS_DECL_IS_IMAGE(false)

public:
	static const int __Typeid =	(((int)TypeTraits<typename BaseVector::SpecifyItemType<t_Ele>::t_BaseVec>::Typeid)!=_typeid_class?
								  (int)TypeTraits<BaseVector::SpecifyItemType<t_Ele>::t_BaseVec>::Typeid : (int)_typeid_codelib);

	TYPETRAITS_DECL_EXTENDTYPEID((__Typeid<<16)+1)

	typedef typename rt::FundamentalType<ItemType>::t_Result	FundamentalItemType;
	static const bool IsItemTypePlanar = ::rt::IsTypeSame<FundamentalItemType,typename ::rt::Remove_Qualifer<ItemType>::t_Result>::Result;

////////////////////////////////////////////////////////////////////////////////////////////
// 2. Basic States
public:
#pragma warning(disable:4244)
	ASSIGN_OPERATOR_VEC(_TypedVector)
#pragma warning(default:4244)
	DEFAULT_TYPENAME(_TypedVector)

	template<typename T>
	DYNTYPE_FUNC ItemType & operator [](const T index){ ASSERT((UINT)index<=GetSize()); return At(index); }
	template<typename T>
	DYNTYPE_FUNC const ItemType & operator [](const T index)const { ASSERT((UINT)index<=GetSize()); return At(index); }

	DYNTYPE_FUNC SIZETYPE GetSize()const { return (UINT)__super::GetSize(); }
	DYNTYPE_FUNC BOOL SetSize(SIZETYPE sz=0){ return __super::SetSize(sz); }

	template<class T> 
	DYNTYPE_FUNC BOOL SetSizeAs( const T & x ){ return SetSize((t_SizeType)::rt::GetSize(x)); }

////////////////////////////////////////////////////////////////////////////////////////////
// 3. per-fundamental-element manipulations
public:
	////////////////////////////////////////////////////////////////////////////////////////
	// 3.1 Unary operators
	//
	// 3.1.1 Assignment
#define ASSIGN_OP(op)	template<typename T, class BaseVec>												\
						DYNTYPE_FUNC void operator op (const rt::_TypedVector<T,BaseVec> & x)			\
						{ ASSERT(GetSize()==x.GetSize()); for(UINT i=0;i<GetSize();i++)At(i) op x.At(i);} \
						DYNTYPE_FUNC void operator op (const ItemType & x)								\
						{ for(SIZETYPE i=0;i<GetSize();i++)At(i) op x; }									\
						__static_if(!IsItemTypePlanar){													\
							DYNTYPE_FUNC void operator op (const FundamentalItemType & x)				\
							{ for(SIZETYPE i=0;i<GetSize();i++)At(i) op x; }								\
						}																				\
						template<typename T>															\
						DYNTYPE_FUNC void operator op (const T* x)										\
						{	ASSERT_ARRAY(x,GetSize());													\
							for(SIZETYPE i=0;i<GetSize();i++)At(i) op x[i];								\
						}
						//template<typename T>															\
						//DYNTYPE_FUNC void operator op (T* x)											\
						//{ ASSERT_ARRAY(x,GetSize()); for(UINT i=0;i<GetSize();i++)At(i) op x[i]; }	\

		#pragma warning(disable:4244)
		ASSIGN_OP(+=)
		ASSIGN_OP(-=)
		ASSIGN_OP(*=)
		ASSIGN_OP(/=)
		ASSIGN_OP(%=)
		ASSIGN_OP(<<=)
		ASSIGN_OP(>>=)
		ASSIGN_OP(&=)
		ASSIGN_OP(|=)
		ASSIGN_OP(^=)
		#pragma warning(default:4244)
#undef  ASSIGN_OP

	// 3.1.2 Prefix
#define UNARY_OP_Prefix(op)	DYNTYPE_FUNC const _TypedVector& operator op () \
							{ for(UINT i=0;i<GetSize();i++) op(At(i)); return *this;}
		UNARY_OP_Prefix(++)
		UNARY_OP_Prefix(--)
		UNARY_OP_Prefix(-)
		UNARY_OP_Prefix(!)
#undef  UNARY_OP_Prefix

	// 3.1.3 Postfix, functions same as Prefix
#define UNARY_OP_Postfix(op)	DYNTYPE_FUNC const _TypedVector& operator op (int) \
								{ for(UINT i=0;i<GetSize();i++) (At(i))op; return *this;}
		UNARY_OP_Postfix(++)
		UNARY_OP_Postfix(--)
#undef UNARY_OP_Postfix


	////////////////////////////////////////////////////////////////////////////////////////
	// 3.2 Set all elements with machine zero
	DYNTYPE_FUNC void Zero()
	{	__static_if(TypeTraits_Item::IsAggregate)
		{	__static_if(BaseVector::IsCompactLinear)
			{ ZeroMemory(&At(0),sizeof(ItemType)*GetSize()); }
			__static_if(!BaseVector::IsCompactLinear)
			{ for(SIZETYPE i=0;i<GetSize();i++)At(i) = 0; }
		}
		__static_if(!TypeTraits_Item::IsAggregate)
		{ for(SIZETYPE i=0;i<GetSize();i++)At(i).Zero(); }
	}

	////////////////////////////////////////////////////////////////////////////////////////
	// 3.3 Set all item with given value
	DYNTYPE_FUNC void SetItem(const ItemType & x)
	{ for(SIZETYPE i=0;i<GetSize();i++)At(i) = x; }

	DYNTYPE_FUNC void Set(const FundamentalItemType & x)				
	{	__if_not_exists(ItemType::Set){ SetItem(x); }
		__if_exists(ItemType::Set)
		{ for(SIZETYPE i=0;i<GetSize();i++)At(i).Set(x); }
	}																		

	////////////////////////////////////////////////////////////////////////////////////////
	// 3.8 Dot product/Sum/L2Norm_Sqr
	// 3.8.1 Dot product
	template< typename T, class BV >
	DYNTYPE_FUNC _ItemValueType Dot(const _TypedVector<T,BV> & y ) const
	{	ASSERT(GetSize() == rt::GetSize(y));
		USING_EXPRESSION;
		typedef _ItemValueType t_Accum;
		ASSERT_STATIC(::rt::TypeTraits<t_Accum>::IsAggregate);
		if(GetSize())
		{	t_Accum tot = y.At(0)*At(0);
			for(SIZETYPE i=1;i<GetSize();i++){ tot = tot + At(i)*y.At(i); }
			return tot;
		}else{ return t_Accum(0); }
	}

	// 3.8.2 Sum
	DYNTYPE_FUNC _ItemValueType Sum() const
	{	USING_EXPRESSION;
		typedef _ItemValueType t_Accum;
		ASSERT_STATIC(::rt::TypeTraits<t_Accum>::IsAggregate);
		if(GetSize())
		{	t_Accum tot = At(0);
			for(SIZETYPE i=1;i<GetSize();i++){ tot = tot + At(i); }
			return tot;
		}else{ return t_Accum(0); }
	}

	// 3.8.3 L2Norm_Sqr
	DYNTYPE_FUNC _ItemValueType L2Norm_Sqr() const
	{	USING_EXPRESSION;
		typedef _ItemValueType t_Accum;
		ASSERT_STATIC(::rt::TypeTraits<t_Accum>::IsAggregate);
		if(GetSize())
		{	t_Accum tot = Sqr(At(0));
			for(SIZETYPE i=1;i<GetSize();i++){ tot = tot + Sqr(At(i)); }
			return tot;
		}else{ return t_Accum(0); }
	}

	// 3.8.4 L2Distance_Sqr
	template< typename T, class BV >
	DYNTYPE_FUNC _ItemValueType Distance_Sqr(const _TypedVector<T,BV> & y) const
	{	USING_EXPRESSION;
		typedef _ItemValueType t_Accum;
		ASSERT_STATIC(::rt::TypeTraits<t_Accum>::IsAggregate);
		if(GetSize())
		{	t_Accum tot = Sqr(At(0)-y.At(0));
			for(SIZETYPE i=1;i<GetSize();i++){ tot = tot + Sqr(At(i)-y.At(i)); }
			return tot;
		}else{ return t_Accum(0); }
	}

	//__static_if(!(rt::IsTypeSame<_ItemValueType,typename TypeTraits_Item::t_Accum >::Result))
	//{//Ex version, accumulate on t_Accum
	//	// 3.8.1 Dot product
	//	template< class t_Vec2 >
	//	DYNTYPE_FUNC typename TypeTraits_Item::t_Accum Dot_Ex(const t_Vec2 & y ) const
	//	{	ASSERT(GetSize() == ::rt::GetSize(y));
	//		typename TypeTraits_Item::t_Accum t_Accum;
	//		if(GetSize())
	//		{	t_Accum tot = y[0]*At(0);
	//			for(UINT i=1;i<GetSize();i++){ tot = tot + At(i)*y[i]; }
	//			return tot;
	//		}else{ return t_Accum(0); }
	//	}

	//	// 3.8.2 Sum
	//	DYNTYPE_FUNC typename TypeTraits<ItemType>::t_Accum Sum_Ex() const
	//	{	typename TypeTraits_Item::t_Accum t_Accum;
	//		if(GetSize())
	//		{	t_Accum tot = At(0);
	//			for(UINT i=1;i<GetSize();i++){ tot += At(i); }
	//			return tot;
	//		}else{ return t_Accum(0); }
	//	}

	//	// 3.8.3 L2Norm_Sqr
	//	DYNTYPE_FUNC typename TypeTraits_Item::t_Accum L2Norm_Sqr_Ex() const
	//	{	typename TypeTraits_Item::t_Accum t_Accum;
	//		if(GetSize())
	//		{	t_Accum tot = Sqr(At(0));
	//			for(UINT i=1;i<GetSize();i++){ tot = tot + Sqr(At(i)); }
	//			return tot;
	//		}else{ return t_Accum(0); }
	//	}
	//}

	////////////////////////////////////////////////////////////////////////////////////////
	// 3.10 Randomize elements
	DYNTYPE_FUNC void Random()
	{	for(SIZETYPE i=0;i<GetSize();i++){ rt::_meta_::_Random(At(i)); } }


////////////////////////////////////////////////////////////////////////////////////////////
// 4. per-element manipulations
public:
	////////////////////////////////////////////////////////////////////////////////////////
	// 4.1 Assignment
	template<typename t_ItemType2, typename BaseVec2>
	DYNTYPE_FUNC void CopyTo(_TypedVector<t_ItemType2,BaseVec2> & x) const
	{	ASSERT(GetSize()==x.GetSize());
		typedef rt::IsTypeSame<t_ItemType2,ItemType> IsSame;
		__static_if(TypeTraits_Item::IsAggregate && IsSame::Result && BaseVector::IsCompactLinear && BaseVec2::IsCompactLinear)
		{ memcpy(&x.At(0),&At(0),sizeof(ItemType)*GetSize()); return; }
		__static_if(!TypeTraits_Item::IsAggregate || !IsSame::Result || !BaseVector::IsCompactLinear || !BaseVec2::IsCompactLinear)
		{ for(SIZETYPE i=0;i<GetSize();i++)x[i] = At(i); return; }
		ASSERT(0); //avoid both __static_if fails
	}
	template< class t_ItemType2 >
	DYNTYPE_FUNC void CopyTo( t_ItemType2 * p) const
	{	ASSERT_ARRAY(p,GetSize());
		typedef rt::IsTypeSame<t_ItemType2,ItemType> IsSame;
		__static_if(TypeTraits_Item::IsAggregate && IsSame::Result && BaseVector::IsCompactLinear)
		{ memcpy(p,&At(0),sizeof(ItemType)*GetSize()); return; }
		__static_if(!TypeTraits_Item::IsAggregate || !IsSame::Result || !BaseVector::IsCompactLinear)
		{ for(SIZETYPE i=0;i<GetSize();i++)p[i] = At(i); return; }
		ASSERT(0); //avoid both __static_if fails
	}
	template<typename T, typename BaseVec2>
	DYNTYPE_FUNC void CopyFrom(const _TypedVector<T,BaseVec2> & x)
	{	ASSERT(GetSize()==x.GetSize());
#pragma warning(disable:4244)
		typedef rt::IsTypeSame<T,ItemType> IsSame;
		__static_if(TypeTraits_Item::IsAggregate && IsSame::Result && BaseVector::IsCompactLinear && BaseVec2::IsCompactLinear)
		{ memcpy(&At(0),&x.At(0),sizeof(ItemType)*GetSize()); return; }
		__static_if(!TypeTraits_Item::IsAggregate || !IsSame::Result || !BaseVector::IsCompactLinear || !BaseVec2::IsCompactLinear)
		{ for(SIZETYPE i=0;i<GetSize();i++)At(i) = x.At(i); return; }
#pragma warning(default:4244)
		ASSERT(0); //avoid both __static_if fails
	}
	template< class t_ItemType2 >
	DYNTYPE_FUNC void CopyFrom(const t_ItemType2 * p)
	{	ASSERT_ARRAY(p,GetSize());
		typedef rt::IsTypeSame<t_ItemType2,ItemType> IsSame;
		__static_if(TypeTraits_Item::IsAggregate && IsSame::Result && BaseVector::IsCompactLinear)
		{ memcpy(&At(0),p,sizeof(ItemType)*GetSize()); return; }
		__static_if(!TypeTraits_Item::IsAggregate || !IsSame::Result || !BaseVector::IsCompactLinear)
		{ for(SIZETYPE i=0;i<GetSize();i++)At(i) = p[i]; return; }
		ASSERT(0); //avoid both __static_if fails
	}

	////////////////////////////////////////////////////////////////////////////////////////
	// 4.1 Inverse element order
	DYNTYPE_FUNC void Flip()
	{	for(SIZETYPE i=0;i<GetSize()/2;i++)
			rt::Swap(At(i),At(GetSize()-1-i));
	}

	////////////////////////////////////////////////////////////////////////////////////////
	// 4.2 Randomize element order
	DYNTYPE_FUNC void Shuffle()
	{	for(SIZETYPE i=0;i<GetSize()-1;i++)
		rt::Swap(At(i),At(i+::rand()%(GetSize()-i)));
	}

////////////////////////////////////////////////////////////////////////////////////////////
// 5. basic num operations
public:
	////////////////////////////////////////////////////////////////////////////////////////
	// 5.1 Search item
	template<typename T>
	DYNTYPE_FUNC SIZETYPE SearchItem( const T& x ) const
	{	for(SIZETYPE i=0;i<GetSize();i++)
			if(At(i) == x)return i;
		return -1;
	}

	////////////////////////////////////////////////////////////////////////////////////////
	// 5.2 Min/Max
	DYNTYPE_FUNC ItemType Min() const
	{	ASSERT(GetSize());
		ItemType CurMin = At(0);
		for(SIZETYPE i=1;i<GetSize();i++)
		{	ItemType val = At(i);
			if( CurMin<=val ){}
			else{ CurMin=val; }
		}
		return CurMin;
	}
	DYNTYPE_FUNC UINT MinIndex() const
	{	ASSERT(GetSize());
		ItemType CurMin = At(0);
		UINT min_id=0;
		for(SIZETYPE i=1;i<GetSize();i++)
		{	ItemType val = At(i);
			if( CurMin<=val ){}
			else{ CurMin=val; min_id=i; }
		}
		return min_id;
	}
	DYNTYPE_FUNC ItemType Max() const
	{	ASSERT(GetSize());
		ItemType CurMax = At(0);
		for(SIZETYPE i=1;i<GetSize();i++)
		{	ItemType val = At(i);
			if( CurMax>=val ){}
			else{ CurMax=val; }
		}
		return CurMax;
	}
	DYNTYPE_FUNC UINT MaxIndex() const
	{	ASSERT(GetSize());
		ItemType CurMax = At(0);
		UINT max_id=0;
		for(SIZETYPE i=1;i<GetSize();i++)
		{	ItemType val = At(i);
			if( CurMax>=val ){}
			else{ CurMax=val; max_id=i; }
		}
		return max_id;
	}

	////////////////////////////////////////////////////////////
	//Persisentence
	//Intergate with w32::CFile64/w32::CFile64_Composition
public:
	__if_not_exists(BaseVec::Load) //provide default Save/Load function if BaseVector not provided
	{
		template<class t_w32_file>
		HRESULT	Load(t_w32_file & file)	//Load from storage
		{	if(!file.IsOpen())return E_INVALIDARG;
			ULONGLONG org_pos = file.GetCurrentPosition();
			HRESULT hr = E_INVALIDARG;

			BOOL TypeMismatch;
			DWORD dw=0;
			file.Read(&dw,sizeof(DWORD));
			if( dw == 0x564d4d53 )
			{
				file.Read(&dw,sizeof(DWORD));
				__static_if(rt::TypeTraits<ItemType>::IsAggregate)
				{
					if( sizeof(ItemType) != dw )return E_ABORT;
				}

				file.Read(&dw,sizeof(DWORD)); //Typeid

				DWORD type_sign=0;
				{	__static_if( rt::TypeTraits<ItemType>::IsAggregate )
					{	type_sign = rt::TypeTraits<FundamentalItemType>::Typeid; /*Fundamental type id*/	}
					type_sign |= (rt::TypeTraits<ItemType>::Typeid)<<16; //element type id
				}
				TypeMismatch = ( dw != type_sign );

				ULONGLONG sz=0;
				{	
					if(file.Read(&sz,sizeof(ULONGLONG))!=sizeof(ULONGLONG))return t_w32_file::HresultFromLastError();
#ifdef _WIN64
					if(!SetSize(sz))return E_OUTOFMEMORY;
#else
					SIZETYPE ss = (SIZETYPE)sz;
					if( sz == (ULONGLONG)ss &&
						SetSize(ss)
					){}
					else return E_OUTOFMEMORY;
#endif
				}

				if(sz)
				{	if(_p)
					{	
						__static_if(!rt::TypeTraits<ItemType>::IsAggregate)
						{// non-Aggregate elements, t_w32_file should be CFile64_Composition
							TCHAR SectionName[20];
							for(SIZETYPE i=0;i<GetSize();i++)
							{	_stprintf(SectionName,_T("#%d"),i);
								if(file.EnterSection(SectionName))
								{	hr = At(i).Load(file);
									file.ExitSection();
								}else{ hr = file.HresultFromLastError(); }
								if(FAILED(hr))break;
							}
						}
						__static_if(rt::TypeTraits<ItemType>::IsAggregate)
						{// Aggregate elements
							__static_if(BaseVector::IsCompactLinear)
							{	sz *=sizeof(ItemType);
							#ifdef _WIN64
								ULONGLONG base = 0;
								hr = S_OK;
								do
								{	UINT len = (UINT)__min(sz-base,1024*65536);
									if(len != file.Read(_p+base,len))
									{
										hr = t_w32_file::HresultFromLastError();
										break;
									}
									base += len;
								}while(base<sz);
							#else
								hr = (sz == file.Read(_p,(UINT)sz))?(S_OK):(t_w32_file::HresultFromLastError());
							#endif
							}
							__static_if(!BaseVector::IsCompactLinear)
							{	hr = S_OK;
								for(SIZETYPE i=0;i<sz;i++)
								{	if(sizeof(ItemType) == file.Read(&At(i),sizeof(ItemType))){}
									else{ hr = t_w32_file::HresultFromLastError(); break; }
								}
							}
						}
					}else{ hr = E_OUTOFMEMORY; }
				}else return TypeMismatch?S_FALSE:S_OK;
			}

			if(FAILED(hr)){ file.Seek(org_pos); SetSize(); }
			return hr;
		}
	}

	__if_not_exists(BaseVec::Save) //provide default Save/Load function if BaseVector not provided
	{
		template<class t_w32_file>
		HRESULT Save(t_w32_file & file) const //save to storage
		{
			ASSERT(file.IsOpen());
			ULONGLONG org_pos = file.GetCurrentPosition();

			DWORD dw = 0x564d4d53;		//signature "SMMV"
			file.Write(&dw,sizeof(DWORD));

			__static_if(rt::TypeTraits<ItemType>::IsAggregate)
			{
				dw = sizeof(ItemType);	//byte-per-element, element size	
			}
			__static_if(!rt::TypeTraits<ItemType>::IsAggregate)
			{
				dw = 0;
			}
			
			file.Write(&dw,sizeof(DWORD));
			
			dw = 0;
			__static_if( rt::TypeTraits<ItemType>::IsAggregate )
			{	dw = rt::TypeTraits<FundamentalItemType>::Typeid; /*Fundamental type id*/	}
			dw |= (rt::TypeTraits<ItemType>::Typeid)<<16; //element type id
			file.Write(&dw,sizeof(DWORD));

			ULONGLONG sz = GetSize();
			file.Write(&sz,sizeof(ULONGLONG)); //element count

			HRESULT hr = S_OK;

			__static_if(!rt::TypeTraits<ItemType>::IsAggregate)
			{// non-Aggregate elements, t_w32_file should be CFile64_Composition
				TCHAR SectionName[20];
				for(UINT i=0;i<GetSize();i++)
				{	_stprintf(SectionName,_T("#%d"),i);
					if(file.EnterSection(SectionName))
					{	hr = At(i).Save(file);
						file.ExitSection();
					}else{ hr = E_INVALIDARG; }
					if(FAILED(hr))break;
				}
			}
			__static_if(rt::TypeTraits<ItemType>::IsAggregate)
			{// Aggregate elements
				__static_if(BaseVector::IsCompactLinear)
				{	sz *= sizeof(ItemType); //all data
				#ifdef _WIN64
					ULONGLONG base = 0;
					hr = S_OK;
					do
					{	UINT len = (UINT)min(sz-base,1024*65536);
						if(len != file.Write(_p+base,len))
						{
							hr = t_w32_file::HresultFromLastError();
							break;
						}
						base += len;
					}while(base<sz);
				#else
					hr = (sz == file.Write(_p,(UINT)sz))?(S_OK):(t_w32_file::HresultFromLastError());
				#endif
				}
				__static_if(!BaseVector::IsCompactLinear)
				{	for(SIZETYPE i=0;i<sz;i++)
					{	if(sizeof(ItemType) == file.Write(&At(i),sizeof(ItemType))){}
						else{ hr = t_w32_file::HresultFromLastError(); break; }
					}
					hr = S_OK;
				}
			}

			if(FAILED(hr)){ file.Seek(org_pos); }
			return hr;
		}
	}
};
template<class t_Ostream, typename t_Ele, class BaseVector>
t_Ostream& operator<<(t_Ostream& Ostream, const rt::_TypedVector<t_Ele,BaseVector> & vec)
{	return rt::_meta_::TextDump(Ostream,vec); }
template<class t_Istream, typename t_Ele, class BaseVector>
t_Istream& operator>>(t_Istream& Istream, rt::_TypedVector<t_Ele,BaseVector> & vec)
{	return rt::_meta_::TextLoad(Istream,vec); }


namespace _meta_
{
template<class t_Ostream>
DYNTYPE_FUNC BOOL IsStreamStandard(const t_Ostream& Ostream)
{	return	(((LPCVOID)&Ostream) == &std::cout) || 
			(((LPCVOID)&Ostream) == &std::cerr) || 
			(((LPCVOID)&Ostream) == &std::wcout)|| 
			(((LPCVOID)&Ostream) == &std::wcerr);
}

template<typename T,bool is_static_len_array = (rt::TypeTraits<T>::IsArray && rt::TypeTraits<T>::Length!=TYPETRAITS_SIZE_UNKNOWN),
					bool is_builtin = (rt::NumericTraits<T>::IsBuiltInNumeric || rt::TypeTraits<T>::Typeid == _typeid_pointer)
		>
struct EstimateTextWidth
{	static const int Result = 1000;
	static const bool IsBuiltIn = false; 
};
	template<typename T,bool _b> struct EstimateTextWidth<T,true,_b>
	{	static const int Result = (EstimateTextWidth<typename ::rt::TypeTraits<T>::t_Element>::Result+2) * rt::TypeTraits<T>::Length; 
		static const bool IsBuiltIn = false ;
	};
	template<typename T,bool _b> struct EstimateTextWidth<T,_b,true>
	{	static const int Result = ::rt::TypeTraits<T>::text_output_spec::width_total ; 
		static const bool IsBuiltIn = true ;
	};

template<class t_Ostream, typename T>
t_Ostream& TextDump(t_Ostream& Ostream, const T & vec)
{		
	__static_if(!rt::TypeTraits<T>::IsArray){ return Ostream<<vec; }

	__static_if(rt::TypeTraits<T>::IsArray)
	{	static const TCHAR* _pComma = _T(", ");

		SIZE_T len = rt::GetSize(vec);
		if(!len){ Ostream<<_T("{ -NULL- }"); return Ostream; }
		
		typedef typename ::rt::Remove_Qualifer<typename rt::TypeTraits<T>::t_Element>::t_Result t_Ele;
		static const SIZE_T static_len = rt::TypeTraits<T>::Length ;

		static const bool is_text = ::rt::IsTypeSame<typename t_Ostream::char_type,t_Ele>::Result ;
		__static_if( is_text )
		{// text output
			__static_if(static_len!=1){
			Ostream<<_T('\"');
			for(SIZE_T i=0;i<len;i++)
				if(vec.At(i)){ Ostream<<vec.At(i); }
			Ostream<<_T('\"');
			}
			__static_if(static_len==1){
			Ostream<<_T('\'');
			if(vec.At(0)){ Ostream<<vec.At(0); }else{ Ostream<<_T("\0"); }
			Ostream<<_T('\'');
			}
			return Ostream;
		}

		typedef rt::_meta_::EstimateTextWidth<t_Ele>	t_Ele_Width;

		__static_if(static_len!=1){ Ostream<<((static_len<=4)?_T('('):_T('{')); }
		{
			__static_if(t_Ele_Width::IsBuiltIn)
			{	
				__static_if( rt::TypeTraits<t_Ele>::Typeid == rt::_typeid_pointer )
				{// any pointers
					UINT ele_limite = IsStreamStandard(Ostream)?((RTB_TEXTDUMP_LINEWIDTH_HINT-10)/(2+t_Ele_Width::Result)):UINT_MAX;
					ele_limite = max(4,ele_limite);
					static const int tw = t_Ele_Width::Result;

					Ostream<<std::setfill(_T('0'));
					if( len <=ele_limite )
					{	// output all
						for(SIZE_T i=0;i<len-1;i++)
							Ostream<<_T('0')<<_T('x')<<std::setw(tw)<<((const void*)vec.At(i))<<_pComma;
						Ostream<<_T('0')<<_T('x')<<std::setw(tw)<<((const void*)vec.At(len-1));
					}
					else
					{	// output head and tail
						UINT header = ele_limite-3;
						for(SIZE_T i=0;i<header;i++)Ostream<<_T('0')<<_T('x')<<std::setw(tw)<<((const void*)vec.At(i))<<_pComma;
						Ostream<<_T('0')<<_T('x')<<std::setw(tw)<<((const void*)vec.At(header))<<_T(", ... 0x");
						Ostream<<std::setw(tw)<<((const void*)vec.At(len-2));
						Ostream<<_pComma<<_T('0')<<_T('x')<<std::setw(tw)<<((const void*)vec.At(len-1));
						//also element count
						Ostream<<_T(' ')<<_T(':')<<len;
					}
					Ostream<<std::setbase(10)<<std::setfill(_T(' '));
				}
				__static_if( rt::NumericTraits<t_Ele>::IsBuiltInNumeric )
				{// BYTEs and SHORTs and other built-in numeric types
					typedef rt::TypeTraits<t_Ele> t_Ele_TR;
					typedef typename t_Ele_TR::t_Val ValType;
					UINT ele_limite = IsStreamStandard(Ostream)?((RTB_TEXTDUMP_LINEWIDTH_HINT-10)/(2+(t_Ele_Width::Result))):UINT_MAX;
					ele_limite = __max(4,ele_limite);
					static const int tw = t_Ele_Width::Result;

					__static_if(t_Ele_TR::text_output_spec::width_after_point!=TYPETRAITS_SIZE_UNKNOWN)
					{
						Ostream	<<std::setiosflags(std::ios_base::fixed)
								<<std::setprecision(t_Ele_TR::text_output_spec::width_after_point);
					}
					__static_if( rt::TypeTraits<t_Ele>::Typeid == rt::_typeid_bool )
					{
						if( len <=ele_limite )
						{	// output all
							for(SIZE_T i=0;i<len-1;i++)
								Ostream<<(((ValType)vec.At(i))?_T('T'):_T('F'))<<_pComma;
							Ostream<<(((ValType)vec.At(len-1))?_T('T'):_T('F'));
						}
						else
						{	// output head and tail
							UINT header = ele_limite-3;
							for(SIZE_T i=0;i<header;i++)Ostream<<(((ValType)vec.At(i))?_T('T'):_T('F'))<<_pComma;
							Ostream<<(((ValType)vec.At(header))?_T('T'):_T('F'));
							Ostream<<_T(", ... ")<<(((ValType)vec.At(len-2))?_T('T'):_T('F'));
							Ostream<<_pComma<<(((ValType)vec.At(len-1))?_T('T'):_T('F'));
							//also element count
							Ostream<<_T(' ')<<_T(':')<<len;
						}
					}
					__static_if( rt::TypeTraits<t_Ele>::Typeid != rt::_typeid_bool )
					{
						if( len <=ele_limite )
						{	// output all
							for(SIZE_T i=0;i<len-1;i++)
								Ostream<<std::setw(tw)<<((ValType)vec.At(i))<<_pComma;
							Ostream<<std::setw(tw)<<((ValType)vec.At(len-1));
						}
						else
						{	// output head and tail
							UINT header = ele_limite-3;
							for(SIZE_T i=0;i<header;i++)Ostream<<std::setw(tw)<<((ValType)vec.At(i))<<_pComma;
							Ostream<<std::setw(tw)<<((ValType)vec.At(header));
							Ostream<<_T(", ... ")<<std::setw(tw)<<((ValType)vec.At(len-2));
							Ostream<<_pComma<<std::setw(tw)<<((ValType)vec.At(len-1));
							//also element count
							Ostream<<_T(' ')<<_T(':')<<len;
						}
					}
					Ostream<<std::resetiosflags(std::ios_base::fixed)<<std::setprecision(0);
				}
			}
			
			__static_if(!t_Ele_Width::IsBuiltIn)
			{
				__static_if((t_Ele_Width::Result+2)*4 > (RTB_TEXTDUMP_LINEWIDTH_HINT-5)) //one line for one element
				{	UINT ele_limite = IsStreamStandard(Ostream)?8:UINT_MAX;
					if(len<=ele_limite)
					{	//output all					
						Ostream<<_T('\n');
						for(SIZE_T i=0;i<len-1;i++)
							Ostream<<vec.At(i)<<_T(',')<<_T('\n');
						Ostream<<vec.At(len-1);
						Ostream<<_T('\n');
					}
					else
					{	Ostream<<_T(" // ")<<len<<_T(" Items\n");
						{	//output head and tail
							for(SIZE_T i=0;i<4;i++)Ostream<<vec.At(i)<<_T(',')<<_T('\n');
							Ostream<<vec[4]<<_T("\n  ... ...\n")<<vec.At(len-2)<<_T(',')<<_T('\n');
							Ostream<<vec.At(len-1)<<_T('\n');
						}
					}
				}
				__static_if((t_Ele_Width::Result+2)*4 <= RTB_TEXTDUMP_LINEWIDTH_HINT-5) //one line for all
				{	UINT ele_limite = IsStreamStandard(Ostream)?((RTB_TEXTDUMP_LINEWIDTH_HINT-10)/(t_Ele_Width::Result+2)):UINT_MAX;
					if(len<=ele_limite)
					{	//output all					
						for(SIZE_T i=0;i<len-1;i++)
							Ostream<<vec.At(i)<<_pComma;
						Ostream<<vec.At(len-1);
					}
					else
					{	//output head and tail
						UINT header = ele_limite - 3;
						for(SIZE_T i=0;i<header;i++)Ostream<<vec.At(i)<<_pComma;
						Ostream<<vec.At(header)<<_T(", ... ")<<vec.At(len-2)<<_pComma;
						Ostream<<vec.At(len-1)<<_T(' ')<<_T(':')<<len;
					}					
				}
			}
		}
		__static_if(static_len!=1){ Ostream<<((static_len<=4)?_T(')'):_T('}')); }

		return Ostream;
	}
}

template<class t_Istream, typename T>
t_Istream& TextLoad(t_Istream& Istream, T & vec)
{	ASSERT(0); //not implemented
}

} // namespace _meta_


#define COMMON_CONSTRUCTOR_VEC(clsname)												\
		clsname(){}																	\
		explicit DYNTYPE_FUNC clsname(const clsname& x)								\
		{ SetSizeAs(x); *this=x; }													\
		template<typename T,class BaseVec>											\
		explicit DYNTYPE_FUNC clsname(const rt::_TypedVector<T,BaseVec>& x)			\
		{ SetSizeAs(x); *this=x; }													\

///////////////////////////////////////////////////////////////////////////////////////
// SpecifyItemType
template<class t_Array,typename t_Ele ,
	     bool IsArray = rt::TypeTraits<t_Array>::IsArray,
		 bool IsArray_C = (rt::TypeTraits<t_Array>::Typeid == _typeid_array)
		>
struct SpecifyItemType;
	template<class t_Vec,typename t_Ele,bool b1>
	struct SpecifyItemType<t_Vec,t_Ele,false,b1>{ typedef t_Ele t_Result; };
	template<class t_Vec,typename t_Ele>
	struct SpecifyItemType<t_Vec,t_Ele,true,true>{ typedef t_Ele t_Result[rt::TypeTraits<t_Vec>::Length]; };
	template<class t_Vec,typename t_Ele>
	struct SpecifyItemType<t_Vec,t_Ele,true,false>{ typedef typename t_Vec::SpecifyItemType<t_Ele>::t_Result t_Result; };

} // namespace rt



namespace rt
{
    template<class T, typename t_Val>
    SIZE_T LowerBound(const T& arr, const t_Val &key)	// first item equal to or greater than that of a specified key
    {
        SIZE_T len = rt::GetSize(arr);
		SIZE_T ret;
		if(len)
		{
			if( !(arr[0] < key) ){ ret = 0; goto END; }
			if( !(arr[len - 1] < key) ){}
			else{ ret = len; goto END; }

			ret = len - 1;
			SIZE_T L = 0;
			while ( L + 1 < ret )
			{
				SIZE_T mid = (L + ret) >> 1;
				if ( arr[mid] < key )
					L = mid;
				else
					ret = mid;
					
			}
END:
			typedef TypeTraits<T>::t_Element T2;
			__static_if(rt::NumericTraits<T2>::IsFloat)
			{
				if(ret && rt::IsInRange_CC(key,
					arr[ret-1]+rt::TypeTraits<T2>::Epsilon(),
					arr[ret-1]-rt::TypeTraits<T2>::Epsilon())
				)return ret - 1;
			}
			__static_if(rt::NumericTraits<t_Val>::IsFloat)
			{
				if(ret && rt::IsInRange_CC(arr[ret-1],
					key+rt::TypeTraits<t_Val>::Epsilon(),
					key-rt::TypeTraits<t_Val>::Epsilon())
				)return ret - 1;
			}
			return ret;
		}
		else
		{	return 0;
		}
    }

	template<class T, typename t_Val>
	SIZE_T BinarySearch(const T& arr, const t_Val &key)	// first item equal to key, or length of arr
	{
		SIZE_T r = LowerBound(arr,key);
		SIZE_T len = rt::GetSize(arr);
		if(r<len && key < arr[r])return len;
		return r;
	}


} // namespace rt

namespace rt {



	template<typename T> const T& max_val(const T& a,const T& b){
		return (a>b) ? (a):(b);
	}
	template<typename T> const T& min_val(const T& a,const T& b){
		return (a<b) ? (a):(b);
	}


}
