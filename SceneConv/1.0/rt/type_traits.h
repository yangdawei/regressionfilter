#pragma once

//////////////////////////////////////////////////////////////////////
// Visual Computing Foundation Classes (VCFC)
//					Jiaping Wang  2006.2
//					e_boris2002@hotmail.com
//
// (C) Copyright Jiaping Wang 2004.
//
// Permission to copy, use, modify, sell and distribute this software
// is granted provided this copyright notice appears in all copies.
// This software is provided "as is" without express or implied
// warranty, and with no claim as to its suitability for any purpose.
//
// Absolutely no warranty!!
//////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////
//  Brief Description
//
//  type_traits.h
//
//  The type traits for basic types and customizable type traits framework
//
//  Provides: 
//	TypeTraits: Basic Types, Array, pointer, const
//
//  Option Macros:
//	#define EPSLON_RELAXATION num
//			The relaxation scale for float-point number comparison
//			EPSILON=EPSLON_RELAXATION*FLT_EPSLON ( num=10 by default)
//
//////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////
// Revise History
//
// Inital version	2006.2.23		Jiaping
//					Type traits and ResultType traits were removed
//                  from smallmath.h and placed in this file.
// Output format	2006.8.15		Jiaping
//					Add output format spec for built-in numeric. Affect
//					most TextDump functions
//
//////////////////////////////////////////////////////////////////////


#include <limits.h>
#include <float.h>
#include <stdlib.h>
#include <tchar.h>
#include "compiler_abs.h"

#ifndef DYNTYPE_FUNC
#	define DYNTYPE_FUNC	__forceinline
#endif

#ifndef EPSLON_RELAXATION
#	define EPSLON_RELAXATION 10
#endif

#pragma warning(disable: 4996) //warning C4996: 'xxx' was declared deprecated

//////////////////////////////////////////////////////////////////
#if (!defined _NATIVE_WCHAR_T_DEFINED) && (defined UNICODE)
#error	built-in wchar_t type is disabled, please enable as following: \
		project's Property Pages dialog->[C/C++]->[Language]->[Treat wchar_t as Built-in Type]
#endif



//////////////////////////////////////////////////
// TypeTraits declaration macros
// The following macro can be used in class definition scope only
#define TYPETRAITS_DECL_TYPE_NAME(name_string_ansi)	public:template<class ostream> __forceinline static void __TypeName(ostream & outstr){ outstr<<name_string_ansi; }
#define TYPETRAITS_DECL_TYPEID(_type_id)			public:enum{ __Typeid = _type_id };
#define TYPETRAITS_DECL_EXTENDTYPEID(_type_id)		public:enum{ __ExtendTypeid = _type_id };
#define TYPETRAITS_DECL_ACCUM_TYPE(accum_type)		public:typedef accum_type	__t_Accum;
#define TYPETRAITS_DECL_VALUE_TYPE(value_type)		public:typedef value_type	__t_Val;
#define TYPETRAITS_DECL_ELEMENT_TYPE(ele_type)		public:typedef ele_type		__t_Element;
#define TYPETRAITS_DECL_SIGNED_TYPE(signed_type)	public:typedef signed_type	__t_Signed;
#define TYPETRAITS_DECL_IS_AGGREGATE(true_false)	public:enum{ __IsAggregate	= true_false };
#define TYPETRAITS_DECL_IS_NUMERIC(true_false)		public:enum{ __IsNumeric	= true_false };
#define TYPETRAITS_DECL_IS_ARRAY(true_false)		public:enum{ __IsArray		= true_false };	// having At(i) or At(x,y)
#define TYPETRAITS_DECL_IS_IMAGE(true_false)		public:enum{ __IsImage		= true_false };	// having At(x,y)
#define TYPETRAITS_DECL_LENGTH(int_value)			public:enum{ __Length		= int_value };

#define TYPETRAITS_FOLLOW_TYPEID(type_to_follow)		public:enum{ __Typeid = rt::TypeTraits<type_to_follow>::Typeid };
#define TYPETRAITS_FOLLOW_ACCUM_TYPE(type_to_follow)	public:typedef typename rt::TypeTraits<type_to_follow>::t_Accum		__t_Accum;
#define TYPETRAITS_FOLLOW_VALUE_TYPE(type_to_follow)	public:typedef typename rt::TypeTraits<type_to_follow>::t_Val		__t_Val;
#define TYPETRAITS_FOLLOW_ELEMENT_TYPE(type_to_follow)	public:typedef typename rt::TypeTraits<type_to_follow>::t_Element	__t_Element;
#define TYPETRAITS_FOLLOW_SIGNED_TYPE(type_to_follow)	public:typedef typename rt::TypeTraits<type_to_follow>::t_Signed	__t_Signed;
#define TYPETRAITS_FOLLOW_IS_AGGREGATE(type_to_follow)	public:enum{ __IsAggregate	= rt::TypeTraits<type_to_follow>::IsAggregate };
#define TYPETRAITS_FOLLOW_IS_NUMERIC(type_to_follow)	public:enum{ __IsNumeric		= rt::TypeTraits<type_to_follow>::IsNumeric };
#define TYPETRAITS_FOLLOW_IS_ARRAY(type_to_follow)		public:enum{ __IsArray		= rt::TypeTraits<type_to_follow>::IsArray };
#define TYPETRAITS_FOLLOW_IS_IMAGE(type_to_follow)		public:enum{ __IsImage		= rt::TypeTraits<type_to_follow>::IsImage };
#define TYPETRAITS_FOLLOW_LENGTH(type_to_follow)		public:enum{ __Length		= rt::TypeTraits<type_to_follow>::Length };


namespace rt
{

#define TYPETRAITS_SIZE_UNKNOWN		(0)
#define SUBTYPEID_MASK				(0xffff)		// apply to ExtendTypeId

/////////////////////////////////////////////////////////////////
// TypeIds for built-in types, it is not designed to be extended
//////////////////////////////////////////////////////////////
// Qualifiers that can not be nest specified (const/volatile/&)
static const int _typeid_expression		=-5;	// classes for expressions in expr_templ.h
static const int _typeid_virtual_array	=-4;	// class for simulate array from scalar in expr_templ.h
static const int _typeid_volatile		=-3;
static const int _typeid_const			=-2;	
static const int _typeid_ref			=-1;	

static const int _typeid_class			=0;
static const int _typeid_void			=1;

static const int _typeid_bool			=2;		//like unsigned char
static const int _typeid_8u				=3;
static const int _typeid_16u			=4;
static const int _typeid_32u			=5;
static const int _typeid_64u			=6;

static const int _typeid_8s				=7;
static const int _typeid_wchar_t		=8;		//like signed short
static const int _typeid_16s			=9;
static const int _typeid_32s			=10;
static const int _typeid_64s			=11;

static const int _typeid_32f			=12;
static const int _typeid_64f			=13;

// composited types
static const int _typeid_pointer		=0x10;	// C pointer
static const int _typeid_array			=0x20;	// C array

	// extended types
static const int 	_typeid_extend_type_min=0x30;
static const int 	_typeid_codelib=0x30;		// CodeLib containers
static const int 	_typeid_stl=0x40;			// STL containers
static const int 	_typeid_afx=0x50;			// ATL/MFC classes
static const int 	_typeid_w32=0x60;			// Win32 structures
static const int 	_typeid_opencv=0x70;		// OpenCV structures



namespace _ExtendedType
{
	DWORD _DefineExtendedTypeId(...);	// ID 1 indicate unkonwn class
}

template<typename T>class TypeTraits;

namespace _meta_
{
template<typename T> struct _GetExtendedTypeId
{ static const int Result = sizeof(::rt::_ExtendedType::_DefineExtendedTypeId((T*)(NULL)))/sizeof(DWORD); };

/*************************************************************************\
_TypeAgent is another mechanism to override the default or predefined
characteristic of a class T (including TypeTraits/TypeTraitsEx and Member List).
Overriding by defining _TypeAgent or _TypeAgent of _TypeAgent does not required
to modify T's class definition, which customizable TypeTraits mechanism is required
to add some macro such as TYPETRAITS_DECL_xxx). But the overrided characteristic
will NOT be inherited by derived class

The priority of the three kind of TypeTraits descriptions is (high to low)
0.  _TypeAgent<_TypeAgent<...>>	// Higher level of nesting has higher priority
1.  _TypeAgent<T>				// Type Agent
2.  T::__xxxx					// customizable TypeTraits by TYPETRAITS_DECL_xxx
3.  TypeTraits/TypeTraitsEx		// default TypeTrait
\*************************************************************************/

template<typename T>
struct _TypeAgent{ static const bool __IsDefined = false; };
	template<typename T>	// Agent of TypeTraits class is not allowed
	struct _TypeAgent<TypeTraits<T>>{ ASSERT_STATIC(0); };
}

template<typename T> struct HasTypeAgent
{ static const int Result = _meta_::_TypeAgent<T>::__IsDefined; };


namespace _meta_
{
/*************************************************************************\
_TypeTraits_Default is the Base class for "TypeTraits" to providing default
typetrait contents. A concise implementation can be adding __if_not_exists 
part in EXTENSIBLE_* marcos (in the below) to provide the defaults. But the 
Intel compiler does not support both __if_exists/__if_not_exists statement 
appear at the same location in a template class scope (both will be true).                     
\*************************************************************************/
template<	typename T, 
			UINT _extended_typeid = _GetExtendedTypeId<T>::Result, 
			bool has_agent = rt::HasTypeAgent<T>::Result >
class _TypeTraits_Default;
	template<typename T>
	class _TypeTraits_Default<T,1,true>		// non-extended TypeTraits and has type agent
		:public rt::TypeTraits<_meta_::_TypeAgent<T> >{};	
	template<typename T>
	class _TypeTraits_Default<T,1,false>	// non-extended TypeTraits and has't type agent
	{	static const int __IsIntrinsicsAggregate =
						 COMPILER_INTRINSICS_IS_POD(T) || 
						 (	COMPILER_INTRINSICS_HAS_TRIVIAL_CONSTRUCTOR(T) &&
							COMPILER_INTRINSICS_HAS_TRIVIAL_COPY(T) &&
							COMPILER_INTRINSICS_HAS_TRIVIAL_ASSIGN(T) &&
							COMPILER_INTRINSICS_HAS_TRIVIAL_DESTRUCTOR(T)
						 );
	public:
		typedef T		t_Accum;
		typedef T		t_Val;
		typedef void	t_Element;
		typedef T		t_Signed;

		static const int Typeid			= _typeid_class				;
		static const int ExtendTypeid	= _typeid_class<<16			;
		static const bool IsAggregate	= __IsIntrinsicsAggregate	;
		static const bool IsNumeric		= false						;
		static const bool IsArray		= false						;
		static const bool IsImage		= false						;
		static const SIZE_T Length		= 1							;
	public:
		template<class ostream>
		static void TypeName(ostream & outstr)
		{	__if_exists(T::__GetClassName){ outstr<<T::__GetClassName(); return; }
			outstr<<_T("class");
		}
		struct text_output_spec
		{	static const int width_total = TYPETRAITS_SIZE_UNKNOWN;
			static const int width_after_point = TYPETRAITS_SIZE_UNKNOWN;
		};
	};

} // namespace _meta_


// root trait for all types
#define EXTENSIBLE_TYPEDEF(name)	__if_exists(T::__##name){ typedef typename T::__##name name; }
#define EXTENSIBLE_ENUM(name)	 	__if_exists(T::__##name){ static const SIZE_T name = T::__##name; }
template<typename T>
class TypeTraits:public rt::_meta_::_TypeTraits_Default<T>
{	
public:
	EXTENSIBLE_TYPEDEF(t_Accum	)	//type for accumulating values
	EXTENSIBLE_TYPEDEF(t_Val	)	//type for calculation
	EXTENSIBLE_TYPEDEF(t_Element)	//the type of the element (void for n/a)
	EXTENSIBLE_TYPEDEF(t_Signed	)	//signed version type
	
	EXTENSIBLE_ENUM(Typeid		)	//Typeid of the type
	EXTENSIBLE_ENUM(ExtendTypeid)	//ExtendTypeid of the type
	EXTENSIBLE_ENUM(IsAggregate	)	//has trivial ctor/dtor/copy
	EXTENSIBLE_ENUM(IsNumeric	)	//support built-in operattors (+-*/) and =0
	EXTENSIBLE_ENUM(IsArray		)	//support At(..) GetSize/GetWidth/GetHeight
	EXTENSIBLE_ENUM(IsImage		)	//support At(x,y) GetWidth/GetHeight
	EXTENSIBLE_ENUM(Length		)	//count of elements, 0 for types dynamic element number

public:
	__if_exists(T::__TypeName)
	{	template<class ostream> 
		DYNTYPE_FUNC static void TypeName(ostream & outstr){ T::__TypeName(outstr); }
	}
};
#undef EXTENSIBLE_TYPEDEF
#undef EXTENSIBLE_ENUM

///////////////////////////////////////////
// For Internal use only
#define _FOLLOW_ACCUM_TYPE(type_to_follow)		public:typedef typename rt::TypeTraits<type_to_follow>::t_Accum		t_Accum;
#define _FOLLOW_VALUE_TYPE(type_to_follow)		public:typedef typename rt::TypeTraits<type_to_follow>::t_Val		t_Val;
#define _FOLLOW_ELEMENT_TYPE(type_to_follow)	public:typedef typename rt::TypeTraits<type_to_follow>::t_Element	t_Element;
#define _FOLLOW_SIGNED_TYPE(type_to_follow)		public:typedef typename rt::TypeTraits<type_to_follow>::t_Signed	t_Signed;
#define _FOLLOW_IS_AGGREGATE(type_to_follow)	public:static const bool IsAggregate =rt::TypeTraits<type_to_follow>::IsAggregate;
#define _FOLLOW_IS_NUMERIC(type_to_follow)		public:static const bool IsNumeric =	 rt::TypeTraits<type_to_follow>::IsNumeric	;
#define _FOLLOW_IS_ARRAY(type_to_follow)		public:static const bool IsArray =	 rt::TypeTraits<type_to_follow>::IsArray	;
#define _FOLLOW_IS_IMAGE(type_to_follow)		public:static const bool IsImage =	 rt::TypeTraits<type_to_follow>::IsImage	;
#define _FOLLOW_LENGTH(type_to_follow)			public:static const SIZE_T Length =	 rt::TypeTraits<type_to_follow>::Length		;
///////////////////////////////////////////


// void type
template<>
class TypeTraits<void>
{
public:	
	typedef void t_Accum;     
	typedef void t_Val;
	typedef void t_Element;
	typedef void t_Signed;
	static const int Typeid = _typeid_void ;
	static const int ExtendTypeid	= Typeid<<16 ;
	static const bool IsAggregate = false ;
	static const bool IsNumeric = false ;	
	static const bool IsArray = false ;	
	static const bool IsImage = false ;	
	static const SIZE_T Length = 1 ;
	template<class ostream> 
	static void TypeName(ostream & outstr){ outstr<<_T("void");  }
	struct text_output_spec
	{	static const int width_total = TYPETRAITS_SIZE_UNKNOWN;
		static const int width_after_point = TYPETRAITS_SIZE_UNKNOWN;
	};
};
// const qualifier
template<typename t_RawType>
class TypeTraits< const t_RawType >
{
public:
	typedef typename t_RawType	t_Nested;
	_FOLLOW_ACCUM_TYPE(t_RawType);
	_FOLLOW_VALUE_TYPE(t_RawType);
	_FOLLOW_ELEMENT_TYPE(t_RawType);
	_FOLLOW_SIGNED_TYPE(t_RawType);
	static const int Typeid = _typeid_const ;
	static const int ExtendTypeid	= Typeid<<16 ;
	_FOLLOW_IS_AGGREGATE(t_RawType);
	_FOLLOW_IS_NUMERIC(t_RawType);
	_FOLLOW_IS_ARRAY(t_RawType);
	_FOLLOW_IS_IMAGE(t_RawType);
	_FOLLOW_LENGTH(t_RawType);
public:
	template<class ostream> 
	static void TypeName(ostream & outstr)
		{ outstr<<_T('('); rt::TypeTraits<t_Nested>::TypeName(outstr); outstr<<_T(")const"); }
	typedef typename rt::TypeTraits<t_Nested>::text_output_spec text_output_spec;
};
// volatile qualifier
template<typename t_RawType>
class TypeTraits< volatile t_RawType >
{	
public:
	typedef typename t_RawType		t_Nested;
	_FOLLOW_ACCUM_TYPE(t_RawType);
	_FOLLOW_VALUE_TYPE(t_RawType);
	_FOLLOW_ELEMENT_TYPE(t_RawType);
	_FOLLOW_SIGNED_TYPE(t_RawType);
	static const int Typeid = _typeid_volatile ;
	static const int ExtendTypeid	= Typeid<<16 ;
	_FOLLOW_IS_AGGREGATE(t_RawType);
	_FOLLOW_IS_NUMERIC(t_RawType);
	_FOLLOW_IS_ARRAY(t_RawType);
	_FOLLOW_IS_IMAGE(t_RawType);
	_FOLLOW_LENGTH(t_RawType);
public:
	template<class ostream> 
	static void TypeName(ostream & outstr)
		{ outstr<<_T('('); rt::TypeTraits<t_Nested>::TypeName(outstr); outstr<<_T(")volatile"); }
	typedef typename rt::TypeTraits<t_Nested>::text_output_spec text_output_spec;
};
// reference
template<typename t_RawType>
class TypeTraits< t_RawType& >
{	typedef t_RawType T;
public:
	typedef typename t_RawType		t_Nested;
	_FOLLOW_ACCUM_TYPE(t_RawType);
	_FOLLOW_VALUE_TYPE(t_RawType);
	_FOLLOW_ELEMENT_TYPE(t_RawType);
	_FOLLOW_SIGNED_TYPE(t_RawType);
	static const int Typeid = _typeid_ref ;
	static const int ExtendTypeid	= Typeid<<16 ;
	_FOLLOW_IS_AGGREGATE(t_RawType);
	_FOLLOW_IS_NUMERIC(t_RawType);
	_FOLLOW_IS_ARRAY(t_RawType);
	_FOLLOW_IS_IMAGE(t_RawType);
	_FOLLOW_LENGTH(t_RawType);
public:
	template<class ostream> 
	static void TypeName(ostream & outstr)
		{ outstr<<_T('('); rt::TypeTraits<t_Nested>::TypeName(outstr); outstr<<_T(")&"); }
	typedef typename rt::TypeTraits<t_Nested>::text_output_spec text_output_spec;
};
// C array
template<typename t_Ele, SIZE_T t_Size>
class TypeTraits< t_Ele[t_Size] >
{	typedef t_Ele T[t_Size];
public:
	typedef typename TypeTraits<t_Ele>::t_Accum		t_Accum[t_Size];
	typedef typename TypeTraits<t_Ele>::t_Val		t_Val[t_Size];
	typedef t_Ele									t_Element;
	typedef typename TypeTraits<t_Ele>::t_Signed	t_Signed[t_Size];
	static const int Typeid = _typeid_array ;
	static const int ExtendTypeid	= Typeid<<16 ;
	_FOLLOW_IS_AGGREGATE(t_Ele);
	static const bool IsNumeric = false ;	
	static const bool IsArray = true ;
	static const bool IsImage = false ;
	static const SIZE_T Length = t_Size ;
public:
	template<class ostream> 
	static void TypeName(ostream & outstr)
		{ outstr<<_T('('); rt::TypeTraits<t_Ele>::TypeName(outstr); outstr<<_T(")[")<<t_Size<<_T(']'); }
	struct text_output_spec
	{	static const int width_total = TYPETRAITS_SIZE_UNKNOWN ;
		static const int width_after_point = TYPETRAITS_SIZE_UNKNOWN ;
	};
};
// C pointer
template<typename t_Ele>
class TypeTraits< t_Ele* >	//All pointers
{	typedef t_Ele *T;
public:
	typedef typename t_Ele*				t_Accum;
	typedef typename t_Ele*				t_Val;
	typedef t_Ele						t_Element;
	typedef typename t_Ele*				t_Signed;
	static const int Typeid = _typeid_pointer;
	static const int ExtendTypeid	= Typeid<<16 ;
	static const bool IsAggregate = true ;
	static const bool IsNumeric = false ;	
	static const bool IsArray = false ;
	static const bool IsImage = false ;
	static const SIZE_T Length = TYPETRAITS_SIZE_UNKNOWN ;
public:
	template<class ostream>
	static void TypeName(ostream & outstr)
		{ outstr<<_T('('); rt::TypeTraits<t_Ele>::TypeName(outstr); outstr<<_T(")*"); }
	struct text_output_spec
	{	
#ifdef _WIN64
		static const int width_total = 16 ;
#else
		static const int width_total = 8 ;
#endif
		static const int width_after_point = 0 ;
	};
};

#undef _FOLLOW_ACCUM_TYPE	
#undef _FOLLOW_VALUE_TYPE	
#undef _FOLLOW_ELEMENT_TYPE
#undef _FOLLOW_SIGNED_TYPE	
#undef _FOLLOW_IS_AGGREGATE
#undef _FOLLOW_IS_NUMERIC	
#undef _FOLLOW_IS_ARRAY	
#undef _FOLLOW_LENGTH		


/////////////////////////////////////////////////////////////////
//all built-in num types
#define __NumT(T_,Val_,Accum_,Signed_,TypeId_,Eps_,Min_,Max_,tos_tw,tos_wap)	\
		template<>																\
		class TypeTraits<T_>													\
		{public:	typedef Accum_	t_Accum;									\
					typedef Val_	t_Val;										\
					typedef void	t_Element;									\
					typedef Signed_	t_Signed;									\
					static const int Typeid = TypeId_ ;							\
					static const int ExtendTypeid	= Typeid<<16 ;				\
					static const bool IsAggregate = true ;						\
					static const bool IsNumeric = true ;						\
					static const bool IsArray = false ;							\
					static const bool IsImage = false ;							\
					static const SIZE_T Length = 1 ;							\
					template<class ostream>										\
					static void TypeName(ostream & outstr){ outstr<<_T(#T_); }	\
		 public:	DYNTYPE_FUNC static T_	  	Epsilon(){ return Eps_; }		\
					DYNTYPE_FUNC static T_	  	MinVal(){ return Min_; }		\
					DYNTYPE_FUNC static T_	  	MaxVal(){ return Max_; }		\
		 public:	struct text_output_spec										\
					{	static const int width_total = tos_tw ;					\
						static const int width_after_point = tos_wap ;			\
					};															\
		};																		\

//all integer types
#define __NumInt(T_,Val_,Accum_,Signed_,TypeId_,Min_,Max_,tos_tw)				\
		__NumT(T_,Val_,Accum_,Signed_,TypeId_,0,Min_,Max_,tos_tw,0)
//      |Type			 |Value T		  |Accum T		   |Signed Type |Type Id		|Min		|Max		|tot|
#ifdef _CHAR_UNSIGNED
__NumInt(char			 ,int			  ,int			   ,signed char ,_typeid_8u	  	,0			,UCHAR_MAX	,3	)
#else
__NumInt(char			 ,int			  ,int			   ,signed char ,_typeid_8s	  	,SCHAR_MIN	,SCHAR_MAX	,4	)
#endif
__NumInt(signed char	 ,int			  ,int			   ,signed char ,_typeid_8s	  	,SCHAR_MIN	,SCHAR_MAX	,4	)	
__NumInt(short			 ,int			  ,int			   ,signed short,_typeid_16s	,SHRT_MIN	,SHRT_MAX	,6	)	
__NumInt(__wchar_t		 ,int			  ,int			   ,__wchar_t	,_typeid_wchar_t,SHRT_MIN	,USHRT_MAX	,1	)	
__NumInt(long			 ,long			  ,__int64		   ,signed long ,_typeid_32s	,LONG_MIN	,LONG_MAX	,6	)	
__NumInt(int			 ,int			  ,__int64		   ,signed int  ,_typeid_32s	,INT_MIN	,INT_MAX	,6	)	
__NumInt(__int64		 ,__int64		  ,__int64		   ,__int64	    ,_typeid_64s	,LLONG_MIN	,LLONG_MAX	,8	)	
__NumInt(bool			 ,int			  ,int			   ,bool		,_typeid_bool	,0			,1			,1	)	
__NumInt(unsigned char	 ,int			  ,int			   ,signed char ,_typeid_8u	  	,0			,UCHAR_MAX	,3	)	
__NumInt(unsigned short	 ,int			  ,int			   ,signed short,_typeid_16u	,0			,USHRT_MAX	,5	)	
__NumInt(unsigned long	 ,unsigned long	  ,unsigned __int64,signed long ,_typeid_32u	,0			,ULONG_MAX	,5	)	
__NumInt(unsigned int	 ,unsigned int	  ,unsigned __int64,signed int  ,_typeid_32u	,0			,UINT_MAX	,5	)
__NumInt(unsigned __int64,unsigned __int64,unsigned __int64,__int64	    ,_typeid_64u	,0			,ULLONG_MAX	,7	)
#undef __NumInt

//all float-point types
#define __NumFloat(T_,Accum_,TypeId_,Eps_,Min_,Max_,tos_tw,tos_wap)			\
		__NumT(T_,T_,Accum_,T_,TypeId_,Eps_,Min_,Max_,tos_tw,tos_wap)
//		  |Type		    |Accum T	|Type Id		|EPSILON								|Min		|Max		|tot|wap|
__NumFloat(float		,double		,_typeid_32f	,(float)(FLT_EPSILON*EPSLON_RELAXATION)	,-FLT_MAX	,FLT_MAX	,7	,4	)
__NumFloat(double		,long double,_typeid_64f	,DBL_EPSILON*EPSLON_RELAXATION*10		,-DBL_MAX	,DBL_MAX	,9	,6	)
__NumFloat(long double	,long double,_typeid_64f	,LDBL_EPSILON*EPSLON_RELAXATION*10		,LDBL_MAX	,LDBL_MAX	,9	,6	)
#undef __NumFloat

#undef __NumT
//all built-in num types
/////////////////////////////////////////////////////////////////


//////////////////////////////////////////////////
// Trait num type properties
template<typename T_in>
struct NumericTraits
{	typedef typename rt::Remove_QualiferAndRef<T_in>::t_Result T;
	static const int _Typeid = (int)rt::TypeTraits<T>::Typeid ;
	static const bool IsInteger = (_Typeid>=_typeid_bool)&&(_Typeid<=_typeid_64s) ;
	static const bool IsFloat = (_Typeid==_typeid_32f)||(_Typeid==_typeid_64f) ;
	static const bool IsUnsigned = (_Typeid>=_typeid_bool)&&(_Typeid<=_typeid_64u) ;
	static const bool IsSigned = (_Typeid>=_typeid_8s)&&(_Typeid<=_typeid_64s) ;
	static const bool IsBuiltInNumeric = (_Typeid>=_typeid_bool)&&(_Typeid<=_typeid_64f) ;
	static const int  TypePriority = IsBuiltInNumeric?(sizeof(T)*8 + (IsFloat?34:0) + (IsUnsigned?1:0)):0xffff ;
	// non-num is rated as 0xffff
};


//////////////////////////////////////////////////
// Test if built-in POD
template<typename T>
struct IsBuiltInType
{
	static const int Result =  (((int)TypeTraits<T>::Typeid)>=_typeid_bool && ((int)TypeTraits<T>::Typeid)<=_typeid_64f)
							|| ((int)TypeTraits<T>::Typeid)==_typeid_pointer;
};


//////////////////////////////////////////////////
// Trait fundamental element type 
// fundamental element type is a num type or a non-array type
template<class T, bool TraitElement	=(rt::TypeTraits<T>::IsArray==true),// && (TypeTraits<T>::IsNumeric==false) ,
				  bool TraitNest	=((rt::TypeTraits<T>::Typeid)<0) >
struct FundamentalType;
	template<class T> struct FundamentalType<T,false,false>
	{typedef T t_Result; };
	template<class T> struct FundamentalType<T,false,true>
	{typedef typename rt::FundamentalType< typename rt::TypeTraits< T >::t_Nested >::t_Result t_Result; };
	template<class T, bool b1> struct FundamentalType<T,true,b1>
	{typedef typename rt::FundamentalType< typename rt::TypeTraits< T >::t_Element>::t_Result t_Result; };


////////////////////////////////////////////////////
// the number of nested level of a array type
template< typename T, bool is_array=TypeTraits<T>::IsArray>
struct TypeDimension;
	template< typename T> struct TypeDimension<T,true>
	{ static const int Result = rt::TypeDimension<typename TypeTraits<T>::t_Element>::Result+1; };
	template< typename T> struct TypeDimension<T,false>
	{ static const int Result = 0; };

////////////////////////////////////////////////////
// square and cubic inline
template<typename T>
DYNTYPE_FUNC typename TypeTraits<T>::t_Val	Sqr(const T& x){ return x*x; }

template<typename T>
DYNTYPE_FUNC typename TypeTraits<T>::t_Val	Cubic(const T& x){ return x*x*x; }

namespace _meta_
{
// Type index
template<typename T1, typename T2, int index > 
struct _TypeIndex{ typedef void t_Result; }; //void for other index values
	template<typename T1, typename T2> //Index 0
	struct _TypeIndex<T1,T2,0>{ typedef T1 t_Result; };
	template<typename T1, typename T2> //Index 1
	struct _TypeIndex<T1,T2,1>{ typedef T2 t_Result; };

} // namespace _meta_


//////////////////////////////////////////////////
// General get size
template<typename T>
DYNTYPE_FUNC SIZE_T GetSize(const T& x)	// return count of elements for container types
{	__if_exists(T::GetSize)
	{
		return (SIZE_T)x.GetSize();
	}
	__if_not_exists(T::GetSize)
	{	
		static const int _typeid = rt::TypeTraits<T>::Typeid;
		__static_if(_typeid == rt::_typeid_stl)
		{	return (SIZE_T)x.size();
		}
		__static_if(_typeid == rt::_typeid_afx)
		{
			__if_exists(T::GetCount){ return (SIZE_T)x.GetCount(); }
			__if_exists(T::GetLength){ return (SIZE_T)x.GetLength(); }
		}
		__static_if(_typeid != rt::_typeid_stl && _typeid != rt::_typeid_afx)
		{		
			ASSERT_STATIC(rt::TypeTraits<T>::Length != 0);
			return rt::TypeTraits<T>::Length;
		}

	}
}

//////////////////////////////////////////////////
// General get row count
template<typename T>
DYNTYPE_FUNC UINT GetRowCount(const T& x)	// return count of elements for container types
{
	static const int _typeid = rt::TypeTraits<T>::Typeid;
	__static_if(_typeid == rt::_typeid_opencv)
	{	return (UINT)x.rows;
	}
}

//////////////////////////////////////////////////
// General get column count
template<typename T>
DYNTYPE_FUNC UINT GetColCount(const T& x)	// return count of elements for container types
{
	static const int _typeid = rt::TypeTraits<T>::Typeid;
	__static_if(_typeid == rt::_typeid_opencv)
	{	return (UINT)x.cols;
	}
}

namespace expr{}

}//namespace rt


#define USING_EXPRESSION using namespace ::rt::expr;

#define DEFAULT_TYPENAME(clsname)														\
		template<class ostream> static void __TypeName(ostream & outstr)				\
		{	outstr<<_T("rt::") _T(#clsname) _T("<");									\
			TypeTraits_Item::TypeName(outstr); outstr<<_T('>');							\
		}																				\
		static const int MARCO_JOIN(is_,clsname);										\

