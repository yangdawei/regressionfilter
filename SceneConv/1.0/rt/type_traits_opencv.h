#pragma once

//////////////////////////////////////////////////////////////////////
// Revise History
//
// Inital version	2010.05.26		Peiran Ren
//
//////////////////////////////////////////////////////////////////////

#ifdef _CV_H_

#include "type_traits_ext.h"


#ifndef max
#define max(a,b)            (((a) > (b)) ? (a) : (b))
#endif

#ifndef min
#define min(a,b)            (((a) < (b)) ? (a) : (b))
#endif

namespace rt{
namespace _meta_{
	__forceinline LPCTSTR _text_opencv_(){ return _T("cv::"); }
}} // namespace _meta_ / namespace rt

//__OPENCV_CONTAINER1(Vec,	true,	1)
	namespace cv{ template<class,int> class Vec; }
	namespace rt{
	enum{ _typeid_opencv_Vec = (_typeid_opencv<<16)+1 };
	namespace _ExtendedType{	template<class T, int _Cn>
								::rt::_meta_::_ExtenedTypeId_Struct<_typeid_opencv_Vec>
									_DefineExtendedTypeId(cv::Vec<T, _Cn>* p);
	} /* _ExtendedType */
	template<class t_Ele, int t_Size>
	class TypeTraits< cv::Vec<t_Ele,t_Size> >
	{public:
		typedef cv::Vec<typename TypeTraits<t_Ele>::t_Accum	,t_Size>	t_Accum;
		typedef cv::Vec<typename TypeTraits<t_Ele>::t_Val	,t_Size>	t_Val;
		typedef cv::Vec<typename TypeTraits<t_Ele>::t_Signed,t_Size>	t_Signed;
	//__OPENCV_CONTAINER_TYPENAME(Vec)
		template<class ostream>
		static void TypeName(ostream & outstr)
			{	outstr<<_meta_::_text_opencv_()<<_T("Vec")<<_T('<');
				rt::TypeTraits<t_Ele>::TypeName(outstr); outstr<<_T('>');
			}
	//__OPENCV_CONTAINER_BODY(Vec,is_array)		
		typedef t_Ele										t_Element;
		enum{ Typeid = _typeid_opencv };
		enum{ ExtendTypeid = _typeid_opencv_Vec };
		enum{ IsAggregate = false};	
		enum{ IsNumeric = false };
		enum{ IsArray = true };	
		enum{ IsImage = false };
		enum{ Length = t_Size };
	public:	
		struct text_output_spec
		{	enum{ width_total = TYPETRAITS_SIZE_UNKNOWN };
			enum{ width_after_point = TYPETRAITS_SIZE_UNKNOWN };
	};	};
	namespace _ExtendedType{
	template<typename T> class TypeTraitsEx<T, _typeid_opencv_Vec >
		:public ::rt::TypeTraits<T>{};
	}} /* namespace _ExtendedType/rt */


//__OPENCV_CONTAINER2(Mat_,	true,	2)
	namespace cv{ template<class> class Mat_; }	
	namespace rt{
	enum{ _typeid_opencv_Mat = (_typeid_opencv<<16)+2 };
	namespace _ExtendedType{	template<class T>
								::rt::_meta_::_ExtenedTypeId_Struct<_typeid_opencv_Mat>
									_DefineExtendedTypeId(cv::Mat_<T>* p);
	} /* _ExtendedType */
	template<class t_Ele>
	class TypeTraits< cv::Mat_<t_Ele> >	
	{public:
		typedef cv::Mat_<typename TypeTraits<t_Ele>::t_Accum>	t_Accum;
		typedef cv::Mat_<typename TypeTraits<t_Ele>::t_Val>		t_Val;
		typedef cv::Mat_<typename TypeTraits<t_Ele>::t_Signed>	t_Signed;
	//__OPENCV_CONTAINER_TYPENAME(Mat)
		template<class ostream>	
		static void TypeName(ostream & outstr)
			{	outstr<<_meta_::_text_opencv_()<<_T("Mat_")<<_T('<');
				rt::TypeTraits<t_Ele>::TypeName(outstr); outstr<<_T('>');
			}
	//__OPENCV_CONTAINER_BODY(Mat,is_array)	
		typedef t_Ele										t_Element;
		enum{ Typeid = _typeid_opencv };
		enum{ ExtendTypeid = _typeid_opencv_Mat };
		enum{ IsAggregate = false};
		enum{ IsNumeric = false };
		enum{ IsArray = false };	
		enum{ IsImage = true };
		enum{ Length = TYPETRAITS_SIZE_UNKNOWN };							
	public:
		struct text_output_spec
		{	enum{ width_total = TYPETRAITS_SIZE_UNKNOWN };
			enum{ width_after_point = TYPETRAITS_SIZE_UNKNOWN };
	};	};
	namespace _ExtendedType{
	template<typename T> class TypeTraitsEx<T, _typeid_opencv_Mat >
		:public ::rt::TypeTraits<T>{};
	}} /* namespace _ExtendedType/rt */	



#define _MEMBER_TABLE_BEGIN_TEMPLATE(clsname, t_Ele)								\
	public: static LPCTSTR __GetClassName(){										\
		std::basic_stringstream<TCHAR, char_traits<TCHAR>, allocator<TCHAR> > outstr;\
		outstr.flush();																\
		outstr<<#clsname<<_T('<');													\
		rt::TypeTraits<t_Ele>::TypeName(outstr); outstr<<_T('>');					\
		static std::string str = outstr.str();										\
		return str.data();}															\
	static const int HAS_MEMBER_VISITOR ;											\
	template<typename t_MemberVisitor>												\
	HRESULT VisitMembers(t_MemberVisitor & __visitor)								\
	{	HRESULT hr = S_OK;		DWORD flag = 0;										\
		hr=__visitor.Begin(__GetClassName());										\
		if(FAILED(hr))return hr;													\

#define _MEMBER_TABLE_BEGIN_EXTERNAL_TEMPLATE(cls_name)								\
	namespace rt{namespace _meta_{													\
	template<class t_Ele> struct _TypeAgent<cls_name<t_Ele>>: public cls_name<t_Ele>\
	{	static const int __IsDefined = true ;										\
		_MEMBER_TABLE_BEGIN_TEMPLATE(cls_name, t_Ele)								\

	_MEMBER_TABLE_BEGIN_EXTERNAL_TEMPLATE(cv::Rect_)
		_MEMBER_ENTRY(x)
		_MEMBER_ENTRY(y)
		_MEMBER_ENTRY(width)
		_MEMBER_ENTRY(height)
		_MEMBER_TABLE_TYPETRAITS_DECL_BEGIN()
			TYPETRAITS_DECL_TYPEID(rt::_typeid_opencv)
			TYPETRAITS_DECL_EXTENDTYPEID((rt::_typeid_opencv<<16) + 3)
			TYPETRAITS_DECL_IS_AGGREGATE(true)
		_MEMBER_TABLE_TYPETRAITS_DECL_END()
	_MEMBER_TABLE_END_EXTERNAL()

#endif