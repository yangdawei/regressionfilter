#include "stdafx.h"
#include "debug_log.h"
#include "file_comp.h"
#include "..\rt\runtime_base.h"
#include <atlbase.h>
#include <atlconv.h>

#define __PersistDump(x) _CheckDump(x)

//////////////////////////////////
// CFile64_Composition

w32::CFile64_Composition::_OnDemandLoadingObjectList_class w32::CFile64_Composition::m_OnDemandLoadingObjectList;
w32::CFile64_Composition w32::CFile64_Composition::g_File_OnDemandObject;

/////////////////////////////////////////////////////////////////
//support for On-Demand loading
//_OnDemandLoadingItem
w32::CFile64_Composition::_OnDemandLoadingItem::~_OnDemandLoadingItem()
{
	_SafeDelArray(Filename);	
	_SafeDelArray(SectionName); 	
}

w32::CFile64_Composition::_OnDemandLoadingItem::_OnDemandLoadingItem(LPCTSTR fn,LPCTSTR section_full)
{
	Filename = new TCHAR[_tcslen(fn)+1];
	_tcscpy(Filename,fn);
	SectionName = new TCHAR[_tcslen(section_full)+1];
	_tcscpy(SectionName,section_full);
}

w32::CFile64_Composition::_OnDemandLoadingObjectList_class::~_OnDemandLoadingObjectList_class()
{
	std::map< LPVOID, _LPOnDemandLoadingItem>::iterator p=begin();
	for(;p!=end();p++)
	{
		_SafeDel(p->second);
	}
}

void w32::CFile64_Composition::_OnDemandLoadingObjectList_class::AddObject(LPVOID runtime_addr,LPCTSTR fn,LPCTSTR section_full)
{	//ASSERT( QueryObject(runtime_addr) == NULL ); //confirm new key
	std::pair< LPVOID,_LPOnDemandLoadingItem> 
		item = std::make_pair(runtime_addr,new _OnDemandLoadingItem(fn,section_full) );

	//_CheckDump("On-Demand object added: "<<item.second->Filename<<"\\"<<item.second->SectionName<<'\n');
	insert( item );
}

const w32::CFile64_Composition::_OnDemandLoadingItem* 
w32::CFile64_Composition::_OnDemandLoadingObjectList_class::QueryObject(LPVOID runtime_addr)
{	std::map<LPVOID,_LPOnDemandLoadingItem>::iterator p;
	p=find(runtime_addr);
	if(p!=end())
		return p->second;
	else
		return NULL;
}

BOOL w32::CFile64_Composition::_OnDemandLoadingObjectList_class::RemoveObject(LPVOID runtime_addr)
{	std::map<LPVOID,_LPOnDemandLoadingItem>::iterator p;
	p=find(runtime_addr);
	if(p!=end())
	{	//_CheckDump("On-Demand object removed: "<<p->second->Filename<<"\\"<<p->second->SectionName<<'\n');
		_SafeDel(p->second);
		erase(p);
		return TRUE;
	}else{ return FALSE; }
}

BOOL w32::CFile64_Composition::_EnterSection(LPCTSTR section_name)
{
	ASSERT(!m_bInSectionMode);
	stdstring ss;
	ss.assign(section_name);

	m_CurSection = m_SectionTable.find(ss);
	if( m_CurSection==m_SectionTable.end() )
	{ //not find
		if( !m_bReadOnly )
		{// New a section and enter
			__super::SeekToEnd();

			std::pair<ULONGLONG,ULONGLONG> entry;
			entry.first = __super::GetCurrentPosition();
			entry.second = UINT_MAX;

			m_SectionTable.insert(std::make_pair< stdstring, std::pair<ULONGLONG,ULONGLONG> >(ss,entry));
			m_CurSection = m_SectionTable.find(ss);
			ASSERT( m_CurSection!=m_SectionTable.end() );

			m_bInSectionMode = TRUE;
		}
	}
	else
	{
		m_bInSectionMode = TRUE;
		__super::Seek(m_CurSection->second.first);
	}

	return m_bInSectionMode;
}

void w32::CFile64_Composition::_ExitSection()
{	
	ASSERT(m_bInSectionMode);
	m_bInSectionMode = FALSE;
	if( !m_bReadOnly && m_CurSection->second.second == UINT_MAX)
	{	//Update section size
		ULONGLONG len = __super::GetLength() - m_CurSection->second.first;
		if(len)
			m_CurSection->second.second = len;
		else
			m_SectionTable.erase(m_CurSection);
	}
}


BOOL w32::CFile64_Composition::EnterSectionFull(LPCTSTR section_name_full)
{	if( m_bInSectionMode )_ExitSection();
	if(_EnterSection(section_name_full))
	{	m_NestedSectionLevel = 1;
		return TRUE;
	}else{	m_NestedSectionLevel = 0; return FALSE; }
}

BOOL w32::CFile64_Composition::EnterSection(LPCTSTR section_name)
{
	//reserved charactor
	if( _tcschr(section_name,'\\') || 
		_tcschr(section_name,'/') )
	{
		return FALSE;
	}

	if( m_bInSectionMode )
	{
		TCHAR* name = new TCHAR[_tcslen(section_name)+_tcslen(GetCurrentSectionName())+2];
		ASSERT(name);

		_tcscpy(name,GetCurrentSectionName());

		_tcscat(name,_T("\\"));
		_tcscat(name,section_name);

		if( m_CurSection->second.second == UINT_MAX ) //leaving Newly created section
		{
			ASSERT( m_bReadOnly == FALSE );
			if( !GetLength() )
			{
				DWORD padding = 0;
				Write(&padding,4);
			}
		}

		_ExitSection();
		BOOL ret = _EnterSection(name);
        if(ret)
		{
			m_bSectionSwitched = TRUE;
			m_NestedSectionLevel++;
		}
		else
		{
			TCHAR* pt = _tcsrchr(name,_T('\\'));
			ASSERT(pt);
			*pt = _T('\0');
			VERIFY( _EnterSection(name) );
		}

		_SafeDelArray(name);
		return ret;
	}
	else
	{
		BOOL ret = _EnterSection(section_name);
		if(ret)m_NestedSectionLevel++;
		return ret;
	}
}

void w32::CFile64_Composition::ExitSection()
{
	ASSERT(m_bInSectionMode);
	TCHAR* name = new TCHAR[_tcslen(GetCurrentSectionName())+1];
	ASSERT(name);

	_tcscpy(name,GetCurrentSectionName());
	_ExitSection();

    TCHAR* p = _tcsrchr(name,_T('\\'));
	if(p)
	{
		*p=_T('\0');
		VERIFY(_EnterSection(name));
	}
		
	_SafeDelArray(name);
	m_NestedSectionLevel--;
}

LPCTSTR w32::CFile64_Composition::GetCurrentSectionName()
{
	return m_bInSectionMode?m_CurSection->first.c_str():NULL;
}

LPCTSTR w32::CFile64_Composition::GetCurrentSubSectionName()
{
	LPCTSTR n = GetCurrentSectionName();
	if(n)
	{
		LPCTSTR p = _tcsrchr(n,_T('\\'));
		if(p)
			return p+1;
		else
			return n;
	}
	else{ return NULL; }
}

void w32::CFile64_Composition::Close()
{
	ASSERT( IsOpen() );
	if( m_bInSectionMode )ExitSection();
	
	if( !m_bReadOnly )
	{//Finalize section table
		__super::SeekToEnd();
		ULONGLONG SectionTableOffset = __super::GetCurrentPosition();

		int SectionCount = (DWORD)m_SectionTable.size();
		__super::Write("CMPF",4);
		__super::Write(&SectionCount,sizeof(SectionCount));

		std::map< stdstring, std::pair<ULONGLONG,ULONGLONG> >::iterator p = m_SectionTable.begin();
		for(int i=0;i<SectionCount;i++,p++)
		{
			CT2A pStr(p->first.c_str());

			__super::Write(pStr,(UINT)(strlen(pStr)+1)); //string include zero
			__super::Write(&(p->second.first),sizeof(ULONGLONG)); //Section offset
			__super::Write(&(p->second.second),sizeof(ULONGLONG)); //Section length
		}
		__super::Write(&SectionTableOffset,sizeof(ULONGLONG));
	}

	__super::Close();
}

LONGLONG w32::CFile64_Composition::GetLength()
{	
	if( m_bInSectionMode )
	{
		if( m_CurSection->second.second == UINT_MAX )
			return __super::GetLength() - m_CurSection->second.first;
		else
			return m_CurSection->second.second;
	}else{ return __super::GetLength(); }

}

BOOL w32::CFile64_Composition::IsEOF()
{	
	if( m_bInSectionMode )
	{
		if( m_CurSection->second.second == UINT_MAX )
			return FALSE;
		else
			return __super::GetCurrentPosition() >= (LONGLONG)(m_CurSection->second.first + m_CurSection->second.second);
	}else{ return __super::IsEOF(); }
}

BOOL w32::CFile64_Composition::Open(LPCTSTR fn,UINT OpenFlag)
{
	ASSERT(!g_File_OnDemandObject.IsOpen());  //Do not touch g_File_OnDemandObject
	ASSERT( !IsOpen() );
	m_bInSectionMode = FALSE;
	BYTE * pTableOrg = NULL;

	m_NestedSectionLevel = 0;

	m_bReadOnly = !(OpenFlag & Access_Write);

	//No both read & write
	if( __super::Open(fn,OpenFlag) )
	{
		m_SectionTable.clear();

		//load section table
		if(GetLength())
		{
			__super::Seek(-((int)sizeof(ULONGLONG)),Seek_End);

			ULONGLONG TableOffset;
			__super::Read(&TableOffset,sizeof(ULONGLONG));

			LONGLONG table_len = GetLength() - TableOffset;
			if( table_len>0 )
			{
                BYTE * pTable = new BYTE[(size_t)table_len];					
				ASSERT(pTable);

				pTableOrg = pTable;

				__super::Seek(TableOffset);
				__super::Read(pTable,(DWORD)table_len);

				//parse it CMPF
				if( pTable[0]=='C' && pTable[1]=='M' && 
					pTable[2]=='P' && pTable[3]=='F' )  //check header
				{	pTable+=4;
					int entry_co = *((DWORD*)pTable); pTable+=sizeof(DWORD);
					table_len -= (4+sizeof(DWORD));

					for(int i=0;i<entry_co;i++)
					{
						// name length
                        int j=0;
						for(;j<table_len;j++)
							if(pTable[j]){}else{ break; }

						if( j==table_len )goto LoadSectionTableFailed;

						size_t str_len = strlen((LPSTR)pTable);

						stdstring name;
						name.assign(CA2T((LPSTR)pTable));

						pTable+=str_len+1;
						table_len-=str_len+1;

						if( table_len>=sizeof(ULONGLONG)*2 )
						{
							std::pair<ULONGLONG,ULONGLONG> entry;
							entry.first = *((ULONGLONG*)pTable);
							entry.second = *((ULONGLONG*)&pTable[sizeof(ULONGLONG)]);

							pTable+=sizeof(ULONGLONG)*2;
							table_len-=sizeof(ULONGLONG)*2;

							if(entry.first<TableOffset && (entry.first+entry.second)<=TableOffset )
							{
								m_SectionTable.insert(std::make_pair< stdstring, std::pair<ULONGLONG,ULONGLONG> >(name,entry));
							}else goto LoadSectionTableFailed;
						}
						else goto LoadSectionTableFailed;
					}

					//Section table parsed ok and remove it
					if( !m_bReadOnly )
					{	
						LARGE_INTEGER liOff;
						liOff.QuadPart = TableOffset;
						::SetFilePointer(hFile, liOff.LowPart, &liOff.HighPart,(DWORD)Seek_Begin);
						::SetEndOfFile(hFile);
					}

					_SafeDelArray(pTableOrg);
					m_bSectionSwitched = FALSE;
					return TRUE;
				}
			}
		}
		else
		{
			_SafeDelArray(pTableOrg);
			return TRUE; 
		}
	}

LoadSectionTableFailed:
	_SafeDelArray(pTableOrg);
	if(IsOpen())__super::Close();
	return FALSE;
}

UINT w32::CFile64_Composition::Read(LPVOID lpBuf,UINT nCount)
{
	//check section boundary
	if( m_bInSectionMode )
	{
		nCount = (UINT)min(	(ULONGLONG)nCount,
							( m_CurSection->second.first+m_CurSection->second.second )
							- __super::GetCurrentPosition() );
	}

	return __super::Read(lpBuf,nCount);
}

UINT w32::CFile64_Composition::Write(LPCVOID lpBuf,UINT nCount)
{
	//check section boundary
	if( m_bInSectionMode )
	{
		nCount = (UINT)min(	(ULONGLONG)nCount,
								( m_CurSection->second.first+m_CurSection->second.second )
								- __super::GetCurrentPosition() );
	}

	return __super::Write(lpBuf,nCount);
}

BOOL w32::CFile64_Composition::SetLength(ULONGLONG len)
{
	if( m_bInSectionMode )
	{
		ASSERT( m_CurSection->second.second == UINT_MAX );
		return __super::SetLength(len+m_CurSection->second.first);
	}
	else
	{
		return __super::SetLength(len);
	}
}

LONGLONG w32::CFile64_Composition::GetCurrentPosition()
{
	ASSERT(m_bInSectionMode); //available only in Section mode
	ULONGLONG offset = __super::GetCurrentPosition();
	ASSERT(offset>=m_CurSection->second.first);
	offset -= m_CurSection->second.first;
	ASSERT( offset <= m_CurSection->second.second );
	return offset;
}  

LONGLONG w32::CFile64_Composition::Seek(LONGLONG offset,UINT nFrom)
{
	ASSERT(m_bInSectionMode); //available only in Section mode

	ULONGLONG base;
	switch(nFrom)
	{
	case Seek_Begin:
		base = m_CurSection->second.first;
		break;
	case Seek_Current:
		base = __super::GetCurrentPosition();
		break;
	case Seek_End:
		base = m_CurSection->second.first + m_CurSection->second.second-1;
		break;
	default: ASSERT(0);
	}

	base += offset;
	ASSERT( base >= m_CurSection->second.first );
	ASSERT( base < m_CurSection->second.first + m_CurSection->second.second );
	
	return __super::Seek(base) - m_CurSection->second.first; 
}


///////////////////////////////////////////////////////
// _Base_MemberVisitor_SaveLoad
rt::_meta_::_Base_MemberVisitor_SaveLoad::
	_Base_MemberVisitor_SaveLoad(w32::CFile64_Composition& file)
		:m_File(file)
{
}

void rt::_meta_::_Base_MemberVisitor_SaveLoad::PrintIndent(int levelup)
{
	static const LPCSTR space_str= "  ";
	for(int i=-levelup;i<m_File.GetCurrentNestedSectionLevel()*2;i++)
		__PersistDump(space_str);
}

void rt::_meta_::_Base_MemberVisitor_SaveLoad::DumpSize(UINT bytes)
{
	if(bytes>2048)
	{
		if(bytes>2048*1024)
		{	__PersistDump((bytes/(1024*1024))<<"MB");	}
		else
		{	__PersistDump((bytes/1024)<<"KB");	}
	}
	else
		__PersistDump(bytes<<'B');
}

HRESULT rt::_meta_::_Base_MemberVisitor_SaveLoad::Begin(LPCTSTR pClassName)
{	
	int len = (int)_tcslen(pClassName);

	_pMemberName = &_SectionName[len+1];
	_pMemberName[-1] = _T('_');
	memcpy(_SectionName,pClassName,sizeof(TCHAR)*(len));
	
	if(m_File.IsOpen())
	{
		m_File.ClearError();

		PrintIndent(); __PersistDump("class "<<pClassName<<'\n');
		PrintIndent(); 

		if(m_File.GetCurrentSectionName())
		{	__PersistDump("{ [ root:\\"<<m_File.GetCurrentSectionName()<<" ]\n");	}
		else
		{	__PersistDump("{ [ root:\\ ]\n"); }
	}
	//else force load all On-demand members

	return S_OK;
}

LPCTSTR rt::_meta_::_Base_MemberVisitor_SaveLoad::ComposeSectionName(LPCTSTR pMemberName)
{
	ASSERT(pMemberName);

	if(pMemberName[0] != _T('['))
	{
		int len = (int)_tcslen(pMemberName);
		_pMemberName[0] = _T('(');
		memcpy(&_pMemberName[1],pMemberName,sizeof(TCHAR)*len);
		_pMemberName[len+1] = _T(')');
		_pMemberName[len+2] = _T('\0');
	}
	else
	{
		_tcscpy(_pMemberName,pMemberName);
	}

	PrintIndent(1); __PersistDump(_SectionName);
	return _SectionName;
}

HRESULT rt::_meta_::_Base_MemberVisitor_SaveLoad::End(LPCTSTR pClassName)
{
	ASSERT(*_pMemberName);
	if(m_File.IsOpen())
	{
		PrintIndent(); __PersistDump("}\n");

		if(m_File.ErrorOccured())
		{	return w32::HresultFromLastError();	}
		else
		{	return S_OK; }
	}
	//else force load all On-demand members

	*_pMemberName=0;
	return S_OK;
}

///////////////////////////////////////
// _MemberVisitor::_Save/_Load
rt::_MemberVisitor::_Save::_Save(w32::CFile64_Composition& file)
	:_Base_MemberVisitor_SaveLoad(file)
{
}

rt::_MemberVisitor::_Load::_Load(w32::CFile64_Composition& file)
	:_Base_MemberVisitor_SaveLoad(file)
{
}

#undef __PersistDump
