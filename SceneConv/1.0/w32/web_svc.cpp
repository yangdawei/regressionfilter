#include "stdafx.h"
#include "..\rt\string_type.h"
#include <mmsystem.h>
#include <atlconv.h>
#include <Psapi.h>
#include "w32_misc.h"
#include "debug_log.h"
#include "web_svc.h"
#include "file_64.h"
#include <google/dense_hash_map>
#include "file_zip.h"
#include "xml_soap.h"

#pragma comment(lib, "Psapi.lib")

#define WSC_LOG(x)	{ if(get_httpd()->DebugLogEnabled){ w32::inet_mod::DebugLog(get_httpd(), rt::StringA_Ref() + x); } }

w32::inet::WebServCore::WebServCore()
{
	_httpd_ext.pThis = this;
	_szLogTime_Backup = NULL;
	m_hUpdateWebLogTimeThread = NULL;

	m_szLogTime = _LogTimePool;
	_szLogTime_Backup = &_LogTimePool[28];

	TIME_ZONE_INFORMATION tz;
	::GetTimeZoneInformation(&tz);
	sprintf(&m_szLogTime[22],"%+03d00]",tz.Bias/-60);
	memcpy(&_szLogTime_Backup[22],&m_szLogTime[22],6);

	_szLogTime_Backup[0] = '[';
	m_szLogTime[0] = '[';

	_pNewlyWebSites = NULL;
	m_pWebSites = new WebSites;
	m_pWebSites->m_DomainMap.Build(-1);
	m_pWebSites->m_NameMap.Build(-1);

	_DeletionLatency = 30*1000;

	SetDebugLogWriter(NULL);

	ResetAllDefaultRequestHandler();
}

w32::inet::WebServCore::~WebServCore()
{
	UnloadWebSites();
}


void w32::inet::WebServCore::SetDebugLogWriter(w32::CParallelFileWriter* p)
{
	m_pDebugLogWriter = p;
	_httpd_ext.DebugLogEnabled = TRUE;
}

LPSTR w32::inet::WebServCore::httpd_ext::DebugLogClaimWriting(UINT size)
{
	ASSERT(pThis->m_pDebugLogWriter);
	LPBYTE p = pThis->m_pDebugLogWriter->ClaimWriting(size + 23);
	if(p)return (LPSTR)p+23;
	else return NULL;
}

void w32::inet::WebServCore::httpd_ext::DebugLogFinalizeWritten(LPSTR p, UINT len)
{
	p -= 23;
	ASSERT(pThis->m_pDebugLogWriter);
	memcpy(p, pThis->m_szLogTime, 21);
	*((WORD*)&p[21]) = 0x205d;	// "] "
	pThis->m_pDebugLogWriter->FinalizeWritten((LPBYTE)p,len + 23);
}

UINT w32::inet::WebServCore::httpd_ext::GetDeletionLatency()
{
	return pThis->_DeletionLatency;
}

UINT w32::inet::WebServCore::httpd_ext::GetWebsiteCount()
{
	return pThis->_getWebSites()->m_WebSites.GetSize();
}

LPCSTR w32::inet::WebServCore::httpd_ext::GetWebsiteName(UINT i)
{
	w32::inet::WebSites& web = *(pThis->_getWebSites());
	return web.GetWebSiteCount() > i?(LPCSTR)web.m_WebSites[i]->Name:NULL;
}

LPCTSTR	w32::inet::WebServCore::httpd_ext::GetExtensionWorkingDirectory(UINT website_idx)
{
	w32::inet::WebSites& web = *(pThis->_getWebSites());
	if(website_idx < web.m_WebSites.GetSize() && !web.m_WebSites[website_idx]->m_ExtensionWorkingSpace.IsEmpty())
		return web.m_WebSites[website_idx]->m_ExtensionWorkingSpace;
	return NULL;
}

LPCSTR	w32::inet::WebServCore::httpd_ext::GetExtensionConfig(UINT website_idx)
{
	w32::inet::WebSites& web = *(pThis->_getWebSites());
	if(website_idx < web.m_WebSites.GetSize())
		return web.m_WebSites[website_idx]->m_ExtensionConfig;
	return NULL;
}

LPCTSTR	w32::inet::WebServCore::httpd_ext::GetServConfigFilename()
{
	return pThis->m_WebsiteConfigFilename;
}

const w32::inet::WebServCore::_WebServExt* w32::inet::WebServCore::GetExtension(LPCSTR name) const
{
	for(UINT i=0;i<_Extensions.GetSize();i++)
		if(_Extensions[i].name == name)
			return &_Extensions[i];
	return NULL;
}

BOOL w32::inet::WebServCore::RegisterExtension(LPCSTR ext_name, w32::inet_mod::WebServExtension* pMod)
{
	if(GetExtension(ext_name))return FALSE;

	_WebServExt& n = _Extensions.push_back();
	n.name = ext_name;
	n.mod = pMod;

	if(pMod->OnRegistered(get_httpd(), n.name, _Extensions.GetSize()-1))
	{
		WSC_LOG('[' + ext_name + "] registered.");
		return TRUE;
	}
	else
	{	_Extensions.pop_back();
	}

	WSC_LOG('[' + ext_name + "] failed to register.");
	return FALSE;
}


void w32::inet::WebServCore::DefaultRequest_DumpWebCore(inet::WebThreadLocalBlock* pTLB)
{
	w32::inet::HttpServCore::DefaultRequest_SectionBegin(pTLB,"Web Sites Information");
	char peerinfo[1024];	int len = 0;

	{	// Basic info
		len = sprintf_s(peerinfo,"Website Config File:   %s\r\n\r\n",
								 (LPCSTR)ATL::CT2A(m_WebsiteConfigFilename));
		pTLB->SendChunkResponse(peerinfo,len);
	}

	if(m_pWebSites)
	{
		WebSites& web = *m_pWebSites;
		for(UINT i=0;i<web.m_WebSites.GetSize();i++)
		{
			WebSite& s = *(web.m_WebSites[i]);

			if(i)
			{	pTLB->SendChunkResponse('\r');
				pTLB->SendChunkResponse('\n');
			}

			len = sprintf(	peerinfo,	"[ %s ]%c   ",
							s.Name.Begin(),
							(i == pTLB->WebsiteIndex)?'*':' '
						 );
			pTLB->SendChunkResponse(peerinfo,len);

			for(UINT d=0;d<s.Domains.GetSize();d++)
			{
				if(d)
				{	pTLB->SendChunkResponse(',');
					pTLB->SendChunkResponse(' ');
				}
				pTLB->SendChunkResponse(s.Domains[d].Begin(),s.Domains[d].GetLength());
			}

			pTLB->SendChunkResponse('\r');
			pTLB->SendChunkResponse('\n');

			if(s.IsWebLogRunning())
			{
				LONG cf = s.m_LogFile.stat_ClaimFailure;
				LONG ff = s.m_LogFile.stat_UnfinalizedChunk;
				len = sprintf(	peerinfo,	"   Web Logging Path:   %s\r\n"
											"   Web Logging File:   %s (%d/%d/%d)\r\n"
											"       Save Interval:  %d msec\r\n"
											"       Throughput:     %s/s (Max. %s/s)\r\n"
											"       File I/O Load:  %2d%% / %2d%% (Avg/Peek) of %dms\r\n"
											"       Buffer Load:    %2d%% / %2d%% (Avg/Peek) of %s\r\n"
											"       Total Logs:     %d\r\n"
											"       Exceptions:     %d Dropped Logs ( %d overrun, %d long-latency ) \r\n"
											"                       %d Finalization Failures\r\n"
											"                       %d File Writing Failures\r\n",
								(LPCSTR)ATL::CT2A(s.m_WebLogPath),
								(LPCSTR)s.m_LogFileName,
								s.m_LogFileStart.GetMonth(),
								s.m_LogFileStart.GetDay(),
								s.m_LogFileStart.GetYear(),
								s.m_LogFile.GetWriteDownInterval(),

								(LPCSTR)rt::tos::FileSize<char,true,true>((ULONGLONG)(s.m_LogFile.GetBufferSize()*s.m_LogFile.stat_BufferUsageAvg*10/((double)(s.m_LogFile.GetWriteDownInterval())))),
								(LPCSTR)rt::tos::FileSize<char,true,true>((ULONGLONG)(s.m_LogFile.GetBufferSize()*1000/((double)(s.m_LogFile.GetWriteDownInterval())))),

								s.m_LogFile.stat_FileIOUsageAvg,s.m_LogFile.stat_FileIOUsagePeek,
								s.m_LogFile.GetWriteDownInterval(),

								s.m_LogFile.stat_BufferUsageAvg,s.m_LogFile.stat_BufferUsagePeek,
								(LPCSTR)rt::tos::FileSize<char,true,true>((ULONGLONG)s.m_LogFile.GetBufferSize()),

								s.m_LogFile.stat_TotalClaimed,

								cf + ff,cf,ff,
								s.m_LogFile.stat_FinalizeFailure,
								s.m_LogFile.stat_FileError
							);
				pTLB->SendChunkResponse(peerinfo,len);
			}
			else
			{	static const char sp[] = "   Web Logging:        [stopped]\r\n";
				pTLB->SendChunkResponse(sp,sizeof(sp)-1);
			}
		}
	}
	else
	{	static const char sp[] = "No website loaded.\r\n";
		pTLB->SendChunkResponse(sp,sizeof(sp)-1);
	}

	w32::inet::HttpServCore::DefaultRequest_SectionEnd(pTLB);
}

void w32::inet::WebServCore::DefaultRequest_HostNotConfigured(inet::WebThreadLocalBlock* pTLB, LPCSTR host, int host_len)
{
	static const char msg[] = "Requested domain is not configured !!\r\n<br />Host: ";

	pTLB->SendChunkResponse_Begin();
		pTLB->SendChunkResponse(msg,sizeof(msg)-1);
		pTLB->SendChunkResponse(host, host_len);
	pTLB->SendChunkResponse_End();
}

HRESULT	w32::inet::WebServCore::DefaultRequest_DocumentNotFound(w32::inet_mod::_httpd_tlb* pTLB, LPVOID cookie)
{
	return pTLB->SendResponseHeadOnly()?S_OK:E_FAIL;
}

void w32::inet::WebServCore::SetDefaultRequestHandler(DefaultRequestHandlerType type, const RequestHandler& hd)
{
	ASSERT(type < DRHT_MAX);
	m_pDefaultRequestHandler[type] = hd;
}

void w32::inet::WebServCore::ResetAllDefaultRequestHandler()
{
	SetDefaultRequestHandler(DRHT_DOCUMENT_NOT_FOUND, RequestHandler(DefaultRequest_DocumentNotFound,NULL));
}

void w32::inet::WebServCore::UpdateWebLogTime()
{
	w32::CTime64<true> tt;
	tt.LoadCurrentTime();

	sprintf(&_szLogTime_Backup[1],"%02d/%s/%04d:%02d:%02d:%02d",
								  tt.GetDayOfMonth(),
								  tt.GetMonthName(),
								  tt.GetYear(),
								  tt.GetHour(),
								  tt.GetMinute(),
								  tt.GetSecond());
	_szLogTime_Backup[21] = ' ';
	LPSTR tmp = m_szLogTime;
	m_szLogTime = _szLogTime_Backup;
	_szLogTime_Backup = tmp;

	_Maintenance_WebLog();
}

void w32::inet::WebServCore::UnloadWebSites()
{
	if(m_hUpdateWebLogTimeThread)
	{	HANDLE h = m_hUpdateWebLogTimeThread;
		m_hUpdateWebLogTimeThread = NULL;
		w32::WaitForThreadEnding(h);
	}
}

void w32::inet::WebServCore::StopAllWebLog()
{
	if(m_pWebSites)
	{
		WebSites& web = *m_pWebSites;
		for(UINT i=0;i<web.m_WebSites.GetSize();i++)
		{
			web.m_WebSites[i]->_StopWebLog();
		}
	}	
}

UINT w32::inet::WebServCore::GetWebsiteCount() const
{
	const WebSites* p = m_pWebSites;
	if(p)return p->m_WebSites.GetSize();

	return 0;
}

 w32::inet::WebSite* w32::inet::WebServCore::GetWebsite(UINT i)
{
	WebSites* p = m_pWebSites;
	if(p && i<p->m_WebSites.GetSize())
		return p->m_WebSites[i];

	return NULL;
}

UINT w32::inet::WebServCore::GetWebsiteIndex_ByName(const rt::StringA_Ref& name) const
{
	WebSites* p = m_pWebSites;
	return p->LookupWebSite(rt::StringA_Ref(name));
}

HRESULT	w32::inet::WebServCore::_Maintenance_WebLog()
{
	if(m_pWebSites)
	{
		w32::CTime64<> date;
		date.LoadCurrentTime();

		if(_LastWeblogProbed.GetDayOfMonth() != date.GetDayOfMonth())
		{
			_LastWeblogProbed = date;
			w32::CDate32 dd(date);

			WebSites& web = *(m_pWebSites);
			for(UINT i=0;i<web.m_WebSites.GetSize();i++)
			{
				if(	web.m_WebSites[i]->m_LogFileStart.GetMonth() != date.GetMonth() &&
					!web.m_WebSites[i]->m_WebLogPath.IsEmpty()
				)	// weblog per month
				{	
					if(web.m_WebSites[i]->DebugLogEnabled)
						w32::inet_mod::DebugLog(web.m_WebSites[i],
												rt::StringA_Ref("Switch weblog monthly (") + w32::CTime64<>::GetMonthName(dd.GetMonth()) + ' ' + dd.GetYear() + ')'
											   );
					web.m_WebSites[i]->StartWebLog(web.m_WebSites[i]->m_WebLogPath, &dd);
				}
			}
		}
	}

	return S_OK;
}

void w32::inet::WebServCore::_LogSocketServCoreState(w32::inet::SocketServCore* pCore)
{
	if(pCore->GetListenerCount())
	{
		WSC_LOG("Listening address(es):");
		for(UINT i=0;i<pCore->GetListenerCount();i++)
		{
			const CInetAddr& addr = pCore->GetListenerAddress(i);
			if(pCore->IsListenerWorking(i))
			{	WSC_LOG("    " + rt::StringA(addr.GetDottedDecimalAddress()) + ':' + addr.GetPort());	}
			else
			{	WSC_LOG("    " + rt::StringA(addr.GetDottedDecimalAddress()) + ':' + addr.GetPort() + " [Error]");	}
		}
	}
	else
	{	WSC_LOG("No Listening address.");
	}
}

BOOL w32::inet::WebServCore::LoadWebSites(LPCTSTR fn_xml)
{
	m_WebsiteConfigFilename = fn_xml;
	w32::CFileBuffer<char>	file;
	w32::CXMLParser	xml;

	ASSERT(_pNewlyWebSites == NULL);
	WSC_LOG("Reload website config: " + ((LPCSTR)ATL::CT2A(fn_xml)));

	int host_count = 0;

	if(	file.SetSizeAs(fn_xml) &&
		xml.Load(file,TRUE,file.GetSize()) &&
		xml.EnterXPath("/config/websites/website[@name]")
	)
	{
		WebSites&  web = *(new WebSites);

		rt::BufferEx<rt::StringA_Ref>	domains_ref;
		rt::BufferEx<int>				domains_index;

		rt::BufferEx<rt::StringA_Ref>	name_ref;
		rt::BufferEx<int>				name_index;

		WebSites* pOldWebSites = m_pWebSites;
		rt::Buffer<BOOL> old_website_reused;
		rt::BufferEx<BOOL> new_website_created;

		if(pOldWebSites)
		{	old_website_reused.SetSize(pOldWebSites->m_WebSites.GetSize());
			old_website_reused.Set(FALSE);
		}

		_LastWeblogProbed.LoadCurrentTime();

		do
		{
			w32::CXMLParser node = xml.GetNodeDocument();
			if(node.EnterXPath("/website/domain"))
			{
				rt::StringA text;
				VERIFY(xml.GetAttribute("name",text));

				int idx;
				WebSite* p;
				BOOL created;
				if(pOldWebSites && (idx = pOldWebSites->LookupWebSite(text)) >=0)
				{
					p = m_pWebSites->m_WebSites[idx];
					old_website_reused[idx] = true;
					created = FALSE;
				}
				else
				{	
					p = new WebSite(get_httpd());
					p->SetDeletionLatency(_DeletionLatency);
					ASSERT(p);
					p->Name = text;
					created = TRUE;
				}

				web.m_WebSites.push_back(p);
				p->Index = web.m_WebSites.GetSize()-1;

				name_ref.push_back() = p->Name.GetRef();
				name_index.push_back() = web.m_WebSites.GetSize() - 1;
				new_website_created.push_back(created);

				if(xml.GetAttribute_Path("weblog",text))
				{
					rt::String path;
					path.ConvertCodePage(text,CP_UTF8);
					text.Replace('/','\\');
					text.MakeLower();

					int si;
					if(si = xml.GetAttribute_Int("weblog_save_interval",30000))
					{
						p->m_WebLogSaveInterval = si;
					}

					if(si = (int)xml.GetAttribute_FileSize("weblog_buffer_size",1024*1024))
					{
						p->m_WebLogBufferSize = si;
					}
					
					p->StartWebLog(path);
				}

				if(xml.GetAttribute_Path("workspace",text))
					p->m_ExtensionWorkingSpace.ConvertCodePage(text,CP_UTF8);

				p->Domains.ChangeSize(0);
				do
				{
					node.GetInnerText(p->Domains.push_back());
					domains_ref.push_back(p->Domains[p->Domains.GetSize()-1]);
					domains_index.push_back(web.m_WebSites.GetSize()-1);
					host_count++;
				}while(node.EnterSucceedNode());

				p->Endpoints.ChangeSize(0);
				rt::StringA name;
				rt::StringA uri;
				DWORD type;
				UINT default_maxage;
				CHAR hidden = 0;

				if(node.EnterXPath("/website/endponts"))
				{
					static const LPCSTR MAXAGE_TEXT = "maxage";
					default_maxage = node.GetAttribute_Int(MAXAGE_TEXT,3600);
					if(node.EnterXPath("/website/endponts/*[@uri]"))
					{
						do
						{	
							if(node.GetInnerText(name) && node.GetAttribute("uri",uri))
							{
								if	(node.GetNodeName() == "directory" || node.GetNodeName() == "folder")
								{
									type = inet::WebSite::EPT_DIRECTORY;
								}
								else 
								if	(node.GetNodeName() == "file")
									type = inet::WebSite::EPT_SINGLEFILE;
								else 
								if	(node.GetNodeName() == "zip")
									type = inet::WebSite::EPT_ZIPFILE;
								else 
								if	(node.GetNodeName() == "alias")
									type = inet::WebSite::EPT_ALIAS;
								else 
								if	(node.GetNodeName() == "prefix")
									type = inet::WebSite::EPT_PREFIX;
								else 
								if	(node.GetNodeName() == "handler")
									type = inet::WebSite::EPT_HANDLER;
								else
								{	continue;
								}

								if(node.GetAttribute("hidden",text) && text.GetLength())
									hidden = text[0];

								inet::WebSite::_Endpoint& f = p->Endpoints.push_back();
								f.Name = name;
								f.uri = uri;
								f.Type = type;
								f.HasError = TRUE;
								f.HiddenChar = hidden;
								if((type & inet::WebSite::EPT_FILETYPE_MASK) == type)
									f.MaxAge = node.GetAttribute_Int(MAXAGE_TEXT, default_maxage);
								else
									f.MaxAge = 0;
							}

						}while(node.EnterSucceedNode());
					}
					// p->RefreshEndpoints(*this);   do this after _Extensions[i].mod->OnWebsitesReloaded();
				}

				if(node.EnterXPath("/website/extensions"))
				{
					node.GetNodeDocument(p->m_ExtensionConfig);
				}
				
				WSC_LOG("Website #" + p->Index + ": " + p->Name + ' ' + 
						p->Domains.GetSize() + " domain(s) " +
						p->Endpoints.GetSize() + " Endponit(s)");

				if(p->m_WebLogPath.IsEmpty())
					WSC_LOG("         No WebLog")
				else
					WSC_LOG("         WebLog Path: " + (LPCSTR)ATL::CT2A(p->m_WebLogPath))

				WSC_LOG("         Extension Workspace: " + (LPCSTR)ATL::CT2A(p->m_ExtensionWorkingSpace));
			}
		}while(xml.EnterSucceedNode());

		WSC_LOG(web.GetWebSiteCount() + " Website(s) loaded.");

		// build map
		if(	web.m_DomainMap.Build(-1,domains_ref.GetSize(),domains_ref,domains_index) &&
			web.m_NameMap.Build(-1,name_ref.GetSize(),name_ref,name_index)
		)
		{	_pNewlyWebSites = &web;

			{
				for(UINT i=0;i<_Extensions.GetSize();i++)
					_Extensions[i].mod->OnWebsitesReloaded();

				for(UINT i=0;i<web.m_WebSites.GetSize();i++)
					web.m_WebSites[i]->RefreshEndpoints(*this);
			}
			m_pWebSites = &web;
			_pNewlyWebSites = NULL;

			if(pOldWebSites)
			{
				for(UINT i=0;i<old_website_reused.GetSize();i++)
					if(!old_website_reused[i])
						_SafeDel_Delayed(pOldWebSites->m_WebSites[i], _DeletionLatency + 1000);
				w32::CGarbageCollection::DeleteObj(pOldWebSites, _DeletionLatency);
			}

			if(!m_hUpdateWebLogTimeThread)
			{
				struct _cb
				{	static DWORD WINAPI _func(LPVOID p)
					{	w32::inet::WebServCore* pThis = (w32::inet::WebServCore*)p;
						while(pThis->m_hUpdateWebLogTimeThread)
						{	::Sleep(994);
							pThis->UpdateWebLogTime();
						}
						return 0;
					}
				};

				UpdateWebLogTime();
				m_hUpdateWebLogTimeThread = (HANDLE)1;
				m_hUpdateWebLogTimeThread = ::CreateThread(NULL,0,_cb::_func,this,0,&m_hUpdateWebLogTimeThreadId);
			}

			return TRUE;
		}

		ASSERT(0);
	}

	return FALSE;
}

#undef WSC_LOG

w32::inet::WebSite::WebSite(w32::inet_mod::_httpd* serv_core)
{
	m_WebLogFieldsOption = 0xffffffff;

	_phttpd = serv_core;
	DebugLogEnabled = _phttpd->DebugLogEnabled;
	m_WebLogBufferSize = 1034*1024;
	m_WebLogSaveInterval = 30000;
}

w32::inet::WebSite::~WebSite()
{
	_StopWebLog();
}

int	w32::inet::WebSites::LookupWebSite(const rt::StringA_Ref& name) const
{
	return m_NameMap[name];
}

int	w32::inet::WebSites::LookupDomain(const rt::StringA_Ref& domain) const
{
	return m_DomainMap[domain];
}

w32::inet::WebThreadLocalBlock::WebThreadLocalBlock(w32::inet::SocketServCore* pServCore)
	:HttpThreadLocalBlock(pServCore)
{
	TLB_http_mod.pTLB = this;
	TLB_http_mod.webhost = NULL;
	WebsiteIndex = -1;
}

const w32::inet::HttpRequest* w32::inet::WebThreadLocalBlock::_TLB_Mod::GetParsedRequest()
	{ return &pTLB->Request; }
BOOL w32::inet::WebThreadLocalBlock::_TLB_Mod::SendResponseHeadOnly(UINT code, BOOL close_session)
	{ return pTLB->SendResponseHeadOnly(code,close_session); }
BOOL w32::inet::WebThreadLocalBlock::_TLB_Mod::SendRedirection(UINT code, LPCSTR redirected_url, int url_len)
	{ return pTLB->SendRedirection(code,redirected_url,url_len); }
BOOL w32::inet::WebThreadLocalBlock::_TLB_Mod::SendResponse(LPCBYTE pData, SIZE_T len, UINT code, BOOL close_session, LPCSTR content_type)
	{ return pTLB->SendResponse(pData,len,code,close_session,content_type); }
BOOL w32::inet::WebThreadLocalBlock::_TLB_Mod::SendResponse_NoCopy(LPCBYTE pData, SIZE_T len, UINT code, BOOL close_session, LPCSTR content_type)
	{ return pTLB->SendResponse_NoCopy(pData, len, code, close_session, content_type); }
void w32::inet::WebThreadLocalBlock::_TLB_Mod::SendChunkResponse_Begin()
	{ pTLB->SendChunkResponse_Begin(); }
BOOL w32::inet::WebThreadLocalBlock::_TLB_Mod::SendChunkResponse(LPCVOID pData, UINT len)
	{ return pTLB->SendChunkResponse(pData, len); }
BOOL w32::inet::WebThreadLocalBlock::_TLB_Mod::SendChunkResponse_EncodeHTML(LPCSTR PlainText, UINT len)
	{ return pTLB->SendChunkResponse_EncodeHTML(PlainText, len); }
BOOL w32::inet::WebThreadLocalBlock::_TLB_Mod::SendChunkResponse_End(UINT code, BOOL close_session, LPCSTR content_type)
	{ return pTLB->SendChunkResponse_End(code, close_session, content_type); }
void w32::inet::WebThreadLocalBlock::_TLB_Mod::AppendResponseHeader_ContentRange(const inet_mod::_ContentRange& r)
	{ pTLB->AppendResponseHeader_ContentRange((const WebThreadLocalBlock::ContentRange&)r); }
void w32::inet::WebThreadLocalBlock::_TLB_Mod::AppendResponseHeader_ETag(LPCSTR tag, int maxage_sec)
	{ pTLB->AppendResponseHeader_ETag(tag,maxage_sec); }
void w32::inet::WebThreadLocalBlock::_TLB_Mod::AppendResponseHeader_Maxage(int maxage_sec)
	{ pTLB->AppendResponseHeader_Maxage(maxage_sec); }
void w32::inet::WebThreadLocalBlock::_TLB_Mod::AppendResponseHeader_NoCache()
	{ pTLB->AppendResponseHeader_NoCache(); }
void w32::inet::WebThreadLocalBlock::_TLB_Mod::AppendResponseHeader_ContentDisposition(LPCSTR filename)
	{ pTLB->AppendResponseHeader_ContentDisposition(filename); }
BOOL w32::inet::WebThreadLocalBlock::_TLB_Mod::AppendResponseHeader(LPCSTR head, UINT len)
	{ return pTLB->AppendResponseHeader(head,len); }
DWORD w32::inet::WebThreadLocalBlock::_TLB_Mod::GetWebSiteID()
	{ return pTLB->WebsiteIndex; }
LPCSTR  w32::inet::WebThreadLocalBlock::_TLB_Mod::GetWebSiteName()
	{ return pTLB->pWebsite->Name; }
const sockaddr_in& w32::inet::WebThreadLocalBlock::_TLB_Mod::GetRemoteAddress()
	{ return pTLB->pConn->m_RemoteAddress; }
const sockaddr_in& w32::inet::WebThreadLocalBlock::_TLB_Mod::GetLocalAddress()
	{ return pTLB->pConn->m_LocalAddress; }
LPVOID w32::inet::WebThreadLocalBlock::_TLB_Mod::GetThreadLocalObject(int extension_index)
	{	if(extension_index>=0 && extension_index<(int)pTLB->ExtensionCookie.GetSize())
		{	return pTLB->ExtensionCookie[extension_index];	}
		else
		{	return NULL; }
	}


BOOL w32::inet::WebSite::_StartWebLog(LPCTSTR filename, UINT buffer_size, UINT sync_interval, DWORD flag)
{
	_UpdateWebLogInfo(filename);

	ASSERT(!m_LogFile.IsOpen());
	return m_LogFile.Open(filename,buffer_size,sync_interval,0,flag);
}
	
void w32::inet::WebSite::_StopWebLog()
{
	m_LogFile.Close();
}

void w32::inet::WebSite::_SwitchWebLogTo(LPCTSTR filename, BOOL no_wait)
{
	ASSERT(m_LogFile.IsOpen());
	m_LogFile.SwitchTo(filename, no_wait);
	_UpdateWebLogInfo(filename);
}

void w32::inet::WebSite::_UpdateWebLogInfo(LPCTSTR fn)
{
	LPCTSTR name = _tcsrchr(fn,'\\');
	if(name){ name++; }
	else name = fn;

	m_LogFileName.ConvertCodePage(rt::String_Ref(name));
	m_LogFileStart.LoadCurrentDate();
}

LPSTR w32::inet::WebSite::DebugLogClaimWriting(UINT size)
{	
	LPSTR ret;
	if(	_phttpd->DebugLogEnabled &&
		(ret = _phttpd->DebugLogClaimWriting(size + Name.GetLength() + 4))
	)
	{	return ret += Name.GetLength() + 4;
	}
	else
		return NULL;
}

void w32::inet::WebSite::DebugLogFinalizeWritten(LPSTR p, UINT len)
{
	ASSERT(_phttpd->DebugLogEnabled);
	ASSERT(p);
	p -= Name.GetLength() + 4;
	p[0] = '[';
	memcpy(p+1,Name,Name.GetLength());
	p[Name.GetLength()+1] = ']';
	*((WORD*)&p[Name.GetLength()+2]) = 0x203a;	// ": "
	_phttpd->DebugLogFinalizeWritten(p, len + Name.GetLength() + 4);
}

UINT w32::inet::WebSite::GetDeletionLatency()
{
	return GetWebServCore()->_DeletionLatency;
}

//void w32::inet::WebSite::Endpoint_FinalUpdateIndex(){ m_Documents.FinalUpdateIndex(); }
//void w32::inet::WebSite::Endpoint_AddAlias(LPCSTR uri, LPCSTR uri_mapped_to, BOOL is_prefix){ m_Documents.AddAlias(uri_mapped_to,uri,is_prefix); }
//BOOL w32::inet::WebSite::Endpoint_AddSingleFile(LPCSTR uri, LPCTSTR fn, LPCSTR MIME){ return SUCCEEDED(m_Documents.LoadSingleFile(fn,uri,MIME)); }
//BOOL w32::inet::WebSite::Endpoint_AddZipFile(LPCSTR uri_suffix, LPCTSTR zip_fn, BOOL merge){ return SUCCEEDED(m_Documents.LoadZipFile(zip_fn,uri_suffix,merge)); }
//BOOL w32::inet::WebSite::Endpoint_AddDirectory(LPCSTR uri_suffix, LPCTSTR directory, BOOL merge){ return SUCCEEDED(m_Documents.LoadDirectory(directory,uri_suffix,merge)); }
//BOOL w32::inet::WebSite::Endpoint_Remove(LPCSTR uri){ return m_Documents.Remove(uri); }
BOOL w32::inet::WebSite::Endpoint_AddHandler(LPCSTR uri, const w32::inet_mod::_httpd_request_handler& hd){ return SUCCEEDED(m_Documents.LoadHandler(hd,uri)); }
BOOL w32::inet::WebSite::Endpoint_IsExist(LPCSTR uri){ return m_Documents.IsExist(rt::StringA_Ref(uri)); }

LPCSTR w32::inet::WebSite::Website_GetName(){ return Name; }

w32::inet::WebServCore* w32::inet::WebSite::GetWebServCore()
{
	return ((w32::inet::WebServCore::httpd_ext*)_phttpd)->pThis;
}

void w32::inet::WebSite::StartWebLog(const rt::String_Ref& path, w32::CDate32* pForceNewDate)
{
	if(m_WebLogPath != path || pForceNewDate)
	{
		m_WebLogPath = path;

		if(!pForceNewDate)
		{	pForceNewDate = &m_LogFileStart;
			m_LogFileStart.LoadCurrentDate();
		}
		else
		{	m_LogFileStart = *pForceNewDate;
		}

		rt::String fn;
		{
			rt::_meta_::toS<TCHAR> mon(m_LogFileStart.GetMonth());
			mon.RightAlign(2,_T('0'));
			fn = path + _T('\\') + ((LPCTSTR)ATL::CA2T(Name.Begin())) + 
				 _T('_') + m_LogFileStart.GetYear() + _T('_') + mon + _T(".log");
		}

		if(IsWebLogRunning())
		{
			_SwitchWebLogTo(fn);
			m_LogFile.SetWriteDownInterval(m_WebLogSaveInterval);
		}
		else
			_StartWebLog(fn,m_WebLogBufferSize,m_WebLogSaveInterval);
	}
}

HRESULT	w32::inet::WebSite::RefreshEndpoints(WebServCore& core)
{
	HRESULT ret = S_OK;
	m_Documents.ResetStatCounters();

	m_Documents.ClearAliases();
	m_Documents.RemoveAll();

	for(UINT i=0;i<Endpoints.GetSize();i++)
	{
		_Endpoint& ep = Endpoints[i];
		switch(ep.Type)
		{
		case EPT_DIRECTORY:
			m_Documents.SetDefaultMaxAge(ep.MaxAge);
			m_Documents.SetHiddenFileInitial(ep.HiddenChar);
			if(ep.HasError = (S_OK != m_Documents.LoadDirectory(ATL::CA2T(ep.Name.Begin()), ep.uri,TRUE, &ep.TotalDataSize)))
				ret = S_FALSE;
			m_Documents.SetHiddenFileInitial(0);
			break;
		case EPT_ZIPFILE:
			m_Documents.SetDefaultMaxAge(ep.MaxAge);
			m_Documents.SetHiddenFileInitial(ep.HiddenChar);
			ep.HasError = (S_OK != m_Documents.LoadZipFile(ATL::CA2T(ep.Name.Begin()), ep.uri, TRUE, &ep.TotalDataSize));
			m_Documents.SetHiddenFileInitial(0);
			break;
		}
	}

	for(UINT i=0;i<Endpoints.GetSize();i++)
	{
		_Endpoint& ep = Endpoints[i];
		if(ep.Type == EPT_SINGLEFILE)
		{
			m_Documents.SetDefaultMaxAge(ep.MaxAge);
			m_Documents.SetHiddenFileInitial(ep.HiddenChar);
			ep.HasError = (S_OK != m_Documents.LoadSingleFile(ATL::CA2T(ep.Name.Begin()), ep.uri, NULL, &ep.TotalDataSize));
			m_Documents.SetHiddenFileInitial(0);
		}
	}

	for(UINT i=0;i<Endpoints.GetSize();i++)
	{
		_Endpoint& ep = Endpoints[i];
		if(ep.Type == EPT_HANDLER)
		{
			rt::StringA nn(ep.Name);
			int off = nn.FindCharactor('.');
			LPCSTR routename = NULL;
			if(off>=0)
			{	nn.SetLength(off);
				routename = &ep.Name[off+1];
			}
			const WebServCore::_WebServExt* p = core.GetExtension(nn);
			ep.HasError = !(p && p->mod->BindRequestHandler(this,ep.uri,routename));
		}
	}

	for(UINT i=0;i<Endpoints.GetSize();i++)
	{
		_Endpoint& ep = Endpoints[i];
		if(ep.Type == EPT_ALIAS || ep.Type == EPT_PREFIX)
		{
			m_Documents.AddAlias(ep.Name, ep.uri, ep.Type == EPT_PREFIX);
			ep.HasError = FALSE;
		}
	}

	m_Documents.FinalUpdateIndex();
	inet_mod::DebugLog(this,rt::StringA_Ref("Reload Namespace: ") + 
							m_Documents.stat_DocumentUpdated + " Updated, " + 
							m_Documents.stat_DocumentAdded + " Added, " +
							m_Documents.stat_DocumentRemoved + " Removed.");
	return S_OK;
}


namespace w32
{
namespace inet
{
namespace _meta_
{

typedef ::google::dense_hash_map<rt::StringA_Ref,IndexedEntry_Base::__Entry*,rt::StringA_Ref::hash_compare>	t_DocumentIndex;
typedef rt::StringPrefixPrefectHashMap<CHAR, IndexedEntry_Base::__Entry*> t_DocumentPrefixIndex;

IndexedEntry_Base::IndexedEntry_Base()
{
	m_HeadDocument = NULL;
	m_DeletionDelay = 30*1000;
	m_IndexedDocumentCount = 0;

	m_DocumentIndex = NULL;
	m_DocumentPrefixIndex = NULL;
	m_DocumentIndexDirty = FALSE;
	m_DocumentPrefixIndexDirty = FALSE;

	m_HiddenFileInitial = 0;
}

void IndexedEntry_Base::_alias::Set(LPCSTR org_name, LPCSTR alias)
{
	int nlen = strlen(org_name);
	int alen = strlen(alias);
	VERIFY(_buf.SetSize(nlen + alen + 2));
	Name = rt::StringA_Ref(_buf.Begin(),nlen);
	Alias = rt::StringA_Ref(_buf.Begin()+nlen+1,alen);
	memcpy((LPVOID)Name.Begin(),org_name,nlen+1);
	memcpy((LPVOID)Alias.Begin(),alias,alen+1);
}

void IndexedEntry_Base::ClearAliases()
{
	m_IndexedDocumentCount -= m_Aliases.GetSize();
	m_Aliases.SetSize();
}

void IndexedEntry_Base::AddAlias(LPCSTR org_name, LPCSTR alias, BOOL prefix)
{
	//if(prefix)
	//	_CheckDump(alias<<" [P]\n");
	//else
	//	_CheckDump(alias<<" [A]\n");

	for(UINT i = 0;i < m_Aliases.GetSize();i++)
	{
		if(m_Aliases[i].Alias == alias && m_Aliases[i]._Prefix == prefix)
		{
			m_Aliases[i].Set(org_name, alias);
			if(!m_Aliases[i]._Prefix)
			{	m_DocumentIndexDirty = TRUE;
			}
			else
			{	m_DocumentPrefixIndexDirty = TRUE;
			}
			return;
		}
	}

	_alias& a = m_Aliases.push_back();
	a.Set(org_name, alias);
	a._Prefix = prefix;
	if(prefix)
		m_DocumentPrefixIndexDirty = TRUE;
	else
		m_DocumentIndexDirty = TRUE;
}

void IndexedEntry_Base::Add(const rt::StringA_Ref& name, LPVOID x)
{
	//_CheckDump(name<<'\n');

	t_DocumentIndex& docindex = *((t_DocumentIndex*)m_DocumentIndex);
	__Entry* nn = (__Entry*)x;

	t_DocumentIndex::iterator it;
	if( m_DocumentIndex &&
		(it = docindex.find(name)) != docindex.end()
	)
	{	// overwrite
		if(it->second)it->second->Flag |= EF_REMOVED;
		it->second = nn;
	}
	else
	{	m_IndexedDocumentCount++;
		m_DocumentIndexDirty = TRUE;
	}

	nn->Flag = nn->Flag & ~EF_REMOVED;
	nn->Next = m_HeadDocument;
	m_HeadDocument = nn;
}

BOOL IndexedEntry_Base::Remove(const rt::StringA_Ref& name)
{
	t_DocumentIndex& docindex = *((t_DocumentIndex*)m_DocumentIndex);

	t_DocumentIndex::iterator it;
	if( m_DocumentIndex &&
		(it = docindex.find(name)) != docindex.end()
	)
	{	ASSERT(it->second);
		it->second->Flag |= EF_REMOVED;
		m_IndexedDocumentCount--;
		m_DocumentIndexDirty = TRUE;
		return TRUE;
	}

	return FALSE;
}

void IndexedEntry_Base::RemoveAll()
{
	__Entry* p = m_HeadDocument;
	while(p)
	{	p->Flag |= EF_REMOVED;
		p = p->Next;
	}
	m_IndexedDocumentCount = 0;
	m_DocumentIndexDirty = TRUE;
}

void IndexedEntry_Base::RemoveSubname(const rt::StringA_Ref& name_suffix)
{
	__Entry* p = m_HeadDocument;
	while(p)
	{	
		if(!p->IsRemoved())
		{
			rt::StringA_Ref name = _getname(p);
			if(	name.GetLength() >= name_suffix.GetLength() &&
				memcmp(name.Begin(),name_suffix.Begin(),name_suffix.GetLength()) == 0 
			)
			{	p->Flag |= EF_REMOVED;
				m_IndexedDocumentCount--;
				m_DocumentIndexDirty = TRUE;
			}
		}	
		p = p->Next;
	}
}

UINT IndexedEntry_Base::UpdateIndex()
{
	UINT removed = 0;
	if(m_DocumentIndexDirty)
	{
		m_DocumentIndexDirty = FALSE;
		m_DocumentPrefixIndexDirty = TRUE;

		t_DocumentIndex& new_index = *(new t_DocumentIndex);
		new_index.set_empty_key(rt::StringA_Ref());
		UINT indexed = 0;
		new_index.rehash(m_IndexedDocumentCount * _hash_space_multiplier);

		//int co = 0;
		__Entry* p = m_HeadDocument;
		__Entry** prev = &m_HeadDocument;
		while(p)
		{	
			t_DocumentIndex::iterator it;
			if(	!p->IsRemoved() &&
				(it = new_index.find(_getname(p))) == new_index.end()
			)
			{	//co++;
				new_index.insert(std::pair<rt::StringA_Ref, __Entry*>(_getname(p), p));

				prev = &(p->Next);
				p = p->Next;
			}
			else
			{	// remove p
				*prev = p->Next;
				
				__Entry* del = p;
				p = p->Next;

				_release_delayed(del, m_DeletionDelay);
				removed++;
			}
		}

		for(UINT i=0;i<m_Aliases.GetSize();i++)
		{	if(!m_Aliases[i]._Prefix)
			{
				//co++;
				t_DocumentIndex::const_iterator it;
				if((it = new_index.find(m_Aliases[i].Name)) != new_index.end())
				{	new_index[m_Aliases[i].Alias] = it->second;
				}
			}
		}

		if(new_index.size())
		{
			t_DocumentIndex* del = (t_DocumentIndex*)m_DocumentIndex;
			m_DocumentIndex = &new_index;

			_SafeDel_Delayed(del, m_DeletionDelay);
		}
		else
		{	t_DocumentIndex* del = (t_DocumentIndex*)m_DocumentIndex;
			m_DocumentIndex = NULL;
			_SafeDel_Delayed(del, m_DeletionDelay);

			t_DocumentIndex* del2 = &new_index;
			_SafeDel(del2);
		}
	}

	if(m_DocumentPrefixIndexDirty)
	{
		t_DocumentIndex& new_index = *((t_DocumentIndex*)m_DocumentIndex);
		m_DocumentPrefixIndexDirty = FALSE;
		t_DocumentPrefixIndex* prefix_index = new t_DocumentPrefixIndex;

		rt::BufferEx<rt::StringA_Ref>	key;
		rt::BufferEx<__Entry*>			val;

		for(UINT i=0;i<m_Aliases.GetSize();i++)
		{	t_DocumentIndex::const_iterator it;
			if(m_Aliases[i]._Prefix && (it = new_index.find(m_Aliases[i].Name)) != new_index.end())
			{	
				for(UINT j=0;j<key.GetSize();j++)
					if(key[j] == m_Aliases[i].Alias)
					{
						val[j] = it->second;
						goto Updated_Previous;
					}
			
					key.push_back(m_Aliases[i].Alias);
					val.push_back(it->second);
Updated_Previous:
					continue;
			}
		}

		int hashsize = 0, hashmagic = 0;
		if(m_DocumentPrefixIndex)
		{
			t_DocumentPrefixIndex* p = (t_DocumentPrefixIndex*)m_DocumentPrefixIndex;
			hashsize = p->GetHashSpaceSize();
			hashmagic = p->GetHashMagic();
		}

		if(	key.GetSize() && 
			prefix_index->Build(NULL, key.GetSize(),key,val,hashsize,hashmagic)
		)
		{
			t_DocumentPrefixIndex* del = (t_DocumentPrefixIndex*)m_DocumentPrefixIndex;
			m_DocumentPrefixIndex = prefix_index;

			_SafeDel_Delayed(del, m_DeletionDelay);
		}
		else
		{	_SafeDel(prefix_index);
			t_DocumentPrefixIndex* del = (t_DocumentPrefixIndex*)m_DocumentPrefixIndex;
			m_DocumentPrefixIndex = NULL;
			_SafeDel_Delayed(del, m_DeletionDelay);
		}
	}

	return removed;
}

IndexedEntry_Base::__Entry* IndexedEntry_Base::Get(const rt::StringA_Ref& name) const
{
	if(m_DocumentIndex)
	{
		t_DocumentIndex& docindex = *((t_DocumentIndex*)m_DocumentIndex);
		t_DocumentIndex::const_iterator it = docindex.find(name);
		if(it != docindex.end())
		{	return it->second;
		}
		else if(m_DocumentPrefixIndex)
		{
			return (*((t_DocumentPrefixIndex*)m_DocumentPrefixIndex))[name];
		}
	}

	return NULL;
}

}}} // w32::inet::_meta_


void w32::inet::ServerNamespace::Document::ResetCacheControlHeader()
{
	AdditionalResponseHeaderLen = 0;
	LastModified = 0;
	MaxAge = 0;
	ETag[0] = 0;
}

void w32::inet::ServerNamespace::Document::UpdateCacheControlHeader()
{
	AdditionalResponseHeaderLen = 0;
	if(MaxAge)
	{
		AdditionalResponseHeaderLen += 
		sprintf_s(&AdditionalResponseHeader[AdditionalResponseHeaderLen],_additional_header_size - AdditionalResponseHeaderLen,
				  "Cache-Control: public, max-age=%d\r\n",MaxAge);

		if(LastModified)
		{
			static const char lastmod[] = "Last-Modified: ";
			memcpy(&AdditionalResponseHeader[AdditionalResponseHeaderLen], lastmod, sizeof(lastmod)-1);
			AdditionalResponseHeaderLen += sizeof(lastmod)-1;

			ASSERT(_additional_header_size - AdditionalResponseHeaderLen > 32);
	
			w32::CTime64<false> tt(LastModified);
			tt.FillInternetTimeFormat(&AdditionalResponseHeader[AdditionalResponseHeaderLen]);

			AdditionalResponseHeaderLen += 29;
			AdditionalResponseHeader[AdditionalResponseHeaderLen++] = '\r';
			AdditionalResponseHeader[AdditionalResponseHeaderLen++] = '\n';
		}
		if(ETag[0])
		{
			static const char etag[] = "ETag: ";

			memcpy(&AdditionalResponseHeader[AdditionalResponseHeaderLen], etag, sizeof(etag)-1);
			AdditionalResponseHeaderLen += sizeof(etag)-1;

			memcpy(&AdditionalResponseHeader[AdditionalResponseHeaderLen],ETag,_etag_length);
			AdditionalResponseHeaderLen += _etag_length;

			AdditionalResponseHeader[AdditionalResponseHeaderLen++] = '\r';
			AdditionalResponseHeader[AdditionalResponseHeaderLen++] = '\n';
		}
	}
	else
	{	static const char nocache[] = "Cache-Control: no-cache\r\n";
		AdditionalResponseHeaderLen = sizeof(nocache) - 1;
		memcpy(AdditionalResponseHeader, nocache, AdditionalResponseHeaderLen);
	}
}

void w32::inet::ServerNamespace::Document::CalculateETag()
{
	w32::CRC32 crc;

	crc.Update(Data,Length);
	crc.Finalize();

	DWORD tag[2] = { crc.GetCRC(), Length^0xdeedbeef };
	ASSERT(inet::Base64EncodeLength(8) == _etag_length);
	inet::Base64Encode(ETag,tag,sizeof(tag));
}

void w32::inet::ServerNamespace::Document::Release(w32::inet::ServerNamespace::Document* p, UINT TTL)
{
	struct _func
	{	static void _call(LPVOID x)
		{	w32::inet::ServerNamespace::Document* p = (w32::inet::ServerNamespace::Document*)x;
			if(*((DWORD*)p->ETag) == 0xcafebebe && p->Length == sizeof(inet::RequestHandler))
			{
				inet::RequestHandler& h = *((inet::RequestHandler*)&(p->Data));
				if(h.pReleaseRequestHandler)
				{	rt::StringA_Ref uri = p->GetName(p);
					h.pReleaseRequestHandler(h.Cookie,uri.Begin(),uri.GetLength());
				}
			}
			_SafeFree32AL(p);
		}
	};

	w32::CGarbageCollection::Delete(p,TTL,_func::_call);
}

rt::StringA_Ref w32::inet::ServerNamespace::Document::GetName(w32::inet::ServerNamespace::Document* p)
{
	return rt::StringA_Ref((LPSTR)(p->Data + p->Length), p->URI_Len);
}

const rt::StringA_Ref w32::inet::ServerNamespace::_MIMEs[19] = 
{
	rt::StringA_Ref("application/octet-stream"),

	rt::StringA_Ref("image/bmp"),
	rt::StringA_Ref("image/png"),
	rt::StringA_Ref("text/css"),
	rt::StringA_Ref("image/gif"),

	rt::StringA_Ref("text/html"),
	rt::StringA_Ref("image/x-icon"),
	rt::StringA_Ref("image/jpeg"),
	rt::StringA_Ref("application/x-javascript"),

	rt::StringA_Ref("application/pdf"),
	rt::StringA_Ref("image/svg+xml"),
	rt::StringA_Ref("application/x-shockwave-flash"),
	rt::StringA_Ref("image/tiff"),

	rt::StringA_Ref("text/plain"),
	rt::StringA_Ref("x-world/x-vrml"),
	rt::StringA_Ref("audio/x-wav"),
	rt::StringA_Ref("application/zip"),
	rt::StringA_Ref("text/xml"),
	rt::StringA_Ref("application/x-silverlight-app")
};

rt::StringA_Ref	w32::inet::ServerNamespace::Ext2MIME(const rt::StringA_Ref& ext)
{
	static const int ext_hash[29] = 
	{5, 18, 13, 14, 15, 3, 0, 13, 17, 12, 0, 4, 8, 6, 0, 0, 10, 0, 16, 7, 0, 2, 0, 0,11, 0, 0, 9, 1};

	static const WORD ext_hash_name[29] = 
	{0x7468, 0x6178, 0x7874, 0x7276, 0x6177, 0x7363, 0x0000, 0x6574, 0x6d78, 0x6974,0x0000, 0x6967, 0x736a, 0x6369, 
	 0x0000, 0x0000, 0x7673, 0x0000, 0x697a, 0x706a,0x0000, 0x6e70, 0x0000, 0x0000, 0x7773, 0x0000, 0x0000, 0x6470, 0x6d62
	};

	if(ext.GetLength()>=2)
	{
		int a = (ext[0] - 'A')%('a' - 'A') + 'a';
		int b = (ext[1] - 'A')%('a' - 'A') + 'a';
		int h = ((123^a)*b)%29;
		if(ext_hash_name[h] == a + (b<<8))
			return _MIMEs[ext_hash[h]];
	}

	return _MIMEs[0];
}

LPCSTR w32::inet::ServerNamespace::GetMIME(const rt::StringA_Ref& filename)
{
	rt::StringA_Ref ext = filename.GetExtName();
	if(ext.GetLength() && ext[0] == '.')
	{	ext.TruncateLeft(1);
		return Ext2MIME(ext);
	}
	return NULL;
}

w32::inet::ServerNamespace::Document* w32::inet::ServerNamespace::_Add(LPCSTR uri, UINT data_len, LPCVOID data, LPCSTR MIME)
{
	rt::StringA_Ref name(uri);

	Document* nn = (Document*)rt::Malloc32AL(name.GetLength() + sizeof(Document) + data_len, TRUE);
	if(!nn)return NULL;

	if(data)
		memcpy(nn->Data,data,data_len);
	nn->Flag = 0;
	nn->Length = data_len;
	nn->URI_Len = name.GetLength();
	memcpy(nn->Data + data_len, name, name.GetLength() + 1);
	nn->ResetCacheControlHeader();
	Document::GetName(nn).Replace('\\','/');

	m_Documents.Add(name,nn);
	if(!MIME)MIME = GetMIME(rt::StringA_Ref(uri));
	if(!MIME)MIME = _MIMEs[0];
	nn->MIME = rt::StringA_Ref(MIME);

	return nn;
}

BOOL w32::inet::ServerNamespace::IsExist(const rt::StringA_Ref& url) const
{
	return m_Documents.Get(url)!=NULL;
}

HRESULT w32::inet::ServerNamespace::OnRequest(w32::inet::WebThreadLocalBlock* pTLB)
{
	w32::inet::HttpRequest& req = pTLB->Request;
	
	if(req.Method == w32::inet::REGQUEST_GET)
	{	ASSERT(req.URI_len);
		Document* pDoc = m_Documents.Get(rt::StringA_Ref(req.pURI, req.URI_len));
		if(pDoc)
		{
			if(!pDoc->IsHandler())
			{
				LPCBYTE etag = NULL;
				UINT len;
				w32::CTime64<false> tm;

				if(	pDoc->ETag[0] &&
					(etag = req.GetETag(&len)) && 
					_etag_length == len
				)
				{	// control by eTag
					if(memcmp(etag,pDoc->ETag,_etag_length) == 0)
						return pTLB->SendRedirection(w32::inet::HTTP_NOT_MODIFIED,NULL,0);
					else goto SEND_FULLDATA;
				}
				else
				if(	pDoc->LastModified &&
					req.GetLastModified(tm)
				)
				{	// control by Last-Modified
					if(pDoc->LastModified <= tm.MakeTime())
						return pTLB->SendRedirection(w32::inet::HTTP_NOT_MODIFIED,NULL,0);
					else goto SEND_FULLDATA;
				}

SEND_FULLDATA:	if(pDoc->AdditionalResponseHeaderLen)
					VERIFY(pTLB->AppendResponseHeader(pDoc->AdditionalResponseHeader, pDoc->AdditionalResponseHeaderLen));
				else
					pTLB->AppendResponseHeader_NoCache();

				if(req.IsContentRanged())
				{
					if(	req.Range[0]<pDoc->Length )
					{
						w32::inet::HttpThreadLocalBlock::ContentRange part = 
						{	req.Range[0],
							min(req.Range[1],pDoc->Length-1),
							pDoc->Length
						};
						pTLB->AppendResponseHeader_ContentRange(part);
						return pTLB->SendResponse_NoCopy(&pDoc->Data[part.start],(SIZE_T)part.GetInterestedSize(),206,FALSE,pDoc->MIME)?S_OK:S_FALSE;
					}
					else
					{	pTLB->SendResponseHeadOnly(406);
						return S_FALSE;
					}
				}
				else
				{
					return pTLB->SendResponse_NoCopy(pDoc->Data, pDoc->Length, w32::inet::HTTP_OK, FALSE, pDoc->MIME)?S_OK:S_FALSE;
				}
			}
			else
			{	// handler
				ASSERT(pDoc->Length == sizeof(inet::RequestHandler));
				inet::RequestHandler& h = *((inet::RequestHandler*)&(pDoc->Data));
				return h.OnRequest(&pTLB->TLB_http_mod);
			}
		}
		else
		{	return S_REQUEST_NOT_HANDLED;
		}
	}
	else return S_REQUEST_NOT_HANDLED;
}

w32::inet::ServerNamespace::ServerNamespace()
{
	m_HiddenFileInitial = 0;
	m_MaxAgeDefault = 3600;
	ResetStatCounters();
}

void w32::inet::ServerNamespace::ResetStatCounters()
{
	stat_DocumentAdded = 
	stat_DocumentUpdated = 
	stat_DocumentRemoved = 0;

	stat_DocumentTotalSize = 
	stat_DocumentFileCount =
	stat_DocumentHandlerCount = 0;
}

void w32::inet::ServerNamespace::FinalUpdateIndex()
{
	stat_DocumentRemoved = m_Documents.UpdateIndex() - stat_DocumentUpdated;
	LPDocument doc = m_Documents.GetFirst();
	while(doc)
	{
		if(doc->IsHandler())
			stat_DocumentHandlerCount++;
		else
		{	stat_DocumentFileCount++;
			stat_DocumentTotalSize += doc->Length;
		}

		doc = m_Documents.GetNext(doc);
	}
}

w32::inet::ServerNamespace::Document* w32::inet::ServerNamespace::Update(BOOL* pReused, LPCSTR uri, UINT data_len, __time64_t ftime,LPCSTR MIME)
{
	Document* pDoc = m_Documents.Get(uri);
	if(pReused)*pReused = FALSE;
	if(pDoc)
	{
		pDoc->MaxAge = m_MaxAgeDefault;
		if(pDoc->LastModified == ftime && pDoc->Length == data_len)
		{	// the same content
			pDoc->Flag = pDoc->Flag & ~inet::_meta_::EF_REMOVED;
			if(pReused)*pReused = TRUE;
			if(MIME)pDoc->MIME = rt::StringA_Ref(MIME);
			return pDoc;
		}
		else if(pDoc->Length == data_len)
		{	// the length is same
			pDoc->Flag = pDoc->Flag & ~inet::_meta_::EF_REMOVED;
			pDoc->LastModified = ftime;
			if(MIME)pDoc->MIME = rt::StringA_Ref(MIME);
			stat_DocumentUpdated++;
			return pDoc;
		}
		else
		{	// length varied
			pDoc->Flag = pDoc->Flag | inet::_meta_::EF_REMOVED;
		}
		stat_DocumentUpdated++;
	}
	else
	{	stat_DocumentAdded++;
	}

	if(pDoc = _Add(uri, data_len, NULL, MIME))
	{	
		pDoc->LastModified = ftime;
		pDoc->MaxAge = m_MaxAgeDefault;
		
		if(!MIME)MIME = GetMIME(rt::StringA_Ref(uri));
		if(!MIME)MIME = _MIMEs[0];
		pDoc->MIME = rt::StringA_Ref(MIME);
	}

	return pDoc;
}

HRESULT w32::inet::ServerNamespace::LoadHandler(const RequestHandler& hd, LPCSTR uri)
{
	_CheckDump(uri<<'\n');
	Document* pDoc = NULL;
	pDoc = _Add(uri, sizeof(RequestHandler), &hd);
	if(pDoc)
	{	pDoc->ResetCacheControlHeader();
		*((DWORD*)pDoc->ETag) = 0xcafebebe;
		stat_DocumentAdded++;
	}

	return pDoc?S_OK:E_FAIL;
}

HRESULT w32::inet::ServerNamespace::LoadZipFile(LPCTSTR zip_fn, LPCSTR uri_suffix, BOOL merge, ULONGLONG* pfsz)
{
	rt::StringA path(uri_suffix);
	path.Replace('\\','/');
	if(path[path.GetLength()-1] != '/')path += '/';

	if(pfsz)*pfsz = 0;
	ULONGLONG totsz = 0;
	w32::CFileZip zip;
	if(zip.Open(zip_fn))
	{
		if(!merge)
			m_Documents.RemoveSubname(path);

		rt::StringA uri;
		HRESULT ret = S_OK;
		for(UINT i=0;i<zip.GetEntryCount();i++)
		{
			Document* pDoc = NULL;
			if(zip.GetFileName(i)[0]!=m_HiddenFileInitial && zip.IsFileExtractable(i) && zip.GetFileSize(i) <= _inmemory_document_size_max)
			{	uri = path + zip.GetFileName(i);
				FILETIME ftm;
				zip.GetFileTime(i,&ftm);
				__time64_t ft = w32::CTime64<false>(ftm).MakeTime();
				
				BOOL reused = TRUE;
				UINT flen = zip.GetFileSize(i);
				if(pDoc = Update(&reused, uri, flen, ft))
				{
					if(!reused)
					{
						if(	zip.ExtractFile(i, pDoc->Data) )
						{	if(m_MaxAgeDefault)
								pDoc->CalculateETag();
						}
						else
						{	pDoc->Flag |= inet::_meta_::EF_REMOVED;
							ret = S_FALSE;
							continue;
						}
					}
					totsz += flen;
					pDoc->UpdateCacheControlHeader();
				}
			}
		}

		if(pfsz)*pfsz = totsz;
		return ret;
	}

	if(pfsz)*pfsz = totsz;
	return w32::HresultFromLastError();
}

HRESULT	w32::inet::ServerNamespace::LoadDirectory(LPCTSTR directory, LPCSTR uri_suffix, BOOL merge, ULONGLONG* pfsz) // need call UpdateIndex() when done
{
	rt::StringA path(uri_suffix);
	path.Replace('\\','/');
	if(path[path.GetLength()-1] != '/')path += '/';

	if(pfsz)*pfsz = 0;
	ULONGLONG totsz = 0;
	rt::String dir(directory);
	{
		int i = dir.GetLength();
		while((dir[i-1] == '\\' || dir[i-1] == '/') && i>0)i--;
		dir.Truncate(0,i);
	}

	w32::CFileList filefind;
	if(SUCCEEDED(filefind.ParsePathName(m_HiddenFileInitial,dir + rt::String_Ref(_T("\\*.*"),4), w32::CFileList::PATHNAME_FLAG_RECURSIVE)))
	{
		if(!merge)
			m_Documents.RemoveSubname(path);

		rt::StringA uri;
		HRESULT ret = S_OK;
		for(UINT i=0;i<filefind.GetFileCount();i++)
		{
			LPCTSTR fn = filefind.GetFilename(i);
			Document* pDoc = NULL;
			w32::CFile64 file;

			ATL::CT2A _t2a(&fn[dir.GetLength()+1],CP_UTF8);
			rt::StringA_Ref fnn((LPSTR)_t2a);
			fnn.Replace('\\','/');

			if(file.Open(fn) && file.GetLength() <= _inmemory_document_size_max)
			{	uri = path + fnn;
				FILETIME ftm;
				file.GetTime_LastModify(&ftm);
				__time64_t ft = w32::CTime64<false>(ftm).MakeTime();
				UINT fsz = (UINT)file.GetLength();

				BOOL reused = TRUE;
				if(pDoc = Update(&reused, uri, fsz, ft))
				{
					if(!reused)
					{
						if(file.Read(pDoc->Data, fsz) == fsz)
						{	if(m_MaxAgeDefault)
								pDoc->CalculateETag();
						}
						else
						{	pDoc->Flag |= inet::_meta_::EF_REMOVED;
							ret = S_FALSE;
							continue;
						}
					}
					totsz += fsz;
					pDoc->UpdateCacheControlHeader();
				}
			}
		}
		if(pfsz)*pfsz = totsz;
		return ret;
	}
	if(pfsz)*pfsz = totsz;
	return w32::HresultFromLastError();
}

HRESULT w32::inet::ServerNamespace::LoadSingleFile(LPCTSTR fn, LPCSTR uri, LPCSTR MIME, ULONGLONG* pfsz)
{
	Document* pDoc = NULL;
	w32::CFile64 file;

	if(pfsz)*pfsz = 0;
	if(file.Open(fn) && file.GetLength() <= _inmemory_document_size_max)
	{
		FILETIME ftm;
		file.GetTime_LastModify(&ftm);
		__time64_t ft = w32::CTime64<false>(ftm).MakeTime();

		if(!MIME)MIME = GetMIME(rt::StringA_Ref(uri));
		if(!MIME)MIME = GetMIME(rt::StringA_Ref(ATL::CT2A(fn)));
		if(!MIME)MIME = _MIMEs[0];
		
		BOOL reused = TRUE;
		UINT fsz = (UINT)file.GetLength();
		if(pDoc = Update(&reused, uri, fsz, ft, MIME))
		{
			if(!reused)
			{
				if(file.Read(pDoc->Data, fsz) == fsz)
				{	if(m_MaxAgeDefault)
						pDoc->CalculateETag();
				}
				else
				{	pDoc->Flag |= inet::_meta_::EF_REMOVED;
					return w32::HresultFromLastError();
				}
			}
			pDoc->UpdateCacheControlHeader();
			if(pfsz)*pfsz = fsz;
			return S_OK;
		}
	}
	return w32::HresultFromLastError();
}

