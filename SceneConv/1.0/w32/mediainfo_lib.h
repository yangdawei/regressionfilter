#pragma once
//////////////////////////////////////////////////////////////////////
// Visual Computing Foundation Classes (VCFC)
//					Jiaping Wang  2006.2
//					e_boris2002@hotmail.com
//
// (C) Copyright Jiaping Wang 2004.
//
// Permission to copy, use, modify, sell and distribute this software
// is granted provided this copyright notice appears in all copies.
// This software is provided "as is" without express or implied
// warranty, and with no claim as to its suitability for any purpose.
//
// Absolutely no warranty!!
//////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////
//  Brief Description
//
//  sqlite_plus.h
//
//  Warpping class of Sqlite
//
//  Library Dependence: sqlite.lib
//
//////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////
// Revise History
//
// Inital version	2010.7.1		Jiaping
//
//////////////////////////////////////////////////////////////////////

#include "w32_basic.h"


namespace w32
{

class CMediaFileInfo
{
	void*	pObj;
	void*	pString;
	__int64 _open_buffer_file_size;

public:
	enum _tagStreamType
	{
		Stream_Any = -1,
		Stream_General = 0,             ///< StreamKind = General
		Stream_Video,                   ///< StreamKind = Video
		Stream_Audio,                   ///< StreamKind = Audio
		Stream_Text,                    ///< StreamKind = Text
		Stream_Chapters,                ///< StreamKind = Chapters
		Stream_Image,                   ///< StreamKind = Image
		Stream_Menu,                    ///< StreamKind = Menu
		Stream_Max
	};

public:
	CMediaFileInfo();
	~CMediaFileInfo();

	bool			Open(const wchar_t* filename);
	void			Close();
	const wchar_t*	GetInformation();
	const wchar_t*	GetString(const wchar_t* pParamName,	int StreamType = -1);
	int				GetInteger(const wchar_t* pParamName,	int StreamType = -1);
	double			GetFloat(const wchar_t* pParamName,		int StreamType = -1);

	void			OpenBuffer_Begin(__int64 file_size);
	bool			OpenBuffer_Push(const void * pData, unsigned int Size, __int64 * pOffsetNext); // return false to stop, *pOffsetNext == -1 continue to read, no seek
	void			OpenBuffer_End();
};


} // namespace w32


/******************** Example Code of using OpenBuffer_xxx ****************************\

	MediaFileInfo		fileinfo;
	rt::Buffer<BYTE>	buf;
	VERIFY(buf.SetSize(5*1024));  // 5k buffer, can be changed
	UINT		ReadSize = 0;
	LONGLONG	SeekTo = 0;

	amdev::CFile* pFile = amdev::CFile::UnifiedOpen(FileName,TRUE);
	if(pFile)
	{
		fileinfo.OpenBuffer_Begin(pFile->GetLength());

		for(UINT j=0; j<1024; j++)  // give up of 5M data has been read, can be changed
		{
			if(	(ReadSize = pFile->Read(buf,buf.GetSize())) &&
				(fileinfo.OpenBuffer_Push(buf,ReadSize,&SeekTo))
			)
			{	if(SeekTo!=-1 && pFile->Seek(SeekTo) != SeekTo)
					break;
			}
			else break;
		}

		_SafeDel(pFile);
	}

	fileinfo.OpenBuffer_End();
	_CheckDump(fileinfo.GetInformation());

\***************************************************************************************/


