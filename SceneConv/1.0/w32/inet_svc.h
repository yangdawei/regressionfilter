#pragma once

//////////////////////////////////////////////////////////////////////
// Base classes for Network Services
//					Jiaping Wang  2007.5
//					e_boris2002@hotmail.com
//
// Copyright (C) Jiaping Wang 2007.
//
// Permission to copy, use, modify, sell and distribute this software
// is granted provided this copyright notice appears in all copies.
// This software is provided "as is" without express or implie// warranty, and with no claim as to its suitability for any purpose.
//
// Absolutly no garanty!!
//////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////
//  Brief Description
//
//  inet_svc.h
//  light weight Socket (TCP) server
//  light weight HTTP containor
//
//  Library Dependence: inet.lib
//
//////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////
// Revise History
//
// Inital version	2009.6.21		Jiaping
//
//////////////////////////////////////////////////////////////////////

#include "inet.h"
#include "w32_basic.h"

#define ENABLE_SERV_STATISTIC	// static library was compiled with ENABLE_SERV_STATISTIC enabled
#include <intrin.h>

namespace w32
{

class CPerformanceCounters;

namespace inet
{

class Connection;
class SocketServCore;

struct ThreadLocalBlock
{
	UINT		ThreadId;
	Connection* pConn;
	ThreadLocalBlock(inet::SocketServCore* pServCore){ ThreadId = INFINITE; }
};

class SocketServCore
{
protected:
	typedef void (*FUNC_DestroyConnection)(LPVOID);
	FUNC_DestroyConnection			_DestroyConnectionFun;

public:
	class _Listener:public CSocket
	{
		friend class SocketServCore;
	public:
		CInetAddr					LocalAddress;
		HANDLE						AcceptingThread;
		DWORD						AcceptingThreadId;

		SocketServCore*				pServCore;
		rt::Buffer<Connection*>		Connections;
		UINT						ConnectionsUsed;

		_Listener();
		void						DumpConnection();
		void						GarbageConnectionRemoval();

	protected:
		DWORD						LastGarbageProbeTick;
	};

public:
	static const int _listen_backlog = 32;
	static const int _receiving_chuck_size = 1023;	// byte
	static const int _sending_chuck_size = 1024;	// byte
	static const int _garbage_session_probe_interval = 1000; //msec
	static const int _default_session_TTL = 15000; // msec
	static const int _default_session_TTL_removal_addon = 5000;
	static const int _default_zombie_session_TTL = 1000; // msec
	static const int _default_concurrent_session_max = 1000;
	static const int _favorable_recieving_buffer_size = 10*1024; // 10k
	static const int _favorable_sending_buffer_size = 256*1024; // 256k
	
protected:
	rt::Buffer<_Listener>	m_Listeners;
	LPTHREAD_START_ROUTINE	m_pPooledDataTransferThreadFunc;
	LPTHREAD_START_ROUTINE	m_pConnectionManagerThreadFunc;
	UINT					m_ConnectionPoolSize;

protected:
	HANDLE					m_hCompletePort;
	rt::Buffer<HANDLE>		m_hPooledThread;
	rt::Buffer<DWORD>		m_hPooledThreadId;

	SocketServCore::SocketServCore();
	SocketServCore::~SocketServCore();

#ifdef ENABLE_SERV_STATISTIC
public: // statistic
	volatile	UINT		stat_SentKiloByte;
	volatile	UINT		stat_RecvKiloByte;
	volatile	UINT		stat_SentByte;
	volatile	UINT		stat_RecvByte;

	volatile	LONG		stat_Connected;
	volatile	LONG		stat_ConnectionRefused;
	volatile	LONG		stat_ConnectionFake;
	volatile	LONG		stat_ConnectionTimeout;
#endif // #ifdef ENABLE_SERV_STATISTIC

public:
	UINT	GetListenerCount() const;
	UINT	GetConnectionCount(UINT listern_index) const;
	UINT	GetConnectionNumberLimit() const;	// limit per listener
	UINT	GetTotalConnectionCount() const;

	UINT	GetThreadCount() const;

	const CInetAddr& GetListenerAddress(int i) const { return m_Listeners[i].LocalAddress; }
	BOOL			 IsListenerWorking(int i) const { return m_Listeners[i].AcceptingThread!=NULL; }

protected:
	enum
	{	SERVER_UNINITIALIZED = 0,
		SERVER_STOPPED,
		SERVER_PAUSE,
		SERVER_RUN,
	};
	DWORD				m_ServerState;
	///////////////////////////////////////////////
	// Server State
	// UNINITIALIZED:	socket closed, IOCP closed,  thread pool is empty
	// STOPPED:			socket closed, IOCP created, thread pool is empty
	//					existing connection will be disconnected
	//					in-progress data processing will be finished
	// PAUSE:			socket opened, IOCP created, thread pool created
	//					all incoming connection will be rejected
	//					all incoming data will be discard
	//					existing connection won't be disconnected
	//					in-progress data processing will be finished
	// RUN:				socket opened, IOCP created, thread pool created
	//					all incoming connection will be accepted
	//					all incoming data will be received, processed

	//w32::CCriticalSection	m_ServerStateCCS;

public:
	// UNINITIALIZED-->STOPPED
	BOOL	Initialize(const CInetAddr* pAddr, UINT addr_count = 1, UINT thread_count = 0); // thread_count = 0 to used default value
	// STOPPED-->UNINITIALIZED
	void	Uninitialize();
	void	EmergencyExit();	// Any State --> UNINITIALIZED, for crash handling only

	BOOL	Start();	// PAUSE/STOPPED-->RUN
	BOOL	Pause();	// RUN/STOPPED-->PAUSE
	BOOL	Stop();		// RUN/PAUSE-->STOPPED
};


class Connection  // all members will be maintained by SocketServCore
{	
	static const int _overlapped_sending_chunk_max = 8;

	CSocket		m_Socket;		// indicator for removal

	WSABUF		m_OverlappedSendingChunk[_overlapped_sending_chunk_max];
	UINT		m_OverlappedSendingChunkCount;		// As indicator
	UINT		m_OverlappedSendingChunkSent;		// offset to m_OverlappedSendingBuffers
	UINT		m_OverlappedSentInChunk;			// how much has been sent in current buf

public:
	BOOL		m_DisconnectAfterSending;

	OVERLAPPED	m_Overlapped;
	OVERLAPPED	m_SendingOverlapped;
	HRESULT		ReceiveNextData();
	HRESULT		SendNextData();	//S_OK=sendok, S_FALSE=nothing sent
	BOOL		IsOverlappedSending(){ return m_OverlappedSendingChunkCount; }

public:
	__forceinline BOOL WSASend_Sync(LPWSABUF lpBuffers, DWORD dwBufferCount, LPDWORD lpNumberOfBytesSent,DWORD dwFlags = 0)
	{	
		ASSERT(lpNumberOfBytesSent);
		int ret = ::WSASend(m_Socket,lpBuffers,dwBufferCount,lpNumberOfBytesSent,dwFlags,NULL,NULL);
#ifdef ENABLE_SERV_STATISTIC
		SocketServCore* p = m_pServ;
		if(p)
		{	_InterlockedExchangeAdd((volatile long *)&p->stat_SentByte,*lpNumberOfBytesSent);
			_InterlockedExchangeAdd((volatile long *)&p->stat_SentKiloByte,(*lpNumberOfBytesSent + 512)/1024);
		}
#endif
		return ret==0;
	}
	__forceinline BOOL WSASend_Overlapped(LPCVOID pData, UINT len) // pData should pointing a memory that is available during sending
	{	
		ASSERT(m_OverlappedSendingChunkCount == 0);  // should be called once for each request handling
		ASSERT(len);	ASSERT(pData);
		m_OverlappedSendingChunkCount = 1;
		m_OverlappedSendingChunk[0].buf = (LPSTR)pData;
		m_OverlappedSendingChunk[0].len = len;
		m_OverlappedSendingChunkSent = 0;
		m_OverlappedSentInChunk = 0;

		return TRUE;
	}
	__forceinline BOOL WSASend_Overlapped(WSABUF* pBuffer, UINT buf_co)
	{	
		ASSERT(m_OverlappedSendingChunkCount == 0);  // should be called once for each request handling
		ASSERT(buf_co);	ASSERT(pBuffer);
		ASSERT(pBuffer->len);
		ASSERT(buf_co <=_overlapped_sending_chunk_max);
		memcpy(m_OverlappedSendingChunk,pBuffer,sizeof(WSABUF)*buf_co);
		m_OverlappedSendingChunkCount = buf_co;
		m_OverlappedSendingChunkSent = 0;
		m_OverlappedSentInChunk = 0;

		return TRUE;
	}
	__forceinline void AssignRawSocket(SOCKET s){ m_Socket.Attach(s); }
	__forceinline void DisableSocket(){ shutdown(m_Socket,SD_BOTH); }
public:
	SocketServCore*		m_pServ;
	CInetAddr			m_RemoteAddress;
	CInetAddr			m_LocalAddress;
	volatile DWORD		m_LastActiveTick;
	DWORD				m_TimeToLive;
	rt::BufferEx<BYTE>	m_RecvBuffer;	//received data is accumulated here, and is remove from head by RemoveData
	volatile UINT		m_RecvBufferUsed;

	__forceinline BOOL	IsEmpty(){ return m_Socket.IsEmpty(); }
	__forceinline void	ActiveTick(){ m_LastActiveTick = GetTickCount(); }
	__forceinline void	ActiveTick(DWORD tick){ m_LastActiveTick = tick; }
	__forceinline UINT	GetIdleTime(DWORD cur_time) // msec
	{
		if(cur_time > m_LastActiveTick)
			return (UINT)(cur_time - m_LastActiveTick);
		else
			return (UINT)(0x100000000LL + cur_time - m_LastActiveTick);
	}
	void		SetTimeToLive(DWORD msec);
	void		RemoveData(UINT size);
	void		Clear();
	void		ClearOverlappedSending();
	void		DestroyConnection();
	void		NormalizeBuffers();

public:
	rt::BufferEx<BYTE>		m_OverlapSendingBuffer;		//dynamic overlapped sending data is accumulated here, and is remove after sending completion or connection reset
	volatile UINT			m_OverlapSendingBufferUsed;

	__forceinline void		ClearSendingBuffer(){ m_OverlapSendingBufferUsed = 0; }
	__forceinline void		FinalizeSendingChunk(int real_size_taken){ m_OverlapSendingBufferUsed+=real_size_taken; ASSERT(m_OverlapSendingBufferUsed<=m_OverlapSendingBuffer.GetSize()); }  // call FinalizeSendingChunk after each ClaimSendingChunk call
	__forceinline LPVOID	ClaimSendingChunk(int claimed_size_byte)
	{		if(m_OverlapSendingBufferUsed + claimed_size_byte > m_OverlapSendingBuffer.GetSize())
			{	if(!m_OverlapSendingBuffer.ChangeSize(max(m_OverlapSendingBuffer.GetSize()*2,m_OverlapSendingBufferUsed + claimed_size_byte)))
					return NULL;
			};
			return &m_OverlapSendingBuffer[m_OverlapSendingBufferUsed];
	}
	__forceinline BOOL		AppendSendingChunk(LPCVOID pData, int len)
	{		LPVOID p = ClaimSendingChunk(len);	if(p==NULL)return FALSE;
			memcpy(p,pData,len);	FinalizeSendingChunk(len);	return TRUE;
	}
};

} // namespace inet


namespace _meta_
{
	template<typename target, typename default_type>
	struct _reduce_target_type{ typedef target t_Result; };
		template<typename default_type>
		struct _reduce_target_type<void,default_type>{ typedef default_type t_Result; };
}

///////////////////////////////////////////////////
// socket server, based on I/O complete port
// I/O among each session is not executed parallelly
// all sessions are executed in parallel
// by default the server is an echo server
template<class t_Derived = void, class t_TLB = inet::ThreadLocalBlock, class t_Conn = inet::Connection>
class CSocketServ:public inet::SocketServCore
{
	ASSERT_STATIC(COMPILER_INTRINSICS_IS_BASE_OF(inet::ThreadLocalBlock,t_TLB));
	ASSERT_STATIC(COMPILER_INTRINSICS_IS_BASE_OF(inet::Connection,t_Conn));
	typedef typename _meta_::_reduce_target_type<t_Derived,CSocketServ>::t_Result t_ServCore;

private:
	volatile  LONG		__TLB_slot;

protected:
	t_Conn*				m_pConnectionPool;
	t_TLB*				m_ThreadLocalBlockPool;

protected: // default handler
	static HRESULT OnDataReceived(t_TLB* pTLB, UINT newly_received_size)
	{	// echo server by default
		pTLB->pConn->m_RecvBuffer[pTLB->pConn->m_RecvBufferUsed] = 0;
		WSABUF buf = { newly_received_size, (LPSTR)pTLB->pConn->m_RecvBuffer.Begin() };
		DWORD sent;
		pTLB->pConn->WSASend_Sync(&buf,1,&sent);
		pTLB->pConn->RemoveData(newly_received_size);
		return S_OK;
	}
	void DeletePools()
	{
		for(UINT i=0; i<m_Listeners.GetSize()*m_ConnectionPoolSize;i++)
			m_pConnectionPool[i].~t_Conn();
		for(UINT i=0; i<m_hPooledThread.GetSize();i++)
			m_ThreadLocalBlockPool[i].~t_TLB();
		_SafeFree32AL(m_pConnectionPool);
		_SafeFree32AL(m_ThreadLocalBlockPool);
	}
protected:
	typedef typename _meta_::_reduce_target_type<t_Derived,CSocketServ>::t_Result t_ServCore;

public:
	CSocketServ()
	{	m_pPooledDataTransferThreadFunc = _PooledDataTransferingThread;
		m_pConnectionManagerThreadFunc = _ConnectionManagingThread;
		m_pConnectionPool = NULL;
		m_ThreadLocalBlockPool = NULL;
		__TLB_slot = 0;
		struct _func
		{	static void _DC(LPVOID x){ ((t_Conn*)x)->DestroyConnection(); }
		};
		_DestroyConnectionFun = _func::_DC;
	}
	~CSocketServ(){ Uninitialize(); DeletePools(); }
	t_TLB& GetThreadLocalBlock(UINT i) 
	{	ASSERT(i<GetThreadCount());
		return m_ThreadLocalBlockPool[i];
	}
	const t_TLB& GetThreadLocalBlock(UINT i) const
	{	ASSERT(i<GetThreadCount());
		return m_ThreadLocalBlockPool[i];
	}
	// UNINITIALIZED-->STOPPED
	BOOL	Initialize(const CInetAddr* pAddr, UINT addr_count = 1, UINT thread_count = 0, UINT concurrent_session_max = 0)
	{	
		ASSERT(addr_count);
		ASSERT(m_pConnectionPool == NULL);
		ASSERT(m_ThreadLocalBlockPool == NULL);

		if(concurrent_session_max)m_ConnectionPoolSize = concurrent_session_max;

		if(__super::Initialize(pAddr,addr_count,thread_count))
		{	
			m_pConnectionPool = (t_Conn*)_Malloc32AL(BYTE,m_ConnectionPoolSize * m_Listeners.GetSize()*sizeof(t_Conn));
			ASSERT(m_pConnectionPool);
			for(UINT i=0;i<m_ConnectionPoolSize * m_Listeners.GetSize();i++)
				new (&m_pConnectionPool[i]) t_Conn();

			for(UINT i=0;i<m_Listeners.GetSize();i++)
			{	
				VERIFY(m_Listeners[i].Connections.SetSize(m_ConnectionPoolSize));
				for(UINT j=0;j<m_ConnectionPoolSize;j++)
					m_Listeners[i].Connections[j] = &m_pConnectionPool[i*m_ConnectionPoolSize + j];
				m_Listeners[i].ConnectionsUsed = 0;
			}

			m_ThreadLocalBlockPool = (t_TLB*)_Malloc32AL(BYTE,GetThreadCount()*sizeof(t_TLB));
			for(UINT i=0;i<m_hPooledThread.GetSize();i++)
			{
				new (&m_ThreadLocalBlockPool[i]) t_TLB((t_ServCore*)this);
				m_ThreadLocalBlockPool[i].ThreadId = i;
			}

			return TRUE;
		}
		return FALSE;
	}
	// STOPPED-->UNINITIALIZED
	void	Uninitialize()
	{	__super::Uninitialize();
		DeletePools();
	}
	BOOL	Start()	// PAUSE/STOPPED-->RUN
	{	__TLB_slot = 0;
		return __super::Start();
	}
protected:
	static DWORD WINAPI _ConnectionManagingThread(LPVOID p)
	{
		inet::SocketServCore::_Listener* pListener = (inet::SocketServCore::_Listener*)p;
		CSocketServ* pServCore = ((CSocketServ*)pListener->pServCore);
		HANDLE complete_port = pServCore->m_hCompletePort;

		while(pServCore->m_ServerState == SocketServCore::SERVER_RUN)
		{	
			CInetAddr peer;
			int la = sizeof(peer);
			//SOCKET hSock = WSAAccept(*pListener,peer,&la,_tagCallback::AcceptingCondition,(DWORD_PTR)pListener);
			SOCKET hSock = accept(*pListener,peer,&la);
			if(INVALID_SOCKET != hSock)
			{
				if(pListener->ConnectionsUsed < pListener->Connections.GetSize())
				{
					inet::Connection* pConnection = (t_Conn*)pListener->Connections[pListener->ConnectionsUsed];

					{	linger l = {1,0};
						VERIFY(0==::setsockopt(hSock,SOL_SOCKET,SO_LINGER,(char*)&l,sizeof(linger)));
					}

					((t_Conn*)pConnection)->Clear();
					((t_Conn*)pConnection)->ClearOverlappedSending();
					pConnection->m_pServ = pServCore;
					pConnection->AssignRawSocket(hSock);
					pConnection->m_RemoteAddress = peer;
					pConnection->m_LocalAddress = pListener->LocalAddress;
					pConnection->SetTimeToLive(SocketServCore::_default_session_TTL);

					// associate it with completion port
					VERIFY(complete_port == CreateIoCompletionPort((HANDLE)hSock, complete_port, (ULONG_PTR)pConnection, 0));
					if(SUCCEEDED(pConnection->ReceiveNextData()))
					{

#ifdef ENABLE_SERV_STATISTIC
						_InterlockedIncrement(&pServCore->stat_Connected);
#endif
						pListener->ConnectionsUsed++; // keep this entry
						//_CheckDump("\tConnected from "<<peer<<"\n");
					}
					else
					{
#ifdef ENABLE_SERV_STATISTIC
						_InterlockedIncrement(&pServCore->stat_ConnectionFake);
#endif
						//_CheckDump("Failed to start I/O "<<WSAGetLastError()<<", from "<<peer<<"\n");
						//pConnection->DestroyConnection();
						pServCore->_DestroyConnectionFun(pConnection);
					}
				}
				else
				{	
#ifdef ENABLE_SERV_STATISTIC
					_InterlockedIncrement(&pServCore->stat_ConnectionRefused);
#endif
					closesocket(hSock);
				}
			}
			else if(!pListener->IsValid())
			{	// stopping
				return 0;
			}

			/////////////////////////////
			// garbage connection removal
			if(pServCore->m_ServerState == SocketServCore::SERVER_RUN)
				pListener->GarbageConnectionRemoval();
		}

		return 0;
	}

	static DWORD WINAPI _PooledDataTransferingThread(LPVOID p_in)
	{
		t_ServCore * p = (t_ServCore*)(inet::SocketServCore*)p_in;
		HANDLE IOCP_port = p->m_hCompletePort;

		LONG id = InterlockedIncrement(&p->__TLB_slot);
		id--;
		ASSERT(id>=0 && id<(long)p->m_hPooledThread.GetSize());
		t_TLB& ThreadLocal = p->m_ThreadLocalBlockPool[id];

		while(p->m_ServerState == SERVER_RUN)
		{	
			inet::Connection*	pConnection = NULL;
			DWORD				BytesTransferred = 0;
			OVERLAPPED*			pOverlap = NULL;
			GetQueuedCompletionStatus(	IOCP_port, 
										&BytesTransferred, 
										(PULONG_PTR)&pConnection, 
										(LPOVERLAPPED*)&pOverlap, INFINITE);

			if(pConnection){}
			else
			{	// server asks for exit
				break;
			}

			if(BytesTransferred){}
			else
			{	// asks for closing
				goto TERMINATE_CONNECTION;
			}

			//ASSERT(!pConnection->IsEmpty());
			//ASSERT(pConnection->m_pServ);

			if( !pConnection->IsEmpty() && pConnection->m_pServ ){}
			else
			{	// shit, some bug here
				goto TERMINATE_CONNECTION;
			}

			DWORD tick = ::GetTickCount();

			if(pConnection->GetIdleTime(tick) <= pConnection->m_TimeToLive){}
			else
			{	// time-out
				goto TERMINATE_CONNECTION;
			}

			pConnection->ActiveTick(tick);

			if(pConnection->IsOverlappedSending()) // sending data
			{	
				if(pOverlap == &pConnection->m_SendingOverlapped){}
				else
				{	// shit, some bug here
					goto TERMINATE_CONNECTION;
				}

#ifdef ENABLE_SERV_STATISTIC
				_InterlockedExchangeAdd((volatile long *)&p->stat_SentByte,BytesTransferred);
				_InterlockedExchangeAdd((volatile long *)&p->stat_SentKiloByte,(BytesTransferred + 512)/1024);
#endif
				HRESULT hret;
				if(SUCCEEDED(hret = pConnection->SendNextData()))
				{
					if(hret == S_OK)
					{	
						continue;
					}
					else // sending completed
					{	
						pConnection->ClearOverlappedSending();
								
						if(!pConnection->m_DisconnectAfterSending && SUCCEEDED(pConnection->ReceiveNextData()))
						{	continue;
						}
						else goto TERMINATE_CONNECTION;
					}
				}
				else goto TERMINATE_CONNECTION;
			}
			else // receiving data
			{
				if(pOverlap == &pConnection->m_Overlapped){}
				else
				{	// shit, some bug here
					goto TERMINATE_CONNECTION;
				}

#ifdef ENABLE_SERV_STATISTIC
				_InterlockedExchangeAdd((volatile long *)&p->stat_RecvByte,BytesTransferred);
				_InterlockedExchangeAdd((volatile long *)&p->stat_RecvKiloByte,(BytesTransferred + 512)/1024);
#endif
				pConnection->m_RecvBufferUsed += BytesTransferred;
				ThreadLocal.pConn = pConnection;

				pConnection->m_DisconnectAfterSending = FALSE;
				HRESULT ret = t_ServCore::OnDataReceived(&ThreadLocal,BytesTransferred);
				pConnection->ActiveTick();

				if(pConnection->IsOverlappedSending()) // handler called lanunch overlapped sending
				{
					if(FAILED(ret))pConnection->m_DisconnectAfterSending = TRUE;
					if(FAILED(pConnection->SendNextData()))goto TERMINATE_CONNECTION; 
					continue;
				}
				else if(SUCCEEDED(ret) && SUCCEEDED(pConnection->ReceiveNextData()))
				{	continue;
				}
				else goto TERMINATE_CONNECTION;
			}

			ASSERT(0);
TERMINATE_CONNECTION:
			pConnection->DisableSocket();
			//pConnection->DestroyConnection();
			p->_DestroyConnectionFun(pConnection);
		}

		return 0;
	}
};

namespace inet
{

enum _tag_HTTP_STATUS
{	
	HTTP_OK						= 200,
	HTTP_CREATED				= 201,
	HTTP_ACCEPTED				= 202,
	HTTP_NON_AUTH				= 203,
	HTTP_NO_CONTENT				= 204,
	HTTP_RESET_CONTENT			= 205,
	HTTP_PARTIAL_CONTENT		= 206,

	HTTP_MULTIPLE				= 300,
	HTTP_MOVED					= 301,
	HTTP_FOUND					= 302,
	HTTP_SEE_OTHER				= 303,
	HTTP_NOT_MODIFIED			= 304,
	HTTP_USE_PROXY				= 305,
	HTTP_SWITCH_PROXY			= 306,
	HTTP_REDIRECT				= 307,

	HTTP_BAD_REQUEST			= 400,
	HTTP_UNAUTHORIZED			= 401,
	HTTP_PAYMENT_REQUIRED		= 402,
	HTTP_FORBIDDEN				= 403,
	HTTP_NOT_FOUND				= 404,
	HTTP_NOT_ALLOWED			= 405,
	HTTP_NOT_ACCEPTABLE			= 406,
	HTTP_PROXY_AUTH				= 407,
	HTTP_TIMEOUT				= 408,
	HTTP_CONFLICT				= 409,

	HTTP_INTERNAL_ERROR			= 500,
	HTTP_NOT_IMPLEMENTED		= 501,
	HTTP_BAD_GATEWAY			= 502,
	HTTP_UNAVAILABLE			= 503,
	HTTP_GATEWAY_TIMEOUT		= 504,
	HTTP_VERSION				= 505,
	HTTP_NEGOTIATES				= 506,
	HTTP_INSUFFICIENT_STORAGE	= 507,

	HTTP_BANDWIDTH_EXCEEDED		= 509,
};

enum _tag_HTTP_METHOD
{	// defined by hash valus of ((H+(H>>15))%17) - 1;
	REGQUEST_GET	= 11,
	REGQUEST_HEAD	= 12,
	REGQUEST_POST	= 13,
	REGQUEST_PUT	= 2,
	REGQUEST_DELETE = 10,
	REGQUEST_TRACE	= 0,
	REGQUEST_CONNECT= 1,
	REGQUEST_MAX	= 13
};

struct HttpRequest
{
	DWORD		Method;	// one of REGQUEST_xxx
	LPCSTR		pURI;	// UTF-8, thing before '?'
	UINT		URI_len;
	LPCSTR		pQuery;	// things after '?'
	UINT		Query_len;
	LPCBYTE		pContent;
	UINT		Content_len;
	ULONGLONG	Range[2];
	LPCSTR		pRemainHeader;
	UINT		RemainHeader_len;

	BOOL		IsPreparingDataPost() const { return (pContent == NULL) && (Content_len>0); }
	BOOL		IsDataPost() const { return Content_len>0; }
	LPCBYTE		GetETag(UINT* plen){ return GetAdditionalField("If-None-Match: ",plen); }	// return is NOT zero-terminated
	BOOL		IsContentRanged() const { return Range[0]<Range[1]; }
	BOOL		GetLastModified(w32::CTime64<false>& tm)
				{	LPCBYTE data; UINT len;
					return	(data = GetAdditionalField("If-Modified-Since: ",&len)) &&
							len >= 29 && tm.ParseInternetTimeFormat((LPCSTR)data);
				}
	BOOL		IsKeepAlive() const // is KeepAlive required
				{	ASSERT(pRemainHeader);		LPCSTR http;
					return	(http = strstr((LPCSTR)pRemainHeader,"Connection: ")) &&
							(http[12] == 'k' || http[12] == 'K');
				}
	LPCBYTE		GetAdditionalField(LPCSTR field_name, UINT* plen) const  // return is NOT a zero-terminated string, use pLen to determine its end
				{	LPCSTR p = strstr((LPCSTR)pRemainHeader,field_name);
					if(p)
					{	p += strlen(field_name);	LPCSTR end;
						if(end = strchr(p,'\r'))
						{	if(plen)*plen = (UINT)(end - p);
							return (LPCBYTE)p;
					}	}
					if(plen)*plen = 0;	return NULL;
				}
};

struct HttpConnection:public Connection
{
	UINT		m_RequestPacketSize;	// for data posting
	UINT		m_RequestHeaderSize;

	void		Clear()
	{	__super::Clear();
		m_RequestPacketSize = 0;
		m_RequestHeaderSize = 0;
	}
};

class HttpServCore;

struct HttpThreadLocalBlock:public ThreadLocalBlock
{	
	static const int _additional_header_size = 256;
	char			AdditionalResponseHeader[_additional_header_size];
	UINT			AdditionalResponseHeaderLen;
	HttpServCore*	pHttpCore;

	struct ContentRange
	{	ULONGLONG	start;	// 0-based
		ULONGLONG	end;
		ULONGLONG	total;
		ULONGLONG	GetInterestedSize(){ return end-start+1; }
	};
	HttpThreadLocalBlock(inet::SocketServCore* pServCore);
	void Clear();
	// All functions can only be called within request handlers
	BOOL				SendResponseHeadOnly(UINT code = 404, BOOL close_session = TRUE);
	BOOL				SendRedirection(UINT code, LPCSTR redirected_url, int url_len);
	BOOL				SendResponse(LPCBYTE pData, SIZE_T len, UINT code = 200, BOOL close_session = FALSE, LPCSTR content_type = NULL); // response w/ Content-Length
	BOOL				SendResponse_NoCopy(LPCBYTE pData, SIZE_T len, UINT code = 200, BOOL close_session = FALSE, LPCSTR content_type = NULL); // response w/ Content-Length
	// pData will not be copied, so caller must make sure the memory is available during sending
						
	void				SendChunkResponse_Begin();
	BOOL				SendChunkResponse(char ch){ return SendChunkResponse(&ch,1); }
	BOOL				SendChunkResponse(LPCVOID pData, UINT len){ pConn->ActiveTick(); return pConn->AppendSendingChunk(pData,len); }
	BOOL				SendChunkResponse_EncodeHTML(LPCSTR PlainText, UINT len);
	BOOL				SendChunkResponse_End(UINT code = 200, BOOL close_session = FALSE, LPCSTR content_type = NULL);

	// Call before SendXXX
	void				AppendResponseHeader_ContentRange(const ContentRange& r);
	void				AppendResponseHeader_ETag(LPCSTR tag, int maxage_sec);
	void				AppendResponseHeader_Maxage(int maxage_sec);
	void				AppendResponseHeader_NoCache();
	void				AppendResponseHeader_ContentDisposition(LPCSTR filename);
	BOOL				AppendResponseHeader(LPCSTR head, UINT len);

	// Helper for sending response
	int					ComposeResponseHeader(LPSTR pHdrOut,__int64 content_len, UINT code, BOOL close_session, LPCSTR content_type);
																// content_len = 0			no content
																// content_len = -1			Transfer-Encoding: chunked
																// content_len = >0			Content-Length: len

	HttpRequest			Request;
	ULONGLONG			RequestCount;

	UINT				ResponseByteSent;	// Web Logfile
	UINT				ResponseCode;		// such as 200, 404 
};

class HttpServCore
{
public:
	static const int _default_httpsession_TTL = 60000; // msec
	static const int _postdata_size_max = 1024*1024*1024; // 1GB
	static const int _http_header_size_max = 4*1024; // 4KB
	static const int _http_response_sending_size = 1024;

protected:
	HttpServCore();
	BOOL	LoadHttpServConfig(LPCTSTR config_xml, rt::BufferEx<CInetAddr>& addr, UINT& thread_count, UINT& concurrent_session_max);

public:

#ifdef ENABLE_SERV_STATISTIC
public: // statistic
	volatile	LONG		stat_MaxRequestHandlingTime;	// approximated
	volatile	LONG		stat_TotRequestHandlingTime;
	volatile	LONG		stat_NumberOfRequest;
#endif // #ifdef ENABLE_SERV_STATISTIC

public:
	static __forceinline UINT HashHttpVerb(LPCVOID pRequestHead)
	{	const DWORD H = *((DWORD*)pRequestHead);
		static const UINT available_bits = 0x700E; // 0111 0000 0000 1110
		UINT hash = ((H+(H>>15))%17);
		return (available_bits&(1<<hash))?(hash-1):(REGQUEST_MAX + 1);
	}

	static UINT		ConvertToUTF8(LPSTR pInOut, UINT len); // return converted length

	static void		DefaultRequest_DumpSession(inet::HttpThreadLocalBlock* pTLB);
	static void		DefaultRequest_DumpRequest(inet::HttpThreadLocalBlock* pTLB);
	static void		DefaultRequest_DumpTCPCore(inet::HttpThreadLocalBlock* pTLB);
	static void		DefaultRequest_DumpOSInfo(inet::HttpThreadLocalBlock* pTLB, CPerformanceCounters* pPrefCounters = NULL);

	static void		DefaultRequest_Begin(inet::HttpThreadLocalBlock* pTLB, LPCSTR title = "Http Service Internal");
	static void		DefaultRequest_End(inet::HttpThreadLocalBlock* pTLB);

	static void		DefaultRequest_SectionBegin(inet::HttpThreadLocalBlock* pTLB, LPCSTR title);
	static void		DefaultRequest_SectionEnd(inet::HttpThreadLocalBlock* pTLB);

	static HRESULT	ParseDataReceived(inet::HttpThreadLocalBlock* pTLB, UINT newly_received_size);
	// returns
	// S_FALSE	:	more data is required
	// S_OK		:	request is ready for processing
	// E_xxx	:	error detected in the data, suggesting disconnection

};

} // namespace inet


template<class t_Derived = void, class t_TLB = inet::HttpThreadLocalBlock, class t_Conn = inet::HttpConnection>
class CHttpServ:public CSocketServ<typename _meta_::_reduce_target_type<t_Derived, CHttpServ<t_Derived,t_TLB,t_Conn> >::t_Result, t_TLB, t_Conn >,
				protected inet::HttpServCore
{
	ASSERT_STATIC(COMPILER_INTRINSICS_IS_BASE_OF(inet::HttpThreadLocalBlock,t_TLB));
	ASSERT_STATIC(COMPILER_INTRINSICS_IS_BASE_OF(inet::HttpConnection,t_Conn));
	typedef typename _meta_::_reduce_target_type<t_Derived,CHttpServ>::t_Result t_ServCore;
	
public:
	BOOL	Initialize(const CInetAddr* pAddr, UINT addr_count = 1, UINT thread_count = 0, UINT concurrent_session_max = 0)
	{
		if(__super::Initialize(pAddr, addr_count, thread_count, concurrent_session_max))
		{
			for(UINT i=0;i<GetThreadCount();i++)
				GetThreadLocalBlock(i).pHttpCore = this;
			return TRUE;
		}
		return FALSE;
	}
	BOOL	Initialize(LPCTSTR config_xml)
	{	rt::BufferEx<CInetAddr>	pAddr;
		UINT thread_count = 0; UINT concurrent_session_max = 0;
		return	LoadHttpServConfig(config_xml, pAddr, thread_count, concurrent_session_max) &&
				pAddr.GetSize() &&
				Initialize(pAddr,pAddr.GetSize(),thread_count,concurrent_session_max);
	}
	static HRESULT OnDataReceived(inet::ThreadLocalBlock* pTLB_in, UINT newly_received_size)	// can not be overrided
	{
		inet::HttpThreadLocalBlock* pTLB = (inet::HttpThreadLocalBlock*)pTLB_in;
		CHttpServ* pServ = (CHttpServ*)pTLB->pConn->m_pServ;
		ASSERT(pServ);

		HRESULT ret = inet::HttpServCore::ParseDataReceived((inet::HttpThreadLocalBlock*)pTLB,newly_received_size);
		if(SUCCEEDED(ret))
		{
			if(ret == S_OK)
			{	
				pTLB->RequestCount++;
				pTLB->AdditionalResponseHeaderLen = 0;
				pTLB->AdditionalResponseHeader[0] = '\0';
				pTLB->ResponseCode = 0;
				pTLB->ResponseByteSent = 0;

				ret = ((t_ServCore*)pServ)->OnRequestReceived((t_TLB*)pTLB);

				if(SUCCEEDED(ret))
					return (pTLB->Request.IsKeepAlive() || pTLB->Request.IsContentRanged())?S_OK:E_FAIL;
				else
					return ret;
			}
			return S_OK;
		}
		return ret;
	}

	ULONGLONG	GetTotalRequestCount() const
	{	ULONGLONG ret = 0;
		for(UINT i=0;i<GetThreadCount();i++)
			ret += GetThreadLocalBlock(i).RequestCount;
		return ret;
	}

	void	DefaultRequest_DumpHTTPCore(inet::HttpThreadLocalBlock* pTLB)
	{
		DefaultRequest_SectionBegin(pTLB,"HTTP Core Information");
		char peerinfo[1024];	int len = 0;
		
#ifdef ENABLE_SERV_STATISTIC			 
		len = sprintf(peerinfo,"Request Process Time:  %d/%d msec   (Avg/Peak)\r\n",
						stat_TotRequestHandlingTime/max(1,stat_NumberOfRequest),
						stat_MaxRequestHandlingTime
					 );
		pTLB->SendChunkResponse(peerinfo,len);
#endif
		len  =sprintf(peerinfo,"Incoming Requests:     %lld on %d threads\r\n",GetTotalRequestCount(),GetThreadCount());
		pTLB->SendChunkResponse(peerinfo,len);
		
		for(UINT i=0;i<GetThreadCount();i+=2)
		{
			len = sprintf(peerinfo,"                       Thread Balance (%d) |%10lld |%10lld |\r\n",
									i/2,
									GetThreadLocalBlock(i).RequestCount,
									GetThreadLocalBlock(i+1).RequestCount);
			pTLB->SendChunkResponse(peerinfo,len);
		}

		DefaultRequest_SectionEnd(pTLB);
	}

protected:
	__forceinline
	HRESULT OnRequestReceived(t_TLB* pTLB)
	{
		return ((t_ServCore*)this)->OnRequest(pTLB);
	}

	HRESULT OnRequest(t_TLB* pTLB)
	{
		if(pTLB->Request.Method == inet::REGQUEST_GET)
		{
			DefaultRequest_Begin(pTLB);
				DefaultRequest_DumpRequest(pTLB);
				((t_ServCore*)this)->DefaultRequest_DumpHTTPCore(pTLB);
				DefaultRequest_DumpTCPCore(pTLB);
				DefaultRequest_DumpSession(pTLB);
				DefaultRequest_DumpOSInfo(pTLB);
			DefaultRequest_End(pTLB);
			return S_OK;
		}

		return E_ABORT;
	}
};

} // namespace w32
