#pragma once

//////////////////////////////////////////////////////////////////////
// Visual Computing Foundation Classes (VCFC)
//					Jiaping Wang  2006.2
//					e_boris2002@hotmail.com
//
// (C) Copyright Jiaping Wang 2004.
//
// Permission to copy, use, modify, sell and distribute this software
// is granted provided this copyright notice appears in all copies.
// This software is provided "as is" without express or implied
// warranty, and with no claim as to its suitability for any purpose.
//
// Absolutely no warranty!!
//////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////
//  Brief Description
//
//  wnd_app.h
//
//  windows class moved from wglh\wglhelper.h
//  ui_event:   event waitinng without blocking message pipeline
//////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////
// Revise History
//
// Inital version	2006.11.9		Jiaping
//
//////////////////////////////////////////////////////////////////////

#include "win32_ver.h"
#include "w32_basic.h"

namespace w32
{

namespace _meta_{ class _WndBase_Class; }

void		SetResourceModule(HANDLE DLL);
HINSTANCE	GetResourceModule();

class CWndBase
{	friend class _meta_::_WndBase_Class;
	friend void SetResourceModule(HANDLE);
	friend HINSTANCE GetResourceModule();
private:

#ifdef __AFXWIN_H__
	CWnd*	_pMfcWnd;
#else
	LPCVOID	_Reserved;
#endif

	static HMODULE	 _hModuleHandle;
	static CWndBase* _pMainWnd;
	static CWndBase* _pAutoDeleteWndList;
	static w32::CCriticalSection _AutoDeleteWndList_CCS;

public:
	enum
	{	ALIGN_X = 1,
		ALIGN_Y = 2,
		ALIGN_WIDTH = 4,
		ALIGN_HEIGHT = 8,
	};

	CWndBase();
	virtual ~CWndBase();
	void CenterWindow();

	BOOL Create(DWORD WinStyle = WS_OVERLAPPEDWINDOW,HWND Parent = NULL);
	BOOL Create_LayeredWindow(INT x, INT y, INT w, INT h, HWND Parent = NULL);
	BOOL Create_Dialog(LPCTSTR lpDlgTemplate, HWND Parent = NULL);
	BOOL Create_ToolWnd(HWND Parent = NULL);
	void EnableDeleteThisOnDestroy(); //call after creation
	void EnableWindowDragFromClientRegion(BOOL yes = TRUE);
	void DestroyWindow();
	HWND GetSafeHwnd(){ return hWnd; }
	operator HWND(){ return GetSafeHwnd(); }
	operator HDC(){ return GetDC(); }
#ifdef __AFXWIN_H__
	operator CWnd*(){ return _pMfcWnd; }
#endif
	BOOL	Attach(HWND hwnd);
	HWND	Detach();

	int		MessageBox(LPCTSTR lpText,LPCTSTR lpCaption = _T("Information"),UINT uType = MB_OK);
	DWORD	GetStyle(){ return ::GetWindowLong(*this,GWL_STYLE); } 
	DWORD   GetStyleEx(){ return ::GetWindowLong(*this,GWL_EXSTYLE); } 
	void	SetStyle(DWORD style){ ::SetWindowLong(*this,GWL_STYLE,style); }
	void	SetStyleEx(DWORD style){ ::SetWindowLong(*this,GWL_EXSTYLE,style); }
	void	SetTopMost(BOOL is = TRUE){ ::SetWindowPos(*this,is?HWND_TOPMOST:HWND_NOTOPMOST,0,0,0,0,SWP_NOMOVE|SWP_NOSIZE); }
	void	ResizeWindowByClientArea(int Width,int Height);
	void	SwitchFullScreen();
	BOOL	IsInFullScreen(){ return _IsInFallScreen; }
    BOOL    IsChildWindow();
	void	AlignTo(HWND Reference,DWORD AlignFlag = CWndBase::ALIGN_X|CWndBase::ALIGN_Y|CWndBase::ALIGN_WIDTH|CWndBase::ALIGN_HEIGHT);

	void	MainMenu_SetCheck(UINT Id,BOOL Check);
	void	MainMenu_Enable(UINT Id,BOOL Enable);
	void	MainMenu_SetText(UINT Id,LPCTSTR);
	void	SetMenu(HMENU hMenu){ ::SetMenu(*this,hMenu); }
	void	SetIcon(HICON hIcon, BOOL bBigIcon){ SendMessage(WM_SETICON, bBigIcon, (LPARAM)hIcon); }
	void	SetMenu(LPCTSTR ResId);
	void	SetIcon(LPCTSTR ResId, BOOL bBigIcon);
	void	SetMenu(UINT ResId){ SetMenu(MAKEINTRESOURCE(ResId)); }
	void	SetIcon(UINT ResId, BOOL bBigIcon){ SetIcon(MAKEINTRESOURCE(ResId),bBigIcon); }
	// WIN32 API wrapper
	HWND	GetDlgItem(int nCtrlID){ return ::GetDlgItem(*this,nCtrlID); }
	BOOL	GetClientRect(LPRECT lpRect){ return ::GetClientRect(*this,lpRect); }
	BOOL	GetWindowRect(LPRECT lpRect){ return ::GetWindowRect(*this,lpRect); }
	BOOL	ScreenToClient(LPPOINT lpPoint){ return ::ScreenToClient(*this,lpPoint); }
	BOOL	ClientToScreen(LPPOINT lpPoint){ return ::ClientToScreen(*this,lpPoint); }
	BOOL	EnableWindow(BOOL bEnable = TRUE){ return ::EnableWindow(*this,bEnable); }
	BOOL	SetWindowText(LPCTSTR str){ return ::SetWindowText(*this,str); }
	BOOL	GetWindowText(LPTSTR str, int count){ return ::GetWindowText(*this,str,count); }
#ifdef UNICODE
	BOOL	SetWindowTextA(LPCSTR str){ return ::SetWindowTextA(*this,str); }
	BOOL	GetWindowTextA(LPSTR str, int count){ return ::GetWindowTextA(*this,str,count); }
#else
	BOOL	SetWindowTextW(LPCWSTR str){ return ::SetWindowTextW(*this,str); }
	BOOL	GetWindowTextW(LPWSTR str, int count){ return ::GetWindowTextW(*this,str,count); }
#endif
	BOOL	ShowWindow(int nCmdShow=SW_SHOW){ return ::ShowWindow(*this,nCmdShow); }
	BOOL	GetWindowPlacement(WINDOWPLACEMENT* plc){ return ::GetWindowPlacement(*this,plc); }
	UINT	GetShowWindowState(){ WINDOWPLACEMENT plc; return GetWindowPlacement(&plc)?plc.showCmd:INFINITE; }
	HWND	SetFoucs(){ return ::SetFocus(*this); }
	HWND	SetCapture(){ return ::SetCapture(*this); }
	BOOL	ReleaseCapture(){ return ::ReleaseCapture(); }
	LRESULT SendMessage(UINT Msg, WPARAM wParam, LPARAM lParam){ return ::SendMessage(hWnd,Msg,wParam,lParam); }  
	BOOL	SetWindowPos(HWND hWndAfterZ, int x, int y, int cx, int cy, UINT nFlags){ return ::SetWindowPos(hWnd,hWndAfterZ,x,y,cx,cy,nFlags); }

	void	EnableResizing(BOOL is = FALSE);
	void	HostChildWindow(HWND hChild = NULL);
	void	BringWindowToTop(){ ShowWindow(); ::BringWindowToTop(*this); }
	void	SetAsMainWnd(){ _pMainWnd = this; }
	HDC		GetDC(){ if(hDC){}else{hDC = ::GetDC(hWnd);} return hDC; }
	void	ReleaseDC(){ if(hDC){ ::ReleaseDC(hWnd,hDC); hDC = NULL; } }

protected:
	HDC		hDC;   // obtained by first call of GetDC();
	HWND	hWnd;
	HWND	Hosted_Child;
	BOOL	SetupAfterCreation();
	virtual LRESULT WndProc(UINT message, WPARAM wParam, LPARAM lParam);
	virtual BOOL	OnInitWnd(){ return TRUE; };

private:
	BOOL		_bAsDialog;
	BOOL		_bMoveWindowByClientRegion;
	BOOL		_bAutoDeleteThis;
	CWndBase*	_Next_AutoDelWnd;

	BOOL		_IsInFallScreen; // following available only when _IsInFallScreen==TRUE
	RECT		_OrgLayout;
	DWORD		_OrgStyle;
	DWORD		_OrgStyleEx;
	HWND		_OrgParentWnd;
};


class CLayeredWnd:public ::w32::CWndBase
{
	void	HostChildWindow(HWND hChild = NULL){ ASSERT(0); }

	WPARAM			_LastWM_SIZEWPARAM;
	HDC				_hMemoryDC;
	HGDIOBJ			_hOrgBmpOnMemoryDC;
	HBITMAP			_CreateMemoryDC_BMP();
	void			_UpdateRenderLayerSize();

	w32::CWndBase	_wRenderLayer;

protected:
	POINT			m_Position;
	SIZE			m_Size;

	// Rendering buffer
	int				m_Alpha;		// 0-255
	POINT			m_RLExt_Position;
	SIZE			m_RLExt_Padding;
	POINT			m_RLPosition;
	SIZE			m_RLSize;
	
protected:
	void	Refresh();
	virtual void OnRender(HDC hMemoryDC) = 0; // Better to use Gdiplus::Graphics to take hMemoryDC
	LRESULT WndProc(UINT message, WPARAM wParam, LPARAM lParam);
	BOOL	_CreateBaseWnd(UINT Style, HWND Parent = NULL);
	BOOL	_CreateRenderLayer(HWND parent);

public:
	CLayeredWnd();

	void	SetRenderLayerExtension(int left, int top, int right, int bottom);
	BOOL	Create(UINT Style, HWND Parent = NULL);
	void	SetOpacity(int alpha = 255);
	BOOL	ShowWindow(int nCmdShow=SW_SHOW);
};


/////////////////////////////////////////////////////////
// UI Event
class CUIEvent
{
protected:
	static	DWORD	g_UI_Thread;
public:
	static const int ANY_ID = UINT_MAX;

	static void	Fire(int id,UINT cookie=0,BOOL ThreadId=UINT_MAX);

	static UINT	Awaiting(UINT id,UINT* pCookie=NULL,int TimeLimit_ms = INFINITE,BOOL IgnoreGlobalThreadId = FALSE); //return waited event id
	static BOOL PumpMessageOnce();	//return TRUE if some message is processed, No additional handling for WM_QUIT

#ifdef __AFXWIN_H__
	static UINT	Awaiting_MFC(UINT id,UINT* pCookie=NULL,int TimeLimit_ms = INFINITE,BOOL IgnoreGlobalThreadId = FALSE); //return waited event id
	static BOOL PumpMessageOnce_MFC();	////return TRUE if some message is processed, No additional handling for WM_QUIT
#endif  // __AFXWIN_H__

};

BOOL WaitForEnteringCriticalSection_UI(w32::CCriticalSection& ccs, UINT timeout = INFINITE);  // return FALSE when WM_QUIT received or timeout
BOOL WaitForThreadEnding_UI(HANDLE hThread, UINT time_wait_ms = INFINITE);

} // namespace w32

namespace w32
{ 
namespace _meta_
{
	class _IdleKick_MFC
	{	UINT	IdleCount;
	public:
#ifdef __AFXWIN_H__
		__forceinline _IdleKick_MFC(){ IdleCount = 0; }
		__forceinline void OnMessage(){ IdleCount = 0; }
		void OnIdle();
#else
		__forceinline _IdleKick_MFC(){}
		__forceinline void OnMessage(){}
		void OnIdle();
#endif // __AFXWIN_H__
	};
}} // namespace _meta_/w32

