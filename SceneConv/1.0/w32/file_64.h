#pragma once

//////////////////////////////////////////////////////////////////////
// Visual Computing Foundation Classes (VCFC)
//					Jiaping Wang  2006.2
//					e_boris2002@hotmail.com
//
// (C) Copyright Jiaping Wang 2004.
//
// Permission to copy, use, modify, sell and distribute this software
// is granted provided this copyright notice appears in all copies.
// This software is provided "as is" without express or implied
// warranty, and with no claim as to its suitability for any purpose.
//
// Absolutely no warranty!!
//////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////
//  Brief Description
//
//  file_64.h
//
//  64bit File IO and text file IO
//  Common object presistence
//  File/Folder change detection
//  File Mapping
//
//////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////
// Revise History
//
// Inital version	2006.6.8		Jiaping
// V0.2				2006.10.27		Jiaping
//					Add File Mapping
// V0.21			2006.12.17		Jiaping				
//					Redirection of stdio to CFile64_Text
// V0.22            2008.11.16      Yiming
//                  Add Flush() Method for CFile64
//
//////////////////////////////////////////////////////////////////////

#include "win32_ver.h"
#include "w32_basic.h"
#include "..\rt\string_type.h"
#include "..\rt\compact_vector.h"
#include <stdio.h>

namespace w32
{

class CFile64:public rt::_stream
{
protected:
	HANDLE		hFile;
	BOOL		bErrorFlag;
	rt::String	Filename;

public:
	enum OP_FLAG
	{
		Access_Read			= 0x01,
		Access_Write		= 0x02,
		Access_GetAttribute	= 0x04,
		Access_SetAttribute	= 0x08,
		Access_All			= 0x0f,

		Share_Read			= 0x10,
		Share_Write			= 0x20,
		Share_All			= 0x30,

		Mode_OpenNew		= 0x100,
		Mode_OpenExist		= 0x200,
		Mode_OpenAny		= 0x300,
		Mode_CreatePath		= 0x400,

		Flag_DeleteOnClose	= 0x1000,
		Flag_WriteThrough	= 0x2000,
		Flag_SequentialScan	= 0x4000,  
		Flag_Truncate		= 0x8000,

		Seek_Begin			= FILE_BEGIN,
		Seek_Current		= FILE_CURRENT,
		Seek_End			= FILE_END,

		Normal_Read			= Access_Read|Share_All|Mode_OpenExist,
		Normal_Write		= Access_Write|Mode_OpenAny|Flag_Truncate,
		Normal_ReadWrite	= Access_Read|Access_Write|Mode_OpenAny,
		Normal_Append		= Access_Write|Mode_OpenAny,
		Normal_SetAttribute	= Access_SetAttribute|Share_All|Mode_OpenExist,
		Normal_GetAttribute	= Access_GetAttribute|Share_All|Mode_OpenExist,
	};
	 
public:
	CFile64();
	CFile64(LPCTSTR fn, UINT OpenFlag = Normal_Read);
	virtual ~CFile64();
	operator	HANDLE(){ return hFile; }
	BOOL		IsOpen() const { return hFile != INVALID_HANDLE_VALUE; }

	template<typename t_Char>
	UINT				Write(const rt::StringT_Ref<t_Char>& x){ return Write(x.Begin(),x.GetLength()*sizeof(t_Char)); }
	UINT				Write(CHAR x){ return Write(&x,1); }
	UINT				Write(WCHAR x){ return Write(&x,2); }
	UINT				Write(const rt::StringA_Ref& x){ return Write(x.Begin(),x.GetLength()); }
	UINT				Write(const rt::StringW_Ref& x){ return Write(x.Begin(),x.GetLength()*sizeof(WCHAR)); }

	virtual BOOL		IsEOF();
	virtual BOOL		Open(LPCTSTR fn,UINT OpenFlag = Normal_Read);
	virtual UINT		Read(LPVOID lpBuf,UINT nCount);
	virtual UINT		Write(LPCVOID lpBuf,UINT nCount);
	virtual void		Close();
	virtual LONGLONG	GetLength() const;
	virtual LONGLONG	GetCurrentPosition();
	virtual LONGLONG	Seek(LONGLONG offset,UINT nFrom = Seek_Begin);
	virtual void		SeekToBegin();
	virtual void		SeekToEnd();
	virtual BOOL		SetLength(ULONGLONG sz);

	// NOTE: Flush method is useful only when several processes
	// need to access a file simutaneously. If a single process 
	// uses CFile64, even if it crashes before flushing the file,
	// the remaining in the buffer will be flushed into the file
	// by windows operating system (But it is true only for 
	// CFile64, NOT for FILE* or fstream, because their buffers
	// are maintained by C Runtime Library but not the operating
	// system API).
	virtual void        Flush();

	rt::String_Ref GetFilename() const { return Filename.GetRef(); }
	void	ClearError(){ bErrorFlag = FALSE; }
	BOOL	ErrorOccured(){ return bErrorFlag; }


	void	SetTime_Creation(const FILETIME* ft);
	void	SetTime_LastAccess(const FILETIME* ft);
	void	SetTime_LastModify(const FILETIME* ft);
	void	GetTime_Creation(FILETIME* ft);
	void	GetTime_LastAccess(FILETIME* ft);
	void	GetTime_LastModify(FILETIME* ft);

	static  BOOL CreateDirectories(LPCTSTR path);	// create intermediate subdirectories if necessary, 
													// e.g. a/b/c/d/e, the folder a/b/c/d will be created, e is regard as the filename
	static	BOOL Remove(LPCTSTR fn){ return ::DeleteFile(fn); }
	static	BOOL IsExist(LPCTSTR fn){ return GetAttributes(fn)!=INVALID_FILE_ATTRIBUTES; }
	static	BOOL Rename(LPCTSTR fn,LPCTSTR new_fn){ return ::MoveFile(fn,new_fn); }
	static	BOOL Copy(LPCTSTR fn,LPCTSTR new_fn,BOOL no_overwrite = FALSE){ return ::CopyFile(fn,new_fn,no_overwrite); }
	static	BOOL Move(LPCTSTR fn,LPCTSTR new_fn,BOOL no_overwrite = FALSE){ return ::MoveFileEx(fn,new_fn,MOVEFILE_COPY_ALLOWED|(no_overwrite?0:MOVEFILE_REPLACE_EXISTING)); }
	static	DWORD GetAttributes(LPCTSTR fn){ return ::GetFileAttributes(fn); }
	static  BOOL IsDirectory(LPCTSTR fn)
	{	DWORD Attrib = GetAttributes(fn);
		return (INVALID_FILE_ATTRIBUTES!=Attrib) && (Attrib&FILE_ATTRIBUTE_DIRECTORY);
	}
	static	BOOL SetAttributes(LPCTSTR fn,DWORD attrib){ return ::SetFileAttributes(fn,attrib); }

	static  BOOL SetPathTime(LPCTSTR pathname, const FILETIME* creation,const FILETIME* last_access,const FILETIME* last_modify);	// handles file and directory, feed NULL if not interested
	static  BOOL GetPathTime(LPCTSTR pathname, FILETIME* creation,FILETIME* last_access,FILETIME* last_modify);	// handles file and directory, feed NULL if not interested

//#if (_WIN32_WINNT >= 0x0500)
//	static  BOOL CreateHardLink(LPCTSTR fn,LPCTSTR new_fn){	return ::CreateHardLink(new_fn,fn,NULL); }
//#endif
	static HRESULT HresultFromLastError(){ return w32::HresultFromLastError(); }
};

class CFile64_Text:public CFile64
{
	FILE	_Stdfile_org;
	int		_os_file_handle;
protected:
	FILE*	pStream;
	UINT	_Redirected; //0: none, 1:stdout, 2:stdin
public:
	BOOL		Open(LPCTSTR fn,UINT OpenFlag = Normal_Read);
	void		Close();
	LONGLONG	Seek(LONGLONG offset,UINT nFrom = Seek_Begin);
	void		SeekToBegin(){ Seek(0); }
	void		SeekToEnd(){ Seek(0,Seek_End); }

	CFile64_Text(){ pStream = NULL; _Redirected=0; }
	~CFile64_Text(){ StopRedirect(); Close(); }
	operator FILE* (){ return pStream; }

	BOOL		WriteString(LPCSTR str);
	BOOL		WriteString(LPCWSTR str);
	BOOL		WriteString(const rt::StringA_Ref& x);
	BOOL		WriteString(const rt::StringW_Ref& x);

	void		Redirect(BOOL as_stdout = TRUE);  // otherwise, as stdin
	void		StopRedirect();
};


class CFileVolume: public CFile64	// For Disks, USB and CD/DVD drivers
{
	UINT	_open_flag;
public:
	enum _VolType
	{
		Volume_Unknown = 0,
		Volume_NonRoot,
		Volume_Removable,
		Volume_Fixed,
		Volume_Remote,
		Volume_CDROM,
		Volume_RamDisk
	};
public:
	virtual BOOL		Open(LPCTSTR volume_name,UINT OpenFlag = Normal_Read | Share_All | Flag_SequentialScan);

	DWORD		GetMediaType();	// return one of _VolType
	
	BOOL		EjectMedia(); // for removable media & CD/DVD
	BOOL		LoadMedia();
	ULONGLONG	GetMediaLength();
	INT			ProbeMinimumSeekBlock();
};


namespace _meta_
{
// _BaseVecFileMapping
template<typename t_Val>
class _BaseVecFileMapping
{	
private:
	HANDLE	_hFileMapping;
public:
	static const bool IsElementNotAggregate = !rt::TypeTraits<t_Val>::IsAggregate ;
	static const bool IsRef = false;
	typedef SIZE_T t_SizeType;
	ASSERT_STATIC(!IsElementNotAggregate);  // Support POD element only

	DYNTYPE_FUNC _BaseVecFileMapping(){ _hFileMapping=NULL; _p=NULL; _len=0; }
	DYNTYPE_FUNC ~_BaseVecFileMapping(){ SetSize(); }
	DYNTYPE_FUNC explicit _BaseVecFileMapping(const _BaseVecFileMapping &x){ ASSERT_STATIC(0); }	//copy ctor should be avoided, use reference for function parameters
	DYNTYPE_FUNC BOOL SetSize(LPCTSTR filename=NULL, DWORD mode=w32::CFile64::Normal_Read, SIZE_T element_count=0) //zero for clear
	{	
		if(filename == NULL)
		{	// destory object
			if(_p){ VERIFY(UnmapViewOfFile(_p)); _p=NULL; }
			if(_hFileMapping)
			{	::CloseHandle(_hFileMapping);
				_hFileMapping = NULL;
			}
			return TRUE;
		}
		else
		{
			if(_hFileMapping)SetSize();

			SIZE_T fsize = sizeof(t_Val)*element_count;
			DWORD mapvof = 0;
			if(mode&w32::CFile64::Access_Read)mapvof |= FILE_MAP_READ;
			if(mode&w32::CFile64::Access_Write)mapvof |= FILE_MAP_WRITE;

			ASSERT(_hFileMapping == NULL);
			w32::CFile64	file;
			if(	file.Open(filename,mode|w32::CFile64::Access_Read) &&
				(fsize==0 || file.SetLength(fsize)) &&
#ifdef _WIN64
				(_hFileMapping = 
					::CreateFileMapping(file,NULL,
										(mode&w32::CFile64::Access_Write)?PAGE_READWRITE:PAGE_READONLY,
										(DWORD)(fsize>>32),(DWORD)fsize,NULL)
				) &&
#else
				(_hFileMapping = 
					::CreateFileMapping(file,NULL,
										(mode&w32::CFile64::Access_Write)?PAGE_READWRITE:PAGE_READONLY,
										0,(DWORD)fsize,NULL)
				) &&
#endif
				(_p = (t_Val*)MapViewOfFile(_hFileMapping,mapvof,0,0,fsize))
			)
			{	_len = (SIZE_T)file.GetLength()/sizeof(t_Val);
				return TRUE;
			}
			else return FALSE;
		}
	}
	DYNTYPE_FUNC SIZE_T GetSize() const{ return _len; }

	//TypeTraits
	TYPETRAITS_DECL_LENGTH(TYPETRAITS_SIZE_UNKNOWN)
	TYPETRAITS_DECL_IS_AGGREGATE(false)

	template<typename T>
	DYNTYPE_FUNC t_Val & At(const T index){ return _p[(int)index]; }
	template<typename T>
	DYNTYPE_FUNC const t_Val & At(const T index)const { return _p[(int)index]; }
	static const bool IsCompactLinear = true;
protected:
	t_Val*	_p;
	SIZE_T	_len;
};

} // namespace _meta_
class _BaseVecFileMap
{public:	template<typename t_Ele> class SpecifyItemType
			{public: typedef w32::_meta_::_BaseVecFileMapping<t_Ele> t_BaseVec; };
			static const bool IsCompactLinear = true;
};


template<typename t_Val>
class CFileBuffer:public rt::Buffer<t_Val,_BaseVecFileMap>
{
	DYNTYPE_FUNC SIZE_T SetSize(UINT sz);
	template<class T> 
	DYNTYPE_FUNC BOOL SetSizeAs( const T & x );

public:
	CFileBuffer(){}
	DYNTYPE_FUNC BOOL SetSizeAs(LPCTSTR filename=NULL, DWORD mode=w32::CFile64::Normal_Read, SIZE_T element_count=0) //zero for clear
	{
		return w32::_meta_::_BaseVecFileMapping<t_Val>::SetSize(filename,mode,element_count);
	}
	DYNTYPE_FUNC SIZE_T GetSize() const
	{
		return w32::_meta_::_BaseVecFileMapping<t_Val>::GetSize();
	}
	rt::StringT_Ref<t_Val>	GetStringRef(){ return rt::StringT_Ref<t_Val>(Begin(),GetSize()); }
#pragma warning(disable:4244)
	ASSIGN_OPERATOR_VEC(CFileBuffer)
#pragma warning(default:4244)
	DEFAULT_TYPENAME(CFileBuffer)
	TYPETRAITS_DECL_EXTENDTYPEID((rt::_typeid_codelib<<16)+11)
};


class CParallelFileWriter
{
	rt::StringA			__string_acp;
	rt::StringW			__string_wchar;

protected:
	rt::String			_SwitchNewFileNameBuffer;
	volatile LPCTSTR	_SwitchNewFileName;
	DWORD				_SwitchNewFileOpenFlag;
	rt::Buffer<BYTE>	_WritePool;
	w32::CFile64		_File;
	double				_AvgAttenuation;
	LONGLONG			_DesiredFileLength;			// preivous data will be truncated
	DWORD				_TruncateBoundaryTag;
	DWORD				_TruncateBoundaryTagSize;	// 0 - 4 byte
	DWORD				_TruncateBoundaryTagMask;
	rt::Buffer<BYTE>	_TruncateBuffer;
	UINT				_SeekTruncateBoundary(LPCBYTE p, UINT len);

	UINT				_InMemoryCopyBufferCodepage;
	rt::Buffer<BYTE>	_InMemoryCopyBuffer;
	void				_WriteInMemoryCopyBuffer(LPBYTE p, UINT len);

protected:
	struct _Chunk
	{	UINT	size;		
		UINT	length;		// INFINITE indicates the chunk is not finalized
		BYTE	data[1];	// real size set is by SetLogEntrySize
		_Chunk*	GetNext(){ return (_Chunk*)(data + size); }
	};
	static const int _ChunkHeaderSize = (int)&(((_Chunk*)0)->data);
	struct _WriteBuf
	{	LPBYTE			pBuf;
		volatile LONG	Used;
	};
	_WriteBuf			_WriteBuffers[2];

protected:
	_WriteBuf			_InMemoryCopy[2];
	_WriteBuf*			_InMemoryCopy_FrontBuf;
	_WriteBuf*			_InMemoryCopy_BackBuf;

protected:
	_WriteBuf*			m_FrontBuf;
	_WriteBuf*			m_BackBuf;
	UINT				m_WriteDownInterval;
	UINT				m_WriteBufferSize;
	HANDLE				m_hSyncThread;

	void				_WriteDownBackBuffer();
	void				_SyncThread();

public: // statistic
	LONG				stat_FileError;
	LONG				stat_UnfinalizedChunk;
	LONG				stat_BufferUsageAvg;			// precentage
	LONG				stat_BufferUsagePeek;			// precentage
	LONG				stat_FileIOUsageAvg;			// precentage
	LONG				stat_FileIOUsagePeek;			// precentage
	volatile LONG		stat_TotalClaimed;
	volatile LONG		stat_ClaimFailure;
	volatile LONG		stat_FinalizeFailure;

public:
	CParallelFileWriter();
	~CParallelFileWriter();

	BOOL	Open(LPCTSTR filename, UINT buffer_size = 1024*1024, UINT writedown_interval = 1000, LONGLONG desired_length = 0,
				 DWORD flag = CFile64::Access_Write|CFile64::Share_Read|CFile64::Mode_OpenAny); // it is by default to append to any existing file
	void	EnableInMemoryCopy(UINT memory_copy_size_max = 8*1024, UINT target_codepage = CP_ACP);	// in byte, zero to disable
	LPCBYTE	GetLatestInMemoryCopy(UINT& len);		// returned buffer will dead after GetWriteDownInterval() msec
	void	Close();
	BOOL	IsOpen() const { return _File.IsOpen(); }
	void	SwitchTo(LPCTSTR filename, BOOL no_wait = TRUE);
	UINT	GetWriteDownInterval() const { return m_WriteDownInterval; }
	void	SetWriteDownInterval(UINT write_interval_msec);
	UINT	GetBufferSize() const { return m_WriteBufferSize; }
	void	SetTruncationBoundaryTag(DWORD tag, int tag_size);
	rt::String_Ref GetLogFilename() const { return _File.GetFilename(); }

	__forceinline LPBYTE ClaimWriting(UINT size)
	{	ASSERT(size);
		_WriteBuf* buf = m_FrontBuf;
		LONG chunk = _InterlockedExchangeAdd(&buf->Used,size + _ChunkHeaderSize);
		_InterlockedIncrement(&stat_TotalClaimed);
		if(chunk + (int)size + _ChunkHeaderSize <= (LONG)m_WriteBufferSize)
		{
			_Chunk* p = (_Chunk*)&buf->pBuf[chunk];
			p->size = size;
			p->length = INFINITE;
			return p->data;
		}
		else
		{	// this may happen when the writer is much faster then the sync thread
			_InterlockedIncrement(&stat_ClaimFailure);
			return NULL;
		}
	}

	__forceinline void FinalizeWritten(LPBYTE pBuf, UINT len)
	{	_Chunk* p = (_Chunk*)&pBuf[-((int)_ChunkHeaderSize)];

		if(	len <= p->size &&
			p->length == INFINITE && p->size < m_WriteBufferSize)
		{	p->length = len;	}
		else 
			_InterlockedIncrement(&stat_FinalizeFailure);
			// This may happen when it takes too long time between corresponding 
			// ClaimWriting/FinalizeWritten calling. e.g. longer thaun m_WriteDownInterval
			// other possiblity is the caller ruined the buffer (run down)  
	}

	template<class T>
	__forceinline BOOL WriteString(const T& x)
	{	typedef typename T::t_Char t_Val;
		SIZE_T len = x.GetLength();
		LPBYTE p = ClaimWriting(len*sizeof(t_Val));
		if(p)
		{	VERIFY(len == x.CopyTo((t_Val*)p));
			FinalizeWritten(p,len*sizeof(t_Val));
			return TRUE;
		}else return FALSE;
	}

	template<typename CH>
	__forceinline BOOL WriteString(const CH* str){ return WriteString(str,rt::_meta_::xxxlen(str)); }
	template<typename CH>
	__forceinline BOOL WriteString(CH* str){ return WriteString(str,rt::_meta_::xxxlen(str)); }

	template<typename CH>
	__forceinline BOOL WriteString(const CH* str, SIZE_T len)
	{	
		LPBYTE p = ClaimWriting(len*sizeof(CH));
		if(p)
		{	memcpy(p,str,len*sizeof(CH));
			FinalizeWritten(p,len*sizeof(CH));
			return TRUE;
		}else return FALSE;
	}
};


///////////////////////////////////////
// CFolderChangingMonitor
class CFolderChangingMonitor
{
private:
	static DWORD WINAPI _WorkingThreadFunc(LPVOID p);
protected:
	virtual void OnFolderChanged();
	HANDLE	m_WaitingHandle;
	HANDLE	m_WorkingThread;
	UINT	m_CommandId;
	HWND	m_NotificationWnd;
public:
	CFolderChangingMonitor();
	~CFolderChangingMonitor(){ Destroy(); }
	BOOL Create(LPCTSTR FolderName,BOOL IncludeSubTree = FALSE, DWORD Filter =	FILE_NOTIFY_CHANGE_LAST_WRITE );
	void Destroy();
	void SetNotificatonWindow(HWND hwnd, UINT CommandId);
	BOOL IsStarted() { return m_WorkingThread!=NULL; }
};

///////////////////////////////////////
// CFileChangingMonitor add by Kun Xu at 10.9 2005
class CFileChangingMonitor : protected CFolderChangingMonitor
{
protected:
	TCHAR		_fullName[MAX_PATH];
	FILETIME	_lastModifiedTime;

public:
	void SetNotificatonWindow(HWND hwnd, UINT CommandId){ __super::SetNotificatonWindow(hwnd,CommandId); }
	BOOL IsStarted() { return __super::IsStarted(); }
	BOOL StartMonitor(LPCTSTR fn);
	void StopMonitor(){ __super::Destroy(); }
protected:
	virtual void OnFileChange(){ __super::OnFolderChanged(); }
	void OnFolderChanged();
	void GetLastModifiedTime(FILETIME * pTime);
};

template<class T>
HRESULT SaveObject(const T& x, LPCTSTR fn)
{	HRESULT hr = E_FAIL;
	__if_exists(T::MEMBER_ALLOW_PERSISTENT)
	{	w32::CFile64_Composition file;
		if(file.Open(fn,w32::CFile64::Normal_Write))
		{ return x.Save(file); }
		else{ return w32::HresultFromLastError(); }
	}
	__if_not_exists(T::MEMBER_ALLOW_PERSISTENT)
	{	w32::CFile64 file;
		if(file.Open(fn,w32::CFile64::Normal_Write))
		{
			__if_exists(T::Save){ return x.Save(file); }
			__if_not_exists(T::Save)
			{	
				ASSERT_STATIC(::rt::TypeTraits<T>::IsAggregate);
				if(file.Write(&x,sizeof(T))!=sizeof(T))return w32::HresultFromLastError();
				return S_OK;
			}
		}
		else{ return w32::HresultFromLastError(); }
	}
}

template<class T>
HRESULT LoadObject(T& x, LPCTSTR fn)
{	HRESULT hr = E_FAIL;
	__if_exists(T::MEMBER_ALLOW_PERSISTENT)
	{	w32::CFile64_Composition file;
		if(file.Open(fn,w32::CFile64::Normal_Read))
		{ return x.Load(file); }
		else{ return w32::HresultFromLastError(); }
	}
	__if_not_exists(T::MEMBER_ALLOW_PERSISTENT)
	{	w32::CFile64 file;
		if(file.Open(fn,w32::CFile64::Normal_Read))
		{
			__if_exists(T::Load){ return x.Load(file); }
			__if_not_exists(T::Load)
			{	
				ASSERT_STATIC(::rt::TypeTraits<T>::IsAggregate);
				if(file.Read(&x,sizeof(T))!=sizeof(T))return w32::HresultFromLastError();
				return S_OK;
			}
		}
		else{ return w32::HresultFromLastError(); }
	}
}

// CRC32 method used in WinZip and Ethernet
// class compatible with ipp::OneWayHash
class CRC32
{
    ULONG	_Reflect(ULONG ref, char ch);
    static  ULONG _Table[256];
protected:
	DWORD	CRC;
public:
	static const UINT HASH_CODE_SIZE = 4;

	CRC32(DWORD init = 0);
	void	Reset(DWORD init = 0);
	void	Update(LPCVOID pData, UINT len);
	void	Finalize();
	LPCBYTE GetHashCode() const { return (LPCBYTE)&CRC; }
	UINT	GetHashCodeSize() const { return HASH_CODE_SIZE; }
	DWORD	GetCRC() const { return CRC; }	// call after Finalize
};


} // namespace w32

