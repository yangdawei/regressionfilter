#include "stdafx.h"
#include "inet.h"
#include "..\rt\string_type.h"
#include <mmsystem.h>
#include <atlconv.h>

#pragma comment(lib, "ws2_32.lib")

namespace w32
{

struct _w32_socket_init
{	
	_w32_socket_init()
	{	WORD wVersionRequested = MAKEWORD(2,2);
		WSADATA wsaData;
		WSAStartup(wVersionRequested, &wsaData);
	}
	~_w32_socket_init(){ WSACleanup(); }
};

_w32_socket_init _w32_socket_init_obj;


/////////////////////////////////////////////////
// CInetAddr
CInetAddr::CInetAddr(const ULONG uipAddr_hostbyteorder, const USHORT ushPort)
{
	sin_family = AF_INET;
	SetAddress(uipAddr_hostbyteorder);
	SetPort(ushPort);
}

BOOL CInetAddr::SetAsLocal()
{
	char szHostname[256];
	gethostname(szHostname, sizeof(szHostname));
	struct hostent*_pHostInfo = gethostbyname(szHostname);
	if(_pHostInfo)
	{
		sin_addr.s_addr = *((u_long*)_pHostInfo->h_addr_list[0]);
		return TRUE;
	}

	return FALSE;
}

CInetAddr::CInetAddr(LPCTSTR pHostname, USHORT ushPort) // dotted IP addr string
{
	sin_family = AF_INET;
	SetPort(ushPort);
	SetAddress(pHostname);
}

BOOL CInetAddr::SetAddress(LPCTSTR pHostname)
{
	rt::StringA text((LPCSTR)ATL::CT2A(pHostname));

	LPSTR port = strrchr(text.Begin(),':');
	if(port)
	{
		SetPort(atoi(port+1));
		*port = '\0';
	}

	sin_addr.s_addr = inet_addr((LPCSTR)text);
	if(sin_addr.s_addr==INADDR_NONE)
	{	//need DNS
		struct hostent*_pHostInfo = gethostbyname(text);
		if(_pHostInfo)
		{
			sin_addr.s_addr = *((u_long*)_pHostInfo->h_addr_list[0]);
			return TRUE;
		}
	}
	else
		return TRUE;

	return FALSE;
}

////////////////////////////////////////////////////////////
// CSocket
int CSocket::GetLastError()
{
	return WSAGetLastError();
}

CSocket::CSocket()
{
	m_hSocket = INVALID_SOCKET;
}

BOOL CSocket::Create(const CInetAddr &BindTo,int nSocketType,BOOL overlapped_io,BOOL reuse_addr ,int AddrFormat)
{
	ASSERT(m_hSocket == INVALID_SOCKET);

	if(overlapped_io)
	{
		m_hSocket = WSASocket(	AddrFormat, 
								nSocketType, 0, 
								NULL, 0, 
								WSA_FLAG_OVERLAPPED);
	}
	else
	{
		m_hSocket = socket(AddrFormat, nSocketType, 0);
	}

	///////////////////////////////////////////
	// For blocking mode socket. The one created by WSASocket
	// is failed to cancel the blocking call after closesocket returns
	// , which usually causing deadlock

	BOOL  one = TRUE;

	if((INVALID_SOCKET != m_hSocket) &&
	   (!reuse_addr || 0==setsockopt(m_hSocket,SOL_SOCKET,SO_REUSEADDR,(char*)&one,sizeof(BOOL))) &&
	   (0==bind(m_hSocket,BindTo,sizeof(CInetAddr)))
	)
	{	linger l = {1,0};
		VERIFY(0==::setsockopt(m_hSocket,SOL_SOCKET,SO_LINGER,(char*)&l,sizeof(linger)));
		return TRUE;
	}
	else 
	{
		Close();
		return FALSE;
	}
}

void CSocket::Attach(SOCKET hSocket)
{
	ASSERT(m_hSocket == INVALID_SOCKET);
	m_hSocket = hSocket;
	ASSERT(m_hSocket != INVALID_SOCKET);
}

SOCKET CSocket::Detach()
{
	SOCKET s;
	s = m_hSocket;
	m_hSocket = INVALID_SOCKET;
	return s;
}

void CSocket::Close()
{
	if(m_hSocket != INVALID_SOCKET)
	{
		SOCKET tmp = m_hSocket;
		m_hSocket = INVALID_SOCKET;

		int val;
		int len = sizeof(val);
		if(	(getsockopt(tmp,SOL_SOCKET,SO_TYPE,(char*)&val,&len) == 0) &&
			(val == SOCK_STREAM)
		)
		{
			shutdown(tmp,SD_BOTH);	// cause deadlock with RecvFrom on SOCK_DGRAM socket
		}

		closesocket(tmp);
	}
}

BOOL CSocket::GetPeerName(CInetAddr &BindTo)
{
	ASSERT(m_hSocket != INVALID_SOCKET);
	int size = sizeof(CInetAddr);
	return getpeername(m_hSocket,BindTo,&size)==0;
}

BOOL CSocket::GetBindName(CInetAddr &BindTo)		// address of this socket
{
	ASSERT(m_hSocket != INVALID_SOCKET);
	int size = sizeof(CInetAddr);
	return getsockname(m_hSocket,BindTo,&size)==0;
}


BOOL CSocket::ConnectTo(const CInetAddr &target)
{
	return connect(m_hSocket,target,sizeof(CInetAddr))==0;
}

BOOL CSocket::IsValid()
{
	int val;
	int len = sizeof(val);
	return getsockopt(m_hSocket,SOL_SOCKET,SO_TYPE,(char*)&val,&len) == 0;
}

BOOL CSocket::IsConnected()
{
	CInetAddr peeraddr;
	int size = sizeof(CInetAddr);
	return getpeername(m_hSocket,peeraddr,&size)==0;
}

BOOL CSocket::Send(LPCVOID pData, UINT len)
{
	return len==send(m_hSocket,(const char*)pData,len,0);
}

BOOL CSocket::SendTo(LPCVOID pData, UINT len,const CInetAddr &target)
{
	return len==sendto(m_hSocket,(const char*)pData,len,0,target,sizeof(target));
}

BOOL CSocket::Recv(LPVOID pData, UINT len, UINT& len_out, BOOL Peek)
{
	UINT l = recv(m_hSocket,(char*)pData,len,Peek?MSG_PEEK:0);
	if(l==SOCKET_ERROR)return FALSE;
	len_out = l;
	return TRUE;
}

BOOL CSocket::RecvFrom(LPVOID pData, UINT len, UINT& len_out, CInetAddr &target, BOOL Peek)
{
	int la = sizeof(CInetAddr);
	int l = recvfrom(m_hSocket,(char*)pData,len,Peek?MSG_PEEK:0,target,&la);
	if(l==SOCKET_ERROR)return FALSE;
	len_out = l;
	return TRUE;
}

BOOL CSocket::SetBufferSize(int reserved_size, BOOL receiving_sending)
{
	return 0 == setsockopt(m_hSocket,SOL_SOCKET,receiving_sending?SO_RCVBUF:SO_SNDBUF,(char*)&reserved_size,sizeof(int));
}

BOOL CSocket::Listen(UINT pending_size)
{
	return 0 == listen(m_hSocket,pending_size);
}
	
BOOL CSocket::Accept(CSocket& connected_out, CInetAddr& peer_addr)
{
	int la = sizeof(CInetAddr);
	SOCKET sock = accept(m_hSocket,peer_addr,&la);
	if(INVALID_SOCKET != sock)
	{
		connected_out.Attach(sock);
		return TRUE;
	}
	else
	{
		return FALSE;
	}
}


//////////////////////////////////////////////////
// CHttpSession
CHttpSession::CHttpSession()
{
	m_Port = 0;
	_RecvBuffer.SetSize(16);
	_ClearHangingTick();
	m_pEventCallback = NULL;
	SetCommonHeaders("Mozilla/5.0");
}

CHttpSession::~CHttpSession()
{
}


BOOL CHttpSession::Request_Get(LPCSTR pURL, LPCSTR additional_header, UINT additional_header_len)
{
	return SendRequest(pURL, HTTP_VERB_GET, additional_header, additional_header_len);
}


BOOL CHttpSession::Request_Post(LPCSTR pURL, LPCBYTE data, UINT sz, LPCSTR data_type, LPCSTR charset, BOOL keep_alive)
{
	//if( SendRequest(pURL, HTTP_VERB_POST, TRUE, keep_alive) )
	//{
	//	BOOL succ = TRUE;
	//	succ = succ && m_TcpConn.Send("Content-Type: ", 14);
	//	succ = succ && m_TcpConn.Send(data_type, (UINT)strlen(data_type));
	//	succ = succ && m_TcpConn.Send("; charset=\"", 11);
	//	succ = succ && m_TcpConn.Send(charset, (UINT)strlen(charset));
	//	succ = succ && m_TcpConn.Send("\"\r\nContent-Length: ", 19);

	//	if(sz==0)sz = (UINT)strlen((LPCSTR)data);
	//	char buf[30];
	//	itoa(sz,buf,10);

	//	succ = succ && m_TcpConn.Send(buf, (UINT)strlen(buf));
	//	succ = succ && m_TcpConn.Send("\r\n\r\n", 4);

	//	if(succ)
	//	{
	//		for(; sz>1024; sz-=1024, data+=1024)
	//			if(!m_TcpConn.Send(data, 1024))
	//				return FALSE;

	//		return m_TcpConn.Send(data, sz);
	//	}
	//}

	return FALSE;
}

void CHttpSession::CancelResponseWaiting() // supposed to be called from other thread
{
	_ClearHangingTick();
	m_TcpConn.Close();
}

BOOL CHttpSession::SendRequest(LPCSTR pURL, DWORD verb, LPCSTR additional_header, UINT additional_header_len)
{
	ASSERT(verb < HTTP_VERB_MAX);

	LPCSTR p = pURL;
	for(;*p;p++)
		if(*p>' ')break;

	// parse scheme, handles http only
	{	LPCSTR t = strstr(p,"://");
		if(t)
		{
			if(	(pURL<=t-4) &&
				(t[-1] == 'p' || t[-1] == 'P') &&
				(t[-2] == 't' || t[-2] == 'T') &&
				(t[-3] == 't' || t[-3] == 'T') &&
				(t[-4] == 'h' || t[-4] == 'H') ){}
			else return FALSE;
			p = t+3;
		}
	}

	_UpdateHangingTick();

	// parse site domain
	BOOL reconnect = FALSE;
	LPCSTR	path="/";
	{	LPCSTR	host=p;
		UINT	port=80;
		UINT	host_len;
		LPCSTR t = strchr(p,'/');
		if(t)
		{	host_len = (UINT)(t-host);
			path=t; 
		}
		else{ host_len = (UINT)strlen(p); }

		UINT i=0;
		for(;i<host_len;i++)
			if(host[i]==':')
			{	char buf[6];
				for(UINT j=0;j<5;j++)
					if(i+j+1<host_len)buf[j] = host[i+j+1];
					else buf[j] = 0;
				buf[5] = 0;
				port = atoi(buf);
				break;
			}
		host_len = i;

		BOOL IsHostSame = TRUE;
		if(m_ServerName.GetLength() != host_len)
			IsHostSame = FALSE;
		else
			for(i=0;i<host_len;i++)
				if(m_ServerName[i] == host[i] || m_ServerName[i] == host[i] - ('a'-'A')){}
				else{ IsHostSame = FALSE; break; }

		if(port != m_Port || m_ServerName.GetLength() != host_len || !IsHostSame || !m_Keepalive)
		{	
			reconnect = TRUE;
			m_Port = port;
			m_ServerName = rt::StringA_Ref(host,host_len);
		}
	}

	_UpdateHangingTick();
	if(m_pEventCallback)m_pEventCallback->OnReflect((LPVOID)p,HTTPCLIENT_EVENT_URL_PARSED);

	if(reconnect || !m_TcpConn.IsConnected())
	{	
		m_TcpConn.Close();
		VERIFY(m_TcpConn.Create(w32::CInetAddr()));

		//printf("Site: %s\nPort: %d\nPath: %s\n",(LPSTR)m_ServerName,m_Port,path);
		//puts("connecting ... \n");

		w32::CInetAddr website(ATL::CA2T(m_ServerName),m_Port);
		if(m_pEventCallback)m_pEventCallback->OnReflect(0,HTTPCLIENT_EVENT_DNS_RESOLVED);
		if(!m_TcpConn.ConnectTo(website))
		{
			_ClearHangingTick();
			return FALSE;
		}
		if(m_pEventCallback)m_pEventCallback->OnReflect(m_ServerName.GetBuffer(),HTTPCLIENT_EVENT_CONNECTED);
	}
	else if(m_pEventCallback)
	{
		m_pEventCallback->OnReflect(0,HTTPCLIENT_EVENT_DNS_RESOLVED);
		m_pEventCallback->OnReflect(m_ServerName.GetBuffer(),HTTPCLIENT_EVENT_CONNECTED);
	}
	_UpdateHangingTick();

	BOOL succ = TRUE;

	if(HTTP_VERB_POST == verb)
		_SendBuffer = rt::StringA_Ref("POST ",5);
	else if(HTTP_VERB_GET == verb)
		_SendBuffer = rt::StringA_Ref("GET ",4);
	else ASSERT(0);

	_SendBuffer += rt::StringA_Ref(path,(UINT)strlen(path));
	_SendBuffer += rt::StringA_Ref(" HTTP/1.1\r\n",11);

	_SendBuffer += rt::StringA_Ref("Host: ",6);
	_SendBuffer += rt::StringA_Ref(m_ServerName,(UINT)(m_ServerName.GetLength()));
	_SendBuffer += rt::StringA_Ref("\r\n",2);

	_UpdateHangingTick();
	DWORD sent = 0;
	if(additional_header_len && additional_header)
	{
		WSABUF b[3] = {	{	_SendBuffer.GetLength(), _SendBuffer.GetBuffer() },
						{	additional_header_len, (CHAR*)additional_header },
						{	_CommonFields.GetLength(),_CommonFields.GetBuffer() }
					  };
		::WSASend(m_TcpConn,b,3,&sent,0,NULL,NULL);
		return sent == additional_header_len + _SendBuffer.GetLength() + _CommonFields.GetLength();
	}
	else
	{
		WSABUF b[2] = {	{	_SendBuffer.GetLength(), _SendBuffer.GetBuffer() },
						{	_CommonFields.GetLength(),_CommonFields.GetBuffer() }
					  };
		::WSASend(m_TcpConn,b,2,&sent,0,NULL,NULL);
		return sent == _SendBuffer.GetLength() + _CommonFields.GetLength();
	}

	ASSERT(0);
}

void CHttpSession::SetCommonHeaders(LPCSTR agent, BOOL keepalive)
{
	if(keepalive)
	{
		_CommonFields = rt::StringA_Ref("User-Agent: ") + agent + "\r\n"
						"Accept: */*\r\n"
						"Cache-Control: no-cache\r\n"
						"Proxy-Connection: Keep-Alive\r\n"
						"Connection: Keep-Alive\r\n"
						"\r\n";
	}
	else
	{
		_CommonFields = rt::StringA_Ref("User-Agent: ") + agent + "\r\n"
						"Accept: */*\r\n"
						"Cache-Control: no-cache\r\n"
						"Proxy-Connection: Close\r\n"
						"Connection: Close\r\n"
						"\r\n";
	}
	m_Keepalive = keepalive;
}


UINT CHttpSession::_RecvOneChunk()
{
	ASSERT(_RecvBuffer_Used <= _RecvBuffer.GetSize());

	UINT head = _RecvBuffer_Parsed;
	UINT tail = _RecvUntil("\r\n");

	_RecvBuffer[tail] = 0;
	UINT chunk_size = 0;
	strlwr(&_RecvBuffer[head]);
	if(sscanf(&_RecvBuffer[head],"%x",&chunk_size)==1)
	{
		if(chunk_size)
		{
			chunk_size+=2;
			{	//remove chunk size tag
				UINT tag_len = tail-head+2;
				_RecvBuffer_Parsed = head;
				_RecvBuffer_Used -= tag_len;
				memmove(&_RecvBuffer[head], &_RecvBuffer[tail+2], _RecvBuffer_Used - _RecvBuffer_Parsed);
			}

			if(_RecvUpTo(_RecvBuffer_Parsed + chunk_size))
				return chunk_size;
			else
				return INFINITE;
		}
		else
		{
			_RecvBuffer_Parsed -= 4;
		}
	}

	ASSERT(_RecvBuffer_Used <= _RecvBuffer.GetSize());

	return 0;
}


UINT CHttpSession::_RecvUntil(LPCSTR end_token, BOOL remove_parsed)
{
	ASSERT(_RecvBuffer_Used <= _RecvBuffer.GetSize());

	UINT token_len = (UINT)strlen(end_token);

	UINT head = 0xffffffff;
	for(;;)
	{	int remain_len = _RecvBuffer_Used - _RecvBuffer_Parsed;
		if(remain_len >= (int)token_len)
		{
			LPCSTR p = strstr(&_RecvBuffer[_RecvBuffer_Parsed],end_token);
			if(p)
			{	head = (UINT)(p-(LPCSTR)_RecvBuffer);
				_RecvBuffer_Parsed = head + token_len;
				break;
			}
			else{ _RecvBuffer_Parsed = _RecvBuffer_Used-token_len+1; }
		}

		if(_RecvBuffer.GetSize() <= _RecvBuffer_Used+1)_RecvBuffer.ChangeSize(_RecvBuffer_Used*2+1);
		UINT recv_len = 0;
		_UpdateHangingTick();
		if(!m_TcpConn.Recv(&_RecvBuffer[_RecvBuffer_Used],(UINT)(_RecvBuffer.GetSize()-_RecvBuffer_Used-1),recv_len)
		   || recv_len == 0
		)
		{	m_TcpConn.Close();
			return INFINITE; 
		}
		_RecvBuffer_Used += recv_len;
		_RecvBuffer[_RecvBuffer_Used] = '\0';
	}

	ASSERT(head != INFINITE);
	if(remove_parsed)
	{	
		memmove(_RecvBuffer,&_RecvBuffer[head],_RecvBuffer_Used - head);
		_RecvBuffer_Used -= head;
		_RecvBuffer_Parsed -= head;
		head = 0;
	}

	ASSERT(_RecvBuffer_Used <= _RecvBuffer.GetSize());

	return head;
}

BOOL CHttpSession::_RecvUpTo(UINT size_byte)
{	
	ASSERT(_RecvBuffer_Used <= _RecvBuffer.GetSize());

	if(size_byte+1>_RecvBuffer.GetSize())
		_RecvBuffer.ChangeSize(size_byte+1);

	while(_RecvBuffer_Used < size_byte)
	{
		UINT recv_len = 0;
		_UpdateHangingTick();
		if(!m_TcpConn.Recv(&_RecvBuffer[_RecvBuffer_Used],(UINT)(_RecvBuffer.GetSize()-1-_RecvBuffer_Used),recv_len)
		   || recv_len == 0
		)
		{	m_TcpConn.Close();
			return FALSE; 
		}

		_RecvBuffer_Used += recv_len;

		if(m_pEventCallback && !m_pEventCallback->OnReflect((LPVOID)(_RecvBuffer_Used - _ResponseStart),HTTPCLIENT_EVENT_CONTENT_RECEIVING))
			return FALSE;
	}

	_RecvBuffer[_RecvBuffer_Used] = 0;
	_RecvBuffer_Parsed = size_byte;
	ASSERT(_RecvBuffer_Used <= _RecvBuffer.GetSize());

	return TRUE;
}

BOOL CHttpSession::_RecvUntilClose()
{
	ASSERT(_RecvBuffer_Used <= _RecvBuffer.GetSize());
	static const int chunk_size = 1024;
	for(;;)
	{
		if(_RecvBuffer_Used + chunk_size >= _RecvBuffer.GetSize())
			_RecvBuffer.ChangeSize(max(_RecvBuffer_Used + chunk_size,_RecvBuffer.GetSize()*2));

		UINT recv_len = 0;
		_UpdateHangingTick();
		if(!m_TcpConn.Recv(&_RecvBuffer[_RecvBuffer_Used],(UINT)(_RecvBuffer.GetSize()-1-_RecvBuffer_Used),recv_len)
		   || recv_len == 0
		)
		{	m_TcpConn.Close();
			break;
		}

		if(recv_len)
			_RecvBuffer_Used += recv_len;
		else
			break;

		if(m_pEventCallback && !m_pEventCallback->OnReflect((LPVOID)(_RecvBuffer_Used - _ResponseStart),HTTPCLIENT_EVENT_CONTENT_RECEIVING))
			break;
	}

	_RecvBuffer_Parsed = _RecvBuffer_Used;
	_RecvBuffer[_RecvBuffer_Used] = 0;
	return TRUE;
}


void CHttpSession::ResponseHeader::Parse_Tie1(LPCSTR header_text)
{
	memset(this,0xff,sizeof(ResponseHeader));

	if(3!=sscanf(header_text,"HTTP/%d.%d %d",&m_VersionMajor,&m_VersionMinor,&m_StateCode))
	{	m_StateCode = 0xffffffff;
		return;
	}
	
	// connect
	m_ConnectPersisent = FALSE;
	{	LPCSTR conn = strstr(header_text,"Connection:");
		if(conn)
			m_ConnectPersisent = ( conn[12]!='C' && conn[12]!='c' );
		else
			m_ConnectPersisent = FALSE;
	}

	// content length
	{
		LPCSTR msglen_entry = strstr(header_text,"Content-Length:");
		if(msglen_entry)
		{	msglen_entry += 15;
			while(!isdigit(*msglen_entry))msglen_entry++;

			char numb[20];
			UINT l;
			for(l=0;l<19;l++,msglen_entry++)
				if(isdigit(*msglen_entry))
					numb[l] = *msglen_entry;
				else
					break;

			numb[l] = 0;
			m_ContentLength = atoi(numb);
		}
	}

	// chunked tranfer
	{	m_ChunkedTransfer = FALSE;
		LPCSTR te_entry = strstr(header_text,"Transfer-Encoding:");
		if(te_entry)
		{	te_entry += 18;
			while(!isalpha(*te_entry))te_entry++;
			m_ChunkedTransfer = (*te_entry == 'c') || (*te_entry == 'C');
		}
	}
}


void CHttpSession::ResponseHeader::Parse_Tie2(LPCSTR header_text)
{
	m_RawHeader = header_text;

	// content type
	{
		LPCSTR type_entry = strstr(header_text,"Content-Type: ");
		if(type_entry)
		{	type_entry += 14;
			LPCSTR tail = type_entry;
			while(*tail == '/' || (*tail>='a' && *tail<='z') || (*tail>='A' && *tail<='Z') )tail++;
			m_ContentType = rt::StringA_Ref(type_entry, tail);
			m_ContentType.ParseFields(m_ContentSubtype,2,'/');
		}
	}

	// redirection
	m_Redirected.Empty();
	if(m_StateCode/100 == 3)
	{
		LPCSTR redirect = strstr(header_text,"ocation: ");
		if(redirect)
		{
			redirect += 8;
			while(*redirect ==' ')redirect++;
			LPCSTR end = redirect;
			while(*end>' ')end++;
			m_Redirected = rt::StringA_Ref(redirect, end);
		}
	}
}



BOOL CHttpSession::WaitResponse()
{
	_ResponseStart = 0;
	_RecvBuffer_Used = 0;
	_RecvBuffer_Parsed = 0;

	if(0!=_RecvUntil("HTTP/", TRUE))
	{
		if(m_pEventCallback)m_pEventCallback->OnReflect(0,HTTPCLIENT_EVENT_ABORT);
		m_TcpConn.Close();
		return FALSE;
	}

	if(m_pEventCallback)m_pEventCallback->OnReflect(0,HTTPCLIENT_EVENT_FIRSTBYTE);

	UINT	header = 0;
	UINT	header_len = _RecvUntil("\r\n\r\n");
	if(header_len==0xffffffff)
	{	
		if(m_pEventCallback)m_pEventCallback->OnReflect(0,HTTPCLIENT_EVENT_ABORT);
		m_TcpConn.Close();
		return FALSE;
	}

	header_len += 2;
	char tt = _RecvBuffer[header_len];
	_RecvBuffer[header_len] = 0;

	m_ResponseHeader.Parse_Tie1(_RecvBuffer);
	if(m_pEventCallback)m_pEventCallback->OnReflect(&m_ResponseHeader,HTTPCLIENT_EVENT_HEADER_RECEIVED);

	_RecvBuffer[header_len] = tt;
	_ResponseStart = header_len+2;

	if(m_ResponseHeader.m_ChunkedTransfer)
	{	UINT ret;
		while(ret = _RecvOneChunk())
			if(ret == INFINITE)
			{	m_TcpConn.Close();
				return FALSE;
			}
	}
	else if(m_ResponseHeader.IsContentLengthKnown())
	{
		if(_RecvUpTo(header_len + 2 + m_ResponseHeader.m_ContentLength))
			_RecvBuffer[header_len + 2 + m_ResponseHeader.m_ContentLength] = 0;
		else
		{
			if(m_pEventCallback)m_pEventCallback->OnReflect(0,HTTPCLIENT_EVENT_ABORT);
			m_TcpConn.Close();
			return FALSE;
		}
	}
	else if(m_ResponseHeader.m_ConnectPersisent == FALSE)
	{	
		if(_RecvUntilClose()){}
		else
		{	if(m_pEventCallback)m_pEventCallback->OnReflect(0,HTTPCLIENT_EVENT_ABORT);
			m_TcpConn.Close();
			return FALSE;
		}
	}

	m_ResponseHeader.Parse_Tie2(_RecvBuffer);
	if(m_pEventCallback)m_pEventCallback->OnReflect((LPVOID)GetResponseLength(),HTTPCLIENT_EVENT_DONE);
	_ClearHangingTick();
	if(!m_Keepalive)m_TcpConn.Close();
	return TRUE;
}

LPBYTE CHttpSession::GetResponse()
{
	if(_RecvBuffer_Parsed > _ResponseStart)
	{
		ASSERT(_RecvBuffer.GetSize() >= _RecvBuffer_Parsed);
		return (LPBYTE)&_RecvBuffer[_ResponseStart];
	}
	else
		return NULL;
}

UINT CHttpSession::GetResponseLength()
{
	return _RecvBuffer_Parsed - _ResponseStart;
}

UINT CHttpSession::GetResponseHeaderLength()
{
	return _ResponseStart;
}

DWORD CHttpSession::GetIrresponsiveTime() const	// msec
{
	if(_HangingTick != INFINITE)
	{
		DWORD t = ::GetTickCount();
		if(t >= _HangingTick)
		{	return t - _HangingTick;
		}
		else
		{	return 0xffffffff - (_HangingTick - t);
		}
	}
	else return 0;
}

namespace inet
{

UINT Base64EncodeLength(int len)
{
	return (len+2)/3*4;
}

UINT Base64DecodeLength(LPCSTR pBase64, int str_len)
{
	if((str_len&3))return 0;

	if(pBase64[str_len-2] != '=' && pBase64[str_len-1] != '=')
	{
		return (str_len/4*3);
	}
	else if(pBase64[str_len-2] != '=' && pBase64[str_len-1] == '=')
	{
		return (str_len/4*3)-1;
	}
	else if(pBase64[str_len-2] == '=' && pBase64[str_len-1] == '=')
	{
		return (str_len/4*3)-2;
	}
	else return 0;
}

void Base64Encode(LPSTR pBase64Out,LPCVOID pData, int data_len)
{
	static const char table[] = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/";
	
	LPCBYTE p = (LPCBYTE)pData;
	for(;data_len>=3;data_len-=3,p+=3,pBase64Out+=4)
	{
		pBase64Out[0] = table[p[0]>>2];
		pBase64Out[1] = table[((p[0]&0x3)<<4)|(p[1]>>4)];
		pBase64Out[2] = table[((p[1]&0xf)<<2)|(p[2]>>6)];
		pBase64Out[3] = table[p[2]&0x3f];
	}

	if(data_len == 1)
	{
		pBase64Out[0] = table[p[0]>>2];
		pBase64Out[1] = table[((p[0]&0x3)<<4)];
		pBase64Out[2] = '=';
		pBase64Out[3] = '=';
		//pBase64Out[4] = '\0';
	}
	else if(data_len == 2)
	{
		ASSERT(data_len == 2);
		pBase64Out[0] = table[p[0]>>2];
		pBase64Out[1] = table[((p[0]&0x3)<<4)|(p[1]>>4)];
		pBase64Out[2] = table[((p[1]&0xf)<<2)];
		pBase64Out[3] = '=';
		//pBase64Out[4] = '\0';
	}
	//else
	//	pBase64Out[0] = '\0';
}

BOOL Base64Decode(LPVOID pDataOut,int data_len,LPCSTR pBase64, int str_len)
{
	if((str_len&3))return FALSE;

	static const int rev_table[80] = // base on 2B
	{
		62,-1,-1,-1,			//'+' 2B,2C,2D,2E,
		63,						//'/' 2F
		52,53,54,55,56,57,58,59,60,61,	// '0' - '9', 30 - 39
		-1,-1,-1,-1,-1,-1,-1,	// '=', 3A, 3B, 3C, |3D|, 3E, 3F, 40
		0,1,2,3,4,5,6,7,8,9,	// 'A'-'Z', 41 - 5A
		10,11,12,13,14,15,16,17,18,19,
		20,21,22,23,24,25,		
		-1,-1,-1,-1,-1,-1,		// 5B 5C 5D 5E 5F 60
		26,27,28,29,30,31,32,33,34,35, // 'a'-'z' 61 - 7A
		36,37,38,39,40,41,42,43,44,45,46,47,48,49,50,51
	};

	LPBYTE p = (LPBYTE)pDataOut;
	int obj_len = (str_len/4*3)-2;
	if(data_len<obj_len)return FALSE;

	int a[4];
	if(rt::IsInRange_CC<int>(pBase64[str_len-4],0x2b,0x7a) && (a[0]=rev_table[pBase64[str_len-4]-0x2b])>=0){}
	else return FALSE;
	if(rt::IsInRange_CC<int>(pBase64[str_len-3],0x2b,0x7a) && (a[1]=rev_table[pBase64[str_len-3]-0x2b])>=0){}
	else return FALSE;

	if(pBase64[str_len-2] != '=' && pBase64[str_len-1] != '=')
	{
		if(data_len != obj_len+2)return FALSE;

		if(rt::IsInRange_CC<int>(pBase64[str_len-2],0x2b,0x7a) && (a[2]=rev_table[pBase64[str_len-2]-0x2b])>=0){}
		else return FALSE;
		if(rt::IsInRange_CC<int>(pBase64[str_len-1],0x2b,0x7a) && (a[3]=rev_table[pBase64[str_len-1]-0x2b])>=0){}
		else return FALSE;

		p[obj_len-1] = (a[0]<<2) | (a[1]>>4);
		p[obj_len+0] = ((a[1]&0xf)<<4) | (a[2]>>2);
		p[obj_len+1] = ((a[2]&0x3)<<6) | a[3];
	}
	else if(pBase64[str_len-2] != '=' && pBase64[str_len-1] == '=')
	{
		if(data_len != obj_len+1)return FALSE;
		if(rt::IsInRange_CC<int>(pBase64[str_len-2],0x2b,0x7a) && (a[2]=rev_table[pBase64[str_len-2]-0x2b])>=0){}
		else return FALSE;

		p[obj_len-1] = (a[0]<<2) | (a[1]>>4);
		p[obj_len+0] = ((a[1]&0xf)<<4) | (a[2]>>2);
	}
	else if(pBase64[str_len-2] == '=' && pBase64[str_len-1] == '=')
	{
		if(data_len != obj_len)return FALSE;
		p[obj_len-1] = (a[0]<<2) | (a[1]>>4);
	}
	else return FALSE;

	for(;str_len>4;str_len-=4,pBase64+=4,p+=3)
	{
		if(rt::IsInRange_CC<int>(pBase64[0],0x2b,0x7a) && (a[0]=rev_table[pBase64[0]-0x2b])>=0){}
		else return FALSE;
		if(rt::IsInRange_CC<int>(pBase64[1],0x2b,0x7a) && (a[1]=rev_table[pBase64[1]-0x2b])>=0){}
		else return FALSE;
		if(rt::IsInRange_CC<int>(pBase64[2],0x2b,0x7a) && (a[2]=rev_table[pBase64[2]-0x2b])>=0){}
		else return FALSE;
		if(rt::IsInRange_CC<int>(pBase64[3],0x2b,0x7a) && (a[3]=rev_table[pBase64[3]-0x2b])>=0){}
		else return FALSE;

		p[0] = (a[0]<<2) | (a[1]>>4);
		p[1] = ((a[1]&0xf)<<4) | (a[2]>>2);
		p[2] = ((a[2]&0x3)<<6) | a[3];
	}

	return TRUE;
}


UINT Base16EncodeLength(int len){ return len/2; }
UINT Base16DecodeLength(int len){ return len*2 + 1; }
void Base16Encode(LPSTR pBase16Out,LPCVOID pData_in, int data_len)
{
	LPCBYTE pData = (LPCBYTE)pData_in;
	for(int i=0;i<data_len;i++)
	{
		int c1 = pData[i]>>4;
		int c2 = pData[i]&0xf;
		pBase16Out[2*i+0] = (c1>9)?('A'+c1-10):('0'+c1);
		pBase16Out[2*i+1] = (c2>9)?('A'+c2-10):('0'+c2);
	}
}

BOOL Base16Decode(LPVOID pDataOut_in,int data_len,LPCSTR pBase16, int str_len)
{
	LPBYTE pDataOut = (LPBYTE)pDataOut_in;

	if(str_len != data_len*2)return FALSE;
	for(int i=0;i<data_len;i++)
	{
		int c1 = pBase16[2*i+0];
		int c2 = pBase16[2*i+1];

		if(c1<='9')
		{	if(c1<'0')return FALSE;
			c1 -= '0';
		}
		else
		{	if(c1<'A' || c1>'F')return FALSE;
			c1 -= 'A' - 10;
		}

		if(c2<='9')
		{	if(c2<'0')return FALSE;
			c2 -= '0';
		}
		else
		{	if(c2<'A' || c2>'F')return FALSE;
			c2 -= 'A' - 10;
		}
	
		pDataOut[i] = (c1<<4) | c2;
	}

	return TRUE;
}


} // namespace inet


} // namespace w32
