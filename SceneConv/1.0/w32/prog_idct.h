#pragma once

//////////////////////////////////////////////////////////////////////
// Visual Computing Foundation Classes (VCFC)
//					Jiaping Wang  2006.2
//					e_boris2002@hotmail.com
//
// (C) Copyright Jiaping Wang 2004.
//
// Permission to copy, use, modify, sell and distribute this software
// is granted provided this copyright notice appears in all copies.
// This software is provided "as is" without express or implied
// warranty, and with no claim as to its suitability for any purpose.
//
// Absolutely no warranty!!
//////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////
//  Brief Description
//
//  prog_idct.h
//
//  display prograss 
//
//////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////
// Revise History
//
// Inital version	2006.6.9		Jiaping
//
//////////////////////////////////////////////////////////////////////

#include "win32_ver.h"
#include "w32_basic.h"
#include "debug_log.h"
#include <atlimage.h>


namespace w32
{

enum _tagTimeMeasureDisplayMode
{
	TMDM_CONSOLE = 0,
	TMDM_STATUSBAR_FRAMEWND,	// draw on statusbar of the mainframe	hWnd is handle of the mainframe
	TMDM_STATUSBAR,				// draw on statusbar					hWnd is handle of the status bar
	TMDM_WND_RECT,				// draw a region of a window/dialog		hWnd is handle of the window, pArea is the region in client coordinate
};

extern void SetTimeMeasureDisplayMode(DWORD mode = TMDM_CONSOLE, HWND hWnd = NULL, const LPRECT pArea = NULL);

namespace _meta_
{
	class _Prograss_Indicator
	{
	private:
		int					_bar_offset;
	protected:
		static HBRUSH		m_uiProgressbar;
		static HBRUSH		m_uiBg;
		static HPEN			m_uiBorder;
		static HPEN			m_uiBorder_lt;
		ATL::CImage			m_BackDC;

		DWORD				m_Mode;
		HWND				m_hHostWnd;
		POINT				m_HostPosition;
		RECT				m_HostArea;

		int					m_ThreadOriginalPriority;
		w32::CTimeMeasure	m_Timer;
		w32::CTimeMeasure	m_TimeMeasure;
		TCHAR				m_szTitle[54];
		UINT				m_szTitleLen;

		void				_SetupStatusBar();
	public:
		void				UpdatePrograss(float ratio);
		_Prograss_Indicator(DWORD work_thread_id, LPCTSTR title = NULL);
		~_Prograss_Indicator();
	};
}

///////////////////////////////////////
// CTimeMeasureDisplay
template<typename t_Counting=UINT>
class CTimeMeasureDisplay
{
protected:
	LPCTSTR				m_szTitle;
	UINT				m_TimeInterval;
	CCriticalSection	m_CCS;
	DWORD				m_WorkThreadId;
	HANDLE				m_hDisplayThread;

	t_Counting			m_TotalCounter;
	t_Counting *		m_pCounter;
	t_Counting			m_Total;

	static DWORD WINAPI TimerThread( LPVOID pThis )
	{
		UINT TimeInterval;
		t_Counting Start;
		{
			EnterCCSBlock(((CTimeMeasureDisplay*)pThis)->m_CCS);
			TimeInterval = ((CTimeMeasureDisplay*)pThis)->m_TimeInterval;
			if( ((CTimeMeasureDisplay*)pThis)->m_pCounter )
			{	Start = (*((CTimeMeasureDisplay*)pThis)->m_pCounter);
			}else{ return 0; }
		}

		{	_meta_::_Prograss_Indicator prog_bar(	((CTimeMeasureDisplay*)pThis)->m_WorkThreadId,
													((CTimeMeasureDisplay*)pThis)->m_szTitle);
			for(;;)
			{
				float prog;
				{EnterCCSBlock(((CTimeMeasureDisplay*)pThis)->m_CCS);
					if( ((CTimeMeasureDisplay*)pThis)->m_pCounter )
					{	prog =	(float)((*((CTimeMeasureDisplay*)pThis)->m_pCounter) - Start)
								/
								(float)(((CTimeMeasureDisplay*)pThis)->m_Total - Start);
					}else{ return 0; }
				}
				prog_bar.UpdatePrograss(prog);
				Sleep(TimeInterval);
			}
		}
	}

public:
	~CTimeMeasureDisplay(){ Terminate(); }

	CTimeMeasureDisplay(t_Counting * pCounter,t_Counting Total,LPCTSTR pszCaption = NULL,HWND hMainWnd = NULL,UINT TimeInterval = 500) // TimeInterval unit is ms
	{
		m_pCounter = pCounter;
		m_Total = Total;
		m_TimeInterval = TimeInterval;
		m_szTitle = pszCaption;

		m_hDisplayThread = ::CreateThread(NULL,0,TimerThread,this,0,NULL);
		::SetThreadPriority(m_hDisplayThread,THREAD_PRIORITY_HIGHEST);
		
		Sleep(0);	
	}

	void Terminate()
	{	if(m_pCounter)
		{
			{	EnterCCSBlock(m_CCS);
				m_pCounter = NULL;
			}
			w32::WaitForThreadEnding(m_hDisplayThread);
		}
		::CloseHandle(m_hDisplayThread);
	}
};


}

