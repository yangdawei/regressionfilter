#pragma once

//////////////////////////////////////////////////////////////////////
// Visual Computing Foundation Classes (VCFC)
//					Jiaping Wang  2006.2
//					e_boris2002@hotmail.com
//
// (C) Copyright Jiaping Wang 2004.
//
// Permission to copy, use, modify, sell and distribute this software
// is granted provided this copyright notice appears in all copies.
// This software is provided "as is" without express or implied
// warranty, and with no claim as to its suitability for any purpose.
//
// Absolutely no warranty!!
//////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////
//  Brief Description
//
//  mysql_conn.h
//
//  Warpping class of MySQL connection (MySQL 5.1 client SDK)
//
//  Library Dependence: mysql.lib
//
//////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////
// Revise History
//
// Inital version	2010.2.14		Jiaping
//
//////////////////////////////////////////////////////////////////////

#include <vector>
#include <string>

#include "w32_basic.h"
#include "../rt/compact_vector.h"

#pragma comment(lib,"mysqlclient.lib")

struct st_mysql;
struct st_mysql_res;
struct st_mysql_stmt;
struct st_mysql_bind;

namespace w32
{

class MySQL_Connection;
class MySQL_RecordSet;
class MySQL_BindValues;

class MySQL_Connection
{
	friend class MySQL_RecordSet;
	rt::StringA	 _Backup_User;
	rt::StringA	 _Backup_Password;
	rt::StringA	 _Backup_Server;
	rt::StringA	 _Backup_Databse;
	UINT		 _Backup_Port;

protected:
	st_mysql*	m_pConn;
	BOOL		m_bTranscation;
	w32::CCriticalSection	m_TranscationCSS;
	BOOL		m_bConnected;

public:
	struct _QRet
	{
		friend class MySQL_RecordSet;
		friend class MySQL_Connection;

		st_mysql_res*	m_pResultSet;
		UINT			m_FieldCount;

		_QRet(const _QRet& x){ *this = x; rt::_CastToNonconst(&x)->m_pResultSet = NULL;}
		~_QRet(){ Clear(); }
		void Clear();

	protected:
		_QRet(){ m_pResultSet = NULL; m_FieldCount = 0; }
		_QRet(st_mysql_res* set, UINT fco){ m_pResultSet = set; m_FieldCount = fco; }
	};

public:
	MySQL_Connection();
	~MySQL_Connection();
	BOOL	DuplicateConnection(const MySQL_Connection& x);
	BOOL	ConnectTo(LPCSTR user, LPCSTR password, LPCSTR server_addr, LPCSTR databse, UINT port = 0);
	BOOL	ConnectTo(LPCSTR user, LPCSTR password, LPCSTR server_url); // server_url = [schema@]hostname[:port]
	void	Destroy();
	BOOL	IsConnected() const { return m_pConn && m_bConnected; }
	_QRet	Query(LPCSTR pSQL, BOOL allow_seek = FALSE);
	BOOL	Execute(LPCSTR pSQL, ULONGLONG* pMatchedCount = NULL);
	void	Transcation_Begin();
	void	Transcation_Commit();
	void	Transcation_Rollback();
	LPCSTR	GetLastErrorMessage() const;
	UINT	GetLastError() const;
	UINT	GetLastWarningCount() const;
};

class MySQL_UpdateValues
{	
	MySQL_Connection&	_Conn;
	rt::Buffer<char>	_Statement;
	LPSTR	_pValuesBase;
	LPSTR	_pValues;
	BOOL	_Claimed;
	BOOL	_HasError;
public:
	MySQL_UpdateValues(MySQL_Connection& sql_conn, LPCSTR table, LPCSTR fields, BOOL no_overwirte = FALSE);
	~MySQL_UpdateValues(){ Flush(); }
	LPSTR	ClaimRow(UINT length = 256);		// write a full row xxx,xxx...,xxx to the buffer, without any whitespace
	void	SubmitRow(UINT finalized_length);
	BOOL	Flush();
	BOOL	HasError() const { return _HasError; }
};

enum
{	SQL_FIELD_TYPE_INTEGER,
	SQL_FIELD_TYPE_FLOAT,
	SQL_FIELD_TYPE_STRING,
	SQL_FIELD_TYPE_TIMEDATE,
	SQL_FIELD_TYPE_ENUM,
	SQL_FIELD_TYPE_SET,
	SQL_FIELD_TYPE_BLOB,
	SQL_FIELD_TYPE_UNKNOWN,
};

class MySQL_RecordSet:public MySQL_Connection::_QRet	// not thread-safe
{	
private:
	MySQL_RecordSet(const MySQL_RecordSet& x);

protected:
	char **		m_CurrentRow;
public:
	MySQL_RecordSet(){};
	MySQL_RecordSet(const MySQL_Connection::_QRet& x);
	void operator = (const MySQL_Connection::_QRet& x);
	UINT		GetFieldCount() const { return m_FieldCount; }
	LPCSTR		GetFieldName(UINT i) const;
	DWORD		GetFieldType(UINT i) const;
	LPCSTR		GetField(UINT i);
	BOOL		IsEOF() const { return m_CurrentRow==NULL || (*m_CurrentRow)==NULL; }
	BOOL		Next();
	BOOL		SeekTo(UINT absolute_offset);
	UINT		GetSize_Seekable() const; // only available when the set is seekable

	LPCSTR operator	[](UINT field_id){ return GetField(field_id); }
};


//template <typename T> class _BindParam{
//
//};
//
//template <> class _BindParam<float>{
//public:
//	enum {BUF_TYPE = MYSQL_TYPE_FLOAT};
//	enum {IS_NUM = true};
//	enum {IS_UNSIGNED = false};
//
//};


class MySQL_BindValues{
public:
	enum {DEFAULT_BUF_SIZE = 256};

	MySQL_BindValues(unsigned int size = 0);
	virtual ~MySQL_BindValues();

	void Create(unsigned int size);
	void Destroy();
	unsigned int size() const {return _lens.GetSize();}

	void setValue(unsigned int idx, float val);
	void setValue(unsigned int idx, double val);
	
	void setValue(unsigned int idx, short val);
	void setValue(unsigned int idx, int val);
	void setValue(unsigned int idx, long val);
	void setValue(unsigned int idx, long long val);

	void setValue(unsigned int idx, unsigned short val);
	void setValue(unsigned int idx, unsigned int val);
	void setValue(unsigned int idx, unsigned long val);
	void setValue(unsigned int idx, unsigned long long val);

	void setValue(unsigned int idx, const std::string& val);
	void setValue(unsigned int idx, const rt::Buffer<BYTE>& val);

	//bool getValue(unsigned int idx, float& val);
	//bool getValue(unsigned int idx, double& val);
	//
	//bool getValue(unsigned int idx, short& val);
	//bool getValue(unsigned int idx, int& val);
	//bool getValue(unsigned int idx, long& val);
	//bool getValue(unsigned int idx, long long& val);

	//bool getValue(unsigned int idx, unsigned short& val);
	//bool getValue(unsigned int idx, unsigned int& val);
	//bool getValue(unsigned int idx, unsigned long& val);
	//bool getValue(unsigned int idx, unsigned long long& val);

	//bool getValue(unsigned int idx, std::string& val);
	//bool getValue(unsigned int idx, rt::Buffer<BYTE>& val);



	bool isReady() const {		
		return size()==0 || ( __binds != NULL && _bindFlag.Sum()==size()) ;
	}

	st_mysql_bind* getMySqlBind() {return __binds;}
	
	//void ReallocateEnoughBuffer();

protected:
	st_mysql_bind* __binds;
	rt::Buffer<unsigned long> _lens;
	rt::Buffer<unsigned short> _bindFlag;
	rt::Buffer<rt::Buffer<BYTE> > _vals;
};


template<typename T> void fromBuf(const rt::Buffer<BYTE> &buf, T& t){
	std::string str((const char*)buf.Begin(),buf.GetSize());
	std::stringstream ss(str);
	ss >> t;
}
__forceinline void fromBuf(const rt::Buffer<BYTE> &buf, std::string& t){	
	t = std::string((const char*)buf.Begin(),buf.GetSize());
}
__forceinline void fromBuf(const rt::Buffer<BYTE> &buf,rt::Buffer<BYTE> &t){	
	t.SetSize(buf.GetSize());
	t.CopyFrom(buf);
}

class MySQL_ConnectionEx: public MySQL_Connection {

//public:
//	class _ResultSet: public MySQL_BindValues{
//	public:
//		_ResultSet(unsigned int size):MySQL_BindValues(size){
//			_stmt = NULL;
//		}
//		~_ResultSet(){
//			if(_stmt) delete _stmt;
//		}
//
//	public:
//		 NextRow();
//
//		st_mysql_stmt* _stmt;
//	};
	class _QueryOutput{
	public:
		rt::Buffer< rt::Buffer<rt::Buffer<BYTE> >  >  vals;

		template<typename T1> bool unique(T1& t1){
			if(vals.GetSize()!=1 && vals[0].GetSize()!= 1) 
				return false;
			else{
				fromBuf(vals[0][0],t1);
				return true;
			}			
		}

		template<typename T1,typename T2> bool unique(T1& t1,T2& t2){
			if(vals.GetSize()!=2 && vals[0].GetSize()!= 1) 
				return false;
			else{
				fromBuf(vals[0][0],t1);fromBuf(vals[0][1],t2);
				return true;
			}			
		}

		template<typename T1,typename T2,typename T3> bool unique(T1& t1,T2& t2,T3& t3){
			if(vals.GetSize()!=3 && vals[0].GetSize()!= 1) 
				return false;
			else{
				fromBuf(vals[0][0],t1);fromBuf(vals[0][1],t2); fromBuf(vals[0][2],t3);
				return true;
			}			
		}

		template<typename T1,typename T2,typename T3,typename T4> bool unique(T1& t1,T2& t2,T3& t3,T4& t4){
			if(vals.GetSize()!=4 && vals[0].GetSize()!= 1) 
				return false;
			else{
				fromBuf(vals[0][0],t1);fromBuf(vals[0][1],t2); fromBuf(vals[0][2],t3); fromBuf(vals[0][3],t4);
				return true;
			}			
		}

		template<typename T1,typename T2,typename T3,typename T4,typename T5> bool unique(T1& t1,T2& t2,T3& t3,T4& t4,T5& t5){
			if(vals.GetSize()!=5 && vals[0].GetSize()!= 1) 
				return false;
			else{
				fromBuf(vals[0][0],t1);fromBuf(vals[0][1],t2); fromBuf(vals[0][2],t3); fromBuf(vals[0][3],t4); fromBuf(vals[0][4],t5);
				return true;
			}			
		}

		template <typename T1>  bool   list(rt::Buffer<T1> & v1){

			if(vals.GetSize()>0 && vals[0].GetSize()!= 1) return false;
			if(vals.GetSize()>0){
				v1.SetSize(vals.GetSize());
				for(unsigned int i=0;i<vals.GetSize();i++){
					fromBuf(vals[i][0],v1[i]);
				}
			}
			return true;
		}
		template<typename T1,typename T2> 
		bool   list(rt::Buffer<T1> & v1,rt::Buffer<T2> & v2){

			if(vals.GetSize()>0 && vals[0].GetSize()!= 2) return false;
			if(vals.GetSize()>0){
				v1.SetSize(vals.GetSize()); v2.SetSize(vals.GetSize());
				for(unsigned int i=0;i<vals.GetSize();i++){
					fromBuf(vals[i][0],v1[i]); fromBuf(vals[i][1],v2[i]);
				}
			}
			return true;
		}

		template<typename T1,typename T2,typename T3> 
		bool   list(rt::Buffer<T1> & v1,rt::Buffer<T2> & v2,rt::Buffer<T3> & v3){

			if(vals.GetSize()>0 && vals[0].GetSize()!= 3) return false;
			if(vals.GetSize()>0){
				v1.SetSize(vals.GetSize());v2.SetSize(vals.GetSize());
				v3.SetSize(vals.GetSize());
				for(unsigned int i=0;i<vals.GetSize();i++){
					fromBuf(vals[i][0],v1[i]); fromBuf(vals[i][1],v2[i]);
					fromBuf(vals[i][2],v3[i]);
				}
			}
			return true;
		}

		template<typename T1,typename T2,typename T3,typename T4> 
		bool   list(rt::Buffer<T1> & v1,rt::Buffer<T2> & v2,rt::Buffer<T3> & v3,rt::Buffer<T4> & v4){

			if(vals.GetSize()>0 && vals[0].GetSize()!= 4) return false;
			if(vals.GetSize()>0){
				v1.SetSize(vals.GetSize()); v2.SetSize(vals.GetSize());
				v3.SetSize(vals.GetSize()); v4.SetSize(vals.GetSize());
				for(unsigned int i=0;i<vals.GetSize();i++){
					fromBuf(vals[i][0],v1[i]); fromBuf(vals[i][1],v2[i]);
					fromBuf(vals[i][2],v3[i]); fromBuf(vals[i][3],v4[i]);
				}
			}
			return true;
		}

		template<typename T1,typename T2,typename T3,typename T4,typename T5> 
		bool   list(rt::Buffer<T1> & v1,rt::Buffer<T2> & v2,rt::Buffer<T3> & v3,rt::Buffer<T4> & v4,rt::Buffer<T5> & v5){

			if(vals.GetSize()>0 && vals[0].GetSize()!= 5) return false;
			if(vals.GetSize()>0){
				v1.SetSize(vals.GetSize()); v2.SetSize(vals.GetSize());
				v3.SetSize(vals.GetSize()); v4.SetSize(vals.GetSize());
				v5.SetSize(vals.GetSize());
				for(unsigned int i=0;i<vals.GetSize();i++){
					fromBuf(vals[i][0],v1[i]); fromBuf(vals[i][1],v2[i]);
					fromBuf(vals[i][2],v3[i]); fromBuf(vals[i][3],v4[i]);
					fromBuf(vals[i][4],v5[i]);
				}
			}
			return true;
		}

	};

public:

	bool   _QueryPrepare(LPCSTR pSQL, MySQL_BindValues & inValues,rt::Buffer< rt::Buffer<rt::Buffer<BYTE> >  > & outVals);
	int    _ExecutePrepare(LPCSTR pSQL,MySQL_BindValues & inValues); // the num of affected rows is returned, if failed, return -1

	int ExecutePrepare(LPCSTR pSQL){
		MySQL_BindValues bv;
		return _ExecutePrepare(pSQL,bv);
	}
	template<typename T1> 
	int  ExecutePrepare(LPCSTR pSQL,const T1& t1) {
		MySQL_BindValues bv(1);
		bv.setValue(0,t1);
		return _ExecutePrepare(pSQL,bv);
	}
	template<typename T1,typename T2> 
	int  ExecutePrepare(LPCSTR pSQL,const T1& t1,const T2& t2) {
		MySQL_BindValues bv(2);
		bv.setValue(0,t1);bv.setValue(1,t2);
		return _ExecutePrepare(pSQL,bv);
	}
	template<typename T1,typename T2,typename T3> 
	int  ExecutePrepare(LPCSTR pSQL,const T1& t1,const T2& t2,const T3& t3) {
		MySQL_BindValues bv(3);
		bv.setValue(0,t1);bv.setValue(1,t2);bv.setValue(2,t3);
		return _ExecutePrepare(pSQL,bv);
	}
	template<typename T1,typename T2,typename T3, typename T4> 
	int  ExecutePrepare(LPCSTR pSQL,const T1& t1,const T2& t2,const T3& t3,const T4& t4) {
		MySQL_BindValues bv(4);
		bv.setValue(0,t1);bv.setValue(1,t2);bv.setValue(2,t3);bv.setValue(3,t4);
		return _ExecutePrepare(pSQL,bv);
	}
	template<typename T1,typename T2,typename T3, typename T4, typename T5> 
	int  ExecutePrepare(LPCSTR pSQL,const T1& t1,const T2& t2,const T3& t3,const T4& t4,const T5& t5) {
		MySQL_BindValues bv(5);
		bv.setValue(0,t1);bv.setValue(1,t2);bv.setValue(2,t3);bv.setValue(3,t4);bv.setValue(4,t5);
		return _ExecutePrepare(pSQL,bv);
	}
	template<typename T1,typename T2,typename T3, typename T4, typename T5, typename T6> 
	int  ExecutePrepare(LPCSTR pSQL,const T1& t1,const T2& t2,const T3& t3,const T4& t4,const T5& t5,const T6& t6) {
		MySQL_BindValues bv(6);
		bv.setValue(0,t1);bv.setValue(1,t2);bv.setValue(2,t3);bv.setValue(3,t4);bv.setValue(4,t5);bv.setValue(5,t6);
		return _ExecutePrepare(pSQL,bv);
	}

	_QueryOutput QueryPrepare(LPCSTR pSQL){
		_QueryOutput qo; MySQL_BindValues bv;
		_QueryPrepare(pSQL,bv,qo.vals);
		return qo;
	}
	template<typename T1> _QueryOutput QueryPrepare(LPCSTR pSQL,const T1& t1) {
		_QueryOutput qo; MySQL_BindValues bv(1);
		bv.setValue(0,t1);
		_QueryPrepare(pSQL,bv,qo.vals);
		return qo;
	}
	template<typename T1,typename T2> 
	 _QueryOutput QueryPrepare(LPCSTR pSQL,const T1& t1,const T2& t2) {
		_QueryOutput qo; MySQL_BindValues bv(2);
		bv.setValue(0,t1);bv.setValue(1,t2);
		_QueryPrepare(pSQL,bv,qo.vals);
		return qo;
	}
	template<typename T1,typename T2,typename T3> 
	_QueryOutput QueryPrepare(LPCSTR pSQL,const T1& t1,const T2& t2,const T3& t3) {
		_QueryOutput qo; MySQL_BindValues bv(3);
		bv.setValue(0,t1);bv.setValue(1,t2);bv.setValue(2,t3);
		_QueryPrepare(pSQL,bv,qo.vals);
		return qo;
	}
	template<typename T1,typename T2,typename T3, typename T4> 
	_QueryOutput QueryPrepare(LPCSTR pSQL,const T1& t1,const T2& t2,const T3& t3,const T4& t4) {
		_QueryOutput qo;MySQL_BindValues bv(4);
		bv.setValue(0,t1);bv.setValue(1,t2);bv.setValue(2,t3);bv.setValue(3,t4);
		_QueryPrepare(pSQL,bv,qo.vals);
		return qo;
	}
	template<typename T1,typename T2,typename T3, typename T4, typename T5> 
	_QueryOutput QueryPrepare(LPCSTR pSQL,const T1& t1,const T2& t2,const T3& t3,const T4& t4,const T5& t5) {
		_QueryOutput qo;MySQL_BindValues bv(5);
		bv.setValue(0,t1);bv.setValue(1,t2);bv.setValue(2,t3);bv.setValue(3,t4);bv.setValue(4,t5);
		_QueryPrepare(pSQL,bv,qo.vals);
		return qo;
	}
	template<typename T1,typename T2,typename T3, typename T4, typename T5, typename T6> 
	_QueryOutput QueryPrepare(LPCSTR pSQL,const T1& t1,const T2& t2,const T3& t3,const T4& t4,const T5& t5,const T6& t6) {
		_QueryOutput qo;MySQL_BindValues bv(6);
		bv.setValue(0,t1);bv.setValue(1,t2);bv.setValue(2,t3);bv.setValue(3,t4);bv.setValue(4,t5);bv.setValue(5,t6);
		_QueryPrepare(pSQL,bv,qo.vals);
		return qo;
	}
};

} // namespace w32

