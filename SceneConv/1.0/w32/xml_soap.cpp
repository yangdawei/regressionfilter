#include "stdafx.h"
#include "w32_basic.h"
#include "xml_soap.h"
#include "file_64.h"
#include "debug_log.h"

namespace w32
{

const CHAR CXMLComposer::g_XML_Header[40] = "<?xml version=\"1.0\" encoding=\"utf-8\" ?>";

CXMLComposer::CXMLComposer(rt::_stream * out_stream)
{
	if(out_stream)
	{
		m_StreamOutOrgPos = out_stream->Seek(0,rt::_stream::Seek_Current);
		m_pStreamOut = out_stream;
	}
	else
	{
		m_StreamOutOrgPos = 0;
		m_pStreamOut = NULL;
	}
	ResetContent();
}

void CXMLComposer::Linebreak(int vary)
{
	Output('\15');
	Output('\12');

	for(UINT i = 0; i<m_NestLevelTag.GetSize() + vary; i++)
	{
		Output(' ');
		Output(' ');
		Output(' ');
		Output(' ');
	}
}

void CXMLComposer::ResetContent(LPCSTR customized_header)
{
	if(m_pStreamOut)
	{
		m_pStreamOut->Seek(m_StreamOutOrgPos);
	}
	else m_Output.Empty();

	if(customized_header)
		Output(customized_header);
	else
		Output(g_XML_Header);

	m_NestLevelTag.ChangeSize(0);
	_EnteringNodeTag = NULL;
}

void CXMLComposer::EnterNode(LPCSTR tag)
{
	EnteringNode(tag);
	EnteringNodeDone();
}

void CXMLComposer::EnteringNode(LPCSTR tag)
{
	ASSERT(!_EnteringNodeTag);

	Linebreak();
	Output('<');
	Output(tag);
	_EnteringNodeTag = tag;
}

void CXMLComposer::EnteringNodeDone()
{
	ASSERT(_EnteringNodeTag);
	Output('>');
	_TagCache& tag = m_NestLevelTag.push_back();
	tag.tag = _EnteringNodeTag;
	tag.tag_pos = GetDocumentLength();
	_EnteringNodeTag = NULL;
	_DepthMax = (UINT)m_NestLevelTag.GetSize();
}

void CXMLComposer::SetAttribute(LPCSTR name, long long value)
{
	char buf[40];
	_i64toa(value, buf, 10);
	SetAttribute(name, buf);
}

void CXMLComposer::SetAttribute(LPCSTR name, int value)
{
	char buf[20];
	itoa(value, buf, 10);
	SetAttribute(name, buf);
}

void CXMLComposer::SetAttribute(LPCSTR name, unsigned long long value)
{
	char buf[40];
	_ui64toa(value, buf, 10);
	SetAttribute(name, buf);
}

void CXMLComposer::SetAttribute(LPCSTR name, unsigned int value)
{
	SetAttribute(name, (unsigned long long)value);
}

void CXMLComposer::SetAttribute(LPCSTR name, float value)
{	
	char buf[40];
	sprintf(buf,"%f",value);
	SetAttribute(name, buf);
}

void CXMLComposer::SetAttribute(LPCSTR name, double value)
{
	char buf[40];
	sprintf(buf,"%lf",value);
	SetAttribute(name, buf);
}

void CXMLComposer::SetAttribute(LPCSTR name, LPCSTR value)
{
	ASSERT(_EnteringNodeTag);
	ASSERT(name);
	ASSERT(value);

	Output(' ');
	Output(name);
	Output('=');
	Output('"');
	_InsertPlainText(value);
	Output('"');
}

void CXMLComposer::AddCData(LPCSTR cdata)
{
	ASSERT(cdata);
	ASSERT(!_EnteringNodeTag);

	Linebreak();
	Output("<![CDATA[");
	Output(cdata);
	Output("]]>");
}

void CXMLComposer::AddText(LPCSTR text)
{	
	ASSERT(text);
	ASSERT(!_EnteringNodeTag);

	_InsertPlainText(text);
}


void CXMLComposer::_InsertPlainText(LPCSTR text)
{
	while(*text)
	{
		switch(*text)
		{
		case '<':	Output("&lt;");		break;
		case '&':	Output("&amp;");	break;
		case '>':	Output("&gt;");		break;
		case '\"':	Output("&quot;");	break;
		case '\'':	Output("&apos;");	break;
		default:	Output(*text);
		}
		text++;
	}
}

void CXMLComposer::ExitNode()
{	
	ASSERT(m_NestLevelTag.GetSize());
	UINT level = (UINT)(m_NestLevelTag.GetSize()-1);
	if(m_NestLevelTag[level].tag_pos == GetDocumentLength())
	{	//empty node, end with "/>"
		if(m_pStreamOut)
		{
			m_pStreamOut->Seek(-1,rt::_stream::Seek_Current);
			m_WrittenLength--;
			Output(' ');
			Output('/');
			Output('>');
		}
		else
		{
			m_Output += ' ';
			m_Output += '/';
			m_Output += '>';
		}
	}
	else
	{
		if(_DepthMax > m_NestLevelTag.GetSize())
			Linebreak(-1);

		Output('<');
		Output('/');
		Output(m_NestLevelTag[level].tag);
		Output('>');
	}
	m_NestLevelTag.pop_back();
}

void CXMLComposer::AppendTrail(LPCSTR text)
{	
	ASSERT(m_NestLevelTag.GetSize()==0);
	Output(text);
}

LPCSTR CXMLComposer::GetDocument() const
{
	ASSERT(m_pStreamOut == NULL);
	ASSERT(m_NestLevelTag.GetSize()==0);
	return m_Output;
}

UINT CXMLComposer::GetDocumentLength() const
{
	if(m_pStreamOut)
		return m_WrittenLength;
	else
		return (UINT)m_Output.GetLength();
}

void CXMLParser::ClearSyntaxError()
{
	m_XMLParseError = ERR_XML_OK;
	m_XMLParseErrorPosition = 0;
}

#pragma warning(disable:4355)

CXMLParser::CXMLParser():m_XPathParser(*this)
{
	SetUserTagFilter();
	m_pCurTagFilter = NULL;
	_attribute_cursor = NULL;
	_root_node_xml_start = NULL;
	m_bTrySkipError = FALSE;
	ClearSyntaxError();
}

CXMLParser::CXMLParser(const CXMLParser& xml):m_XPathParser(*this)
{
	ClearSyntaxError();
	_attribute_cursor = NULL;

	*this = xml;
}

#pragma warning(default:4355)

void CXMLParser::GetNodeDocument(rt::StringA& doc_out)
{
	LPCSTR p = _CurNode().OuterXML_Start;
	while(p > m_pDocument && (p[-1] == '\t' || p[-1] == ' ') )p--;

	LPCSTR pend;
	if(_CurNode().IsCompactNode)
		pend = _meta_::_seek_tag_close(_CurNode().OuterXML_Start);
	else
		pend = _search_node_close(_CurNode().InnerXML_Start,_CurNode().TagName);

	doc_out = rt::StringA_Ref(CXMLComposer::g_XML_Header,sizeof(CXMLComposer::g_XML_Header)-1) + 
			  '\r' + '\n' + rt::StringA_Ref(p, pend + 1);
}

CXMLParser CXMLParser::GetNodeDocument(int nth_parent) const
{
	CXMLParser ret;
	ret.SetUserTagFilter(m_pUserTagFilter);
	ret.m_pCurTagFilter = m_pUserTagFilter;

	ASSERT(nth_parent < (int)m_NodePath.GetSize());
	const _node& out = m_NodePath[m_NodePath.GetSize() - 1 - nth_parent];

	ret._root_node_xml_start = out.OuterXML_Start;
	if(out.IsCompactNode)
		ret._root_node_xml_end = _meta_::_seek_tag_close(out.OuterXML_Start);
	else
		ret._root_node_xml_end = _search_node_close(out.InnerXML_Start,out.TagName);

	ret.m_pDocument = out.OuterXML_Start;
	ret.m_NodePath.SetSize(1);
	ret.m_NodePath[0] = out;

	return ret;
}


const CXMLParser& CXMLParser::operator = (const CXMLParser& xml)
{
	_content_copy.Empty();
	ClearSyntaxError();

	LONGLONG ptr_shift = 0;
	if(!xml._content_copy.IsEmpty())
	{	_content_copy = xml._content_copy;
		ptr_shift = _content_copy.Begin() - xml._content_copy.Begin();
	}

	_attribute_cursor = NULL;
	_root_node_xml_start = xml._root_node_xml_start + ptr_shift;

	__static_if(sizeof(LPVOID) == 4)	// x86
	{
		if(xml._root_node_xml_end == (LPCSTR)0xffffffff)
			_root_node_xml_end = (LPCSTR)0xffffffff;
		else
			_root_node_xml_end = xml._root_node_xml_end + ptr_shift;
	}

	__static_if(sizeof(LPVOID) == 8)	// x64
	{
		if(xml._root_node_xml_end == (LPCSTR)0xffffffffffffffff)
			_root_node_xml_end = (LPCSTR)0xffffffffffffffff;
		else
			_root_node_xml_end = xml._root_node_xml_end + ptr_shift;
	}
	
	SetUserTagFilter(xml.m_pUserTagFilter);
	if(xml.m_pCurTagFilter == &xml.m_XPathParser)
	{
		m_XPathParser._pXPath = NULL;
		m_XPathParser.m_bRelativePath = xml.m_XPathParser.m_bRelativePath;
		m_XPathParser.m_bIncludeDescendants = xml.m_XPathParser.m_bIncludeDescendants;
		m_XPathParser.m_FinalQualifier = xml.m_XPathParser.m_FinalQualifier;
		m_XPathParser.m_UpLevelCount = xml.m_XPathParser.m_UpLevelCount;
		m_XPathParser.m_QualifierShifts = xml.m_XPathParser.m_QualifierShifts;

		m_XPathParser.m_Qualifiers.SetSizeAs(xml.m_XPathParser.m_Qualifiers);
		m_XPathParser.m_Qualifiers = xml.m_XPathParser.m_Qualifiers;

		m_pCurTagFilter = &m_XPathParser;
	}
	else
		m_pCurTagFilter = xml.m_pCurTagFilter;

	m_pDocument = xml.m_pDocument + ptr_shift;
	m_NodePath.SetSizeAs(xml.m_NodePath);
	for(UINT i=0;i<m_NodePath.GetSize();i++)
	{
		if(!xml.m_NodePath[i].TagName.IsEmpty())
			m_NodePath[i].TagName = rt::StringA_Ref(xml.m_NodePath[i].TagName.Begin() + ptr_shift, xml.m_NodePath[i].TagName.GetLength());
		if(!xml.m_NodePath[i].Attributes.IsEmpty())
			m_NodePath[i].Attributes = rt::StringA_Ref(xml.m_NodePath[i].Attributes.Begin() + ptr_shift, xml.m_NodePath[i].Attributes.GetLength());
		m_NodePath[i].OuterXML_Start = xml.m_NodePath[i].OuterXML_Start + ptr_shift;
		m_NodePath[i].InnerXML_Start = xml.m_NodePath[i].InnerXML_Start + ptr_shift;
		m_NodePath[i].IsCompactNode = xml.m_NodePath[i].IsCompactNode;
	}

	return xml;
}

LPCSTR CXMLParser::SetLastSyntaxError(XMLParseError errnum, LPCSTR pos)
{
	m_XMLParseError = errnum;
	if(pos)
	{
		ASSERT(pos >= m_pDocument);
		m_XMLParseErrorPosition = (UINT)(pos - m_pDocument);
	}
	else
		m_XMLParseErrorPosition = 0;

	ASSERT(0);
	//_CheckDump("XML Parse Error: "<<errnum<<'\n');
	//_asm int 3
	return pos;
}

LPCSTR CXMLParser::_search_control_close(LPCSTR start) const
{
	int enclosure = 0;
	for(;*start;start++)
	{
		if(*start != '<' && *start != '>'){}
		else
		{	enclosure -= *start - 0x3d;	// '<' = \x3c, '>' = \x3e
			if(enclosure == -1)
				return start;
		}
	}

	return NULL;
}

LPCSTR CXMLParser::_html_check_node_close(const rt::StringA_Ref& tagname, LPCSTR p, BOOL just_next)
{
SEARCH_AGAIN:
	LPCSTR c = strstr(p,"</");

	if(c)
	{	c+=2;

		UINT i=0;
		for(;i<tagname.GetLength();i++)
		{
			if(	tagname[i] == c[i] ||
				tagname[i] == c[i] - 'A' + 'a'
			){}
			else
			{
				if(!just_next)
				{
					p = c+2;
					goto SEARCH_AGAIN;
				}
				else
					return NULL;
			}
		}

		if(c[i] == '>' || c[i] <=' ')
			return c-2;
	}

	return NULL;
}


LPCSTR CXMLParser::_search_special_node_close(LPCSTR start) const
{
	ASSERT(*start == '<'); // must pointing to outer xml
	// special nodes
	LPCSTR ending_symbol;
	int ending_symbol_length;
	LPCSTR start_pos;
	XMLParseError errnum;

	if( *((DWORD*)(start)) == 0x2d2d213c )
	{	// <!-- , comments
		start_pos = start + 4;
		ending_symbol = "-->";
		ending_symbol_length = 3;
		errnum = ERR_COMMENT_CLOSURE_NOT_MATCHED;
	}
	else if(*((WORD*)(start)) == 0x3f3c)
	{	// <?, processing instruction
		start_pos = start + 2;
		ending_symbol = "?>";
		ending_symbol_length = 2;
		errnum = ERR_PROC_INST_CLOSURE_NOT_MATCHED;
	}
	else if(*((DWORD*)(start+1)) == 0x44435b21 && *((DWORD*)(start+5)) == 0x5b415441)
	{	// <![CDATA
		start_pos = start + 9;
		ending_symbol = "]]>";
		ending_symbol_length = 3;
		errnum = ERR_CDATA_CLOSURE_NOT_MATCHED;
	}
	else
	{	// <! 
		ASSERT(start[1] == '!');
		LPCSTR p = start + 1;
		for(;*p && *p!='>' && *p!='[';p++)
			if(*p == '<')
			{	rt::_CastToNonconst(this)->SetLastSyntaxError(ERR_XML_SYMBOL_UNEXPECTED,p);
				return NULL;
			}

		if(*p)
		{	if(*p == '>'){ return p; }	// compact <!
			else
			{	ASSERT(*p == '[');
				start_pos = start + 2;
				ending_symbol = "]>";
				ending_symbol_length = 2;
				errnum = ERR_PROC_INST_CLOSURE_NOT_MATCHED;
			}
		}
		else
		{	rt::_CastToNonconst(this)->SetLastSyntaxError(ERR_XML_UNEXPECTED_ENDOFFILE,start);
			return NULL;
		}
	}

	LPCSTR p = strstr(start_pos,ending_symbol);
	if(p){ return p + ending_symbol_length - 1; }
	else
	{	rt::_CastToNonconst(this)->SetLastSyntaxError(errnum,start);
		return NULL;
	}

	ASSERT(0);
}

void _meta_::_convert_xml_to_text(const rt::StringA_Ref& string, rt::StringA& text)
{
	VERIFY(text.SetLength(string.GetLength()));
	LPCSTR p = string.Begin();
	LPSTR d = text.Begin();
	LPCSTR pend = string.Begin() + string.GetLength();

	while(p<pend)
	{
		LPCSTR s;
		for(s=p;s<pend && *s!='&';s++);

		int len = (int)(s - p);
		memcpy(d,p,len);
		d += len;

		if(*s == '&')
		{	if(s[1] == '#')
			{	
				if(s[2]>='0' && s[2]<='9')
				{
					s+=2;
					int a = 0;
					while(*s>='0' && *s<='9')
					{	a = a*10 + (*s - '0');
						s++;
					}
					if(*s==';')s++;
					*d = (char)a;
					d++;
				}
				else
				{	d[0] = '&';	d[1] = '#';
					d+=2;
					s+=2;
				}
			}
			else
			{
				switch(*((DWORD*)s))
				{	
				case 0x3b746c26: *d = '<'; d++; s+=4; break;
				case 0x706d6126: *d = '&'; d++; s+=5; break;
				case 0x3b746726: *d = '>'; d++; s+=4; break;
				case 0x6f757126: *d = '\"'; d++; s+=6; break;
				case 0x6f706126: *d = '\''; d++; s+=5; break;
				default: *d = *s; d++; s++;
				}
			}
		}
		else break;

		p = s;
	}

	text.SetLength((UINT)(d - text.Begin()));
}

/*
LPCSTR CXMLParser::_search_node_close(LPCSTR start_inner, const rt::StringA_Ref& tag_name, LPCSTR* pInnerXML_End) const
{
	int enclosure = 1;
	rt::StringA_Ref tagname_stack[_SEARCH_DEPTH_MAX];
	tagname_stack[0] = tag_name;

	LPCSTR start = start_inner;
	while(start = _meta_::_seek_tag_open(start))
	{
		if(start[1] != '!' && start[1] != '?')
		{	LPCSTR pend = _meta_::_seek_tag_close(start+1);
			if(pend)
			{	if(start[1] == '/')
				{	if(pend[-1] == '/' && !m_bTrySkipError)
					{	rt::_CastToNonconst(this)->SetLastSyntaxError(ERR_XML_SYMBOL_UNEXPECTED,pend-1);
						return NULL;
					}
					// check tag match
					LPCSTR p = _meta_::_skip_whitespace(start+2);
					rt::StringA_Ref tag(p,_meta_::_seek_symbol_end(p));
					int encls = enclosure;
					int encls_stop = max(0,enclosure - _SEARCH_FAULT_TOLERANCE_SKIP_DEPTH_MAX);
					while(encls>encls_stop)
					{	encls--;
						if(tag==tagname_stack[encls])
						{	enclosure = encls;
							break;
						}
						if(!m_bTrySkipError)
						{	rt::_CastToNonconst(this)->SetLastSyntaxError(ERR_NODE_CLOSURE_NOT_MATCHED,pend-1);
							return NULL;
						}
					}
					if(enclosure == 0)	// get it
					{	if(pInnerXML_End)*pInnerXML_End = start;
						return pend;
					}
				}
				else if(pend[-1] != '/')
				{	
					ASSERT(enclosure>=0);
					if(enclosure<_SEARCH_DEPTH_MAX)
					{	LPCSTR p = _meta_::_skip_whitespace(start+1);
						tagname_stack[enclosure] = rt::StringA_Ref(p,_meta_::_seek_symbol_end(p));	}
					else
					{	rt::_CastToNonconst(this)->SetLastSyntaxError(ERR_XML_DEPTH_EXCEEDED,pend-1);
						return NULL;
					}
					enclosure++;					
				}

				start = pend + 1;
				continue;
			}
			else
			{	rt::_CastToNonconst(this)->SetLastSyntaxError(ERR_TAG_CLOSURE_NOT_MATCHED,start);
				return NULL;
			}
		}
		else
		{	
			LPCSTR p = _search_special_node_close(start);
			if(!p)return NULL;
			start = p + 1;
		}
	}

	rt::_CastToNonconst(this)->SetLastSyntaxError(ERR_XML_UNEXPECTED_ENDOFFILE,start_inner);
	return NULL;
}
*/

LPCSTR CXMLParser::_search_node_close(LPCSTR start_inner, const rt::StringA_Ref& tag_name, LPCSTR* pInnerXML_End) const
{
	LPCSTR start = start_inner;
	int enclosure = 0;
	while(start = _meta_::_seek_tag_open(start))
	{
		if(start[1] != '!' && start[1] != '?')
		{	LPCSTR pend = _meta_::_seek_tag_close(start+1);
			if(pend)
			{	if(start[1] == '/')
				{	if(pend[-1] == '/')
					{	rt::_CastToNonconst(this)->SetLastSyntaxError(ERR_XML_SYMBOL_UNEXPECTED,pend-1);
						return NULL;
					}
					enclosure--;
					if(enclosure == -1)
					{	// get it
						if(memcmp(tag_name.Begin(),start+2,tag_name.GetLength()) == 0)
						{
							if(pInnerXML_End)*pInnerXML_End = start;
							return pend;
						}
					}
				}
				else if(pend[-1] != '/')
				{	enclosure++;
				}

				start = pend + 1;
				continue;
			}
			else
			{	rt::_CastToNonconst(this)->SetLastSyntaxError(ERR_TAG_CLOSURE_NOT_MATCHED,start);
				return NULL;
			}
		}
		else
		{	
			LPCSTR p = _search_special_node_close(start);
			if(!p)return NULL;
			start = p + 1;
		}
	}

	rt::_CastToNonconst(this)->SetLastSyntaxError(ERR_XML_UNEXPECTED_ENDOFFILE,start_inner);
	return NULL;
}

LPCSTR CXMLParser::_seek_next_attrib(LPCSTR start, rt::StringA_Ref& attrib_name, rt::StringA_Ref& value) const
{
	for(;*start && *start != '>' && !(start[0] == '/' && start[1] == '>');start++)
	{
		if(start[-1] <'\x2d' && (start[0] >='\x2d' || start[0]<0))
		{	
			// determine attribute name
			LPCSTR _String = start;
			start = _meta_::_seek_symbol_end(start);
			UINT _Length = (UINT)(start - _String);

			attrib_name = rt::StringA_Ref(_String,_Length);

			if(attrib_name.IsEmpty())
			{	rt::_CastToNonconst(this)->SetLastSyntaxError(ERR_ATTRIBUTE_NAME_NOT_FOUND,start);
				break;
			}
			
			// seek '='
			LPCSTR p = strchr(start, '=');
			if(p == NULL)
			{	rt::_CastToNonconst(this)->SetLastSyntaxError(ERR_ATTRIBUTE_EQUALSIGN_NOT_FOUND,start);
				break;
			}

			start = p+1;
			while(*start && *start != '>')
			{
				if(*start == '"' || *start == '\'')
				{
					LPCSTR _String = start+1;
					p = strchr(start+1, *start);
					if(p == NULL)
					{	rt::_CastToNonconst(this)->SetLastSyntaxError(ERR_ATTRIBUTE_QUOTATION_NOT_MATCHED,start);
						break;
					}
					UINT _Length = (UINT)(p - start - 1);

					value = rt::StringA_Ref(_String, _Length);
					return p+1;
				}
				start++;
			}
		}
	}

	attrib_name.Empty();
	value.Empty();
	return NULL;
}

BOOL CXMLParser::_search_attrib(LPCSTR start, LPCSTR attrib_name,  rt::StringA_Ref& value) const
{
	rt::StringA_Ref name;
		
	while((start = _seek_next_attrib(start,name,value)) && !name.IsEmpty())
	{
		if(	name == attrib_name)
			return TRUE;
	}

	value.Empty();
	return FALSE;
}

ULONGLONG CXMLParser::GetNodeDocumentOffset() const
{
	return (ULONGLONG)(_CurNode().OuterXML_Start - m_pDocument);
}

BOOL CXMLParser::_EnterNextNode(LPCSTR start, _meta_::_XML_Tag_Filter* pFilter, BOOL replace_node)
{
	rt::StringA_Ref tag;
	LPCSTR ptag, patt, pend;
	int descendant_offset = !replace_node;

	if(start >= _root_node_xml_end)return FALSE;

	while((ptag = _meta_::_seek_tag_open(start)) && ptag[1]!='/')
	{
		if(ptag >= _root_node_xml_end)return FALSE;

		if(ptag[1] == '!' || ptag[1] == '?')
		{
			ptag = _search_special_node_close(ptag);
			if(ptag == NULL)return FALSE;
			start = ptag + 1;
			continue;
		}

		LPCSTR _String = ptag+1;
		patt = _meta_::_seek_symbol_end(ptag+1);
		ASSERT(patt);
		UINT _Length = (UINT)(patt - _String);

		tag = rt::StringA_Ref(_String,_Length);

		if(tag.IsEmpty())
		{
			SetLastSyntaxError(ERR_TAG_NAME_NOT_FOUND,ptag+1);
			return FALSE;
		}
		
		pend = _meta_::_seek_tag_close(patt);
		if(pend == NULL)
		{
			SetLastSyntaxError(ERR_TAG_CLOSURE_NOT_MATCHED,ptag);
			return FALSE;
		}

		if(pFilter && !pFilter->_TagTest(tag,patt,GetDescendantLevel()+descendant_offset))
		{	// skip this node
			if(pend[-1] == '/') // compact node
			{	start = pend + 1; continue;	}
			else
			{	start = _search_node_close(pend + 1, tag);
				if(start){ start++; continue; }
				else
				{	SetLastSyntaxError(ERR_NODE_CLOSURE_NOT_MATCHED,ptag);
					return FALSE;
				}
			}
		}
		else
		{	// enter this node
			_node& node = replace_node?m_NodePath[m_NodePath.GetSize()-1]:m_NodePath.push_back();

			node.InnerXML_Start = pend+1;
			node.OuterXML_Start = ptag;
			node.TagName = tag;
			if(pend[-1] == '/')
			{
				node.Attributes = rt::StringA_Ref(patt, (UINT)(pend - patt) - 1);
				node.IsCompactNode = TRUE;
			}
			else
			{
				node.Attributes = rt::StringA_Ref(patt, (UINT)(pend - patt));
				node.IsCompactNode = FALSE;
			}
			node.Attributes.Trim();
			return TRUE;
		}
	}

	return FALSE;
}

BOOL CXMLParser::Load(LPCTSTR fn)
{
	w32::CFile64 file;
	return	file.Open(fn) &&
			_content_copy.SetLength((SIZE_T)file.GetLength()) &&
			file.Read(_content_copy.GetBuffer(),_content_copy.GetLength()) == (SIZE_T)_content_copy.GetLength() &&
			Load(_content_copy,TRUE,_content_copy.GetLength());
}

void CXMLParser::Clear()
{
	_content_copy.Empty();
	_attribute_cursor = _root_node_xml_start = _root_node_xml_end = NULL;
	m_XMLParseError = ERR_XML_OK;
	m_XMLParseErrorPosition = 0;
	ClearXPathFilter();
	m_pDocument = NULL;
	m_NodePath.ChangeSize(0);
}

BOOL CXMLParser::_ForceWellformed(rt::StringA& xml)
{
	struct _tag
	{	rt::StringA_Ref		tagname;
		LPSTR				tagclose;
	};
	rt::BufferEx<_tag>		tagname_stack;

	BOOL ret = TRUE;

	LPSTR start = xml.Begin();
	while(start = (LPSTR)_meta_::_seek_tag_open(start))
	{
		if(start[1] != '!' && start[1] != '?')
		{	
			LPSTR pend = (LPSTR)_meta_::_seek_tag_close(start+1);
			if(pend && start+1<pend)
			{	if(start[1] == '/')
				{	// close tag
					if(pend[-1] == '/'){ pend[-1] = ' '; ret = FALSE; }
					// check tag match
					LPCSTR p = _meta_::_skip_whitespace(start+2);
					LPCSTR tagend = _meta_::_seek_symbol_end(p);
					ASSERT(tagend);
					rt::StringA_Ref tag(p,tagend);
					int encls = tagname_stack.GetSize();
					while(encls>0)
					{	encls--;
						if(tag==tagname_stack[encls].tagname)
						{	int sz = tagname_stack.GetSize();
							// earse all unmatched open tag
							for(int i=encls+1;i<sz;i++)
							{	
								ret = FALSE;
								LPSTR h = tagname_stack[i].tagname.Begin();
								while(*h!='<')h--;
								*h = ' ';
								ASSERT(*tagname_stack[i].tagclose == '>');
								*tagname_stack[i].tagclose = ' ';
								if(tagname_stack[i].tagname.GetLength() <= 2)
									memset(tagname_stack[i].tagname.Begin(),' ',tagname_stack[i].tagname.GetLength());
							}
							tagname_stack.erase(encls,sz);
							goto NEXT_TAG;
						}
					}
					// earse the close tag
					memset(start,' ',tagend - start+1);
				}
				else if(pend[-1] != '/')
				{	// open tag
					LPCSTR p = _meta_::_skip_whitespace(start+1);
					_tag& t = tagname_stack.push_back();
					t.tagname = rt::StringA_Ref(p,_meta_::_seek_symbol_end(p));
					t.tagclose = pend;
				}
NEXT_TAG:
				start = pend + 1;
				continue;
			}
		}
		else
		{	
			LPSTR p = (LPSTR)_search_special_node_close(start);
			if(p)
			{	start = p + 1;
				continue;
			}
		}

		ret = FALSE;
		*start++ = ' ';	// earse the open tag
	}

	if(tagname_stack.GetSize())
	{
		ret = FALSE;
		for(int i=tagname_stack.GetSize()-1;i>=0;i--)
		{	xml += '<';
			xml += '/';
			xml += tagname_stack[i].tagname;
			xml += '>';
		}
	}

	return ret;
}

BOOL CXMLParser::LoadHTML(LPCSTR in, UINT len)
{
	static const rt::StringA_Ref _szlt("&lt;",4);
	static const rt::StringA_Ref _szgt("&gt;",4);
	static const rt::StringA_Ref _szquot("&quot;",6);
	static const rt::StringA_Ref _szapos("&apos;",6);

	struct _ect 
	{	static BOOL check(DWORD tagid)
		{	return 	tagid == MAKE_FOURCC(b,a,s,e)
				||	tagid == MAKE_FOURCC(m,e,t,a)
				||	tagid == MAKE_FOURCC(l,i,n,k)
				||	tagid == MAKE_FOURCC(a,r,e,a)
				||	tagid == MAKE_FOURCC3(r,e,l)
				||	tagid == MAKE_FOURCC3(i,m,g)
				||	tagid == MAKE_FOURCC2(h,r)
				||	tagid == MAKE_FOURCC2(b,r);
		}	
		static BOOL becompact(rt::StringA_Ref& tagname)
		{
			return (tagname.GetLength()<=4 && _ect::check((*((DWORD*)tagname.Begin())) & (0xffffffffU >> (8*(4 - tagname.GetLength()))))) ||
					tagname == rt::StringA_Ref("input",5) ||
					tagname == rt::StringA_Ref("button",6);
		}
		static void appendXHTML(rt::StringA& out, const rt::StringA_Ref& in)
		{	LPCSTR open = in.Begin();
			LPCSTR p = in.Begin();
			LPCSTR end = in.End();
			rt::StringA_Ref	sztag;
			for(;;p++)
			{	
				if(p==end){}
				else if(*p == '<'){ sztag = _szlt; }
				else if(*p == '>'){ sztag = _szgt; }
				else if(*p == '"'){ sztag = _szquot; }
				else if(*p == '\''){ sztag = _szapos; }
				else continue;

				if(open!=p)out += rt::StringA_Ref(open,p);
				if(p==end)break;
				open = p+1;
				out += sztag;
			}
		}
	};

	static const rt::StringA_Ref _szsslash(" /",2);

	if(len == INFINITE)len = strlen(in);
	rt::StringA input;
	{	input.SetLength(len);
		LPSTR p = input.Begin();
		LPCSTR inend = in + len;
		for(;in < inend;in++)
		{
			if(*in>=' ' || *in<0 || *in=='\t')
			{	*p++ = *in;	}
			else
			{
				p++; p--; 
			}
		}
		len = p - input.Begin();
		input.SetLength(len);
	}
	
	_content_copy.SetLength((UINT)(len*1.2f + 100));
	_content_copy.SetLength(0);

	LPSTR p = input.Begin();
	LPSTR pend = p+len;
	LPSTR open;
	
	while(p = strchr(open = p, '<'))
	{
START_OF_CLOSURE_PARSE:
		ASSERT(*p = '<');

		/////////////////////////////////////////////////////////
		// special nodes
		if(p[1] == '!')
		{
			if(p[2] == '-' && p[3] == '-')	// comment
			{	
				p = strstr(p+3, "-->");
				if(p)
				{	p += 3;
					continue;
				}
				else
				{	SetLastSyntaxError(ERR_HTML_COMMENT_CLOSURE_NOT_MATCHED, NULL);
					return FALSE;
				}
			}
			else
			{	p = strchr(p+1,'>');
				p++;
				_content_copy += rt::StringA_Ref(open,p);
				continue;
			}
		}
		// text in between
		if(open != p)
		{	_ect::appendXHTML(_content_copy,rt::StringA_Ref(open,p));
			open = p;
		}

		int orgoutlen = _content_copy.GetLength();	// restart closure parsing for not well formated tag

		//////////////////////////////////////////////////
		// some tag
		LPSTR tag = _meta_::_skip_whitespace(p+1);
		BOOL closingtag;
		if(*tag == '/')
		{	
			closingtag = TRUE;
			tag = _meta_::_skip_whitespace(tag + 1);
		}
		else
		{	closingtag = FALSE;
		}

		LPSTR tagend = _meta_::_seek_symbol_end(tag);
		if(!tagend)
		{	SetLastSyntaxError(ERR_HTML_TAG_CLOSURE_NOT_MATCHED, NULL);
			return FALSE;
		}

		if(tagend == tag)
		{
			if(m_bTrySkipError){ p++; continue; }
			else
			{	SetLastSyntaxError(ERR_HTML_TAG_NAME_NOT_FOUND, NULL);
				return FALSE;
			}
		}

		rt::StringA_Ref tagname(tag,tagend);
		tagname.MakeLower();

		BOOL shouldbecompact;
		if((shouldbecompact = _ect::becompact(tagname)) && closingtag)
		{	// skip this node
			p = strchr(tagend,'>');
			if(*p)p++;
			continue;
		}
		_content_copy += rt::StringA_Ref("</",1+(int)closingtag);
		_content_copy += tagname;

		if(closingtag)
		{	LPCSTR cl = _meta_::_skip_whitespace(tagname.End());
			if(*cl == '>'){ cl++; }
			else
			{	if(!m_bTrySkipError)
				{	SetLastSyntaxError(ERR_HTML_TAG_CLOSURE_NOT_MATCHED, NULL);
					return FALSE;
				}
			}
			p = (LPSTR)cl;
			_content_copy += '>';
			continue;
		}
		else // clear attributes until the closure
		{	p = tagend;
			for(;;)
			{	
				p = _meta_::_skip_whitespace(p);

				if(*p == '<')		// not wellformed
				{	_content_copy.SetLength(orgoutlen);	// remove content has been processed
					goto START_OF_CLOSURE_PARSE;
				}
				else if(*p == '>')	// reach the end of tag
				{	// enforce compact tag
					if(shouldbecompact)
					{	_content_copy += _szsslash;
					}
					else
					if(tagname == rt::StringA_Ref("script",6) || tagname == rt::StringA_Ref("style",5))
					{	//make all content CDATA
						_content_copy += rt::StringA_Ref("><![CDATA[",10);
						LPCSTR cl = _html_check_node_close(tagname, p+1, FALSE);
						if(cl)
						{	_content_copy += rt::StringA_Ref(p+1,cl);
							_content_copy += rt::StringA_Ref("]]></",5);
							_content_copy += tagname;
							p = (LPSTR)_meta_::_skip_whitespace(cl+1+tagname.GetLength()+1);
							if(*p != '>')
							{	SetLastSyntaxError(ERR_HTML_NODE_CLOSURE_NOT_MATCHED, NULL);
								return FALSE;
							}
						}
						else
						{	SetLastSyntaxError(ERR_HTML_NODE_CLOSURE_NOT_MATCHED, NULL);
							return FALSE;
						}
					}
					goto GO_NEXT;
				}
				else if(p[0] == '/' && p[1] == '>')
				{	_content_copy += _szsslash;
					goto GO_NEXT;
				}

				// start of an attribute
				_content_copy += ' ';

				LPSTR patt = p;
				LPSTR pattend = _meta_::_seek_symbol_end(patt);

				if(patt == pattend)
				{
					if(m_bTrySkipError){ p++; continue; }
					else
					{	SetLastSyntaxError(ERR_HTML_ATTRIBUTE_NAME_NOT_FOUND, NULL);
						return FALSE;
					}
				}
				
				rt::StringA_Ref att(patt, pattend);
				
				att.MakeLower();
				_content_copy += att;

				p = _meta_::_skip_whitespace(pattend);
				if(*p != '=')
				{	// attribute without value
					_content_copy += rt::StringA_Ref("=\"\"",3);
					continue;
				}
				_content_copy += '=';

				p = _meta_::_skip_whitespace(p+1);
				if(*p == '"' || *p == '\'')
				{	int qc = *p;
					_content_copy += '"';
					LPSTR end = strchr(p+1,*p);
					_ect::appendXHTML(_content_copy,rt::StringA_Ref(p+1,end));
					_content_copy += '"';
					p = end+1;
				}
				else
				{	_content_copy += '"';
					tagend = _meta_::_seek_symbol_end(p);
					_content_copy += rt::StringA_Ref(p,tagend);
					_content_copy += '"';
					p = tagend;
				}
			}
		}
GO_NEXT:
		_content_copy += '>';
		p++;
	}

	_ForceWellformed(_content_copy);
	return Load(_content_copy,TRUE,_content_copy.GetLength());
}

BOOL CXMLParser::Load(LPCSTR text, BOOL Keep_referring, int len)
{
	if(Keep_referring)
	{	m_pDocument = text;	}
	else
	{	_content_copy = text;
		m_pDocument = _content_copy;
	}

	if(len < 0)
	{
		__static_if(sizeof(LPCVOID) == 4)	// x86
		{	_root_node_xml_end = (LPCSTR)0xfffffffff;	}
		__static_if(sizeof(LPCVOID) == 8)	// x64
		{	_root_node_xml_end = (LPCSTR)0xffffffffffffffff;	}
	}
	else
		_root_node_xml_end = m_pDocument + len;

	ASSERT(text);
	_attribute_cursor = NULL;
	m_pCurTagFilter = NULL;
	ClearSyntaxError();


	m_NodePath.SetSize();

	// parse header
//	LPCSTR pxml, pend, perr;

	//if( (pxml = _meta_::_seek_tag_open(perr = m_pDocument)) &&
	//	(*((DWORD*)(perr = pxml+1)) == 0x6c6d783f) &&	// ?xml
	//	(pend = _meta_::_seek_tag_close(perr = pxml+5)) &&
	//	(pend[-1] == '?')
	//)
	{	//LPCSTR start = pend+1;
		LPCSTR start = m_pDocument;
		while(start = _meta_::_seek_tag_open(start))
		{
			if(start[1] == '!' || start[1] == '?')
			{
				start = _search_special_node_close(start);
				if(start){ start++; }
				else return FALSE;
			}
			else break;
		}
		
		_root_node_xml_start = start;
		return _EnterNextNode(start);
	}
//	else SetLastSyntaxError(ERR_XML_HEADER_BAD_FORMAT,perr);

	return FALSE;
}

void CXMLParser::EnterRootNode()
{
	_attribute_cursor = NULL;
	ASSERT(m_NodePath.GetSize() > 0);
	m_NodePath.ChangeSize(1);
	m_pCurTagFilter = m_pUserTagFilter;
}

BOOL CXMLParser::EnterParentNode()
{
	_attribute_cursor = NULL;
	if(GetDescendantLevel()>1)
	{
		m_NodePath.ChangeSize(m_NodePath.GetSize()-1);
		return TRUE;
	}
	else
		return FALSE;
}

BOOL CXMLParser::EnterNextSiblingNode()
{
	if(m_NodePath.GetSize())
	{
		_attribute_cursor = NULL;
		LPCSTR close = _CurNode().IsCompactNode?(_CurNode().InnerXML_Start-1):
												_search_node_close(_CurNode().InnerXML_Start,_CurNode().TagName);

		return close && _EnterNextNode(close+1, m_pCurTagFilter, TRUE);
	}
	else return FALSE;
}

BOOL CXMLParser::EnterFirstChildNode(LPCSTR tag_name)
{
	if( m_NodePath.GetSize() && _CurNode().IsCompactNode)return FALSE;

	_attribute_cursor = NULL;
	_meta_::_XML_Tag_Filter* pfilter;

	struct _name_filter: public _meta_::_XML_Tag_Filter
	{	
		_XML_Tag_Filter*	CurFilter;
		LPCSTR				Name;

		virtual BOOL _TagTest(const rt::StringA_Ref& tag_name, LPCSTR attributes, int DescendantLevel)
		{
			if(tag_name!=Name )return FALSE;
			if(CurFilter)return CurFilter->_TagTest(tag_name, attributes,DescendantLevel);
			return TRUE;
		}
	};

	_name_filter filter;

	if(tag_name)
	{
		filter.Name = tag_name;
		filter.CurFilter = m_pCurTagFilter;
		
		pfilter = &filter;
	}
	else
		pfilter = m_pCurTagFilter;

	if(m_NodePath.GetSize())
		return _EnterNextNode(_CurNode().InnerXML_Start,pfilter);
	else
		return _EnterNextNode(_root_node_xml_start,pfilter);
}

void CXMLParser::ClearXPathFilter()
{
	m_pCurTagFilter = m_pUserTagFilter;
	m_XPathParser.Clear();
}

void CXMLParser::EscapeXPath()				// call this to remove XPath filtering
{
	ClearXPathFilter();
}

BOOL CXMLParser::EnterXPathByCSSPath(LPCSTR css_path)
{
	rt::StringA xpath;
	LPCSTR p = w32::_meta_::_skip_whitespace(css_path);

	while(*p)
	{
		LPCSTR tagend = w32::_meta_::_seek_symbol_end(p);

		rt::StringA_Ref tag(p,tagend);

		int off;
		int olen = xpath.GetLength();
		if((off = tag.FindCharactor('#')) > 0)
		{	LPCSTR idtag = "[@id='";
			int cls = tag.FindCharactor('.');
			if(cls>0)
			{	
				if(cls < off)
				{	xpath += '/' + rt::StringA_Ref(tag.Begin(),&tag[cls]) + idtag + rt::StringA_Ref(&tag[off+1],tag.End()) + '\'' + ']';
					off = cls;
				}
				else
					xpath += '/' + rt::StringA_Ref(tag.Begin(),&tag[off]) + idtag + rt::StringA_Ref(&tag[off+1],&tag[cls]) + '\'' + ']';
			}
			else
				xpath += '/' + rt::StringA_Ref(tag.Begin(),&tag[off]) + idtag + rt::StringA_Ref(&tag[off+1],tag.End()) + '\'' + ']';
		}
		else
		if((off = tag.FindCharactor('.')) > 0)
		{
			xpath += '/' + rt::StringA_Ref(tag.Begin(),&tag[off]) + "[@class:='" + rt::StringA_Ref(&tag[off+1],tag.End()) + '\'' + ']';
		}
		else
		{	off = tag.GetLength();
			xpath += '/' + tag;
		}

		xpath.Truncate(olen+1,off).MakeLower();
		p = w32::_meta_::_skip_whitespace(tagend);
	}

	return EnterXPath(xpath);
}

BOOL CXMLParser::EnterXPath(LPCSTR xpath)
{
	_attribute_cursor = NULL;
	ClearXPathFilter();
	ClearSyntaxError();

	if(m_XPathParser.Load(xpath))
	{
		if(m_XPathParser.m_bRelativePath)
		{	// translate to fixed path
			if(m_XPathParser.m_UpLevelCount == GetDescendantLevel())
			{	m_XPathParser.m_UpLevelCount = 0;
				m_XPathParser.m_QualifierShifts = 0;
				m_XPathParser.m_bRelativePath = FALSE;
				m_NodePath.SetSize();
			}
			else
			{
				while(m_XPathParser.m_UpLevelCount > 0)
				{	m_XPathParser.m_UpLevelCount--;
					if(!EnterParentNode())goto XPATH_ERROR;
				}
				m_XPathParser.m_QualifierShifts = GetDescendantLevel();
				_root_node_xml_start = _CurNode().OuterXML_Start;
			}
		}
		else
		{	m_XPathParser.m_QualifierShifts = 0;
			m_NodePath.SetSize();
		}

		m_pCurTagFilter = &m_XPathParser;
		return EnterSucceedNode();
	}
	
	return FALSE;
XPATH_ERROR:
	EnterRootNode();

	return FALSE;
}

void CXPathParser::_ClearLastOrdinalTestPast(int level)
{
	if(level < (int)m_Qualifiers.GetSize())
		m_Qualifiers[level]._LastOrdinalTestPast = 0;
	else
		m_FinalQualifier._LastOrdinalTestPast = 0;
}

BOOL CXPathParser::_TagTest(const rt::StringA_Ref& tag_name, LPCSTR attributes, int DescendantLevel)
{
	BOOL ret = FALSE;
	ASSERT(DescendantLevel > m_QualifierShifts);
	_Qualifier* pQ = NULL;
	if(DescendantLevel <= (int)(m_QualifierShifts + m_Qualifiers.GetSize()))
	{	
		_ClearLastOrdinalTestPast(DescendantLevel - m_QualifierShifts); // clear _LastOrdinalTestPast
		// stepping into path halfway
		_Qualifier& qu = m_Qualifiers[DescendantLevel - m_QualifierShifts - 1];

		if(qu.qt_Ordinal>=0 && qu.qt_Ordinal<qu._LastOrdinalTestPast)
			return FALSE;

		pQ = &qu;
		_LastNodeSatificated = FALSE;	// desired level yet reached
		ret =	QualifierTest(qu, tag_name, attributes) &&
				( !m_pUserTagFilter || m_pUserTagFilter->_TagTest(tag_name, attributes, DescendantLevel));
	}
	else
	{	// checking last node
		if(!m_bIncludeDescendants && DescendantLevel > (int)(m_QualifierShifts + m_Qualifiers.GetSize() + 1))
		{
			ret = _LastNodeSatificated = FALSE;
		}
		else
		{	pQ = &m_FinalQualifier;
			if(m_FinalQualifier.qt_Ordinal>=0 && m_FinalQualifier.qt_Ordinal<m_FinalQualifier._LastOrdinalTestPast)
				return FALSE;

			_LastNodeSatificated =	QualifierTest(m_FinalQualifier, tag_name, attributes) &&
									( !m_pUserTagFilter || m_pUserTagFilter->_TagTest(tag_name, attributes, DescendantLevel));

			ret = m_bIncludeDescendants || _LastNodeSatificated;
		}
	}

	if(pQ->qt_Ordinal>=0)
	{
		if(ret)
		{	ret = (pQ->_LastOrdinalTestPast == pQ->qt_Ordinal);
			pQ->_LastOrdinalTestPast++;
		}
	}

	return ret;
}



CXPathParser::CXPathParser(CXMLParser& xml_parser)
 :_XmlParser(xml_parser)
{
	_pXPath = NULL;
	m_XPathParseError = ERR_XPATH_OK;
	m_XPathParseErrorPosition = 0;
	m_UpLevelCount = m_QualifierShifts = 0;
	m_pUserTagFilter = NULL;
	_LastNodeSatificated = TRUE;
}

BOOL CXPathParser::QualifierTest(_Qualifier& q, const rt::StringA_Ref& tag_name, LPCSTR attributes)
{
	ASSERT(QT_NAME_NOT_PARSED != q.qt_TagName);

	if((q.qt_TagName == QT_NAME_EXACT) && tag_name!=q.TagName)return FALSE;

	rt::StringA_Ref name, value;
	switch(q.qt_Attribute)
	{
	case QT_ATTRIBUTE_ANY: return TRUE;
	case QT_ATTRIBUTE_HAVE:
		while(attributes)
		{
			attributes = _XmlParser._seek_next_attrib(attributes, name, value);
			if(attributes)
			{	if(name == q.Attribute)return TRUE;
			}
			else
			{	if(GetLastSyntaxError()!=ERR_XML_OK)return FALSE;
			}
		}
		return FALSE;
	case QT_ATTRIBUTE_EQUAL:
		while(attributes)
		{
			attributes = _XmlParser._seek_next_attrib(attributes, name, value);
			if(attributes)
			{	if(name == q.Attribute)
				{	
					_meta_::_convert_xml_to_text(value,_conv_value_temp);
					return _conv_value_temp == q.Value;
				}
			}
			else
			{	if(GetLastSyntaxError()!=ERR_XML_OK)return FALSE;
			}
		}
		return FALSE;
	case QT_ATTRIBUTE_NOTEQUAL:
		while(attributes)
		{
			attributes = _XmlParser._seek_next_attrib(attributes, name, value);
			if(attributes)
			{	if(name == q.Attribute)
				{	
					_meta_::_convert_xml_to_text(value,_conv_value_temp);
					return !(_conv_value_temp == q.Value);
				}
			}
			else
			{	if(GetLastSyntaxError()!=ERR_XML_OK)return FALSE;
			}
		}
		return TRUE;
	case QT_ATTRIBUTE_INITIATED:
		while(attributes)
		{
			attributes = _XmlParser._seek_next_attrib(attributes, name, value);
			if(attributes)
			{	if(name == q.Attribute)
				{	
					_meta_::_convert_xml_to_text(value,_conv_value_temp);
					return	q.Value.GetLength() <= _conv_value_temp.GetLength() &&
							memcmp(q.Value,_conv_value_temp,q.Value.GetLength()) == 0;
				}
			}
			else
			{	if(GetLastSyntaxError()!=ERR_XML_OK)return FALSE;
			}
		}
		return FALSE;
	default:	ASSERT(0);
	}

	return FALSE;
}

BOOL CXMLParser::EnterSucceedNode()
{
	_attribute_cursor = NULL;

	if(m_pCurTagFilter!= &m_XPathParser)
	{
		if(EnterFirstChildNode())return TRUE;
		if(GetLastSyntaxError() != w32::ERR_XML_OK)return FALSE;

		if(EnterNextSiblingNode()){ return TRUE; }
		else
		{	do
			{	if(GetLastSyntaxError() != w32::ERR_XML_OK)return FALSE;
				if(!EnterParentNode())return FALSE;
			}while(!EnterNextSiblingNode());
			return TRUE;
		}
	}
	else
	{
		for(;;)
		{
			if(	m_XPathParser.m_bIncludeDescendants || 
				m_NodePath.GetSize() < (int)(m_XPathParser.m_QualifierShifts + m_XPathParser.m_Qualifiers.GetSize() + 1)
			)
			{	if(EnterFirstChildNode())goto CHECK_THIS_NODE;
				if(GetLastSyntaxError() != w32::ERR_XML_OK)return FALSE;
			}

			if(EnterNextSiblingNode()){ goto CHECK_THIS_NODE; }
			else
			{	
				while( ((int)GetDescendantLevel()) > m_XPathParser.m_QualifierShifts+1 )
				{
					if(GetLastSyntaxError() != w32::ERR_XML_OK)return FALSE;
					if(!EnterParentNode())return FALSE;
					if(EnterNextSiblingNode())goto CHECK_THIS_NODE;
				}

				return FALSE;
			}

CHECK_THIS_NODE:
			if(m_XPathParser._LastNodeSatificated)break;
		}

		return TRUE;
	}

	ASSERT(0);
}

void CXMLParser::TextDump()
{
	rt::StringA att;
	do
	{	if(GetLastSyntaxError() != w32::ERR_XML_OK)
		{	_CheckDump("XML Parse Error = "<<GetLastSyntaxError()<<'\n');
			break;
		}

		for(UINT i=0;i/2<GetDescendantLevel();i++)
			_CheckDump(' ');

		_CheckDump("<"<<GetNodeName()<<">");

		if(GetAttributesXML(att))
		{
			_CheckDump(" ATTRIB = [ "<<(LPCSTR)att<<" ]");
		}
		_CheckDump('\n');
	}while(EnterSucceedNode());
}

BOOL CXMLParser::GetInnerXML(rt::StringA& text) const
{
	if(_CurNode().IsCompactNode)
		text.Empty();
	else
	{	LPCSTR inner_end = NULL;
		if(!_search_node_close(_CurNode().InnerXML_Start,_CurNode().TagName,&inner_end))
		{	text.Empty();
			return FALSE;
		}
		if(!text.SetLength((UINT)(inner_end - _CurNode().InnerXML_Start)))
		{
			text.Empty();
			rt::_CastToNonconst(this)->SetLastSyntaxError(ERR_XML_OUT_OF_MEMORY,_CurNode().InnerXML_Start);
			return FALSE;
		}
		memcpy(text.GetBuffer(),_CurNode().InnerXML_Start,text.GetLength());
	}

	return TRUE;
}

BOOL CXMLParser::GetAttributesXML(rt::StringA& text) const
{
	if(!_CurNode().Attributes.IsEmpty())
	{	
		text.SetLength(_CurNode().Attributes.GetLength());
		memcpy(text.GetBuffer(),_CurNode().Attributes.Begin(),_CurNode().Attributes.GetLength());
		return TRUE;
	}
	else
	{	text.Empty();
		return FALSE;
	}
}

BOOL CXMLParser::GetInnerText(rt::StringA& text) const
{
	if(_CurNode().IsCompactNode)
		text.Empty();
	else
	{	LPCSTR inner_end = NULL;
		if(!_search_node_close(_CurNode().InnerXML_Start,_CurNode().TagName,&inner_end))
		{
			text.Empty();
			return FALSE;
		}
		if(!text.SetLength((int)(inner_end - _CurNode().InnerXML_Start)))
		{
			rt::_CastToNonconst(this)->SetLastSyntaxError(ERR_XML_OUT_OF_MEMORY,_CurNode().InnerXML_Start);
			text.Empty();
			return FALSE;
		}
				
		_meta_::_convert_xml_to_text(rt::StringA_Ref(_CurNode().InnerXML_Start,(int)(inner_end - _CurNode().InnerXML_Start)),text);
		return TRUE;
	}

	return TRUE;
}

BOOL CXMLParser::GetOuterXML(rt::StringA& text) const
{
	LPCSTR end;
	if(_CurNode().IsCompactNode)
	{	end = _CurNode().InnerXML_Start;	}
	else
	{	end = _search_node_close(_CurNode().InnerXML_Start,_CurNode().TagName);
		if(end == NULL)
		{	text.Empty();
			return FALSE;
		}
	}

	end++;
	if(!text.SetLength((int)(end - _CurNode().OuterXML_Start)))
	{
		text.Empty();
		rt::_CastToNonconst(this)->SetLastSyntaxError(ERR_XML_OUT_OF_MEMORY, _CurNode().OuterXML_Start);
		return FALSE;
	}
			
	memcpy(text.GetBuffer(),_CurNode().OuterXML_Start,text.GetLength());
	return TRUE;
}

BOOL CXMLParser::GetAttribute(LPCSTR name, rt::StringA& value) const
{
	rt::StringA_Ref val;
	if(!_CurNode().Attributes.IsEmpty() && _search_attrib(_CurNode().Attributes.Begin(),name,val))
	{	
		_meta_::_convert_xml_to_text(val,value);
		return TRUE;
	}

	value.Empty();
	return FALSE;
}


BOOL CXMLParser::GetAttribute_Path(LPCSTR name, rt::StringA& value) const
{
	if(GetAttribute(name,value))
	{
		if(value.End()[-1] == '\\' || value.End()[-1] == '/')
			value.SetLength(value.GetLength()-1);
		return TRUE;
	}
	return FALSE;
}


INT CXMLParser::GetAttribute_Int(LPCSTR name, int dd) const
{
	rt::StringA value;
	if(GetAttribute(name,value))
	{
		return atoi(value);
	}
	else return dd;
}

ULONGLONG CXMLParser::GetAttribute_FileSize(LPCSTR name, ULONGLONG dd) const
{
	rt::StringA value;
	if(GetAttribute(name,value))
	{
		ULONGLONG sz = 0;
		UINT i = value.GetRef().ToNumber(sz);

		for(;i<value.GetLength();i++)	// skip whitespace
			if(value[i] > ' ')break;

		if(value[i] == 'k' || value[i] == 'K')
			sz *= 1024;
		else if(value[i] == 'm' || value[i] == 'M')
			sz *= 1024*1024;
		else if(value[i] == 'g' || value[i] == 'G')
			sz *= 1024LL*1024*1024;
		else if(value[i] == 't' || value[i] == 'T')
			sz *= 1024LL*1024*1024*1024;

		return sz;
	}
	else return dd;
}

double CXMLParser::GetAttribute_Float(LPCSTR name, double dd) const
{
	rt::StringA value;
	if(GetAttribute(name,value))
	{
		return atof(value);
	}
	else return dd;
}

BOOL CXMLParser::GetFirstAttribute(rt::StringA& name, rt::StringA& value) const
{
	rt::_CastToNonconst(this)->_attribute_cursor = _CurNode().Attributes.Begin();
	return GetNextAttribute(name, value);
}

BOOL CXMLParser::GetNextAttribute(rt::StringA& name, rt::StringA& value) const
{
	ASSERT(_attribute_cursor >= _CurNode().Attributes.Begin());
	rt::StringA_Ref n, v;
	LPCSTR p = _seek_next_attrib(_attribute_cursor, n, v);
	if(p && !n.IsEmpty())
	{	rt::_CastToNonconst(this)->_attribute_cursor = p;
		name = n;	value = v;
		return TRUE;
	}
	else return FALSE;
}


LPCSTR CXPathParser::SetLastSyntaxError(XPathParseError errnum, LPCSTR pos)
{
	ASSERT(pos >= _pXPath);
	m_XPathParseError = errnum;
	m_XPathParseErrorPosition = (UINT)(pos - _pXPath);

	ASSERT(0);
	return pos;
}


void CXPathParser::_Qualifier::Load(LPCSTR qualify, UINT length)
{
	qt_TagName = QT_NAME_NOT_PARSED;
	TagName = rt::StringA_Ref(qualify, length);
}

BOOL CXPathParser::ParseQualifier(_Qualifier& q)
{
	q._LastOrdinalTestPast = 0;
	ASSERT(q.qt_TagName == QT_NAME_NOT_PARSED);
	rt::StringA qt;
	rt::Swap(qt,q.TagName);

	LPSTR qualify = qt.GetBuffer();
	qualify = _meta_::_skip_whitespace(qualify);
	if(*qualify == '@')
	{
		SetLastSyntaxError(ERR_SELECTOR_ATTRIBUTE_SET_NOT_SUPPORTED,qualify);
		return FALSE;
	}
	else
	{	
		LPSTR patt = NULL;

		if(*qualify == '*')
		{	
			patt = qualify + 1;
			q.qt_TagName = QT_NAME_ANY;
		}
		else
		{	patt = (LPSTR)_meta_::_seek_symbol_end(qualify);
			if(patt == qualify)
			{
				SetLastSyntaxError(ERR_SELECTOR_NODENAME_NOT_FOUND,qualify);
				return FALSE;
			}
			q.qt_TagName = QT_NAME_EXACT;
			q.TagName.SetLength((UINT)(patt - qualify));
			memcpy(q.TagName.GetBuffer(),qualify,(UINT)(patt - qualify));
		}

		patt = _meta_::_skip_whitespace(patt);

		if(*patt == '[')
		{	
			patt = _meta_::_skip_whitespace(patt+1);

			if(*patt >= '0' && *patt <= '9')
			{
				q.qt_Attribute = QT_ATTRIBUTE_ANY;
				LPSTR tail = _meta_::_seek_symbol_end(patt);
				if(*tail == ']')
				{
					rt::StringA_Ref(patt,tail).ToNumber(q.qt_Ordinal);
					return TRUE;
				}
				else
				{	SetLastSyntaxError(ERR_QUALIFIER_BAD_ORDINAL,tail);
					return FALSE;
				}
			}
			else if(*patt == '@')
			{
				q.qt_Ordinal = -1;
				q.qt_Attribute = QT_ATTRIBUTE_HAVE;

				patt = _meta_::_skip_whitespace(patt+1);
				LPCSTR pend = _meta_::_seek_symbol_end(patt);
				q.Attribute.SetLength((UINT)(pend - patt));
				memcpy(q.Attribute.GetBuffer(),patt,(UINT)(pend - patt));

				patt = (LPSTR)_meta_::_skip_whitespace(pend);

				if(*patt == '=')
				{	if(patt[-1] == ':')
					{	q.qt_Attribute = QT_ATTRIBUTE_INITIATED;
						q.Attribute.SetLength(q.Attribute.GetLength()-1);
					}
					else
					{
						q.qt_Attribute = QT_ATTRIBUTE_EQUAL;
					}
					patt++;
				}
				else if(patt[0] == '!' && patt[1] == '=')
				{	q.qt_Attribute = QT_ATTRIBUTE_NOTEQUAL;
					patt+=2;
				}
				else if(patt[0] == ']')
				{	return TRUE;
				}
				else
				{
					SetLastSyntaxError(ERR_QUALIFIER_OPERATOR_NOT_SUPPORTED,patt);
					return FALSE;
				}

				patt = (LPSTR)_meta_::_skip_whitespace(patt);
				if(*patt == '\'' || *patt == '\"')
				{
					pend = strchr(patt+1,*patt);
					if(pend)
					{
						_meta_::_convert_xml_to_text(rt::StringA_Ref(patt+1,(UINT)(pend - patt) -1),q.Value);

						patt = (LPSTR)_meta_::_skip_whitespace(pend+1);
						if(*patt == ']')return TRUE;
						else
						{
							SetLastSyntaxError(ERR_QUALIFIER_CONDITION_NOT_SUPPORTED,patt);
							return FALSE;
						}
					}
					else
					{
						SetLastSyntaxError(ERR_QUALIFIER_QUOTATION_NOT_MATCHED,patt);
						return FALSE;
					}
				}
				else
				{
					SetLastSyntaxError(ERR_QUALIFIER_VALUE_NOT_SUPPORTED,patt);
					return FALSE;
				}
			}
			else
			{
				SetLastSyntaxError(ERR_QUALIFIER_CONDITION_NOT_SUPPORTED,patt);
				return FALSE;
			}
		}
		else if(*patt == '\0')
		{
			q.qt_Attribute = QT_ATTRIBUTE_ANY;
			q.qt_Ordinal = -1;
			return TRUE;
		}
		else
		{	
			SetLastSyntaxError(ERR_SELECTOR_UNEXPECTED_SYMBOL,patt);
			return FALSE;
		}
	}
	
	ASSERT(0);
}

void CXPathParser::Clear()
{
	m_bRelativePath = FALSE;
	m_bIncludeDescendants = FALSE;
	m_FinalQualifier.qt_TagName = QT_NAME_NOT_PARSED;
	_LastNodeSatificated = TRUE;

	m_UpLevelCount = m_QualifierShifts = 0;
	m_Qualifiers.SetSize();
}

BOOL CXPathParser::Load(LPCSTR xpath)
{
	m_Qualifiers.ChangeSize(0);
	m_UpLevelCount = 0;
	_LastNodeSatificated = TRUE;
	
	_pXPath = xpath;
	xpath = _meta_::_skip_whitespace(xpath);

	m_UpLevelCount = 0;

	LPCSTR pLastSep = strrchr(xpath,'/');
	if(pLastSep == NULL)
	{	m_bRelativePath = TRUE;
		m_bIncludeDescendants = FALSE;
		m_FinalQualifier.Load(xpath,(UINT)strlen(xpath));
		return ParseQualifier(m_FinalQualifier);
	}
	else
	{	LPCSTR pend;
		if(pLastSep[-1] == '/')
		{	m_bIncludeDescendants = TRUE;
			pend = pLastSep - 1;
		}
		else
		{	m_bIncludeDescendants = FALSE;
			pend = pLastSep;
		}
		pLastSep++;
		m_FinalQualifier.Load(pLastSep,(UINT)strlen(pLastSep));
		if( ParseQualifier(m_FinalQualifier) )
		{
			if(*xpath == '/')
			{	m_bRelativePath = FALSE;
				xpath++;
			}else m_bRelativePath = TRUE;

			while(xpath < pend)
			{
				LPCSTR next = strchr(xpath,'/');
				ASSERT(next);

				if(xpath[0] == '/')
				{	SetLastSyntaxError(ERR_XPATH_DESCENDANT_IN_HALFWAY,xpath);
					return FALSE;
				}

				if(xpath[0] == '.' && xpath+1 == next){}
				else if(xpath[0] == '.' && xpath[1] == '.' && xpath+2 == next)
				{	
					if(m_Qualifiers.GetSize())
					{	m_Qualifiers.ChangeSize(m_Qualifiers.GetSize()-1);
					}
					else m_UpLevelCount++;
				}
				else
				{	m_Qualifiers.push_back().Load(xpath,(UINT)(next - xpath));
				}

				xpath = next+1;
			}
		}

		for(UINT i=0;i<m_Qualifiers.GetSize();i++)
			if(!ParseQualifier(m_Qualifiers[i]))return FALSE;

		return TRUE;
	}

	return FALSE;
}

rt::StringA_Ref	CXMLParser::GetNodeName() const
{
	return _CurNode().TagName;
}

void CXMLParser::GetNodeName(rt::StringA& name) const
{
	name = _CurNode().TagName;
}

BOOL GetTextFromXML(rt::StringA & out, LPCTSTR fn, LPCSTR xpath, LPCSTR attrib)
{
	w32::CXMLParser doc;
	w32::CFileBuffer<char> file;
	if(	file.SetSizeAs(fn) &&
		doc.Load(file,TRUE,file.GetSize()) &&
		doc.EnterXPath(xpath)
	)
	{	if(attrib)
			return doc.GetAttribute(attrib,out);
		else
			return doc.GetInnerText(out);
	}

	return FALSE;
}

const char CSoapRequest_Flickr::g_Soap_Header[149] = 
	"<?xml version=\"1.0\" encoding=\"utf-8\" ?>"  // 39
	"<s:Envelope xmlns:s=\"http://www.w3.org/2003/05/soap-envelope\" >" //63
	"<s:Body>" //8
	"<x:FlickrRequest xmlns:x=\"urn:flickr\">"; //38

const char CSoapRequest_Flickr::g_Soap_Tailer[41] = 
	"</x:FlickrRequest>" // 18
	"</s:Body></s:Envelope>";  // 39


CSoapRequest_Flickr::CSoapRequest_Flickr()
{
	m_bFinalized = TRUE;
}

void CSoapRequest_Flickr::SetMethod(LPCSTR function_name)
{
	m_XMLCompos.ResetContent(g_Soap_Header);
	m_XMLCompos.EnterNode("method");
	m_XMLCompos.AddText(function_name);
	m_XMLCompos.ExitNode();
	m_bFinalized = FALSE;
}

void CSoapRequest_Flickr::SetParam(LPCSTR param_name, LPCSTR value)
{	
	ASSERT(!m_bFinalized);
	m_XMLCompos.EnterNode(param_name);
	m_XMLCompos.AddText(value);
	m_XMLCompos.ExitNode();
}

void CSoapRequest_Flickr::Finalize()
{
	if(!m_bFinalized)
	{
		m_bFinalized = TRUE;
		m_XMLCompos.AppendTrail(g_Soap_Tailer);
	}
}

CSoapRequest_Flickr::operator	LPCSTR()
{
	Finalize();
	return m_XMLCompos.GetDocument();
}

CSoapRequest_Flickr::operator	LPCBYTE()
{
	Finalize();
	return (LPCBYTE)m_XMLCompos.GetDocument();
}

UINT CSoapRequest_Flickr::GetSize()
{
	Finalize();
	return m_XMLCompos.GetDocumentLength();
}

} // namespace w32
