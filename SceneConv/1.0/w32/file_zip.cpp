#include "stdafx.h"
#include "debug_log.h"
#include "file_zip.h"
#include "w32_misc.h"
#include <algorithm>

#ifndef ZLIB_VERSION
#pragma message(">> <zlib.h> is not been included before w32/file_zip.h. w32::CFileZip can only manipulate .zip in store mode (uncompressed)")
#else
	#ifdef ZLIB_LOCATION
	#pragma comment(lib,ZLIB_LOCATION)
	#else
	#pragma comment(lib,"zlib.lib")
	#endif
#endif

namespace w32
{

DWORD _FileMappingBlockSize = 0;
DWORD _StreamBlockSize = 0;

void CFileZip::_crc_checksum(LPCVOID pSrc, UINT SrcLen, LPDWORD crc32)
{
	w32::CRC32 crc(*crc32);
	crc.Update(pSrc,SrcLen);
	crc.Finalize();
	*crc32 = crc.GetCRC();
}


CFileZip::CFileZip()
{
	if(!_FileMappingBlockSize)
	{
		SYSTEM_INFO sysi;
		::GetSystemInfo(&sysi);
		_FileMappingBlockSize = sysi.dwAllocationGranularity;
		_StreamBlockSize = (1024*1024/_FileMappingBlockSize)*_FileMappingBlockSize;
	}

	m_pZipFile = NULL;
	m_CentralDirectoryStart = 0;
	m_CentralDirectoryModified = FALSE;
	m_CompressionMode = (UINT)ZIP_STORE;

	stream_read_store.pThis = this;
	stream_write_store.pThis = this;

#ifdef ZLIB_VERSION
	stream_write_zlib.pThis = this;
	stream_read_zlib.pThis = this;
#endif

	_AddingNonZeroFileEntry = NULL;
}

BOOL CFileZip::Open(LPCTSTR fn, UINT openflag, BOOL load_indexed)
{
	ASSERT(!IsOpen());
	return _BaseFile.Open(fn,openflag) && Open(&_BaseFile,openflag);
}

int	CFileZip::FindFileRecursive(LPCSTR string, int start)
{
	int len = strlen(string);
	for(int i=start; i<(int)m_FileEntries.GetSize(); i++)
	{
		FileEntry& en = *(FileEntry*)m_FileEntries[i];
		if(&en && en.FileNameLength>=len && memcmp(string, &en.FileName[en.FileNameLength-len],len)==0)
			return i;
	}
	return -1;
}

BOOL CFileZip::Open(rt::_stream* pFile, UINT mode, BOOL load_indexed)
{
	ASSERT(!IsOpen());
	ASSERT(pFile);

	m_Password.SetSize();
	m_pZipFile = pFile;
	m_IsReadOnly = !(mode&CFile64::Access_Write);

	if( (mode & (CFile64::Mode_OpenNew|CFile64::Mode_OpenAny)) == (CFile64::Mode_OpenNew|CFile64::Mode_OpenAny) &&
		(pFile->GetLength() == 0)
	)
	{	// create new
		m_CentralDirectoryStart = 0;
		ASSERT(m_FileEntries.GetSize()==0);
		ASSERT(!m_IsReadOnly);
	}
	else
	{	// open existing
		DWORD filelen = (int)pFile->GetLength();
		if(filelen < sizeof(CentralDirectory)){ goto ERROR_OCCURED; }

		int len = (DWORD)min(pFile->GetLength(), 0xffff + sizeof(CentralDirectory));
		pFile->Seek(-len, rt::_stream::Seek_End);

		rt::Buffer<BYTE>	cdir;
		VERIFY(cdir.SetSize(len));
		CentralDirectory*	pCD = NULL;
		if(	pFile->Read(cdir,len) == len )
		{
			for(UINT i=0;i<len - sizeof(CentralDirectory) + 4; i++)
			{
				CentralDirectory* p = (CentralDirectory*)(&cdir[i]);
				if( p->Signature == CentralDirectory::header_magic && p->m_uOffset < filelen )
				{	pCD = p;
					break;
				}
			}
		}

		if( pCD &&
			pCD->m_uThisDisk == 0 &&		// no volume support
			pCD->m_uDiskWithCD == 0 &&
			pCD->m_uDiskEntriesNo == pCD->m_uEntriesNumber &&
			(pCD->m_uSize + pCD->m_uOffset) < filelen
		){}
		else
		{ goto ERROR_OCCURED; }
		
		if(pCD->m_ZipCommentLength)
		{
			int len = min(pCD->m_ZipCommentLength, (WORD)(((LPSTR)cdir.End()) - pCD->m_ZipComment));
			m_ZipComment = rt::StringA_Ref(pCD->m_ZipComment, len);
		}

		m_CentralDirectoryStart = pCD->m_uOffset;
		pFile->Seek(pCD->m_uOffset);
		VERIFY(_FileEntryPool.SetSize(pCD->m_uSize));
		if( pFile->Read(_FileEntryPool, pCD->m_uSize) == pCD->m_uSize )
		{
			// Load file list
			VERIFY(m_FileEntries.SetSize(pCD->m_uEntriesNumber));
			m_FileEntries.Zero();

			FileEntry* p = (FileEntry*)_FileEntryPool.Begin();
			for(UINT i=0;i<m_FileEntries.GetSize();i++)
			{
				if( p->Signature == FileEntry::header_magic &&
					((LPBYTE)p) + sizeof(FileEntry) <= _FileEntryPool.End() &&
					((LPBYTE)p) + sizeof(FileEntry) + p->FileNameLength + p->ExtraFieldLength + p->FileCommentLength - 1 <= _FileEntryPool.End()
				)
				{	m_FileEntries[i] = p;

					//_CheckDump(rt::StringA_Ref(p->FileName,p->FileNameLength)<<"\n");

					p = (FileEntry*)(((LPBYTE)p) + sizeof(FileEntry) + p->FileNameLength + p->ExtraFieldLength + p->FileCommentLength - 1);
				}
				else
				{	goto ERROR_OCCURED;
				}
			}

			m_ListSorted = FALSE;
			if(load_indexed)
				ReindexAllEntries();
			
			m_CentralDirectoryModified = FALSE;
		}
	}

	//_CheckDump(m_FileEntries.GetSize()<<"\n");

	return TRUE;

ERROR_OCCURED:
	Close();
	return FALSE;
}

void CFileZip::_FreeFileEntry(FileEntryPtr& ptr)
{
	ptr.Empty();
	FileEntry* p = ptr;
	if(p==NULL || (p>=(LPCVOID)_FileEntryPool.Begin() && p<(LPCVOID)_FileEntryPool.End()) ){}
	else
	{
		_SafeFree32AL(ptr.p);
	}
}

void CFileZip::Close()
{
	ASSERT(_AddingNonZeroFileEntry == NULL);

	if(IsOpen())
	{
		Save();
		// flush ...
		_BaseFile.Close();
	}

	for(UINT i=0;i<m_FileEntries.GetSize();i++)
		_FreeFileEntry(m_FileEntries[i]);

	m_FileEntries.SetSize(0);
	m_ZipComment.Empty();
	m_CentralDirectoryStart = 0;

	m_pZipFile = NULL;
}


int CFileZip::FindFile(const rt::StringA_Ref& pathname)
{
	if(!m_FileEntries.GetSize())return -1;
	ASSERT(m_ListSorted);

	SIZE_T idx = rt::BinarySearch(m_FileEntries,pathname);

	if(idx < m_FileEntries.GetSize() && m_FileEntries[idx].p->Signature)
	{
		return (int)idx;
	}
	else return -1;
}

BOOL CFileZip::AddZeroSizedEntry(const rt::StringA_Ref& pathname, const FILETIME& ftime, DWORD attrib)
{
	ASSERT(_AddingNonZeroFileEntry == NULL);
	if(pathname.GetLength() > 0xffff)return FALSE;
	if(m_FileEntries.GetSize() >= 0xffff)return FALSE;
	ASSERT(!m_IsReadOnly);

	int slash_add = pathname[pathname.GetLength()-1] != '/' && 
					pathname[pathname.GetLength()-1] != '\\' &&
					attrib == FILE_ATTRIBUTE_DIRECTORY;

	UINT FileEntryLen = sizeof(FileEntry)-1 + pathname.GetLength() + slash_add;
	FileEntry& e = *((FileEntry*)rt::Malloc32AL(FileEntryLen));
		
	e.CRC32 = 0;
	::FileTimeToDosDateTime(&ftime,&e.FileDate,&e.FileTime);
	e.Compression = 0;
	e.DiskNumber = 0;
	e.ExtraFieldLength = 0;
	e.FileAttributes = 0;
	e.FileAttributesOriginal = attrib;
	e.FileCommentLength = 0;
	memcpy(e.FileName,pathname.Begin(),pathname.GetLength());
	rt::StringA_Ref(e.FileName,pathname.GetLength()).Replace('\\','/');
	if(slash_add)e.FileName[pathname.GetLength()] = '/';
	e.FileNameLength = (WORD)pathname.GetLength() + slash_add;
	e.Flag = 0;
	e.OffsetToLocalFileHeader = 0;
	e.Signature = e.header_magic;
	e.Size = 0;
	e.SizeOriginal = 0;
	e.ZipVersion = 0x014;
	e.UnzipVersion = 0x000a;

	m_FileEntries.push_back(FileEntryPtr(&e));
	m_CentralDirectoryModified = TRUE;
	m_ListSorted = FALSE;

	return TRUE;
}

void CFileZip::ReindexAllEntries()
{
	if(!m_ListSorted)
	{
		std::sort(m_FileEntries.Begin(),m_FileEntries.End());
		m_ListSorted = TRUE;
	}
}

CFileZip::FileEntry* CFileZip::_AddNonZeroFileEntry_Begin(const rt::StringA_Ref& pathname, UINT size, const FILETIME& ftime)
{
	ASSERT(_AddingNonZeroFileEntry == NULL);
	if(pathname.GetLength() > 0xffff)return FALSE;
	if(m_FileEntries.GetSize() >= 0xffff)return FALSE;
	ASSERT(size); // add zero-sized file by AddDirectory and set attrib = FILE_ATTRIBUTE_ARCHIVE
	
	ASSERT(!m_IsReadOnly);

	// store
	UINT FileEntryLen = sizeof(FileEntry)-1 + pathname.GetLength();
	UINT offset;

	_CentralDirectoryStart_Old = m_CentralDirectoryStart;

	UINT LFlen = sizeof(LocalFileHeader)-1 + pathname.GetLength() + size; // + sizeof(DataDesc);
	{	
		// Looking for removed place
		for(UINT i=0;i<m_FileEntries.GetSize();i++)
			if(m_FileEntries[i].p->Signature==0 && m_FileEntries[i].p->Size >= LFlen)
			{	
				offset = m_FileEntries[i].p->OffsetToLocalFileHeader;
				m_FileEntries[i].p->Size -= LFlen;
				m_FileEntries[i].p->OffsetToLocalFileHeader += LFlen;
				_HoleFromEntry = m_FileEntries[i].p;
				_HoleTakenSize = LFlen;
				goto SAVE_ENTRY;
			}

		if(m_CentralDirectoryStart + LFlen > (UINT_MAX - sizeof(CentralDirectory) - 0xffff))return FALSE;
		offset = m_CentralDirectoryStart;
		m_CentralDirectoryStart += LFlen;
		_HoleFromEntry = NULL;
	}

SAVE_ENTRY:

	FileEntry& e = *((FileEntry*)rt::Malloc32AL(FileEntryLen));
	e.CRC32 = 0;
	e.Compression = 0;
	e.SizeOriginal = size;
	::FileTimeToDosDateTime(&ftime,&e.FileDate,&e.FileTime);
	e.DiskNumber = 0;
	e.ExtraFieldLength = 0;
	e.FileAttributes = 0;
	e.FileAttributesOriginal = FILE_ATTRIBUTE_ARCHIVE;
	e.FileCommentLength = 0;
	memcpy(e.FileName,pathname.Begin(),pathname.GetLength());
	rt::StringA_Ref(e.FileName,pathname.GetLength()).Replace('\\','/');
	e.FileNameLength = (WORD)pathname.GetLength();
	e.Flag = 0;
	e.OffsetToLocalFileHeader = offset;
	e.Signature = e.header_magic;
	e.Size = size;
	e.ZipVersion = 0x014;
	e.UnzipVersion = 0x000a;
	
	_AddingNonZeroFileEntry = &e;
	_ClaimedSize = size;
	UINT name_pos = e.OffsetToLocalFileHeader+sizeof(LocalFileHeader)-1;
	if( m_pZipFile->Seek(name_pos) == name_pos &&
		m_pZipFile->Write(pathname.Begin(),pathname.GetLength()) == pathname.GetLength()
	)
	{	return &e;
	}
	else
	{	_AddNonZeroFileEntry_End(FALSE);
		return NULL;
	}
}

BOOL CFileZip::_AddNonZeroFileEntry_End(BOOL commit)
{
	ASSERT(_AddingNonZeroFileEntry);
	ASSERT(_ClaimedSize >= _AddingNonZeroFileEntry->Size);

	BOOL ret = TRUE;

	if(commit)
	{
		FileEntry& e = *_AddingNonZeroFileEntry;
		LocalFileHeader LFH;
		LFH.Compression = e.Compression;
		LFH.CRC32 = e.CRC32;
		LFH.ExtraFieldLength = 0;
		LFH.FileDate = e.FileDate;
		LFH.FileNameLength = e.FileNameLength;
		LFH.FileTime = e.FileTime;
		LFH.Flag = e.Flag;
		LFH.Signature = LFH.header_magic;
		LFH.Size = e.Size;
		LFH.SizeOriginal = e.SizeOriginal;
		LFH.Version = e.UnzipVersion;

		if(	m_pZipFile->Seek(_AddingNonZeroFileEntry->OffsetToLocalFileHeader) == _AddingNonZeroFileEntry->OffsetToLocalFileHeader &&
			m_pZipFile->Write(&LFH,sizeof(LocalFileHeader)-1) == sizeof(LocalFileHeader)-1
		)
		{	m_FileEntries.push_back(FileEntryPtr(_AddingNonZeroFileEntry));
			m_CentralDirectoryModified = TRUE;
			m_ListSorted = FALSE;

			int saved = _ClaimedSize - _AddingNonZeroFileEntry->Size;
			if(saved)
			{
				if(_HoleFromEntry)
				{
					_HoleFromEntry->Size+=saved;
					_HoleFromEntry->OffsetToLocalFileHeader-=saved;
				}
				else
					m_CentralDirectoryStart -= saved;
			}

			_AddingNonZeroFileEntry = NULL;
			return TRUE;
		}

		ret = FALSE;
	}

	// rollback
	_SafeFree32AL(_AddingNonZeroFileEntry);
	if(_HoleFromEntry)
	{
		_HoleFromEntry->Size+=_HoleTakenSize;
		_HoleFromEntry->OffsetToLocalFileHeader-=_HoleTakenSize;
	}
	else
		m_CentralDirectoryStart = _CentralDirectoryStart_Old;

	return ret;
}

void CFileZip::compress_stream::Init(UINT base)
{
	Written = 0;
	BaseInFile = base;
	CRC = 0;
}

BOOL CFileZip::compress_stream::Finalize(BOOL commit)
{
	pThis->_AddingNonZeroFileEntry->CRC32 = CRC;
	pThis->_AddingNonZeroFileEntry->Size = Written;
	return pThis->_AddNonZeroFileEntry_End(commit);
}

UINT CFileZip::compress_store::Write(LPCVOID p, UINT size)
{
	if(Written + size <= pThis->_ClaimedSize)
	{
		pThis->_crc_checksum(p,size,&CRC);
		if(	pThis->m_pZipFile->Seek(BaseInFile + Written) == BaseInFile + Written &&
			pThis->m_pZipFile->Write(p,size) == size
		)
		{	Written += size;
			return size;
		}
	}
	return 0;
}

#ifdef ZLIB_VERSION
CFileZip::compress_zlib::compress_zlib()
{
	defstrm = NULL;
}

void CFileZip::compress_zlib::Clear()
{
	if(defstrm)
	{	deflateEnd(defstrm);
		_SafeFree32AL(defstrm);
	}
}

BOOL CFileZip::compress_zlib::Finalize(BOOL commit)
{
	if(commit)
	{
		defstrm->avail_in = 0;
		defstrm->next_in = Z_NULL;
		defstrm->avail_out = pThis->_StreamBuff.GetSize();
		defstrm->next_out = (Bytef*)pThis->_StreamBuff.Begin();

		for(;;)
		{	int err;
			if((err = deflate(defstrm, Z_FINISH)) == Z_STREAM_END)break;
			if(err != Z_OK)
			{	__super::Finalize(FALSE);
				return FALSE;
			}
		}

		int wsize = pThis->_StreamBuff.GetSize() - defstrm->avail_out;

		if(wsize>0)
		{
			if(Written + wsize <= pThis->_ClaimedSize)
			{
				if(	pThis->m_pZipFile->Seek(BaseInFile + Written) == BaseInFile + Written &&
					pThis->m_pZipFile->Write(pThis->_StreamBuff,wsize) == wsize
				)
				{	Written += wsize;
					goto Z_FINISH_DONE;
				}
			}
			__super::Finalize(FALSE);
			return FALSE;
		}
	}
Z_FINISH_DONE:
	return __super::Finalize(commit);
}

UINT CFileZip::compress_zlib::Write(LPCVOID p, UINT size)
{
	if(!Written)
	{
		Clear();
		VERIFY(pThis->_StreamBuff.SetSize(_StreamBlockSize));
		defstrm = (z_stream*)rt::Malloc32AL(sizeof(*defstrm));
		ASSERT(defstrm);

		defstrm->zalloc = Z_NULL;
		defstrm->zfree = Z_NULL;
		defstrm->opaque = Z_NULL;
		defstrm->avail_in = 0;
		defstrm->next_in = Z_NULL;
		defstrm->next_out = Z_NULL;
		defstrm->avail_out = 0;

		if(deflateInit2(defstrm, Z_DEFAULT_COMPRESSION, Z_DEFLATED, -15, 8, Z_DEFAULT_STRATEGY)!= Z_OK)return 0;
	}

	pThis->_crc_checksum(p,size,&CRC);
	defstrm->avail_in = size;
	defstrm->next_in = (Bytef*)p;

	for(;;)
	{
		defstrm->avail_out = pThis->_StreamBuff.GetSize();
		defstrm->next_out = (Bytef*)pThis->_StreamBuff.Begin();

		deflate(defstrm, Z_SYNC_FLUSH);

		if(defstrm->avail_in == 0 || defstrm->avail_out == 0)
		{	
			int wsize = pThis->_StreamBuff.GetSize() - defstrm->avail_out;

			if(Written + wsize <= pThis->_ClaimedSize)
			{
				if(	pThis->m_pZipFile->Seek(BaseInFile + Written) == BaseInFile + Written &&
					pThis->m_pZipFile->Write(pThis->_StreamBuff,wsize) == wsize
				)
				{	Written += wsize;
				}
			}
			else
			{	return INFINITE;	// switch to no compress !!
			}

			if(defstrm->avail_in == 0)
				return size;	// done
		}
		else return 0; // Error
	}

	ASSERT(0);
	return 0;
}


CFileZip::decompress_zlib::decompress_zlib()
{
	infstrm = NULL;
	_FileMap = NULL;
	_Mapped = NULL;
}

void CFileZip::decompress_zlib::Clear()
{
	if(infstrm)
	{	inflateEnd(infstrm);
		_SafeFree32AL(infstrm);
		if(_Mapped)
		{	::UnmapViewOfFile(_Mapped);
			_Mapped = NULL;
		}
		if(_FileMap)
		{	::CloseHandle(_FileMap);
			_FileMap = NULL;
		}
	}
}

void CFileZip::decompress_zlib::Init(UINT total_out_size, UINT base, DWORD target_CRC, UINT compressed_size)
{
	__super::Init(total_out_size, base, target_CRC);
	CompressedSize = compressed_size;
	CompressedAte = 0;
}

UINT CFileZip::decompress_zlib::Read(LPVOID p, UINT size)
{
	if(!CompressedAte)	// init
	{	Clear();
		if(	pThis->_BaseFile.IsOpen() && 
			(_FileMap = ::CreateFileMapping(pThis->_BaseFile,NULL,PAGE_READONLY,0,BaseInFile + CompressedSize,NULL))
		)
		{	_AlignOffset = BaseInFile - (BaseInFile/_FileMappingBlockSize)*_FileMappingBlockSize;
		}
		else
		{	VERIFY(pThis->_StreamBuff.SetSize(_StreamBlockSize));
		}
		infstrm = (z_stream*)rt::Malloc32AL(sizeof(*infstrm));
		ASSERT(infstrm);

		infstrm->zalloc = Z_NULL;
		infstrm->zfree = Z_NULL;
		infstrm->opaque = Z_NULL;
		infstrm->avail_in = 0;
		infstrm->next_in = Z_NULL;
		infstrm->next_out = Z_NULL;
		infstrm->avail_out = 0;

		if(inflateInit2(infstrm,-15) != Z_OK)return 0;
	}

	infstrm->next_out = (Bytef*)p;
	infstrm->avail_out = size;

	for(;;)
	{
		if(infstrm->avail_in == 0)
		{	// load compressed data
			if(CompressedAte >= CompressedSize)goto CHECK_CRC32;

			int load = min(_StreamBlockSize,CompressedSize - CompressedAte);
			infstrm->avail_in = load;
			if(_FileMap)
			{	if(_Mapped)::UnmapViewOfFile(_Mapped);
				_Mapped = (LPCBYTE)::MapViewOfFile(	_FileMap,FILE_MAP_READ,0,
													BaseInFile + CompressedAte - _AlignOffset,
													_AlignOffset + load
												  );
				if(!_Mapped)goto CHECK_CRC32;
				infstrm->next_in = (Bytef*)(_Mapped + _AlignOffset);
			}
			else
			{	
				if(	pThis->m_pZipFile->Seek(BaseInFile + CompressedAte) == BaseInFile + CompressedAte &&
					pThis->m_pZipFile->Read(pThis->_StreamBuff,load) == load)
				{	infstrm->next_in = (Bytef*)pThis->_StreamBuff.Begin();
				}
				else goto CHECK_CRC32;
			}
			CompressedAte += load;
		}

		inflate(infstrm, Z_SYNC_FLUSH);
		if(infstrm->avail_out == 0 || infstrm->avail_in)
			break;
	}

CHECK_CRC32:
	UINT outed = size - infstrm->avail_out;
	if(__super::CheckCRC(p,outed))
	{
		Outputed += outed;
		return outed;
	}
	else
		return 0;
}


#endif // #ifdef ZLIB_VERSION

CFileZip::stream_writeonly* CFileZip::AddFile(const rt::StringA_Ref& pathname, UINT size, const FILETIME& ftime)
{
	ASSERT(_AddingNonZeroFileEntry == NULL);

	FileEntry* e = _AddNonZeroFileEntry_Begin(pathname,size,ftime);
	if(e)
	{
		e->Compression = m_CompressionMode;
		e->SizeOriginal = size;
		if(m_CompressionMode)
		{	
			switch(m_CompressionMode)
			{
			case 8:	// 8 - The file is Deflated (Z_DEFLATED)
#ifdef ZLIB_VERSION
				stream_write_zlib.Init(e->GetOffsetToLocalData());
				return &stream_write_zlib;
#else
				return NULL;
#endif
				break;
			}
		}
		else
		{	stream_write_store.Init(e->GetOffsetToLocalData());
			return &stream_write_store;
		}
	}

	return NULL;
}

#ifdef ZLIB_VERSION
BOOL CFileZip::_zlib_encode(LPCVOID pSrc, UINT SrcLen, LPVOID pDst, UINT& DstLen)
{
	z_stream defstrm;

	defstrm.zalloc = Z_NULL;
	defstrm.zfree = Z_NULL;
	defstrm.opaque = Z_NULL;
	defstrm.avail_in = SrcLen;
	defstrm.next_in = (Bytef*)pSrc;
	defstrm.next_out = (Bytef*)pDst;
	defstrm.avail_out = DstLen;

	if(deflateInit2(&defstrm, Z_DEFAULT_COMPRESSION, Z_DEFLATED, -15, 8, Z_DEFAULT_STRATEGY)!= Z_OK)
		return FALSE;

	int ret = deflate(&defstrm, Z_FINISH);
	deflateEnd(&defstrm);
	if(ret == Z_NEED_DICT || ret == Z_DATA_ERROR || ret == Z_MEM_ERROR)
		return FALSE;

	DstLen = defstrm.total_out;
	return TRUE;
}

BOOL CFileZip::_zlib_decode(LPCVOID pSrc, UINT SrcLen, LPVOID pDst, UINT& DstLen)
{
	z_stream infstrm;
					
	infstrm.zalloc = Z_NULL;
	infstrm.zfree = Z_NULL;
	infstrm.opaque = Z_NULL;
	infstrm.avail_in = SrcLen;
	infstrm.next_in = (Bytef*)pSrc;
	infstrm.next_out = (Bytef*)pDst;
	infstrm.avail_out = DstLen;
					
	// Inflate using raw inflation
	if (inflateInit2(&infstrm,-15) != Z_OK)return FALSE;
	int ret = inflate(&infstrm, Z_FINISH);
	inflateEnd(&infstrm);
	if(ret == Z_NEED_DICT || ret == Z_DATA_ERROR || ret == Z_MEM_ERROR)
		return FALSE;

	DstLen = infstrm.total_out;
	return TRUE;
}
#endif


BOOL CFileZip::AddFile(const rt::StringA_Ref& pathname, LPCVOID pData, UINT size, const FILETIME& ftime)
{
	ASSERT(_AddingNonZeroFileEntry == NULL);
	UINT size_org = size;
	DWORD CRC32 = 0;
	_crc_checksum(pData,size,&CRC32);

	// compress
	rt::Buffer<BYTE>	compressed;
	if(m_CompressionMode)
	{	
#ifdef ZLIB_VERSION
		if(	compressed.SetSize(max(1024,(UINT)(size*1.2f))) &&
			(size = compressed.GetSize()) &&
			_zlib_encode(pData,size_org,compressed,size) &&
			size < size_org
		)
		{	pData = compressed;
		}
		else
		{	size = size_org;
			compressed.SetSize();
		}
#else
		return FALSE;
#endif
	}

	FileEntry* e = _AddNonZeroFileEntry_Begin(pathname,size,ftime);
	if(e)
	{
		UINT data_pos = e->GetOffsetToLocalData();
		if( m_pZipFile->Seek(data_pos) == data_pos &&
			m_pZipFile->Write(pData,size) == size
		)
		{	e->CRC32 = CRC32;
			e->SizeOriginal = size_org;
			e->Compression = (size == size_org)?0:m_CompressionMode;

			return _AddNonZeroFileEntry_End(TRUE);
		}
		else
		{
			_AddNonZeroFileEntry_End(FALSE);
		}
	}

	return FALSE;
}

void CFileZip::RemoveFile(UINT idx)
{
	ASSERT(!m_IsReadOnly);
	ASSERT(m_FileEntries[idx].p->Signature);
	m_FileEntries[idx].p->Signature = 0;
	m_FileEntries[idx].p->Size += (sizeof(LocalFileHeader)-1) + m_FileEntries[idx].p->FileNameLength;					
}

BOOL CFileZip::IsFileExtractable(UINT idx) const
{
	FileEntry* e = m_FileEntries[idx].p;
#ifdef ZLIB_VERSION
	return e->Compression == 0 || e->Compression == 8;
#else
	return e->Compression == 0;
#endif
}

BOOL CFileZip::ExtractFile(UINT idx, LPVOID pData)
{
	FileEntry* e = m_FileEntries[idx].p;
	LocalFileHeader LFH;
	
	if(	m_pZipFile->Seek(e->OffsetToLocalFileHeader) == e->OffsetToLocalFileHeader &&
		m_pZipFile->Read(&LFH,sizeof(LocalFileHeader)-1) == sizeof(LocalFileHeader)-1 &&
		LFH.Signature == LocalFileHeader::header_magic
	)
	{	// decompress
		DWORD begin = e->OffsetToLocalFileHeader+LFH.FileNameLength+LFH.ExtraFieldLength+sizeof(LocalFileHeader)-1;
		DWORD end = begin + e->Size;

		if(LFH.Compression)
		{
			BOOL succ = FALSE;

			HANDLE  hFileMapping = NULL;
			LPCBYTE  pMapped = NULL;
			LPCBYTE  p = NULL;
			rt::Buffer<BYTE> compressed;

			DWORD align_begin = (begin/_FileMappingBlockSize)*_FileMappingBlockSize;
			DWORD align_offset = begin - align_begin;
			
			if(	_BaseFile.IsOpen() &&
				(hFileMapping = ::CreateFileMapping(_BaseFile,NULL,PAGE_READONLY,0,end,NULL)) &&
				(pMapped = (LPCBYTE)::MapViewOfFile(hFileMapping,FILE_MAP_READ,0,align_begin,e->Size + align_offset))
			)
			{	p = pMapped + align_offset;
			}
			else
			{	_CheckErrorW32;
				if(	compressed.SetSize(e->Size) &&
					m_pZipFile->Seek(begin) == begin &&
					m_pZipFile->Read(compressed,compressed.GetSize()) == compressed.GetSize()
				)
				{	p = compressed;
				}
				else return FALSE; // no memory
			}

			switch(LFH.Compression)
			{
			case 8:	// 8 - The file is Deflated (Z_DEFLATED)
				{	
#ifdef ZLIB_VERSION
					if(_zlib_decode(p,e->Size,pData,(UINT&)e->SizeOriginal))
						succ = TRUE;
#endif
					break;
				}
			}
			
			if(hFileMapping)::CloseHandle(hFileMapping);
			if(pMapped)::UnmapViewOfFile(pMapped);

			if(!succ)return FALSE;
		}
		else
		{	// store mode
			if(	e->Size == e->SizeOriginal &&
				m_pZipFile->Seek(begin) == begin &&
				m_pZipFile->Read(pData,e->Size) == e->Size
			){}
			else return FALSE;
		}
	}

	// decrypt
	
	// CRC check
	{	DWORD sum = 0;
		_crc_checksum(pData,e->SizeOriginal,&sum);
		if(sum != e->CRC32)return FALSE;
	}

	return TRUE;
}

void CFileZip::decompress_stream::Init(UINT total_size, UINT base, DWORD target_CRC)
{
	TotalOutput = total_size;
	Outputed = 0;
	BaseInFile = base;
	Target_CRC = target_CRC;
	CRC = 0;
}

BOOL CFileZip::decompress_stream::CheckCRC(LPCVOID p, UINT sz)
{
	ASSERT(sz + Outputed <= TotalOutput);
	pThis->_crc_checksum(p,sz,&CRC);
	if(sz + Outputed == TotalOutput)
		return CRC == Target_CRC;

	return TRUE;
}

CFileZip::stream_readonly* CFileZip::ExtractFile(UINT idx)
{
	FileEntry* e = m_FileEntries[idx].p;
	LocalFileHeader LFH;
	
	if(	m_pZipFile->Seek(e->OffsetToLocalFileHeader) == e->OffsetToLocalFileHeader &&
		m_pZipFile->Read(&LFH,sizeof(LocalFileHeader)-1) == sizeof(LocalFileHeader)-1 &&
		LFH.Signature == LocalFileHeader::header_magic
	)
	{	// decompress
		DWORD begin = e->OffsetToLocalFileHeader+LFH.FileNameLength+LFH.ExtraFieldLength+sizeof(LocalFileHeader)-1;
		DWORD end = begin + e->Size;

		if(LFH.Compression)
		{
			switch(LFH.Compression)
			{
			case 8:	// 8 - The file is Deflated (Z_DEFLATED)
				{	
#ifdef ZLIB_VERSION
					stream_read_zlib.Init(e->SizeOriginal,begin,e->CRC32, e->Size);
					return &stream_read_zlib;
#endif
					break;
				}
			}
		}
		else
		{	stream_read_store.Init(e->SizeOriginal,begin,e->CRC32);
			return &stream_read_store;
		}
	}

	return NULL;
}

UINT CFileZip::decompress_store::Read(LPVOID p, UINT size)
{
	UINT read = 0;
	if( Outputed < TotalOutput &&
		pThis->m_pZipFile->Seek(BaseInFile + Outputed) == BaseInFile + Outputed &&
		(read = pThis->m_pZipFile->Read(p,size))>0 &&
		CheckCRC(p,read)
	)
	{	Outputed += read;
		return read;
	}
	return 0;
}



UINT CFileZip::GetFileSize(UINT idx) const
{
	return m_FileEntries[idx].p->SizeOriginal;
}

UINT CFileZip::GetFileArchivedSize(UINT idx) const
{
	return m_FileEntries[idx].p->Size;
}

DWORD CFileZip::GetFileAttribute(UINT idx) const
{
	return m_FileEntries[idx].p->FileAttributesOriginal;
}

rt::StringA_Ref	CFileZip::GetFileName(UINT idx) const
{
	return rt::StringA_Ref(m_FileEntries[idx].p->FileName,m_FileEntries[idx].p->FileNameLength);
}

void CFileZip::GetFileTime(UINT idx, FILETIME* t)
{
	DosDateTimeToFileTime(m_FileEntries[idx].p->FileDate,m_FileEntries[idx].p->FileTime,t);
}

BOOL CFileZip::Save()
{
	if( m_CentralDirectoryModified && m_pZipFile->Seek(m_CentralDirectoryStart) == m_CentralDirectoryStart )
	{
		UINT cdsize = 0;
		UINT entry_co = 0;
		for(UINT i=0; i<m_FileEntries.GetSize(); i++)
		{
			if(m_FileEntries[i].p->Signature)
			{
				entry_co++;
				cdsize += m_FileEntries[i].p->GetTotalSize();
				if(m_pZipFile->Write(m_FileEntries[i].p,m_FileEntries[i].p->GetTotalSize())
							   != m_FileEntries[i].p->GetTotalSize()
				)return FALSE;
			}
		}
		
		CentralDirectory cd;
		cd.Signature = cd.header_magic;
		cd.m_uDiskWithCD = 0;
		cd.m_uDiskEntriesNo = (WORD)entry_co;
		cd.m_uEntriesNumber = (WORD)entry_co;
		cd.m_uOffset = m_CentralDirectoryStart;
		cd.m_uSize = cdsize;
		cd.m_uThisDisk = 0;
		cd.m_ZipCommentLength = (UINT)m_ZipComment.GetLength();

		if(	m_pZipFile->Write(&cd,sizeof(CentralDirectory)-1) != sizeof(CentralDirectory)-1 )return FALSE;
		if(	m_ZipComment.GetLength() && 
			m_pZipFile->Write(m_ZipComment,m_ZipComment.GetLength()) != m_ZipComment.GetLength()
		)return FALSE;

		ULONGLONG len = m_pZipFile->Seek(0,rt::_stream::Seek_Current);
		if(!m_pZipFile->SetLength(len))return FALSE;

		m_CentralDirectoryModified = FALSE;
		return TRUE;
	}

	return FALSE;
}

BOOL CFileZip::AddEntryFrom(LPCTSTR pathname_sys, LPCSTR pathname_zip)
{
	ASSERT(_AddingNonZeroFileEntry == NULL);
	FILETIME tt;
	{	
		if(w32::CFile64::GetAttributes(pathname_sys)&FILE_ATTRIBUTE_DIRECTORY)
		{
			VERIFY(w32::CFile64::GetPathTime(pathname_sys,NULL,NULL,&tt));
			return AddZeroSizedEntry(pathname_zip,tt,FILE_ATTRIBUTE_DIRECTORY);
		}

		w32::CFile64 file;
		if(!file.Open(pathname_sys))return FALSE;
		file.GetTime_LastModify(&tt);
		if(!file.GetLength())	// zero sized file
			return AddZeroSizedEntry(pathname_zip,tt,FILE_ATTRIBUTE_ARCHIVE);
	}

	w32::CFileBuffer<BYTE>	file;
	return	file.SetSizeAs(pathname_sys) &&
			AddFile(pathname_zip, file, file.GetSize(), tt);
}

BOOL CFileZip::ExtractEntryTo(UINT fid, LPCTSTR path_sys)
{
	ASSERT(fid < m_FileEntries.GetSize());
	if(!m_FileEntries[fid].p->Signature)return FALSE;	// been deleted

	if(GetFileAttribute(fid)&FILE_ATTRIBUTE_DIRECTORY)
	{
		if(!w32::CFile64::CreateDirectories(path_sys))return FALSE;
	}
	else
	{
		if(GetFileSize(fid))
		{	
			w32::CFileBuffer<BYTE>	file;
			if(!file.SetSizeAs(path_sys,w32::CFile64::Normal_Write,GetFileSize(fid)))return FALSE;
			if(!ExtractFile(fid,file))return FALSE;
		}
		else
		{	w32::CFile64 file;
			if(!file.Open(path_sys,w32::CFile64::Normal_Write))return FALSE;
		}
	}

	FILETIME tt;
	GetFileTime(fid,&tt);
	w32::CFile64::SetPathTime(path_sys,NULL,NULL,&tt);
	return TRUE;
}


BOOL CFileZip::AddAllEntriesFrom(LPCTSTR path_sys, LPCSTR path_zip, BOOL include_subfolders)
{
	ASSERT(_AddingNonZeroFileEntry == NULL);
	UINT syslen = _tcslen(path_sys);

	w32::CFileList	list;

	if(syslen)
	{	ASSERT(path_sys[syslen-1]!='/');
		ASSERT(path_sys[syslen-1]!='\\');

		if(!(w32::CFile64::GetAttributes(path_sys)&FILE_ATTRIBUTE_DIRECTORY))return FALSE;

	
		if(FAILED(list.ParsePathName(	rt::String_Ref(path_sys,syslen) + _T("\\*.*"),
										include_subfolders?w32::CFileList::PATHNAME_FLAG_RECURSIVE:0,
										FILE_ATTRIBUTE_OFFLINE, 0))
		)return FALSE;
	}
	else
	{	syslen = 1;
		if(FAILED(list.ParsePathName(	_T(".\\*.*"),
										include_subfolders?w32::CFileList::PATHNAME_FLAG_RECURSIVE:0,
										FILE_ATTRIBUTE_OFFLINE, 0))
		)return FALSE;
	}

	rt::StringA_Ref prefix(path_zip);
	if(prefix.GetLength())
	{
		if(prefix[prefix.GetLength()-1] == '/' || prefix[prefix.GetLength()-1] == '\\')
			prefix.TruncateRight(1);
	}
	else
	{	syslen++;
	}
		
	
	for(UINT i=0;i<list.GetFileCount();i++)
	{
		LPCTSTR fn = list.GetFilename(i);
		if(!AddEntryFrom(fn, prefix + (LPCSTR)ATL::CT2A(fn+syslen)))
			return FALSE;
	}

	return TRUE;
}

BOOL CFileZip::ExtractAllEntriesTo(LPCSTR path_zip, LPCTSTR path_sys, BOOL include_subfolders)		// extract all files in directory path_zip in zip to path_sys
{
	UINT ziplen = path_zip?strlen(path_zip):0;

	rt::String prefix(path_sys);
	if(	prefix.GetLength() && 
		prefix[prefix.GetLength()-1] != '/' && prefix[prefix.GetLength()-1] != '\\'
	)
	{	prefix += '\\';	}

	rt::StringA outname;
	if(ziplen)
	{
		ASSERT(path_zip[ziplen-1]!='/');
		ASSERT(path_zip[ziplen-1]!='\\');

		rt::StringA zip_prefix;
		zip_prefix = rt::StringA_Ref(path_zip,ziplen) + '/';
	
		for(UINT i=0;i<m_FileEntries.GetSize();i++)
		{
			FileEntry& e = *m_FileEntries[i].p;
			if(	e.Signature && 
				e.FileNameLength > zip_prefix.GetLength() && 
				memcmp(e.FileName,zip_prefix,zip_prefix.GetLength()) == 0
			)
			{
				if(!include_subfolders)
				{
					for(UINT i=zip_prefix.GetLength();i<e.FileNameLength;i++)
						if(e.FileName[i] == '/' || e.FileName[i] == '\\')
							goto SKIP_IT;
				}
				outname = rt::StringA_Ref(e.FileName + zip_prefix.GetLength(), e.FileName + e.FileNameLength);
				if(!ExtractEntryTo(i,prefix + (LPCTSTR)ATL::CA2T(outname)))
					return FALSE;
			}
SKIP_IT:
			continue;
		}
	}
	else
	{
		for(UINT i=0;i<m_FileEntries.GetSize();i++)
		{
			FileEntry& e = *m_FileEntries[i].p;
			if(	e.Signature )
			{
				if(!include_subfolders)
				{
					for(UINT i=0;i<e.FileNameLength;i++)
						if(e.FileName[i] == '/' || e.FileName[i] == '\\')
							goto SKIP_IT2;
				}
				outname = rt::StringA_Ref(e.FileName,e.FileNameLength);
				if(!ExtractEntryTo(i,prefix + (LPCTSTR)ATL::CA2T(outname)))
					return FALSE;
			}
SKIP_IT2:
			continue;
		}
	}

	return TRUE;
}

} // namespace w32
