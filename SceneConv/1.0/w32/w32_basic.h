#pragma once

//////////////////////////////////////////////////////////////////////
// Visual Computing Foundation Classes (VCFC)
//					Jiaping Wang  2006.2
//					e_boris2002@hotmail.com
//
// (C) Copyright Jiaping Wang 2004.
//
// Permission to copy, use, modify, sell and distribute this software
// is granted provided this copyright notice appears in all copies.
// This software is provided "as is" without express or implied
// warranty, and with no claim as to its suitability for any purpose.
//
// Absolutely no warranty!!
//////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////
//  Brief Description
//
//  w32_basic.h
//
//  core components of OS
//
//////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////
// Revise History
//
// Inital version	2006.6.8		Jiaping
// Add CSemaphore	2006.10.20		Jiaping
// + CTimeSpinText	2006.11.3		Jiaping
//
//////////////////////////////////////////////////////////////////////

#include "win32_ver.h"
#include "..\rt\string_type.h"
#include <CommDlg.h>
#include <time.h>
#include <objbase.h>
#include <intrin.h>


namespace w32
{

//////////////////////////////////////////////////////////
// Misc functions
__forceinline HRESULT HresultFromLastError()
{
	HRESULT ret = HRESULT_FROM_WIN32(::GetLastError());
	if(FAILED(ret))
		return ret;
	else
		return E_FAIL;
}

__forceinline void GoExecutableFileDirectory()
{	TCHAR fn[MAX_PATH+1];
	fn[0] = 0;
	::GetModuleFileName(NULL,fn,MAX_PATH);
	LPTSTR p=_tcsrchr(fn,_T('\\'));
	if(p)*p=0;
	::SetCurrentDirectory(fn);
}

__forceinline UINT NumberOfProcessor()
{	SYSTEM_INFO si;
	GetSystemInfo(&si);
	return si.dwNumberOfProcessors;
}

__forceinline BOOL WaitForThreadEnding(HANDLE hThread, UINT time_wait_ms = INFINITE)
{
	Sleep(0);
	DWORD exitcode;
	while(time_wait_ms > (UINT)100)
	{	if(!::GetExitCodeThread(hThread,&exitcode) || exitcode != STILL_ACTIVE)return TRUE;
		Sleep(100);
		if(time_wait_ms!=INFINITE)time_wait_ms -= 100;
	}
	return FALSE;
}


__forceinline BOOL IsValidAddress_Read(LPCVOID p,int size){ return !IsBadReadPtr(p,size); }
__forceinline BOOL IsValidAddress_Write(LPVOID p,int size){ return !IsBadWritePtr(p,size); }
__forceinline BOOL IsValidAddress_ReadWrite(LPVOID p,int size){ return IsValidAddress_Read(p,size) && IsValidAddress_Write(p,size); }
__forceinline BOOL IsValidAddress_Execute(FARPROC p){ return !IsBadCodePtr(p); }

template<typename t_Val>
__forceinline BOOL IsPointerValid(t_Val* p,int len=1)
{ return IsValidAddress_ReadWrite(p,len*sizeof(t_Val)); }

template<typename t_Val>
__forceinline BOOL IsPointerValid(const t_Val* p,int len=1)
{ return IsValidAddress_Read(p,len*sizeof(t_Val)); }

class CSystemAwake
{
public:
	CSystemAwake()
	{
		SetThreadExecutionState(ES_CONTINUOUS | ES_SYSTEM_REQUIRED | ES_AWAYMODE_REQUIRED);
	}
	~CSystemAwake()
	{
		SetThreadExecutionState(ES_CONTINUOUS);
	}
};

///////////////////////////////////////
// Process/Thread Priority
class CPriority
{public:	enum
			{	High	= 0,
				Normal	= 1,
				Low		= 2
};
static __forceinline void SetCurrentThread(DWORD tp=w32::CPriority::Low){ ::SetThreadPriority(::GetCurrentThread(), tp); }
static __forceinline void SetCurrentProcess(DWORD tp=w32::CPriority::Low)
{	DWORD cp[3] = {ABOVE_NORMAL_PRIORITY_CLASS,NORMAL_PRIORITY_CLASS,BELOW_NORMAL_PRIORITY_CLASS};
	::SetPriorityClass(::GetCurrentProcess(), cp[tp] );
}
};


//////////////////////////////////////
// CTimeMeasure
class CTimeMeasure
{
protected:
	double FreqInv;
	LARGE_INTEGER StartTime;
	LARGE_INTEGER LastPauseTime;
public:
	CTimeMeasure()
	{	LARGE_INTEGER Freq;
		Freq.QuadPart = 1;
		LastPauseTime.QuadPart = 0;
		QueryPerformanceFrequency(&Freq);
		FreqInv = 1000.0/(double)(Freq.QuadPart);
	}
	__forceinline void Start(){ QueryPerformanceCounter(&StartTime);	}
	__forceinline void Pause(BOOL do_pause = TRUE)
	{	if(do_pause)
		{	if(LastPauseTime.QuadPart==0)QueryPerformanceCounter(&LastPauseTime); }
		else
		{	if(LastPauseTime.QuadPart!=0)
			{	LARGE_INTEGER s;
				QueryPerformanceCounter(&s);
				StartTime.QuadPart += (s.QuadPart - LastPauseTime.QuadPart);
				LastPauseTime.QuadPart = 0;
			}
		}
	}
	__forceinline double Snapshot() const // in ms
	{	if(LastPauseTime.QuadPart==0)
		{
			LARGE_INTEGER s;
			QueryPerformanceCounter(&s);
			return (LONGLONG(s.QuadPart - StartTime.QuadPart))*FreqInv;
		}
		else return (LONGLONG(LastPauseTime.QuadPart - StartTime.QuadPart))*FreqInv;
	}
};


/////////////////////////////////////
// CTime64
enum tagDATEFMT
{	DATEFMT_MMDDYYYY = 0x010002,
	DATEFMT_YYYYMMDD = 0x020100
};


template<bool _local = true>
class CTime64
{
protected:
	struct tm	time_struct;

public:
	void	LoadCurrentTime(){ *this = _time64(NULL); }
	CTime64(){ /* ZeroMemory(&time_struct,sizeof(time_struct));*/ }
	CTime64(const __time64_t tm){ *this = tm; }
	CTime64(const FILETIME& ft){ *this = ft; }
	CTime64(int mon, int day, int year, int hour = 0, int min = 0, int sec = 0){ SetDateTime(mon,day,year,hour,min,sec); }
	operator const tm& () const{ return time_struct; }
	operator tm& (){ return time_struct; }

	const __time64_t operator =(const __time64_t ttt)
	{	if(_local){ _localtime64_s(&time_struct,&ttt); }
		else{ _gmtime64_s(&time_struct,&ttt); }
		return ttt;
	}
	const FILETIME operator =(const FILETIME& ft)
	{	__time64_t x = FileTimeToTime64(ft);
		if(_local){ _localtime64_s(&time_struct,(__time64_t*)&x); }
		else{ _gmtime64_s(&time_struct,(__time64_t*)&x); }
		return ft;
	}
	__time64_t	MakeTime()const { return _mktime64((tm*)&time_struct); }
	FILETIME	MakeFileTime()const { return Time64ToFileTime(_mktime64((tm*)&time_struct)); }
	operator	__time64_t () const { return MakeTime(); }
	operator	FILETIME () const { return MakeFileTime(); }
	int		GetSecond()const{ return time_struct.tm_sec; }
	int		GetMinute()const{ return time_struct.tm_min; }
	int		GetHour()const{ return time_struct.tm_hour; }
	int		GetDayOfMonth()const{ return time_struct.tm_mday; }
	int		GetMonth()const{ return time_struct.tm_mon+1; }
	int		GetYear()const{ return time_struct.tm_year+1900; }
	int		GetDayOfWeek()const{ return time_struct.tm_wday; }
	int		GetDayOfYear()const{ return time_struct.tm_yday; }
	int		GetDayOfMonthMax()const{ return GetDayOfMonthMax(GetMonth(),GetYear()); }
	WORD	GetDosDate()const{ return (WORD)(GetDayOfMonth() | (GetMonth()<<5) | ((GetYear()-1980)<<9)); }
	WORD	GetDosTime()const{ return (WORD)((GetSecond()>>1) | (GetMinute()<<5) | (GetHour()<<11)); }
	
	int		Difference_GetDay(const CTime64<_local>& y)const { return (int)_difftime64(MakeTime(),y.MakeTime()) / (24*3600); }  // this - y
	int		Difference_GetSecond(const CTime64<_local>& y)const { return (int)(0.5+_difftime64(MakeTime(),y.MakeTime())); }  // this - y
	int		Difference_GetMillisecond(const CTime64<_local>& y)const { return (int)(0.5 + 1000*_difftime64(MakeTime(),y.MakeTime())); }  // this - y

	BOOL	IsDaylightSaving()const{ return time_struct.tm_isdst; }

	BOOL	SetDosTime(WORD date, WORD timex)
	{
		return SetDateTime((date>>5)&0xf,date&0x1f,(date>>9) + 1980,timex>>11,(timex>>5)&0x3f,timex&0x1f);
	}
	BOOL	SetDateTime(int mon, int day, int year, int hour = 0, int min = 0, int sec = 0, int day_of_week = 1)
	{	
		if(	sec<0 || sec>59 || min<0 || min>59 || hour<0 || hour>23 || 
			day<1 || day>31 || mon<1 || mon>12 || day_of_week < 1 || day_of_week > 7
		)return FALSE;
		
		time_struct.tm_sec = sec;	
		time_struct.tm_min = min;
		time_struct.tm_hour = hour;
		time_struct.tm_mday = day;
		time_struct.tm_mon = mon-1;
		time_struct.tm_year = year-1900;

		time_struct.tm_wday = day_of_week;
		time_struct.tm_yday = 0;
		time_struct.tm_isdst = 0;

		return TRUE;
	}
	static BOOL	IsLeapYear(int year){ return ((year&0x3)==0) && (year%100) || ((year%400)==0); }
	static int	GetDayOfMonthMax(int mon, int year)
	{	static const int dm[] = {31,28,31, 30,31,30, 31,31,30, 31,30,31};
		return dm[mon-1] + (int)(IsLeapYear(year)&&(mon==2));
	}
	static __time64_t FileTimeToTime64(const FILETIME& ft)
	{	return (*((__time64_t*)&ft))/10000000 - 11644473600;	}
	static FILETIME Time64ToFileTime(const __time64_t& tt)
	{	__time64_t ret = (tt + 11644473600)*10000000;	return *((FILETIME*)&ret); }
	static int ParseInternetWeekday(LPCSTR b)
	{	static const int wday_hash[15] = { 0, 0, 3, 4, 6, 5, 0, 0, 0, 0, 1, 7, 0, 2, 0 };
		return wday_hash[(b[0]+b[1]+b[2])%15];
	}
	static int ParseInternetMonth(LPCSTR b)
	{	static const int month_hash[21] = { 10, 5, 9, 0, 0, 7, 0, 6, 1, 0, 0, 0, 8, 11, 0, 3, 12, 2, 4, 0, 0 };
		return month_hash[(b[0]+b[1]+b[2])%21];
	}
	static LPCSTR	GetMonthName(int month)
	{	ASSERT(month <= 12 && month > 0);
		static const LPCSTR month_name = 
		"Jan\0Feb\0Mar\0Apr\0May\0Jun\0Jul\0Aug\0Sep\0Oct\0Nov\0Dec\0";
		return &month_name[(month-1)*4];
	}
	static LPCSTR	GetDayOfWeekName(int weekday)
	{	ASSERT(weekday <= 7 && weekday > 0);
		static const LPCSTR day_name = "Sun\0Mon\0Tue\0Wed\0Thu\0Fri\0Sat\0";
		return &day_name[(weekday-1)*4];
	}
	LPCSTR	GetMonthName() const { return GetMonthName(time_struct.tm_mon+1); }
	LPCSTR	GetDayOfWeekName() const { return GetDayOfWeekName(time_struct.tm_wday+1); }
	template<typename t_Ch>
	rt::StringT_Ref<t_Ch> ToString_Time(rt::StringT<t_Ch>& date_string) const // HH:MM:SS
	{	rt::_meta_::toS<t_Ch> hh(time_struct.tm_hour);	hh.RightAlign(2,(t_Ch)'0');
		rt::_meta_::toS<t_Ch> mm(time_struct.tm_min);	mm.RightAlign(2,(t_Ch)'0');
		rt::_meta_::toS<t_Ch> ss(time_struct.tm_sec);	ss.RightAlign(2,(t_Ch)'0');
		date_string = hh + rt::StringT_Ref<t_Ch>(":",1) + mm + ':' + ss;
		return date_string;
	}
	template<typename t_Ch>
	rt::StringT_Ref<t_Ch> ToString(rt::StringT<t_Ch>& date_string, tagDATEFMT format = DATEFMT_MMDDYYYY) const
	{	rt::_meta_::toS<t_Ch> v[3] = { GetDayOfMonth(), GetMonth(), GetYear() };
		v[0].RightAlign(2,(t_Ch)'0'); v[1].RightAlign(2,(t_Ch)'0'); v[2].RightAlign(4,(t_Ch)'0');
		date_string = v[(format>>16)&0xff] + rt::StringT_Ref<t_Ch>("/",1) + v[(format>>8)&0xff] + '/' + v[format&0xff];
		return date_string;
	}
	void FillInternetTimeFormat(LPSTR buf)	// RFC 2822 format, buf size > 30, zero-terminated
	{
		ASSERT(!_local);
		sprintf(buf,"%s, %02d %s %04d %02d:%02d:%02d GMT",
				GetDayOfWeekName(),		GetDayOfMonth(),		GetMonthName(),
				GetYear(),				GetHour(),				GetMinute(),
				GetSecond());
	}
	BOOL ParseInternetTimeFormat(LPCSTR b)	// RFC 2822 format
	{	//Tue, 15 Nov 1994 12:45:26 GMT
		//0123456789012345678901234567890
		//          1         2 
		ASSERT(!_local);
		return b[3] == ',' && b[19] == ':' && b[22] == ':' && 
			   b[26] == 'G' && b[27] == 'M' && b[28] == 'T' &&
			   SetDateTime(	ParseInternetMonth(&b[8]),
							(b[5]-'0')*10 + b[6] - '0',
							(b[12]-'0')*1000 + (b[13]-'0')*100 + (b[14]-'0')*10 + b[15]-'0',
							(b[17]-'0')*10 + b[18] - '0',
							(b[20]-'0')*10 + b[21] - '0',
							(b[23]-'0')*10 + b[24] - '0',
							ParseInternetWeekday(b)
							);
	}
};
template<class t_Ostream, bool _local>
t_Ostream& operator<<(t_Ostream& Ostream, const CTime64<_local>& x)
{	
	if(sizeof(t_Ostream::char_type) == sizeof(CHAR))
	{
		Ostream<<x.GetMonth()<<('/')<<x.GetDayOfMonth()<<('/')<<x.GetYear()<<(' ')<<
		x.GetHour()<<(':')<<x.GetMinute()<<(':')<<x.GetSecond();
		if(!_local){ Ostream<<(' ')<<('G')<<('M')<<('T'); }
		return Ostream; 
	}
	if(sizeof(t_Ostream::char_type) == sizeof(WCHAR))
	{
		Ostream<<x.GetMonth()<<L'/'<<x.GetDayOfMonth()<<L'/'<<x.GetYear()<<L' '<<
		x.GetHour()<<L':'<<x.GetMinute()<<L':'<<x.GetSecond();
		if(!_local){ Ostream<<L' '<<L'G'<<L'M'<<L'T'; }
		return Ostream; 
	}
}

/////////////////////////////////////
// CDate32, representing only days
#pragma pack(1)
struct CDate32
{
	TYPETRAITS_DECL_IS_AGGREGATE(true);
	UINT	_Date; // year in 0xffff0000, month in 0xff00, day in 0xff

	CDate32(){}
	CDate32(const CDate32& x){ _Date = x._Date; }
	CDate32(int year, int mon = 0, int day = 0){ SetDate(year, mon, day); }
	template<bool _local>
	CDate32(const w32::CTime64<_local>& tm){ SetDate(tm.GetYear(),tm.GetMonth(),tm.GetDayOfMonth()); }

	int		GetYear() const { return _Date>>16; }
	int		GetMonth() const { return (_Date>>8)&0xff; }
	int		GetDay() const { return _Date&0xff; }
	BOOL	IsMonthEqual(const CDate32& x) const { return ((_Date^x._Date)&0xffffff00) == 0; }

	void	SetYear(int y){ ASSERT(y>0); _Date = (_Date&0xffff) | (y<<16); }
	void	SetMonth(int m){ ASSERT(m>=0&&m<=13); _Date = (_Date&0xffff00ff) | (m<<8); }
	void	SetDay(int d){ ASSERT(d>=0&&d<=32); _Date = (_Date&0xffffff00) | d; }
	template<typename t_Ch>
	BOOL	FromString(const t_Ch* date_string, UINT format = DATEFMT_MMDDYYYY)
	{	int v[3] = { 0,0,0 };
		while(isdigit(*date_string)){ v[0] = v[0]*10 + (*date_string - (t_Ch)('0'));	date_string++; } date_string++; 
		while(isdigit(*date_string)){ v[1] = v[1]*10 + (*date_string - (t_Ch)('0'));	date_string++; } date_string++; 
		while(isdigit(*date_string)){ v[2] = v[2]*10 + (*date_string - (t_Ch)('0'));	date_string++; }
		if(v[0]&&v[1]&&v[2]){ SetDate(v[format&0xff],v[(format>>8)&0xff],v[(format>>16)&0xff]); return TRUE; }else return FALSE;
	}
	template<typename t_Ch>
	rt::StringT_Ref<t_Ch> ToString(rt::StringT<t_Ch>& date_string, UINT format = DATEFMT_MMDDYYYY) const
	{	rt::_meta_::toS<t_Ch> v[3] = { GetDay(), GetMonth(), GetYear() };
		v[0].RightAlign(2,(t_Ch)'0'); v[1].RightAlign(2,(t_Ch)'0'); v[2].RightAlign(4,(t_Ch)'0');
		date_string = v[(format>>16)&0xff] + rt::StringT_Ref<t_Ch>((const t_Ch*)"/\x0\x0",1) + v[(format>>8)&0xff] + (t_Ch)'/' + v[format&0xff];
		return date_string;
	}
	void	LoadCurrentDate(bool _local = true)
	{	__time64_t ttt = _time64(NULL);
		tm time_struct;
		if(_local){ _localtime64_s(&time_struct,&ttt); }
		else{ _gmtime64_s(&time_struct,&ttt); }
		SetDate(time_struct.tm_year+1900,time_struct.tm_mon+1,time_struct.tm_mday);
	}
	void	SetDate(int year, int mon, int day){ _Date = (year<<16) | (mon<<8) | day; }
	template<bool _local>
	void	SetDate(const w32::CTime64<_local>& tm){ SetDate(tm.GetYear(),tm.GetMonth(),tm.GetDayOfMonth()); }
	template<bool _local>
	void	GetTime64(w32::CTime64<_local>& tm) const { tm.SetDateTime(GetMonth(),GetDay(),GetYear()); }
		
	const CDate32& operator = (const CDate32& i){ _Date = i._Date; return i; }
	bool operator < (const CDate32& x) const { return _Date < x._Date; }
	bool operator == (const CDate32& x) const { return _Date == x._Date; }
	bool operator != (const CDate32& x) const { return _Date != x._Date; }
	bool operator > (const CDate32& x) const { return _Date > x._Date; }
	bool operator >= (const CDate32& x) const { return _Date >= x._Date; }
	bool operator <= (const CDate32& x) const { return _Date <= x._Date; }
	int	 operator - (const CDate32& x) const
	{	w32::CTime64<> th(GetMonth(),GetDay(),GetYear(),10);
		w32::CTime64<> y(x.GetMonth(),x.GetDay(),x.GetYear());
		return th.Difference_GetDay(y);
	}
	CDate32& operator ++(int)
	{	if(GetDay() != w32::CTime64<>::GetDayOfMonthMax(GetMonth(),GetYear()))
		{	_Date++;	}
		else
		{	
			if(GetMonth()!=12)
			{	
				_Date = (_Date&0xffffff00) + 0x101;
			}
			else
			{	_Date = (_Date&0xffff0000) + 0x10101;
			}
		}

		return *this;
	}
	CDate32& operator -- (int)
	{	if(GetDay() != 1)
		{	_Date--;	}
		else
		{	
			if(GetMonth() !=1 )
			{	
				_Date = (_Date&0xffffff00) - 0x100 + w32::CTime64<>::GetDayOfMonthMax(GetMonth()-1,GetYear());
			}
			else
			{	_Date = (_Date&0xffff0000) - 0x10000 + 31 + (12<<8);
			}
		}

		return *this;
	}
	CDate32 NextMonth() const // day will be copied, maybe invalid
	{	int m = GetMonth();
		return CDate32(GetYear() + m/12, 1 + m%12, GetDay());
	}
	CDate32 LastMonth() const // day will be copied, maybe invalid
	{	int m = GetMonth();
		return CDate32(GetYear() - (13-m)/12, 1 + (10+m)%12, GetDay());
	}
};
#pragma pack()
template<class t_Ostream>
t_Ostream& operator<<(t_Ostream& Ostream, const CDate32& x)
{	
	if(sizeof(t_Ostream::char_type) == sizeof(CHAR))
	{
		Ostream<<x.GetMonth()<<('/')<<x.GetDay()<<('/')<<x.GetYear();
		return Ostream; 
	}
	if(sizeof(t_Ostream::char_type) == sizeof(WCHAR))
	{
		Ostream<<x.GetMonth()<<L'/'<<x.GetDay()<<L'/'<<x.GetYear();
		return Ostream; 
	}
}

namespace _meta_
{
	template<typename t_Val>
	static	t_Val*	_seek_symbol_end(t_Val* start)
	{ for(;(*start>=0x2d || *start=='#' || *start<0) && *start!='/' && *start!='<' && *start!='>' && *start!='=' && *start!='[' && *start!=']' ;start++);  return start; }
	// Be sure extended-ascii, ':', '.', '#' is not a symbol_end !!

	template<typename t_Val>
	static	t_Val*	_skip_whitespace(t_Val* start) { while(*start>0 && *start <=' ')start++; return start; }
}

} // namespace w32


namespace w32
{

///////////////////////////////////////////
// Classes for synchronization multi-threading application
#define EnterCCSBlock(x) w32::CCriticalSection::_CCS_Holder MARCO_JOIN(_CCS_Holder_,__COUNTER__)(x);

#pragma warning(disable:4512) // assignment operator could not be generated
class CCriticalSection
{
public:
	class _CCS_Holder
	{	const CCriticalSection& _CCS;
	public:
		__forceinline _CCS_Holder(const CCriticalSection& so, BOOL lock_it = TRUE)
			:_CCS(so){ if(lock_it)_CCS.Lock(); }
		__forceinline ~_CCS_Holder(){ _CCS.Unlock(); }
	};
protected:
	CRITICAL_SECTION hCS;
public:
	CCriticalSection(){ InitializeCriticalSection(&hCS); }
	~CCriticalSection(){ DeleteCriticalSection(&hCS); }
	__forceinline void Lock() const{ EnterCriticalSection(rt::_CastToNonconst(&hCS)); }
	__forceinline void Unlock() const{ LeaveCriticalSection(rt::_CastToNonconst(&hCS)); }
#if(_WIN32_WINNT >= 0x0400)
	__forceinline BOOL TryLock() const{ return TryEnterCriticalSection(rt::_CastToNonconst(&hCS)); }
#endif
};
#pragma warning(default:4512) // assignment operator could not be generated

class CAtomLocker  //  a no waiting CriticalSection
{
	volatile LONG	_iAtom;
public:
	__forceinline CAtomLocker(){ _iAtom = 0; }
	__forceinline void Reset(){ _iAtom = 0; }
	__forceinline BOOL TryLock()	// must call Unlock ONCE, or Reset, after returns TRUE
	{	LONG ret = _InterlockedIncrement(&_iAtom);
		if(ret == 1)return TRUE;
		_InterlockedDecrement(&_iAtom);
		return FALSE;
	}
	__forceinline void Unlock(){ _InterlockedDecrement(&_iAtom); }
#ifdef _DEBUG
	__forceinline ~CAtomLocker(){ ASSERT(_iAtom == 0);  }
#endif
};

class CEvent
{
protected:
	HANDLE	hEvent;
public:
	CEvent(BOOL AutoReset = FALSE,LPCTSTR pName = NULL){ VERIFY(hEvent = ::CreateEvent(NULL,!AutoReset,FALSE,pName)); }
	~CEvent(){ ::CloseHandle(hEvent); }
	__forceinline BOOL WaitSignal(DWORD Timeout = INFINITE){ return WaitForSingleObject(hEvent,Timeout) == WAIT_OBJECT_0; }
	__forceinline BOOL IsSignaled(){ return WaitSignal(0); }
	__forceinline void Set(){ VERIFY(::SetEvent(hEvent)); }
	__forceinline void Reset(){ VERIFY(::ResetEvent(hEvent)); }
	__forceinline void Pulse(){ VERIFY(::PulseEvent(hEvent)); }
};

class CSemaphore
{
protected:
	HANDLE	hSemaphore;
public:
	CSemaphore(LONG InitCount = 0,LPCTSTR pName = NULL){ VERIFY(hSemaphore = ::CreateSemaphore(NULL,InitCount,LONG_MAX,pName)); }
	~CSemaphore(){ ::CloseHandle(hSemaphore); }
	__forceinline BOOL WaitSignal(UINT count=1, DWORD Timeout = INFINITE)
	{	while(count--)
		{	if(WaitForSingleObject(hSemaphore,Timeout) == WAIT_OBJECT_0){}
			else{ return FALSE; }
		} return TRUE;
	}
	__forceinline BOOL IsSignaled(){ return WaitSignal(0); }
	__forceinline void Notify(UINT count=1){ VERIFY(ReleaseSemaphore(hSemaphore,count,NULL)); }
};

class CMutex
{
protected:
	HANDLE	hMutex;
public:
	CMutex(LPCTSTR pName = NULL){ VERIFY(hMutex = ::CreateMutex(NULL,FALSE,pName)); }
	~CMutex(){ ::CloseHandle(hMutex); }
	__forceinline void Lock() const{ VERIFY(WAIT_OBJECT_0==::WaitForSingleObject(hMutex,INFINITE)); }
	__forceinline void Unlock() const{ VERIFY(::ReleaseMutex(hMutex)); }
	__forceinline BOOL TryLock() const{ return WAIT_OBJECT_0==::WaitForSingleObject(hMutex,0); }
};

struct ThreadUsingCOM
{
	ThreadUsingCOM(){ VERIFYH(::CoInitialize(NULL)); }
	~ThreadUsingCOM(){ ::CoUninitialize(); }
};

struct NewGUID:public ::GUID
{
	NewGUID(){ ::UuidCreate(this); }
};

//Common File Dialog Helper
#define USE_FILE_DIALOG_DN(_ParentWnd,_FileNameFilter,_DefaultName) \
		TCHAR	_DFOD_Fn[MAX_PATH+1]; \
		_tcscpy(_DFOD_Fn,_DefaultName); \
		OPENFILENAME ofn; \
		ZeroMemory(&ofn,sizeof(ofn)); \
		ofn.lStructSize = sizeof(OPENFILENAME); \
		ofn.hwndOwner = _ParentWnd; \
		ofn.lpstrFilter = _FileNameFilter; \
		ofn.nFilterIndex = 1; \
		ofn.Flags = OFN_ENABLESIZING|OFN_OVERWRITEPROMPT|OFN_EXPLORER; \
		ofn.lpstrFile = _DFOD_Fn; \
		ofn.nMaxFile = MAX_PATH; \
		TCHAR _LastCurDir[MAX_PATH+1]; \
		LPTSTR _NameReturn; \
		VERIFY(::GetCurrentDirectory(MAX_PATH,_LastCurDir));

#define USE_FILE_DIALOG(_ParentWnd,_FileNameFilter)		USE_FILE_DIALOG_DN(_ParentWnd,_FileNameFilter,_T("\0"))
																
#define GET_FILENAME(_IsForRead)	((_NameReturn = ((LPTSTR)((_IsForRead?GetOpenFileName(&ofn):GetSaveFileName(&ofn))?_DFOD_Fn:NULL))),VERIFY(SetCurrentDirectory(_LastCurDir)),_NameReturn)
#define APPEND_EXTNAME(PathName,FileExt) { LPTSTR _p_=_tcsrchr(PathName,_T('\\')); if(!_p_)_p_=PathName; if(!_tcsrchr(_p_,_T('.')))_tcscat(_p_,FileExt); }
#define REPLACE_EXTNAME(PathName,FileExt) { LPTSTR _p_=_tcsrchr(PathName,_T('\\')); if(!_p_)_p_=PathName; if(_p_=_tcsrchr(_p_,_T('.'))){_tcscpy(_p_,FileExt);}else{_tcscat(PathName,FileExt);} }
#define REMOVE_EXTNAME(PathName) { LPTSTR _p_=_tcsrchr(PathName,_T('\\')); if(!_p_)_p_=PathName; if(_p_=_tcsrchr(_p_,_T('.'))){*_p_=0;} }

template<typename T> static T _max(const T&a,const T&b) {
	if(a > b) 
		return a;
	else
		return b;
}
template<typename T> static T _min(const T&a,const T&b) {
	if(a < b) 
		return a;
	else
		return b;
}

}


