#include "stdafx.h"
#include "ssh_sftp.h"

#include <libssh2\include\libssh2.h>

#ifdef _DEBUG
	#pragma comment(lib,"libssh2d.lib")
#else
	#pragma comment(lib,"libssh2.lib")
#endif

#pragma comment(lib,"libeay32.lib")



void w32::CSSHSession::InitSSHLib()
{
	libssh2_init(0);	// will generate fake memory leak
}

void w32::CSSHSession::TermSSHLib()
{
	libssh2_exit();
}

int w32::CSSHSession::_WaitSocket(int socket_fd, LPVOID session, int timeout_msec)
{
    struct timeval timeout;
    int rc;
    fd_set fd;
    fd_set *writefd = NULL;
    fd_set *readfd = NULL;
    int dir;
 
    timeout.tv_sec = timeout_msec/1000;
    timeout.tv_usec = 1000*(timeout_msec%1000);
 
    FD_ZERO(&fd);
    FD_SET(socket_fd, &fd);
 
    /* now make sure we wait in the correct direction */ 
    dir = libssh2_session_block_directions((LIBSSH2_SESSION*)session);
	 
    if(dir & LIBSSH2_SESSION_BLOCK_INBOUND)readfd = &fd;
    if(dir & LIBSSH2_SESSION_BLOCK_OUTBOUND)writefd = &fd;
 
    rc = select(socket_fd + 1, readfd, writefd, NULL, &timeout);
 
    return rc;
}

w32::CSSHSession::CSSHSession()
{
	m_ShellChannel = NULL;
	m_SSH = NULL;
	ZeroMemory(m_HostFingerPrint,17);
	m_ShellChannelMode = SSH_SHELLMODE_DECODE_CHAR|SSH_SHELLMODE_SKIP_CTRLSEQ;
}

w32::CSSHSession::~CSSHSession()
{
	Disconnect();
}

BOOL w32::CSSHSession::Connect(SOCKET tcp_connected, int timeout_sec)
{
	ASSERT(!IsConnected());

	m_Connection.Attach(tcp_connected);
	return _ConnectSSH(timeout_sec);
}


BOOL w32::CSSHSession::_ConnectSSH(int timeout_sec)
{
	_CommandResponse.Empty();
	_LastCommand.Empty();
	_ProcessedCommandResponse.Empty();
	_CommandResponseComplete = FALSE;

	LIBSSH2_SESSION* ssh = NULL;
	
	int rc;
	if(ssh = libssh2_session_init())
	{
		libssh2_session_set_blocking(ssh, false);

		timeout_sec *= 2;
		for(int i=0;i<timeout_sec;i++)
		{
			rc = libssh2_session_startup(ssh,m_Connection);
			if(rc == 0)goto SSH_CONNECTED;
			if(rc != LIBSSH2_ERROR_EAGAIN)goto SSH_ERROR;
			_WaitSocket(m_Connection,ssh,500);
		}
		
SSH_CONNECTED:
		LPCSTR server_sign;
		if(server_sign = libssh2_hostkey_hash(ssh, LIBSSH2_HOSTKEY_HASH_MD5))
			memcpy(m_HostFingerPrint,server_sign,16);
		else
			ZeroMemory(m_HostFingerPrint,16);

		m_SSH = ssh;
		return TRUE;
	}

SSH_ERROR:
	Disconnect();
	return FALSE;
}

BOOL w32::CSSHSession::Connect(const w32::CInetAddr& target, int timeout_sec)
{
	ASSERT(!IsConnected());

	w32::CInetAddr addr = target;
	if(!addr.GetPort())
		addr.SetPort(22); // default port 22

	return	m_Connection.Create(w32::CInetAddr()) &&
			m_Connection.ConnectTo(addr) &&
			_ConnectSSH(timeout_sec);
}

BOOL w32::CSSHSession::Authenticate(LPCSTR user, LPCSTR pass, int timeout_sec)
{
	ASSERT(IsConnected());

	timeout_sec*=4;
	for(int i=0;i<timeout_sec;i++)
	{
		int rc = libssh2_userauth_password((LIBSSH2_SESSION*)m_SSH,user,pass);
		if(rc == 0)
		{	return _ShellMode_Begin();
		}
		if(rc != LIBSSH2_ERROR_EAGAIN)return FALSE;
		_WaitSocket(m_Connection,m_SSH,250);
	}

	return FALSE;
}

BOOL w32::CSSHSession::IsConnected() const
{
	if(m_Connection.IsEmpty())
	{
		ASSERT(!m_SSH);
		return FALSE;
	}
	else
	{
		ASSERT(m_SSH);
		return TRUE;
	}
}

BOOL w32::CSSHSession::WaitResponse(int timeout_sec, BOOL ignore_uncomplete_response, int response_end_tag)
{
	ASSERT(m_ShellChannel);

	if(_CommandResponseComplete || ignore_uncomplete_response)
	{
		_CommandResponseComplete = FALSE;
		_CommandResponse.Empty();
	}
	
	timeout_sec *= 4;

	int rc_err = LIBSSH2_ERROR_EAGAIN;
	int rc = LIBSSH2_ERROR_EAGAIN;
	do
	{
		char buffer[1024];
		if(rc == LIBSSH2_ERROR_EAGAIN)
		{	// read stdout
			for(;;)
			{	
				rc = libssh2_channel_read((LIBSSH2_CHANNEL*)m_ShellChannel, buffer, sizeof(buffer));
				if( rc > 0 )
					_CommandResponse += rt::StringA_Ref(buffer,rc);
				else break;
			}
		}

		if(rc_err == LIBSSH2_ERROR_EAGAIN && (SSH_SHELLMODE_ERROR_STREAM&m_ShellChannelMode))
		{	// read stderr
			for(;;)
			{	
				rc_err = libssh2_channel_read_stderr((LIBSSH2_CHANNEL*)m_ShellChannel, buffer, sizeof(buffer));
				if( rc_err > 0 )
					_CommandResponse += rt::StringA_Ref(buffer,rc_err);
				else break;
			}
		}

		/* loop until EOF */ 
		if(rc == LIBSSH2_ERROR_EAGAIN || rc_err == LIBSSH2_ERROR_EAGAIN)
		{
			if(	_CommandResponse.GetLength()>2 && 
				*((WORD*)((&_CommandResponse.Last())-1)) == response_end_tag
			)
			{	_CommandResponseComplete = TRUE;
				_ProcessCommandResponse();
				return TRUE;
			}
			if(timeout_sec>0)
				_WaitSocket(m_Connection,m_SSH,250);
			timeout_sec--;
		}
		else
			return (rc == 0) || (rc_err == 0);
	}
	while(timeout_sec >=0);

	_ProcessCommandResponse();
	return FALSE;
}

BOOL w32::CSSHSession::SendCommand(LPCSTR command)	// wait until response
{
	ASSERT(m_ShellChannel);

	int len = strlen(command);
	if(m_ShellChannelMode&SSH_SHELLMODE_NO_ECHO)
		_LastCommand = rt::StringA_Ref(command,strlen(command)-1);

	for(int i=0;i<20;i++)
	{
		int rc = libssh2_channel_write((LIBSSH2_CHANNEL*)m_ShellChannel,command,len);
		if(len == rc)break;
		ASSERT(rc < 0);
		if(LIBSSH2_ERROR_EAGAIN != rc)return FALSE;
		_WaitSocket(m_Connection,m_SSH,250);
	}

	return TRUE;
}

BOOL w32::CSSHSession::_ShellMode_Begin() // by default it is "vanilla"
{
	ASSERT(!m_ShellChannel);

	LIBSSH2_CHANNEL* channel = NULL;
	_LastCommand.Empty();

	for(int i=0;i<40;i++)
	{
		channel = libssh2_channel_open_session((LIBSSH2_SESSION*)m_SSH);
		if(channel)goto CHANNEL_READY;
		if(libssh2_session_last_error((LIBSSH2_SESSION*)m_SSH,NULL,NULL,0) != LIBSSH2_ERROR_EAGAIN)
			return FALSE;
		_WaitSocket(m_Connection,m_SSH,250);
	}

	return FALSE;

CHANNEL_READY:
	ASSERT(channel);
	BOOL ret = FALSE;

	for(int i=0;i<40;i++)
	{
		int rc = libssh2_channel_request_pty(channel, "vanilla");
		if(0 == rc)break;
		if(LIBSSH2_ERROR_EAGAIN != rc)goto CHANNEL_ERROR;
		_WaitSocket(m_Connection,m_SSH,250);
	}

	for(int i=0;i<40;i++)
	{
		int rc = libssh2_channel_shell(channel);
		if(0 == rc)break;
		if(LIBSSH2_ERROR_EAGAIN != rc)goto CHANNEL_ERROR;
		_WaitSocket(m_Connection,m_SSH,250);
	}

	m_ShellChannel = channel;
	return TRUE;

CHANNEL_ERROR:
	_ShellMode_End();
	return FALSE;
}

void w32::CSSHSession::_ShellMode_End()
{
	ASSERT(m_ShellChannel);

	for(int i=0;i<40;i++)
	{
		int rc = libssh2_channel_close((LIBSSH2_CHANNEL*)m_ShellChannel);
		if(rc == 0)
		{	libssh2_channel_free((LIBSSH2_CHANNEL*)m_ShellChannel);
			break;
		}
		if(rc != LIBSSH2_ERROR_EAGAIN)break;
		_WaitSocket(m_Connection,m_SSH,250);
	}

	m_ShellChannel = NULL;
	_LastCommand.Empty();
	_CommandResponse.Empty();
}


void w32::CSSHSession::Disconnect()
{
	ZeroMemory(m_HostFingerPrint,16);

	if(m_ShellChannel)
		_ShellMode_End();

	if(m_SSH)
	{
		libssh2_session_disconnect((LIBSSH2_SESSION*)m_SSH,"done, bye.");
		libssh2_session_free((LIBSSH2_SESSION*)m_SSH);
		m_SSH = NULL;
	}

	if(!m_Connection.IsEmpty())
		m_Connection.Close();
}

void w32::CSSHSession::_ProcessCommandResponse()
{
	int el = 0;
	if(	!_LastCommand.IsEmpty() &&
		_LastCommand.GetLength() <= _CommandResponse.GetLength() &&
		_LastCommand == _CommandResponse.Truncate(0,_LastCommand.GetLength())
	)
	{
		el = _LastCommand.GetLength() + 2;
	}
	
	if(0 == (m_ShellChannelMode&(SSH_SHELLMODE_DECODE_CHAR|SSH_SHELLMODE_SKIP_CTRLSEQ)))
	{
		_CommandResponseOut = rt::StringA_Ref(_CommandResponse.Begin() + el, _CommandResponse.End());
	}
	else
	{
		_ProcessedCommandResponse.SetLength(_CommandResponse.GetLength());
		LPSTR out = _ProcessedCommandResponse.Begin();
		LPCSTR p = _CommandResponse.Begin() + el;
		LPCSTR end = _CommandResponse.End();

		while(p<end)
		{
			int a = *p;
			if((SSH_SHELLMODE_DECODE_CHAR&m_ShellChannelMode) && a == '\\')
			{
				if(p[1] >='0' && p[1] <='7')
				{
					int ch;
					int len = rt::StringA_Ref(p+1,p+4).ToNumber<int,8>(ch);
					ASSERT(len);
					*out = (char)ch; out++;
					p += len+1;
					continue;
				}

				*out = p[1]; out++;
				p+=2;
				continue;
			}
			if(((SSH_SHELLMODE_SKIP_CTRLSEQ)&m_ShellChannelMode) && a == 0x1b)	// ESC
			{
				if(p[1] == '[')
				{
					if(p[2] >= '0' && p[2] <= '9')	// (ESC)['num' ... m
					{
						LPCSTR t = p+2;
						for(;*t!='m' && t<end;t++);
						p = t+1;
						continue;
					}
					if(p[2] == 'm' || p[2] == 'K')	// (ESC)[m  (ESC)[K
					{
						p+=3;
						continue;
					}
				}

				ASSERT(0);
			}

			*out = a;  out++;
			p++;
		}

		_ProcessedCommandResponse.SetLength(out - _ProcessedCommandResponse.Begin());
		_CommandResponseOut = _ProcessedCommandResponse;
	}
}

void w32::CSSHSession::EncodeFilename(rt::StringA_Ref& in, rt::StringA& out)
{
	out.Empty();
	char a[4] = {'\\'};
	for(UINT i=0;i<in.GetLength();i++)
	{
		int c = in[i];
		if(c>=' ' && c<=0x7e)
		{
			if(c != '\'' && c != '"' && c != '`' && c != '~')
			{
				if(	c == '|' || c == '\\' || c == '@' || c == '>' || c == '=' || c == ' ' )
				{
					a[1] = c;
					out += rt::StringA_Ref(a,2);
				}
				else out += (char)c;
			}
		}
		else
		{
			a[1] = ((c>>6)&0x7) + '0';
			a[2] = ((c>>3)&0x7) + '0';
			a[3] = ((c   )&0x7) + '0';
			out += rt::StringA_Ref(a,4);
		}
	}
}

