#pragma once

//////////////////////////////////////////////////////////////////////
// Visual Computing Foundation Classes (VCFC)
//					Jiaping Wang  2006.2
//					e_boris2002@hotmail.com
//
// (C) Copyright Jiaping Wang 2004.
//
// Permission to copy, use, modify, sell and distribute this software
// is granted provided this copyright notice appears in all copies.
// This software is provided "as is" without express or implied
// warranty, and with no claim as to its suitability for any purpose.
//
// Absolutely no warranty!!
//////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////
//  Brief Description
//
//  ssh_sftp.h
//
//  SSH/SFTP client
//
//////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////
// Revise History
//
// Inital version			2011.8.24		Jiaping
//
//////////////////////////////////////////////////////////////////////

#include "inet.h"


namespace w32
{

enum _tagShellMode
{
	SSH_SHELLMODE_NO_ECHO			= 0x0001,
	SSH_SHELLMODE_DECODE_CHAR		= 0x0002,	// decode \xxx into real binary code (handle non-ansi charactors)
	SSH_SHELLMODE_SKIP_CTRLSEQ		= 0x0004,
	SSH_SHELLMODE_ERROR_STREAM		= 0x0008,	// include error stream

};

class CSSHSession // not thread-safe!!, don't use the same instance across threads.
{
	rt::StringA		_ProcessedCommandResponse;
protected:
	w32::CSocket	m_Connection;
	LPVOID			m_SSH;
	char			m_HostFingerPrint[17];	// MD5 with zero-terminated

	LPVOID			m_ShellChannel;
	DWORD			m_ShellChannelMode;

	rt::StringA		_LastCommand;
	rt::StringA		_CommandResponse;
	BOOL			_CommandResponseComplete;
	rt::StringA_Ref	_CommandResponseOut;

	static int		_WaitSocket(int socket_fd, LPVOID session, int timeout_msec);
	BOOL			_ConnectSSH(int timeout_sec);
	void			_ProcessCommandResponse();

	BOOL			_ShellMode_Begin();
	void			_ShellMode_End();

public:
	enum _endtag
	{	SSH_RESPONSE_END_TAG = 0x2023	// '# '
	};
	static  void EncodeFilename(rt::StringA_Ref& in, rt::StringA& out);
	static  void InitSSHLib();
	static  void TermSSHLib();

public:
	CSSHSession();
	~CSSHSession();
	BOOL	Connect(const w32::CInetAddr& target, int timeout_sec = 30);
	BOOL	Connect(SOCKET tcp_connected, int timeout_sec = 30);
	void	Disconnect();
	BOOL	IsConnected() const;
	BOOL	Authenticate(LPCSTR user, LPCSTR pass, int timeout_sec = 10);
	void	SetShellMode(int mode){ m_ShellChannelMode = mode; }
	
	LPCSTR				GetCommandResponse(){ return _CommandResponseOut; }
	rt::StringA_Ref		GetCommandResponseString(){ return _CommandResponseOut; }

	BOOL				SendCommand(LPCSTR command);
	BOOL				WaitResponse(int timeout_sec = 10, BOOL ignore_preivous_uncomplete_response = FALSE, int response_end_tag = SSH_RESPONSE_END_TAG);
	
	LPCSTR	GetServerFingerPrint() const { return m_HostFingerPrint; }
};



} // namespace w32