#pragma once
//////////////////////////////////////////////////////////////////////
// Visual Computing Foundation Classes (VCFC)
//					Jiaping Wang  2006.2
//					e_boris2002@hotmail.com
//
// (C) Copyright Jiaping Wang 2004.
//
// Permission to copy, use, modify, sell and distribute this software
// is granted provided this copyright notice appears in all copies.
// This software is provided "as is" without express or implied
// warranty, and with no claim as to its suitability for any purpose.
//
// Absolutely no warranty!!
//////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////
//  Brief Description
//
//  sqlite_plus.h
//
//  Warpping class of Sqlite
//
//  Library Dependence: sqlite.lib
//
//////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////
// Revise History
//
// Inital version	2010.7.1		Jiaping
//
//////////////////////////////////////////////////////////////////////

#include "w32_basic.h"

#ifdef _DEBUG
	#pragma comment(lib,"sqlite_d.lib")
#else
	#pragma comment(lib,"sqlite.lib")
#endif


struct sqlite3;
struct sqlite3_stmt;

namespace w32
{

class SQLite_Database;

enum _tagColumnType
{
	CTYPE_LARGEINTEGER	= 1, // SQLITE_INTEGER,
	CTYPE_DOUBLE		= 2, // SQLITE_FLOAT,
	CTYPE_TEXT			= 3, // SQLITE_TEXT,
	CTYPE_BLOB			= 4, // SQLITE_BLOB,
	CTYPE_NULL			= 5, // SQLITE_NULL,
};

namespace _meta_
{

struct _Query
{
	SQLite_Database*		m_pDatabase;
	sqlite3_stmt*	m_pStatement;
	_Query(SQLite_Database* pDb, sqlite3_stmt* pStmt){ m_pDatabase = pDb; m_pStatement = pStmt; }
	_Query(const _Query&);
	~_Query();
};

}	// namespace _meta_
}   // namespace w32


namespace w32
{

class SQLite_ResultSet:public _meta_::_Query
{
private:
	SQLite_ResultSet();
	SQLite_ResultSet(const SQLite_ResultSet&);

public:
	SQLite_ResultSet(const _Query&);
	~SQLite_ResultSet(){};
	BOOL		IsOk() const;
	void		Close();

	UINT	GetColumnCount();
	DWORD	GetColumnType(int iCol);	// return one of _tagColumnType
	LPCTSTR	GetColumnName(int iCol);
	LPCSTR	GetColumnNameA(int iCol);
	LPCWSTR	GetColumnNameW(int iCol);

	double	GetFloat(int iCol);
	int		GetInteger(int iCol);
	__int64	GetLargeInteger(int iCol);
	LPCBYTE	GetBinary(int iCol, int* pSize);
	LPCTSTR	GetText(int iCol);
	LPCSTR	GetTextA(int iCol);
	LPCWSTR	GetTextW(int iCol);

	BOOL	Next(); // false: last record reached, or failure
	BOOL	Rewind();

	void	ExportExcel(LPCTSTR fn);	// CSV format
};


class SQLite_Database
{
protected:
	sqlite3*	m_pDatabase;
	int			m_LastError;

public:
	SQLite_Database();
	~SQLite_Database();

	BOOL			Execute(LPCTSTR sql_statement);	// non-select statement w/o returning records
	_meta_::_Query	Query(LPCTSTR sql_statement);	// select statement only

	
	BOOL	Open(LPCTSTR filename);
	void	Close();
	LPCTSTR	GetLastErrorMessage();
};

} // namespace sqlite


#include "..\_static_lib\link_w32_sqlite.h"