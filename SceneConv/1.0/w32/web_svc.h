#pragma once

//////////////////////////////////////////////////////////////////////
// Base classes for Network Services
//					Jiaping Wang  2007.5
//					e_boris2002@hotmail.com
//
// Copyright (C) Jiaping Wang 2007.
//
// Permission to copy, use, modify, sell and distribute this software
// is granted provided this copyright notice appears in all copies.
// This software is provided "as is" without express or implie// warranty, and with no claim as to its suitability for any purpose.
//
// Absolutly no garanty!!
//////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////
//  Brief Description
//
//  www_svr.h
//  light weight www server (serving static content)
//
//  Library Dependence: inet.lib
//
//////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////
// Revise History
//
// Inital version	2010.10.15		Jiaping
//
//////////////////////////////////////////////////////////////////////

#include "inet.h"
#include "w32_basic.h"
#include "file_64.h"
#include "inet_svc.h"
#include "..\rt\algorithm.h"


#define ENABLE_SERV_STATISTIC	// static library was compiled with ENABLE_SERV_STATISTIC enabled
#include <intrin.h>

#define S_REQUEST_NOT_HANDLED	((HRESULT)0xff)
#define MakeFCCTag(a,b,c,d)	((#@a) | ((#@b)<<8) | ((#@c)<<16) | ((#@d)<<24))

namespace w32
{

namespace inet_mod
{

struct _httpd;
struct _webhostd;
struct _httpd_request_handler;
struct _httpd_tlb;

struct WebServExtension
{
	 virtual BOOL	OnRegistered(_httpd* pHttpd, LPCSTR name, int extension_id) = 0;	// call when the mod is registered
	 virtual void	OnWebsitesReloaded() = 0;
	 virtual BOOL	BindRequestHandler(_webhostd* pWebCore, LPCSTR uri_mapped, LPCSTR handle_subname) = 0; // call to bind handler to specific uri in document tree
	 virtual LPVOID BindThreadLocalObject() = 0;	// call to create object for each HTTP request handling thread, which can be obtained when RequestHandler is called
	 virtual void	UnbindThreadLocalObject(LPVOID p) = 0;
};


// HRESULT WINAPI OnRequest(_httpd_tlb* pTLB_in, LPVOID cookie);
typedef HRESULT (WINAPI *FUNC_DOCUMENT_ONREQUEST)(_httpd_tlb* pTLB_in, LPVOID cookie);
typedef void (WINAPI *FUNC_RELEASE_REQUEST_HANDLER)(LPVOID cookie, LPCSTR pURI, UINT URI_Len);

struct _httplog
{	// debug log
	BOOL			DebugLogEnabled;
	virtual LPSTR	DebugLogClaimWriting(UINT size) = 0;
	virtual void	DebugLogFinalizeWritten(LPSTR p, UINT len) = 0;
	virtual UINT	GetDeletionLatency() = 0;
};

struct _httpd:public _httplog
{
	// websites
	virtual UINT	GetWebsiteCount() = 0;
	virtual LPCTSTR GetExtensionWorkingDirectory(UINT website_idx) = 0;
	virtual LPCSTR	GetExtensionConfig(UINT website_idx) = 0;
	virtual LPCTSTR GetServConfigFilename() = 0;

	virtual LPCSTR	GetWebsiteName(UINT index) = 0;
};

struct _webhostd:public _httplog
{
	//virtual void	Endpoint_UpdateIndex() = 0;	// call after a seris of Endpoint_AddXXX or Endpoint_Remove calls
	//virtual void	Endpoint_AddAlias(LPCSTR uri, LPCSTR uri_mapped_to, BOOL is_prefix) = 0;
	//virtual BOOL	Endpoint_AddSingleFile(LPCSTR uri, LPCTSTR fn, LPCSTR MIME) = 0; // need call UpdateIndex() when done
	//virtual BOOL	Endpoint_AddZipFile(LPCSTR uri_suffix, LPCTSTR zip_fn, BOOL merge) = 0; // need call UpdateIndex() when done
	//virtual BOOL	Endpoint_AddDirectory(LPCSTR uri_suffix, LPCTSTR directory, BOOL merge) = 0; // need call UpdateIndex() when done
	//virtual BOOL	Endpoint_Remove(LPCSTR uri) = 0;
	virtual BOOL	Endpoint_AddHandler(LPCSTR uri, const w32::inet_mod::_httpd_request_handler& hd) = 0; // need call UpdateIndex() when done
	virtual BOOL	Endpoint_IsExist(LPCSTR uri) = 0;

	virtual LPCSTR	Website_GetName() = 0;
};

struct _ContentRange
{	ULONGLONG	start;	// 0-based
	ULONGLONG	end;
	ULONGLONG	total;
};

struct _httpd_tlb
{
	_webhostd*		webhost;

	/////////////////////////////////////////////////////////
	// Helpers
	template<class T>
	BOOL SendChunkResponse(const T& s){ return SendChunkResponse(s.Begin(),s.GetLength()*sizeof(typename rt::TypeTraits<T>::t_Element)); }
	template<class T>
	BOOL SendChunkResponse_EncodeHTML(const T& s){ return SendChunkResponse_EncodeHTML(s.Begin(),s.GetLength()*sizeof(typename rt::TypeTraits<T>::t_Element)); }
	BOOL SendChunkResponse(char c){ return SendChunkResponse(&c,1); }
	template<class T>
	BOOL	SendRedirection(UINT code, const T& s)
	{		ASSERT_STATIC(sizeof(T::t_Char) == 1);	// StringA, StringExpr<char>  only
			return SendRedirection(code,s.Begin(),s.GetLength()); 
	}

	/////////////////////////////////////////////////////////
	// HTTP request
	virtual const w32::inet::HttpRequest* GetParsedRequest() = 0;
	virtual const sockaddr_in& GetRemoteAddress() = 0;
	virtual const sockaddr_in& GetLocalAddress() = 0;
	virtual LPVOID GetThreadLocalObject(int extension_id) = 0;	// passed by OnRegistered

	/////////////////////////////////////////////////////////
	// HTTP response, All SendXXX is non-blocking
	// All functions can only be called within request handlers
	virtual BOOL	SendResponseHeadOnly(UINT code = 404, BOOL close_session = TRUE) = 0;
	virtual BOOL	SendRedirection(UINT code, LPCSTR redirected_url, int url_len) = 0;
	virtual BOOL	SendResponse(LPCBYTE pData, SIZE_T len, UINT code = 200, BOOL close_session = FALSE, LPCSTR content_type = NULL) = 0; // response w/ Content-Length
	virtual BOOL	SendChunkResponse_EncodeHTML(LPCSTR PlainText, UINT len) = 0;
	virtual BOOL	SendResponse_NoCopy(LPCBYTE pData, SIZE_T len, UINT code = 200, BOOL close_session = FALSE, LPCSTR content_type = NULL) = 0; // response w/ Content-Length
	// pData will not be copied, so caller must make sure the memory is available during sending, be aware sending is asynchronized
						
	virtual void	SendChunkResponse_Begin() = 0;
	virtual BOOL	SendChunkResponse(LPCVOID pData, UINT len) = 0;
	virtual BOOL	SendChunkResponse_End(UINT code = 200, BOOL close_session = FALSE, LPCSTR content_type = NULL) = 0;

	// Call before SendXXX
	virtual void	AppendResponseHeader_ContentRange(const _ContentRange& r) = 0;
	virtual void	AppendResponseHeader_Maxage(int maxage_sec) = 0;
	virtual void	AppendResponseHeader_ETag(LPCSTR tag, int maxage_sec) = 0;
	virtual void	AppendResponseHeader_NoCache() = 0;
	virtual void	AppendResponseHeader_ContentDisposition(LPCSTR filename) = 0;
	virtual BOOL	AppendResponseHeader(LPCSTR head, UINT len) = 0;
	// END of HTTP response
	/////////////////////////////////////////////////////////

	///////////////////////////////////////////////
	// virtual website related
	virtual	DWORD	GetWebSiteID() = 0;
	virtual LPCSTR	GetWebSiteName() = 0;
};


struct _httpd_request_handler
{
	int								ExtensionIndex;
	LPVOID							Cookie;
	FUNC_DOCUMENT_ONREQUEST			pOnRequest;
	FUNC_RELEASE_REQUEST_HANDLER	pReleaseRequestHandler;
	
	TYPETRAITS_DECL_IS_AGGREGATE(true);

	_httpd_request_handler(FUNC_DOCUMENT_ONREQUEST func, LPVOID cookie, FUNC_RELEASE_REQUEST_HANDLER rel){ pOnRequest = func;	Cookie = cookie; pReleaseRequestHandler = rel; }
	_httpd_request_handler(){ ZeroMemory(this,sizeof(_httpd_request_handler)); }
};


template<class T>
__forceinline void DebugLog(_httplog* core, const T &x)
{	ASSERT(core->DebugLogEnabled);
	static const bool is_char = rt::IsTypeSame<typename T::t_Char, char>::Result;
	ASSERT_STATIC(is_char);
	SIZE_T len = x.GetLength();

	if(!w32::Console::IsCreated)
	{	LPSTR p = core->DebugLogClaimWriting(len+2);
		if(p)
		{	VERIFY(len == x.CopyTo(p));
			*((WORD*)&p[len]) = 0x0a0d;	//"\r\n"
			core->DebugLogFinalizeWritten(p,len+2);
		}
	}
	else
	{	LPSTR p = core->DebugLogClaimWriting(len+2+1);
		if(p)
		{	VERIFY(len == x.CopyTo(p));
			*((WORD*)&p[len]) = 0x0a0d;	//"\r\n"
			p[len+2] = '\0';
			if(len < 79)
				_CheckDump(p);
			else
				_CheckDump(rt::StringA_Ref(p,30) <<" ... "<<(p+(len - (79 - 30 - 5))));
			core->DebugLogFinalizeWritten(p,len+2);
		}
	}
}

}} // namespace w32::inet_mod


namespace w32
{

namespace inet
{

namespace _meta_
{

enum _EntryFlag
{	EF_REMOVED	= 0x0001
};

struct IndexedEntry_Base
{
	static const UINT	_hash_space_multiplier = 4;	// rehash(_hash_space_multiplier * document_count)

	template<typename t_Entry>
	friend class IndexedEntryTyped;

	struct	__Entry
	{	__Entry* Next;
		DWORD Flag;
		BOOL IsRemoved() const { return Flag&EF_REMOVED; }
	};

private:
	typedef void (*ReleaseFunc)(__Entry*, UINT TTL);
	typedef rt::StringA_Ref (*GetNameFunc)(__Entry*);

	ReleaseFunc			_release_delayed;
	GetNameFunc			_getname;

	LPVOID				m_DocumentIndex;			// map URI to document			, std::hash_map
	LPVOID				m_DocumentPrefixIndex;		// map URI prefix to document	, rt::StringPrefectHashMap
	BOOL				m_DocumentIndexDirty;
	BOOL				m_DocumentPrefixIndexDirty;

protected:
	__Entry*			m_HeadDocument;
	UINT				m_IndexedDocumentCount;
	UINT				m_DeletionDelay;		// ms
	TCHAR				m_HiddenFileInitial;
	class _alias
	{	friend struct IndexedEntry_Base;
		rt::Buffer<char>	_buf;
	public:
		BOOL			_Prefix;
		rt::StringA_Ref	Name;
		rt::StringA_Ref Alias;
		void Set(LPCSTR org_name, LPCSTR alias);
		_alias(){ _Prefix = FALSE; }
	};
	rt::BufferEx<_alias>	m_Aliases;
	
	~IndexedEntry_Base(){ ASSERT(m_HeadDocument == NULL); }
	IndexedEntry_Base();
	///////////////////////////////////////////////////
	// Entry manipulation
	void				Add(const rt::StringA_Ref& name, LPVOID x);	// existing uri will be overwrited
	BOOL				Remove(const rt::StringA_Ref& name);
	void				RemoveSubname(const rt::StringA_Ref& name_suffix);
	void				RemoveAll();
	void				ClearAliases();
	void				AddAlias(LPCSTR org_name, LPCSTR alias, BOOL prefix = FALSE);	// alias will overwrite non-alias uri
	UINT				UpdateIndex();	// call after entry manipulation to be realized. return number of entries removed
	__Entry*			GetFirst(){ return m_HeadDocument; }
	__Entry*			GetNext(__Entry* x){ return x?x->Next:NULL; }

	///////////////////////////////////////////////////
	
	//					Following are thread-safe functions.
	//					Upon the same instance, there can be multiple threads calling Get and only one thread call manipulation functions
	__Entry*			Get(const rt::StringA_Ref& name) const;
};

// t_Entry should have:
// dervied from Entry						
// static void Release(t_Entry*, UINT TTL);	// delete this, delayed
// static rt::StringA_Ref GetName(t_Entry*);// get name
template<typename t_Entry>
class IndexedEntryTyped:protected _meta_::IndexedEntry_Base	// // partial thread-safe based on delayed deletion
{
public:
	struct Entry{ t_Entry* Next; DWORD Flag; };
	IndexedEntryTyped(){ _release_delayed = (ReleaseFunc)t_Entry::Release; _getname = (GetNameFunc)t_Entry::GetName; }
	~IndexedEntryTyped(){ RemoveAll(); UpdateIndex(); }

	void				ClearAliases(){ __super::ClearAliases(); }
	void				AddAlias(LPCSTR org_name, LPCSTR alias, BOOL prefix){ __super::AddAlias(org_name,alias,prefix); }

	void				Add(const rt::StringA_Ref& name, t_Entry* x){ ((_meta_::IndexedEntry_Base*)this)->Add(name,x); }
	BOOL				Remove(const rt::StringA_Ref& name){ return ((_meta_::IndexedEntry_Base*)this)->Remove(name); }
	BOOL				Remove(LPCSTR uri){ return Remove(rt::StringA_Ref(uri)); }
	void				RemoveAll(){ return ((_meta_::IndexedEntry_Base*)this)->RemoveAll(); }
	void				RemoveSubname(const rt::StringA_Ref& name_suffix){ ((_meta_::IndexedEntry_Base*)this)->RemoveSubname(name_suffix); }
	UINT				UpdateIndex(){ return ((_meta_::IndexedEntry_Base*)this)->UpdateIndex(); } // return number of entries removed
	//					Following are thread-safe functions.
	//					Upon the same instance, there can be multiple threads calling Get and only one thread call manipulation functions
	t_Entry*			Get(const rt::StringA_Ref& name) const { return (t_Entry*)((_meta_::IndexedEntry_Base*)this)->Get(name); }
	t_Entry*			Get(LPCSTR uri) const { return Get(rt::StringA_Ref(uri)); }
	void				SetDeletionLatency(UINT msec){ ASSERT(msec > 10); m_DeletionDelay = msec; }
	UINT				GetCount(){ return m_IndexedDocumentCount; }

	t_Entry*			GetFirst(){ return (t_Entry*)__super::GetFirst(); }
	t_Entry*			GetNext(t_Entry* x){ return (t_Entry*)__super::GetNext((__Entry*)x); }
};

} // namespace _meta_


class WebSite;
class WebServCore;

class WebThreadLocalBlock:public HttpThreadLocalBlock
{
public:
	struct _TLB_Mod:public w32::inet_mod::_httpd_tlb
	{
		WebThreadLocalBlock* pTLB;

		virtual const w32::inet::HttpRequest* GetParsedRequest();
		virtual const sockaddr_in&	GetRemoteAddress();
		virtual const sockaddr_in&	GetLocalAddress();
		virtual LPVOID				GetThreadLocalObject(int extension_index);

		virtual BOOL	SendResponseHeadOnly(UINT code = 404, BOOL close_session = TRUE);
		virtual BOOL	SendRedirection(UINT code, LPCSTR redirected_url, int url_len);
		virtual BOOL	SendResponse(LPCBYTE pData, SIZE_T len, UINT code = 200, BOOL close_session = FALSE, LPCSTR content_type = NULL);
		virtual BOOL	SendResponse_NoCopy(LPCBYTE pData, SIZE_T len, UINT code = 200, BOOL close_session = FALSE, LPCSTR content_type = NULL);
						
		virtual void	SendChunkResponse_Begin();
		virtual BOOL	SendChunkResponse(LPCVOID pData, UINT len);
		virtual BOOL	SendChunkResponse_EncodeHTML(LPCSTR PlainText, UINT len);
		virtual BOOL	SendChunkResponse_End(UINT code = 200, BOOL close_session = FALSE, LPCSTR content_type = NULL);

		virtual void	AppendResponseHeader_ContentRange(const inet_mod::_ContentRange& r);
		virtual void	AppendResponseHeader_ETag(LPCSTR tag, int maxage_sec);
		virtual void	AppendResponseHeader_Maxage(int maxage_sec);
		virtual void	AppendResponseHeader_NoCache();
		virtual void	AppendResponseHeader_ContentDisposition(LPCSTR filename);
		virtual BOOL	AppendResponseHeader(LPCSTR head, UINT len);

		virtual	DWORD	GetWebSiteID();
		virtual LPCSTR	GetWebSiteName();
	};
	_TLB_Mod				TLB_http_mod;
	int						WebsiteIndex;
	WebSite*				pWebsite;
	rt::Buffer<LPVOID>		ExtensionCookie;
	WebThreadLocalBlock(inet::SocketServCore* pServCore);
};

struct RequestHandler:public w32::inet_mod::_httpd_request_handler
{
	RequestHandler(){};
	RequestHandler(const w32::inet_mod::_httpd_request_handler& x){ pOnRequest = x.pOnRequest;	Cookie = x.Cookie; };
	RequestHandler(w32::inet_mod::FUNC_DOCUMENT_ONREQUEST func, LPVOID cookie){ pOnRequest = func;	Cookie = cookie; }
	__forceinline HRESULT OnRequest(w32::inet_mod::_httpd_tlb * pTLB){ return pOnRequest(pTLB,Cookie); }
};

class ServerNamespace
{
public:
	static const UINT				_etag_length = 12;
	static const UINT				_additional_header_size = _etag_length + 128;
	static const UINT				_inmemory_document_size_max = 128*1024*1024;
	static const rt::StringA_Ref	_MIMEs[19];
	static rt::StringA_Ref			Ext2MIME(const rt::StringA_Ref& ext);
	// support only bmp, png, css, gif, htm, ico, jpg, js, pdf, svg, swf, xap, tif, txt, text, vrml, wav, wave, zip, xml
	// for extension other than listed, the return is unpredicatible.

#pragma pack(1)
	class Document:public _meta_::IndexedEntryTyped<Document>::Entry
	{	friend class ServerNamespace;
		CHAR		AdditionalResponseHeader[_additional_header_size];	// Cache-Control: public, max-age=3600\r\nETag: %s\r\n
		UINT		AdditionalResponseHeaderLen;
		CHAR		ETag[_etag_length];		// = 0xcafebebe for handlers
		__time64_t	LastModified;
		UINT		MaxAge;
		UINT		URI_Len;	// in bytes (zero-terminator included)
	public:
		static void	Release(Document* p, UINT TTL);
		static rt::StringA_Ref GetName(Document* p);

		void		ResetCacheControlHeader();
		void		UpdateCacheControlHeader();
		void		CalculateETag();
		BOOL		IsHandler() const { return (*((DWORD*)ETag) == 0xcafebebe && Length == sizeof(inet::RequestHandler)); }

		rt::StringA_Ref	MIME;
		SIZE_T			Length;		// data_len in bytes
		BYTE			Data[1];	// size = data_len + uri_len + 1
	};
	typedef Document*	LPDocument;
#pragma pack()

protected:
	_meta_::IndexedEntryTyped<Document>	m_Documents;
	TCHAR		m_HiddenFileInitial;
	UINT		m_MaxAgeDefault;
	LPCSTR		GetMIME(const rt::StringA_Ref& filename);
	Document*	_Add(LPCSTR uri, UINT data_len, LPCVOID data = NULL, LPCSTR MIME = NULL);	// overwrite existed one, call UpdateIndex, MIME pointer will be held

public:
	int			stat_DocumentAdded;
	int			stat_DocumentUpdated;
	int			stat_DocumentRemoved;
	int			stat_DocumentTotalSize;
	int			stat_DocumentFileCount;
	int			stat_DocumentHandlerCount;

	void		ResetStatCounters();
	rt::StringA	stat_HandlerNames;
		
public:
	ServerNamespace();
	Document*	Update(BOOL* pReused, LPCSTR uri, UINT data_len, __time64_t ftime,LPCSTR MIME = NULL);

	void		ClearAliases(){ m_Documents.ClearAliases(); }
	void		AddAlias(LPCSTR org_name, LPCSTR alias, BOOL prefix){ m_Documents.AddAlias(org_name,alias,prefix); }

	void		RemoveAll(){ m_Documents.RemoveAll(); }
	BOOL		Remove(const rt::StringA_Ref& name){ return m_Documents.Remove(name); }
	BOOL		Remove(LPCSTR uri){ return Remove(rt::StringA_Ref(uri)); }
	void		FinalUpdateIndex();  // call ResetStatCounters before each FinalUpdateIndex
	void		SetDeletionLatency(UINT ms){ m_Documents.SetDeletionLatency(ms); }
	void		SetHiddenFileInitial(TCHAR hc){ m_HiddenFileInitial = hc; }

	HRESULT		LoadSingleFile(LPCTSTR fn, LPCSTR uri, LPCSTR MIME = NULL, ULONGLONG* pfsz = NULL); // need call UpdateIndex() when done
	HRESULT		LoadZipFile(LPCTSTR zip_fn, LPCSTR uri_suffix, BOOL merge = TRUE, ULONGLONG* pfsz = NULL); // need call UpdateIndex() when done
	HRESULT		LoadDirectory(LPCTSTR directory, LPCSTR uri_suffix, BOOL merge = TRUE, ULONGLONG* pfsz = NULL); // need call UpdateIndex() when done
	HRESULT		LoadHandler(const RequestHandler& hd, LPCSTR uri);
	
	HRESULT		OnRequest(inet::WebThreadLocalBlock* pTLB); // if failed, HTTP response is NOT sent.
	BOOL		IsExist(const rt::StringA_Ref& url) const;
	void		SetDefaultMaxAge(UINT sec){ m_MaxAgeDefault = sec; }
};

enum _tagWebLogOption
{
	WEBLOG_FIELD_REFERER		= 0x1,
	WEBLOG_FIELD_USERAGENT		= 0x2,
	WEBLOG_FIELD_AUTH_USER		= 0x4,	// not yet implemented
};

class WebSite:public w32::inet_mod::_webhostd
{	
	friend class WebServCore;
	struct _Endpoint
	{	rt::StringA		uri;
		rt::StringA		Name;		// name for directory/file/zipfile/handler
		DWORD			Type;		// _tagEndpointType
		UINT			MaxAge;
		ULONGLONG		TotalDataSize;
		BOOL			HasError;
		int				HiddenChar;
	};
public:
	enum _tagEndpointType
	{	EPT_DIRECTORY	= 0x001,
		EPT_SINGLEFILE	= 0x002,
		EPT_ZIPFILE		= 0x003,
		EPT_FILETYPE_MASK	= 0x00f,
		EPT_HANDLER		= 0x101,		// Handler is available
		EPT_ALIAS		= 0x201,
		EPT_PREFIX		= 0x202,
	};
	UINT						Index;
	rt::StringA					Name;
	rt::BufferEx<rt::StringA>	Domains;	// not thread-safe
	rt::BufferEx<_Endpoint>		Endpoints;
	rt::String					m_ExtensionWorkingSpace;	// path to local filesystem
	
	rt::String					m_WebLogPath;
	DWORD						m_WebLogFieldsOption;
	w32::CParallelFileWriter	m_LogFile;
	UINT						m_WebLogSaveInterval;
	UINT						m_WebLogBufferSize;

	rt::StringA					m_ExtensionConfig;

	w32::inet::WebServCore*		GetWebServCore();

	//////////////////////////////////////////
	// _webhostd and _httplog
	w32::inet_mod::_httpd*		_phttpd;
	virtual LPSTR		DebugLogClaimWriting(UINT size);
	virtual void		DebugLogFinalizeWritten(LPSTR p, UINT len);
	virtual UINT		GetDeletionLatency();
	// _webhostd
	//virtual void		Endpoint_UpdateIndex();
	//virtual void		Endpoint_AddAlias(LPCSTR uri, LPCSTR uri_mapped_to, BOOL is_prefix);
	//virtual BOOL		Endpoint_AddSingleFile(LPCSTR uri, LPCTSTR fn, LPCSTR MIME);
	//virtual BOOL		Endpoint_AddZipFile(LPCSTR uri_suffix, LPCTSTR zip_fn, BOOL merge);
	//virtual BOOL		Endpoint_AddDirectory(LPCSTR uri_suffix, LPCTSTR directory, BOOL merge);
	//virtual BOOL		Endpoint_Remove(LPCSTR uri);
	virtual BOOL		Endpoint_AddHandler(LPCSTR uri, const w32::inet_mod::_httpd_request_handler& hd);
	virtual BOOL		Endpoint_IsExist(LPCSTR uri);

	virtual LPCSTR		Website_GetName();

public:
	WebSite(w32::inet_mod::_httpd* serv_core);
	~WebSite();
	void			WebLogRemoveField(DWORD remove){ m_WebLogFieldsOption &= ~(remove); }
	void			WebLogAddField(DWORD add){ m_WebLogFieldsOption |= add; }
	void			StartWebLog(const rt::String_Ref& path, w32::CDate32* pForceNewDate = NULL);
	BOOL			IsWebLogRunning() const { return m_LogFile.IsOpen(); }
	w32::CDate32	GetLogStartDate() const { return m_LogFileStart; }
	HRESULT			RefreshEndpoints(WebServCore& core);
	void			SetDeletionLatency(UINT ms){ m_Documents.SetDeletionLatency(ms); }
	HRESULT			OnRequest(WebThreadLocalBlock* pTLB){ return m_Documents.OnRequest(pTLB); }

protected:
	rt::StringA		m_LogFileName;
	w32::CDate32	m_LogFileStart;
	ServerNamespace	m_Documents;

	BOOL			_StartWebLog(LPCTSTR filename, UINT buffer_size = 1024*1024, UINT writedown_interval = 1000, 
								 DWORD flag = CFile64::Access_Write|CFile64::Share_Read|CFile64::Mode_OpenAny|CFile64::Flag_WriteThrough);	// web logging will be start if it is stopped
	void			_StopWebLog();
	void			_SwitchWebLogTo(LPCTSTR filename, BOOL no_wait = TRUE);
	void			_UpdateWebLogInfo(LPCTSTR fn);

};

class WebSites
{	friend class WebServCore;
protected:
	rt::BufferEx<WebSite*>				m_WebSites;
	rt::StringPrefectHashMap<CHAR, int>	m_DomainMap;
	rt::StringPrefectHashMap<CHAR, int>	m_NameMap;

public:
	UINT			GetWebSiteCount() const { return m_WebSites.GetSize(); }
	WebSite*		GetWebSite(UINT index){ return m_WebSites[index]; }
	int				LookupWebSite(const rt::StringA_Ref& name) const;
	int				LookupDomain(const rt::StringA_Ref& domain) const;
};

enum DefaultRequestHandlerType
{
	DRHT_DOCUMENT_NOT_FOUND = 0,
	DRHT_MAX
};

} // namespace inet


namespace inet
{

class WebServCore 
{	
	friend class WebSite;
	friend class WebSites;
	friend struct httpd_ext;
private:
	char			_LogTimePool[28*2];
	LPSTR			_szLogTime_Backup;
	UINT			_DeletionLatency;	// in ms
	WebSites*		_pNewlyWebSites;
	__forceinline	WebSites* _getWebSites(){ return _pNewlyWebSites?_pNewlyWebSites:m_pWebSites; }

protected:
	struct httpd_ext: public ::w32::inet_mod::_httpd
	{	WebServCore*	pThis;
		LPSTR			DebugLogClaimWriting(UINT size);
		void			DebugLogFinalizeWritten(LPSTR p, UINT len);
		LPCTSTR			GetServConfigFilename();
		UINT			GetDeletionLatency();

		UINT			GetWebsiteCount();
		LPCSTR			GetWebsiteName(UINT index);
		LPCTSTR			GetExtensionWorkingDirectory(UINT website_idx);
		LPCSTR			GetExtensionConfig(UINT website_idx);
	};
	httpd_ext			_httpd_ext;

public:
	::w32::inet_mod::_httpd* get_httpd(){ return &_httpd_ext; }

public:
	// Registered Extension
	struct _WebServExt
	{	rt::StringA	name;
		w32::inet_mod::WebServExtension*			mod;
		_WebServExt(){ mod = NULL; }
	};
	rt::BufferEx<_WebServExt>	_Extensions;

protected:
	// WebLog stuffs
	w32::CTime64<>	_LastWeblogProbed;
	HANDLE			m_hUpdateWebLogTimeThread;
	DWORD			m_hUpdateWebLogTimeThreadId;
	LPSTR			m_szLogTime; // [31/May/2009:16:15:07 -0500]
	void			StopAllWebLog();
	HRESULT			_Maintenance_WebLog();
	void			_LogSocketServCoreState(w32::inet::SocketServCore* pCore);

	// Default handlers
	RequestHandler	m_pDefaultRequestHandler[DRHT_MAX];

	// Web site config
	rt::String		m_WebsiteConfigFilename;
	WebSites*		m_pWebSites;

protected:
	w32::CParallelFileWriter*	m_pDebugLogWriter;
	w32::CEvent					m_ShutDownEvent;

public:
	void			SetDebugLogWriter(w32::CParallelFileWriter* p);
	w32::CParallelFileWriter* GetDebugLogWriter(){ return m_pDebugLogWriter; }

public:
	void			UpdateWebLogTime();		// will be called in each second after websites are loaded
	const w32::CTime64<>& GetWebLogTime() const { return _LastWeblogProbed; }
	void			DefaultRequest_DumpWebCore(inet::WebThreadLocalBlock* pTLB);
	void			DefaultRequest_HostNotConfigured(inet::WebThreadLocalBlock* pTLB, LPCSTR host, int host_len);

	static HRESULT	WINAPI DefaultRequest_DocumentNotFound(w32::inet_mod::_httpd_tlb* pTLB, LPVOID cookie);

	void			SetDefaultRequestHandler(DefaultRequestHandlerType type, const RequestHandler& hd);
	void			ResetAllDefaultRequestHandler();

	BOOL			RegisterExtension(LPCSTR ext_name, inet_mod::WebServExtension* pMod);
	const _WebServExt* GetExtension(LPCSTR name) const;
	const _WebServExt* GetExtension(UINT i) const { return &_Extensions[i]; }
	UINT			GetExtensionCount() const { return _Extensions.GetSize(); }

	BOOL			LoadWebSites(LPCTSTR fn_xml);
	void			UnloadWebSites();

	void			Shutdown(){ m_ShutDownEvent.Set(); }
	void			WaitForShutdown(){ m_ShutDownEvent.Reset(); m_ShutDownEvent.WaitSignal(); }

	WebServCore();
	~WebServCore();
	UINT			GetWebsiteCount() const;
	WebSite*		GetWebsite(UINT i);
	UINT			GetWebsiteIndex_ByName(const rt::StringA_Ref& name) const;		// INFINTE for not found
};

} // namespace inet


template<class t_Derived = void, class t_Website = inet::WebSite, class t_TLB = inet::WebThreadLocalBlock, class t_Conn = inet::HttpConnection>
class CWebServ:	public CHttpServ<typename _meta_::_reduce_target_type<t_Derived, CHttpServ<t_Derived,t_TLB,t_Conn> >::t_Result, t_TLB, t_Conn>,
				public inet::WebServCore
{
	typedef HRESULT (*FUNC_ON_REQUEST)(inet::WebThreadLocalBlock*);

	ASSERT_STATIC(COMPILER_INTRINSICS_IS_BASE_OF(inet::HttpThreadLocalBlock,t_TLB));
	ASSERT_STATIC(COMPILER_INTRINSICS_IS_BASE_OF(inet::HttpConnection,t_Conn));
	typedef typename _meta_::_reduce_target_type<t_Derived,CSocketServ>::t_Result t_ServCore;

public:
	HRESULT OnRequestReceived(inet::HttpThreadLocalBlock* pTLB_in)	// can not be overrided
	{
		inet::WebThreadLocalBlock* pTLB = (inet::WebThreadLocalBlock*)pTLB_in;
		HRESULT ret = E_FAIL;

		inet::WebSites*	pWebs = m_pWebSites;
		{	UINT len = 0;
			LPCSTR host;
			if(	pWebs &&
				(host = (LPCSTR)pTLB->Request.GetAdditionalField("Host: ",&len)) &&
				len>=4 &&
				(pTLB->WebsiteIndex = pWebs->LookupDomain(rt::StringA_Ref(host,len))) >=0
			){}
			else
			{	// this Host is NOT configured !!
				if(pTLB->Request.pURI[1] != '@')
				{
					pTLB->WebsiteIndex = -1;
					DefaultRequest_HostNotConfigured(pTLB,host,len);
					ret = E_FAIL;
				}
				else
				{	pTLB->WebsiteIndex = 0;
					ret = ((t_ServCore*)this)->OnRequest((t_TLB*)pTLB);
				}
				goto REQUEST_FINISHED;
			}
		}
		
		inet::WebSite& web = *(pWebs->GetWebSite(pTLB->WebsiteIndex));
		pTLB->TLB_http_mod.webhost = &web;
		pTLB->pWebsite = &web;
		if(web.IsWebLogRunning())
		{
			ASSERT(m_szLogTime);
			// write web log
			UINT refer_len,agent_len;
			LPCBYTE pRefer,pAgent;

			if(inet::WEBLOG_FIELD_REFERER & web.m_WebLogFieldsOption)
			{	pRefer = pTLB->Request.GetAdditionalField("Referer: ",&refer_len);
			}
			else
			{	pRefer = NULL;	refer_len = 1;
			}

			if(inet::WEBLOG_FIELD_USERAGENT & web.m_WebLogFieldsOption)
			{	pAgent = pTLB->Request.GetAdditionalField("User-Agent: ",&agent_len);
			}
			else
			{	pAgent = NULL;	agent_len = 1;
			}

			int hdr_len = pTLB->Request.URI_len + (int)(pTLB->Request.pURI - (LPCSTR)pTLB->pConn->m_RecvBuffer.Begin());

			UINT loglen = 21 + 28 + 2 + hdr_len + pTLB->Request.Query_len + 1 +
						  9 + 2 + 4 + 30 + refer_len + 4 + agent_len + 10 + 100 + 2;
			LPSTR pEntry = (LPSTR)web.m_LogFile.ClaimWriting(loglen);
			if(pEntry)
			{	
				ULONG addr = pTLB->pConn->m_RemoteAddress.GetAddress();
				UINT len =	sprintf(pEntry,"%d.%d.%d.%d - - ",
							addr>>24,(addr>>16)&0xff,(addr>>8)&0xff,(addr)&0xff);
				memcpy(&pEntry[len],m_szLogTime,28);									len+=28;
				*((WORD*)&pEntry[len]) = 0x2220;  /* " \"" */							len+=2;
						
				memcpy(&pEntry[len],pTLB->pConn->m_RecvBuffer,hdr_len);					len+=hdr_len;
				if(pTLB->Request.pQuery)
				{	pEntry[len] = '?';													len++;
					memcpy(&pEntry[len],pTLB->Request.pQuery,pTLB->Request.Query_len);	len+=pTLB->Request.Query_len; 
				}
				pEntry[len] = ' ';
				memcpy(&pEntry[len+1],pTLB->Request.pRemainHeader - 10,8);				len+=9;
				*((WORD*)&pEntry[len]) = 0x2022;  /* "\" " */							len+=2;
				
#ifdef ENABLE_SERV_STATISTIC
				UINT start = GetTickCount();
#endif
				ret = ((t_Website*)&web)->OnRequest((t_TLB*)pTLB);
				if(ret == S_REQUEST_NOT_HANDLED)ret = ((t_ServCore*)this)->OnRequest((t_TLB*)pTLB);
				if(ret == S_REQUEST_NOT_HANDLED)ret = m_pDefaultRequestHandler[inet::DRHT_DOCUMENT_NOT_FOUND].OnRequest(&pTLB->TLB_http_mod);

#ifdef ENABLE_SERV_STATISTIC
				UINT end = GetTickCount();
				if(end >= start)
				{	end -= start;
					if(stat_MaxRequestHandlingTime < (LONG)end)
						stat_MaxRequestHandlingTime = end;
					_InterlockedExchangeAdd(&stat_TotRequestHandlingTime,end);
					_InterlockedIncrement(&stat_NumberOfRequest);
				}
#endif
				if(pTLB->ResponseCode > 0 && pTLB->ResponseCode < 999){}
				else
				{	pTLB->ResponseCode = 999;
					// ASSERT(0);
				}
				itoa(pTLB->ResponseCode,&pEntry[len],10);								len+=3;
				pEntry[len] = ' ';														len++;
				itoa(pTLB->ResponseByteSent,&pEntry[len],10);							while(pEntry[len])len++;
				if(pRefer)
				{	*((WORD*)&pEntry[len]) = 0x2220;  /* " \"" */						len+=2;
					memcpy(&pEntry[len],pRefer,refer_len);								len+=refer_len;
					pEntry[len] = '"';													len++;
				}
				else
				{	*((DWORD*)&pEntry[len]) = 0x222d2220;	/* "-"*/					len+=4;
				}
				if(pAgent)
				{	*((WORD*)&pEntry[len]) = 0x2220;  /* " \"" */						len+=2;
					memcpy(&pEntry[len],pAgent,agent_len);								len+=agent_len;
					pEntry[len] = '"';													len++;
				}
				else
				{	*((DWORD*)&pEntry[len]) = 0x222d2220;	/* "-"*/					len+=4;
				}
				ASSERT(loglen >= len);
				*((WORD*)&pEntry[len]) = 0x0a0d;										len+=2;

				web.m_LogFile.FinalizeWritten((LPBYTE)pEntry,len);

				goto REQUEST_FINISHED;
			}
		}

		ret = ((t_Website*)&web)->OnRequest((t_TLB*)pTLB);
		if(ret == S_REQUEST_NOT_HANDLED)ret = ((t_ServCore*)this)->OnRequest((t_TLB*)pTLB);
		if(ret == S_REQUEST_NOT_HANDLED)ret = m_pDefaultRequestHandler[inet::DRHT_DOCUMENT_NOT_FOUND].OnRequest(&pTLB->TLB_http_mod);

REQUEST_FINISHED:	
		t_Conn* pHttpConn = (t_Conn*)pTLB->pConn;
		pTLB->pConn->RemoveData(pHttpConn->m_RequestPacketSize);
		pHttpConn->Clear();

		ASSERT(ret != S_REQUEST_NOT_HANDLED);
		return ret;
	}

public:
	void EmergencyExit()
	{
		if(GetCurrentThreadId() != m_hUpdateWebLogTimeThreadId)
			SuspendThread(m_hUpdateWebLogTimeThread);
		__super::EmergencyExit();
		WebServCore::StopAllWebLog();
	}
	void Uninitialize()
	{	for(UINT i=0;i<GetThreadCount();i++)
		{	w32::inet::WebThreadLocalBlock& TLB = GetThreadLocalBlock(i);
			TLB.ExtensionCookie.SetSize(_Extensions.GetSize());
			for(UINT e=0;e<_Extensions.GetSize();e++)
				_Extensions[e].mod->UnbindThreadLocalObject(TLB.ExtensionCookie[e]);
		}
		__super::Uninitialize();
		UnloadWebSites();
	}
	BOOL	Initialize(LPCTSTR config_xml)
	{	if(!LoadWebSites(config_xml))
			inet_mod::DebugLog(get_httpd(),rt::StringA_Ref() + "Error occurred in loading websites config.");
		BOOL ret = __super::Initialize(config_xml);
		_LogSocketServCoreState(this);
		m_ShutDownEvent.Reset();
		for(UINT i=0;i<GetThreadCount();i++)
		{	w32::inet::WebThreadLocalBlock& TLB = GetThreadLocalBlock(i);
			TLB.ExtensionCookie.SetSize(_Extensions.GetSize());
			for(UINT e=0;e<_Extensions.GetSize();e++)
				TLB.ExtensionCookie[e] = _Extensions[e].mod->BindThreadLocalObject();
		}
		return ret;
	}
	t_Website* GetWebsite(UINT i){ return (t_Website*)__super::GetWebsite(i); }

	CWebServ(){}
};


} // namespace w32
