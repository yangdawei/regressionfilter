#pragma once

//////////////////////////////////////////////////////////////////////
// TCP/IP Socket classes for conmmucation
//					Jiaping Wang  2007.5
//					e_boris2002@hotmail.com
//
// Copyright (C) Jiaping Wang 2007.
//
// Permission to copy, use, modify, sell and distribute this software
// is granted provided this copyright notice appears in all copies.
// This software is provided "as is" without express or implied
// warranty, and with no claim as to its suitability for any purpose.
//
// Absolutly no garanty!!
//////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////
//  Brief Description
//
//  inet.h
//	Wrapper of winsock APIs
//	light weight HTTP protocal handler
//
//  Library Dependence: inet.lib
//
//////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////
// Revise History
//
// Inital version	2007.6.8		Jiaping
// Inital version	2007.8.4		+ http	Jiaping
//
//////////////////////////////////////////////////////////////////////

#include <Winsock2.h>
#include "..\rt\string_type.h"
#include "..\rt\compact_vector.h"


namespace w32
{

class CInetAddr : public sockaddr_in 
{
public:
	// constructors
	CInetAddr(){ sin_family = AF_INET; sin_port = 0; sin_addr.s_addr = 0; } // Default 0.0.0.0:0
	CInetAddr(const sockaddr& sa) { memcpy(this, &sa, sizeof(sockaddr)); }
	CInetAddr(const sockaddr_in& sin) { memcpy(this, &sin, sizeof(sockaddr_in)); }
	CInetAddr(const ULONG uipAddr_hostbyteorder, const USHORT ushPort = 0); // parms are host byte order
	CInetAddr(LPCTSTR pHostname, USHORT ushPort = 0); // dotted IP addr string

	BOOL SetAsLocal();
	BOOL SetAddress(LPCTSTR pHostname);  // www.xxx.com:80
	void SetAddress(ULONG uipAddr_hostbyteorder = 0){ sin_addr.s_addr = htonl(uipAddr_hostbyteorder); }  //default is 0.0.0.0
	void SetPort(const USHORT ushPort = 0){ sin_port = htons(ushPort); }; //default is zero

	LPCSTR	GetDottedDecimalAddress() const { return inet_ntoa(sin_addr); } // Return the address in dotted-decimal format
	USHORT	GetPort() const	{ return ntohs(sin_port); } // Get port and address (even though they're public)
	ULONG	GetAddress() const { return ntohl(sin_addr.s_addr); }

	// operators added for efficiency
	const CInetAddr& operator=(const sockaddr& sa){ memcpy(this, &sa, sizeof(sockaddr)); return *this; }
	const CInetAddr& operator=(const sockaddr_in& sin){ memcpy(this, &sin, sizeof(sockaddr_in)); return *this; }

	operator const sockaddr()const{ return *((sockaddr*) this); }
	operator const sockaddr* ()const{ return (const sockaddr*) this; }
	operator const sockaddr_in* ()const{ return (const sockaddr_in*) this; }

	operator sockaddr(){ return *((sockaddr*) this); }
	operator sockaddr* (){ return (sockaddr*) this; }
	operator sockaddr_in* (){ return (sockaddr_in*) this; }
};
template<class t_Ostream>
t_Ostream& operator<<(t_Ostream& Ostream, const CInetAddr& x)
{	Ostream<<x.GetDottedDecimalAddress()<<':'<<x.GetPort();
	return Ostream; 
}


class CSocket
{
protected:
	SOCKET m_hSocket;

public:
	BOOL Create(const CInetAddr &BindTo,int nSocketType = SOCK_STREAM,BOOL overlapped_io = FALSE, BOOL reuse_addr = FALSE, int AddrFormat = PF_INET);
	BOOL GetPeerName(CInetAddr &ConnectedTo);	// address of the peer
	BOOL GetBindName(CInetAddr &BindTo);		// address of this socket
	void Close();
	SOCKET Detach();
	void Attach(SOCKET hSocket);
	CSocket();
	~CSocket(){ Close(); }
	
	operator SOCKET	(){ return m_hSocket; }
	BOOL	 IsEmpty() const { return m_hSocket == INVALID_SOCKET; }

public: //helpers
	static int	GetLastError();

public:
	BOOL	IsValid();
	BOOL	ConnectTo(const CInetAddr &target);
	BOOL	IsConnected();
	BOOL	Listen(UINT pending_size);
	BOOL	Accept(CSocket& connected_out, CInetAddr& peer_addr);

	BOOL	Send(LPCVOID pData, UINT len);
	BOOL	SendTo(LPCVOID pData, UINT len,const CInetAddr &target);
	BOOL	Recv(LPVOID pData, UINT len, UINT& len_out, BOOL Peek = FALSE);
	BOOL	RecvFrom(LPVOID pData, UINT len, UINT& len_out, CInetAddr &target, BOOL Peek = FALSE);

	BOOL	SetBufferSize(int reserved_size, BOOL receiving_sending = TRUE); // TRUE for receiving buffer
};

//////////////////////////////////////////////
// HTTP client
enum	// values provided in msg of OnReflect
{	
	HTTPCLIENT_EVENT_URL_PARSED = 1,		// (LPCSTR)param is url with "http://" removed
	HTTPCLIENT_EVENT_DNS_RESOLVED,
	HTTPCLIENT_EVENT_CONNECTED,				// (LPCSTR)param is the server name
	HTTPCLIENT_EVENT_FIRSTBYTE,
	HTTPCLIENT_EVENT_HEADER_RECEIVED,		// (ResponseHeader*)param is the parsed header
	HTTPCLIENT_EVENT_CONTENT_RECEIVING,		// (int)param is bytes received so far
	HTTPCLIENT_EVENT_DONE,					// (int)param is the length of content downloaded
	HTTPCLIENT_EVENT_ABORT,
};

class CHttpSession
{
	rt::StringA			_CommonFields;
	rt::StringA			_SendBuffer;
	rt::Buffer<CHAR>	_RecvBuffer;
	UINT				_RecvBuffer_Used;
	UINT				_RecvBuffer_Parsed;
	UINT				_RecvUntil(LPCSTR end_token, BOOL remove_parsed = FALSE);  // return the index of first token charactor in _RecvBuffer
	BOOL				_RecvUntilClose();
	BOOL				_RecvUpTo(UINT size_byte);  // fill _RecvBuffer up to size_byte bytes, make size_byte == _RecvBuffer_Used
	UINT				_RecvOneChunk();  // return the size of the chunk, or INFINITE for error, 0 for end
	UINT				_ResponseStart;
	DWORD				_HangingTick;
	__forceinline void	_UpdateHangingTick(){ _HangingTick = ::GetTickCount()&0xfffffffe; }
	__forceinline void	_ClearHangingTick(){ _HangingTick = INFINITE; }

protected:
	w32::CSocket		m_TcpConn;
	rt::StringA			m_ServerName;
	UINT				m_Port;
	BOOL				m_Keepalive;

	rt::_reflection*	m_pEventCallback;

	BOOL				SendRequest(LPCSTR pURL, DWORD verb = HTTP_VERB_GET, LPCSTR additional_header = NULL, UINT additional_header_len = 0);

public:
	struct ResponseHeader
	{
		UINT			m_StateCode;
		UINT			m_VersionMajor;
		UINT			m_VersionMinor;
		BOOL			m_ConnectPersisent;
		UINT			m_ContentLength;
		UINT			m_ChunkedTransfer;
		rt::StringA_Ref	m_ContentType;
		rt::StringA_Ref	m_ContentSubtype[2];
		rt::StringA_Ref	m_Redirected;
		rt::StringA_Ref	m_RawHeader;

		void	Parse_Tie1(LPCSTR header_text);
		void	Parse_Tie2(LPCSTR header_text);
		BOOL	IsParsedOk(){ return m_StateCode != 0xffffffff; }
		BOOL	IsContentLengthKnown(){ return m_ContentLength != 0xffffffff; }
	};

public:
	ResponseHeader		m_ResponseHeader;
	enum
	{	HTTP_VERB_GET = 0,
		HTTP_VERB_POST,
		HTTP_VERB_MAX
	};

	CHttpSession();
	~CHttpSession();

	void	SetEventSink(rt::_reflection* pSink){ m_pEventCallback = pSink; }
	BOOL	Request_Get(LPCSTR pURL, LPCSTR additional_header = NULL, UINT additional_header_len = 0);	// no automatic redirection (3xx) handling
	BOOL	Request_Post(LPCSTR pURL, LPCBYTE data, UINT sz=0, LPCSTR data_type = "text/plain", LPCSTR charset = "utf-8", BOOL keep_alive = TRUE);

	DWORD	GetIrresponsiveTime() const;	// msec
	BOOL	WaitResponse();
	LPBYTE	GetResponse();
	UINT	GetResponseLength();
	LPCSTR	GetResponseHeader(){ return _RecvBuffer; }
	UINT	GetResponseHeaderLength();
	void	SetCommonHeaders(LPCSTR agent, BOOL keepalive = FALSE);
	void	CancelResponseWaiting(); // supposed to be called from other thread
	

	const ResponseHeader& 
			GetResponseParsedHeader()const{ return m_ResponseHeader; }
};


namespace inet
{

extern UINT Base64EncodeLength(int len);
extern UINT Base64DecodeLength(LPCSTR pBase64, int len);
extern void Base64Encode(LPSTR pBase64Out,LPCVOID pData, int data_len);
extern BOOL Base64Decode(LPVOID pDataOut,int data_len,LPCSTR pBase64, int str_len);

extern UINT Base16EncodeLength(int len);
extern UINT Base16DecodeLength(int len);
extern void Base16Encode(LPSTR pBase16Out,LPCVOID pData, int data_len);
extern BOOL Base16Decode(LPVOID pDataOut,int data_len,LPCSTR pBase16, int str_len);

template<class SymCipher>	// class of ipp::SymCipher
BOOL Base64Encrypt(const rt::StringA_Ref& plain_text, rt::StringA& cipher_base64, LPCVOID key, UINT key_len)
{
	LPVOID pdata;
	rt::Buffer<BYTE>	plain_text_align;
	rt::Buffer<BYTE>	cipher;

	UINT len = plain_text.GetLength();
	if(len & SymCipher::Align)
	{
		len = (len + SymCipher::Align) & (~SymCipher::Align);
		if(!plain_text_align.SetSize(len))goto ERROR_EXIT;
		pdata = plain_text_align;
		ZeroMemory(pdata,len);
		memcpy(pdata,plain_text,plain_text.GetLength());
	}
	else pdata = (LPVOID)plain_text.Begin();
	
	if(!cipher.SetSize(len))goto ERROR_EXIT;
	
	SymCipher::Encrypt(key,key_len,pdata,cipher,len);

	cipher_base64.SetLength(Base64EncodeLength(len));
	Base64Encode(cipher_base64,cipher,cipher.GetSize());
	return TRUE;

ERROR_EXIT:
	cipher_base64.Empty();
	return FALSE;
}

template<class SymCipher>	// class of ipp::SymCipher
BOOL Base64Decrypt(const rt::StringA_Ref& cipher_base64, rt::StringA& plain_text, LPCVOID key, UINT key_len)
{
	int len = Base64DecodeLength(cipher_base64,cipher_base64.GetLength());
	rt::Buffer<BYTE>	cipher;

	if(	(0 == (len & SymCipher::Align)) &&
		cipher.SetSize(len) &&
		Base64Decode(cipher,cipher.GetSize(),cipher_base64,cipher_base64.GetLength()) &&
		plain_text.SetLength(cipher.GetSize()) &&
		SymCipher::Decrypt(key,key_len,cipher,plain_text,len)
	)
	{
		plain_text.RecalculateLength();
		return TRUE;
	}
	else
	{
		plain_text.Empty();
		return FALSE;
	}
}


} // namespace inet
} // namespace w32

