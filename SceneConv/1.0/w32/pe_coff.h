#pragma once

//////////////////////////////////////////////////////////////////////
// Visual Computing Foundation Classes (VCFC)
//					Jiaping Wang  2006.2
//					e_boris2002@hotmail.com
//
// (C) Copyright Jiaping Wang 2004.
//
// Permission to copy, use, modify, sell and distribute this software
// is granted provided this copyright notice appears in all copies.
// This software is provided "as is" without express or implied
// warranty, and with no claim as to its suitability for any purpose.
//
// Absolutely no warranty!!
//////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////
//  Brief Description
//
//  pe_coff.h
//
//  PE/Coff file parser (platform win32 only)
//
//
//////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////
// Revise History
//
// Inital version	2006.6			Jiaping
//
//////////////////////////////////////////////////////////////////////


#include "file_64.h"

namespace w32
{

class CPECoffModule
{
public:
	enum
	{
		TargetMachine_UNKNOWN       = IMAGE_FILE_MACHINE_UNKNOWN     ,      
		TargetMachine_I386          = IMAGE_FILE_MACHINE_I386        ,      // Intel 386.
		TargetMachine_R3000         = IMAGE_FILE_MACHINE_R3000       ,      // MIPS little-endian, 0x160 big-endian
		TargetMachine_R4000         = IMAGE_FILE_MACHINE_R4000       ,      // MIPS little-endian
		TargetMachine_R10000        = IMAGE_FILE_MACHINE_R10000      ,      // MIPS little-endian
		TargetMachine_WCEMIPSV2     = IMAGE_FILE_MACHINE_WCEMIPSV2   ,      // MIPS little-endian WCE v2
		TargetMachine_ALPHA         = IMAGE_FILE_MACHINE_ALPHA       ,      // Alpha_AXP
		TargetMachine_SH3           = IMAGE_FILE_MACHINE_SH3         ,      // SH3 little-endian
		TargetMachine_SH3DSP        = IMAGE_FILE_MACHINE_SH3DSP      ,      
		TargetMachine_SH3E          = IMAGE_FILE_MACHINE_SH3E        ,      // SH3E little-endian
		TargetMachine_SH4           = IMAGE_FILE_MACHINE_SH4         ,      // SH4 little-endian
		TargetMachine_SH5           = IMAGE_FILE_MACHINE_SH5         ,      // SH5
		TargetMachine_ARM           = IMAGE_FILE_MACHINE_ARM         ,      // ARM Little-Endian
		TargetMachine_THUMB         = IMAGE_FILE_MACHINE_THUMB       ,      
		TargetMachine_AM33          = IMAGE_FILE_MACHINE_AM33        ,      
		TargetMachine_POWERPC       = IMAGE_FILE_MACHINE_POWERPC     ,      // IBM PowerPC Little-Endian
		TargetMachine_POWERPCFP     = IMAGE_FILE_MACHINE_POWERPCFP   ,      
		TargetMachine_IA64          = IMAGE_FILE_MACHINE_IA64        ,      // Intel 64
		TargetMachine_MIPS16        = IMAGE_FILE_MACHINE_MIPS16      ,      // MIPS
		TargetMachine_ALPHA64       = IMAGE_FILE_MACHINE_ALPHA64     ,      // ALPHA64
		TargetMachine_MIPSFPU       = IMAGE_FILE_MACHINE_MIPSFPU     ,      // MIPS
		TargetMachine_MIPSFPU16     = IMAGE_FILE_MACHINE_MIPSFPU16   ,      // MIPS
		TargetMachine_AXP64         = IMAGE_FILE_MACHINE_AXP64       ,      // ALPHA64
		TargetMachine_TRICORE       = IMAGE_FILE_MACHINE_TRICORE     ,      // Infineon
		TargetMachine_CEF           = IMAGE_FILE_MACHINE_CEF         ,      
		TargetMachine_EBC           = IMAGE_FILE_MACHINE_EBC         ,      // EFI Byte Code
		TargetMachine_AMD64         = IMAGE_FILE_MACHINE_AMD64       ,      // AMD64 (K8)
		TargetMachine_M32R          = IMAGE_FILE_MACHINE_M32R        ,      // M32R little-endian
		TargetMachine_CEE           = IMAGE_FILE_MACHINE_CEE         ,
	};

	enum
	{
		PE_Characteristics_relocs_stripped         = IMAGE_FILE_RELOCS_STRIPPED          ,  // Relocation info stripped from file.
		PE_Characteristics_executable_image        = IMAGE_FILE_EXECUTABLE_IMAGE         ,  // File is executable  (i.e. no unresolved externel references).
		PE_Characteristics_line_nums_stripped      = IMAGE_FILE_LINE_NUMS_STRIPPED       ,  // Line nunbers stripped from file.
		PE_Characteristics_local_syms_stripped     = IMAGE_FILE_LOCAL_SYMS_STRIPPED      ,  // Local symbols stripped from file.
		PE_Characteristics_aggresive_ws_trim       = IMAGE_FILE_AGGRESIVE_WS_TRIM        ,  // Agressively trim working set
		PE_Characteristics_large_address_aware     = IMAGE_FILE_LARGE_ADDRESS_AWARE      ,  // App can handle >2gb addresses
		PE_Characteristics_bytes_reversed_lo       = IMAGE_FILE_BYTES_REVERSED_LO        ,  // Bytes of machine word are reversed.
		PE_Characteristics_32bit_machine           = IMAGE_FILE_32BIT_MACHINE            ,  // 32 bit word machine.
		PE_Characteristics_debug_stripped          = IMAGE_FILE_DEBUG_STRIPPED           ,  // Debugging info stripped from file in .DBG file
		PE_Characteristics_removable_run_from_swap = IMAGE_FILE_REMOVABLE_RUN_FROM_SWAP  ,  // If Image is on removable media, copy and run from the swap file.
		PE_Characteristics_net_run_from_swap       = IMAGE_FILE_NET_RUN_FROM_SWAP        ,  // If Image is on Net, copy and run from the swap file.
		PE_Characteristics_system                  = IMAGE_FILE_SYSTEM                   ,  // System File.
		PE_Characteristics_dll                     = IMAGE_FILE_DLL                      ,  // File is a DLL.
		PE_Characteristics_up_system_only          = IMAGE_FILE_UP_SYSTEM_ONLY           ,  // File should only be run on a UP machine
		PE_Characteristics_bytes_reversed_hi       = IMAGE_FILE_BYTES_REVERSED_HI           // Bytes of machine word are reversed.
	};

	//Symbol String Table
	rt::Buffer<char>					m_SymbolStringTable;
public:
	//Symbol Table
	rt::Buffer<IMAGE_SYMBOL>			m_SymbolTable;
	//PE Raw Sections
	rt::Buffer<IMAGE_SECTION_HEADER>	m_RawSections;

public:
	CPECoffModule(){ Clear(); }
	void	Clear();
	BOOL	IsCOFF()const{ return !m_BasicInfo.PE_HeaderOffset; }
	UINT	GetRawSectionCount()const{ return m_RawSections.GetSize(); }
	UINT	GetSymbolCount()const{ return m_SymbolTable.GetSize(); }
	LPCSTR  GetSymbolString(UINT offset)const{ return &m_SymbolStringTable[min(offset-4,m_SymbolStringTable.GetSize()-1)]; }  

	BOOL	ParsePECoff(CFile64& file, BOOL HandleDosHeader, UINT base_offset=0);
public:
	//Basic PE file informations
	struct _PE_info_basic
	{
		LONG	PE_HeaderOffset;
		WORD    TargetMachine;
		time_t  TimeDateStamp;
		WORD	SizeOfOptionalHeader;
		WORD    Characteristics;
	};
	_PE_info_basic m_BasicInfo;
	//PE Optional header
	typedef union _tag__PE_optional_header
	{
		WORD						opt_header_type;
		IMAGE_OPTIONAL_HEADER32		opt_header32;
		IMAGE_OPTIONAL_HEADER64		opt_header64;
		IMAGE_ROM_OPTIONAL_HEADER	opt_header_rom;
	}_PE_optional_header;
	_PE_optional_header	m_OptionalHeader;
};

class CExecutableModule:public CPECoffModule
{
protected:
	CFile64 _pefile;

protected:
	class _AddrSpcConv
	{
	protected:
		class SectionMapEntry
		{public:
			DWORD RVA_Start;
			DWORD RVA_End;
			DWORD RawDataAddress; //OFB
			rt::Buffer<BYTE> RawData;
		};
		rt::Buffer<SectionMapEntry>	_SectionMap;
		UINT						_LastSection;
	public:
		void	Clear();
		BOOL	BuildSectionMap(IMAGE_SECTION_HEADER* pSections,UINT len,w32::CFile64& file);
		DWORD	RVA2OFB(DWORD rva);

		BOOL	MountSection(w32::CFile64& file,DWORD rva_that_within);
		LPBYTE	RVA2PTR(DWORD rva);
	};
	_AddrSpcConv						_SectMap;	//Address space conversion

	//PE Data Directories
	IMAGE_DATA_DIRECTORY* GetDataDirectoryEntry(UINT entry); //IMAGE_DIRECTORY_ENTRY_EXPORT etc.

public:
	//Export Directory
	struct Directory_Export
	{	struct FunctionEntry
		{	DWORD	Entrypoint;
			DWORD	Ordinal;
			LPSTR	Name;	//refer to Names
		};
		IMAGE_EXPORT_DIRECTORY*			Header;
		rt::Buffer<FunctionEntry>		Functions;		
	};

public:

	//Member Directory
	Directory_Export					m_Directory_Export;

public:
	DWORD	RVA2OFB(DWORD rva){ return _SectMap.RVA2OFB(rva); }

public:
	BOOL	Open(LPCTSTR fn);
	void	Close();

	//data directory parser
	BOOL Parse_Directory_IMPORT();
	BOOL Parse_Directory_DELAY_IMPORT();
	BOOL Parse_Directory_BOUND_IMPORT();

	BOOL Parse_Directory_EXPORT();
	BOOL Parse_Directory_RESOURCE();
	BOOL Parse_Directory_SECURITY();
	BOOL Parse_Directory_BASERELOC();
	BOOL Parse_Directory_DEBUG();
	BOOL Parse_Directory_ARCHITECTURE();
	BOOL Parse_Directory_LOAD_CONFIG();
	BOOL Parse_Directory_GLOBALPTR();

	BOOL Parse_Directory_TLS();
	BOOL Parse_Directory_IAT();
};

class CLibrarianModule
{
protected:
#pragma pack(1)
	struct _ArchiveMemberHDR
	{	char	Name[16];
		char	Date[12];
		char	UserId[6];
		char	GroupId[6];
		char	Mode[8];
		char	Size[10];
		WORD	EndOfHdr;

		BOOL	FixupEndOfHdr()
		{	if(EndOfHdr == 0x0a60){ EndOfHdr=0; Date[0]=0; return TRUE; }
			else return FALSE;
		}
	};
#pragma pack()
	struct _ArchiveMember
	{	union
		{	char	Name[16];
			struct
			{		DWORD	NoRef;
					LPCSTR	NameRef;
			};
		};
		DWORD	Offset;	//to content
		DWORD	Size;	//size of content
		LPCSTR	ShortName;
		DWORD	Flag;	//Lower 16Bit is Machine Tag
	};
	struct _SymbolEntry
	{	LPCSTR	Name;
		UINT	ArchiveMemberId;
	};
protected:
	CFile64 _pefile;

protected:
	rt::Buffer<char>		m_SymbolStringTable;
	rt::Buffer<char>		m_LongNameTable;

	rt::Buffer<_ArchiveMember>	m_ArchiveMembers;
	rt::Buffer<_SymbolEntry>	m_Symbols;

public:
	enum
	{	ArchMem_MaskMachine		= 0x0000FFFF,
		ArchMem_MaskImportType	= 0x00030000,
		ArchMem_Object			= 0x00030000,
		ArchMem_ImportConst		= 0x00020000,
		ArchMem_ImportData		= 0x00010000,
		ArchMem_ImportCode		= 0x00000000,
	};
public:
	LPCSTR	GetArchiveMemberFullName(UINT idx)const;
	LPCSTR	GetArchiveMemberShortName(UINT idx)const{ return m_ArchiveMembers[idx].ShortName; }
	UINT	GetArchiveMemberCount()const{ return m_ArchiveMembers.GetSize(); }
	UINT	GetArchiveMemberSize(UINT idx)const{ return m_ArchiveMembers[idx].Size; }
	DWORD	GetArchiveMemberFlag(UINT idx)const{ return m_ArchiveMembers[idx].Flag; }
	BOOL	GetArchiveMemberRawData(UINT idx, LPBYTE pData, UINT len = UINT_MAX);

	UINT	GetSymbolCount()const{ return m_Symbols.GetSize(); }
	LPCSTR	GetSymbolName(UINT idx)const{ return m_Symbols[idx].Name; }
	UINT	GetSymbolArchiveId(UINT idx)const{ return m_Symbols[idx].ArchiveMemberId; }


public:
	void Close();
	BOOL Open(LPCTSTR fn);
};

/////////////////////////////////////
// Dump Info
LPCSTR	DumpInfo_PECoff_MachineName(UINT machine_tag);
void	DumpInfo_PECoff_Basic(const CPECoffModule& mod);
void	DumpInfo_PECoff_RawSection(const CPECoffModule& mod);
void	DumpInfo_LIB_ArchiveMember(const CLibrarianModule& mod);


}

