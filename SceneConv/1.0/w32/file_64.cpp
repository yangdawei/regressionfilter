#include "stdafx.h"
#include "file_64.h"
#include "..\rt\runtime_base.h"
#include "debug_log.h"
#include <fcntl.h>
#include <WinIoCtl.h>
#include <io.h>


//////////////////////////////////////////////
// CFile64
w32::CFile64::CFile64()
{	
	hFile = INVALID_HANDLE_VALUE; 
	bErrorFlag=FALSE; 
}

w32::CFile64::CFile64(LPCTSTR fn, UINT OpenFlag)
{
	hFile = INVALID_HANDLE_VALUE; 
	bErrorFlag=FALSE; 
	Open(fn,OpenFlag);
}

w32::CFile64::~CFile64()
{
	if(IsOpen())Close();
}

BOOL w32::CFile64::IsEOF()
{
	ASSERT(hFile != INVALID_HANDLE_VALUE); 
	return (GetLength() == GetCurrentPosition());
}

BOOL w32::CFile64::CreateDirectories(LPCTSTR pathin)
{
	rt::String	path(pathin);
	LPTSTR p = path.Begin();
	LPTSTR sep = p;
	LPTSTR last_sep = NULL;
	for(;;)
	{
		while(*sep && *sep!='/' && *sep!='\\')sep++;
		if(!*sep)break;
		last_sep = sep;
		*sep = 0;
		::CreateDirectory(p,NULL);
		*sep = '\\';
		sep++;
	}

	if(last_sep)*last_sep = 0;

	return IsDirectory(p);
}

BOOL w32::CFile64::Open(LPCTSTR fn,UINT OpenFlag)
{
	ASSERT(!IsOpen());
	DWORD accessmode;
	DWORD sharemode;
	DWORD openmode;
	bErrorFlag = FALSE;

	if( (OpenFlag & Access_All) == Access_All)
		accessmode = FILE_ALL_ACCESS;
	else
		accessmode = ((OpenFlag&Access_Read)?GENERIC_READ:0)|((OpenFlag&Access_Write)?GENERIC_WRITE:0)|
					 ((OpenFlag&Access_GetAttribute)?FILE_READ_ATTRIBUTES:0)|
					 ((OpenFlag&Access_SetAttribute)?FILE_WRITE_ATTRIBUTES:0);

	if( (OpenFlag & Share_All) == Share_All)
		sharemode = FILE_SHARE_DELETE|FILE_SHARE_READ|FILE_SHARE_WRITE;
	else
		sharemode = (OpenFlag&Share_Read?FILE_SHARE_READ:0)|(OpenFlag&Share_Write?FILE_SHARE_WRITE:0);

	if( (OpenFlag & Mode_OpenAny) == Mode_OpenAny )
		openmode = OPEN_ALWAYS;
	else if( OpenFlag & Mode_OpenExist )
		openmode = OPEN_EXISTING;
	else if( OpenFlag & Mode_OpenNew )
		openmode = CREATE_NEW;
	else{ ASSERT(0); }

	if((OpenFlag & Mode_CreatePath) && (OpenFlag&Access_Write) && (OpenFlag&Mode_OpenNew))
		CreateDirectories(fn);

	hFile = ::CreateFile(	fn,accessmode,sharemode,NULL,openmode,
							(OpenFlag&Flag_DeleteOnClose?FILE_FLAG_DELETE_ON_CLOSE:0)	|
							(OpenFlag&Flag_WriteThrough?FILE_FLAG_WRITE_THROUGH:0)		|
							(OpenFlag&Flag_SequentialScan?FILE_FLAG_SEQUENTIAL_SCAN:0)
							,NULL );
	if(IsOpen())
	{
		Filename = fn;
		if(OpenFlag & Flag_Truncate)  // Truncate to empty
		{	::SetFilePointer(hFile, 0, NULL,(DWORD)Seek_Current);
			::SetEndOfFile(hFile);
		}

		ClearError();
		return TRUE;
	}
	else
	{	return FALSE;
	}
}

UINT w32::CFile64::Read(LPVOID lpBuf,UINT nCount)
{
	DWORD	read = 0;
	::ReadFile(hFile,lpBuf,nCount,&read,NULL);

	if( read == nCount ){}
	else{ rt::_CastToNonconst(this)->bErrorFlag = TRUE; }

	return read;
}

UINT w32::CFile64::Write(LPCVOID lpBuf,UINT nCount)
{
	DWORD	write = 0;
	::WriteFile(hFile,lpBuf,nCount,&write,NULL);

	if( write == nCount ){}
	else{ rt::_CastToNonconst(this)->bErrorFlag = TRUE; }

	return write;
}

void w32::CFile64::Flush()
{
	ASSERT(IsOpen());
	::FlushFileBuffers(hFile);
}

void w32::CFile64::Close()
{
	if(IsOpen())
	{
		::CloseHandle(hFile);
		hFile = INVALID_HANDLE_VALUE;
	}
}

LONGLONG w32::CFile64::GetLength() const
{
	DWORD high = 0;
	return ::GetFileSize(hFile,&high) | (((ULONGLONG)high)<<32);
}

BOOL w32::CFile64::SetLength(ULONGLONG len)
{
	LONGLONG cur = Seek(0,w32::CFile64::Seek_Current);
	if(Seek(len,Seek_Begin)==len)
	{
		BOOL ret = SetEndOfFile(hFile);
		Seek(cur);
		return ret;
	}else return FALSE;
}


LONGLONG w32::CFile64::GetCurrentPosition()
{ 
	ASSERT(IsOpen());

	LARGE_INTEGER liOff;

	liOff.QuadPart = 0;
	liOff.LowPart = ::SetFilePointer(hFile, liOff.LowPart, &liOff.HighPart,(DWORD)Seek_Current);

	return liOff.QuadPart;
}

LONGLONG w32::CFile64::Seek(LONGLONG offset,UINT nFrom)
{
	ASSERT(IsOpen());

	LARGE_INTEGER liOff;

	liOff.QuadPart = offset;
	liOff.LowPart = ::SetFilePointer(hFile, liOff.LowPart, &liOff.HighPart,(DWORD)nFrom);

	return liOff.QuadPart;
}

void w32::CFile64::SeekToBegin()
{
	ASSERT(IsOpen());
	::SetFilePointer(hFile, 0, NULL,(DWORD)Seek_Begin);
}

void w32::CFile64::SeekToEnd()
{
	ASSERT(IsOpen());
	::SetFilePointer(hFile, 0, NULL,(DWORD)Seek_End);
}

void w32::CFile64::SetTime_Creation(const FILETIME* ft)
{
	ASSERT(hFile);
	ASSERT(ft); 
	VERIFY(SetFileTime(hFile,ft,NULL,NULL)); 
}
void w32::CFile64::SetTime_LastAccess(const FILETIME* ft)
{
	ASSERT(hFile);
	ASSERT(ft); 
	VERIFY(SetFileTime(hFile,NULL,ft,NULL)); 
}
void w32::CFile64::SetTime_LastModify(const FILETIME* ft)
{
	ASSERT(hFile);
	ASSERT(ft); 
	VERIFY(SetFileTime(hFile,NULL,NULL,ft)); 
}

void w32::CFile64::GetTime_Creation(FILETIME* ft)
{
	ASSERT(hFile);
	ASSERT(ft); 
	VERIFY(GetFileTime(hFile,ft,NULL,NULL));
}

void w32::CFile64::GetTime_LastAccess(FILETIME* ft)
{
	ASSERT(hFile);
	ASSERT(ft); 
	VERIFY(GetFileTime(hFile,NULL,ft,NULL)); 
}

void w32::CFile64::GetTime_LastModify(FILETIME* ft)
{
	ASSERT(hFile);
	ASSERT(ft); 
	VERIFY(GetFileTime(hFile,NULL,NULL,ft)); 
}

BOOL w32::CFile64::SetPathTime(LPCTSTR pathname, const FILETIME* creation,const FILETIME* last_access,const FILETIME* last_modify)
{
	HANDLE h = ::CreateFile(pathname,GENERIC_READ|GENERIC_WRITE,FILE_SHARE_READ|FILE_SHARE_DELETE,NULL,OPEN_EXISTING,FILE_FLAG_BACKUP_SEMANTICS,NULL);
	if(h == INVALID_HANDLE_VALUE)return FALSE;
	BOOL ret = ::SetFileTime(h,creation,last_access,last_modify);
	::CloseHandle(h);
	return ret;
}

BOOL w32::CFile64::GetPathTime(LPCTSTR pathname, FILETIME* creation,FILETIME* last_access,FILETIME* last_modify)
{
	HANDLE h = ::CreateFile(pathname,GENERIC_READ,FILE_SHARE_READ|FILE_SHARE_WRITE|FILE_SHARE_DELETE,NULL,OPEN_EXISTING,FILE_FLAG_BACKUP_SEMANTICS,NULL);
	if(h == INVALID_HANDLE_VALUE)return FALSE;
	BOOL ret = ::GetFileTime(h,creation,last_access,last_modify);
	::CloseHandle(h);
	return ret;
}

////////////////////////////////////////////////////////////
// CFile64_Text
BOOL w32::CFile64_Text::Open(LPCTSTR fn,UINT OpenFlag)
{
	if(CFile64::Open(fn,OpenFlag))
	{
		int nFlags = _O_TEXT;
		if( OpenFlag & Access_Write ){}
		else{ nFlags|=_O_RDONLY; }

		// open a C-runtime low-level file handle
		_os_file_handle = _open_osfhandle((UINT_PTR) hFile, nFlags);
		// open a C-runtime stream from that handle
		if (_os_file_handle != -1)
		{
			const char * szMode;
			if( OpenFlag & Access_Read )szMode = "r";
			if( OpenFlag & Access_Write )szMode = "w";
			if( OpenFlag & Access_All )szMode = "r+";
			pStream = _fdopen(_os_file_handle, szMode);
		}

		if (pStream != NULL)
		{
			return TRUE;
		}
		else
		{
			Close();
		}
	}
	return FALSE;
}

void w32::CFile64_Text::Close()
{
	if(pStream)
	{	
		StopRedirect();
		//_close(_os_file_handle);

		fflush(pStream);
		fclose(pStream); 
		pStream = NULL;

		hFile = INVALID_HANDLE_VALUE;
	}
}

LONGLONG w32::CFile64_Text::Seek(LONGLONG offset,UINT nFrom)
{
	ASSERT(IsOpen());
	CFile64::Seek(offset,nFrom);

	if( nFrom == Seek_Begin)
		fseek(pStream,(long)offset,SEEK_SET);
	else if( nFrom == Seek_End )
		fseek(pStream,(long)offset,SEEK_END);
	else if( nFrom == Seek_Current )
		fseek(pStream,(long)offset,SEEK_CUR);

	return ftell(pStream);
}

BOOL w32::CFile64_Text::WriteString(LPCSTR str)
{	ASSERT(pStream); 
	return fputs(str,pStream)>=0; 
}

BOOL w32::CFile64_Text::WriteString(LPCWSTR str)
{	ASSERT(pStream);
	return fputws(str,pStream)>=0; 
}

BOOL w32::CFile64_Text::WriteString(const rt::StringA_Ref& x)
{
	return x.GetLength() == Write(x.Begin(),x.GetLength());
}

BOOL w32::CFile64_Text::WriteString(const rt::StringW_Ref& x)
{
	return 2*x.GetLength() == Write(x.Begin(),2*x.GetLength());
}

void w32::CFile64_Text::Redirect(BOOL as_stdout)
{	ASSERT(pStream);
	StopRedirect();
	if(as_stdout)
	{	_Stdfile_org = *stdout;
		*stdout = *pStream;
		_Redirected = 1;
	}
	else
	{	_Stdfile_org = *stdin;
		*stdin = *pStream;
		_Redirected = 2;
	}
	std::ios::sync_with_stdio();
}

void w32::CFile64_Text::StopRedirect()
{	
	if(_Redirected)
	{	
		if(_Redirected==1)
			*stdout = _Stdfile_org;
		else
			*stdin = _Stdfile_org;

		_Redirected = 0;
		std::ios::sync_with_stdio();
	}
}

BOOL w32::CFileVolume::Open(LPCTSTR volume_name, UINT OpenFlag)
{
	ASSERT(!IsOpen());

	TCHAR szRootName[100];

	if(volume_name[0] == _T('\\')){}
	else
	{
		_stprintf_s(szRootName, sizeofArray(szRootName), _T("\\\\.\\%s"), volume_name);
		volume_name = szRootName;
	}

	ASSERT( !(Flag_DeleteOnClose & OpenFlag) );
	ASSERT( !(Mode_OpenNew & OpenFlag) );
	ASSERT( !(Flag_Truncate & OpenFlag) );
	
	OpenFlag |= Mode_OpenExist;
	_open_flag = OpenFlag;

	return __super::Open(volume_name,OpenFlag);
}

DWORD w32::CFileVolume::GetMediaType()
{
	ASSERT(IsOpen());
	return GetDriveType(Filename);
}

BOOL w32::CFileVolume::LoadMedia()
{
	ASSERT(IsOpen());

	DWORD dwDummy; // We don't really need this info

	if(	DeviceIoControl(hFile, IOCTL_STORAGE_LOAD_MEDIA2, NULL, 0, NULL, 0, &dwDummy, NULL) || 
		DeviceIoControl(hFile, IOCTL_STORAGE_LOAD_MEDIA, NULL, 0, NULL, 0, &dwDummy, NULL)
	)
	{	Close();		// must reopen in case that the media is changed.
		return Open(Filename,_open_flag);
	}

	return FALSE;
}

ULONGLONG w32::CFileVolume::GetMediaLength()
{
	ASSERT(IsOpen());
	GET_LENGTH_INFORMATION	sizeinfo;

	DWORD dwDummy;
	if(DeviceIoControl(hFile, IOCTL_DISK_GET_LENGTH_INFO, NULL, 0, &sizeinfo, sizeof(sizeinfo), &dwDummy, NULL))
	{
		return sizeinfo.Length.QuadPart;
	}

	return -1;
}

BOOL w32::CFileVolume::EjectMedia() // for removable media
{
	ASSERT(IsOpen());

	PREVENT_MEDIA_REMOVAL PMRBuffer;
	PMRBuffer.PreventMediaRemoval = FALSE;

	DWORD dwBytesReturned;
	if(	DeviceIoControl(hFile,IOCTL_STORAGE_MEDIA_REMOVAL,&PMRBuffer,sizeof(PREVENT_MEDIA_REMOVAL),NULL,0,&dwBytesReturned,NULL) &&
		DeviceIoControl(hFile,IOCTL_STORAGE_EJECT_MEDIA,NULL,0,NULL,0,&dwBytesReturned,NULL) )
	{
		return TRUE;
	}

	return FALSE;
}

INT w32::CFileVolume::ProbeMinimumSeekBlock()
{
	ASSERT(IsOpen());
	for(int i=1;i<1024*1024*1024;i<<=1)
	{
		if(i == Seek(i))return i;
	}

	return -1;
}

/////////////////////////////////////////////////
// CParallelFileWriter

w32::CParallelFileWriter::CParallelFileWriter()
{
	_InMemoryCopyBufferCodepage = CP_ACP;
	_SwitchNewFileName = NULL;
	m_hSyncThread = NULL;
	m_WriteDownInterval = 0;

	ZeroMemory(_WriteBuffers,sizeof(_WriteBuffers));
	m_FrontBuf = _WriteBuffers;
	m_BackBuf = m_FrontBuf + 1;

	ZeroMemory(_InMemoryCopy,sizeof(_InMemoryCopy));
	_InMemoryCopy_FrontBuf = _InMemoryCopy;
	_InMemoryCopy_BackBuf = _InMemoryCopy_FrontBuf + 1;

	stat_BufferUsagePeek = 0;
	stat_BufferUsageAvg = 0;
	stat_FileIOUsageAvg = 0;
	stat_FileIOUsagePeek = 0;

	_DesiredFileLength = 0;
	SetTruncationBoundaryTag('\n',1);
}

w32::CParallelFileWriter::~CParallelFileWriter()
{
	Close();
}

void w32::CParallelFileWriter::SwitchTo(LPCTSTR filename, BOOL no_wait)
{
	ASSERT(_File.IsOpen() && m_hSyncThread);
	ASSERT(_SwitchNewFileName == NULL); // Last switching should be done.

	_SwitchNewFileNameBuffer = filename;
	if(_SwitchNewFileNameBuffer != _File.GetFilename())
	{
		_SwitchNewFileName = _SwitchNewFileNameBuffer;

		if(!no_wait)
		{
			while(_SwitchNewFileName == NULL)
				::Sleep(500);
		}
	}
}

void w32::CParallelFileWriter::EnableInMemoryCopy(UINT memory_copy_size_max, UINT target_codepage)	// in byte, zero to disable
{
	_InMemoryCopyBufferCodepage = target_codepage;

	if(_InMemoryCopyBuffer.GetSize() == memory_copy_size_max*2)return;

	if(_InMemoryCopyBuffer.GetSize())
		ASSERT(m_hSyncThread == NULL);	// call before Open(...) when resize or stop

	if(memory_copy_size_max && _InMemoryCopyBuffer.SetSize(memory_copy_size_max*2))
	{
		_InMemoryCopy_BackBuf->Used = 0;
		_InMemoryCopy_BackBuf->pBuf = &_InMemoryCopyBuffer[memory_copy_size_max];
		_InMemoryCopy_FrontBuf->Used = 0;
		_InMemoryCopy_FrontBuf->pBuf = _InMemoryCopyBuffer;
	}
	else
	{	
		ZeroMemory(_InMemoryCopy,sizeof(_InMemoryCopy));
		_InMemoryCopyBuffer.SetSize();
	}
}

LPCBYTE	w32::CParallelFileWriter::GetLatestInMemoryCopy(UINT& len)
{
	_WriteBuf* p = _InMemoryCopy_FrontBuf;
	if(p->pBuf)
	{
		LPCBYTE ret = p->pBuf;
		UINT rlen = p->Used;
		if(len && len < rlen)
		{
			ret += rlen - len;
			rlen = len;

			UINT i = _SeekTruncateBoundary(ret, rlen);
			if(i < rlen)
			{
				ret += i;
				rlen -= i;
			}
		}

		len = rlen;
		return ret;
	}
	else return NULL;
}

void w32::CParallelFileWriter::SetWriteDownInterval(UINT sync_interval)
{
	m_WriteDownInterval = max(100,sync_interval);
	_AvgAttenuation = pow(0.64, sync_interval/1000.0);		// Attenuate to 0.01 after 10 sec
	ASSERT_FLOAT(_AvgAttenuation);
}

void w32::CParallelFileWriter::SetTruncationBoundaryTag(DWORD tag, int tag_size)
{
	_TruncateBoundaryTag = tag;
	_TruncateBoundaryTagSize = tag_size;
	_TruncateBoundaryTagMask = INFINITE;
	_TruncateBoundaryTagMask >>= 8*(4 - tag_size);
}

UINT	w32::CParallelFileWriter::_SeekTruncateBoundary(LPCBYTE p, UINT len)
{
	for(UINT i=0;i<len;i++)
	{
		if( (*((DWORD*)(p+i))^_TruncateBoundaryTag) & _TruncateBoundaryTagMask )
			continue;
		else
			return i;			
	}
	return len;
}

BOOL w32::CParallelFileWriter::Open(LPCTSTR filename, UINT buffer_size, UINT sync_interval, LONGLONG desired_length, DWORD flag)
{
	ASSERT(!_File.IsOpen() && m_hSyncThread == NULL);

	stat_BufferUsagePeek = 0;
	stat_BufferUsageAvg = 0;
	stat_FileError = 0;
	stat_UnfinalizedChunk = 0;
	stat_ClaimFailure = 0;
	stat_FinalizeFailure = 0;
	stat_FileIOUsageAvg = 0;			// precentage
	stat_FileIOUsagePeek = 0;			// precentage
	stat_TotalClaimed = 0;

	_SwitchNewFileName = NULL;
	_SwitchNewFileOpenFlag = flag|w32::CFile64::Mode_CreatePath|w32::CFile64::Flag_SequentialScan;

	_DesiredFileLength = desired_length;
	if(desired_length)
		_TruncateBuffer.SetSize(10*1024);

	SetWriteDownInterval(sync_interval);

	m_WriteBufferSize = (buffer_size + 3) & 0xffffffffc;

	if(	_File.Open(filename,_SwitchNewFileOpenFlag) &&
		_WritePool.SetSize(2*m_WriteBufferSize)
	)
	{	_File.SeekToEnd();

		m_FrontBuf->pBuf = _WritePool;
		m_FrontBuf->Used = 0;
		m_BackBuf->pBuf = &_WritePool[m_WriteBufferSize];
		m_BackBuf->Used = 0;

		struct _call
		{	static DWORD WINAPI _func(LPVOID pThis)
			{	((CParallelFileWriter*)pThis)->_SyncThread();
				return 0;
			}
		};

		m_hSyncThread = ::CreateThread(NULL,0,_call::_func,this,0,NULL);
		ASSERT(m_hSyncThread);

		return TRUE;
	}
	
	if(_File.IsOpen())
		_File.Close();

	_WritePool.SetSize(0);
	return FALSE;
}

void w32::CParallelFileWriter::Close()
{
	if(m_hSyncThread)
	{	HANDLE h = m_hSyncThread;
		m_hSyncThread = NULL;
		w32::WaitForThreadEnding(h);
		::CloseHandle(h);
	}
	
	if(_File.IsOpen())
		_File.Close();

	//_WritePool.SetSize(0);
}

void w32::CParallelFileWriter::_WriteInMemoryCopyBuffer(LPBYTE p, UINT len)
{
	if(	_InMemoryCopyBufferCodepage &&
		__string_wchar.ConvertCodePage(rt::StringA_Ref((LPCSTR)p,len)) &&
		__string_acp.ConvertCodePage(__string_wchar.GetRef(),_InMemoryCopyBufferCodepage)
	)
	{	// assuming p is MultiByte String of CP_ACP
		p = (LPBYTE)__string_acp.GetBuffer();
		len = __string_acp.GetLength();
	}

	if(_InMemoryCopy_FrontBuf->Used + len <= _InMemoryCopyBuffer.GetSize()/2)
	{
		memcpy(_InMemoryCopy_FrontBuf->pBuf + _InMemoryCopy_FrontBuf->Used,p,len);
		_InMemoryCopy_FrontBuf->Used += len;
	}
	else if(len < _InMemoryCopyBuffer.GetSize() / 10)
	{
		UINT skip = _InMemoryCopyBuffer.GetSize() / 10;
		memcpy(_InMemoryCopy_BackBuf->pBuf, _InMemoryCopy_FrontBuf->pBuf + skip, _InMemoryCopy_FrontBuf->Used - skip);
		memcpy(_InMemoryCopy_BackBuf->pBuf + _InMemoryCopy_FrontBuf->Used - skip, p, len);
		_InMemoryCopy_BackBuf->Used = _InMemoryCopy_FrontBuf->Used - skip + len;
		
		rt::Swap(_InMemoryCopy_BackBuf, _InMemoryCopy_FrontBuf);
	}
}

void w32::CParallelFileWriter::_WriteDownBackBuffer()
{
	if(_InMemoryCopy_FrontBuf->pBuf)
	{	// dump to file and also keep a copy in memory
		_Chunk* p = (_Chunk*)m_BackBuf->pBuf;
		_Chunk* pend = (_Chunk*)(m_BackBuf->pBuf + min(m_BackBuf->Used,(int)m_WriteBufferSize - _ChunkHeaderSize));
	
		for(;p<pend;p = p->GetNext())
		{	
			if(p->size)
			{
				if(p->length==INFINITE)
				{	stat_UnfinalizedChunk++;
					continue;
				}

				if(p->length>0)
				{
					_WriteInMemoryCopyBuffer(p->data,p->length);
					if(_File.Write(p->data,p->length) != p->length)
						stat_FileError++;
				}
			}
			else break;
		}
		_File.Flush();
	}
	else
	{	// dump to file only
		_Chunk* p = (_Chunk*)m_BackBuf->pBuf;
		_Chunk* pend = (_Chunk*)(m_BackBuf->pBuf + min(m_BackBuf->Used,(int)m_WriteBufferSize - _ChunkHeaderSize));
	
		for(;p<pend;p = p->GetNext())
		{	
			if(p->size)
			{
				if(p->length==INFINITE)
				{	stat_UnfinalizedChunk++;
					continue;
				}

				if(p->length>0)
					if(_File.Write(p->data,p->length) != p->length)
						stat_FileError++;
			}
			else break;
		}
		_File.Flush();
	}

	// Use Rate
	{	LONG fr;
		if(m_BackBuf->Used >= (int)m_WriteBufferSize)
		{	fr = 100;	}
		else
		{	fr = 100*(m_BackBuf->Used/1024)/(m_WriteBufferSize/1024);	}

		if(stat_BufferUsagePeek)
		{
			if(fr > stat_BufferUsagePeek)stat_BufferUsagePeek = fr;
			stat_BufferUsageAvg = (LONG)(fr + _AvgAttenuation*(stat_BufferUsageAvg - fr) + 0.5);
		}
		else
		{	stat_BufferUsagePeek = fr;
			stat_BufferUsageAvg = fr;
		}
	}

	m_BackBuf->Used = 0;
	ZeroMemory(m_BackBuf->pBuf,m_WriteBufferSize);
}

void w32::CParallelFileWriter::_SyncThread()
{
	int time_to_sleep = m_WriteDownInterval;
	w32::CTimeMeasure	tm;

	for(;;)
	{
		while(time_to_sleep > 500)
		{	::Sleep(500);
			time_to_sleep -= 500;
			if(m_hSyncThread == NULL)
				goto CLOSING_FILE;
		}

		::Sleep(time_to_sleep);

		tm.Start();
		_WriteDownBackBuffer();

		// swap buffers
		rt::Swap(m_BackBuf,m_FrontBuf);

		if(_SwitchNewFileName)
		{	// switch log file
			w32::CFile64	_log;
			if(_log.Open(_SwitchNewFileName,_SwitchNewFileOpenFlag))
			{	_log.SeekToEnd();
				rt::Swap(_log,_File);
			}
			_SwitchNewFileName = NULL;
		}
		
		{	LONGLONG fsize;
			w32::CFile64	_log;
			w32::CFile64	_log_old;
			if(	_DesiredFileLength &&
				(fsize = _File.GetLength()) > _DesiredFileLength*2 &&
				_log.Open(_File.GetFilename() + _T('.') + _T('t'),w32::CFile64::Normal_Write) &&
				_log_old.Open(_File.GetFilename(),w32::CFile64::Normal_Read) &&
				_log_old.Seek(fsize - _DesiredFileLength) == (fsize - _DesiredFileLength)
			)
			{	// truncate old data
				LONGLONG tot = _DesiredFileLength;

				if(_TruncateBoundaryTagSize)
				{	// seek boundary Tag
					while(tot)
					{
						int block = (int)min(tot,_TruncateBuffer.GetSize());
						if(	_log_old.Read(_TruncateBuffer,block) == block )
						{	
							tot -= block;
							int bound = _SeekTruncateBoundary(_TruncateBuffer,block);
							if(bound < block)
							{
								bound += _TruncateBoundaryTagSize;
								if(bound < block)
								{
									if(_log.Write(&_TruncateBuffer[bound],block - bound) != (block - bound))
										goto END_OF_FILE_TRUNCATION;
								}
								break;
							}
							else continue;
						}
						else goto END_OF_FILE_TRUNCATION;
					}
				}

				while(tot)
				{	// move remaining
					int block = (int)min(tot,_TruncateBuffer.GetSize());
					if(	_log_old.Read(_TruncateBuffer,block) == block &&
						_log.Write(_TruncateBuffer,block) == block
					)
					{	tot -= block;
					}
					else goto END_OF_FILE_TRUNCATION;
				}
				_log.Flush();

				{	rt::String fn(_File.GetFilename());
					_log_old.Close();
					_log.Close();
					_File.Close();
					VERIFY(w32::CFile64::Move(fn + _T('.') + _T('t'),fn));
					VERIFY(_File.Open(fn,_SwitchNewFileOpenFlag));
				}

END_OF_FILE_TRUNCATION:
				_File.SeekToEnd();
			}
		}

		int time_used = (int)tm.Snapshot();

		time_to_sleep = max(max(100,time_used),((int)m_WriteDownInterval) - ((int)tm.Snapshot()));

		{	int IOUage;
			if(	time_used < (int)m_WriteDownInterval )
				IOUage = 100*time_used/m_WriteDownInterval;
			else
				IOUage = 100;

			if( stat_FileIOUsagePeek )
			{
				if(IOUage>stat_FileIOUsagePeek)stat_FileIOUsagePeek = IOUage;
				stat_FileIOUsageAvg = (LONG)(IOUage + _AvgAttenuation*(stat_FileIOUsageAvg - IOUage) + 0.5);
			}
			else
			{
				stat_FileIOUsagePeek = IOUage;
				stat_FileIOUsageAvg = IOUage;
			}
		}
	}	

CLOSING_FILE:

	_WriteDownBackBuffer();
	rt::Swap(m_BackBuf,m_FrontBuf);

	_WriteDownBackBuffer();

	_File.Close();
}


////////////////////////////////////////////////
// CFolderChangingMonitor
void w32::CFolderChangingMonitor::OnFolderChanged()
{	
	if(m_NotificationWnd)
		::SendMessage(m_NotificationWnd,WM_COMMAND,m_CommandId,0);
};

w32::CFolderChangingMonitor::CFolderChangingMonitor()
{	
	m_WaitingHandle = INVALID_HANDLE_VALUE; 
	m_WorkingThread = NULL;
}

DWORD WINAPI w32::CFolderChangingMonitor::_WorkingThreadFunc(LPVOID p)
{
	ASSERT(p);
	w32::CFolderChangingMonitor* This  = ((w32::CFolderChangingMonitor*)p);

	for(;;)
	{
		DWORD ret;
		ret = WaitForSingleObject(This->m_WaitingHandle,INFINITE);

		VERIFY(FindNextChangeNotification(This->m_WaitingHandle));

		if(ret == WAIT_OBJECT_0)
			This->OnFolderChanged();
		else
			::Sleep(1000);
	}
}

void w32::CFolderChangingMonitor::SetNotificatonWindow(HWND hwnd, UINT CommandId)
{
	m_NotificationWnd = hwnd;
	m_CommandId = CommandId;
}

BOOL w32::CFolderChangingMonitor::Create(LPCTSTR Folder,BOOL IncludeSubTree, DWORD filter)
{
	ASSERT(m_WaitingHandle == INVALID_HANDLE_VALUE);

	m_WaitingHandle = FindFirstChangeNotification(Folder,IncludeSubTree,filter);
	if(m_WaitingHandle != INVALID_HANDLE_VALUE)
	{
		m_WorkingThread = CreateThread(NULL,0,_WorkingThreadFunc,this,0,NULL);
		SetThreadPriority(m_WorkingThread,THREAD_PRIORITY_ABOVE_NORMAL);

		ASSERT( m_WorkingThread );

		return TRUE;
	}
	else
	{
		return FALSE;
	}
}

void w32::CFolderChangingMonitor::Destroy()
{
	if( m_WorkingThread )
	{
		::TerminateThread(m_WorkingThread,0);
		m_WorkingThread = NULL;
	}

	if( m_WaitingHandle != INVALID_HANDLE_VALUE )
	{
		FindCloseChangeNotification(m_WaitingHandle);
		m_WaitingHandle = INVALID_HANDLE_VALUE;
	}
}


//////////////////////////////////////////////////////////////
// CFileChangingMonitor
BOOL w32::CFileChangingMonitor::StartMonitor(LPCTSTR fn)
{
	TCHAR fullPath[512];
	_tfullpath(fullPath,fn,512);
	_tcscpy(_fullName,fullPath);

	TCHAR* starPos = _tcsrchr(fullPath,'\\');
	*(starPos+1) = '\0';
	
	GetLastModifiedTime(&_lastModifiedTime);
	return __super::Create(fullPath);
}

void w32::CFileChangingMonitor::GetLastModifiedTime(FILETIME * pTime)
{
	ASSERT(pTime);
	w32::CFile64 file;
	if(file.Open(_fullName))
		file.GetTime_LastModify(pTime);
}

void w32::CFileChangingMonitor::OnFolderChanged()
{
	FILETIME lastTime;
	GetLastModifiedTime(&lastTime);

	if(	lastTime.dwHighDateTime!=_lastModifiedTime.dwHighDateTime || 
		lastTime.dwLowDateTime!=_lastModifiedTime.dwLowDateTime	)
	{
		OnFileChange();
	}
}

ULONG w32::CRC32::_Reflect(ULONG ref, char ch)
{
    ULONG value = 0;
    // Swap bit 0 for bit 7
    // bit 1 for bit 6, etc.
    for(int i = 1; i < (ch + 1); i++)
    {
        if (ref & 1)
            value |= 1 << (ch - i);
        ref >>= 1;
    }
    return value;
}

ULONG w32::CRC32::_Table[256];

w32::CRC32::CRC32(DWORD init)
{
	static bool _init_table = FALSE;
	if(!_init_table)
	{
		ULONG ulPolynomial = 0x04C11DB7;
		// 256 values representing ASCII character codes.
		for (int i = 0; i <= 0xFF; i++)
		{
			_Table[i] = _Reflect(i, 8) << 24;
			for (int j = 0; j < 8; j++)
				_Table[i] = (_Table[i] << 1) ^ (_Table[i] & (1 << 31) ? ulPolynomial : 0);
			_Table[i] = _Reflect(_Table[i], 32);
		}
		_init_table = TRUE;
	}
	Reset(init);
}

void w32::CRC32::Finalize()
{
	CRC ^= 0xFFFFFFFF;
};

void w32::CRC32::Reset(DWORD init)
{
	CRC = init^0xFFFFFFFF;
};

void w32::CRC32::Update(LPCVOID buffer, UINT size)
{
    LPCBYTE pbyte = (LPCBYTE)buffer;
    while (size--)
        CRC = (CRC >> 8) ^ _Table[(CRC & 0xFF) ^ *pbyte++];
}
