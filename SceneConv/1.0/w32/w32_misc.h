#pragma once

//////////////////////////////////////////////////////////////////////
// Visual Computing Foundation Classes (VCFC)
//					Jiaping Wang  2006.2
//					e_boris2002@hotmail.com
//
// (C) Copyright Jiaping Wang 2004.
//
// Permission to copy, use, modify, sell and distribute this software
// is granted provided this copyright notice appears in all copies.
// This software is provided "as is" without express or implied
// warranty, and with no claim as to its suitability for any purpose.
//
// Absolutely no warranty!!
//////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////
//  Brief Description
//
//  w32_mics.h
//
//  mics stuffs
//
//  Command line paser
//
//////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////
// Revise History
//
// Inital version	2006.6.9		Jiaping
// V0.2				2006.10.16		Jiaping
//					Add CCommandLine
// V0.3				2006.10.26		Jiaping
//					Add System Color
// V0.4				2006.11.4		Jiaping
//					Add CDIB
// V0.5				2007.12.24		Jiaping
//					Add process launch
//
//////////////////////////////////////////////////////////////////////

#include "win32_ver.h"
#include "w32_basic.h"
#include "file_64.h"
#include <limits.h>
#include <shellapi.h>
#include <MsHTML.h>

#include "..\rt\string_type.h"
#include "..\num\small_vec.h"


namespace w32
{


///////////////////////////////////////////////////
// Cmdline parser ( option indicator is '/' '-' )
class CCommandLine
{
public:
	struct Argument
	{	DWORD				Flag;
		::rt::String		OptionText;
		Argument*			pNextArg;
	};
	enum
	{	ARG_LASTONE	=		0x0001,
		ARG_OPTION	=		0x0002,
	};
protected:
	rt::Buffer<Argument>	m_Arguments;
	UINT					m_OptionCount;
	UINT					m_TextCount;

public:
	CCommandLine();
	void	Parse(int argc, TCHAR* argv[]);	// for _tmain
	void	Parse(LPCTSTR pCmdLine);		// for _twmain

	LPCTSTR	SearchOption(LPCTSTR option_name) const;	//search an option of this name (in lower-case)
												//If not found, return NULL, otherwise the text of next argument is returned
												//If the next argument is not exist or is option, zero-length string is returned
	LPCTSTR	SearchOptionEx(LPCTSTR option_substring) const;	//search an option contains this name (in lower-case)	, if found return the remaining text of the option

	UINT	GetTextCount()const{ return m_TextCount; }
	LPCTSTR	GetText(UINT idx)const{ ASSERT(idx<m_TextCount); return m_Arguments[idx].OptionText; }

	UINT	GetOptionCount()const{ return m_OptionCount; }
	LPCTSTR	GetOption(UINT idx)const{ ASSERT(idx<m_OptionCount); return m_Arguments[m_TextCount+idx].OptionText; }
};

class CResourceStream
{
	IStream*	pstream;
public:
	CResourceStream(HINSTANCE hInst, LPCTSTR res_group, LPCTSTR res_name);
	~CResourceStream(){ _SafeRelease(pstream); }
	operator IStream*(){ return pstream; }
};

class CClipboard
{
public:
	CClipboard();
	~CClipboard();

	BOOL	Content_IsText();
	BOOL	Content_IsImage();
	BOOL	Content_IsFileList();

	BOOL	PutText(LPCTSTR string);
	LPCTSTR	GetText();
	BOOL	PutImage(HGLOBAL hDib);
	HGLOBAL	GetImage();
	HDROP	GetFileList();
};

class CFileList
{
protected:
	::rt::BufferEx<::rt::String>	m_FilenameList;
	::rt::_reflection*				m_pCallback;

public:
	enum
	{	PATHNAME_FLAG_NONE		 = 0x0,
		PATHNAME_FLAG_FIRST_FILE = 0x1,
		PATHNAME_FLAG_RECURSIVE  = 0x2,
	};
	static void EnableFileDropping(HWND win,BOOL enable = TRUE);

	CFileList(){ m_pCallback = NULL; }
	void SetCallback(::rt::_reflection* pCB){ m_pCallback = pCB; } // OnReflect's param, is a LPCTSTR
	UINT GetFileCount() const{ return (UINT)m_FilenameList.GetSize(); }   
	LPCTSTR GetFilename(UINT idx) const{ return m_FilenameList[idx]; }
	LPCTSTR operator [](UINT idx) const{ return GetFilename(idx); }
	LPCTSTR operator [](int  idx) const{ return GetFilename(idx); }
	operator LPCTSTR() const{ return GetFilename(0); }

	BOOL SetFileCount(UINT sz=0);
	void SetFilename(UINT idx, LPCTSTR fn);
	UINT AppendFilename(LPCTSTR fn);

	HRESULT MultipleSelectionFileDialog(HWND parent_wnd, LPCTSTR pExtFilter = NULL);
	HRESULT ParseDropFileList(HDROP hDropFile, BOOL NeedFirstFileOnly = FALSE);
	HRESULT ParsePathName(	LPCTSTR fn_templ, 
							DWORD Flag = PATHNAME_FLAG_NONE,
							DWORD attrib_not_have = FILE_ATTRIBUTE_DIRECTORY | FILE_ATTRIBUTE_OFFLINE,
							DWORD attrib_must_have = 0
						 ) // eg. c:\*.*
						 {  return ParsePathName(0,fn_templ,Flag,attrib_not_have,attrib_must_have); }
	HRESULT ParsePathName(	TCHAR hidden_Initial, // skip folders/files with name starts with this charactor
							LPCTSTR fn_templ, 
							DWORD Flag = PATHNAME_FLAG_NONE,
							DWORD attrib_not_have = FILE_ATTRIBUTE_DIRECTORY | FILE_ATTRIBUTE_OFFLINE,
							DWORD attrib_must_have = 0
						 ); // eg. c:\*.*

	HRESULT LoadFileList(LPCTSTR fn);
	HRESULT SaveFileList(LPCTSTR fn, BOOL UTF_8 = FALSE) const;	//UTF_8 is used only in non-unicode project

	HDROP	GetDropHandle();	//return Clipborad format: CF_HDROP, call ReleaseDropHandle after use.
	static void	ReleaseDropHandle(HDROP hDrop){ if(hDrop)GlobalFree(hDrop); }
};


struct CW2BSTR
{
	BSTR	_p;
	CW2BSTR(LPCWSTR s){ _p = ::SysAllocString(s); ASSERT(_p); }
	~CW2BSTR(){ _SafeFreeBSTR(_p); }
	operator BSTR(){ return _p; }
};


class CDIB  //DIB is bottom-up, BGR/BGRA
{
protected:
	HGLOBAL	m_hDIB;

	LPBYTE	m_pBits;		
	UINT	m_DataOffset; 

	LPBYTE	m_pBitsAux;		//available when m_DataOffset == 0xffffffff

public:
	CDIB();
	~CDIB(){ Destroy(); }

	BOOL	Create(UINT width, UINT height, UINT BPP);
	void	Destroy();

	void	GetFormat(UINT* width=NULL, UINT* height=NULL, UINT* BPP=NULL);
	LPBYTE	GetBits();
	LPCBYTE	GetBits() const;

	void	Attach(HGLOBAL hDIB);
	HGLOBAL	Detach();

	operator HGLOBAL() const{ return m_hDIB; }
	CDIB& operator = (HGLOBAL hDIB);

	void	VerticalFlip();
	void	ChannelFlip();		//RGB <-> BGR / RGBA <-> BGRA
};

void ShowMouseCursor();
void HideMouseCursor();
BOOL OpenDefaultBrowser(LPCTSTR url, DWORD show_window = SW_SHOWNORMAL);	// open url in newly created browser
BOOL WriteHtmlDocument(IHTMLDocument2* doc, LPCTSTR html);
IHTMLElement* SearchHtmlElement(IHTMLDocument2* doc, LPCWSTR id, LPCWSTR tag = NULL, UINT after_occuerrence = 0);
BOOL		  InvokeJavascript(IHTMLDocument2* doc, LPCWSTR func, LPCWSTR* args, int argc);
IHTMLDocument2* GetHtmlDocument(LPDISPATCH disp_doc);

class CFullPathName
{	
private:
	
protected:
	::rt::String	_unc_full_path;
	::rt::String	_path;
	::rt::String	_title;
	::rt::String	_filename;
	LPCTSTR			_ext;		// refer to _filename

public:
	CFullPathName(LPCTSTR full_UNC_path);
	BOOL	IsLocal() const;
	BOOL	IsRemote() const;
	LPCTSTR	GetHostName() const;	// return server name for "\\host\"
	//TCHAR	GetDiskId() const;		// C:
	LPCTSTR GetPathName() const{ return _path; }		// C:\abc
	LPCTSTR GetFileName() const{ return _filename; }	// xxx.dat
	LPCTSTR GetFileTitle() const{ return _title; }		// without extend name
	LPCTSTR GetExtName() const{ return _ext; }			// extend name
	operator LPCTSTR ()const { return _unc_full_path; }
};

class CDirectoryDialog
{
	static UINT_PTR CALLBACK	_OFNHOOKPROC(HWND hwnd, UINT msg, WPARAM wparam, LPARAM lParam);
	static LRESULT CALLBACK		_WndProcMy(HWND hwnd,UINT message, WPARAM wParam, LPARAM lParam);
	static WNDPROC				g_DefaultFileDialogProc;

	LPCTSTR	_caption_dialog;
	LPCTSTR _caption_filename;
	static const int PATH_BUFFER_SIZE = MAX_PATH*5 ;

protected:
	TCHAR	m_Path[PATH_BUFFER_SIZE];
	BOOL	m_Done;

public:
	CDirectoryDialog(LPCTSTR caption_dialog = NULL, LPCTSTR caption_filename = NULL);
	LPTSTR GetDirectoryName(HWND hWnd, BOOL for_open = TRUE);
	operator LPTSTR ();		// return NULL if user cancelled
};

class CLaunchProcess
{
	HANDLE hChildStdinRd, hChildStdinWrDup, 
		   hChildStdoutWr,hChildStdoutRdDup;

	DWORD	_Flag;
	HANDLE	_hOutputHookThread;
	void	_HookedOutput(char* p, UINT len);

protected:
	w32::CCriticalSection	m_CCS;
	HANDLE					m_hProcess;
	::rt::StringA			m_Output;
	DWORD					m_ExitCode;
	UINT					m_ExecutionTime;		// in msec
	w32::CTime64<>			m_ExitTime;				// available after call IsRunning and it returns false
	void					ClearAll();
	static DWORD WINAPI		OutputHookThread(LPVOID);

public:
	enum
	{	FLAG_ROUTE_OUTPUT	= 0x1,
		FLAG_SAVE_OUTPUT	= 0x2,	// retrieve by GetOutput/GetOutputLen
	};
	CLaunchProcess();
	~CLaunchProcess();
	BOOL	Launch(LPCTSTR cmdline, DWORD flag = FLAG_ROUTE_OUTPUT, DWORD window_show = SW_HIDE, LPCTSTR pWorkDirectory = NULL, LPVOID pEnvVariable = NULL);
	BOOL	WaitForEnding(DWORD timeout = INFINITE); // return false when timeout
	void	Terminate();
	BOOL	IsRunning();
	LPCSTR	GetOutput();
	UINT	GetOutputLen();
	UINT	GetExecutionTime() const { return m_ExecutionTime; }		// available after IsRunning() returns false!	
	UINT	GetExitCode() const { return m_ExitCode; }					// available after IsRunning() returns false!	
	const w32::CTime64<>& GetExitTime() const { return m_ExitTime; }	// available after IsRunning() returns false!
};

DWORD	FindProcessByBaseName(LPCTSTR binname);	// return 0 if not find
BOOL	KillProcess(DWORD process_id);

class CPerformanceCounters
{
	HANDLE _Query;
	enum tagValType
	{	PCVT_DBL = 0,
		PCVT_I64,
		PCVT_I32
	};
	struct t_Counter
	{	HANDLE				hCounter;
		rt::String			TextFormat;
		rt::String			FriendName;
		UINT				Denominator;
		tagValType			ValueType;
	};
	rt::BufferEx<t_Counter>	_Counters;
	rt::String				_TempText;

public:
	CPerformanceCounters();
	~CPerformanceCounters();
	BOOL AddCounter(LPCTSTR pFullPathName, LPCTSTR TextFormat = NULL, UINT Denominator = 1);	// TextFormat: e.g.  "DBL|CPU Load|%2d%%"
	BOOL UpdateAll();
	
	CPerformanceCounters(LPCTSTR pFullPathName);

	double			GetValue_DBL(UINT i = 0);
	LONGLONG		GetValue_I64(UINT i = 0);
	LONG			GetValue_I32(UINT i = 0);
	UINT			GetSize() const { return (UINT)_Counters.GetSize(); }

	rt::String_Ref	GetValue_Text(UINT i = 0);
	const rt::String_Ref& GetFriendName(UINT i) const { return _Counters[i].FriendName; }
};

class CWin32Service	// singleton
{
protected:
	volatile LPVOID			m_hServiceHandler;
	static CWin32Service*	_pWin32Service;
	LPCTSTR					_ServiceName;

public:
	static BOOL InstallService(LPCTSTR svc_name, LPCTSTR display_name = NULL, LPCTSTR desc = NULL, LPCTSTR arguments = NULL, LPCTSTR binpath = NULL, LPCTSTR  dependence = NULL);
	static BOOL UninstallService(LPCTSTR svc_name);

public:
	CWin32Service();
	~CWin32Service();
	BOOL	InitializeService(LPCTSTR svc_name);	// NOT thread-safe
	void	ReportServiceStatus(DWORD state = 0x7);	// SERVICE_PAUSED
	virtual void OnServiceControl(DWORD dwControl){};	// dwControl = SERVICE_CONTROL_STOP/SERVICE_CONTROL_SHUTDOWN/ ...
};


class CGarbageCollection	// singleton
{
protected:
	typedef void (*LPFUNC_DELETION)(LPVOID x);

	struct item
	{	LPVOID			pObj;
		LPFUNC_DELETION	pDeletionFunction;
		int				TTL;
		void			Delete();
		BOOL			Tick(); // return TRUE when the item should be NOT deleted
	};

	rt::BufferEx<item>		m_PendingDeletion;
	w32::CCriticalSection	m_CCS;
	HANDLE					m_hDeletionThread;

	void	_DeletionThread();

	void	DeleteObject(LPVOID x, DWORD TTL_sec, LPFUNC_DELETION delete_func);
	static	CGarbageCollection*	g_pDDC;

public:
	CGarbageCollection();
	~CGarbageCollection();
	
	template<typename OBJ>
	static void DeleteObj(OBJ * ptr, int TTL_sec)
	{	struct _func{ static void delete_func(LPVOID x){ delete ((OBJ *)x); } };
		g_pDDC->DeleteObject(ptr,TTL_sec,_func::delete_func);
	}
	template<typename OBJ>
	static void DeleteArray(OBJ * ptr, int TTL_sec)
	{	struct _func{ static void delete_func(LPVOID x){ delete [] ((OBJ *)x); } };
		g_pDDC->DeleteObject(ptr,TTL_sec,_func::delete_func);
	}
	template<typename OBJ>
	static void DeleteRelease(OBJ * ptr, int TTL_sec)
	{	struct _func{ static void delete_func(LPVOID x){ ((OBJ *)x)->Release(); } };
		g_pDDC->DeleteObject(ptr,TTL_sec,_func::delete_func);
	}
	static void DeleteCoTaskMem(LPVOID ptr, int TTL_sec)
	{	struct _func{ static void delete_func(LPVOID x){ ::CoTaskMemFree(x); } };
		g_pDDC->DeleteObject(ptr,TTL_sec,_func::delete_func);
	}
	static void Delete32AL(LPVOID ptr, int TTL_sec)
	{	struct _func{ static void delete_func(LPVOID x){ rt::Free32AL(x); } };
		g_pDDC->DeleteObject(ptr,TTL_sec,_func::delete_func);
	}
	static void Delete(LPVOID ptr, int TTL_sec, LPFUNC_DELETION dfunc )
	{	g_pDDC->DeleteObject(ptr,TTL_sec,dfunc);
	}
};


#define _SafeDel_Delayed(x, TTL)		{ if(x){ w32::CGarbageCollection::DeleteObj(x,TTL); x=NULL; } }
#define _SafeDelArray_Delayed(x,TTL)	{ if(x){ w32::CGarbageCollection::DeleteArray(x,TTL); x=NULL; } }
#define _SafeRelease_Delayed(x,TTL)		{ if(x){ w32::CGarbageCollection::DeleteRelease(x,TTL); x=NULL; } }
#define _SafeCoFree_Delayed(x,TTL)		{ if(x){ w32::CGarbageCollection::DeleteCoTaskMem(x,TTL); x=NULL; } }
#define _SafeFree32AL_Delayed(x,TTL)	{ if(x){ w32::CGarbageCollection::Delete32AL(x,TTL); x=NULL; } }


namespace image_codec   // access to PFM
{
	class _PFM_Header
	{	friend HRESULT _Open_PFM(LPCTSTR fn,_PFM_Header* pHeader);
		friend HRESULT _Read_PFM(_PFM_Header* pHeader,LPFLOAT pData,UINT ch,UINT step);
		w32::CFile64	file;
	public:
		UINT			width;
		UINT			height;
		UINT			ch;
	};
	HRESULT _Write_PFM(LPCTSTR fn,LPCFLOAT pData,UINT ch,UINT w,UINT h,UINT step);
	HRESULT _Open_PFM(LPCTSTR fn,_PFM_Header* pHeader);
	HRESULT _Read_PFM(const _PFM_Header* pHeader,LPFLOAT pData,UINT ch,UINT step);
} // namespace im_codec


} // namespace w32


