#include "StdAfx.h"
#include "debug_log.h"
#include "dshow_media.h"
#include <uuids.h>
#include <vfwmsgs.h>
#include <Dvdmedia.h>
#include <mmreg.h>
#include <amvideo.h>
#include <math.h>
#include <mmsystem.h>

#include <atlexcept.h>
#include <atlconv.h>

#define		SetRetH(x)	{if(Ret){*Ret = (x);}else{x;}}

namespace w32
{
namespace dshow
{

IGraphBuilder * CreateFilterGraph(HRESULT * Ret)
{
	IGraphBuilder * out;

	HRESULT ret = CoCreateInstance(CLSID_FilterGraph, NULL, CLSCTX_INPROC_SERVER,IID_IGraphBuilder, (void **)&out);
	SetRetH(ret);

	return out;
}

BOOL IsFilterAdded(IBaseFilter* pFilter, IFilterGraph* pGraph)
{
	ASSERT(pFilter);
	ASSERT(pGraph);

	IEnumFilters* pEnum = NULL;
	if(SUCCEEDED(pGraph->EnumFilters(&pEnum)))
	{
		IBaseFilter* pAnyFilter;
		while(SUCCEEDED(pEnum->Next(1,&pAnyFilter,NULL)))
		{
			ASSERT(pFilter);
			if(pAnyFilter == pFilter)
			{
				_SafeRelease(pAnyFilter);
				_SafeRelease(pEnum);
				return TRUE;
			}
			_SafeRelease(pAnyFilter);
		}
	}

	_SafeRelease(pEnum);
	return FALSE;
}


DWORD	AddFilterGraphToRot(IUnknown *pUnkGraph, HRESULT * Ret)
{
    TCHAR Name[128];
	wsprintf(Name, _T("FilterGraph %08x pid %08x"), (DWORD_PTR)pUnkGraph, GetCurrentProcessId());

	ASSERT(pUnkGraph);

	DWORD dwRegister = 0;

	HRESULT hr;

    IMoniker * pMoniker;
    IRunningObjectTable *pROT;
    if (SUCCEEDED(hr = GetRunningObjectTable(0, &pROT))) 
    {
		HRESULT hr = CreateItemMoniker(L"!", ATL::CT2CW(Name), &pMoniker);
		if (SUCCEEDED(hr)) 
		{
			// Use the ROTFLAGS_REGISTRATIONKEEPSALIVE to ensure a strong reference
			// to the object.  Using this flag will cause the object to remain
			// registered until it is explicitly revoked with the Revoke() method.
			//
			// Not using this flag means that if GraphEdit remotely connects
			// to this graph and then GraphEdit exits, this object registration 
			// will be deleted, causing future attempts by GraphEdit to fail until
			// this application is restarted or until the graph is registered again.
			hr = pROT->Register(ROTFLAGS_REGISTRATIONKEEPSALIVE, pUnkGraph, 
								pMoniker, &dwRegister);
			pMoniker->Release();
		}

		pROT->Release();
    }

	SetRetH(hr);

    return dwRegister;
}


void RemoveObjectFromRot(DWORD pdwRegister)
{
    IRunningObjectTable *pROT;

    if (SUCCEEDED(GetRunningObjectTable(0, &pROT))) 
    {
        pROT->Revoke(pdwRegister);
        pROT->Release();
    }
}

IBaseFilter * AddFileSourceToFilterGraph(IGraphBuilder *pGraph, LPCTSTR Fn, LPCTSTR FilterName, OUT HRESULT * Ret)
{
	ASSERT(pGraph);
	ASSERT(Fn);

	IBaseFilter * ff = NULL;

	ATL::CT2CW file(Fn);
	ATL::CT2CW name(FilterName);

	if(Ret)
		*Ret = pGraph->AddSourceFilter(file,name,&ff);
	else
		pGraph->AddSourceFilter(file,name,&ff);
	
	return ff;
}

LPPinsSummary	QueryPinInformation(IBaseFilter * filter,OUT HRESULT * Ret)
{
	ASSERT(filter);

	HRESULT hr = S_OK;

	IEnumPins * pEnum = NULL;
	hr = filter->EnumPins(&pEnum);
	LPPinsSummary summ = NULL;

	if(pEnum)
	{
		//Count
		int	co = 0;
		while(pEnum->Skip(1) == S_OK)co++;

		//Create Summary
		summ = (LPPinsSummary)new BYTE[sizeof(PinsSummary)+sizeof(PinInfo)*(co-_AnyLength)];
		if(summ)
		{
			//Dump all pins
			VERIFYH(pEnum->Reset());
			IPin*	pPin = NULL;
			int		in_pos = 0;
			int		out_pos = co-1;
			while((out_pos>=in_pos) && pEnum->Next(1,&pPin,NULL) == S_OK)
			{
				LPPinInfo	pinfo;
				PIN_DIRECTION dir;

				if(SUCCEEDED(pPin->QueryDirection(&dir)))
				{
					if(dir == PINDIR_INPUT)
					{
						pinfo = &summ->Pins[in_pos++];
					}
					else
					{
						pinfo = &summ->Pins[out_pos--];
					}

					pinfo->pPin = pPin;
					VERIFYH(pPin->QueryId(&pinfo->Name));

					//Media type
					BOOL			typeok = FALSE;
					AM_MEDIA_TYPE	type;

					if(SUCCEEDED(pPin->ConnectionMediaType(&type)))
					{
						pinfo->Connected = TRUE;
						typeok = TRUE;
					}
					else
					{
						pinfo->Connected = FALSE;
						AM_MEDIA_TYPE * ptype = NULL;

						IEnumMediaTypes * pEMT = NULL;
						if(SUCCEEDED(pPin->EnumMediaTypes(&pEMT)))
						{
							if(pEMT->Next(1,&ptype,NULL) == S_OK)
							{
								memcpy(&type,ptype,sizeof(AM_MEDIA_TYPE));
								_SafeCoFree(ptype);
								typeok = TRUE;
							}

							_SafeRelease(pEMT);
						}
					}

					if(typeok)
					{
						//Dump
						if(memcmp(&type.majortype,&MEDIATYPE_Video,sizeof(GUID)) == 0)
						{
							pinfo->MediaType = MT_Video;
						}
						else if(memcmp(&type.majortype,&MEDIATYPE_Audio,sizeof(GUID)) == 0)
						{
							pinfo->MediaType = MT_Audio;
						}
						else if(memcmp(&type.majortype,&MEDIATYPE_Text,sizeof(GUID)) == 0)
						{
							pinfo->MediaType = MT_Text;
						}
						else if(memcmp(&type.majortype,&MEDIATYPE_Stream,sizeof(GUID)) == 0)
						{
							pinfo->MediaType = MT_ByteCompressed;
						}
						else if(memcmp(&type.majortype,&MEDIATYPE_Timecode,sizeof(GUID)) == 0)
						{
							pinfo->MediaType = MT_Timecode;
						}
						else
						{
							pinfo->MediaType = MT_Unknown;
						}
						
						if( (pinfo->MediaType == MT_Video) )
						{
							if( type.bTemporalCompression || type.lSampleSize == 0 || type.bFixedSizeSamples == FALSE )
							{
								pinfo->MediaType = MT_VideoCompressed;
							}
						}

						if( (pinfo->MediaType == MT_Audio) )
						{
							if(memcmp(&type.subtype,&MEDIASUBTYPE_PCM,sizeof(GUID)))
							{
								pinfo->MediaType = MT_AudioCompressed;
							}
						}

						//Clear
						_SafeCoFree(type.pbFormat);
						_SafeRelease(type.pUnk);
					}
					else
					{
						pinfo->MediaType = MT_NotAvailable;
					}
				}
				else
					_SafeRelease(pPin);
			}

			summ->InputPinCount = in_pos;
			summ->OutputPinCount = co - out_pos - 1;

			if(in_pos != out_pos+1)
			{// some pmissed, move out pins down.
				ASSERT(in_pos<=out_pos);
				memmove(&summ->Pins[in_pos],&summ->Pins[out_pos+1],sizeof(LPPinInfo)*(co - out_pos - 1));
			}
			
			hr = S_OK;
		}
		else
			hr = E_OUTOFMEMORY;
	}

	_SafeRelease(pEnum);
	SetRetH(hr);

	return summ;
}


void	FreePinsSummary(LPPinsSummary *lpp)
{
	ASSERT(lpp);
	ASSERT(*lpp);

	int	co = (*lpp)->InputPinCount + (*lpp)->OutputPinCount;
	int i;
	for(i=0;i<co;i++)
	{
		_SafeRelease((*lpp)->Pins[i].pPin);
		_SafeCoFree((*lpp)->Pins[i].Name);
	}

	delete [] ((LPBYTE)(*lpp));
	*lpp = NULL;
}


void FreeMediaType(AM_MEDIA_TYPE& mt)
{
    if (mt.cbFormat != 0)
    {
        CoTaskMemFree((PVOID)mt.pbFormat);
        mt.cbFormat = 0;
        mt.pbFormat = NULL;
    }
    if (mt.pUnk != NULL)
    {
        // Unecessary because pUnk should not be used, but safest.
        mt.pUnk->Release();
        mt.pUnk = NULL;
    }
}


HRESULT ConnectFileWriter(IGraphBuilder *pGraph,IBaseFilter* pFrom, LPCTSTR fn,IBaseFilter** pFileWriter_out)
{
	HRESULT hr = S_OK;

	IBaseFilter* pFileWriter = NULL;
	IFileSinkFilter2* pFile = NULL;
	IPin* pin_in = NULL;

	if(	(pFileWriter = CreateFilter(CLSID_FileWriter,&hr)) &&
		(SUCCEEDED(hr = pFileWriter->QueryInterface(__uuidof(IFileSinkFilter2),(LPVOID*)&pFile))) &&
		(SUCCEEDED(hr = pFile->SetMode(AM_FILE_OVERWRITE))) &&
		(SUCCEEDED(hr = pFile->SetFileName(ATL::CT2CW(fn),NULL))) &&
		(SUCCEEDED(hr = pGraph->AddFilter(pFileWriter,L"File"))) &&
		(SUCCEEDED(hr = ConnectFilters(pGraph,pFrom,pFileWriter))) &&
		(pin_in = SearchPin(pFileWriter,PT_Direction_Input,PT_DoNotCare,PT_Connected)) 
	)
	{
		hr = S_OK;
	}
	else
	{
		_SafeRelease(pFileWriter); 
		hr = SUCCEEDED(hr)?E_FAIL:hr;
	}

	_SafeRelease(pFile);
	_SafeRelease(pin_in);

	if(pFileWriter)
	{
		if(pFileWriter_out)*pFileWriter_out = pFileWriter;
		else
		{
			_SafeRelease(pFileWriter); 
		}
	}

	return hr;
}

HRESULT ConnectFileWriter(IGraphBuilder *pGraph,IPin* pFrom, LPCTSTR fn,IBaseFilter** pFileWriter_out)
{
	HRESULT hr = S_OK;

	IBaseFilter* pFileWriter = NULL;
	IFileSinkFilter2* pFile = NULL;
	IPin* pin_in = NULL;

	if(	(pFileWriter = CreateFilter(CLSID_FileWriter,&hr)) &&
		(SUCCEEDED(hr = pFileWriter->QueryInterface(__uuidof(IFileSinkFilter2),(LPVOID*)&pFile))) &&
		(SUCCEEDED(hr = pFile->SetMode(AM_FILE_OVERWRITE))) &&
		(SUCCEEDED(hr = pFile->SetFileName(ATL::CT2CW(fn),NULL))) &&
		(SUCCEEDED(hr = pGraph->AddFilter(pFileWriter,L"File"))) &&
		(SUCCEEDED(hr = ConnectPinToFilter(pGraph,pFrom,pFileWriter))) &&
		(pin_in = SearchPin(pFileWriter,PT_Direction_Input,PT_DoNotCare,PT_Connected)) 
	)
	{
		hr = S_OK;
	}
	else
	{
		_SafeRelease(pFileWriter); 
		hr = SUCCEEDED(hr)?E_FAIL:hr;
	}

	_SafeRelease(pFile);
	_SafeRelease(pin_in);

	if(pFileWriter)
	{
		if(pFileWriter_out)*pFileWriter_out = pFileWriter;
		else
		{
			_SafeRelease(pFileWriter); 
		}
	}

	return hr;
}


IBaseFilter* CreateFilter(REFCLSID FilterCLSID,HRESULT *Ret)
{
	HRESULT hr;

	IBaseFilter* pFilter = NULL;
	hr = CoCreateInstance(FilterCLSID,NULL,CLSCTX_INPROC_SERVER,IID_IBaseFilter,(LPVOID *)&pFilter);
	
	SetRetH(hr);
	return pFilter;
}


IPin*	_tagPinsSummary::GetPinByName(LPCTSTR Name)
{
	int co,i;
	co = InputPinCount+OutputPinCount;
	LPPinInfo pin = Pins;

	ATL::CT2CW str(Name);

	for(i=0;i<co;i++,pin++)
	{
		if(pin->Name && wcscmp(str,pin->Name) == 0)
		{
			ASSERT(pin->pPin);
			pin->pPin->AddRef();
			return pin->pPin;
		}
	}

	return NULL;
}

IPin*	_tagPinsSummary::GetPinByType(PinType Direction,PinType MediaType, PinType Connection)
{
	int co,i;
	LPPinInfo pin;
	if(Direction)
	{
		if(Direction == PT_Direction_Input)
		{
			co = InputPinCount;
			pin = Pins;
		}
		else
		{
			ASSERT(Direction == PT_Direction_Output);
			co = OutputPinCount;
			pin = &Pins[InputPinCount];
		}
	}
	else
	{
		co = InputPinCount+OutputPinCount;
		pin = Pins;
	}

	for(i=0;i<co;i++,pin++)
	{
		if(MediaType)
		{
			switch(MediaType)
			{
				case PT_Media_AnyVideo:
					if( (pin->MediaType&MT_MediaTypeMask) != MT_Video)goto NotMatch;
					break;
				case PT_Media_AnyAudio:
					if( (pin->MediaType&MT_MediaTypeMask) != MT_Audio)goto NotMatch;
					break;
				case PT_Media_RawVideo:
					if( (pin->MediaType) != MT_Video)goto NotMatch;
					break;
				case PT_Media_RawAudio:
					if( (pin->MediaType) != MT_Audio)goto NotMatch;
					break;
				case PT_Media_CompressedVideo:
					if( (pin->MediaType) != MT_VideoCompressed)goto NotMatch;
					break;
				case PT_Media_CompressedAudio:
					if( (pin->MediaType) != MT_AudioCompressed)goto NotMatch;
					break;
				case PT_Media_CompressedByte:
					if( (pin->MediaType) != MT_ByteCompressed)goto NotMatch;
					break;
				case PT_Media_Text:
					if( (pin->MediaType) != MT_Text)goto NotMatch;
					break;
				case PT_Media_Timecode:
					if( (pin->MediaType) != MT_Timecode)goto NotMatch;
					break;
				default:ASSERT(0);
			}
		}

		if(Connection)
		{
			switch(Connection)
			{
				case PT_Connected:
					if( !pin->Connected )goto NotMatch;
					break;
				case PT_Unconnected:
					if( pin->Connected )goto NotMatch;
					break;
				default:ASSERT(0);
			}
		}

		//Ok a match pis found
		ASSERT(pin->pPin);
		pin->pPin->AddRef();
		return pin->pPin;

NotMatch:
		continue;
	}

	return NULL;
}

class _CPinRecursiveSearch
{
protected:
	PinType Direction;
	PinType MediaType;
	PinType Connection;

public:
	_CPinRecursiveSearch(PinType iDirection,PinType iMediaType,PinType iConnection)
	{
		ASSERT(Direction);
		ASSERT(MediaType);

		Direction = iDirection;
		MediaType = iMediaType;
		Connection = iConnection;
	}

    IPin* Search(LPPinsSummary	pSumm)
	{
		ASSERT(pSumm);

		{IPin* p;
			p = pSumm->GetPinByType(Direction,MediaType,Connection);
			if(p)return p;

			//Try linked pins
			LPPinInfo pin,pend;
			{
				int co;
				if(Direction)
				{
					if(Direction == PT_Direction_Input)
					{
						co = pSumm->InputPinCount;
						pin = pSumm->Pins;
					}
					else
					{
						ASSERT(Direction == PT_Direction_Output);
						co = pSumm->OutputPinCount;
						pin = &pSumm->Pins[pSumm->InputPinCount];
					}
				}
				else
				{
					co = pSumm->InputPinCount+pSumm->OutputPinCount;
					pin = pSumm->Pins;
				}

				pend = &pin[co];
			}

			for(;pin<pend;pin++)
			{
				if(pin->Connected)
				{
					//go to peer pin
					LPPinsSummary pPeerSum = NULL;
					ASSERT(pin->pPin);
					{
						IPin* p;
						pin->pPin->ConnectedTo(&p);
						ASSERT(p);

						PIN_INFO pInfo;
						p->QueryPinInfo(&pInfo);
						ASSERT(pInfo.pFilter);
						pPeerSum = QueryPinInformation(pInfo.pFilter);

						_SafeRelease(p);
						_SafeRelease(pInfo.pFilter);
					}

					if(pPeerSum)
					{
						IPin* p = Search(pPeerSum);
						FreePinsSummary(&pPeerSum);

						if(p)return p;
					}
				}
			}
		}

		return NULL;
	}
};

IPin*	_tagPinsSummary::GetPinByTypeEx(PinType Direction, PinType MediaType, PinType Connection)	//Recursively search pins
{
	_CPinRecursiveSearch searcher(Direction,MediaType,Connection);
	return searcher.Search(this);
}

IBaseFilter* CreateFilterByPin(IPin*	pPin, BOOL PeerRequired, HRESULT* Ret)
{
	ASSERT(pPin);
	
	HRESULT hr;
	GUID*	pTypes = NULL;
	DWORD	TypeCount = 0;

	{
		IEnumMediaTypes*	pEMT = NULL;
		hr = pPin->EnumMediaTypes(&pEMT);
		if(pEMT)
		{
			while(pEMT->Skip(1) == S_OK)TypeCount++;

			if(TypeCount)
			{
				pTypes = new GUID[TypeCount*2];
				if(pTypes)
				{
					AM_MEDIA_TYPE*	ptype;
					VERIFYH(pEMT->Reset());
					GUID*	pG = pTypes;
					while(pEMT->Next(1,&ptype,NULL) == S_OK)
					{
						ASSERT(ptype);

						memcpy(pG,&ptype->majortype,sizeof(GUID));
						pG++;

						memcpy(pG,&ptype->subtype,sizeof(GUID));
						pG++;

						_SafeCoFree(ptype);
					}
				}
				else
					hr = E_OUTOFMEMORY;
			}
			else
				hr = VFW_E_NO_TYPES;
		}

		_SafeRelease(pEMT);
	}

	SetRetH(hr);
	if(!pTypes)return NULL;

	PIN_DIRECTION dir;
	pPin->QueryDirection(&dir);

    IFilterMapper2*	FilterMap = NULL;
	hr = CoCreateInstance(CLSID_FilterMapper2,NULL,CLSCTX_INPROC_SERVER,IID_IFilterMapper2,(LPVOID *)&FilterMap);

	IBaseFilter* pFilter = NULL;

	if(FilterMap)
	{
		IEnumMoniker*	pEM;
		if(dir == PINDIR_INPUT)
		{
			//Seeking a output filter
			hr = FilterMap->EnumMatchingFilters(&pEM,0,TRUE,0,PeerRequired,0,NULL,NULL,NULL,NULL,TRUE,TypeCount,pTypes,NULL,NULL);
		}
		else
		{
			//Seeking a input filter
			ASSERT(dir == PINDIR_OUTPUT);
			hr = FilterMap->EnumMatchingFilters(&pEM,0,TRUE,0,TRUE,TypeCount,pTypes,NULL,NULL,NULL,PeerRequired,0,NULL,NULL,NULL);
		}

        if(pEM)
		{
			IMoniker*	pM = NULL;

			while(pEM->Next(1,&pM,NULL) == S_OK)
			{
				//CLSID	id;

				if(SUCCEEDED(BindMoniker(pM,0,IID_IBaseFilter,(LPVOID *)&pFilter)))
				{
					_SafeRelease(pM);
					break;
				}

				_SafeRelease(pM);
			}
			
			_SafeRelease(pEM);
		}

		_SafeRelease(FilterMap);
	}

	_SafeDelArray(pTypes);
	SetRetH(hr);
	return pFilter;
}

IBaseFilter*	CreateFilter(IEnumMoniker* pEnumMoniker,LPCTSTR FriendName, BOOL StrictlyMatch, HRESULT* Ret)
{
	ASSERT(pEnumMoniker);

	HRESULT hr;

	SetRetH(S_OK);

	IMoniker *pMoniker = NULL;
	while (pEnumMoniker->Next(1, &pMoniker, NULL) == S_OK)
	{
		ASSERT(pMoniker);

		BOOL IsMatched;

		if(FriendName)   // Match for name
		{
			IsMatched = FALSE;
			IPropertyBag *pPropBag;

			hr = pMoniker->BindToStorage(0, 0, IID_IPropertyBag,(void**)(&pPropBag));
			if(SUCCEEDED(hr))
			{
				// Find the friendly name
				VARIANT varName;
				VariantInit(&varName);

				hr = pPropBag->Read(L"FriendlyName", &varName, 0);

				if(SUCCEEDED(hr))
				{
					if(StrictlyMatch)
					{
						if(_tcscmp(ATL::CW2T(varName.bstrVal),FriendName) == 0)IsMatched = TRUE;
					}
					else
					{
						if(_tcsstr(ATL::CW2T(varName.bstrVal),FriendName))IsMatched = TRUE;
					}

					VariantClear(&varName); 
				}

				_SafeRelease(pPropBag);
			}
		}
		else
		{
			IsMatched = TRUE;
		}

		if(IsMatched)
		{
			IBaseFilter *pCap = NULL;
			hr = pMoniker->BindToObject(0, 0, IID_IBaseFilter, (void**)&pCap);
			if(SUCCEEDED(hr))
			{
				_SafeRelease(pMoniker);
				return pCap;
			}
		}

		_SafeRelease(pMoniker);
	}

	SetRetH(hr);
	return NULL;
}

IPin* SearchPin(IBaseFilter* pFilter, PinType Direction, PinType MediaType, PinType Connection)
{
	IPin* out = NULL;
	LPPinsSummary	Pins = QueryPinInformation(pFilter);
	if(Pins)
	{
		out = Pins->GetPinByType(Direction,MediaType,Connection);
		FreePinsSummary(&Pins); 
	}

	return out;
}

IPin* SearchPinEx(IGraphBuilder *pGraph,IBaseFilter* pFilter, PinType Direction, PinType MediaType, PinType Connection)
{
	IPin* out = NULL;
	LPPinsSummary	Pins = QueryPinInformation(pFilter);
	if(Pins)
	{
		out = Pins->GetPinByTypeEx(Direction,MediaType,Connection);
		FreePinsSummary(&Pins); 
	}

	return out;
}

HRESULT	ConnectFilters(IGraphBuilder *pGraph,IBaseFilter* pFilterFrom,IBaseFilter* pFilterTo,MediaType mt)
{
	ASSERT(pGraph);
	ASSERT(pFilterFrom);
	ASSERT(pFilterTo);

    HRESULT hr;
	BOOL	OutputFormatMismatch = TRUE;

	LPPinsSummary	pFromPins = NULL,pToPins = NULL;
	pFromPins = QueryPinInformation(pFilterFrom,&hr);
	if(pFromPins)
	{
		pToPins = QueryPinInformation(pFilterTo,&hr);
		if(pToPins)
		{
			LPPinInfo	FromPin = pFromPins->GetOutputPins();
			int			FromCo  = pFromPins->OutputPinCount;

			LPPinInfo	ToPin = pToPins->GetInputPins();
			int			ToCo  = pToPins->InputPinCount;

			for(int i=0;i<FromCo;i++)
			{
				if( ((FromPin[i].MediaType&MT_MediaTypeMask) == mt || 
					  mt == MT_DoNotCare || 
					  MT_ByteCompressed == FromPin[i].MediaType
					 ) && !FromPin[i].Connected )
				{
					OutputFormatMismatch = FALSE;

					for(int j=0;j<ToCo;j++)
					{
						if( ( ( ToPin[j].MediaType&MT_MediaTypeMask) == mt || 
								mt == MT_DoNotCare || 
								ToPin[j].MediaType == MT_NotAvailable
							) && !ToPin[j].Connected )
						{
							hr = pGraph->Connect(FromPin[i].pPin,ToPin[j].pPin);
							if(SUCCEEDED(hr))
							{
								if(pFromPins)FreePinsSummary(&pFromPins);
								if(pToPins)FreePinsSummary(&pToPins);

								return S_OK;
							}
						}
					}
				}
			}

		}
	}

	if(pFromPins)FreePinsSummary(&pFromPins);
	if(pToPins)FreePinsSummary(&pToPins);

	return OutputFormatMismatch?VFW_E_NOT_FOUND:VFW_E_CANNOT_CONNECT;
}

HRESULT	ConnectPinToFilter(IGraphBuilder *pGraph,IPin* pPinFrom,IBaseFilter* pFilterTo,MediaType mt)
{
	ASSERT(pGraph);
	ASSERT(pPinFrom);
	ASSERT(pFilterTo);

    HRESULT hr;
	LPPinsSummary	pToPins = NULL;
	pToPins = QueryPinInformation(pFilterTo,&hr);
	if(pToPins)
	{
		LPPinInfo	ToPin = pToPins->GetInputPins();
		int			ToCo  = pToPins->InputPinCount;

		for(int j=0;j<ToCo;j++)
		{
			if( ( ( ToPin[j].MediaType&MT_MediaTypeMask) == mt || 
					mt == MT_DoNotCare || 
					ToPin[j].MediaType == MT_NotAvailable
				) && !ToPin[j].Connected )
			{
				hr = pGraph->Connect(pPinFrom,ToPin[j].pPin);
				if(SUCCEEDED(hr))
				{
					FreePinsSummary(&pToPins);
					return S_OK;
				}
			}
		}
	}

	if(pToPins)FreePinsSummary(&pToPins);
	return VFW_E_CANNOT_CONNECT;
}

UINT GetFilterName(IBaseFilter* pFilter,LPTSTR NameBuffer,int BufLen) //return length of the name not include terminator zero
{
	ASSERT(pFilter);

	FILTER_INFO fi;
	if(SUCCEEDED(pFilter->QueryFilterInfo(&fi)))
	{
		int len = (int)wcslen(fi.achName);
		if(NameBuffer)
		{	
			len = min(len,BufLen-1);
			memcpy(NameBuffer,ATL::CW2T(fi.achName),len*sizeof(TCHAR));
			NameBuffer[len] = 0;
		}

		return len;
	}

	return 0;
}

UINT GetFilterVendorInfo(IBaseFilter* pFilter,LPTSTR VendorBuffer,int BufLen) //return length of the name not include terminator zero
{
	ASSERT(pFilter);

	LPWSTR pv=NULL;
	if(SUCCEEDED(pFilter->QueryVendorInfo(&pv)))
	{
		int len = (int)wcslen(pv);
		if(VendorBuffer)
		{	
			len = min(len,BufLen-1);
			memcpy(VendorBuffer,ATL::CW2T(pv),len*sizeof(TCHAR));
			VendorBuffer[len] = 0;
		}

		CoTaskMemFree(pv);
		return len;
	}

	return 0;
}

IEnumMoniker* 	EnumFilters(REFCLSID CategoryClsid, HRESULT* Ret)
{
	ICreateDevEnum *pDevEnum = NULL;
	IEnumMoniker *pEnum = NULL;

	SetRetH(S_OK);
	// Create the System Device Enumerator.
	HRESULT hr = CoCreateInstance(CLSID_SystemDeviceEnum, NULL,
								  CLSCTX_INPROC_SERVER, 
								  IID_ICreateDevEnum, 
								  reinterpret_cast<void**>(&pDevEnum));
	if (SUCCEEDED(hr))
	{
		// Create an enumerator for the video capture category.
		hr = pDevEnum->CreateClassEnumerator(CategoryClsid,&pEnum, 0);
		_SafeRelease(pDevEnum);
		if(SUCCEEDED(hr))
		{
			return pEnum;
		}
		else
		{
			_SafeRelease(pEnum);
		}
	}

	SetRetH(hr);
	return NULL;
}

} // namespace dshow
} // namespace w32
#undef SetRetH

using namespace w32::dshow;

w32::CSyncMediaSource::~CSyncMediaSource()
{
	Destroy();
}

HRESULT STDMETHODCALLTYPE	w32::CSyncMediaSource::StreamTerminated()
{
	IsEndOfStreamReached = TRUE;
	return S_OK;
}

HRESULT w32::dshow::_DShowMedia::Run()
{
	ASSERT(pMediaControl);

	if(GetState() == RunState_Stopped)
		m_IncomingFrameCount = 0;

	HRESULT hr;
	hr = pMediaControl->Run();
	GetState();		// sync up

	return hr;
}

HRESULT w32::dshow::_DShowMedia::Stop()
{
	ASSERT(pMediaControl);

	HRESULT hr = pMediaControl->Stop();
	m_IncomingFrameCount = 0;
	GetState();		// sync up

	return hr;
}

HRESULT w32::dshow::_DShowMedia::Pause()
{
	ASSERT(pMediaControl);

	HRESULT hr = pMediaControl->Pause();
	GetState();		// sync up

	return hr;
}


DWORD w32::dshow::_DShowMedia::GetState()
{
	if(pMediaControl)
	{
		OAFilterState state;
		if(SUCCEEDED(pMediaControl->GetState(200,&state)))
		{
			switch(state)
			{
			case State_Stopped:
				return RunState_Stopped;
			case State_Paused:
				return RunState_Paused;
			case State_Running:
				return RunState_Running;
			default:
				ASSERT(0);
				return RunState_Blocking;
			}
		}
		else
			return RunState_Blocking;
	}
	else
		return RunState_Uninitialized;
}

HRESULT	w32::dshow::_DShowMedia::WaitForEnding(UINT time_out)
{
	if(pStreamEvent)
	{	long exitcode;
		HRESULT hr = pStreamEvent->WaitForCompletion(time_out,&exitcode);
		if(hr == E_ABORT)
			return S_FALSE;
		else
			return hr;
	}
	else return E_NOTIMPL;
}

HRESULT	w32::dshow::_DShowMedia::WaitForEnding_UI() // S_OK = ended, S_FALSE = time out
{
	HRESULT hr = S_OK;

	do
	{
		MSG	msgCur;

		while(::PeekMessage(&msgCur, NULL, NULL, NULL,TRUE))
		{	
			if(msgCur.message == WM_QUIT)
			{	
				::PostThreadMessage(GetCurrentThreadId(),WM_QUIT,0,0);
				return E_ABORT;
			}

			::TranslateMessage(&msgCur);
			::DispatchMessage(&msgCur);
		}

	}while( (hr=WaitForEnding(200)) == S_FALSE );

	return hr;
}

w32::dshow::_DShowMedia::_DShowMedia()
{
	pFilterGraph = NULL;
	pMediaControl = NULL;
	pMediaSeeking = NULL;
	pStreamEvent = NULL;
	pVideoWindow = NULL;
	pAudioVolume = NULL;

#ifdef _DEBUG
	_DebugRotCookie = 0;
#endif

}

IVideoWindow* w32::dshow::_DShowMedia::GetVideoWindow()
{
	if(IsInitialized())
	{
		if(!pVideoWindow)
		{
			IEnumFilters* pEnum = NULL;
			if(SUCCEEDED(pFilterGraph->EnumFilters(&pEnum)))
			{
				IBaseFilter* pFilter = NULL;
				while(SUCCEEDED(pEnum->Next(1,&pFilter,NULL)))
				{
					if(pFilter && SUCCEEDED(pFilter->QueryInterface(__uuidof(pVideoWindow),(LPVOID*)&pVideoWindow)))
						break;
					_SafeRelease(pFilter);
				}
				_SafeRelease(pFilter);
			}
			_SafeRelease(pEnum);
		}
	}

	return pVideoWindow;
}

IBasicAudio* w32::dshow::_DShowMedia::GetAudioVolume()
{
	if(IsInitialized())
	{
		if(!pAudioVolume)
		{
			IEnumFilters* pEnum = NULL;
			if(SUCCEEDED(pFilterGraph->EnumFilters(&pEnum)))
			{
				IBaseFilter* pFilter = NULL;
				while(SUCCEEDED(pEnum->Next(1,&pFilter,NULL)))
				{
					if(pFilter && SUCCEEDED(pFilter->QueryInterface(__uuidof(pAudioVolume),(LPVOID*)&pAudioVolume)))
						break;
					_SafeRelease(pFilter);
				}
				_SafeRelease(pFilter);
			}
			_SafeRelease(pEnum);
		}
	}

	return pAudioVolume;
}

w32::dshow::_DShowMedia::~_DShowMedia()
{
	Destroy();
}

void w32::dshow::_DShowMedia::Destroy()
{
	_SafeRelease(pVideoWindow);
	_SafeRelease(pAudioVolume);
	_SafeRelease(pMediaControl);
	_SafeRelease(pMediaSeeking);
	_SafeRelease(pStreamEvent);
	_SafeRelease(pFilterGraph);

#ifdef _DEBUG
	if(_DebugRotCookie)
	{
		RemoveObjectFromRot(_DebugRotCookie);
		_DebugRotCookie = 0;
	}
#endif

}

void w32::CSyncMediaSource::Destroy()
{
	m_MajorMediaType = MTEI_FORMAT_NONE;
	ReleaseAllObjects();
}

LONGLONG w32::CSyncMediaSource::GetDuration(UINT time_fmt)
{
	LONGLONG ret = -1;
	if(pMediaSeeking)
	{	
		GUID time_guid_org;

		static const GUID* time_guid[3] = 
		{	&TIME_FORMAT_FRAME,
			&TIME_FORMAT_SAMPLE,
			&TIME_FORMAT_MEDIA_TIME,
		};

		if(	SUCCEEDED(pMediaSeeking->GetDuration(&ret)) &&
			SUCCEEDED(pMediaSeeking->GetTimeFormat(&time_guid_org)))
		{
			if(time_fmt == time_guid_org.Data1){}
			else if(SUCCEEDED(pMediaSeeking->ConvertTimeFormat(&ret,time_guid[(time_fmt&0xf)/2],ret,&time_guid_org))){}
			else if(time_guid_org.Data1 == MTU_100NANO) 
			{
				if( time_fmt == MTU_FRAME && _video_info_nanosec_perframe )
				{
					ret /= _video_info_nanosec_perframe;
				}
				else if( time_fmt == MTU_SAMPLE && _audio_info_sample_persecond )
				{
					ret = _audio_info_sample_persecond*(ret/10000/1000);
				}
				else{ ret = -1; }
			}
			else{ ret = -1; }
		}
	}

	return ret;
}

HRESULT	w32::CSyncMediaSource::Rewind()
{
	if( pMediaSeeking )
	{HRESULT hr;
		LONGLONG pos = 0;
		hr = pMediaSeeking->SetPositions(	&pos,AM_SEEKING_AbsolutePositioning,
											NULL,AM_SEEKING_NoPositioning);
		if( SUCCEEDED(hr))
		{
			IsEndOfStreamReached = FALSE;
			m_IncomingFrameCount=0;
		}

		return hr;
	}

	return E_FAIL;
}


void w32::CSyncMediaSource::RewindIfEOF()
{
	if( IsEndOfStreamReached )
	{
		IsEndOfStreamReached = FALSE;

		Stop();
		Rewind();
	}
}

HRESULT STDMETHODCALLTYPE w32::CSyncMediaSource::CheckMediaType(LPC_AM_MEDIA_TYPE pMediaType)
{
	return m_MediaConstrain.CheckConstrain(pMediaType)?S_OK:E_FAIL;
}

void w32::CSyncMediaSource::ReleaseAllObjects()
{
	if(pFilterExtension)
		pFilterExtension->SetEventSink(NULL);

	_SafeRelease(pTerminator);
	_SafeRelease(pFilterExtension);
	_SafeRelease(pSourceFilter);

	__super::Destroy();
}

HRESULT w32::CSyncMediaSource::CreateLiveSource(LPCTSTR DeviceFriendlyName,BOOL StrictlyMatch)
{
	HRESULT hr = E_FAIL;

	IBaseFilter* pFilter = NULL;

	IEnumMoniker* pMonk = NULL;
	
	switch(m_MediaConstrain.iExtendInfoType)
	{
	case MTEI_FORMAT_VIDEO:
		pMonk = EnumFilters(CLSID_VideoInputDeviceCategory,&hr);
		break;
	case MTEI_FORMAT_AUDIO:
		pMonk = EnumFilters(CLSID_AudioInputDeviceCategory,&hr);
		break;
	default:
		ASSERT(0); //Set m_MediaConstrain before creation
	}

	if(pMonk)
	{
		pFilter = CreateFilter(pMonk,DeviceFriendlyName,StrictlyMatch,&hr);
		if(pFilter)
		{
			hr = Create(pFilter);
		}
	}
	
	_SafeRelease(pFilter);
	_SafeRelease(pMonk);

	return hr;
}

HRESULT w32::CSyncMediaSource::Create(IBaseFilter * pInSourceFilter,IBaseFilter *RenderFilter,IGraphBuilder * pGraph)
{
	static const LPCWSTR source_name = L"CamPack Media Source";
	clean_cache_info();

	ASSERT(pFilterGraph == NULL);
	ASSERT(pInSourceFilter);

	pSourceFilter = pInSourceFilter;
	pSourceFilter->AddRef();

	HRESULT hr;
	if(pGraph)
	{
		pFilterGraph = pGraph;
		pFilterGraph->AddRef();
		// check if pSourceFilter has been added or not
		if(!IsFilterAdded(pSourceFilter,pFilterGraph))
			hr = pFilterGraph->AddFilter(pSourceFilter,source_name);
		else
			hr = S_OK;
	}
	else
	{
		pFilterGraph = CreateFilterGraph(&hr);
		if(pFilterGraph)
			hr = pFilterGraph->AddFilter(pSourceFilter,source_name);
	}

	if(	SUCCEEDED(hr) &&
		SUCCEEDED(hr = _PostCreation(RenderFilter)) )
	{
		return S_OK;
	}
		
	ReleaseAllObjects();
    return hr;
}

HRESULT w32::CSyncMediaSource::CreateFileSource(LPCTSTR FileName)
{
	ASSERT(pFilterGraph == NULL);

	HRESULT hr;
	pFilterGraph = CreateFilterGraph(&hr);
	if(pFilterGraph)
	{
		pSourceFilter = AddFileSourceToFilterGraph(pFilterGraph,FileName,_T("CamPack Media Source"),&hr);

		if(pSourceFilter)
		{
			hr = _PostCreation();
			if(SUCCEEDED(hr))return S_OK;
		}
	}

	ReleaseAllObjects();
	return hr;
}

HRESULT w32::CSyncMediaSource::CreateAdditionalSource(CSyncMediaSource& main_source,IBaseFilter *RenderFilter)
{	
	ASSERT(main_source.IsInitialized());
	ASSERT(main_source.GetState() != RunState_Blocking && main_source.GetState() != RunState_Uninitialized);

	HRESULT hr = E_FAIL;
	IPin* pin = NULL;
	PIN_INFO	pi;
	pi.pFilter = NULL;

	if( SUCCEEDED(hr = main_source.Stop()) && 
		(pin = SearchPinEx(main_source.pFilterGraph,
							main_source.pSourceFilter,
							PT_Direction_Output,
							m_MediaConstrain.GetPinMediaTypeHint(),
							PT_Unconnected)) &&
		SUCCEEDED(hr = pin->QueryPinInfo(&pi)) &&
		SUCCEEDED(hr = Create(pi.pFilter,RenderFilter,main_source.pFilterGraph))
	)
	{
		hr = S_OK;
	}
		
	_SafeRelease(pi.pFilter);
	_SafeRelease(pin);

	return hr;
}

namespace w32
{
// The Terminator CLSID
static const GUID CLSID_Terminator = 
#ifndef _WIN64
{ 0x7aad4d76, 0x1c02, 0x42c5, { 0x92, 0x53, 0x14, 0x65, 0x55, 0x39, 0xbd, 0x57} };
#else
{ 0xb2420e95, 0x5260, 0x4bf1, { 0x99, 0x5b, 0xe2, 0xfc, 0xc8, 0xc8, 0xd7, 0x42} };
#endif
}


#define CheckAndAbort	if(FAILED(hr))goto _CreateFailed;
HRESULT w32::CSyncMediaSource::_PostCreation(IBaseFilter *RenderFilter)
{
	HRESULT hr;
	IBaseFilter* pRender = NULL;
	IBaseFilter* pTee = NULL;
	IPin* pDerivedSource = NULL;
	IPin* pTerminatorInput = NULL;
	AM_MEDIA_TYPE mt;
	mt.pbFormat = NULL;

	AddFilterGraphToRot();

	hr = pFilterGraph->QueryInterface(IID_IMediaControl,(void **)&pMediaControl);
	CheckAndAbort;

	///////////////////////////////////////////
	//Put all required filters
	pTerminator = CreateFilter(CLSID_Terminator,&hr);
	if(FAILED(hr))_CheckDump("CamPack DShow Filter is not installed. \n(Installation is in CodeLibLite\\1.0\\w32\\bin)\n");
	CheckAndAbort;
	ASSERT(pTerminator);

	hr = pTerminator->QueryInterface(__uuidof(ITerminatorExtension),(LPVOID*)&pFilterExtension);
	CheckAndAbort;
	hr = pFilterExtension->SetEventSink(this);
	CheckAndAbort;

	hr = pFilterGraph->AddFilter(pTerminator,L"CamPack Terminator");
	CheckAndAbort;

	// The Preview window
	// Do not add Tee in this stage
	if( bCreateWithPreview )
	{
		// The Render Filter
		if(RenderFilter)
		{
			pRender = RenderFilter;
			pRender->AddRef();
		}
		else
		{
			switch(m_MediaConstrain.iExtendInfoType)
			{
			case MTEI_FORMAT_VIDEO:
				pRender = CreateFilter(CLSID_VideoRenderer,&hr);
				break;
			case MTEI_FORMAT_AUDIO:
				pRender = CreateFilter(CLSID_AudioRender,&hr);
				break;
			default:
				ASSERT(0); //Set m_MediaConstrain before creation
			}
			CheckAndAbort;
		}
		ASSERT(pRender);

		ASSERT(pVideoWindow == NULL);
		pRender->QueryInterface(IID_IVideoWindow,(LPVOID*)&pVideoWindow);

		// The Tee
		pTee = CreateFilter(CLSID_InfTee,&hr);
		CheckAndAbort;
		ASSERT(pRender);
	}

	MediaType	TypeHint = m_MediaConstrain.GetMediaTypeHint();
	if(TypeHint == MT_NotAvailable)TypeHint = MT_DoNotCare;

	//Connect them directly with smart-connection
	if( pRender )
	{
		ASSERT(pTee);

		////////////////////////////////
		/// Connect source to Render firstly
		hr = ConnectFilters(pFilterGraph,pSourceFilter,pTerminator,TypeHint);
		CheckAndAbort;

		////////////////////////////////
		// then break it and get the peer of Terminator filter maybe 
		// not pSourceFilter when deal with a File source
		{
			LPPinsSummary	Pins = QueryPinInformation(pTerminator,&hr);
			CheckAndAbort;
			ASSERT(Pins);

			pTerminatorInput = Pins->GetPinByType(PT_Direction_Input,m_MediaConstrain.GetPinMediaTypeHint(),PT_Connected);
			FreePinsSummary(&Pins); 
			if(!pTerminatorInput){ return VFW_E_NOT_FOUND; };

			hr = pTerminatorInput->ConnectedTo(&pDerivedSource);
			CheckAndAbort;

			ASSERT(pDerivedSource);
			hr = pFilterGraph->Disconnect(pDerivedSource);
			CheckAndAbort;
			hr = pFilterGraph->Disconnect(pTerminatorInput);
			CheckAndAbort;
		}


		////////////////////////////////////
		// finially Connect the peer of render filter to Tee and Connect tee to render and terminator
		hr = pFilterGraph->AddFilter(pTee,L"Tee");
		CheckAndAbort;

		hr = ConnectPinToFilter(pFilterGraph,pDerivedSource,pTee,TypeHint);
		CheckAndAbort;
		_SafeRelease(pDerivedSource);

		hr = ConnectFilters(pFilterGraph,pTee,pTerminator,TypeHint);
		CheckAndAbort;

		hr = pFilterGraph->AddFilter(pRender,L"Render");
		CheckAndAbort;

		hr = ConnectFilters(pFilterGraph,pTee,pRender,TypeHint);
		CheckAndAbort;
	}
	else
	{
		hr = ConnectFilters(pFilterGraph,pSourceFilter,pTerminator,TypeHint);

		LPPinsSummary	Pins = QueryPinInformation(pTerminator,&hr);
		CheckAndAbort;
		ASSERT(Pins);

		pTerminatorInput = Pins->GetPinByType(PT_Direction_Input,m_MediaConstrain.GetPinMediaTypeHint(),PT_Connected);
		FreePinsSummary(&Pins); 
		if(!pTerminatorInput){ return VFW_E_NOT_FOUND; };
	}

	{	hr = pTerminatorInput->ConnectionMediaType(&mt);
		CheckAndAbort;

		hr = OnNewMediaFormat(&mt);
		CheckAndAbort;
	}

	_SafeRelease(pRender);
	_SafeRelease(pTee);
	_SafeRelease(pTerminatorInput);
	_SafeCoFree(mt.pbFormat);

	///////////////////////////////////////////
	// check to see if can seek
	pFilterGraph->QueryInterface(IID_IMediaSeeking,(LPVOID*)&pMediaSeeking);
	pFilterGraph->QueryInterface(IID_IMediaEvent,(LPVOID*)&pStreamEvent);

	return S_OK;

_CreateFailed:

	if(pRender)VERIFYH(pFilterGraph->RemoveFilter(pRender));
	if(pTee)VERIFYH(pFilterGraph->RemoveFilter(pTee));
	if(pSourceFilter)VERIFYH(pFilterGraph->RemoveFilter(pSourceFilter));

	_SafeRelease(pRender);
	_SafeRelease(pTee);
	_SafeRelease(pDerivedSource);
	_SafeRelease(pTerminatorInput);
	_SafeRelease(pVideoWindow);
	_SafeCoFree(mt.pbFormat);

	return hr;
}
#undef CheckAndAbort

void w32::CMediaTypeConstrain::SetAsRGB24VideoType(int cx,int cy,int fps)
{
	FieldMask = MTC_CARE_FIXEDFRAMESIZE| 
				MTC_CARE_MAJORTYPE |
				MTC_CARE_SUBTYPE |
				MTC_CARE_TYPEEXTENDINFO;

	bFixedFrameSize = TRUE;
	MajorType = MEDIATYPE_Video;
	SubType = MEDIASUBTYPE_RGB24;

	VideoInfo.BPP = 24;
	iExtendInfoType = MTEI_FORMAT_VIDEO;

	if( cx>0 || cy>0 )
	{
		ASSERT( cx>0 && cy>0 );
		FieldMask |= MTC_CARE_VIDEO_SIZE|MTC_CARE_VIDEO_BPP;
		VideoInfo.Width = cx;
		VideoInfo.Height = cy;
	}

	if( fps>0 )
	{
		FieldMask |= MTC_CARE_VIDEO_SIZE|MTC_CARE_VIDEO_FRAMERATE;
		VideoInfo.FrameRate = fps;
	}
}

void w32::CMediaTypeConstrain::SetAsRGB32VideoType(int cx,int cy,int fps)
{
	FieldMask = MTC_CARE_FIXEDFRAMESIZE| 
				MTC_CARE_MAJORTYPE |
				MTC_CARE_SUBTYPE |
				MTC_CARE_TYPEEXTENDINFO;

	bFixedFrameSize = TRUE;
	MajorType = MEDIATYPE_Video;
	SubType = MEDIASUBTYPE_RGB32;

	VideoInfo.BPP = 32;
	iExtendInfoType = MTEI_FORMAT_VIDEO;

	if( cx>0 || cy>0 )
	{
		ASSERT( cx>0 && cy>0 );
		FieldMask |= MTC_CARE_VIDEO_SIZE|MTC_CARE_VIDEO_BPP;
		VideoInfo.Width = cx;
		VideoInfo.Height = cy;
	}

	if( fps>0 )
	{
		FieldMask |= MTC_CARE_VIDEO_SIZE|MTC_CARE_VIDEO_FRAMERATE;
		VideoInfo.FrameRate = fps;
	}
}

void w32::CMediaTypeConstrain::SetAsYUV420VideoType(int cx,int cy,int fps)
{
	FieldMask = MTC_CARE_FIXEDFRAMESIZE| 
				MTC_CARE_MAJORTYPE |
				MTC_CARE_SUBTYPE;

	bFixedFrameSize = TRUE;
	MajorType = MEDIATYPE_Video;
	//SubType = MEDIASUBTYPE_IYUV;
	SubType = MEDIASUBTYPE_YV12;

	iExtendInfoType = MTEI_FORMAT_VIDEO;

	if( cx>0 || cy>0 )
	{
		ASSERT( cx>0 && cy>0 );
		FieldMask |= MTC_CARE_TYPEEXTENDINFO|MTC_CARE_VIDEO_SIZE;
		VideoInfo.Width = cx;
		VideoInfo.Height = cy;
	}

	if( fps>0 )
	{
		FieldMask |= MTC_CARE_TYPEEXTENDINFO|MTC_CARE_VIDEO_SIZE|MTC_CARE_VIDEO_FRAMERATE;
		VideoInfo.FrameRate = fps;
	}

}


void w32::CMediaTypeConstrain::SetAsPCMAudioType(int SampleBits,int chan,int samplerate)
{
	FieldMask = MTC_CARE_FIXEDFRAMESIZE| 
				MTC_CARE_MAJORTYPE |
				MTC_CARE_SUBTYPE;

	bFixedFrameSize = TRUE;
	MajorType = MEDIATYPE_Audio;
	SubType = MEDIASUBTYPE_PCM;

	iExtendInfoType = MTEI_FORMAT_AUDIO;

	if( SampleBits > 0)
	{
		FieldMask |= MTC_CARE_TYPEEXTENDINFO|MTC_CARE_WAVE_SAMPLEDEPTH;
		WaveformInfo.SampleDepth = SampleBits;
	}

	if( chan>0 )
	{
		FieldMask |= MTC_CARE_TYPEEXTENDINFO|MTC_CARE_WAVE_CHANNELS;
		WaveformInfo.Channels = chan;
	}

	if( samplerate>0 )
	{
		FieldMask |= MTC_CARE_TYPEEXTENDINFO|MTC_CARE_WAVE_SAMPLERATE;
		WaveformInfo.SampleRate = samplerate;
	}
}

w32::MediaType	w32::CMediaTypeConstrain::GetMediaTypeHint()
{
	if( FieldMask&MTC_CARE_MAJORTYPE )
	{
		if(MajorType == MEDIATYPE_Video)
		{
			return MT_Video;
		}
		else if(MajorType == MEDIATYPE_Audio)
		{
			return MT_Audio;
		}
		else if(MajorType == MEDIATYPE_Text)
		{
			return MT_Text;
		}
		else if(MajorType == MEDIATYPE_Stream)
		{
			return MT_Byte;
		}
		else if(MajorType == MEDIATYPE_Timecode)
		{
			return MT_Timecode;
		}
		else
		{
			return MT_Unknown;
		}
	}
	else
		return MT_NotAvailable;
}

w32::PinType w32::CMediaTypeConstrain::GetPinMediaTypeHint()
{
	if( FieldMask&MTC_CARE_MAJORTYPE )
	{
		if(MajorType == MEDIATYPE_Video)
		{
			return PT_Media_AnyVideo;
		}
		else if(MajorType == MEDIATYPE_Audio)
		{
			return PT_Media_AnyAudio;
		}
		else if(MajorType == MEDIATYPE_Text)
		{
			return PT_Media_Text;
		}
		else if(MajorType == MEDIATYPE_Stream)
		{
			return PT_Media_CompressedByte;
		}
		else if(MajorType == MEDIATYPE_Timecode)
		{
			return PT_Media_Timecode;
		}
	}

	return PT_DoNotCare;
}

w32::CMediaTypeConstrain::CMediaTypeConstrain()
{
	ZeroMemory(this,sizeof(CMediaTypeConstrain));
}

BOOL w32::CMediaTypeConstrain::CheckConstrain(LPC_AM_MEDIA_TYPE	pMediaType)
{
	ASSERT(pMediaType);

	////// check all cared field
	if (FieldMask&MTC_CARE_FIXEDFRAMESIZE)
	{
		if( bFixedFrameSize==pMediaType->bFixedSizeSamples ){}
		else{ return FALSE; }
	}
	if (FieldMask&MTC_CARE_INTERFRAMECOMPRESSION)
	{
		if( bInterframeCompression==pMediaType->bTemporalCompression ){}
		else{ return FALSE; }
	}
	if (FieldMask&MTC_CARE_FRAMESIZE)				
	{
		if( iFrameSize==pMediaType->lSampleSize ){}
		else{ return FALSE; }
	}
	if (FieldMask&MTC_CARE_MAJORTYPE)
	{
		if( MajorType==pMediaType->majortype ){}
		else{ return FALSE; }
	}
	if (FieldMask&MTC_CARE_SUBTYPE)
	{
		if( SubType==pMediaType->subtype ){}
		else{ return FALSE; }
	}

	if (FieldMask&MTC_CARE_TYPEEXTENDINFO)
	{
		switch(iExtendInfoType)
		{
		case MTEI_FORMAT_NONE:
			if( pMediaType->formattype != FORMAT_None && 
				pMediaType->formattype != GUID_NULL )
			{
				return FALSE;
			}
			break;
		case MTEI_FORMAT_VIDEO:
			{
				VIDEOINFOHEADER*	pvi = NULL;
				BITMAPINFOHEADER*	pbi = NULL;
				if( pMediaType->formattype == FORMAT_VideoInfo )
				{
					pvi = (VIDEOINFOHEADER*)pMediaType->pbFormat;
					pbi = &pvi->bmiHeader;
				}
				else if( pMediaType->formattype == FORMAT_VideoInfo2 )
				{
					pvi = (VIDEOINFOHEADER*)pMediaType->pbFormat;
					pbi = &((VIDEOINFOHEADER2*)pvi)->bmiHeader;
				}
				else
				{
					return FALSE;
				}

				// size consistence
				if(SubType == MEDIASUBTYPE_RGB24)
				{
					if(pMediaType->lSampleSize != DIBSIZE(*pbi))
						return FALSE;
				}

				//check extend info 
				if(FieldMask&MTC_CARE_VIDEO_SIZE)
				{
					if( pbi->biWidth!=VideoInfo.Width || pbi->biHeight!=VideoInfo.Height )
						return FALSE;
				}

				if(FieldMask&MTC_CARE_VIDEO_BPP)
				{
					if( VideoInfo.BPP!=pbi->biBitCount )return FALSE;
				}

				if(FieldMask&MTC_CARE_VIDEO_FRAMERATE)
				{
					double fps = 1e7/pvi->AvgTimePerFrame;
					if( fabs(fps - VideoInfo.FrameRate) > 1.6 )return FALSE;
				}
			}

			break;
		case MTEI_FORMAT_AUDIO:
			if( pMediaType->formattype == FORMAT_WaveFormatEx)
			{
				WAVEFORMATEX * pwi = (WAVEFORMATEX *)pMediaType->pbFormat;

				//check extend info
				if(FieldMask&MTC_CARE_WAVE_CHANNELS)
				{
					if(WaveformInfo.Channels!=pwi->nChannels)return FALSE;
				}

				if(FieldMask&MTC_CARE_WAVE_SAMPLERATE)
				{
					if(WaveformInfo.SampleRate!=pwi->nSamplesPerSec)return FALSE;
				}

				if(FieldMask&MTC_CARE_WAVE_SAMPLEDEPTH)
				{
					if(WaveformInfo.SampleDepth!=pwi->wBitsPerSample)return FALSE;
				}
			}
			else
				return FALSE;

			break;
		default:
			ASSERT(0);
		}
	}

	return TRUE;
}


w32::CSyncMediaSource::CSyncMediaSource()
{
	pSourceFilter = NULL;
	pTerminator = NULL;
	pFilterExtension  = NULL;

	IsEndOfStreamReached = FALSE;
	bCreateWithPreview = FALSE;
	m_MajorMediaType = MTEI_FORMAT_NONE;

	clean_cache_info();
	m_MediaBuffer = NULL;
	m_IncomingFrameCount = 0;
}

HRESULT STDMETHODCALLTYPE w32::CSyncMediaSource::IncomingFrame()
{
	if(m_MediaBuffer)
	{
		//printf("[Frame %5d]: %d bytes\r",m_IncomingFrameCount,m_MediaBuffer->MediaDataSize);
		OnIncomingFrame();
		m_IncomingFrameCount++;
		return S_OK;
	}
	else
		return S_FALSE;
}

void w32::CSyncMediaSource::clean_cache_info()
{
	_video_info_nanosec_perframe = 
	_video_info_width = 
	_video_info_height =
	_video_info_bpp = 0;
}

HRESULT	w32::CSyncMediaSource::OnNewMediaFormat(const AM_MEDIA_TYPE* pMediaType)
{
	m_IncomingFrameCount = 0;
	m_MediaBuffer = NULL;

	HRESULT ret = E_FAIL;

	//check the media type
	if( pMediaType->majortype == MEDIATYPE_Video
		//&& 
		//(
		//	pMediaType->subtype ==  MEDIASUBTYPE_RGB8	||
		//	pMediaType->subtype ==  MEDIASUBTYPE_RGB565 ||
		//	pMediaType->subtype ==  MEDIASUBTYPE_RGB555 ||
		//	pMediaType->subtype ==  MEDIASUBTYPE_RGB24	||
		//	pMediaType->subtype ==  MEDIASUBTYPE_RGB32	||
		//	pMediaType->subtype ==  MEDIASUBTYPE_ARGB32 
		//)
	)
	{
		m_MajorMediaType = MTEI_FORMAT_VIDEO;

		BITMAPINFOHEADER * pbi = NULL;
		REFERENCE_TIME nanosec_per_frame;
		if( pMediaType->formattype == FORMAT_VideoInfo )
		{
			pbi = &(( VIDEOINFOHEADER* )pMediaType->pbFormat)->bmiHeader;
			nanosec_per_frame = (( VIDEOINFOHEADER* )pMediaType->pbFormat)->AvgTimePerFrame;
		}
		else if( pMediaType->formattype == FORMAT_VideoInfo2 )
		{
			pbi = &(( VIDEOINFOHEADER2* )pMediaType->pbFormat)->bmiHeader;
			nanosec_per_frame = (( VIDEOINFOHEADER2* )pMediaType->pbFormat)->AvgTimePerFrame;
		}
		else
		{
			return E_INVALIDARG;
		}

		_video_info_width = pbi->biWidth;
		_video_info_height= pbi->biHeight;
		_video_info_bpp   = pbi->biBitCount;
		_video_info_nanosec_perframe = nanosec_per_frame;

		double fps = nanosec_per_frame?((int)(10000000L/nanosec_per_frame)):0;
		VERIFYH(pFilterExtension->GetMediaFrameBuffer(&m_MediaBuffer));

		_CheckDump(	"Incoming Video stream: "<<_video_info_width<<_T('x')<<_video_info_height<<_T(' ')<<
					_video_info_bpp<<"BPP "<<((float)fps)<<"Hz\n");

		ret = OnNewVideoFormat(pbi->biWidth,pbi->biHeight,pbi->biBitCount,fps);
		if(SUCCEEDED(ret))
		{
			return S_OK;
		}
	}
	else if( pMediaType->majortype == MEDIATYPE_Audio
			 // && pMediaType->subtype == MEDIASUBTYPE_PCM 
		 )
	{	
		m_MajorMediaType = MTEI_FORMAT_AUDIO;

		if( pMediaType->formattype == FORMAT_WaveFormatEx )
		{
			VERIFYH(pFilterExtension->GetMediaFrameBuffer(&m_MediaBuffer));

			WAVEFORMATEX * pwf = (WAVEFORMATEX*)pMediaType->pbFormat;
			if(WAVE_FORMAT_PCM!=pwf->wFormatTag)return E_INVALIDARG;

			_audio_info_channel = pwf->nChannels;
			_audio_info_bps = pwf->wBitsPerSample;
			_audio_info_sample_persecond = pwf->nSamplesPerSec;

			_CheckDump("Incoming Audio stream: "<<_audio_info_bps<<"BPS x"<<_audio_info_channel<<
						_T(' ')<<_audio_info_sample_persecond<<"Hz\n");
			ret = OnNewAudioFormat(pwf->nChannels,pwf->wBitsPerSample,pwf->nSamplesPerSec);
			if(SUCCEEDED(ret))
			{
				return S_OK;
			}
		}
		else{ return E_INVALIDARG; }
	}
	else
	{
		m_MajorMediaType = MTEI_FORMAT_NONE;
		return E_NOTIMPL; 
	}

	return ret;
}

w32::CAsyncMediaSource::CAsyncMediaSource()
{
	ZeroMemory(&m_FrameBuffers,sizeof(m_FrameBuffers));
	m_IsMediaStreamMounted = FALSE;
	m_HandledFrameCount = 0;
}

w32::CAsyncMediaSource::~CAsyncMediaSource()
{
	_SafeDelArray(m_FrameBuffers[0].Media.pMediaData);
	_SafeDelArray(m_FrameBuffers[1].Media.pMediaData);
	_SafeDelArray(m_FrameBuffers[2].Media.pMediaData);
}

void w32::CAsyncMediaSource::ClearBufferTimestamps()
{ 
	m_HandledFrameCount = 0;

	EnterCCSBlock(syncOutputBufferInuse);
	EnterCCSBlock(syncBufferSwapping);

	m_FrameBuffers[0].Media.TimeStart =  
	m_FrameBuffers[1].Media.TimeStart =  
	m_FrameBuffers[2].Media.TimeStart =  LLONG_MIN;
}

HRESULT w32::CAsyncMediaSource::OnNewVideoFormat(int Width,int Height,int BPP,double Fps)
{
	ClearBufferTimestamps();

	EnterCCSBlock(syncOutputBufferInuse);
	EnterCCSBlock(syncBufferSwapping);

	m_FrameBuffers[0].MediaType = MTEI_FORMAT_VIDEO;
	m_FrameBuffers[0].Video.BPP = BPP;
	m_FrameBuffers[0].Video.Width = Width;
	m_FrameBuffers[0].Video.Height = Height;
	m_FrameBuffers[1].MediaType = MTEI_FORMAT_VIDEO;
	m_FrameBuffers[1].Video.BPP = BPP;
	m_FrameBuffers[1].Video.Width = Width;
	m_FrameBuffers[1].Video.Height = Height;
	m_FrameBuffers[2].MediaType = MTEI_FORMAT_VIDEO;
	m_FrameBuffers[2].Video.BPP = BPP;
	m_FrameBuffers[2].Video.Width = Width;
	m_FrameBuffers[2].Video.Height = Height;

	m_FrameBuffers[0].Media.TimeStart = 
	m_FrameBuffers[1].Media.TimeStart = 
	m_FrameBuffers[2].Media.TimeStart = LLONG_MIN;

	m_pOutputBuffer = &m_FrameBuffers[0];
	m_pReadyBuffer = &m_FrameBuffers[1];
	m_pBackBuffer = &m_FrameBuffers[2];

	m_IsMediaStreamMounted = TRUE;

	return S_OK;
}

HRESULT w32::CAsyncMediaSource::OnNewAudioFormat(int nChannel,int BPS,DWORD SamPerSec_Hz)
{
	ClearBufferTimestamps();

	EnterCCSBlock(syncOutputBufferInuse);
	EnterCCSBlock(syncBufferSwapping);

	m_FrameBuffers[0].MediaType = MTEI_FORMAT_AUDIO;
	m_FrameBuffers[0].Audio.Channel = nChannel;
	m_FrameBuffers[0].Audio.BitsPerSample = BPS;
	m_FrameBuffers[1].MediaType = MTEI_FORMAT_AUDIO;
	m_FrameBuffers[1].Audio.Channel = nChannel;
	m_FrameBuffers[1].Audio.BitsPerSample = BPS;
	m_FrameBuffers[2].MediaType = MTEI_FORMAT_AUDIO;
	m_FrameBuffers[2].Audio.Channel = nChannel;
	m_FrameBuffers[2].Audio.BitsPerSample = BPS;

	m_FrameBuffers[0].Media.TimeStart = 
	m_FrameBuffers[1].Media.TimeStart = 
	m_FrameBuffers[2].Media.TimeStart = LLONG_MIN;

	m_pOutputBuffer = &m_FrameBuffers[0];
	m_pReadyBuffer = &m_FrameBuffers[1];
	m_pBackBuffer = &m_FrameBuffers[2];

	m_IsMediaStreamMounted = TRUE;

	return S_OK;
}

void w32::CAsyncMediaSource::OnIncomingFrame()
{
	if(m_IsMediaStreamMounted)
	{
		//// Try to update output buffer
		if( syncOutputBufferInuse.TryLock() )
		{
			if( m_pOutputBuffer->Media.TimeStart < m_pReadyBuffer->Media.TimeStart )
			{
				syncBufferSwapping.Lock();
					rt::Swap(m_pOutputBuffer,m_pReadyBuffer);
				syncBufferSwapping.Unlock();

				syncOutputBufferInuse.Unlock();
				syncFrameBufferRenewed.Set();
			}
			else
			{
				syncOutputBufferInuse.Unlock();
			}
		}

		//// Update back buffer and it is the latest frame
		if( m_pBackBuffer->Media.MediaDataSize < m_MediaBuffer->MediaDataSize )
		{
			_SafeDelArray(m_pBackBuffer->Media.pMediaData);
			m_pBackBuffer->Media.pMediaData = new BYTE[m_MediaBuffer->MediaDataSize];
			m_pBackBuffer->Media.MediaDataSize = m_MediaBuffer->MediaDataSize;
			_CheckDump("Back Buffer enlarged to "<<m_MediaBuffer->MediaDataSize<<"bytes\n");
		}
		memcpy(	m_pBackBuffer->Media.pMediaData,
				m_MediaBuffer->pMediaData,
				m_MediaBuffer->MediaDataSize);
		m_pBackBuffer->Media.TimeStart = m_MediaBuffer->TimeStart;
		m_pBackBuffer->Media.TimeEnd = m_MediaBuffer->TimeEnd;


		//// Try to update output buffer
		if( syncOutputBufferInuse.TryLock() )
		{
			//ASSERT(m_pBackBuffer->Media.TimeStamp >= m_pOutputBuffer->Media.TimeStamp);

			//// the back buffer is sure to be the latest , put it to front buffer
			syncBufferSwapping.Lock();
				rt::Swap(m_pOutputBuffer,m_pBackBuffer);
			syncBufferSwapping.Unlock();

			syncOutputBufferInuse.Unlock();
			syncFrameBufferRenewed.Set();
		}
		else
		{
			//// Still occupied Put to back buffer
			SwapReadyBackBuffer();
		}
	}
}

w32::dshow::LPCMediaFrameEx w32::CAsyncMediaSource::ProcessNewFrame()
{
	// Otherwise may cause blocking
#ifdef _DEBUG
	if( !IsEndOfStreamReached )ASSERT( GetState() == RunState_Running );
#endif

	if( !IsEndOfStreamReached )
	{
		if(syncFrameBufferRenewed.WaitSignal())
		{
			syncFrameBufferRenewed.Reset();

			syncOutputBufferInuse.Lock();
			m_HandledFrameCount++;
			return m_pOutputBuffer;
		}
	}
	else
	{
		if(syncFrameBufferRenewed.IsSignaled())
		{
			syncFrameBufferRenewed.Reset();

			syncOutputBufferInuse.Lock();
			m_HandledFrameCount++;
			return m_pOutputBuffer;
		}
	}

	return NULL;
}

w32::dshow::LPCVideoFrame w32::CAsyncMediaSource::ProcessNewVideoFrame()
{ 
	return  (GetMajorMediaType()==MTEI_FORMAT_VIDEO)?
			(&ProcessNewFrame()->Video):NULL;
}

w32::dshow::LPCAudioFrame w32::CAsyncMediaSource::ProcessNewAudioFrame()
{ 
	return  (GetMajorMediaType()==MTEI_FORMAT_AUDIO)?
			(&ProcessNewFrame()->Audio):NULL;
}


void w32::CAsyncMediaSource::ProcessFinished()
{
	if( syncBufferSwapping.TryLock() )
	{
		if( m_pOutputBuffer->Media.TimeStart < m_pReadyBuffer->Media.TimeStart )
		{
			LPMediaFrameEx tmp;
			tmp = m_pOutputBuffer;
			m_pOutputBuffer = m_pReadyBuffer;
			m_pReadyBuffer = tmp;

			syncFrameBufferRenewed.Set();
		}

		syncBufferSwapping.Unlock();
	}
	
	syncOutputBufferInuse.Unlock();
}

HRESULT w32::CAsyncMediaSource::Run()
{
	if( GetState() == RunState_Stopped)
	{
		ClearBufferTimestamps();
	}

	return __super::Run();
}

HRESULT	w32::CAsyncMediaSource::Rewind()
{
	HRESULT hr = __super::Rewind();
	if( SUCCEEDED(hr) )ClearBufferTimestamps();

	return hr;
}

HRESULT	w32::CAsyncMediaSource::Stop()
{
	HRESULT hr;
	hr = __super::Stop();
	if( SUCCEEDED(hr) )m_HandledFrameCount = 0;

	return hr;
}

namespace w32
{

// The Terminator CLSID
static const GUID CLSID_Source = 
#ifndef _WIN64
{ 0x86860f32, 0x8ece, 0x47a1, { 0x9d, 0x61, 0x25, 0xfd, 0xc9, 0x14, 0xbc, 0xfa} };
#else
{ 0xd8cdb87f, 0x54bc, 0x4d4b, { 0xb7, 0x2a, 0x51, 0xcf, 0x30, 0x3f, 0x1b, 0x7d} };
#endif
}

HRESULT w32::CSyncMediaRender::Create(LPC_AM_MEDIA_TYPE pMediaType)
{
	ASSERT(!IsInitialized());

	IPin * out = NULL;

	HRESULT hr = E_FAIL;

	if(	(pSource = dshow::CreateFilter(CLSID_Source,&hr)) &&
		SUCCEEDED(hr = pSource->QueryInterface(__uuidof(pExtension),(LPVOID*)&pExtension)) &&
		SUCCEEDED(hr = pExtension->SetMediaType(pMediaType)) &&
		(pFilterGraph = CreateFilterGraph(&hr)) &&
		SUCCEEDED(hr = pFilterGraph->QueryInterface(__uuidof(pMediaControl),(LPVOID*)&pMediaControl)) &&
		SUCCEEDED(hr = pFilterGraph->QueryInterface(__uuidof(pMediaSeeking),(LPVOID*)&pMediaSeeking)) &&
		SUCCEEDED(hr = pFilterGraph->QueryInterface(__uuidof(pStreamEvent),(LPVOID*)&pStreamEvent)) &&
		SUCCEEDED(hr = pFilterGraph->AddFilter(pSource,L"Campack Source Filter")) &&
		(out = dshow::SearchPin(pSource,PT_Direction_Output,PT_DoNotCare,PT_Unconnected)) &&
		SUCCEEDED(hr = pFilterGraph->Render(out)) &&
		SUCCEEDED(hr = pExtension->GetMediaFrameBuffer(&pMediaFrame)) &&
		SUCCEEDED(hr = pExtension->SetEventSink(this))
	)
	{
		AddFilterGraphToRot();
		hr = S_OK;
		m_IncomingFrameCount=0;

		pMediaFrame->TimeStart = 0;
		pMediaFrame->TimeEnd = 0;
	}
	else
	{
		Destroy();
	}

	_SafeRelease(out);
	return hr;
}


HRESULT w32::CSyncMediaRender::CreateForRGBVideoType(int cx,int cy,double fps)
{
	AM_MEDIA_TYPE mt;

	VIDEOINFOHEADER   vinfo;
	ZeroMemory(&vinfo,sizeof(vinfo));

	vinfo.AvgTimePerFrame = m_TimePerFrame = (LONGLONG)((1000/fps) * 10000.0 + 0.5);
	//vinfo.dwBitErrorRate = 0;
	//vinfo.dwBitRate = 0;
	//RECT reg = {0,0,0,0};
	//vinfo.rcSource = reg;
	//vinfo.rcTarget = reg;

	vinfo.bmiHeader.biSize = sizeof(BITMAPINFOHEADER);
	vinfo.bmiHeader.biWidth = cx;
	vinfo.bmiHeader.biHeight = cy;
	vinfo.bmiHeader.biPlanes = 1;
	vinfo.bmiHeader.biBitCount = 24;
	vinfo.bmiHeader.biCompression = BI_RGB;
	vinfo.bmiHeader.biSizeImage = DIBSIZE(vinfo.bmiHeader);
	//vinfo.bmiHeader.biXPelsPerMeter = 0;
	//vinfo.bmiHeader.biYPelsPerMeter = 0;
	//vinfo.bmiHeader.biClrUsed = 0;
	//vinfo.bmiHeader.biClrImportant = 0;

    mt.majortype = MEDIATYPE_Video;
	mt.subtype = MEDIASUBTYPE_RGB24;
	mt.bFixedSizeSamples = TRUE;
    mt.bTemporalCompression = FALSE;
    mt.lSampleSize = vinfo.bmiHeader.biSizeImage;
    mt.formattype = FORMAT_VideoInfo;
    mt.pUnk = NULL;
    mt.cbFormat = sizeof(VIDEOINFOHEADER);
    mt.pbFormat = (LPBYTE)&vinfo;

	return Create(&mt);
}

HRESULT w32::CSyncMediaRender::CreateForYUV420VideoType(int cx,int cy,double fps)
{
	AM_MEDIA_TYPE mt;

	VIDEOINFOHEADER   vinfo;
	ZeroMemory(&vinfo,sizeof(vinfo));

	vinfo.AvgTimePerFrame = m_TimePerFrame = (LONGLONG)((1000/fps) * 10000.0 + 0.5);
	//vinfo.dwBitErrorRate = 0;
	//vinfo.dwBitRate = 0;
	//RECT reg = {0,0,0,0};
	//vinfo.rcSource = reg;
	//vinfo.rcTarget = reg;

	vinfo.bmiHeader.biSize = sizeof(BITMAPINFOHEADER);
	vinfo.bmiHeader.biCompression = mmioFOURCC('Y','V','1','2');
	vinfo.bmiHeader.biWidth = cx;
	vinfo.bmiHeader.biHeight = cy;
	vinfo.bmiHeader.biPlanes = 1;
	vinfo.bmiHeader.biBitCount = 12;
	vinfo.bmiHeader.biSizeImage = DIBSIZE(vinfo.bmiHeader);
	//vinfo.bmiHeader.biXPelsPerMeter = 0;
	//vinfo.bmiHeader.biYPelsPerMeter = 0;
	//vinfo.bmiHeader.biClrUsed = 0;
	//vinfo.bmiHeader.biClrImportant = 0;

    mt.majortype = MEDIATYPE_Video;
	mt.subtype = MEDIASUBTYPE_YV12;
	mt.bFixedSizeSamples = TRUE;
    mt.bTemporalCompression = FALSE;
    mt.lSampleSize = vinfo.bmiHeader.biSizeImage;
    mt.formattype = FORMAT_VideoInfo;
    mt.pUnk = NULL;
    mt.cbFormat = sizeof(VIDEOINFOHEADER);
    mt.pbFormat = (LPBYTE)&vinfo;


	return Create(&mt);
}

HRESULT w32::CSyncMediaRender::CreateForPCMAudioType(int chan,int SampleBits,int samplerate, UINT frame_size)
{
	ASSERT(frame_size);

	AM_MEDIA_TYPE mt;

	WAVEFORMATEX   winfo;

	winfo.cbSize = sizeof(WAVEFORMATEX);
	winfo.nAvgBytesPerSec = (chan*SampleBits)/8*samplerate;
	winfo.nBlockAlign = 4;
	winfo.nChannels = chan;
	winfo.nSamplesPerSec = samplerate;
	winfo.wBitsPerSample = SampleBits;
	winfo.wFormatTag = WAVE_FORMAT_PCM;

	frame_size = (frame_size + 3)&0xffffffffc;
	frame_size = frame_size*8/chan/SampleBits*(chan*SampleBits)/8;

	m_TimePerFrame = ((__int64)(frame_size*8/chan/SampleBits))*10000000/samplerate;

	mt.majortype = MEDIATYPE_Audio;
	mt.subtype = MEDIASUBTYPE_PCM;
	mt.bFixedSizeSamples = TRUE;
    mt.bTemporalCompression = FALSE;
    mt.lSampleSize = frame_size;
    mt.formattype = FORMAT_WaveFormatEx;
    mt.pUnk = NULL;
    mt.cbFormat = sizeof(WAVEFORMATEX);
    mt.pbFormat = (LPBYTE)&winfo;


	return Create(&mt);
}


void w32::CSyncMediaRender::Destroy()
{
	if(IsInitialized())
	{	if(pExtension)pExtension->SetEventSink(NULL);
		Stop();
	}

	_SafeRelease(pExtension);
	_SafeRelease(pSource);

	__super::Destroy();

	pMediaFrame = NULL;
}

w32::CSyncMediaRender::CSyncMediaRender()
{
	pExtension = NULL;
	pSource = NULL;
	pMediaFrame = NULL;
	m_TimePerFrame = 0;
}

void w32::CSyncMediaRender::AdvanceMediaTime()
{
	if(pMediaFrame)
	{
		pMediaFrame->TimeStart = m_TimePerFrame*m_IncomingFrameCount;
		pMediaFrame->TimeEnd = m_TimePerFrame + m_TimePerFrame*m_IncomingFrameCount;
		m_IncomingFrameCount++;
	}
}

HRESULT STDMETHODCALLTYPE w32::CSyncMediaRender::FillFrame()
{
	if(pMediaFrame)
	{
		LPWORD p = (LPWORD)pMediaFrame->pMediaData;
		LPWORD end = p + (pMediaFrame->MediaDataSize/sizeof(WORD));
		for(;p<end;p++)
			*p = rand();

		AdvanceMediaTime();
		printf("T=%8d\tS=%d\n",(int)(pMediaFrame->TimeStart/10000),pMediaFrame->MediaDataSize);
	}

	return S_OK;
}

w32::CSyncMediaRender::~CSyncMediaRender()
{
	Destroy();
}


w32::CAsyncMediaRender::CAsyncMediaRender()
{
	m_FrameDataSize = 0;
	m_bBackUpdated = FALSE;
	m_pBackFrame = m_pFrontFrame = NULL;
}

HRESULT w32::CAsyncMediaRender::Create(LPC_AM_MEDIA_TYPE pMediaType)
{
	HRESULT hr = __super::Create(pMediaType);
	if(SUCCEEDED(hr))
	{
		if(SetupBuffers(pMediaType->lSampleSize))
			return S_OK;
		else
		{
			hr = E_OUTOFMEMORY;
			Destroy();
		}
	}

	return hr;
}

BOOL w32::CAsyncMediaRender::SetupBuffers(UINT sz)
{
	if(!m_FrameData.SetSize(sz*2))return FALSE;

	m_pFrontFrame= m_FrameData;
	m_pBackFrame = &m_FrameData[sz];
	m_FrameDataSize = sz;

	return TRUE;
}

HRESULT STDMETHODCALLTYPE w32::CAsyncMediaRender::FillFrame()
{
	if(pMediaFrame)
	{
		ASSERT(m_FrameDataSize == pMediaFrame->MediaDataSize);

		AdvanceMediaTime();

		if(m_FrameSwitchCCS.TryLock())
		{
			if(m_bBackUpdated)
			{	rt::Swap(m_pFrontFrame,m_pBackFrame);
				m_bBackUpdated = FALSE;
			}
			m_FrameSwitchCCS.Unlock();
		}

#ifdef W32_DSHOW_USE_IPP
		ipp::memcpy32AL(pMediaFrame->pMediaData,m_pFrontFrame,m_FrameDataSize);
		//IPPCALL(ippsCopy_32s)((Ipp32s*)m_pFrontFrame,(Ipp32s*)pMediaFrame->pMediaData,m_FrameDataSize/4);
#else
		memcpy(pMediaFrame->pMediaData,m_pFrontFrame,m_FrameDataSize);
#endif
	}

	return S_OK;
}

void w32::CAsyncMediaRender::AppendFrame(LPCVOID pData)
{
	ASSERT(IsInitialized());
	ASSERT_ARRAY(((LPBYTE)pData),m_FrameDataSize);

	EnterCCSBlock(m_FrameSwitchCCS);
#ifdef W32_DSHOW_USE_IPP
		ipp::memcpy32AL(m_pBackFrame,pData,m_FrameDataSize);
#else
		memcpy(m_pBackFrame,pData,m_FrameDataSize);
#endif
	m_bBackUpdated = TRUE;
}

void w32::CAsyncMediaRender::Destroy()
{
	if(IsInitialized())
		Stop();

	m_FrameData.SetSize();
	m_FrameDataSize = 0;
	m_bBackUpdated = FALSE;
	m_pBackFrame = m_pFrontFrame = NULL;

	__super::Destroy();
}
