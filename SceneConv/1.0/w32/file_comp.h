#pragma once

//////////////////////////////////////////////////////////////////////
// Visual Computing Foundation Classes (VCFC)
//					Jiaping Wang  2006.2
//					e_boris2002@hotmail.com
//
// (C) Copyright Jiaping Wang 2004.
//
// Permission to copy, use, modify, sell and distribute this software
// is granted provided this copyright notice appears in all copies.
// This software is provided "as is" without express or implied
// warranty, and with no claim as to its suitability for any purpose.
//
// Absolutely no warranty!!
//////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////
//  Brief Description
//
//  file_comp.h
//
//  Composed File for persistence of classes
//  member visitor for loading/saving based on composed file
//
//////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////
// Revise History
//
// Inital version			2006.6.9		Jiaping
// handle massive textout	2006.11.30		Jiaping
//
//////////////////////////////////////////////////////////////////////

#include "win32_ver.h"
#include "file_64.h"
#include "..\rt\member_visitor.h"
#include <map>
#include <string>

#define __PersistDump(x) _CheckDump(x)

////////////////////////////////////////////////////////////
// Helper macro for On-Demand loading stuffs
#define _MEMBER_LOAD_IGNORE_ONDEMAND(x)	{ w32::CFile64_Composition::m_OnDemandLoadingObjectList.RemoveObject(&x); }
#define _MEMBER_LOAD_ONDEMAND(x)		{ w32::CFile64_Composition::LoadObjectOnDemand(&x); }

#define _MEMBER_ENTRY_ONDEMAND_BEGIN()								\
		if(_MEMBER_IS_VISITING_BY(rt::_MemberVisitor::MV_Load))		\
		{ flag = rt::_MemberVisitor::_Load::MV_LOAD_ON_DEMAND; }	\

#define _MEMBER_ENTRY_ONDEMAND_END()	{ flag=0; }
////////////////////////////////////////////////////////////


namespace w32
{

namespace _meta_
{
	__forceinline LPCTSTR _text_left(){ return _T(" ("); }
	__forceinline LPCTSTR _text_right_return(){ return _T(")\n"); }
	__forceinline LPCTSTR _text_composed(){ return _T("Composed"); }
	__forceinline LPCTSTR _text_not_exist(){ return _T(", not existed.\n"); }
}

class CFile64_Composition:public CFile64
{
	__forceinline static LPCTSTR _text_load_ondemand(){ return _T("On-demand loading detected: \n  File: "); }
	__forceinline static LPCTSTR _text_return_sp(){ return _T("\n  "); }
	__forceinline static LPCTSTR _text_section(){ return _T("Section: "); }
	__forceinline static LPCTSTR _text_left(){ return _T(" ("); }
	__forceinline static LPCTSTR _text_fail_ondemand(){ return _T("\nERROR: Failed to load on-demand.\n\7"); }

protected:

#ifdef _UNICODE
	typedef std::wstring	stdstring;
#else
	typedef std::string		stdstring;
#endif

public:
	struct _AutoBool
	{	BOOL m_bValue; 
		_AutoBool(){ m_bValue=FALSE; }
	};

public:
	//support for On-Demand loading
	struct _OnDemandLoadingItem
	{	LPTSTR	Filename;
		LPTSTR	SectionName;
		_OnDemandLoadingItem(LPCTSTR fn,LPCTSTR section_full);
		~_OnDemandLoadingItem();
	};
	typedef _OnDemandLoadingItem* _LPOnDemandLoadingItem;
	struct _OnDemandLoadingObjectList_class:public std::map<LPVOID,_LPOnDemandLoadingItem>
	{
		~_OnDemandLoadingObjectList_class();
		void AddObject(LPVOID runtime_addr,LPCTSTR fn,LPCTSTR section_full);
		const _OnDemandLoadingItem* QueryObject(LPVOID runtime_addr);
		BOOL RemoveObject(LPVOID runtime_addr);
	};

public:
	static _OnDemandLoadingObjectList_class m_OnDemandLoadingObjectList;
	static CFile64_Composition g_File_OnDemandObject;
	template<typename T> static void LoadObjectOnDemand(T* x)
	{	const _OnDemandLoadingItem* pOnDemandEntry = m_OnDemandLoadingObjectList.QueryObject(x);
		if(pOnDemandEntry)
		{	w32::CFile64_Composition File;																	
			_CheckDump(_text_load_ondemand()<<pOnDemandEntry->Filename<<_text_return_sp());
			_CheckDump(_text_section()<<pOnDemandEntry->SectionName);											
			if(	File.Open(pOnDemandEntry->Filename) && File.EnterSectionFull(pOnDemandEntry->SectionName) ) 
			{	
				BOOL IsRecursived = FALSE;
				File.m_bSectionSwitched = FALSE;
				__static_if(rt::TypeTraits<T>::IsAggregate){ File.Read(x,sizeof(T)); }
				__static_if(!rt::TypeTraits<T>::IsAggregate)
				{
					__static_if(rt::IsMemberVisitorSupported<T>::Result){ IsRecursived=TRUE; }
					if(SUCCEEDED(x->Load(File)))														
					{	
						_MEMBER_LOAD_IGNORE_ONDEMAND(x);
					}
				}

				if(!IsRecursived)
				{
					_CheckDump(_meta_::_text_left());
					if(File.m_bSectionSwitched)
					{	_CheckDump(_meta_::_text_composed()); }
					else
					{	rt::_meta_::_Base_MemberVisitor_SaveLoad::DumpSize((UINT)File.GetCurrentPosition()); }
					_CheckDump(_meta_::_text_right_return());
				}
			}
			else
			{
				_CheckDump(_text_fail_ondemand());
			}
		}
	}

protected:
	int		m_NestedSectionLevel;
	BOOL	m_bReadOnly;
	std::map< stdstring, std::pair<ULONGLONG,ULONGLONG> > m_SectionTable;
	std::map< stdstring, std::pair<ULONGLONG,ULONGLONG> >::iterator m_CurSection;
	BOOL	m_bInSectionMode;

	BOOL	_EnterSection(LPCTSTR section_name);
	void	_ExitSection();

public:
	BOOL	m_bSectionSwitched;
	int		GetCurrentNestedSectionLevel(){ return m_NestedSectionLevel; }	
	BOOL	EnterSectionFull(LPCTSTR section_name_full);
	BOOL	EnterSection(LPCTSTR section_name);
	void	ExitSection();
	LPCTSTR	GetCurrentSectionName();
	LPCTSTR	GetCurrentSubSectionName();

	LONGLONG	GetLength();
	virtual ~CFile64_Composition(){ if(IsOpen())Close(); }
	void	Close();
	BOOL	IsEOF();
	BOOL	Open(LPCTSTR fn,UINT OpenFlag = Normal_Read);
	UINT	Read(LPVOID lpBuf,UINT nCount);
	UINT	Write(LPCVOID lpBuf,UINT nCount);
	BOOL	SetLength(ULONGLONG len);

public:
	LONGLONG	GetCurrentPosition();
	LONGLONG	Seek(LONGLONG offset,UINT nFrom = Seek_Begin);
	void		SeekToBegin(){ Seek(0); }
	void		SeekToEnd(){ Seek(0,Seek_End); }
};

} // namespace w32




namespace rt
{

namespace _meta_
{

class _Base_MemberVisitor_SaveLoad
{	friend class ::w32::CFile64_Composition;
private:
	static const int SECTION_NAME_MAX = 256 ;
	TCHAR						_SectionName[SECTION_NAME_MAX+4];
	LPTSTR						_pMemberName;
protected:
	w32::CFile64_Composition&	m_File;

protected:
	_Base_MemberVisitor_SaveLoad(w32::CFile64_Composition& file);

	void PrintIndent(int levelup=0);
	static void DumpSize(UINT bytes);
	LPCTSTR	ComposeSectionName(LPCTSTR pMemberName);

public:
	HRESULT Begin(LPCTSTR pClassName);
	HRESULT End(LPCTSTR pClassName);
};

} // namespace _meta_



#define _MEMBER_ALLOW_PERSISTENT()	public:															\
									HRESULT Save(w32::CFile64_Composition & file) const				\
									{	rt::_MemberVisitor::_Save saveto(file);						\
										return rt::_CastToNonconst(this)->VisitMembers(saveto);		\
									}																\
									HRESULT Load(w32::CFile64_Composition & file					\
											   = w32::CFile64_Composition::g_File_OnDemandObject)	\
									{	rt::_MemberVisitor::_Load loadfrom(file);					\
										return VisitMembers(loadfrom);								\
									}																\
									static const int MEMBER_ALLOW_PERSISTENT ;						\
									private:														\



namespace _MemberVisitor
{

class _Save:public rt::_meta_::_Base_MemberVisitor_SaveLoad
{
	
public:
	static const int Visitor_Id = MV_Save ;
	_Save(w32::CFile64_Composition& file);

	template< typename T >
	HRESULT Visit(T& member,DWORD flag,LPCTSTR pMemberName)
	{	
		ASSERT(&m_File != &w32::CFile64_Composition::g_File_OnDemandObject); //Call Save for OnDemand Loading Sign is not use
		if(!m_File.IsOpen())return E_HANDLE;

		if(m_File.EnterSection(ComposeSectionName(pMemberName)))
		{
			BOOL IsRecursived = FALSE;
			__static_if(rt::IsMemberVisitorSupported<T>::Result){ IsRecursived=TRUE; }

			__static_if(rt::TypeTraits<T>::IsAggregate)
			{	// Aggregate Type
				m_File.Write(&member,sizeof(T));
				__PersistDump(w32::_meta_::_text_left());
				DumpSize(sizeof(T));
				__PersistDump(w32::_meta_::_text_right_return());
			}
			__static_if(!(rt::TypeTraits<T>::IsAggregate))
			{	// non-Aggregate Type
				if(IsRecursived)__PersistDump('\n');
				m_File.m_bSectionSwitched = FALSE;
				member.Save(m_File);
				if(!IsRecursived)
				{
					__PersistDump(w32::_meta_::_text_left());
					if( m_File.m_bSectionSwitched )
					{	__PersistDump(w32::_meta_::_text_composed()); }
					else
					{	DumpSize((UINT)m_File.GetCurrentPosition()); }
					__PersistDump(w32::_meta_::_text_right_return());
				}
			}

			m_File.ExitSection();
			return S_OK;
		}
		else
		{	__PersistDump(w32::_meta_::_text_not_exist());
			return S_FALSE;
		}
	}
};

class _Load:public rt::_meta_::_Base_MemberVisitor_SaveLoad
{	__forceinline static LPCTSTR _text_load_delay(){ return _T(" (delayed)\n"); }
public:
	static const int MV_LOAD_ON_DEMAND = 0x1;
	static const int Visitor_Id = MV_Load;
	_Load(w32::CFile64_Composition& file);

	template< typename T >
	HRESULT Visit(T& member,DWORD flag,LPCTSTR pMemberName=NULL)
	{	
		if(&m_File != &w32::CFile64_Composition::g_File_OnDemandObject)
		{
			if(!m_File.IsOpen())return E_HANDLE;

			if(m_File.EnterSection(ComposeSectionName(pMemberName)))
			{
				ASSERT((flag&MV_LOAD_ON_DEMAND)==0 || pMemberName[0]!=_T('[')); //Base class can not be loaded on-demend

				BOOL IsRecursived = FALSE;
				__static_if(rt::IsMemberVisitorSupported<T>::Result){ IsRecursived=TRUE; }

				if( (MV_LOAD_ON_DEMAND & flag) != MV_LOAD_ON_DEMAND )
				{	
					__static_if(rt::TypeTraits<T>::IsAggregate)
					{	// Aggregate Type
						m_File.Read(&member,sizeof(T));
						__PersistDump(w32::_meta_::_text_left());
						DumpSize(sizeof(T));
						__PersistDump(w32::_meta_::_text_right_return());
					}
					__static_if(!(rt::TypeTraits<T>::IsAggregate))
					{	// non-Aggregate Type
						if(IsRecursived)__PersistDump('\n');
						m_File.m_bSectionSwitched = FALSE;
						member.Load(m_File);
						if(!IsRecursived)
						{
							__PersistDump(w32::_meta_::_text_left());
							if(m_File.m_bSectionSwitched)
								__PersistDump(w32::_meta_::_text_composed());
							else
								DumpSize((UINT)m_File.GetCurrentPosition());
							__PersistDump(w32::_meta_::_text_right_return());
						}
					}
				}
				else
				{	// Load On-demand
					w32::CFile64_Composition::m_OnDemandLoadingObjectList.AddObject(&member,m_File.GetFilename(),m_File.GetCurrentSectionName());
					__PersistDump(_text_load_delay());
				}

				m_File.ExitSection();
				return S_OK;
			}
			else
			{	__PersistDump(w32::_meta_::_text_not_exist());
				return S_FALSE;
			}
		}
		else //Load all On-Demand objects
		{
			if(flag){ _MEMBER_LOAD_ONDEMAND(member); }
			
			__static_if(rt::IsMemberVisitorSupported<T>::Result)
			{	//May contain On-Demand loading objects
				return member.Load(w32::CFile64_Composition::g_File_OnDemandObject);
			}
		}

		if(m_File.ErrorOccured())
			return w32::HresultFromLastError();
		else
			return S_OK;
	}
};

} // _MemberVisitor
} // namespace rt

#undef __PersistDump
