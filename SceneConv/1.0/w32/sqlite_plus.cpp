#include "StdAfx.h"
#include "sqlite_plus.h"
#include "atlconv.h"

#include "src\sqlite3.h"
#include "file_64.h"


namespace w32
{

SQLite_Database::SQLite_Database()
{
	m_pDatabase = NULL;
	m_LastError = SQLITE_OK;
}

SQLite_Database::~SQLite_Database()
{
	Close();
}

BOOL SQLite_Database::Open(LPCTSTR filename)
{
#ifdef UNICODE
	m_LastError = sqlite3_open16(filename, &m_pDatabase);
#else
	m_LastError = sqlite3_open(filename, &m_pDatabase);
#endif

	if(m_LastError == SQLITE_OK)
	{	
		return TRUE;
	}
	else
	{
		Close();
		return FALSE;
	}
}

BOOL SQLite_Database::Execute(LPCTSTR sql_statement)
{
	ASSERT(m_pDatabase);

	LPSTR zErrMsg = NULL;
	m_LastError = sqlite3_exec(m_pDatabase, CT2A(sql_statement), NULL/*_callback::sql_result*/, this, &zErrMsg);

	if(zErrMsg)
	{
		printf("ERROR: %s\n",zErrMsg);
		sqlite3_free(zErrMsg);
	}
	
	return m_LastError == SQLITE_OK;
}

_meta_::_Query SQLite_Database::Query(LPCTSTR sql_statement)	// statement returning records
{
	ASSERT(m_pDatabase);

	sqlite3_stmt* stmt_obj=NULL;
#ifdef UNICODE
	m_LastError = sqlite3_prepare16_v2(m_pDatabase,sql_statement,-1,&stmt_obj,NULL);
#else
	m_LastError = sqlite3_prepare_v2(m_pDatabase,sql_statement,-1,&stmt_obj,NULL);
#endif

	if(m_LastError == SQLITE_OK)
		return _meta_::_Query(this,stmt_obj);
	else
		return _meta_::_Query(0,0);
}

LPCTSTR	SQLite_Database::GetLastErrorMessage()
{
	ASSERT(m_pDatabase);

#ifdef UNICODE
	return (LPCTSTR)sqlite3_errmsg16(m_pDatabase);
#else
	return sqlite3_errmsg(m_pDatabase);
#endif
	
}

void SQLite_Database::Close()
{
	if(m_pDatabase)
	{
		sqlite3_close(m_pDatabase);
		m_pDatabase = NULL;
	}
}

_meta_::_Query::~_Query()
{
	if(m_pStatement)
	{
		sqlite3_finalize(m_pStatement);
		m_pStatement = NULL;
	}
}

_meta_::_Query::_Query(const _Query& x)
{
	m_pStatement = x.m_pStatement;
	m_pDatabase = x.m_pDatabase;
	rt::_CastToNonconst(&x)->m_pStatement = NULL;
}

SQLite_ResultSet::SQLite_ResultSet(const _meta_::_Query& query)
	:_meta_::_Query(query)
{
	if(IsOk())
	{
		int rc = sqlite3_step(m_pStatement);
		if(rc == SQLITE_DONE || rc == SQLITE_ROW){}
		else
			Close();
	}
}

BOOL SQLite_ResultSet::Next()
{
	ASSERT(m_pStatement);
	return sqlite3_step(m_pStatement) == SQLITE_ROW;
}

BOOL SQLite_ResultSet::Rewind()
{
	ASSERT(m_pStatement);
	return (sqlite3_reset(m_pStatement) == SQLITE_OK) && Next();
}

DWORD SQLite_ResultSet::GetColumnType(int iCol)
{
	ASSERT(m_pStatement);
	return sqlite3_column_type(m_pStatement,iCol);
}

UINT SQLite_ResultSet::GetColumnCount()
{
	return sqlite3_column_count(m_pStatement);
}

LPCTSTR	SQLite_ResultSet::GetColumnName(int iCol)
{
	ASSERT(m_pStatement);
#ifdef UNICODE
	return GetColumnNameW(iCol);
#else
	return GetColumnNameA(iCol);
#endif
}

LPCSTR	SQLite_ResultSet::GetColumnNameA(int iCol)
{
	return sqlite3_column_name(m_pStatement,iCol);
}

LPCWSTR	SQLite_ResultSet::GetColumnNameW(int iCol)
{
	return (LPCWSTR)sqlite3_column_name16(m_pStatement,iCol);
}


double	SQLite_ResultSet::GetFloat(int iCol)
{
	ASSERT(m_pStatement);
	return sqlite3_column_double(m_pStatement,iCol);
}

int		SQLite_ResultSet::GetInteger(int iCol)
{
	ASSERT(m_pStatement);
	return sqlite3_column_int(m_pStatement,iCol);
}

__int64	SQLite_ResultSet::GetLargeInteger(int iCol)
{
	ASSERT(m_pStatement);
	return sqlite3_column_int64(m_pStatement,iCol);
}

LPCBYTE	SQLite_ResultSet::GetBinary(int iCol, int* pSize)
{
	ASSERT(m_pStatement);
	LPCBYTE pdata = (LPCBYTE)sqlite3_column_blob(m_pStatement,iCol);
	if(pdata)
	{	
		if(pSize)
			*pSize = sqlite3_column_bytes(m_pStatement,iCol);
		
		return pdata;
	}

	return NULL;
}

LPCTSTR	SQLite_ResultSet::GetText(int iCol)
{
	ASSERT(m_pStatement);
#ifdef UNICODE
	return GetTextW(iCol);
#else
	return GetTextA(iCol);
#endif
}

LPCSTR	SQLite_ResultSet::GetTextA(int iCol)
{
	return (LPCSTR)sqlite3_column_text(m_pStatement,iCol);
}

LPCWSTR	SQLite_ResultSet::GetTextW(int iCol)
{
	return (LPCWSTR)sqlite3_column_text16(m_pStatement,iCol);
}

void SQLite_ResultSet::Close()
{
	if(m_pStatement)
	{
		sqlite3_finalize(m_pStatement);
		m_pStatement = NULL;
	}
}

BOOL SQLite_ResultSet::IsOk() const
{
	return m_pStatement!=NULL;
}

void SQLite_ResultSet::ExportExcel(LPCTSTR fn)	// CSV format
{
	ASSERT(m_pStatement);

	if(Rewind() && GetColumnCount())
	{
		w32::CFile64_Text	file;
		if(file.Open(fn,w32::CFile64::Normal_Write))
		{
			int co = GetColumnCount();
			for(int i=0;i<co;i++)
			{
				fprintf(file,"%s (T%d) %c",GetColumnNameA(i),GetColumnType(i),i<co-1?',':'\n');
			}

			do
			{	for(int i=0;i<co;i++)
				{
					LPCSTR str = GetTextA(i);
					fprintf(file,"%s%c",str?str:"",i<co-1?',':'\n');
				}
			}while(Next());
		}
	}
}


} // namespace sqlite

