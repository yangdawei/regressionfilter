#include "stdafx.h"
#include "mediainfo_lib.h"
#include <MediaInfo\MediaInfo.h>

#ifdef UNICODE
	#ifdef _DEBUG
		#pragma comment(lib,"MediaInfo_x86_ud.Lib")
	#else
		#pragma comment(lib,"MediaInfo_x86_ur.Lib")
	#endif
#else
	#ifdef _DEBUG
		#pragma comment(lib,"MediaInfo_x86_ad.Lib")
	#else
		#pragma comment(lib,"MediaInfo_x86_ar.Lib")
	#endif
#endif

using namespace MediaInfoLib;

#define m_pMI		((MediaInfo*)pObj)
#define m_String	(*((String*)pString))

w32::CMediaFileInfo::CMediaFileInfo()
{
	pObj = new MediaInfo;
	pString = new String;

	//wprintf(m_pMI->Option(_T("\nInfo_Parameters")).c_str());
	//wprintf(m_pMI->Option(_T("\nInfo_Capacities")).c_str());
	//wprintf(m_pMI->Option(_T("\nInfo_Codecs")).c_str());
}


w32::CMediaFileInfo::~CMediaFileInfo()
{
	if(pObj)
	{
		delete m_pMI;
		pObj = NULL;
	}

	if(pString)
	{
		delete  &m_String;
		pString = NULL;
	}
}

bool w32::CMediaFileInfo::Open(const wchar_t* filename)
{
	return m_pMI->Open(String(filename));
}

void w32::CMediaFileInfo::OpenBuffer_Begin(__int64 file_size)
{
	m_pMI->Open_Buffer_Init(file_size);
	_open_buffer_file_size = file_size;
	//printf("%ld FSize\n",file_size);
}

bool w32::CMediaFileInfo::OpenBuffer_Push(const void * pData, unsigned int Size, __int64* pOffsetNext)
{
	if(!pOffsetNext)return false;

	int ret = m_pMI->Open_Buffer_Continue((const ZenLib::int8u*)pData,Size);

	__int64 next = m_pMI->Open_Buffer_Continue_GoTo_Get();
	*pOffsetNext = next;

	//printf("%d\t%ld\n",ret,next);

	if(ret & 0x8)return false;
	if(next>=0)m_pMI->Open_Buffer_Init(_open_buffer_file_size,next);

	return next < _open_buffer_file_size;
}

void w32::CMediaFileInfo::OpenBuffer_End()
{
	m_pMI->Open_Buffer_Finalize();
}


void w32::CMediaFileInfo::Close()
{
	return m_pMI->Close();
}

const wchar_t* w32::CMediaFileInfo::GetInformation()
{
	m_String.clear();
	m_String = m_pMI->Inform();
	return m_String.c_str();
}

const wchar_t* w32::CMediaFileInfo::GetString(const wchar_t* pParamName,int StreamType)
{
	if(StreamType == Stream_Any)
	{
		for(int i=0;i<Stream_Max;i++)
		{
			m_String = m_pMI->Get((stream_t)i,0,String(pParamName));
			if(m_String.c_str()[0])
				break;
		}
	}
	else
		m_String = m_pMI->Get((stream_t)StreamType,0,String(pParamName));

	return m_String.c_str();
}

int w32::CMediaFileInfo::GetInteger(const wchar_t* pParamName,int StreamType)
{
	return _wtoi(GetString(pParamName,StreamType));
}

double w32::CMediaFileInfo::GetFloat(const wchar_t* pParamName,int StreamType)
{
	return _wtof(GetString(pParamName,StreamType));
}

