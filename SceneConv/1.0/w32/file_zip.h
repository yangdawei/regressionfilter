#pragma once

//////////////////////////////////////////////////////////////////////
// Visual Computing Foundation Classes (VCFC)
//					Jiaping Wang  2006.2
//					e_boris2002@hotmail.com
//
// (C) Copyright Jiaping Wang 2004.
//
// Permission to copy, use, modify, sell and distribute this software
// is granted provided this copyright notice appears in all copies.
// This software is provided "as is" without express or implied
// warranty, and with no claim as to its suitability for any purpose.
//
// Absolutely no warranty!!
//////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////
//  Brief Description
//
//  file_zip.h
//
//  I/O zip files. 
//  supports [store mode], for other modes zlib is required
//
//////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////
// Revise History
//
// Inital version			2010.10.28		Jiaping
//							based on ZIP File Format Specification V4.5
//							From PKWARE Inc. Zip64 is NOT handled
//
//////////////////////////////////////////////////////////////////////

#include "file_64.h"


namespace w32
{

////////////////////////////////////////////////////////////////
// File Format Overview (Zip98)
// File = 
//		[local file header 1][file data 1][data descriptor 1]
//		. 
//		.
//		.
//		[local file header n][file data n][data descriptor n]
//		[central directory]
//		[end of central directory record]	// CentralDirectory
//
// [central directory] = 
//      [file header 1]	// FileEntry
//		  .
//		  .
//		  . 
//		[file header n]
//


class CFileZip
{

#pragma pack(1)
	struct LocalFileHeader
	{
		static const int header_magic = 0x04034b50;
		enum	_tagFlag
		{	LFH_DATA_DESCRIPTOR_EXISTS = 0x0004,	// DataDesc
		};

		DWORD	Signature;		
        WORD	Version;
		WORD	Flag;
        WORD	Compression;
        WORD	FileTime;
        WORD	FileDate;
        DWORD	CRC32;
		DWORD	Size;				//	after compression, sizeof [file data 1]
		DWORD	SizeOriginal;
		WORD	FileNameLength;		// sizeof(FileName)
		WORD	ExtraFieldLength;
		CHAR	FileName[1];		//	FileNameLength
		//CHAR	ExtraField[1];		//	after FileName
	};

	struct	DataDesc
	{	DWORD	CRC32;
		DWORD	Size;				//	after compression, sizeof [file data 1]
		DWORD	SizeOriginal;		
	};

	struct	FileEntry
	{	
		static const int header_magic = 0x02014b50;
		DWORD	Signature;				// 
		WORD	ZipVersion;				// version made by 
		WORD	UnzipVersion;			// version needed to extract
		WORD	Flag;
		WORD	Compression;
        WORD	FileTime;
        WORD	FileDate;
        DWORD	CRC32;
		DWORD	Size;				//	after compression, sizeof [file data 1]
		DWORD	SizeOriginal;
		WORD	FileNameLength;		// sizeof(FileName)
		WORD	ExtraFieldLength;
		WORD	FileCommentLength;
		WORD	DiskNumber;
		WORD	FileAttributes;
		DWORD	FileAttributesOriginal;
		DWORD	OffsetToLocalFileHeader;
        CHAR	FileName[1];
		//CHAR	ExtraField[1];
		//CHAR	FileComment[1];

		UINT	GetTotalSize() const { return sizeof(FileEntry)-1+FileNameLength+ExtraFieldLength+FileCommentLength; }
		UINT	GetOffsetToLocalData() const { return OffsetToLocalFileHeader + sizeof(LocalFileHeader)-1 + FileNameLength; }
	};

	struct	CentralDirectory
	{
		static const int header_magic = 0x06054b50;
		DWORD	Signature;
		WORD	m_uThisDisk;	
		WORD	m_uDiskWithCD;		
		WORD	m_uDiskEntriesNo;	
		WORD	m_uEntriesNumber;
		DWORD	m_uSize;			// size of the central directory
		DWORD	m_uOffset;			// offset of start of central directory with respect to the starting disk number
		WORD	m_ZipCommentLength;
		CHAR	m_ZipComment[1];
	};

#pragma pack()

	w32::CFile64				_BaseFile;
	rt::Buffer<BYTE>			_FileEntryPool;

protected:
	rt::_stream*				m_pZipFile;
	rt::Buffer<CHAR>			m_Password;
	UINT						m_CompressionMode;

	BOOL						m_ListSorted;
	BOOL						m_IsReadOnly;
	rt::StringA					m_ZipComment;
	UINT						m_CentralDirectoryStart;
	BOOL						m_CentralDirectoryModified;

	struct FileEntryPtr: public rt::StringA_Ref
	{
		FileEntry*	p;
		TYPETRAITS_DECL_IS_AGGREGATE(true)
		FileEntryPtr(FileEntry* x):rt::StringA_Ref(x->FileName,x->FileNameLength){ p = x; }
		operator FileEntry*() { return p; }
	};
	rt::BufferEx<FileEntryPtr>	m_FileEntries;
	void						_FreeFileEntry(FileEntryPtr& p);

private:
	FileEntry*					_AddingNonZeroFileEntry;
	FileEntry*					_HoleFromEntry;
	UINT						_HoleTakenSize;
	UINT						_CentralDirectoryStart_Old;
	UINT						_ClaimedSize;
	FileEntry*					_AddNonZeroFileEntry_Begin(const rt::StringA_Ref& pathname, UINT size, const FILETIME& ftime);
	// Following member MUST be updated before call _AddNonZeroFileEntry_End(TRUE);
	//e.CRC32 = 0;
	//e.Compression = 0;
	//e.Size and e.SizeOriginal can be only updated to that smaller than claimed in _AddNonZeroFileEntry_Begin call
	BOOL						_AddNonZeroFileEntry_End(BOOL commit);
	void						_crc_checksum(LPCVOID pSrc, UINT SrcLen, LPDWORD crc32);
#ifdef ZLIB_VERSION
	BOOL						_zlib_encode(LPCVOID pSrc, UINT SrcLen, LPVOID pDst, UINT& DstLen);
	BOOL						_zlib_decode(LPCVOID pSrc, UINT SrcLen, LPVOID pDst, UINT& DstLen);
#endif
public:
	//struct plugin
	//{	virtual BOOL Process(LPCVOID pSrc, UINT SrcLen, LPVOID pDst, UINT& DstLen) = 0;	};
	struct stream_readonly
	{	virtual UINT Read(LPVOID p, UINT size) = 0; /* return number of byte read */ };
	struct stream_writeonly
	{	virtual UINT Write(LPCVOID p, UINT size) = 0; /* return number of byte written (INFINITE for compression failure, call for AddFile again in store mode */ 
		virtual BOOL Finalize(BOOL commit) = 0; /* call to stop writing */
	};

protected:
	rt::Buffer<BYTE>	_StreamBuff;

	struct decompress_stream:public stream_readonly
	{
		CFileZip* pThis;
		UINT	TotalOutput;
		UINT	Outputed;
		UINT	BaseInFile;
		DWORD	CRC;
		DWORD	Target_CRC;
		void	Init(UINT total_out_size, UINT base, DWORD target_CRC);
	protected:
		BOOL	CheckCRC(LPCVOID p, UINT sz);
	};
	struct decompress_store:public decompress_stream
	{	virtual UINT Read(LPVOID p, UINT size);
	};
	decompress_store	stream_read_store;
#ifdef ZLIB_VERSION
	struct decompress_zlib:public decompress_stream
	{	
		z_stream* infstrm;
		HANDLE				_FileMap;
		LPCBYTE				_Mapped;
		UINT				_AlignOffset;
		UINT				CompressedSize;
		UINT				CompressedAte;

		decompress_zlib();
		~decompress_zlib(){ Clear(); }
		void	Init(UINT total_out_size, UINT base, DWORD target_CRC, UINT compressed_size);
		void	Clear();
		virtual UINT Read(LPVOID p, UINT size);
	private: void	Init(UINT total_out_size, UINT base, DWORD target_CRC);
	};
	decompress_zlib		stream_read_zlib;
#endif

	struct compress_stream:public stream_writeonly
	{	CFileZip*	pThis;
		UINT		Written;
		UINT		BaseInFile;
		DWORD		CRC;
		void		Init(UINT base);
	protected:
		virtual		BOOL Finalize(BOOL commit);
	};
	struct compress_store:public compress_stream
	{	virtual		UINT Write(LPCVOID p, UINT size);
	};
	compress_store	stream_write_store;
#ifdef ZLIB_VERSION
	struct compress_zlib:public compress_stream
	{	
		z_stream*	defstrm;
		void		Clear();
		virtual		BOOL Finalize(BOOL commit);
		virtual		UINT Write(LPCVOID p, UINT size);
		compress_zlib();
		~compress_zlib(){ Clear(); }
	};
	compress_zlib	stream_write_zlib;
#endif

public:
	enum _tagCompressionMode
	{	ZIP_STORE = 0,
		ZIP_DEFLATED = 8
	};
	void	SetCompressionMode(UINT mode = ZIP_DEFLATED){ m_CompressionMode = mode; }

public:
	CFileZip();
	~CFileZip(){ Close(); }
	BOOL	IsOpen() const { return m_pZipFile!=NULL; }
	BOOL	Open(LPCTSTR fn, UINT openflag = CFile64::Normal_Read, BOOL load_indexed = TRUE);
	BOOL	Open(rt::_stream* pFile, UINT openflag, BOOL load_indexed = TRUE);
	void	Close();	// save and close
	BOOL	Save();		// just save, no squeeze
	BOOL	Squeeze();	// squeeze space after some deletion

	int		FindFile(const rt::StringA_Ref& pathname);	// -1 if not found, returned index will be invalidate when File List is changed
														// by AddFile and RemoveFile
	int		FindFile(LPCSTR pathname){ return FindFile(rt::StringA_Ref(pathname)); }
	int		FindFileRecursive(LPCSTR filename, int start_from_idx = 0);			// by filename, without path

	BOOL	AddZeroSizedEntry(const rt::StringA_Ref& pathname, const FILETIME& ftime, DWORD attrib = FILE_ATTRIBUTE_DIRECTORY); // add a zero-sized file by attrib = FILE_ATTRIBUTE_ARCHIVE
	BOOL	AddZeroSizedEntry(LPCSTR pathname, const FILETIME& ftime, DWORD attrib = FILE_ATTRIBUTE_DIRECTORY)
				{ return AddZeroSizedEntry(rt::StringA_Ref(pathname),ftime, attrib); }
	BOOL	AddZeroSizedEntry(const rt::StringA_Ref& pathname, DWORD attrib = FILE_ATTRIBUTE_DIRECTORY)
				{ return AddZeroSizedEntry(rt::StringA_Ref(pathname),w32::CTime64<>::Time64ToFileTime(_time64(NULL)), attrib); }
	BOOL	AddZeroSizedEntry(LPCSTR pathname, DWORD attrib = FILE_ATTRIBUTE_DIRECTORY)
				{ return AddZeroSizedEntry(rt::StringA_Ref(pathname), attrib); }


	stream_writeonly* AddFile(const rt::StringA_Ref& pathname, UINT size, const FILETIME& ftime);
	stream_writeonly* AddFile(const rt::StringA_Ref& pathname, UINT size)
				{ return AddFile(rt::StringA_Ref(pathname),size,w32::CTime64<>::Time64ToFileTime(_time64(NULL))); }
	stream_writeonly* AddFile(LPCSTR pathname, UINT size, const FILETIME& ftime)
				{ return AddFile(rt::StringA_Ref(pathname),size,ftime); }
	stream_writeonly* AddFile(LPCSTR pathname, UINT size)
				{ return AddFile(rt::StringA_Ref(pathname),size); }

	BOOL	AddFile(const rt::StringA_Ref& pathname, LPCVOID pData, UINT size, const FILETIME& ftime);
	BOOL	AddFile(const rt::StringA_Ref& pathname, LPCVOID pData, UINT size)
				{ return AddFile(rt::StringA_Ref(pathname),pData,size,w32::CTime64<>::Time64ToFileTime(_time64(NULL))); }
	BOOL	AddFile(LPCSTR pathname, LPCVOID pData, UINT size, const FILETIME& ftime)
				{ return AddFile(rt::StringA_Ref(pathname),pData,size,ftime); }
	BOOL	AddFile(LPCSTR pathname, LPCVOID pData, UINT size)
				{ return AddFile(rt::StringA_Ref(pathname),pData,size); }
	
	BOOL	ExtractFile(UINT idx, LPVOID pData);	// pData pointing memory with size = GetFileSize
	stream_readonly* ExtractFile(UINT idx);			// returned object will be inavailable after next ExtractFile call
	DWORD	GetFileAttribute(UINT idx) const;
	UINT	GetFileSize(UINT idx) const;
	UINT	GetFileArchivedSize(UINT idx) const;
	BOOL	IsFileExtractable(UINT idx) const;
	void	GetFileTime(UINT idx, FILETIME* t);
	rt::StringA_Ref
			GetFileName(UINT idx) const;
	void	RemoveFile(UINT idx);	// will not invalidate index returned by FindFile, NOT tested yet
	UINT	GetEntryCount() const { return m_FileEntries.GetSize(); }

	// handle both files, zero-sized files and directories
	void	ReindexAllEntries();	// call after adding something
	BOOL	AddEntryFrom(LPCTSTR pathname_sys, LPCSTR pathname_zip);
	BOOL	ExtractEntryTo(UINT idx, LPCTSTR to_pathname_sys);
	BOOL	AddAllEntriesFrom(LPCTSTR path_sys, LPCSTR path_zip, BOOL include_subfolders);			// add all files in path_sys, to directory path_zip in zip
	BOOL	ExtractAllEntriesTo(LPCSTR path_zip, LPCTSTR path_sys, BOOL include_subfolders);		// extract all files in directory path_zip in zip to path_sys
};



} // namespace w32
