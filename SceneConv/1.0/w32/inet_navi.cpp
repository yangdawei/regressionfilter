#include "stdafx.h"
#include "w32_basic.h"
#include "inet_navi.h"
#include <shlwapi.h>
#pragma comment(lib,"shlwapi.lib")

namespace w32
{

CHttpNavigator::CHttpNavigator()
{
	_pCallback = NULL;
}

void CHttpNavigator::_UpdateAdditionalHeader()
{
	static const rt::StringA_Ref BR("\r\n",2);

	_AdditionalHeader.Empty();
	if(!_Refer.IsEmpty())_AdditionalHeader += "Referer: " + _Refer + BR;
}

void CHttpNavigator::SetReferer(LPCSTR referer)
{
	_Refer = referer;
	_UpdateAdditionalHeader();
}

void CHttpNavigator::SetRedirected(const rt::StringA_Ref& url)
{
	int i = (int)url.FindCharactor(':');

	if(m_NavigatedDestination.IsEmpty() || (i>0 && url[i+1] == '/' && url[i+2] == '/'))
	{
		m_NavigatedDestination = url;
	}
	else
	{	
		LPSTR begin = &m_NavigatedDestination.GetBuffer()[7];

		if(url[0] == '/')
		{
			LPSTR p = strchr(begin,'/');
			if(p)
				m_NavigatedDestination = rt::StringA_Ref(m_NavigatedDestination, p) + url;
			else
				m_NavigatedDestination += url;
		}
		else
		{
			LPSTR pdir = strrchr(begin,'/');
			if(!pdir)
			{	m_NavigatedDestination += '/';
				pdir = &m_NavigatedDestination.GetBuffer()[m_NavigatedDestination.GetLength()-1];
			}

			LPCSTR path = url.Begin();
			for(;;)
			{	
				if(path[0] == '.' && path[1] == '/')
					path += 2;
				else if(path[0] == '.' && path[1] == '.' && path[2] == '/')
				{
					path += 3;
					LPSTR p = pdir;
					for(;p>begin;p--)
						if(*p == '/')
						{	pdir = p;
							break;
						}
				}
				else break;
			}

			m_NavigatedDestination = rt::StringA_Ref(m_NavigatedDestination, pdir + 1) + rt::StringA_Ref(path,url.End());
		}
	}
}

BOOL CHttpNavigator::NavigateTo(LPCSTR pURL, int max_redirection_times)
{
	static const rt::StringA_Ref content_type_html("text/html",9);

	if(strstr(pURL,"://"))
		m_NavigatedDestination = pURL;
	else
		m_NavigatedDestination = rt::StringA_Ref("http://",7) + pURL;

	while(	max_redirection_times &&
			m_HttpSession.Request_Get(m_NavigatedDestination, _AdditionalHeader, _AdditionalHeader.GetLength()) && 
			m_HttpSession.WaitResponse()
	)
	{	max_redirection_times--;
		if(	m_HttpSession.GetResponseParsedHeader().m_StateCode/100 == 3 && 
			!m_HttpSession.GetResponseParsedHeader().m_Redirected.IsEmpty()
		)
		{	SetRedirected(m_HttpSession.GetResponseParsedHeader().m_Redirected);
			if(_pCallback)_pCallback->OnReflect(m_NavigatedDestination.GetBuffer(),HTTPCLIENT_EVENT_REDIRECT);
		}
		else if(m_HttpSession.GetResponseParsedHeader().m_StateCode/100 == 2 && 
				m_HttpSession.GetResponseLength()
		)
		{	if(m_HttpSession.GetResponseParsedHeader().m_ContentType == content_type_html)
			{
				// analysis HTML content
				LPSTR pdoc = (LPSTR)m_HttpSession.GetResponse();

				LPSTR pend = StrStrIA(pdoc,"<body");
				if(pend)*pend = '\0';
				BOOL redirected = FALSE;
			
				while(pdoc && (pdoc = StrStrIA(pdoc,"http-equiv")))
				{
					LPSTR next = pdoc+10;
					LPSTR pclose = NULL;
					LPSTR pUrl = NULL;
					LPSTR pEndUrl = NULL;

					if( (pclose = strchr(pdoc, '>')) &&
						(*pclose = '\0', true) &&
						(pdoc = StrStrIA(pdoc+10,"refresh")) &&
						(pdoc = StrStrIA(pdoc+8,"content")) &&
						(pdoc = strchr(pdoc+8,'"')) &&
						(pdoc = StrStrIA(pdoc+1,"url")) &&
						(pUrl = strchr(pdoc+3,'=')) &&
						(	(pEndUrl = strchr(pUrl,'"')) ||
							(pEndUrl = strchr(pUrl,'\''))
						)
					)
					{	SetRedirected(rt::StringA_Ref(pUrl+1,pEndUrl));
						*pclose = '>';
						redirected = TRUE;
						break;
					}

					if(pclose)*pclose = '>';
					pdoc = next;
				}

				if(pend)*pend = '<';

				if(!redirected)
				{
					LPSTR p = (LPSTR)m_HttpSession.GetResponse();
					p[m_HttpSession.GetResponseLength()] = '\0';

					return TRUE;
				}
			}
			else return TRUE;
		}
		else break;
	}

	return FALSE;
}


} // namespace w32



