#pragma once

//////////////////////////////////////////////////////////////////////
// Visual Computing Foundation Classes (VCFC)
//					Jiaping Wang  2006.2
//					e_boris2002@hotmail.com
//
// (C) Copyright Jiaping Wang 2004.
//
// Permission to copy, use, modify, sell and distribute this software
// is granted provided this copyright notice appears in all copies.
// This software is provided "as is" without express or implied
// warranty, and with no claim as to its suitability for any purpose.
//
// Absolutly no warranty!!
//////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////
//  Brief Description
//
//  xml_soap.h
//  light weight XML composer (UTF-8 only)
//	light weight Just-in-time XML parser (UTF-8 only)
//	light weight soap protocal handler
//
//////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////
// Revise History
//
// Inital version	2007.8.5		Jiaping
//					2010.6.1		Jiaping, rewritten XML parser + XPath
//
//////////////////////////////////////////////////////////////////////

#include "..\rt\string_type.h"
#include "..\rt\compact_vector.h"


namespace w32
{

class CXMLComposer
{
	struct _TagCache
	{	LPCSTR	tag;
		UINT	tag_pos;
	};
	LPCSTR					_EnteringNodeTag;
	void					_InsertPlainText(LPCSTR text);
	UINT					_DepthMax;

protected:
	rt::_stream *			m_pStreamOut;
	LONGLONG				m_StreamOutOrgPos;
	rt::StringA				m_Output;
	UINT					m_WrittenLength;

	rt::BufferEx<_TagCache>	m_NestLevelTag;

	void Linebreak(int vary = 0);
	void Output(char x){ if(m_pStreamOut){m_pStreamOut->Write(&x,1); m_WrittenLength++; }else{m_Output+=x;} }
	void Output(LPCSTR x){ if(m_pStreamOut){int l = (int)strlen(x); m_pStreamOut->Write(x,l); m_WrittenLength+=l; }else{m_Output+=x;} }

public:
	static const char		g_XML_Header[40];
	CXMLComposer(rt::_stream * out_stream = NULL);	// if NULL, output in m_Content, obtain by GetDocument/GetDocumentLength
	
	void ResetContent(LPCSTR customized_header = NULL);
	void EnterNode(LPCSTR tag);
	void EnteringNode(LPCSTR tag);
	void EnteringNodeDone();
	void SetAttribute(LPCSTR name, LPCSTR value);  // call EnteringNode first, and call EnteringNodeDone when done
	void SetAttribute(LPCSTR name, int value);  // call EnteringNode first, and call EnteringNodeDone when done
	void SetAttribute(LPCSTR name, unsigned int value);  // call EnteringNode first, and call EnteringNodeDone when done
	void SetAttribute(LPCSTR name, long long value);  // call EnteringNode first, and call EnteringNodeDone when done
	void SetAttribute(LPCSTR name, unsigned long long value);  // call EnteringNode first, and call EnteringNodeDone when done
	void SetAttribute(LPCSTR name, float value);  // call EnteringNode first, and call EnteringNodeDone when done
	void SetAttribute(LPCSTR name, double value);  // call EnteringNode first, and call EnteringNodeDone when done

	void AddText(LPCSTR text);
	void AddCData(LPCSTR cdata);
	void ExitNode();
	void AppendTrail(LPCSTR text);

	LPCSTR		GetDocument() const;
	UINT		GetDocumentLength() const;  // size of XML document in byte, not include teminator zero
};


namespace _meta_
{
	struct _XML_Tag_Filter
	{	virtual BOOL _TagTest(const rt::StringA_Ref& tag_name, LPCSTR attributes, int DescendantLevel) = 0;	// TRUE for pass
	};

	static	void	_convert_xml_to_text(const rt::StringA_Ref& string, rt::StringA& text);

	static	LPCSTR	_seek_tag_open(LPCSTR start) { return strchr(start,'<'); }	// NULL for not found, or a pointer to '<'
	static	LPCSTR	_seek_tag_close(LPCSTR start) { return strchr(start,'>'); }  // NULL for not found, or a pointer to '>'

};


enum XMLParseError
{
	ERR_XML_OK							=  0,
	ERR_XML_HEADER_BAD_FORMAT			=  1,	// XML header should be <?xml version="1.0" encoding="UTF-8"?>
	ERR_XML_UNEXPECTED_ENDOFFILE		=  2,	// Unexpected end of file in parsing #
	ERR_XML_SYMBOL_UNEXPECTED			=  3,	// '#[0]' is not expected to appear here #
	ERR_XML_OUT_OF_MEMORY				=  4,	// Insufficient memory when paring #
	ERR_ATTRIBUTE_EQUALSIGN_NOT_FOUND	=  5,	// '=' not found after attrubute name #
	ERR_ATTRIBUTE_NAME_NOT_FOUND		=  6,	// attribute name not found, which is expected at #
	ERR_ATTRIBUTE_QUOTATION_NOT_MATCHED	=  7,	// '\'' in attribute value is not found to matched #
	ERR_NODE_CLOSURE_NOT_MATCHED		=  8,	// '</...>' is not found to match #
	ERR_TAG_CLOSURE_NOT_MATCHED			=  9,	// '>' is not found to match #
	ERR_CDATA_CLOSURE_NOT_MATCHED		= 10,	// ']]>' is not found to match #
	ERR_COMMENT_CLOSURE_NOT_MATCHED		= 11,	// '-->' is not found to match #
	ERR_PROC_INST_CLOSURE_NOT_MATCHED	= 12,	// '?>' or ']>' is not found to match #
	ERR_TAG_NAME_NOT_FOUND				= 13,	// tag name is not found after #
	ERR_XML_DEPTH_EXCEEDED				= 14,

	ERR_HTML_COMMENT_CLOSURE_NOT_MATCHED = 100,
	ERR_HTML_SPECIAL_NODE_UNKNONW,
	ERR_HTML_TAG_NAME_NOT_FOUND,
	ERR_HTML_ATTRIBUTE_NAME_NOT_FOUND,
	ERR_HTML_TAG_CLOSURE_NOT_MATCHED,
	ERR_HTML_QUOTATION_NOT_MATCHED,
	ERR_HTML_NODE_CLOSURE_NOT_MATCHED,
	ERR_HTML_UNEXPECTED_SYMBOL
};

enum XPathParseError
{
	ERR_XPATH_OK = 0,
	ERR_XPATH_DESCENDANT_IN_HALFWAY,				// descendants seletor '//' can only appear before last node qualifier
	ERR_SELECTOR_ATTRIBUTE_SET_NOT_SUPPORTED,		// Selects attributes is not supported
	ERR_SELECTOR_NODENAME_NOT_FOUND,				// nodename should appears after #
	ERR_SELECTOR_UNEXPECTED_SYMBOL,					// # is not expected here
	ERR_QUALIFIER_CONDITION_CLOSURE_NOT_MATCHED,	// ']' is not to match #
	ERR_QUALIFIER_CONDITION_NOT_SUPPORTED,			// only attributes conditions are supported
	ERR_QUALIFIER_VALUE_NOT_SUPPORTED,				// only string values are supported
	ERR_QUALIFIER_BAD_ORDINAL,						// non-number charactor found in [n] qualifier
	ERR_QUALIFIER_QUOTATION_NOT_MATCHED,			// '\'' in string value is not found to matched #
	ERR_QUALIFIER_OPERATOR_NOT_SUPPORTED,			// only = and != are supported
	
};

/*////////////////////////////////////////////////////////////////////////////////////////////////////////////
A subset of xpatn syntex is supported:
Node Selection	like ../aa/bb
	nodename  		Selects all nodes as the named
	/ 				Selects from the root node
	// 				Selects nodes in the document from the current node that match the selection no matter where they are
					// should follows only one node, //* , //nodename , //xx[@att] are legal. //aa/cc is illegal.
	. 				Selects the current node
	.. 				Selects the parent of the current node
	*				Selects all child nodes (Wildcard)
	@ 				Selects attributes [** IS NOT SUPPORTED **]
Predicates suffix like /aa/bb[n]/ccc.
	[@att]			Selects child node by having specific attributes
	[@att='xx']		Selects child node by having specific attributes with specific value
	[@att!='xx']	Selects child node by not having specific attributes with specific value
	[@att:='xx']	Selects child node by not having specific attributes with value initiated as 'xx'
	[n]				Selects child node by order (zero-based)
Operators (not done yet)
	=				Equal, between strings
	!=				Inequal, between strings
	or				Logic or, between logic expression
	and				Logic and, between logic expression
////////////////////////////////////////////////////////////////////////////////////////////////////////////*/
class CXMLParser;
class CXPathParser: public _meta_::_XML_Tag_Filter  // This class is not done yet
{	
	friend class CXMLParser;
	CXMLParser&			_XmlParser;
	LPCSTR				_pXPath;
	rt::StringA			_conv_value_temp;
	BOOL				_LastNodeSatificated;

protected:
	XPathParseError		m_XPathParseError;
	UINT				m_XPathParseErrorPosition;	// offset to _pXPath
	LPCSTR				SetLastSyntaxError(XPathParseError errnum, LPCSTR pos);

protected:

	enum _QualifierType	// grouped bit defined
	{	QT_NAME_NOT_PARSED		= 0x000,
		QT_NAME_ANY				= 0x001,		//  		*
		QT_NAME_EXACT			= 0x002,		//			nodename
		QT_ATTRIBUTE_ANY		= 0x010,		//			(not specified)
		QT_ATTRIBUTE_HAVE		= 0x020,		//			[@lang]
		QT_ATTRIBUTE_EQUAL		= 0x030,		//			[@lang='xx']
		QT_ATTRIBUTE_NOTEQUAL	= 0x040,		//			[@lang!='xx']
		QT_ATTRIBUTE_INITIATED  = 0x050,		//			[@lang:='xx']
		QT_ATTRIBUTE_EXPRESSION = 0x060,		//			[@lang='xx' or @lang='yy'] or things more complicated [** not done yet **]
		QT_ORDINAL				= 0x100,		//			[n]
	};	

	struct _Qualifier
	{
		_QualifierType		qt_TagName;
		rt::StringA			TagName;
	
		_QualifierType		qt_Attribute;
		rt::StringA			Attribute;
		rt::StringA			Value;

		int					qt_Ordinal;		// -1 for none

		void	Load(LPCSTR qualify, UINT length);

		// runtime information
		int					_LastOrdinalTestPast;
	};
	rt::BufferEx<_Qualifier>	m_Qualifiers;

	BOOL						m_bRelativePath;
	BOOL						m_bIncludeDescendants;
	_Qualifier					m_FinalQualifier;
	int							m_UpLevelCount;
	int							m_QualifierShifts;
	_meta_::_XML_Tag_Filter*	m_pUserTagFilter;

	BOOL				ParseQualifier(_Qualifier& q);
	BOOL				_TagTest(const rt::StringA_Ref& tag_name, LPCSTR attributes, int DescendantLevel);
	BOOL				QualifierTest(_Qualifier& q, const rt::StringA_Ref& tag_name, LPCSTR attributes);
	void				_ClearLastOrdinalTestPast(int level);

public:
						CXPathParser(CXMLParser& xml_parser);
	BOOL				Load(LPCSTR xpath);
	void				Clear();
	XPathParseError		GetLastSyntaxError() const { return m_XPathParseError; }
	UINT				GetLastSyntaxErrorPostion() const { return m_XPathParseErrorPosition; }
};


class CXMLParser		// no error handle of the content
{
	friend class CXPathParser;
	rt::StringA	_content_copy;
	LPCSTR		_attribute_cursor;
	LPCSTR		_root_node_xml_start;
	LPCSTR		_root_node_xml_end;

	LPCSTR		_seek_next_attrib(LPCSTR start, rt::StringA_Ref& attrib_name, rt::StringA_Ref& value) const;
	// succeeded if both attrib_name !IsEmpty

	LPCSTR		_html_check_node_close(const rt::StringA_Ref& tagname, LPCSTR p, BOOL just_next);
	LPCSTR		_search_control_close(LPCSTR start) const ; // search '>' for '<!'
	LPCSTR		_search_node_close(LPCSTR start_inner, const rt::StringA_Ref& tag_name, LPCSTR* pInnerXML_End = NULL) const ;	// search for matched '</tag_name>', return pointer to '>' OuterXML_End, pInnerXML_End to '<' InnerXML_End
	LPCSTR		_search_special_node_close(LPCSTR start_outer) const ;	// search ending for <!--,<?,<!. return pointer to '>'
	BOOL		_search_attrib(LPCSTR start, LPCSTR attrib_name, rt::StringA_Ref& value) const ; // if found, return pointer where parsing stopped, not found or error return NULL

protected:
	XMLParseError		m_XMLParseError;
	UINT				m_XMLParseErrorPosition;	// offset to m_pDocument
	LPCSTR				SetLastSyntaxError(XMLParseError errnum, LPCSTR pos);

	CXPathParser				m_XPathParser;
	_meta_::_XML_Tag_Filter*	m_pUserTagFilter;
	_meta_::_XML_Tag_Filter*	m_pCurTagFilter;
	BOOL						m_bTrySkipError;
	void						ClearXPathFilter();

protected:
	struct _node
	{
		rt::StringA_Ref	TagName;
		rt::StringA_Ref	Attributes;
		LPCSTR			OuterXML_Start;			// pointing to a '<'
		LPCSTR			InnerXML_Start;			// (InnerXML_Start-1) pointing to a '>', or 0
		BOOL			IsCompactNode;			// no child node, if true, InnerXML_Start is actaully OuterXML_End
	};
	LPCSTR				_search_node_close(_node& node, LPCSTR* pInnerXML_End = NULL) const ;	// search for matched '</tag_name>', return pointer to '>' OuterXML_End, pInnerXML_End to '<' InnerXML_End

	LPCSTR				m_pDocument;
	rt::BufferEx<_node>	m_NodePath;

	BOOL				_ForceWellformed(rt::StringA& out_in);		// FALSE if error found and fixed
	BOOL				_EnterNextNode(LPCSTR start, _meta_::_XML_Tag_Filter* pFilter = NULL, BOOL replace_node = FALSE);
	const _node&		_CurNode() const { ASSERT(m_NodePath.GetSize()); return m_NodePath[m_NodePath.GetSize()-1]; }
	void				ClearSyntaxError();
	
public:
	CXMLParser();
	CXMLParser(const CXMLParser& xml);
	const CXMLParser& operator = (const CXMLParser& xml);
	BOOL			Load(LPCTSTR fn);
	BOOL			LoadHTML(const rt::StringA_Ref& doc){ return LoadHTML(doc.Begin(),doc.GetLength()); }
	BOOL			LoadHTML(LPCSTR pHTML, UINT len = INFINITE);	// HTML will be convert to XHTML, p will not be held, p[] will be modified slightly
	BOOL			Load(LPCSTR text, BOOL Keep_referring = FALSE, int text_len = -1);	// if Keep_referring, the text pointer will be kept for document accessing
	BOOL			IsLoaded() const { return m_NodePath.GetSize(); }

	void			SetUserTagFilter(_meta_::_XML_Tag_Filter* pFilter = NULL){ m_pUserTagFilter = m_XPathParser.m_pUserTagFilter = pFilter; }
	UINT			GetDescendantLevel(){ return (UINT)m_NodePath.GetSize(); }

	BOOL			EnterXPathByCSSPath(LPCSTR css_path);
	BOOL			EnterXPath(LPCSTR xpath);	// Enter first node of the xpath selected set, call EnterSucceedNode for more nodes
	void			EscapeXPath();				// call this to remove XPath filtering

	void			EnterRootNode();	// equivalent to xpath: //* 
	BOOL			EnterParentNode();	// equivalent to xpath: ../*

	BOOL			EnterFirstChildNode(LPCSTR tag_name = NULL);
	BOOL			EnterNextSiblingNode();
	BOOL			EnterSucceedNode();	// go succeed node as depth-first traveling order

	BOOL			GetAttribute(LPCSTR name, rt::StringA& value) const;
	BOOL			GetAttribute_Path(LPCSTR name, rt::StringA& value) const;
	INT				GetAttribute_Int(LPCSTR name, INT default_value = 0) const;
	ULONGLONG		GetAttribute_FileSize(LPCSTR name, ULONGLONG default_value = 0) const;
	double			GetAttribute_Float(LPCSTR name, double default_value = 0.0) const;

	BOOL			GetFirstAttribute(rt::StringA& name, rt::StringA& value) const;
	BOOL			GetNextAttribute(rt::StringA& name, rt::StringA& value) const;

	void			GetNodeDocument(rt::StringA& doc_out);
	CXMLParser		GetNodeDocument(int nth_parent = 0) const;
	void			GetNodeName(rt::StringA& name) const;
	rt::StringA_Ref	GetNodeName() const;			// return the tag name
	BOOL			GetOuterXML(rt::StringA& text) const;
	BOOL			GetInnerXML(rt::StringA& text) const;
	BOOL			GetAttributesXML(rt::StringA& text) const;
	BOOL			GetInnerText(rt::StringA& text) const;
	BOOL			GetInBetweenText(rt::StringA& text) const;	// collect text that in between child nodes like <a> abc <b />, return: </a> =>" abc def "
	ULONGLONG		GetNodeDocumentOffset() const;	// the begining of outer xml of the current node, related to the begining of the input buffer;

	XMLParseError	GetLastSyntaxError() const { return m_XMLParseError; }
	UINT			GetLastSyntaxErrorPostion() const { return m_XMLParseErrorPosition; }
	void			Clear();
	void			EnableSyntaxFaultTolerance(BOOL enable = TRUE){ m_bTrySkipError = enable; }
	void			TextDump();
};

extern BOOL GetTextFromXML(rt::StringA & out, LPCTSTR fn, LPCSTR xpath, LPCSTR attrib = NULL);	// if attrib==NULL, return innerText

class CSoapRequest_Flickr
{
protected:
	static const char	g_Soap_Header[149];
	static const char	g_Soap_Tailer[41];
	BOOL				m_bFinalized;
	CXMLComposer		m_XMLCompos;
	void				Finalize();

public:
	CSoapRequest_Flickr();
	void SetMethod(LPCSTR function_name); // all parameters will be cleared
	void SetParam(LPCSTR param_name, LPCSTR value); // duplication is not checked

	operator	LPCSTR();
	operator	LPCBYTE();
	UINT		GetSize();  // size of XML document in byte, not include teminator zero
};


} // namespace w32

