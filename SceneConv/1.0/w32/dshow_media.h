#pragma once

//////////////////////////////////////////////////////////////////////
// Visual Computing Foundation Classes (VCFC)
//					Jiaping Wang  2006.2
//					e_boris2002@hotmail.com
//
// (C) Copyright Jiaping Wang 2004.
//
// Permission to copy, use, modify, sell and distribute this software
// is granted provided this copyright notice appears in all copies.
// This software is provided "as is" without express or implied
// warranty, and with no claim as to its suitability for any purpose.
//
// Absolutely no warranty!!
//////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////
//  Brief Description
//
//  dshow_media.h
//
//	Streaming video/audio from file source and live capture device
//  Library Dependence: dshow_media.lib
//
//////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////
// Revise History
//
// Inital version	2004.3		Jiaping
// Fit into CodeLib	2009.4.20	Jiaping
//
//////////////////////////////////////////////////////////////////////

#include <strmif.h>
#pragma comment(lib,"strmiids.lib")

#include <uuids.h>
#include <control.h>
#include <Rpc.h>
#pragma comment(lib,"Rpcrt4.lib")
#include "..\rt\compact_vector.h"
#include "w32_misc.h"

namespace w32
{

typedef const AM_MEDIA_TYPE* LPC_AM_MEDIA_TYPE;

namespace dshow
{

// The CLSID
#ifndef _WIN64
DEFINE_GUID(CLSID_Terminator,
0x7aad4d76, 0x1c02, 0x42c5, 0x92, 0x53, 0x14, 0x65, 0x55, 0x39, 0xbd, 0x57);
DEFINE_GUID(CLSID_Source, 
0x86860f32, 0x8ece, 0x47a1, 0x9d, 0x61, 0x25, 0xfd, 0xc9, 0x14, 0xbc, 0xfa);
#else
DEFINE_GUID(CLSID_Terminator, 
0xb2420e95, 0x5260, 0x4bf1, 0x99, 0x5b, 0xe2, 0xfc, 0xc8, 0xc8, 0xd7, 0x42);
DEFINE_GUID(CLSID_Source,
0xd8cdb87f, 0x54bc, 0x4d4b, 0xb7, 0x2a, 0x51, 0xcf, 0x30, 0x3f, 0x1b, 0x7d);
#endif


typedef struct _tagMediaFrame
{
	REFERENCE_TIME	TimeStart;
	REFERENCE_TIME	TimeEnd;
	LPBYTE			pMediaData;
	long			MediaDataSize;
}MediaFrame,*LPMediaFrame;

//////////////////////////////////////////////////////
//   non-standard COM interface for Sink Event
struct ITerminatorFilterEventSink  //It is not a COM interface 
{
	virtual HRESULT STDMETHODCALLTYPE	IncomingFrame() = 0;
	virtual HRESULT STDMETHODCALLTYPE	CheckMediaType(LPC_AM_MEDIA_TYPE pMediaType) = 0;
	virtual HRESULT STDMETHODCALLTYPE	StreamTerminated() = 0;
};
struct ISourceFilterEventSink  //It is not a COM interface 
{
	virtual HRESULT STDMETHODCALLTYPE	FillFrame() = 0;
};

//////////////////////////////////////////////////////
// Extension Interface
MIDL_INTERFACE("388DBFD7-5C18-47ec-B822-E08C94524BD5")
ISourceExtension : public IUnknown
{
	virtual HRESULT STDMETHODCALLTYPE SetMediaType(LPC_AM_MEDIA_TYPE pMediaType) = 0;
	virtual HRESULT STDMETHODCALLTYPE GetMediaFrameBuffer(LPMediaFrame* ppMediaFrame) = 0;
	virtual HRESULT STDMETHODCALLTYPE SetEventSink(ISourceFilterEventSink* pSink) = 0;
};
MIDL_INTERFACE("F5BBBBFB-74D3-4553-B403-63EC8485B0F3")
ITerminatorExtension : public IUnknown
{
	virtual HRESULT STDMETHODCALLTYPE GetMediaFrameBuffer(LPMediaFrame* ppMediaFrame) = 0;
	virtual HRESULT STDMETHODCALLTYPE SetEventSink(ITerminatorFilterEventSink* pSink) = 0;
};

static const int _AnyLength = 1;

enum _tagMediaTypeExtendInfo
{
	MTEI_FORMAT_NONE		=		0,
	MTEI_FORMAT_VIDEO		=		1,
	//MTEI_FORMAT_VIDEO_V2	=		2,	//Not implemented yet :P
	MTEI_FORMAT_AUDIO	=			3,
};

} // namespace dshow

typedef enum _tagVideoLibMediaType
{
	MT_DoNotCare = -1,
	//Raw media stream
	MT_Video = 0,
	MT_Audio,
	MT_Byte,
	MT_Text,
	MT_Timecode,
	MT_MediaTypeMask   = 0x0fff,

	//Compressed media stream
	MT_CompressedMedia = 0x1000,
	MT_VideoCompressed = MT_CompressedMedia,
	MT_AudioCompressed,
	MT_ByteCompressed,

	//Error
	MT_Unknown = 0x2000,
	MT_NotAvailable = 0xffffffff
}MediaType,*LPMediaType;


enum _tagMediaTimeUnit
{	
	MTU_FRAME	= 0x7b785570,
	MTU_SAMPLE	= 0x7b785572,
	MTU_100NANO = 0x7b785574,	// ms = 10000 * 100NANO
};

typedef enum _tagPinType
{
	PT_DoNotCare = 0,

	PT_Direction_Input,
	PT_Direction_Output,

	PT_Media_AnyVideo,
	PT_Media_AnyAudio,
	PT_Media_RawVideo,
	PT_Media_RawAudio,
	PT_Media_CompressedVideo,
	PT_Media_CompressedAudio,
	PT_Media_CompressedByte,
	PT_Media_Text,
	PT_Media_Timecode,

	PT_Connected,
	PT_Unconnected
}PinType,*LPPinType;

class CMediaTypeConstrain
{
public:

	CMediaTypeConstrain();

	/////////////////////////////////
	// Specific media types
	// Video
	typedef struct _tagVideoExtendInfo
	{
		UINT	Width;
		UINT	Height;
		UINT	BPP;
		UINT	FrameRate;   // frame per second
	}VideoExtendInfo,*LPVideoExtendInfo;
	// Audio
	typedef struct _tagWaveformExtendInfo
	{
		UINT	Channels;
		UINT	SampleRate;		//sample per second
		UINT	SampleDepth;	//bit per sample
	}WaveformExtendInfo,*LPWaveformExtendInfo;
	///////////////////////////////////////////////////

	UINT	FieldMask;
	enum
	{
		///////////////////////////////////////
		// Common requirment on media types
		MTC_CARE_FIXEDFRAMESIZE			=0x00000001,
		MTC_CARE_INTERFRAMECOMPRESSION	=0x00000002,
		MTC_CARE_FRAMESIZE				=0x00000004,
		MTC_CARE_MAJORTYPE				=0x00000008,
		MTC_CARE_SUBTYPE				=0x00000010,
		MTC_CARE_TYPEEXTENDINFO			=0x00000020,
		MTC_CARE_COMMON_MASK			=0x00000FFF,

		// Specific requirment on video types
		MTC_CARE_VIDEO_SIZE				=0x00001000,
		MTC_CARE_VIDEO_BPP				=0x00002000,
		MTC_CARE_VIDEO_FRAMERATE		=0x00004000,
		MTC_CARE_VIDEO_MASK				=0x000FF000,

		// Specific requirment on audio types
		MTC_CARE_WAVE_CHANNELS			=0x00100000,
		MTC_CARE_WAVE_SAMPLERATE		=0x00200000,
		MTC_CARE_WAVE_SAMPLEDEPTH		=0x00400000,
		MTC_CARE_WAVE_MASK				=0x00F00000,
	};

	BOOL		bFixedFrameSize;
	BOOL		bInterframeCompression;
	ULONG		iFrameSize;

	GUID		MajorType;
	GUID		SubType;

	//////  Extersion info for specific type of media
	DWORD		iExtendInfoType;

	union
	{
		VideoExtendInfo		VideoInfo;
		WaveformExtendInfo	WaveformInfo;
	};

	void SetAsRGB24VideoType(int cx = -1,int cy = -1,int fps = -1);
	void SetAsRGB32VideoType(int cx = -1,int cy = -1,int fps = -1);
	void SetAsYUV420VideoType(int cx = -1,int cy = -1,int fps = -1);
	void SetAsPCMAudioType(int SampleBits=-1,int chan=-1,int samplerate=-1);
	BOOL CheckConstrain(LPC_AM_MEDIA_TYPE	pMediaType);
	w32::MediaType		GetMediaTypeHint();
	w32::PinType		GetPinMediaTypeHint();
};


enum 
{
	RunState_Uninitialized = -2,
	RunState_Blocking = -1,
	RunState_Running = 0,
	RunState_Paused,
	RunState_Stopped
};


namespace dshow
{

typedef struct _tagPinInfo
{
	IPin*		pPin;
	LPWSTR		Name;
	BOOL		Connected;
	MediaType	MediaType;
}PinInfo,* LPPinInfo;


typedef struct _tagPinsSummary
{
public:
	DWORD	InputPinCount;
	DWORD	OutputPinCount;
	PinInfo	Pins[_AnyLength];	//[0,InputPinCount-1] is input pins
								//[InputPinCount,InputPinCount+OutputPinCount-1] is output ones

public:
	LPPinInfo GetInputPins(){ return &Pins[0]; }
	LPPinInfo GetOutputPins(){ return &Pins[InputPinCount]; }

	IPin*	GetPinByName(LPCTSTR Name);
	IPin*	GetPinByType(PinType Direction, PinType MediaType, PinType Connection = PT_DoNotCare);
	IPin*	GetPinByTypeEx(PinType Direction, PinType MediaType, PinType Connection = PT_DoNotCare);	//Recursively search pins 
			//Direction and MediaType can not be PT_DoNotCare. It search along downstream/upstream direction across filters for desired pin. 

}PinsSummary,*LPPinsSummary;


// - Filter Graph
IGraphBuilder * CreateFilterGraph(HRESULT * Ret = NULL);
DWORD			AddFilterGraphToRot(IUnknown *pUnkGraph, HRESULT * Ret=NULL);
void			RemoveObjectFromRot(DWORD pdwRegister);
IBaseFilter *	AddFileSourceToFilterGraph(IGraphBuilder *pGraph, LPCTSTR Fn, LPCTSTR FilterName = NULL, HRESULT * Ret=NULL);
HRESULT			ConnectFilters(IGraphBuilder *pGraph,IBaseFilter* pFilterFrom,IBaseFilter* pFilterTo,MediaType mt= MT_DoNotCare);
HRESULT			ConnectPinToFilter(IGraphBuilder *pGraph,IPin* pPinFrom,IBaseFilter* pFilterTo,MediaType mt= MT_DoNotCare);
IPin*			SearchPin(IBaseFilter* pFilter, PinType Direction, PinType MediaType, PinType Connection = PT_DoNotCare);
IPin*			SearchPinEx(IBaseFilter* pFilter, PinType Direction, PinType MediaType, PinType Connection = PT_DoNotCare); // recursively search the downstream/upstream filters
HRESULT			ConnectFileWriter(IGraphBuilder *pGraph,IBaseFilter* pFrom, LPCTSTR fn,IBaseFilter** pFileWriter = NULL);
HRESULT			ConnectFileWriter(IGraphBuilder *pGraph,IPin* pFrom, LPCTSTR fn,IBaseFilter** pFileWriter = NULL);

// - Filters
LPPinsSummary	QueryPinInformation(IBaseFilter * filter,OUT HRESULT * Ret = NULL); //Never use "delete" to free LPPinsSummary,use FreePinsSummary instead
void			FreePinsSummary(LPPinsSummary *lpp);

UINT			GetFilterName(IBaseFilter* pFilter,LPTSTR NameBuffer=NULL,int BufLen=0); //return length of the name not include terminator zero
UINT			GetFilterVendorInfo(IBaseFilter* pFilter,LPTSTR VendorBuffer=NULL,int BufLen=0); //return length of the name not include terminator zero
BOOL			IsFilterAdded(IBaseFilter* pFilter, IFilterGraph* pGraph);

IBaseFilter*	CreateFilter(IEnumMoniker* pEnumMoniker,LPCTSTR FriendName = NULL,BOOL StrictlyMatch = FALSE, HRESULT* Ret = NULL);

IBaseFilter*	CreateFilter(REFCLSID FilterCLSID,OUT HRESULT *Ret = NULL);
IBaseFilter*	CreateFilter(LPCTSTR FilterCLSID,OUT HRESULT *Ret = NULL);
IBaseFilter*	CreateFilterByPin(IPin*	pPin, BOOL PeerRequired = TRUE, HRESULT* Ret = NULL);

IEnumMoniker* 	EnumFilters(REFCLSID CategoryClsid, HRESULT* Ret = NULL);
void			FreeMediaType(AM_MEDIA_TYPE& mt);


class _DShowMedia
{
protected:
	IGraphBuilder*		pFilterGraph;
	IMediaControl*		pMediaControl;
	IMediaSeeking*		pMediaSeeking;
	IMediaEvent*		pStreamEvent;
	IVideoWindow*		pVideoWindow;
	IBasicAudio*		pAudioVolume;

	UINT				m_IncomingFrameCount;
	BOOL				IsEndOfStreamReached;

#ifdef _DEBUG
	DWORD	_DebugRotCookie;
#endif

public:
	_DShowMedia();
	~_DShowMedia();

	HRESULT Run();
	HRESULT Stop();
	HRESULT Pause();
	void	Destroy();
	inline void		AddFilterGraphToRot()
	{	
		#ifdef _DEBUG
		_DebugRotCookie = dshow::AddFilterGraphToRot(pFilterGraph);
		#endif
	}

	IVideoWindow*	GetVideoWindow(); // No need to addref/release if obtained.
	IBasicAudio*	GetAudioVolume();
	UINT			GetIncomingFrameCount()const{ return m_IncomingFrameCount; }
	HRESULT			WaitForEnding(UINT time_out = INFINITE); // S_OK = ended, S_FALSE = time out
	HRESULT			WaitForEnding_UI(); // S_OK = ended, S_FALSE = time out
	DWORD			GetState();
	BOOL			IsEndOFStream() const{ return IsEndOfStreamReached; }
	BOOL			IsInitialized() const{ return pFilterGraph!=NULL; }
};

} // namespace dshow


class CSyncMediaSource:public dshow::_DShowMedia, public dshow::ITerminatorFilterEventSink
{
protected:
	union
	{	struct
		{	int _video_info_width;
			int _video_info_height;
			int _video_info_bpp;
			REFERENCE_TIME _video_info_nanosec_perframe;
		};
		struct
		{	int _audio_info_channel;
			int _audio_info_bps;
			int _audio_info_sample_persecond;
		};
	};

	void clean_cache_info();

protected:
	void			RewindIfEOF();

public:
	CMediaTypeConstrain	m_MediaConstrain;

	HRESULT Create(IBaseFilter * pSourceFilter,IBaseFilter *RenderFilter=NULL,IGraphBuilder * pGraph=NULL);
	HRESULT CreateLiveSource(LPCTSTR DeviceFriendlyName = NULL,BOOL StrictlyMatch = TRUE);
	HRESULT CreateFileSource(LPCTSTR FileName);
	HRESULT CreateAdditionalSource(CSyncMediaSource& main_source,IBaseFilter *RenderFilter = NULL);

	HRESULT	Rewind();
	void	Destroy();
	DWORD	GetMajorMediaType() const { return m_MajorMediaType; } //MTEI_FORMAT_NONE indicates stream is not ready

	//Call before initializing, affect on next creation of Filter Graph
	void	EnablePreview( BOOL enable = TRUE ){ bCreateWithPreview = enable; }

	CSyncMediaSource();
	~CSyncMediaSource();

	LONGLONG		GetDuration(UINT time_fmt = MTU_100NANO);	// -1 for unknown
	UINT			GetVideoWidth()const{ return _video_info_width; }
	UINT			GetVideoHeight()const{ return _video_info_height; }
	UINT			GetVideoBPP()const{ return _video_info_bpp; }
	double			GetVideoFrameRate()const{ return 10000.0/(_video_info_nanosec_perframe/1000); }

	UINT			GetAudioChannel()const{ return _audio_info_channel; }
	UINT			GetAudioBPS()const{ return _audio_info_bps; }
	UINT			GetAudioSampleRate()const{ return _audio_info_sample_persecond; }

protected:
	dshow::ITerminatorExtension*	pFilterExtension;

	IBaseFilter*		pTerminator;
	DWORD				m_MajorMediaType;

	IBaseFilter*		pSourceFilter;
	void				ReleaseAllObjects();

	BOOL				bCreateWithPreview;

	dshow::LPMediaFrame	m_MediaBuffer;

	HRESULT STDMETHODCALLTYPE	IncomingFrame();

protected:
	///  Methods to be override by sub class
	virtual HRESULT	OnNewMediaFormat(const AM_MEDIA_TYPE* pMediaType);
	virtual HRESULT OnNewVideoFormat(int Width,int Height,int BPP,double Fps){ return S_FALSE; } // Called by OnNewMediaFormat
	virtual HRESULT OnNewAudioFormat(int nChannel,int BPS,DWORD SamPerSec_Hz){ return S_FALSE; } // Called by OnNewMediaFormat
	virtual void	OnIncomingFrame(){};

	HRESULT STDMETHODCALLTYPE	CheckMediaType(LPC_AM_MEDIA_TYPE pMediaType);
	HRESULT STDMETHODCALLTYPE	StreamTerminated();

private:
	HRESULT	_PostCreation(IBaseFilter *RenderFilter=NULL);
};

namespace dshow
{

struct VideoFrame:public dshow::MediaFrame
{
	int		Width;
	int		Height;
	int		BPP;
};
typedef VideoFrame* LPVideoFrame;
typedef const VideoFrame* LPCVideoFrame;

struct AudioFrame:public dshow::MediaFrame
{
	int		Channel;
	int		BitsPerSample;
};
typedef AudioFrame* LPAudioFrame;
typedef const AudioFrame* LPCAudioFrame;

struct MediaFrameEx
{	DWORD			MediaType;
	union
	{	dshow::MediaFrame	Media;
		VideoFrame	Video;
		AudioFrame	Audio;
	};
};
typedef MediaFrameEx* LPMediaFrameEx;
typedef const MediaFrameEx* LPCMediaFrameEx;

} // namespace dshow

/////////////////////////////////////////////////////
// This class is thread-safe for ProcessNewFrame/ProcessFinished
// But only two thread is supported. Don't make more than one thread
// to call ProcessNewFrame/ProcessFinished competitively.
class CAsyncMediaSource:public CSyncMediaSource
{
public:
	CAsyncMediaSource();
	~CAsyncMediaSource();
	dshow::LPCVideoFrame		ProcessNewVideoFrame();
	dshow::LPCAudioFrame		ProcessNewAudioFrame();
	dshow::LPCMediaFrameEx		ProcessNewFrame();	//Client DO NOT release any memory given out here
	void				ProcessFinished();

	HRESULT				Run();
	HRESULT				Rewind();
	HRESULT				Stop();
	UINT				GetHandledFrameCount()const{ return m_HandledFrameCount; }

protected:
	virtual HRESULT OnNewVideoFormat(int Width,int Height,int BPP,double Fps);
	virtual HRESULT OnNewAudioFormat(int nChannel,int BPS,DWORD SamPerSec_Hz);
	virtual void	OnIncomingFrame();

	UINT				m_HandledFrameCount;
	dshow::MediaFrameEx	m_FrameBuffers[3];
	BOOL				m_IsMediaStreamMounted;

	w32::CCriticalSection syncBufferSwapping;
	w32::CCriticalSection syncOutputBufferInuse;

	dshow::LPMediaFrameEx	m_pOutputBuffer;
	dshow::LPMediaFrameEx	m_pReadyBuffer;
	dshow::LPMediaFrameEx	m_pBackBuffer;

	w32::CEvent		syncFrameBufferRenewed;

	__forceinline	void SwapReadyBackBuffer()
	{
		EnterCCSBlock(syncBufferSwapping);

		dshow::LPMediaFrameEx tmp;
		tmp = m_pBackBuffer;
		m_pBackBuffer = m_pReadyBuffer;
		m_pReadyBuffer = tmp;
	}

	void	ClearBufferTimestamps();
};


class CSyncMediaRender: public dshow::_DShowMedia, public dshow::ISourceFilterEventSink
{
protected:
	dshow::ISourceExtension*	pExtension;
	IBaseFilter*		pSource;
	dshow::LPMediaFrame	pMediaFrame;
	REFERENCE_TIME		m_TimePerFrame;

protected:
	virtual HRESULT STDMETHODCALLTYPE	FillFrame();
	void	ReleaseAllObjects();
	void	AdvanceMediaTime();

public:
	CSyncMediaRender();
	~CSyncMediaRender();
	virtual HRESULT Create(LPC_AM_MEDIA_TYPE pMediaType);
	HRESULT CreateForRGBVideoType(int cx,int cy,double fps);
	HRESULT CreateForYUV420VideoType(int cx,int cy,double fps);
	HRESULT CreateForPCMAudioType(int nChannel,int BPS,int samplerate, UINT frame_size = 4096);
	void Destroy();
};


class CAsyncMediaRender: public CSyncMediaRender
{
protected:
	w32::CCriticalSection	m_FrameSwitchCCS;
	rt::Buffer<BYTE>		m_FrameData;
	LPBYTE					m_pFrontFrame;
	LPBYTE					m_pBackFrame;
	BOOL					m_bBackUpdated;
	UINT					m_FrameDataSize;

	BOOL	SetupBuffers(UINT sz);
	virtual HRESULT STDMETHODCALLTYPE	FillFrame();

public:
	CAsyncMediaRender();
	HRESULT Create(LPC_AM_MEDIA_TYPE pMediaType);
	void	AppendFrame(LPCVOID pData);
	UINT	GetFrameDataSize() const { return m_FrameDataSize; }
	void	Destroy();
};


} // namespace w32

