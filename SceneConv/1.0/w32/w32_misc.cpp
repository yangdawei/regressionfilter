#include "stdafx.h"
#include "..\rt\runtime_base.h"
#include "w32_basic.h"
#include "debug_log.h"
#include "w32_misc.h"
#include <limits.h>
#include <commdlg.h>
#include <Dlgs.h>
#include <shlobj.h>
#include <atlbase.h>
#include <Pdh.h>
#include <MMSystem.h>
#include <comutil.h>
#include <Psapi.h>
#include <WinSvc.h>

#pragma comment(lib,"Pdh.lib")
#pragma comment(lib,"psapi.lib")


////////////////////////////////
// Cmdline parser
w32::CCommandLine::CCommandLine()
{
	m_OptionCount = NULL;
	m_TextCount = NULL;
}

void w32::CCommandLine::Parse(int argc, TCHAR* argv[])	// for _tmain
{
	argc--;
	ASSERT(argc>=0);
	m_Arguments.SetSize(argc);

	Argument*  dumb;
	Argument** last_entry = &dumb;

	m_TextCount = 0;
	m_OptionCount = 0;
	for(UINT i=0;i<(UINT)argc;i++)
	{	
		if(argv[i+1][0] == _T('/') || argv[i+1][0] == _T('-'))
		{	// is option
			*last_entry = &m_Arguments[argc-1-m_OptionCount];
			m_Arguments[argc-1-m_OptionCount].Flag = ARG_OPTION | ((i==argc-1)?ARG_LASTONE:0);
			m_Arguments[argc-1-m_OptionCount].OptionText = &argv[i+1][1];

			_tcslwr(&argv[i+1][1]);
			last_entry = &m_Arguments[argc-1-m_OptionCount].pNextArg;
			m_OptionCount++;
		}
		else
		{	// is text
			*last_entry = &m_Arguments[m_TextCount];
			m_Arguments[m_TextCount].Flag = ((i==argc-1)?ARG_LASTONE:0);
			m_Arguments[m_TextCount].OptionText = argv[i+1];
			last_entry = &m_Arguments[m_TextCount].pNextArg;
			m_TextCount++;
		}
	}
	*last_entry = NULL;
}

void w32::CCommandLine::Parse(LPCTSTR pCmdLine)
{	
	::rt::String	cmdline(pCmdLine);
	LPTSTR pCmd = cmdline.GetBuffer();
	::rt::BufferEx<LPTSTR>	argv;
	argv.push_back((LPTSTR)NULL);

	BOOL is_in_quotation = FALSE;
	BOOL not_start = TRUE;
	while(*pCmd)
	{	
		switch(*pCmd)
		{
		case _T(' '):
			if(!is_in_quotation){ *pCmd = 0; not_start = TRUE; }
			break;
		case _T('\"'):
			if(is_in_quotation)
			{
				*pCmd = 0;
				is_in_quotation = FALSE;
			}
			else
			{
				is_in_quotation = TRUE;
			}
			break;
		default:
			if(!not_start){}
			else{ argv.push_back(pCmd); not_start = FALSE; }
		}

		pCmd++;
	}

	Parse((int)argv.GetSize(),argv);
}


LPCTSTR w32::CCommandLine::SearchOption(LPCTSTR option_name) const
{
	for(UINT i=0;i<m_OptionCount;i++)
	{	
		if(_tcscmp(option_name,m_Arguments[m_TextCount+i].OptionText)){}
		else
		{	if(	m_Arguments[m_TextCount+i].pNextArg && 
				!(m_Arguments[m_TextCount+i].pNextArg->Flag&ARG_OPTION) )
			{	return m_Arguments[m_TextCount+i].pNextArg->OptionText; }
			else
				return _T("");
		}
	}
	return NULL;
}

LPCTSTR w32::CCommandLine::SearchOptionEx(LPCTSTR option_substring) const
{	
	for(UINT i=0;i<m_OptionCount;i++)
	{	
		LPCTSTR p = _tcsstr(m_Arguments[m_TextCount+i].OptionText,option_substring);

		if(!p){}
		else
		{	return p + _tcslen(option_substring); }
	}
	return NULL;
}

////////////////////////////////////////////////
// Win32 Service
BOOL w32::CWin32Service::InstallService(LPCTSTR svc_name, LPCTSTR display_name, LPCTSTR desc, LPCTSTR arguments, LPCTSTR binpath, LPCTSTR  dependence)
{
	ASSERT(svc_name);

	UninstallService(svc_name);

	if(!display_name)display_name = svc_name;
	if(!desc)desc = svc_name;
	if(!dependence)dependence = _T("tcpip");

	TCHAR fn[MAX_PATH];
	if(!binpath)
	{
		if(!::GetModuleFileName(NULL, fn, sizeofArray(fn)))return FALSE;
		binpath = fn;
	}

	SC_HANDLE hSCM = NULL, hSvc = NULL;
	SERVICE_DESCRIPTION	svc_desc = { rt::_CastToNonconst(desc) };
	BOOL ret;

	ret = 	(hSCM = OpenSCManager(NULL,NULL,SC_MANAGER_CREATE_SERVICE)) &&
			(hSvc = CreateService(	hSCM,svc_name,
									display_name,
									SERVICE_ALL_ACCESS,
									SERVICE_WIN32_OWN_PROCESS|SERVICE_INTERACTIVE_PROCESS,
									SERVICE_AUTO_START,
									SERVICE_ERROR_NORMAL,
									rt::String_Ref(binpath) + _T(' ') + arguments,NULL,NULL,
									dependence,
									NULL,NULL)) &&
			ChangeServiceConfig2(hSvc,SERVICE_CONFIG_DESCRIPTION,&svc_desc);

	if(hSvc)::CloseServiceHandle(hSvc);
	if(hSCM)::CloseServiceHandle(hSCM);

	return ret;
}

BOOL w32::CWin32Service::UninstallService(LPCTSTR svc_name)
{
	SC_HANDLE hSCM = NULL, hSvc = NULL;
	BOOL ret;

	ret=(hSCM = OpenSCManager(NULL,NULL,SC_MANAGER_CONNECT)) &&
		(hSvc = OpenService(hSCM,svc_name,DELETE)) &&
		::DeleteService(hSvc);

	if(hSvc)::CloseServiceHandle(hSvc);
	if(hSCM)::CloseServiceHandle(hSCM);

	return ret;
}

w32::CWin32Service* w32::CWin32Service::_pWin32Service = NULL;

w32::CWin32Service::CWin32Service()
{
	ASSERT(_pWin32Service == NULL);
	_pWin32Service = this;
}

w32::CWin32Service::~CWin32Service()
{
	ASSERT(_pWin32Service);
	_pWin32Service = NULL;
}

BOOL w32::CWin32Service::InitializeService(LPCTSTR svc_name)
{
	ASSERT(m_hServiceHandler == 0);
	_ServiceName = svc_name;
	
	struct _Func
	{	
		static VOID WINAPI OnServiceManagerControl(DWORD fdwControl)
		{
			_pWin32Service->OnServiceControl(fdwControl);
		}
		static VOID WINAPI SvcMain(DWORD dwArgc,LPTSTR* lpszArgv)
		{
			_pWin32Service->m_hServiceHandler = ::RegisterServiceCtrlHandler(_pWin32Service->_ServiceName,OnServiceManagerControl);
		}
		static DWORD WINAPI Dispatcher(LPVOID)
		{
			SERVICE_TABLE_ENTRY svc_tab[] = 
			{	{(LPTSTR)_pWin32Service->_ServiceName,_Func::SvcMain},
				{NULL,NULL}
			};
			
			::StartServiceCtrlDispatcher(svc_tab);
			return 0;
		}
	};

	// start dispatch thread
	HANDLE h = ::CreateThread(NULL,0,_Func::Dispatcher,NULL,0,NULL);

	Sleep(10);
	while(m_hServiceHandler == 0)
	{	
		if(w32::WaitForThreadEnding(h,200))
		{	//ASSERT(0); // StartServiceCtrlDispatcher failed
			break;
		}
	}
	Sleep(0);

	::CloseHandle(h);
	ReportServiceStatus(SERVICE_START_PENDING);

	return m_hServiceHandler != 0;
}

void w32::CWin32Service::ReportServiceStatus(DWORD state)
{
	if(m_hServiceHandler)
	{	
		SERVICE_STATUS SvcStatus;

		static DWORD dwCheckPoint = 1;

		SvcStatus.dwServiceType = SERVICE_WIN32_OWN_PROCESS|SERVICE_INTERACTIVE_PROCESS;
		SvcStatus.dwCurrentState = state;
		SvcStatus.dwWin32ExitCode = NO_ERROR;
		SvcStatus.dwServiceSpecificExitCode = NO_ERROR;
		SvcStatus.dwWaitHint = 0;

		switch(state)
		{
		case SERVICE_START_PENDING:
		case SERVICE_CONTINUE_PENDING:
		case SERVICE_PAUSE_PENDING:
		case SERVICE_STOPPED:
			SvcStatus.dwControlsAccepted = 0;
			break;
		case SERVICE_PAUSED:
		case SERVICE_RUNNING:
			SvcStatus.dwControlsAccepted = SERVICE_ACCEPT_STOP;//|SERVICE_ACCEPT_PAUSE_CONTINUE;
			break;
		default:
			SvcStatus.dwControlsAccepted = 0;
		}

		if ( (state == SERVICE_RUNNING) || (state == SERVICE_STOPPED) )
			SvcStatus.dwCheckPoint = 0;
		else
			SvcStatus.dwCheckPoint = dwCheckPoint++;

		VERIFY(::SetServiceStatus((SERVICE_STATUS_HANDLE)m_hServiceHandler,&SvcStatus));
	}
}


////////////////////////////////////////////////
// CResourceStream
w32::CResourceStream::CResourceStream(HINSTANCE hInst, LPCTSTR res_group, LPCTSTR res_name)
{
	HRSRC   res_info = ::FindResource(hInst,res_name,res_group);
	DWORD   sz = SizeofResource(hInst,res_info);
	HGLOBAL res = LoadResource(hInst,res_info);
	LPVOID  ptr = LockResource(res);

	HGLOBAL	h = GlobalAlloc(GMEM_MOVEABLE|GMEM_NODISCARD, sz );
	if(SUCCEEDED(CreateStreamOnHGlobal(h,TRUE,&pstream)))
	{
		memcpy(GlobalLock(h),LockResource(res),sz);
		UnlockResource(res);
		GlobalUnlock(h);
	}
	else pstream = NULL;

	FreeResource(res);
}


////////////////////////////////////////////////
// CClipboard
w32::CClipboard::CClipboard(){ ::OpenClipboard(NULL); }
w32::CClipboard::~CClipboard(){ ::CloseClipboard(); }

BOOL w32::CClipboard::Content_IsText()
{
	return IsClipboardFormatAvailable(CF_TEXT); 
}

BOOL w32::CClipboard::Content_IsFileList()
{
	return IsClipboardFormatAvailable(CF_HDROP); 
}

BOOL w32::CClipboard::Content_IsImage()
{
	return IsClipboardFormatAvailable(CF_DIB) || IsClipboardFormatAvailable(CF_BITMAP);	
}

BOOL w32::CClipboard::PutText(LPCTSTR string)
{	ASSERT(string);

	EmptyClipboard();
	int cch = (int)_tcslen(string);

	HGLOBAL hglbCopy = GlobalAlloc(GMEM_MOVEABLE, (cch + 1) * sizeof(TCHAR)); 
	if(hglbCopy == NULL)return FALSE;

	LPVOID lptstrCopy = GlobalLock(hglbCopy); 
	ASSERT(lptstrCopy);
	memcpy(lptstrCopy, string,(cch+1) * sizeof(TCHAR)); 
	GlobalUnlock(hglbCopy); 

#ifdef UNICODE
	return (BOOL)SetClipboardData(CF_UNICODETEXT, hglbCopy);
#else
	return (BOOL)SetClipboardData(CF_TEXT, hglbCopy);
#endif
}

LPCTSTR	w32::CClipboard::GetText()
{
#ifdef UNICODE
	HGLOBAL hmem = GetClipboardData(CF_UNICODETEXT);
#else
	HGLOBAL hmem = GetClipboardData(CF_TEXT);
#endif
	if( hmem )return (LPCTSTR)GlobalLock(hmem);
	return NULL;
}

BOOL w32::CClipboard::PutImage(HGLOBAL hDib)
{
	EmptyClipboard();
	return (BOOL)::SetClipboardData(CF_DIB, hDib);
}

HGLOBAL	w32::CClipboard::GetImage()
{
	return GetClipboardData(CF_DIB);
}

HDROP w32::CClipboard::GetFileList()
{
	return (HDROP)GetClipboardData(CF_HDROP);
}

////////////////////////////////////////////////////////
// CDIB
w32::CDIB::CDIB()
{
	m_hDIB = NULL;
	m_pBits = NULL;
	m_DataOffset = 0;
	m_pBitsAux = NULL;
}

void w32::CDIB::Destroy()
{	if(m_pBitsAux)_SafeDelArray(m_pBitsAux);
	if(m_pBits){ ::GlobalUnlock(m_hDIB); m_pBits=NULL; }
	if(m_hDIB){ ::GlobalFree(m_hDIB); m_hDIB = NULL; }
}

BOOL w32::CDIB::Create(UINT width, UINT height, UINT BPP)
{
	Destroy();

	BITMAPINFOHEADER	bi;
	bi.biSize			= sizeof(BITMAPINFOHEADER);
	bi.biWidth			= width;
	bi.biHeight 		= height;
	bi.biPlanes 		= 1;
	bi.biBitCount		= BPP;
	bi.biCompression	= BI_RGB;
	bi.biXPelsPerMeter	= 3780;	//96dpi
	bi.biYPelsPerMeter	= 3780;
	bi.biClrUsed		= 0;
	bi.biClrImportant	= 0;

	int step = width*BPP/8;
	if(step & 3){ step += 4 - (step&3); }

	bi.biSizeImage = step*height;

	m_hDIB = GlobalAlloc(GMEM_FIXED,sizeof(bi)+bi.biSizeImage+((BPP==8)?sizeof(RGBQUAD)*256:0));
	if(m_hDIB)
	{
		m_pBits = (LPBYTE)::GlobalLock(m_hDIB);
		if(m_pBits)
		{	LPBYTE pData = m_pBits;
			memcpy(pData,&bi,sizeof(bi));
			pData+=sizeof(bi);

			if( BPP==8 ) //add color table
			{	RGBQUAD ct[256];
				DWORD co=0;
				for(int i=0;i<256;i++,co+=0x10101)
					*((DWORD*)&ct[i])=co;
				memcpy(pData,ct,sizeof(RGBQUAD)*256);
				pData+=sizeof(RGBQUAD)*256;
			}
			m_DataOffset = (UINT)(pData-m_pBits);
			return TRUE;
		}
	}
	return FALSE;
}

void w32::CDIB::GetFormat(UINT* width, UINT* height, UINT* BPP)
{	ASSERT(m_hDIB);
	if(m_pBits){}else{ m_pBits = (LPBYTE)::GlobalLock(m_hDIB); }
	if(m_pBits)
	{	UINT size = (UINT)::GlobalSize(m_hDIB);
		BITMAPINFOHEADER* header = (BITMAPINFOHEADER*)m_pBits;

		if(width)*width = header->biWidth;
		if(height)*height = header->biHeight;
		if(BPP)*BPP = header->biBitCount;

		if( header->biCompression == BI_BITFIELDS )
		{	ASSERT( header->biBitCount > 8 );
			header->biClrUsed = 3;
		}
		if(header->biBitCount > 8)
		{	int step = header->biWidth*(header->biBitCount/8);
			if(step & 3){ step += 4 - (step&3); }

			m_DataOffset = header->biSize + header->biClrUsed*sizeof(RGBQUAD);
		}
		else
		{	m_DataOffset = 0xffffffff;
			header->biClrUsed = 256;
			int step = header->biWidth*(header->biBitCount/8);
			if(step & 3){ step += 4 - (step&3); }

			int step_3c = header->biWidth*3;
			if(step_3c & 3){ step_3c += 4 - (step_3c&3); }

			m_pBitsAux = new BYTE[step_3c * header->biHeight];
			ASSERT(m_pBitsAux);

			RGBQUAD* pct = (RGBQUAD*)&m_pBits[header->biSize];
			LPBYTE pLine = (LPBYTE)&pct[header->biClrUsed];
			LPBYTE pLine_3c = m_pBitsAux;
			for(int y=0;y<header->biHeight;y++,pLine+=step,pLine_3c+=step_3c)
			{	const BYTE * pS = pLine;
				const BYTE * pEnd = &pLine[header->biWidth];
				LPBYTE pD = pLine_3c;
				for(;pS<pEnd;pS++,pD+=3)
				{	pD[2] = pct[pS[0]].rgbRed;
					pD[1] = pct[pS[0]].rgbGreen;
					pD[0] = pct[pS[0]].rgbBlue;
				}
			}
		}
	}
}

LPBYTE w32::CDIB::GetBits()
{	if(m_pBits){}else{ GetFormat(); }
	if(m_DataOffset != 0xffffffff)
		return &m_pBits[m_DataOffset];
	else
		return m_pBitsAux;
}

LPCBYTE w32::CDIB::GetBits() const
{	
	return ::rt::_CastToNonconst(this)->GetBits();
}

w32::CDIB& w32::CDIB::operator = (HGLOBAL hDIB)
{	Attach(hDIB);
	return *this;
}

void w32::CDIB::Attach(HGLOBAL hDIB)
{	Destroy();
	m_hDIB = hDIB;
}

HGLOBAL	w32::CDIB::Detach()
{	HGLOBAL ret = m_hDIB;
	m_hDIB = NULL;
	Destroy();
	return ret;
}

void w32::CDIB::VerticalFlip()
{	ASSERT(m_pBits);
	UINT w = ((BITMAPINFOHEADER*)m_pBits)->biWidth;
	UINT h = ((BITMAPINFOHEADER*)m_pBits)->biHeight;
	UINT c = ((BITMAPINFOHEADER*)m_pBits)->biBitCount/8;
	UINT step = w*c;
	if(step & 3){ step += 4 - (step&3); }

	LPBYTE L1 = GetBits();
	LPBYTE L2 = &L1[(h-1)*step];
	for(;L1<L2;L1+=step,L2-=step)
	{
		LPDWORD p = (LPDWORD)L1;
		LPDWORD end = (LPDWORD)(&L1[step]);
		LPDWORD p2 = (LPDWORD)L2;
		for(;p<end;p++,p2++)
			::rt::Swap(*p,*p2);
	}
}

void w32::CDIB::ChannelFlip()
{	ASSERT(m_pBits);
	UINT w = ((BITMAPINFOHEADER*)m_pBits)->biWidth;
	UINT h = ((BITMAPINFOHEADER*)m_pBits)->biHeight;
	UINT c = ((BITMAPINFOHEADER*)m_pBits)->biBitCount/8;

	if(c>1)
	{
		UINT step = w*c;
		if(step & 3){ step += 4 - (step&3); }

		LPBYTE L1 = GetBits();
		LPBYTE LEND = &L1[h*step];

		if(c==3)
		{
			for(;L1<LEND;L1+=step)
			{
				LPBYTE p = L1;
				LPBYTE end = &L1[step];
				for(;p<end;p+=3)
					::rt::Swap(p[0],p[2]);
			}
		}
		else if(c==4)
		{
			for(;L1<LEND;L1+=step)
			{
				LPBYTE p = L1;
				LPBYTE end = &L1[step];
				for(;p<end;p+=4)
					::rt::Swap(p[0],p[2]);
			}
		}
	}
}




//////////////////////////////////////////////////////
// CFileList
void w32::CFileList::EnableFileDropping(HWND win,BOOL enable)
{	::DragAcceptFiles(win,enable); 
}

HRESULT w32::CFileList::MultipleSelectionFileDialog(HWND parent_wnd, LPCTSTR pExtFilter)
{
	m_FilenameList.SetSize();

	rt::String	ExtFilter;
	if(pExtFilter && _tcschr(pExtFilter,_T('|'))!=NULL )
	{	ExtFilter = pExtFilter; // MFC stlye
		ExtFilter.Replace(_T('|'),_T('\0'));
		pExtFilter = ExtFilter;
	}

	rt::Buffer<TCHAR>	_DFOD_Fn;
	_DFOD_Fn.SetSize(10*1024*1024 + 1);	// 10MB buffer !!!
	_DFOD_Fn[0] = _T('\0');

	OPENFILENAME ofn;
	ZeroMemory(&ofn,sizeof(ofn)); 
	ofn.lStructSize = sizeof(OPENFILENAME);
	ofn.hwndOwner = parent_wnd;
	ofn.lpstrFilter = pExtFilter?pExtFilter:_T("All Files (*.*)\0*.*\0");
	ofn.nFilterIndex = 1;
	ofn.Flags = OFN_ENABLESIZING|OFN_OVERWRITEPROMPT|OFN_ALLOWMULTISELECT|OFN_EXPLORER|OFN_NOTESTFILECREATE|OFN_SHAREAWARE;
	ofn.lpstrFile = _DFOD_Fn;
	ofn.nMaxFile = (DWORD)(_DFOD_Fn.GetSize()-1);

	TCHAR _LastCurDir[MAX_PATH+1];
	::GetCurrentDirectory(MAX_PATH,_LastCurDir);

	BOOL ret = GetOpenFileName(&ofn);
	::SetCurrentDirectory(_LastCurDir);

	if(ret)
	{
		SetFileCount();
		int len = (int)_tcslen(_DFOD_Fn);
		LPCTSTR list = &_DFOD_Fn[len+1];

		if(list[0])
		{
			LPTSTR fn = &_DFOD_Fn[len+1];
			fn[-1] = _T('\\');
			while(list[0])
			{
				len = (int)_tcslen(list);
				memcpy(fn,list,(len+1)*sizeof(TCHAR));
				AppendFilename(_DFOD_Fn);
				list += len+1;
			}
		}
		else
		{	AppendFilename(_DFOD_Fn);
		}
		
		return S_OK;
	}
	
	return E_ABORT;
}

HRESULT w32::CFileList::ParseDropFileList(HDROP hDropInfo, BOOL NeedFirstFileOnly)
{	
	UINT FileCount = ::DragQueryFile(hDropInfo,0xffffffff,NULL,0);
	if(NeedFirstFileOnly)FileCount = min(1,FileCount);

	if(FileCount)
	{
		if(m_FilenameList.SetSize(FileCount))
		{
			for(UINT i=0;i<FileCount;i++)
			{
				UINT	Len;
				Len = ::DragQueryFile(hDropInfo,i,NULL,0);

				if(!m_FilenameList[i].SetLength(Len))return E_OUTOFMEMORY;
				if(!::DragQueryFile(hDropInfo,i,m_FilenameList[i].GetBuffer(),Len+1))
					m_FilenameList[i].Empty();
			}
		}
		else
		{	return E_OUTOFMEMORY;
		}

		return S_OK;
	}

	return w32::HresultFromLastError();
}

void w32::CFileList::SetFilename(UINT idx, LPCTSTR fn)
{
	ASSERT(fn);
	m_FilenameList[idx] = fn;
}

UINT w32::CFileList::AppendFilename(LPCTSTR fn)
{
	ASSERT(fn);
	if(m_pCallback && !m_pCallback->OnReflect((LPVOID)fn))
	{
		return INFINITE;
	}
	else
	{
		::rt::String& str = m_FilenameList.push_back();
		str = fn;
	}

	return (UINT)(m_FilenameList.GetSize()-1);
}

BOOL w32::CFileList::SetFileCount(UINT sz)
{
	return m_FilenameList.SetSize(sz);
}

HDROP w32::CFileList::GetDropHandle()	//return Clipborad format: CF_HDROP
{
	//counting buffer size
	UINT tot_sz = 0;
	for(UINT i=0;i<(UINT)m_FilenameList.GetSize();i++)
		tot_sz += (UINT)m_FilenameList[i].GetLength();

	HDROP hDrop = (HDROP)GlobalAlloc(GHND|GMEM_SHARE, 
									sizeof(TCHAR)*(tot_sz + m_FilenameList.GetSize() + 1) + sizeof(DROPFILES));
	if(hDrop)
	{
		DROPFILES* pDrop = (DROPFILES*)GlobalLock(hDrop);
		if(pDrop)
		{
		    pDrop->pFiles = sizeof(DROPFILES);
			pDrop->pt.x = pDrop->pt.y = 0;
			pDrop->fNC  = FALSE;
			#ifdef _UNICODE
			pDrop->fWide = TRUE;
			#endif

			LPTSTR pStrings = (LPTSTR)&pDrop[1];
			for(UINT i=0;i<m_FilenameList.GetSize();i++)
			{
				_tcscpy(pStrings,m_FilenameList[i]);
				pStrings += m_FilenameList[i].GetLength() + 1;
			}
			*pStrings = _T('\0');

			GlobalUnlock(hDrop);
			return hDrop;
		}

		GlobalFree(hDrop);
	}

	return NULL;
}

HRESULT w32::CFileList::LoadFileList(LPCTSTR fn)
{
	w32::CFileBuffer<BYTE>	file;
	if(file.SetSizeAs(fn))
	{
		rt::String	string;
		string.FromTextFile((LPCSTR)file.Begin());

		m_FilenameList.SetSize(0);
		LPCTSTR p = string;
		BOOL stop = FALSE;
		do
		{
			LPCTSTR t = p;
			while(*t != _T('\15') && *t != _T('\12') && *t!=0)t++;
			stop = *t==0;

			*((LPTSTR)t) = _T('\0');
			while(*p <=_T(' ') && *p!=0)p++;
		
			if(*p)
			{
				if(m_pCallback && !m_pCallback->OnReflect((LPVOID)p)){}
				else
					m_FilenameList.push_back() = p;
			}

			p = t+1;
			while((*p == _T('\15') || *p == _T('\12')) && *p!=0)p++;
		}while(!stop);

		return S_OK;
	}

	return HresultFromLastError();
}

HRESULT w32::CFileList::SaveFileList(LPCTSTR fn, BOOL UTF_8) const
{
	w32::CFile64	file;
	if(file.Open(fn))
	{
#ifdef UNICODE
		WORD utf16 = 0xfeff;
		file.Write(&utf16,2);
#else
		BYTE utf8[3] = {0xef,0xbb,0xbf};
		if(UTF_8)file.Write(utf8,3);
#endif
		TCHAR linebreak[2] = {_T('\15'),_T('\12')};
		for(UINT i=0;i<m_FilenameList.GetSize();i++)
		{
			file.Write(m_FilenameList[i].Begin(),(UINT)(m_FilenameList[i].GetLength()*sizeof(TCHAR)));
			file.Write(linebreak,sizeof(linebreak));
		}

		return S_OK;
	}

	return HresultFromLastError();
}


HRESULT w32::CFileList::ParsePathName(TCHAR hidden_Initial, LPCTSTR fn_templ, DWORD Flag, DWORD attrib_not_have, DWORD attrib_have)
{	
	rt::Buffer<TCHAR>	_buf;
	VERIFY(_buf.SetSize(1024*1024));

	LPTSTR		folder = _buf;
	LPTSTR		pFilename;
	rt::String	templ_suffix;

	rt::BufferEx<rt::String> templ_list;
	templ_list.push_back() = fn_templ;

	m_FilenameList.ChangeSize(0);

	while(templ_list.GetSize())
	{
		// setup prefix
		{	_tcscpy(folder,templ_list[0]);
			pFilename = _tcsrchr(folder,'\\');
			if(pFilename)
				pFilename++;
			else
				pFilename= folder;

			VERIFY(templ_suffix.SetLength((UINT)_tcslen(pFilename)+1));
			templ_suffix[0] = _T('\\');
			_tcscpy(&templ_suffix[1],pFilename);
		}
		// scan files
		{	WIN32_FIND_DATA fi;
			HANDLE findfile = ::FindFirstFile(templ_list[0],&fi);
			if(findfile!=INVALID_HANDLE_VALUE)
			{
				do
				{	if(	 (fi.dwFileAttributes & attrib_have) == attrib_have &&
						!(fi.dwFileAttributes & attrib_not_have) &&
						  fi.cFileName[0] != hidden_Initial &&
						!(  (fi.cFileName[0] == _T('.') && fi.cFileName[1] == _T('\0') ) 
						  ||(fi.cFileName[0] == _T('.') && fi.cFileName[2] == _T('\0') && fi.cFileName[1] == _T('.') )
					  )	 )
					{	// Add to filename list
						_tcscpy(pFilename,fi.cFileName);
						if(m_pCallback && !m_pCallback->OnReflect(folder)){}
						else
						{
							m_FilenameList.push_back() = folder;
							if((Flag&PATHNAME_FLAG_FIRST_FILE) == 0){}
							else
							{	::FindClose(findfile);
								return S_OK; 
							}
						}
					}
				}while(::FindNextFile(findfile,&fi));

				::FindClose(findfile);
			}
		}
		// scan directory
		if( Flag&PATHNAME_FLAG_RECURSIVE )
		{	_tcscpy(pFilename,_T("*.*"));
			WIN32_FIND_DATA fi;
			HANDLE findfile = ::FindFirstFile(folder,&fi);
			if(findfile!=INVALID_HANDLE_VALUE)
			{
				do
				{	if(	(fi.dwFileAttributes & FILE_ATTRIBUTE_DIRECTORY) &&
						fi.cFileName[0] != hidden_Initial &&
					   !(	(fi.cFileName[0] == _T('.') && fi.cFileName[1] == _T('\0') ) 
						  ||(fi.cFileName[0] == _T('.') && fi.cFileName[2] == _T('\0') && fi.cFileName[1] == _T('.') ) 
					  ) )
					{	// Add to templ_list
						_tcscpy(pFilename,fi.cFileName);
						_tcscat(pFilename,templ_suffix);
						templ_list.push_back() = folder;
					}
				}while(::FindNextFile(findfile,&fi));

				::FindClose(findfile);
			}
		}
		// pop
		templ_list.erase(0);
	}

	return m_FilenameList.GetSize()?S_OK:HRESULT_FROM_WIN32(ERROR_FILE_NOT_FOUND);
}

void w32::ShowMouseCursor()
{	int i = ::ShowCursor(TRUE);
	if(i>=0)return;
	else
	{	if(i==-1 && (::ShowCursor(TRUE)==-1))return; // in case mouse is not installed
		while(::ShowCursor(TRUE)<0);
	}	
}

void w32::HideMouseCursor()
{	int i = ::ShowCursor(FALSE);
	if(i<0)return;
	else
	{	if(i==-1 && (::ShowCursor(TRUE)==-1))return; // in case mouse is not installed
		while(::ShowCursor(FALSE)>=0);
	}
}


BOOL w32::WriteHtmlDocument(IHTMLDocument2* pDoc, LPCTSTR html)
{
	ASSERT(pDoc);
	HRESULT ret = E_FAIL;

	SAFEARRAY* psa = NULL;
	VARIANT *param = NULL;
	BSTR bstr = NULL;

	if(	(psa = ::SafeArrayCreateVector(VT_VARIANT, 0, 1)) &&
		SUCCEEDED(SafeArrayAccessData(psa, (LPVOID*)&param)) &&
		(bstr = SysAllocString(ATL::CT2W(html)))
	)
	{	param->vt = VT_BSTR;
		param->bstrVal = bstr;
		ret = pDoc->write(psa);
	}

	_SafeFreeBSTR(bstr);
	if(param)::SafeArrayUnaccessData(psa);
	if(psa)::SafeArrayDestroy(psa);

	return SUCCEEDED(ret);
}

IHTMLDocument2* w32::GetHtmlDocument(LPDISPATCH disp_doc)
{
	IHTMLDocument2* pDoc = NULL;
	disp_doc->QueryInterface(IID_IHTMLDocument2,(LPVOID*)&pDoc);
	return pDoc;
}

IHTMLElement* w32::SearchHtmlElement(IHTMLDocument2* pHTMLDoc, LPCWSTR id, LPCWSTR tag, UINT after_occuerrence)
{
	ATL::CComPtr<IHTMLElementCollection> pCollection;
	pHTMLDoc->get_all( &pCollection.p );
	

	long lItems = 0 ; 
	pCollection->get_length( &lItems );
	for ( long i = 0; i < lItems; ++i )
	{ 
		CComPtr<IDispatch> ppvDisp; 
		CComPtr<IHTMLElement> ppvElem;

		//_variant_t index = i;
		VARIANT index;
		index.lVal = i;
		index.vt = VT_I4;
		pCollection->item( index, index, &ppvDisp.p ); 
		ppvDisp->QueryInterface( IID_IHTMLElement, (void**)&ppvElem.p );

		if(tag)
		{
			BSTR str = NULL;
			ppvElem->get_tagName(&str);
			if(str==NULL || wcscmp(str, tag)!=0)
			{	_SafeFreeBSTR(str);
				continue;
			}
			else _SafeFreeBSTR(str);
		}

		if(id)
		{
			BSTR str = NULL;
			ppvElem->get_id(&str);
			if(str==NULL || wcscmp(str, id)!=0)
			{	_SafeFreeBSTR(str);
				continue;
			}
			else _SafeFreeBSTR(str);
		}

		if( after_occuerrence == 0 )
		{
			IHTMLElement* pElemOut = ppvElem;
			pElemOut->AddRef();
			return pElemOut; 
		}
		else after_occuerrence--;
	}

	return NULL;
}

BOOL w32::InvokeJavascript(IHTMLDocument2* doc, LPCWSTR func, LPCWSTR* args, int argc)
{
	ATL::CComPtr<IDispatch> pScript = NULL;
	ATL::CComBSTR bstrMember(func);
	DISPID dispid = NULL;

	if(	SUCCEEDED(doc->get_Script(&pScript)) &&
		SUCCEEDED(pScript->GetIDsOfNames(IID_NULL,&bstrMember,1,LOCALE_SYSTEM_DEFAULT,&dispid))
	)
	{	DISPPARAMS dispparams;
		memset(&dispparams, 0, sizeof dispparams);
		dispparams.cArgs = argc;
		dispparams.rgvarg = (VARIANT*)alloca(sizeof(VARIANT)*argc);
		dispparams.cNamedArgs = 0;

		for(int i = 0; i < argc; i++)
		{
			dispparams.rgvarg[i].bstrVal = ::SysAllocString(args[i]);
			dispparams.rgvarg[i].vt = VT_BSTR;
		}

		EXCEPINFO excepInfo;
		memset(&excepInfo, 0, sizeof excepInfo);
		CComVariant vaResult;
		UINT nArgErr = (UINT)-1;
		//Call JavaScript function
		HRESULT hr = pScript->Invoke(dispid,IID_NULL,0,DISPATCH_METHOD,&dispparams,&vaResult,&excepInfo,&nArgErr);
		
		for(int i = 0; i < argc; i++)
			::SysFreeString(dispparams.rgvarg[i].bstrVal);

		return SUCCEEDED(hr);
	}

	return FALSE;
}


BOOL w32::OpenDefaultBrowser(LPCTSTR url, DWORD show_window)
{
	ATL::CRegKey reg;
	reg.Open(HKEY_CLASSES_ROOT,_T("HTTP\\shell\\open\\command"),KEY_READ);
	TCHAR file[MAX_PATH+MAX_PATH];
	ULONG len = MAX_PATH+MAX_PATH;
	if(reg.QueryStringValue(NULL,file,&len)==ERROR_SUCCESS)
	{
		rt::String	str;
		VERIFY(str.SetLength((UINT)(_tcslen(url)+_tcslen(file)+MAX_PATH)));

		LPTSTR p = _tcsstr(file,_T("%1"));
		if(p)
		{	p[1] = _T('s');
			_stprintf(str.GetBuffer(),file,url);
		}
		else
		{	_tcscpy(str.GetBuffer(),file);
			_tcscat(str.GetBuffer(),_T(" "));
			_tcscat(str.GetBuffer(),url);
		}

		LPTSTR cmdline = str.GetBuffer();

		STARTUPINFO si;
		ZeroMemory(&si,sizeof(si));
		si.cb = sizeof(si);
		si.wShowWindow = (WORD)show_window;

		PROCESS_INFORMATION pi;
		if(::CreateProcess(NULL,cmdline,NULL,NULL,FALSE,0,NULL,NULL,&si,&pi))
		{
			::CloseHandle(pi.hProcess);
			::CloseHandle(pi.hThread);
			return TRUE;
		}
	}

	struct _func
	{	LPCTSTR url;
		DWORD	show_window;
		static DWORD WINAPI _call(LPVOID p)
		{	
			::ShellExecute(NULL,_T("open"),((_func*)p)->url,NULL,NULL,((_func*)p)->show_window);
			delete (_func*)p;
			return 0;
		};
	};

	_func& c = *new _func;
	c.url = url;
	c.show_window = show_window;

	::CloseHandle(::CreateThread(NULL,0,_func::_call,&c,0,NULL));
	return TRUE;
}

DWORD w32::FindProcessByBaseName(LPCTSTR binname)
{
	DWORD ids[1024];
	DWORD ret_byte = 0;
	if(::EnumProcesses(ids,sizeof(ids),&ret_byte))
	{
		rt::String bin(binname);
		bin.MakeUpper();

		int co = ret_byte/sizeof(DWORD);
		for(int i=0;i<co;i++)
		{
			TCHAR basename[MAX_PATH];
			HANDLE h = NULL;
			if(	(h = ::OpenProcess(PROCESS_QUERY_INFORMATION|PROCESS_VM_READ,FALSE,ids[i])) &&
				::GetModuleBaseName(h,NULL,basename,sizeofArray(basename))
			)
			{	_tcsupr(basename);
				if(bin == basename)
				{	::CloseHandle(h);
					return ids[i];
				}
			}
			if(h)::CloseHandle(h);
		}
	}

	return 0;
}

BOOL w32::KillProcess(DWORD process_id)
{
	HANDLE h = NULL;
	BOOL ret = (h = ::OpenProcess(PROCESS_TERMINATE,FALSE,process_id)) &&
				::TerminateProcess(h,INFINITE);
	if(h)::CloseHandle(h);

	return ret;
}


w32::CFullPathName::CFullPathName(LPCTSTR path)
{
	LPCTSTR p = _tcsrchr(path,_T('\\'));
	if(p)
	{	_path.SetLength((UINT)(p-path));
		memcpy(_path.Begin(),path,(_path.GetLength())*sizeof(TCHAR));
		_path[_path.GetLength()] = 0;
		p++;
	}
	else
	{	_path = _T(".\\");
		p = path;
	}

	_filename = p;
	
	LPCTSTR e = _tcsrchr((LPCTSTR)_filename,_T('.'));
	if(e)
	{	_title.SetLength((UINT)(e-_filename.Begin()));
		memcpy(_title.Begin(),p,(_title.GetLength())*sizeof(TCHAR));
		_title[_title.GetLength()] = 0;
		e++;
		_ext = e;
	}
	else
	{	_title = _filename;
		_ext = ::rt::String::EmptyString();
	}
}

w32::CDirectoryDialog::CDirectoryDialog(LPCTSTR caption_dialog, LPCTSTR caption_filename)
{
	_caption_dialog = caption_dialog?caption_dialog:_T("Select Folder");
	_caption_filename = caption_filename?caption_filename:_T("Folder:");

	m_Done = FALSE;
}

WNDPROC w32::CDirectoryDialog::g_DefaultFileDialogProc = NULL;

LRESULT CALLBACK w32::CDirectoryDialog::_WndProcMy(HWND hwnd,UINT message, WPARAM wParam, LPARAM lParam)
{
	ASSERT(g_DefaultFileDialogProc);
	if(message==WM_COMMAND && wParam==MAKEWPARAM(IDOK,BN_CLICKED))
		::SendMessage(::GetDlgItem(hwnd,0),WM_COMMAND,MAKEWPARAM(IDRETRY,BN_CLICKED),0);
	else if(message==WM_ACTIVATE && wParam==WA_INACTIVE)
		_OFNHOOKPROC(0,0,0,0); // cancel ok_clicked

	return g_DefaultFileDialogProc(hwnd,message,wParam,lParam);
}

UINT_PTR CALLBACK w32::CDirectoryDialog::_OFNHOOKPROC(HWND hwnd, UINT msg, WPARAM wparam, LPARAM lParam)
{	
	static BOOL ok_clicked = FALSE;
	if(hwnd==0){ ok_clicked = FALSE; return 0; }

	if(msg == WM_NOTIFY)
	{
		LPOFNOTIFY lpOfNotify = (LPOFNOTIFY) lParam;
		if(lpOfNotify)
		{	static const LPCTSTR fnsign = _T("#._#_439502");
			static TCHAR lastpath[PATH_BUFFER_SIZE];
			switch(lpOfNotify->hdr.code)
			{
			case CDN_FOLDERCHANGE:
				{	
					HWND Dlg = ::GetParent(hwnd);
					lpOfNotify->lpOFN->lpstrFile[0] = _T('\0');
					ASSERT(lpOfNotify->lpOFN);
					::SendMessage(Dlg,CDM_GETFOLDERPATH,PATH_BUFFER_SIZE,(LPARAM)lpOfNotify->lpOFN->lpstrFile);
					::EnableWindow(::GetDlgItem(Dlg,IDOK),lpOfNotify->lpOFN->lpstrFile[0]);
					::SendMessage(Dlg,CDM_SETCONTROLTEXT,edt1,(LPARAM)lpOfNotify->lpOFN->lpstrFile);

					if(ok_clicked && _tcscmp(lastpath,lpOfNotify->lpOFN->lpstrFile)==0)
					{
						::SendMessage(Dlg,CDM_SETCONTROLTEXT,edt1,(LPARAM)fnsign);
						::SendMessage(Dlg,WM_COMMAND,MAKEWPARAM(IDOK,BN_CLICKED),0);
					}

					_tcscpy(lastpath,lpOfNotify->lpOFN->lpstrFile);
					ok_clicked = FALSE;
				}
				break;
			case CDN_FILEOK:
				{
					LPTSTR p = _tcsrchr(lpOfNotify->lpOFN->lpstrFile,_T('\\'));
					if(p && (_tcscmp(p+1,fnsign)==0))
					{
						p[0] = _T('\0');
						return 0;
					}

					// reject it
					::FormatMessage(FORMAT_MESSAGE_FROM_SYSTEM,NULL,ERROR_PATH_NOT_FOUND,0,lpOfNotify->lpOFN->lpstrFile,PATH_BUFFER_SIZE,NULL);
					::MessageBox(hwnd,
								lpOfNotify->lpOFN->lpstrFile,
								((CDirectoryDialog*)lpOfNotify->lpOFN->lCustData)->_caption_dialog,
								MB_OK|MB_ICONINFORMATION);

					::SetWindowLong(hwnd,0,1); // DWL_MSGRESULT
					_tcscpy(lpOfNotify->lpOFN->lpstrFile,lastpath);
					::SendMessage(::GetParent(hwnd),CDM_SETCONTROLTEXT,edt1,(LPARAM)lpOfNotify->lpOFN->lpstrFile);

					return 1;
				}
				break;
			case CDN_INITDONE:
				{	HWND Dlg = ::GetParent(hwnd);
					// subclass File Dialog
#pragma warning(disable:4244 4312) //conversion from 'LONG_PTR' to 'LONG', possible loss of data
					g_DefaultFileDialogProc = (WNDPROC)::SetWindowLongPtr(Dlg,GWLP_WNDPROC,(LONG_PTR)_WndProcMy);
					::ShowWindow(::GetDlgItem(Dlg,stc2),SW_HIDE);
					::ShowWindow(::GetDlgItem(Dlg,cmb1),SW_HIDE);
					CDirectoryDialog* pDD = (CDirectoryDialog*)lpOfNotify->lpOFN->lCustData;
					::SetWindowText(Dlg,pDD->_caption_dialog);
					::SetWindowText(::GetDlgItem(Dlg,stc3),pDD->_caption_filename);
#pragma warning(default:4244 4312)
					break;
				}
			}
		}
	}
	else if(msg == WM_COMMAND && wparam==MAKEWPARAM(IDRETRY,BN_CLICKED))
	{
		ok_clicked = TRUE;
	}

	return FALSE;
}

LPTSTR w32::CDirectoryDialog::GetDirectoryName(HWND hWnd, BOOL for_open)
{	
	m_Path[0] = _T('\0');
	m_Done = FALSE;

	_OFNHOOKPROC(0,0,0,0);

	OPENFILENAME ofn;
	ZeroMemory(&ofn,sizeof(ofn)); 
	ofn.lStructSize = sizeof(OPENFILENAME);
	ofn.hwndOwner = hWnd; 
	ofn.lCustData = (LPARAM)this;
	ofn.lpstrFilter = _T("FakeExt\0*._#_#_#_#_#\0");
	ofn.nFilterIndex = 1;
	ofn.lpstrTitle = _T("");
	ofn.Flags = OFN_ENABLESIZING|OFN_OVERWRITEPROMPT|OFN_ENABLEHOOK|OFN_EXPLORER|OFN_SHAREAWARE|OFN_PATHMUSTEXIST;
	ofn.lpfnHook = _OFNHOOKPROC;
	ofn.lpstrFile = m_Path; 
	ofn.nMaxFile = PATH_BUFFER_SIZE;

	TCHAR _LastCurDir[MAX_PATH+1];
	VERIFY(::GetCurrentDirectory(MAX_PATH,_LastCurDir));
	m_Done = for_open?GetOpenFileName(&ofn):GetSaveFileName(&ofn);
	VERIFY(SetCurrentDirectory(_LastCurDir));

	return m_Done?(LPTSTR)*this:NULL;
}

w32::CDirectoryDialog::operator LPTSTR ()
{
	//return m_Done?m_Path:NULL;
	return m_Path;
}


////////////////////////////////////////////////////////////
// CLaunchProcess
w32::CLaunchProcess::CLaunchProcess()
{
	m_hProcess = INVALID_HANDLE_VALUE;
	_hOutputHookThread = INVALID_HANDLE_VALUE;
	hChildStdoutRdDup = INVALID_HANDLE_VALUE;
	hChildStdinRd = INVALID_HANDLE_VALUE;
	hChildStdinWrDup = INVALID_HANDLE_VALUE;
	hChildStdoutWr = INVALID_HANDLE_VALUE;

	m_ExitCode = 0;
}

w32::CLaunchProcess::~CLaunchProcess()
{
	IsRunning();
	ClearAll();
}

void w32::CLaunchProcess::ClearAll()
{	
	_SafeCloseHandle(m_hProcess);

	_SafeCloseHandle(hChildStdoutRdDup);	//make hook thread exit
	_SafeCloseHandle(hChildStdinRd);
	_SafeCloseHandle(hChildStdinWrDup);
	_SafeCloseHandle(hChildStdoutWr);

	if(_hOutputHookThread!=INVALID_HANDLE_VALUE)
	{
		if(!w32::WaitForThreadEnding(_hOutputHookThread,5000))
			::TerminateThread(_hOutputHookThread,0);
		_SafeCloseHandle(_hOutputHookThread);
	}
}

BOOL w32::CLaunchProcess::Launch(LPCTSTR cmdline, DWORD flag, DWORD window_show, LPCTSTR pWorkDirectory, LPVOID pEnvVariable)
{
	VERIFY(!IsRunning());

	::rt::String cmd(cmdline);

	_Flag = flag;
	BOOL hook_output = (FLAG_SAVE_OUTPUT&flag) || (FLAG_ROUTE_OUTPUT&flag);

	PROCESS_INFORMATION piProcInfo;
	STARTUPINFO siStartInfo;

	ZeroMemory( &piProcInfo, sizeof(PROCESS_INFORMATION) );
	ZeroMemory( &siStartInfo, sizeof(STARTUPINFO) );
	siStartInfo.cb = sizeof(STARTUPINFO);
	siStartInfo.wShowWindow = (WORD)window_show;
	siStartInfo.dwFlags = STARTF_USESHOWWINDOW;

	if(hook_output)
	{	HANDLE hChildStdoutRd,hChildStdinWr;
		/////////////////////////////////////////////////////////////////////////
		// Creating a Child Process with Redirected Input and Output
		// 1. create pipes
		SECURITY_ATTRIBUTES saAttr;
		BOOL fSuccess; 

		// Set the bInheritHandle flag so pipe handles are inherited. 
		saAttr.nLength = sizeof(SECURITY_ATTRIBUTES);
		saAttr.bInheritHandle = TRUE; 
		saAttr.lpSecurityDescriptor = NULL;

		// Create a pipe for the child process's STDOUT. 
		::CreatePipe(&hChildStdoutRd, &hChildStdoutWr, &saAttr, 0);

		// Create noninheritable read handle and close the inheritable read handle. 
		fSuccess = DuplicateHandle(	GetCurrentProcess(), hChildStdoutRd,
									GetCurrentProcess(),&hChildStdoutRdDup , 0,
									FALSE, DUPLICATE_SAME_ACCESS);
		CloseHandle(hChildStdoutRd);

		// Create a pipe for the child process's STDIN. 
		::CreatePipe(&hChildStdinRd, &hChildStdinWr, &saAttr, 0);

		// Duplicate the write handle to the pipe so it is not inherited. 
		fSuccess = DuplicateHandle(	GetCurrentProcess(), hChildStdinWr, 
									GetCurrentProcess(), &hChildStdinWrDup, 0, 
									FALSE, DUPLICATE_SAME_ACCESS); 
		CloseHandle(hChildStdinWr);

		///////////////////////////////////////////////////////
		// 2. create child process
		siStartInfo.hStdError = hChildStdoutWr;
		siStartInfo.hStdOutput = hChildStdoutWr;
		siStartInfo.hStdInput = hChildStdinRd;
		siStartInfo.dwFlags |= STARTF_USESTDHANDLES;

		m_hProcess = INVALID_HANDLE_VALUE;
		m_Output.SetLength();
		_hOutputHookThread = ::CreateThread(NULL,0,OutputHookThread,this,0,NULL);
		::SetThreadPriority(_hOutputHookThread,THREAD_PRIORITY_ABOVE_NORMAL);
	}

	m_ExitCode = STILL_ACTIVE;
	// Create the child process. 
	if(CreateProcess( NULL, 
					  cmd.GetBuffer(),			 // command line 
					  NULL,          // process security attributes 
					  NULL,          // primary thread security attributes 
					  TRUE,          // handles are inherited 
					  0,             // creation flags 
					  pEnvVariable,
					  pWorkDirectory,
					  &siStartInfo,  // STARTUPINFO pointer 
					  &piProcInfo)  // receives PROCESS_INFORMATION 
	)
	{	::SetPriorityClass(GetCurrentProcess(),ABOVE_NORMAL_PRIORITY_CLASS);

		m_hProcess = piProcInfo.hProcess;
		CloseHandle( piProcInfo.hThread );
		return TRUE;
	}
	else
	{
		ClearAll();
		return FALSE;
	}
}

LPCSTR w32::CLaunchProcess::GetOutput()
{
	return m_Output;
}

UINT w32::CLaunchProcess::GetOutputLen()
{
	return m_Output.GetLength();
}

BOOL w32::CLaunchProcess::WaitForEnding(DWORD timeout)
{
	w32::CTimeMeasure	t;
	t.Start();

	while(IsRunning())
	{	::Sleep(500);
		if((DWORD)t.Snapshot()>timeout)return FALSE;
	}

	return TRUE;
}

void w32::CLaunchProcess::Terminate()
{
	if(IsRunning())
	{
		::TerminateProcess(m_hProcess,-1);
		ClearAll();
	}
}

BOOL w32::CLaunchProcess::IsRunning()
{
	if(m_hProcess!=INVALID_HANDLE_VALUE)
	{
		BOOL exited = FALSE;
		if(_hOutputHookThread!=INVALID_HANDLE_VALUE)
		{
			DWORD exitcode;
			::GetExitCodeThread(_hOutputHookThread,&exitcode);
			exited = (exitcode!=STILL_ACTIVE);
			if(exited)
				VERIFY(::GetExitCodeProcess(m_hProcess,&m_ExitCode));
		}
		else
		{	VERIFY(::GetExitCodeProcess(m_hProcess,&m_ExitCode));
			exited = (m_ExitCode!=STILL_ACTIVE);
		}

		if(exited)
		{	
			FILETIME creat,exit,foo;
			GetProcessTimes(m_hProcess,&creat,&exit,&foo,&foo);
			m_ExitTime = exit;
			m_ExecutionTime = (UINT)((((ULONGLONG&)exit) - ((ULONGLONG&)creat))/10000);

			ClearAll();
			return FALSE;
		}
		return TRUE;
	}

	return FALSE;
}

void w32::CLaunchProcess::_HookedOutput(char* buffer, UINT dwRead)
{
	if(_Flag&FLAG_ROUTE_OUTPUT){ buffer[dwRead]=0; _CheckDump(buffer); }
	if(_Flag&FLAG_SAVE_OUTPUT)
	{	EnterCCSBlock(m_CCS);
		m_Output += rt::StringA_Ref(buffer, dwRead);
	}
}

DWORD WINAPI w32::CLaunchProcess::OutputHookThread(LPVOID p)
{
	CLaunchProcess* pThis = (CLaunchProcess*)p;

	char buffer[65];

	pThis->m_ExitCode = STILL_ACTIVE;

	DWORD dwRead,exitcode;
	exitcode = STILL_ACTIVE;
	while(exitcode==STILL_ACTIVE)
	{	
		if(PeekNamedPipe(pThis->hChildStdoutRdDup,NULL,0,NULL,&dwRead,0))
		{	if( dwRead )
			{	if( ReadFile( pThis->hChildStdoutRdDup, buffer, min(sizeof(buffer)-1,dwRead), &dwRead, NULL) && dwRead )
				{	pThis->_HookedOutput(buffer,dwRead);
					continue;
				}
			}
		}

		Sleep(200);
		if(pThis->hChildStdoutRdDup==INVALID_HANDLE_VALUE)return 0;
		if(pThis->m_hProcess)
			GetExitCodeProcess(pThis->m_hProcess,&exitcode);
	} 

	// check for words before death of the process
	do
	{	dwRead = 0;
		if(PeekNamedPipe(pThis->hChildStdoutRdDup,NULL,0,NULL,&dwRead,0))
		{	if( dwRead )
			{	if( ReadFile( pThis->hChildStdoutRdDup, buffer, min(sizeof(buffer)-1,dwRead), &dwRead, NULL) && dwRead )
					pThis->_HookedOutput(buffer,dwRead);
			}
		}
	}while(dwRead);

	pThis->m_ExitCode = exitcode;
	return 0;
}

w32::CPerformanceCounters::CPerformanceCounters(LPCTSTR pFullPathName)
{
	VERIFY(ERROR_SUCCESS == PdhOpenQuery(NULL,(DWORD_PTR)this,&_Query));
	AddCounter(pFullPathName);
	UpdateAll();
}

w32::CPerformanceCounters::CPerformanceCounters()
{
	VERIFY(ERROR_SUCCESS == PdhOpenQuery(NULL,(DWORD_PTR)this,&_Query));
}

w32::CPerformanceCounters::~CPerformanceCounters()
{
	PdhCloseQuery(_Query);
}

BOOL w32::CPerformanceCounters::AddCounter(LPCTSTR pFullPathName, LPCTSTR TextFormat, UINT Denominator)
{
	ASSERT(Denominator!=0);

	tagValType val_type = PCVT_I32;
	LPCTSTR friendname_end = NULL;

	if(TextFormat)
	{
		switch(TextFormat[1])
		{
		case 'B':	val_type = PCVT_DBL; break;
		case '6':	val_type = PCVT_I64; break;
		case '3':	val_type = PCVT_I32; break;
		default: return FALSE;
		}

		friendname_end = _tcschr(TextFormat+4,_T('|'));
		if(!friendname_end)return FALSE;
	}
	
	HANDLE counter = INVALID_HANDLE_VALUE;
	if(ERROR_SUCCESS == PdhAddCounter(_Query,pFullPathName,0,&counter))
	{
		t_Counter& n = _Counters.push_back();
		n.hCounter = counter;
		n.Denominator = Denominator;
		n.ValueType = val_type;
		if(TextFormat)
		{
			n.FriendName = rt::String_Ref(TextFormat + 4, friendname_end);
			n.TextFormat = friendname_end + 1;
		}
		else
		{
			n.FriendName = _T('[') + rt::String_Ref(pFullPathName) + _T(']');
			n.TextFormat = _T("%d");
		}

		return TRUE;
	}

	return FALSE;
}
	
BOOL w32::CPerformanceCounters::UpdateAll()
{
	return ERROR_SUCCESS == PdhCollectQueryData(_Query);
}

double w32::CPerformanceCounters::GetValue_DBL(UINT i)
{
	PDH_FMT_COUNTERVALUE val;
	val.doubleValue = 0;
	PdhGetFormattedCounterValue(_Counters[i].hCounter,PDH_FMT_DOUBLE,NULL,&val);
	if(_Counters[i].Denominator == 1)
		return val.doubleValue;
	else
		return val.doubleValue/_Counters[i].Denominator;
}

LONGLONG w32::CPerformanceCounters::GetValue_I64(UINT i)
{
	PDH_FMT_COUNTERVALUE val;
	val.largeValue = 0;
	PdhGetFormattedCounterValue(_Counters[i].hCounter,PDH_FMT_LARGE,NULL,&val);
	return val.largeValue/_Counters[i].Denominator;
}

LONG w32::CPerformanceCounters::GetValue_I32(UINT i)
{
	PDH_FMT_COUNTERVALUE val;
	val.longValue = 0;
	PdhGetFormattedCounterValue(_Counters[i].hCounter,PDH_FMT_LONG,NULL,&val);
	return val.longValue/_Counters[i].Denominator;
}

rt::String_Ref w32::CPerformanceCounters::GetValue_Text(UINT i)
{
	_TempText.SetLength(_Counters[i].TextFormat.GetLength() + 32);

	switch(_Counters[i].ValueType)
	{
	case PCVT_DBL:
		_stprintf_s(_TempText.GetBuffer(),_TempText.GetLength(),_Counters[i].TextFormat.Begin(),GetValue_DBL(i));
		break;
	case PCVT_I64:
		_stprintf_s(_TempText.GetBuffer(),_TempText.GetLength(),_Counters[i].TextFormat.Begin(),GetValue_I64(i));
		break;
	case PCVT_I32:
		_stprintf_s(_TempText.GetBuffer(),_TempText.GetLength(),_Counters[i].TextFormat.Begin(),GetValue_I32(i));
		break;
	default: ASSERT(0);
	}

	return _TempText;
}

namespace w32
{
	CGarbageCollection g_GarbageCollection;
	CGarbageCollection* CGarbageCollection::g_pDDC = NULL;
}

w32::CGarbageCollection::CGarbageCollection()
{
	ASSERT(g_pDDC == NULL);	// singleton
	g_pDDC = this;
	m_hDeletionThread = NULL;
}

w32::CGarbageCollection::~CGarbageCollection()
{
	if(m_hDeletionThread)
	{
		HANDLE h = m_hDeletionThread;
		m_hDeletionThread = NULL;
		w32::WaitForThreadEnding(h);

		::CloseHandle(h);
	}
}

void w32::CGarbageCollection::item::Delete()
{
	pDeletionFunction(pObj);
}

BOOL w32::CGarbageCollection::item::Tick()
{
	if(TTL)TTL--;
	return TTL;
}

void w32::CGarbageCollection::_DeletionThread()
{
	for(;;)
	{
		for(UINT i=0;i<5;i++)
		{
			::Sleep(200);
			if(m_hDeletionThread == NULL)goto DELETION_EXITING;
		}

		{	EnterCCSBlock(m_CCS);

			UINT open = 0;
			for(UINT i=0;i<m_PendingDeletion.GetSize();i++)
			{
				if(m_PendingDeletion[i].Tick())
				{
					m_PendingDeletion[open] = m_PendingDeletion[i];
					open++;
				}
				else
				{
					m_PendingDeletion[i].Delete();
				}
			}

			m_PendingDeletion.ChangeSize(open);
		}
	}

DELETION_EXITING:
	{	EnterCCSBlock(m_CCS);

		for(UINT i=0;i<m_PendingDeletion.GetSize();i++)
			m_PendingDeletion[i].Delete();

		m_PendingDeletion.SetSize();
	}
}


void w32::CGarbageCollection::DeleteObject(LPVOID x, DWORD TTL_sec, LPFUNC_DELETION delete_func)
{
	ASSERT(delete_func);

	if(x == NULL)return;
	if(TTL_sec == 0)
	{	delete_func(x);
		return;
	}

	{	EnterCCSBlock(m_CCS);

		if(m_hDeletionThread){}
		else
		{
			struct _call_thread
			{
				static DWORD WINAPI _fun(LPVOID p)
				{
					((CGarbageCollection*)p)->_DeletionThread();
					return 0;
				}
			};

			m_hDeletionThread = ::CreateThread(NULL,0,_call_thread::_fun,this,CREATE_SUSPENDED,NULL);
			ASSERT(m_hDeletionThread);
			::ResumeThread(m_hDeletionThread);
		}

#ifdef _DEBUG
		for(UINT i=0;i<m_PendingDeletion.GetSize();i++)
			ASSERT(m_PendingDeletion[i].pObj != x);
#endif

		item& n = m_PendingDeletion.push_back();
		n.pObj = x;
		n.pDeletionFunction = delete_func;
		n.TTL = TTL_sec;
	}
}



HRESULT w32::image_codec::_Write_PFM(LPCTSTR fn,LPCFLOAT pData,UINT ch,UINT w,UINT h,UINT step)
{	
	ASSERT(ch==1 || ch==3);
	CHAR Header[512];

	sprintf(Header,"P%c\x0a%d %d\x0a-1.000000\x0a",(ch==1)?'f':'F',w,h);

	w32::CFile64	file;
	if(file.Open(fn,w32::CFile64::Normal_Write))
	{
		// write header
		if(file.Write(Header,(UINT)strlen(Header)) == strlen(Header))
		{	LPCBYTE p=(LPBYTE)pData;
			p+=step*(h-1); //in reversed order, to compatible with HDRShop
			for(UINT y=0;y<h;y++,p-=step)
				file.Write(p,sizeof(float)*ch*w);

			if(!file.ErrorOccured())
				return S_OK;
		}
	}

	return w32::HresultFromLastError();
}

HRESULT w32::image_codec::_Open_PFM(LPCTSTR fn,_PFM_Header* pHeader)
{	
	ASSERT(pHeader);
	char Header[1025];

	if(pHeader->file.Open(fn))
	{
		DWORD len = pHeader->file.Read(Header,1024);
		Header[len] = 0;
		if(len>3)
		{	Header[len-1] = 0;
			if( Header[0] == 'P' && ( Header[1] == 'F' || Header[1] == 'f' ) )		// "PF" or "Pf"
			{	pHeader->ch = (Header[1] == 'F')?3:1;
				LPSTR p = strchr(Header,0xa);
				if(p)
				{	LPSTR end;
					p++;
					end = strchr(p,0xa);
					if(end)
					{	end[0] = 0;
						if(sscanf(p,"%d %d",&pHeader->width,&pHeader->height) == 2)	// width height
						{	
							p = &end[1];
							while(*p == '#')
							{	// skip comments, compatible to IST's Data
								end = strchr(p,0xa);
								p = &end[1];
							}
							end = strchr(p,0xa);									// "-1.0000"
							
							if(end)
							{	pHeader->file.Seek(((UINT)(end-Header))+1);
								if(!pHeader->file.ErrorOccured())
									return S_OK;
							}
						}
					}
				}
			}
		}
	}

	return w32::HresultFromLastError();
}

HRESULT w32::image_codec::_Read_PFM(_PFM_Header* pHeader,LPFLOAT pData,UINT ch,UINT step)
{
	if( ch==pHeader->ch )
	{	LPBYTE p=&((LPBYTE)pData)[step*(pHeader->height-1)];
		for(UINT y=0;y<pHeader->height;y++,p-=step) //in reversed order, to compatible with HDRShop
		{
			pHeader->file.Read(p,sizeof(float)*ch*pHeader->width);
		}
		if(!pHeader->file.ErrorOccured())
			return S_OK;
	}
	else
	{	
		if(ch==1 && pHeader->ch==3) //color->grayscale
		{	rt::Buffer<num::Vec3f> buf;
			buf.SetSize(pHeader->width);
			LPBYTE p = (LPBYTE)pData;
			p+=step*(pHeader->height-1); //in reversed order, to compatible with HDRShop
			for(UINT y=0;y<pHeader->height;y++,p-=step)
			{
				pHeader->file.Read(buf,sizeof(num::Vec3f)*pHeader->width);
				if(pHeader->file.ErrorOccured())return w32::HresultFromLastError();
				rt::Buffer<num::Vec1f>::Ref ref((num::Vec1f*)p,pHeader->width);
				ref = buf;
			}
			return S_OK;
		}
		else if(ch==3 && pHeader->ch==1) //grayscale->color
		{	rt::Buffer<num::Vec1f> buf;
			buf.SetSize(pHeader->width);
			LPBYTE p = (LPBYTE)pData;
			p+=step*(pHeader->height-1); //in reversed order, to compatible with HDRShop
			for(UINT y=0;y<pHeader->height;y++,p-=step)
			{
				pHeader->file.Read(buf,sizeof(num::Vec1f)*pHeader->width);
				if(pHeader->file.ErrorOccured())return w32::HresultFromLastError();
				rt::Buffer<num::Vec3f>::Ref ref((num::Vec3f*)p,pHeader->width);
				ref = buf;
			}
			return S_OK;
		}
		
	}

	return w32::HresultFromLastError();
}

