#include "stdafx.h"
#include "inet_svc.h"
#include "..\rt\string_type.h"
#include <mmsystem.h>
#include <atlconv.h>
#include <Psapi.h>
#pragma comment(lib, "Psapi.lib")
#include "w32_misc.h"
#include "debug_log.h"
#include "xml_soap.h"



namespace w32
{

static const LPCSTR g_HTTP_StatusCode[4][10] = 
{
	//200
	{	"OK",							//200
		"Created",						//201
		"Accepted",						//202
		"Non-Authoritative Information",//203
		"No Content",					//204
		"Reset Content",				//205
		"Partial Content",				//206
		NULL,NULL,NULL
	},
	//300
	{	"Multiple Choices",				//300
		"Moved Permanently",			//301
		"Found",						//302
		"See Other",					//303
		"Not Modified",					//304
		"Use Proxy",					//305
		"Switch Proxy",					//306
		"Temporary Redirect",			//307
		NULL,NULL
	},
	//400
	{	"Bad Request",					//400
		"Unauthorized",					//401
		"Payment Required",				//402
		"Forbidden",					//403
		"Not Found",					//404
		"Method Not Allowed",			//405
		"Not Acceptable",				//406
		"Proxy Authentication Required",//407
		"Request Timeout",				//408
		"Conflict "						//409 	
		//410 Gone 
		//411 Length Required 
		//412 Precondition Failed 
		//413 Request Entity Too Large 
		//414 Request-URI Too Long 
		//415 Unsupported Media Type 
		//416 Requested Range Not Satisfiable 
		//417 Expectation Failed 
		//418 I'm a teapot 
		//422 Unprocessable Entity (WebDAV) (RFC 4918 ) 
		//423 Locked (WebDAV) (RFC 4918 ) 
		//424 Failed Dependency (WebDAV) (RFC 4918 ) 
		//425 Unordered Collection 
		//426 Upgrade Required (RFC 2817 ) 
		//449 Retry With 
	},
	//500
	{	"Internal Server Error",		//500
		"Not Implemented",				//501
		"Bad Gateway",					//502
		"Service Unavailable",			//503
		"Gateway Timeout",				//504
		"HTTP Version Not Supported",	//505
		"Variant Also Negotiates",		//506
		"Insufficient Storage",			//507
		NULL,							//508
		"Bandwidth Limit Exceeded",		//509
	},
};



namespace inet
{

void Connection::Clear()
{
	m_RecvBufferUsed = 0;
	ZeroMemory(&m_Overlapped,sizeof(m_Overlapped));
	ZeroMemory(&m_SendingOverlapped,sizeof(m_SendingOverlapped));
}

void Connection::ClearOverlappedSending()
{
	ZeroMemory(&m_OverlappedSendingChunk,sizeof(m_OverlappedSendingChunk));
	m_OverlappedSendingChunkCount = 0;		
	m_OverlappedSendingChunkSent = 0;		
	m_OverlappedSentInChunk = 0;			
}

void Connection::SetTimeToLive(DWORD msec)
{
	ActiveTick();
	m_TimeToLive = msec;
}

void Connection::DestroyConnection()
{
	m_pServ = NULL;
	m_TimeToLive = SocketServCore::_default_zombie_session_TTL;
	m_Socket.Close();
	m_RecvBufferUsed = 0;
	m_OverlapSendingBufferUsed = 0;
}

void Connection::NormalizeBuffers()
{
	if(SocketServCore::_favorable_recieving_buffer_size < m_RecvBuffer.GetReservedSize())
		m_RecvBuffer.SetSize(0);

	if(SocketServCore::_favorable_sending_buffer_size < m_OverlapSendingBuffer.GetReservedSize())
		m_OverlapSendingBuffer.SetSize(0);
}

void Connection::RemoveData(UINT size)
{
	if(m_RecvBufferUsed)	// just in case that the connection has been cleared
	{
		ASSERT(size <= m_RecvBufferUsed);
		if(m_RecvBufferUsed == size)
		{
			m_RecvBufferUsed = 0;
		}
		else
		{	m_RecvBufferUsed -= size;
			memcpy(m_RecvBuffer,&m_RecvBuffer[size],m_RecvBufferUsed);
		}
	}
	//else
	//{	ASSERT(size == 0);
	//}
}

HRESULT	Connection::ReceiveNextData()
{
	// make sure buffer is large enough
	if(m_RecvBufferUsed + SocketServCore::_receiving_chuck_size < m_RecvBuffer.GetSize()){}
	else
		m_RecvBuffer.ChangeSize(m_RecvBufferUsed + SocketServCore::_receiving_chuck_size + 1);

	// initiate the data receiving
	WSABUF _buf =	{	(ULONG)(m_RecvBuffer.GetSize() - m_RecvBufferUsed), 
						(CHAR*)&m_RecvBuffer[m_RecvBufferUsed]
					};
	DWORD  _flag = 0;	
	int ret = WSARecv(m_Socket,&_buf,1,NULL,&_flag,&m_Overlapped,NULL);
	if(	((SOCKET_ERROR == ret) && WSA_IO_PENDING == WSAGetLastError()) || ret==0 )
	{
		return S_OK;
	}

	return E_FAIL;
}

HRESULT	Connection::SendNextData()
{
	WSABUF	chunks[_overlapped_sending_chunk_max];
	int		chunk_co = 0;
	int		tot_sent = 0;

	for(;m_OverlappedSendingChunkSent < m_OverlappedSendingChunkCount;m_OverlappedSendingChunkSent++)
	{
		chunks[chunk_co].buf = m_OverlappedSendingChunk[m_OverlappedSendingChunkSent].buf + m_OverlappedSentInChunk;
		int remain_in_this_chunk = m_OverlappedSendingChunk[m_OverlappedSendingChunkSent].len - m_OverlappedSentInChunk;
		if(tot_sent + remain_in_this_chunk <= SocketServCore::_sending_chuck_size)
		{
			chunks[chunk_co].len = remain_in_this_chunk;
			tot_sent += remain_in_this_chunk;
			m_OverlappedSentInChunk = 0;
			chunk_co++;
			if(tot_sent == SocketServCore::_sending_chuck_size)
			{
				m_OverlappedSendingChunkSent++;
				break;
			}
		}
		else
		{	
			ASSERT(SocketServCore::_sending_chuck_size > tot_sent);
			chunks[chunk_co].len = SocketServCore::_sending_chuck_size - tot_sent;
			m_OverlappedSentInChunk += chunks[chunk_co].len;

			chunk_co++;
			break;
		}
	}

	if(!chunk_co)return S_FALSE;

	DWORD  sent = 0;
	int ret = ::WSASend(m_Socket,chunks,chunk_co,&sent,0,&m_SendingOverlapped,NULL);
	if(((SOCKET_ERROR == ret) && WSA_IO_PENDING == WSAGetLastError()) || ret==0 )
	{	
		return S_OK;
	}
	else
	{	ClearOverlappedSending();
		return E_FAIL;
	}
}

SocketServCore::_Listener::_Listener()
{
	pServCore = NULL;
	ConnectionsUsed = 0;
	AcceptingThread = INVALID_HANDLE_VALUE;
}

void SocketServCore::_Listener::DumpConnection()
{
	_CheckDump("========= connection state of ["<<LocalAddress<<"] =========\n");

	DWORD tick = GetTickCount();
	UINT zombie = 0;
	for(UINT i=0;i<ConnectionsUsed;i++)
	{
		if(!Connections[i]->IsEmpty())
		{
			_CheckDump(	Connections[i]->m_RemoteAddress<<
						" is running, TTL = "<<(int)Connections[i]->m_TimeToLive - (int)Connections[i]->GetIdleTime(tick)<<"\n");
		}
		else
		{
			zombie++;
		}
	}
	_CheckDump("======= total "<<ConnectionsUsed<<" connections ("<<zombie<<" zombie connections) =============\n\n");
}

void SocketServCore::_Listener::GarbageConnectionRemoval()
{
	DWORD tick = GetTickCount();

	if(	(tick-LastGarbageProbeTick) > SocketServCore::_garbage_session_probe_interval || 
		ConnectionsUsed == Connections.GetSize() 
	)
	{
		LastGarbageProbeTick = tick;

		int empty = 0;
		int last_inuse = ((int)ConnectionsUsed) - 1;

		for(;;)
		{
			for(;empty<=last_inuse;empty++)
				if(!Connections[empty]->IsEmpty())
				{	
					if(Connections[empty]->GetIdleTime(tick) > Connections[empty]->m_TimeToLive + _default_session_TTL_removal_addon)
					{
						VERIFY(::PostQueuedCompletionStatus(pServCore->m_hCompletePort,0,(ULONG_PTR)Connections[empty],NULL));
#ifdef ENABLE_SERV_STATISTIC
						_InterlockedIncrement(&pServCore->stat_ConnectionTimeout);
#endif
					}
				}
				else goto AN_EMPTY_FOUND;
			break;

AN_EMPTY_FOUND:
			Connections[empty]->NormalizeBuffers(); // release the buffers if they are too large
			for(;last_inuse>empty;last_inuse--)
				if(!Connections[last_inuse]->IsEmpty())
					goto AN_USING_FOUND;

			last_inuse = empty - 1;
			break;

AN_USING_FOUND:
			rt::Swap(Connections[empty],Connections[last_inuse]);
			// empty ++; // don't move empty forward, the TTL of this entry need to be checked
			last_inuse --;
		}

		ASSERT(last_inuse >= -1);
		ConnectionsUsed = last_inuse+1;
	}
}

SocketServCore::SocketServCore()
{	
	m_hCompletePort = INVALID_HANDLE_VALUE;
	m_ServerState = SERVER_UNINITIALIZED;
	m_pPooledDataTransferThreadFunc = NULL;
	m_pConnectionManagerThreadFunc = NULL;
	m_ConnectionPoolSize = _default_concurrent_session_max;

	struct _func
	{	static void _DC(LPVOID x){ ((inet::Connection*)x)->DestroyConnection(); }
	};
	_DestroyConnectionFun = _func::_DC;

#ifdef ENABLE_SERV_STATISTIC
	stat_SentByte = stat_RecvByte = 0;
	stat_SentKiloByte = stat_RecvKiloByte = 0;
	stat_Connected = 0;
	stat_ConnectionRefused = 0;
	stat_ConnectionFake = 0;
	stat_ConnectionTimeout = 0;
#endif
}

SocketServCore::~SocketServCore()
{
	Uninitialize();
}

UINT SocketServCore::GetTotalConnectionCount() const
{
	UINT session_co = 0;
	for(UINT i=0;i<m_Listeners.GetSize();i++)
	{
		session_co += m_Listeners[i].ConnectionsUsed;
	}

	return session_co;
}

UINT SocketServCore::GetConnectionCount(UINT listern_index) const
{
	return m_Listeners[listern_index].ConnectionsUsed;
}

UINT SocketServCore::GetConnectionNumberLimit() const
{
	if(m_Listeners.GetSize())
		return (UINT)m_Listeners[0].Connections.GetSize();
	else
		return 0;
}

UINT SocketServCore::GetThreadCount() const
{
	return (UINT)m_hPooledThread.GetSize();
}

UINT SocketServCore::GetListenerCount() const
{
	return (UINT)m_Listeners.GetSize();
}

BOOL SocketServCore::Initialize(const CInetAddr* pAddr, UINT addr_count, UINT thread_count)
{
	ASSERT(m_ServerState == SERVER_UNINITIALIZED);

	ASSERT(m_hCompletePort == INVALID_HANDLE_VALUE);
	ASSERT(m_Listeners.GetSize()==0);

	if(thread_count==0)
		thread_count = w32::NumberOfProcessor() * 2;

	m_hCompletePort = CreateIoCompletionPort(INVALID_HANDLE_VALUE, NULL, 0, thread_count);
	VERIFY(m_hCompletePort != INVALID_HANDLE_VALUE);

	VERIFY(m_Listeners.SetSize(addr_count));
	for(UINT i=0;i<addr_count;i++)
	{
		m_Listeners[i].LocalAddress = pAddr[i];
		m_Listeners[i].pServCore = this;
	}

	VERIFY(m_hPooledThread.SetSize(thread_count));
	VERIFY(m_hPooledThreadId.SetSize(thread_count));
	for(UINT i=0;i<m_hPooledThread.GetSize();i++)
	{
		m_hPooledThread[i] = INVALID_HANDLE_VALUE;
		m_hPooledThreadId[i] = 0;
	}

	m_ServerState = SERVER_STOPPED;

	return TRUE;
}

void SocketServCore::Uninitialize()
{
	switch(m_ServerState)
	{
	case SERVER_RUN:
	case SERVER_PAUSE:
		VERIFY(Stop());
	case SERVER_STOPPED:
		::CloseHandle(m_hCompletePort);
		m_hCompletePort = INVALID_HANDLE_VALUE;

		m_Listeners.SetSize();
		m_hPooledThread.SetSize();
		m_hPooledThreadId.SetSize();
		m_ServerState = SERVER_UNINITIALIZED;
	case SERVER_UNINITIALIZED:
		break;
	default:ASSERT(0);
	}
}

BOOL SocketServCore::Start()
{
	ASSERT(m_ServerState == SERVER_PAUSE || m_ServerState == SERVER_STOPPED);

	if(m_ServerState == SERVER_STOPPED)
	{
		BOOL ret = FALSE;
		m_ServerState = SERVER_RUN;

		ASSERT(m_pConnectionManagerThreadFunc); // setup transfer thread before start
		for(UINT i=0;i<m_Listeners.GetSize();i++)
		{
			if(	m_Listeners[i].Create(m_Listeners[i].LocalAddress,SOCK_STREAM,TRUE,FALSE) && 
				m_Listeners[i].Listen(_listen_backlog)
			)
			{	m_Listeners[i].LastGarbageProbeTick = GetTickCount();
				m_Listeners[i].AcceptingThread = ::CreateThread(NULL,0,m_pConnectionManagerThreadFunc,&m_Listeners[i],0,&m_Listeners[i].AcceptingThreadId);
				ret = TRUE;
			}
			else
			{	m_Listeners[i].AcceptingThread = NULL;
			}
		}

		if(ret)
		{	
			ASSERT(m_pPooledDataTransferThreadFunc); // setup transfer thread before start
			{	for(UINT i=0;i<m_hPooledThread.GetSize();i++)
				{
					ASSERT(INVALID_HANDLE_VALUE == m_hPooledThread[i]);
					m_hPooledThread[i] = ::CreateThread(NULL,0,m_pPooledDataTransferThreadFunc,this,0,&m_hPooledThreadId[i]);
					ASSERT(m_hPooledThread[i]);
				}
			}
		}

		return ret;
	}
	else if(m_ServerState == SERVER_PAUSE)
	{
		m_ServerState = SERVER_RUN;
		// ....
		return TRUE;
	}
	else
	{	ASSERT(0);
		return FALSE;
	}
}

BOOL SocketServCore::Pause()
{
	return FALSE;
}

void SocketServCore::EmergencyExit()
{
	if(m_ServerState != SERVER_UNINITIALIZED)
	{
		DWORD CurTID = ::GetCurrentThreadId();

		// stop all listeners
		for(UINT i=0;i<m_Listeners.GetSize();i++)
		{
			if(m_Listeners[i].AcceptingThreadId && m_Listeners[i].AcceptingThreadId != CurTID)
				SuspendThread(m_Listeners[i].AcceptingThread);
		}

		// causing deadlock, no idea why

		// stop all data transfers
		for(UINT i=0;i<m_hPooledThread.GetSize();i++)
		{
			if(m_hPooledThreadId[i] && m_hPooledThreadId[i] != CurTID)
				SuspendThread(m_hPooledThread[i]);
		}

		// close listening socket
		for(UINT i=0;i<m_Listeners.GetSize();i++)
		{
			m_Listeners[i].Close();

			// close all its connections
			for(UINT j=0;j<m_Listeners[i].Connections.GetSize();j++)
				_DestroyConnectionFun(m_Listeners[i].Connections[j]);
				//m_Listeners[i].Connections[j]->DestroyConnection();
		}

		m_ServerState = SERVER_UNINITIALIZED;
	}
}

BOOL SocketServCore::Stop()
{
	if(m_ServerState == SERVER_RUN || m_ServerState == SERVER_PAUSE){}
	else return TRUE;

	if(m_ServerState == SERVER_RUN)
	{
		m_ServerState = SERVER_STOPPED;
		////////////////////////////////////////////////
		// 1. wait for stopping all listeners
		for(UINT i=0;i<m_Listeners.GetSize();i++)
			m_Listeners[i].Close();
		// graceful stopping
		for(UINT i=0;i<m_Listeners.GetSize();i++)
		{
			if(w32::WaitForThreadEnding(m_Listeners[i].AcceptingThread,1000))
			{	::CloseHandle(m_Listeners[i].AcceptingThread);
				m_Listeners[i].AcceptingThread = INVALID_HANDLE_VALUE;
			}
		}
		// bruteforce stopping
		for(UINT i=0;i<m_Listeners.GetSize();i++)
		{	
			if(m_Listeners[i].AcceptingThread != INVALID_HANDLE_VALUE)
			{
				if(!w32::WaitForThreadEnding(m_Listeners[i].AcceptingThread,1000))
				{
					_CheckDump("Accepting thread is terminated, which is listening on ["<<
								m_Listeners[i].LocalAddress<<"]\n");
					::TerminateThread(m_Listeners[i].AcceptingThread,0);
				}

				::CloseHandle(m_Listeners[i].AcceptingThread);
				m_Listeners[i].AcceptingThread = INVALID_HANDLE_VALUE;
			}
		}

		////////////////////////////////////////////////
		// 2. wait for stopping all transfer threads
		for(UINT i=0;i<m_hPooledThread.GetSize();i++)
		{
			VERIFY(PostQueuedCompletionStatus(m_hCompletePort,0,NULL,NULL));
		}
		// graceful stopping
		for(UINT i=0;i<m_hPooledThread.GetSize();i++)
		{
			if(w32::WaitForThreadEnding(m_hPooledThread[i],1000))
			{	::CloseHandle(m_hPooledThread[i]);
				m_hPooledThread[i] = INVALID_HANDLE_VALUE;
			}
		}
		// bruteforce stopping
		for(UINT i=0;i<m_hPooledThread.GetSize();i++)
		{
			if(m_hPooledThread[i] != INVALID_HANDLE_VALUE)
			{
				if(!w32::WaitForThreadEnding(m_hPooledThread[i],1000))
					::TerminateThread(m_hPooledThread[i],0);
				::CloseHandle(m_hPooledThread[i]);
				m_hPooledThread[i] = INVALID_HANDLE_VALUE;
			}
		}

		///////////////////////////////////////////////
		// 3. destroy all connections
		for(UINT i=0;i<m_Listeners.GetSize();i++)
		{
			rt::Buffer<Connection*>& conn = m_Listeners[i].Connections;
			for(UINT c=0;c<m_Listeners[i].ConnectionsUsed;c++)
			{
				if(!conn[c]->IsEmpty())
					_DestroyConnectionFun(conn[c]);
					//conn[c]->DestroyConnection();
			}
			m_Listeners[i].ConnectionsUsed = 0;
			conn.SetSize(0);
		}
	}
	else if(m_ServerState == SERVER_PAUSE)
	{
		// ...
	}

	m_ServerState = SERVER_STOPPED;
	return TRUE;
}

int HttpThreadLocalBlock::ComposeResponseHeader(LPSTR pHdrOut, __int64 len, UINT code, BOOL close_session, LPCSTR content_type)
{
	if((code < 400) && (pConn->m_TimeToLive == SocketServCore::_default_session_TTL))
	{
		pConn->SetTimeToLive(inet::HttpServCore::_default_httpsession_TTL);
	}

	ASSERT(code>=200 && code<600);
	LPCSTR status_str = "";
	if((code%100)<10 && g_HTTP_StatusCode[code/100-2][code%100])
		status_str = g_HTTP_StatusCode[code/100-2][code%100];

	char encoding[64];
	if(len>0)
	{	sprintf(encoding,"Content-Length: %I64u\r\n",len);
	}
	else if(len == -1)
	{	static const char tec[] = "Transfer-Encoding: chunked\r\n";
		memcpy(encoding,tec,sizeof(tec));
	}
	else encoding[0] = 0;

	return	sprintf(pHdrOut,
					"HTTP/1.1 %d %s\r\n"
					"Connection: %s\r\n"
					"Content-Type: %s\r\n"
					"%s"	// encoding
					"%s"	// additional header
					"\r\n",
					code,status_str,
					close_session?"close":"Keep-Alive",
					content_type?content_type:"text/html",
					encoding,
					AdditionalResponseHeaderLen?AdditionalResponseHeader:""
					);
}


void HttpThreadLocalBlock::AppendResponseHeader_ContentRange(const ContentRange& r)
{
	ASSERT(strstr(AdditionalResponseHeader,"Content-Range:") == NULL);
	AdditionalResponseHeaderLen += 
	sprintf_s(&AdditionalResponseHeader[AdditionalResponseHeaderLen],_additional_header_size - AdditionalResponseHeaderLen,
			  "Content-Range: bytes %I64u-%I64u/%I64u\r\n",
			  r.start,r.end,r.total);
}

void HttpThreadLocalBlock::AppendResponseHeader_Maxage(int maxage_sec)
{
	ASSERT(strstr(AdditionalResponseHeader,"Cache-Control:") == NULL);
	AdditionalResponseHeaderLen += 
	sprintf_s(&AdditionalResponseHeader[AdditionalResponseHeaderLen],_additional_header_size - AdditionalResponseHeaderLen,
			  "Cache-Control: public, max-age=%d\r\n",maxage_sec);

}

void HttpThreadLocalBlock::AppendResponseHeader_ETag(LPCSTR e_tag, int maxage_sec)
{
	ASSERT(strstr(AdditionalResponseHeader,"Cache-Control:") == NULL);
	AdditionalResponseHeaderLen += 
	sprintf_s(&AdditionalResponseHeader[AdditionalResponseHeaderLen],_additional_header_size - AdditionalResponseHeaderLen,
			  "Cache-Control: public, max-age=%d\r\nETag: %s\r\n",maxage_sec,e_tag);
}

void HttpThreadLocalBlock::AppendResponseHeader_NoCache()
{
	ASSERT(strstr(AdditionalResponseHeader,"Cache-Control:") == NULL);
	AdditionalResponseHeaderLen += 
	sprintf_s(&AdditionalResponseHeader[AdditionalResponseHeaderLen],_additional_header_size - AdditionalResponseHeaderLen,
			  "Cache-Control: no-cache, no-store\r\nPragma: no-cache\r\n");
}

void HttpThreadLocalBlock::AppendResponseHeader_ContentDisposition(LPCSTR filename)
{
	ASSERT(strstr(AdditionalResponseHeader,"Content-Disposition:") == NULL);
	AdditionalResponseHeaderLen += 
	sprintf_s(&AdditionalResponseHeader[AdditionalResponseHeaderLen],_additional_header_size - AdditionalResponseHeaderLen,
			  "Content-Disposition: attachment; filename=\"%s\"\r\n",filename);
}

BOOL HttpThreadLocalBlock::AppendResponseHeader(LPCSTR head, UINT len)
{
	ASSERT(head[len-1] == '\n');
	if(len + AdditionalResponseHeaderLen <= _additional_header_size)
	{
		memcpy(&AdditionalResponseHeader[AdditionalResponseHeaderLen], head, len);
		AdditionalResponseHeaderLen += len;
		AdditionalResponseHeader[AdditionalResponseHeaderLen] = 0;
		return TRUE;
	}

	return FALSE;
}

BOOL HttpThreadLocalBlock::SendResponseHeadOnly(UINT code, BOOL close_session)
{
	ASSERT(code>=200 && code<600);

	LPCSTR status_str = "";
	if((code%100)<10 && g_HTTP_StatusCode[code/100-2][code%100])
		status_str = g_HTTP_StatusCode[code/100-2][code%100];

	static const char response_temp[] = 
	"<html><head><title>%d - %s</title><body>"
	"<h2>HTTP Status %d</h2>"
	"<p><strong>%d - %s</strong></p>"
	"</body></html>";

	WSABUF buf[2];

	pConn->ClearSendingBuffer();
	buf[1].buf = (LPSTR)pConn->ClaimSendingChunk(sizeof(response_temp) + 64 + 512);
	if(buf[1].buf == NULL)return FALSE;
	buf[1].len = sprintf_s(buf[1].buf,sizeof(response_temp) + 64,response_temp,code,status_str,code,code,status_str);
	pConn->FinalizeSendingChunk(buf[1].len);

	ResponseCode = code;
	ResponseByteSent += (UINT)buf[1].len;

	buf[0].buf = (LPSTR)pConn->ClaimSendingChunk(512);
	if(buf[0].buf == NULL)return FALSE;
	buf[0].len = ComposeResponseHeader(buf[0].buf,buf[1].len,code,close_session,NULL);
	ASSERT(buf[0].len<512);
	pConn->FinalizeSendingChunk(buf[0].len);

	return pConn->WSASend_Overlapped(buf,2);
}

BOOL HttpThreadLocalBlock::SendResponse(LPCBYTE pData, SIZE_T len, UINT code, BOOL close_session, LPCSTR content_type) // response w/ Content-Length
{
	pConn->ClearSendingBuffer();
	LPSTR p = (LPSTR)pConn->ClaimSendingChunk(512);
	if(p == NULL)return FALSE;
	UINT hdr_len = ComposeResponseHeader(p,len,code,close_session,content_type);
	ASSERT(hdr_len<512);
	pConn->FinalizeSendingChunk(hdr_len);

	ResponseCode = code;
	ResponseByteSent += (UINT)len;

	return	pConn->AppendSendingChunk(pData, (int)len) &&
			pConn->WSASend_Overlapped(pConn->m_OverlapSendingBuffer,pConn->m_OverlapSendingBufferUsed);
}

BOOL HttpThreadLocalBlock::SendResponse_NoCopy(LPCBYTE pData, SIZE_T len, UINT code, BOOL close_session, LPCSTR content_type) // response w/ Content-Length
{
	WSABUF buf[2];

	pConn->ClearSendingBuffer();
	buf[0].buf = (LPSTR)pConn->ClaimSendingChunk(512);
	if(buf[0].buf == NULL)return FALSE;
	buf[0].len = ComposeResponseHeader(buf[0].buf,len,code,close_session,content_type);
	ASSERT(buf[0].len<512);
	pConn->FinalizeSendingChunk(buf[0].len);

	ResponseCode = code;
	ResponseByteSent += (UINT)len;

	buf[1].buf = (LPSTR)pData;
	buf[1].len = (ULONG)len;
	return pConn->WSASend_Overlapped(buf,2);
}
					
void HttpThreadLocalBlock::SendChunkResponse_Begin()
{
	ASSERT(!pConn->IsOverlappedSending());
	pConn->ClearSendingBuffer();
}

BOOL HttpThreadLocalBlock::SendChunkResponse_End(UINT code, BOOL close_session, LPCSTR content_type)
{
	ResponseCode = code;

	WSABUF buf[2];
	int content_len = pConn->m_OverlapSendingBufferUsed;
	if(!content_len)return FALSE;

	buf[0].buf = (LPSTR)pConn->ClaimSendingChunk(512);
	if(buf[0].buf == NULL)return FALSE;
	buf[0].len = ComposeResponseHeader(buf[0].buf,content_len,code,close_session,content_type);
	pConn->FinalizeSendingChunk(buf[0].len);

	buf[1].buf = (LPSTR)pConn->m_OverlapSendingBuffer.Begin();
	buf[1].len = content_len;

	return pConn->WSASend_Overlapped(buf,2);
}

BOOL HttpThreadLocalBlock::SendRedirection(UINT code, LPCSTR url, int url_len)
{
	ASSERT(code>=300 && code<400);
	ASSERT(code != 300); // not implemented
	ResponseCode = code;

	pConn->ClearSendingBuffer();

	if(code != w32::inet::HTTP_NOT_MODIFIED)
	{
		ASSERT(url);
		LPCSTR status_str = "";
		if((code%100)<10 && g_HTTP_StatusCode[code/100-2][code%100])
			status_str = g_HTTP_StatusCode[code/100-2][code%100];

		static const char tail[] = "\r\nContent-Length: 0\r\n\r\n";
		auto resp = rt::StringA_Ref("HTTP/1.1 ",9) + 
					code + ' ' + status_str + 
					rt::StringA_Ref("\r\nLocation: ",12) + 
					rt::StringA_Ref(url,url_len);

		//static const char response_msg[] = "HTTP/1.1 %d %s\r\nLocation: ";

		WSABUF  buf[2];

		buf[0].len = resp.GetLength();
		buf[0].buf = (LPSTR)pConn->ClaimSendingChunk(buf[0].len);
		if(buf[0].buf==NULL)return FALSE;
		resp.CopyTo(buf[0].buf);
		//buf[0].len = sprintf(buf[0].buf,response_msg,code,status_str);
		pConn->FinalizeSendingChunk(buf[0].len);

		buf[1].buf = (LPSTR)tail;
		buf[1].len = sizeof(tail);

		return pConn->WSASend_Overlapped(buf,2);
	}
	else // a w32::inet::HTTP_NOT_MODIFIED header (304)
	{
		static const char response_msg[] = "HTTP/1.1 304 Not Modified\r\n\r\n";
		return pConn->WSASend_Overlapped(response_msg,sizeof(response_msg));
	}

	return TRUE;
}

BOOL HttpThreadLocalBlock::SendChunkResponse_EncodeHTML(LPCSTR PlainText, UINT len)
{
	pConn->ActiveTick();

	LPCSTR p = PlainText;
	LPCSTR s = p;
	LPCSTR end = PlainText + len;

	while(s < end)
	{
		static const char BR[] = "<br />";

		if(*s<=62 && (*s == '\'' || *s == '\"' || *s == '&' || *s == '>' || *s == '<'))
		{	
			if(p < s)
				if(!pConn->AppendSendingChunk(p,s-p))
					return FALSE;

			char b[5];
			*((WORD*)b) = 0x2326;	// "&#"
			b[2] = (*s)/10 + '0';
			b[3] = (*s)%10 + '0';
			b[4] = ';';

			pConn->AppendSendingChunk(b,5);
			s++;
			p = s;
			continue;
		}
		else if(*s == '\r')
		{	
			if(p < s)
				if(!pConn->AppendSendingChunk(p,s-p))
					return FALSE;

			pConn->AppendSendingChunk(BR,sizeof(BR)-1);
			if(s[1] == '\n')s++;
			s++;
			p = s;
			continue;
		}
		else if(*s == '\n')
		{	
			if(p < s)
				if(!pConn->AppendSendingChunk(p,s-p))
					return FALSE;

			pConn->AppendSendingChunk(BR,sizeof(BR)-1);
			if(s[1] == '\r')s++;
			s++;
			p = s;
			continue;
		}

		s++;
	}

	if(p < s)
		return pConn->AppendSendingChunk(p,s-p);

	return TRUE;
}

	


HttpThreadLocalBlock::HttpThreadLocalBlock(inet::SocketServCore* pServCore)
	:ThreadLocalBlock(pServCore)
{
	RequestCount = 0;
	pHttpCore = NULL;
}

void HttpThreadLocalBlock::Clear()
{
	ASSERT(pConn);
	((HttpConnection*)pConn)->m_RequestPacketSize = 0;
	((HttpConnection*)pConn)->m_RequestHeaderSize = 0;
}

UINT HttpServCore::ConvertToUTF8(LPSTR pInOut, UINT len)
{
	UINT close = 0;
	for(UINT open = 0;open<len;open++)
	{
		if(pInOut[open] != '%')
			pInOut[close++] = pInOut[open];
		else
		{
			pInOut[close++] = (pInOut[open+2]<='9'?pInOut[open+2]-'0':(pInOut[open+2]-'A'+10)) |
							  ((pInOut[open+1]<='9'?pInOut[open+1]-'0':(pInOut[open+1]-'A'+10))<<4);
			open+=2;
		}
	}
	pInOut[close] = '\0';

	return close;
}

HttpServCore::HttpServCore()
{	

#ifdef ENABLE_SERV_STATISTIC
	stat_MaxRequestHandlingTime = 0;
	stat_TotRequestHandlingTime = 0;
	stat_NumberOfRequest = 0;
#endif

}

void HttpServCore::DefaultRequest_DumpSession(inet::HttpThreadLocalBlock* pTLB)
{
	DefaultRequest_SectionBegin(pTLB,"Session Information");
	char peerinfo[1024];
	int len = sprintf(	peerinfo,
						"Server:                %s:%d\r\n"
						"Client:                %s:%d\r\n"
						"Time-to-Live:          %d msec\r\n"
						"Recv Buffer:           %db/%db\r\n"
						"Send Buffer:           %db/%db",
						(LPCSTR)rt::StringA(pTLB->pConn->m_LocalAddress.GetDottedDecimalAddress()),
						pTLB->pConn->m_LocalAddress.GetPort(),
						(LPCSTR)rt::StringA(pTLB->pConn->m_RemoteAddress.GetDottedDecimalAddress()),
						pTLB->pConn->m_RemoteAddress.GetPort(),
						(int)pTLB->pConn->m_TimeToLive - (int)pTLB->pConn->GetIdleTime(GetTickCount()),
						pTLB->pConn->m_RecvBufferUsed,
						pTLB->pConn->m_RecvBuffer.GetReservedSize(),
						pTLB->pConn->m_OverlapSendingBufferUsed,
						pTLB->pConn->m_OverlapSendingBuffer.GetReservedSize()
						);
	pTLB->SendChunkResponse(peerinfo,len);
	DefaultRequest_SectionEnd(pTLB);
}

void HttpServCore::DefaultRequest_DumpRequest(inet::HttpThreadLocalBlock* pTLB)
{
	DefaultRequest_SectionBegin(pTLB,"Request Information");

	inet::HttpRequest& req = pTLB->Request;
	{	static const LPCSTR MethodName[] = 
		{	"TRACE","CONNECT","PUT",
			NULL,NULL,NULL,NULL,NULL,NULL,NULL,
			"DELETE","GET","HEAD","POST"
		};
		static const char HTTP_tail[] = " HTTP/1.1\r\n";

		pTLB->SendChunkResponse(MethodName[req.Method],(UINT)strlen(MethodName[req.Method]));
		pTLB->SendChunkResponse(' ');
		pTLB->SendChunkResponse(req.pURI,req.URI_len);
		if(req.pQuery)
		{
			pTLB->SendChunkResponse('?');
			pTLB->SendChunkResponse(req.pQuery,req.Query_len);
		}
		pTLB->SendChunkResponse(HTTP_tail,sizeof(HTTP_tail)-1);
		pTLB->SendChunkResponse(req.pRemainHeader,req.RemainHeader_len);
	}

	DefaultRequest_SectionEnd(pTLB);
}

void HttpServCore::DefaultRequest_DumpTCPCore(inet::HttpThreadLocalBlock* pTLB)
{
	DefaultRequest_SectionBegin(pTLB,"Service Base Information");

	__int64 up_time; // seconds
	__int64 kernel,user; // msec
	PROCESS_MEMORY_COUNTERS meminfo;
	{
		HANDLE curproc = OpenProcess(PROCESS_QUERY_INFORMATION|PROCESS_VM_READ,FALSE,GetCurrentProcessId());
		ASSERT(curproc);
					
		FILETIME create,exit;
		GetProcessTimes(curproc,&create,&exit,(FILETIME*)&kernel,(FILETIME*)&user);

		GetProcessMemoryInfo(curproc,&meminfo,sizeof(PROCESS_MEMORY_COUNTERS));
		::CloseHandle(curproc);

		__time64_t	_cur;
		_time64(&_cur);
		up_time = _cur - ((*((__time64_t*)&create))/10000000 - 11644473600);
		kernel /= 10000;
		user /= 10000;
	}

	DWORD handle_count = 0;
	{	// GetProcessHandleCount is not supported on XP
		typedef BOOL  (WINAPI *LPFUNC_GetProcessHandleCount)(HANDLE , PDWORD );
		LPFUNC_GetProcessHandleCount GPHC = (LPFUNC_GetProcessHandleCount)::GetProcAddress(::GetModuleHandle(_T("KERNEL32.DLL")),"GetProcessHandleCount");
		if(GPHC)
			GPHC(GetCurrentProcess(),&handle_count);
	}

	SocketServCore* pCore = pTLB->pConn->m_pServ;
	pTLB->pHttpCore;
	char peerinfo[1024];
	DWORD core_num = w32::NumberOfProcessor();
	w32::CTime64<> ttt;
	ttt.LoadCurrentTime();
	int len = sprintf(	peerinfo,	"Server Date & Time:    %02d/%02d/%04d %02d:%02d:%02d (local)\r\n"
									"Server Up Time:        %s\r\n"
									"User Execution Time:   %s\r\n"
									"Kernel Execution Time: %s\r\n"
									"Overall CPU Usage:     %.2f%% of %d cores  (User/Kernel: %.2f%%/%.2f%%)\r\n"
									"Handle Count:          %d\r\n"
									"Memory Usage:          %0.2fMB/%0.2fMB (Avg/Peak of Working Set)\r\n"
									"Virtual Memory Usage:  %0.2fMB/%0.2fMB (Avg/Peak)\r\n"
#ifdef ENABLE_SERV_STATISTIC
									"Network Throughput:    %0.2fMB/%0.2fMB (Received/Sent)\r\n"
									"Connection:            %d times (Refused:%d, Fake:%d)\r\n"
									"Connection-Timeout:    %d times \r\n"
#endif
									"Thread Pool:           %d threads\r\n"
									"Total Session:         %d over %d binding IP(s)\r\n",
						ttt.GetMonth(),ttt.GetDayOfMonth(),ttt.GetYear(),
						ttt.GetHour(),ttt.GetMinute(),ttt.GetSecond(),
						(LPCSTR)rt::tos::TimeSpan<char,false>((int)(up_time*1000)),
						(LPCSTR)rt::tos::TimeSpan<char,false>((int)user),
						(LPCSTR)rt::tos::TimeSpan<char,false>((int)kernel),
						(kernel+user)/up_time/10.0f/core_num,core_num,(user)/up_time/10.0f/core_num,(kernel)/up_time/10.0f/core_num,
						handle_count,
						meminfo.WorkingSetSize/1024/1024.0f,meminfo.PeakWorkingSetSize/1024/1024.0f,
						meminfo.PagefileUsage/1024/1024.0f,meminfo.PeakPagefileUsage/1024/1024.0f,
#ifdef ENABLE_SERV_STATISTIC
						pCore->stat_RecvKiloByte/1024.0f,pCore->stat_SentKiloByte/1024.0f,
						pCore->stat_Connected+pCore->stat_ConnectionRefused+pCore->stat_ConnectionFake,pCore->stat_ConnectionRefused,pCore->stat_ConnectionFake,
						pCore->stat_ConnectionTimeout,
#endif
						pCore->GetThreadCount(),
						pCore->GetTotalConnectionCount(),
						pCore->GetListenerCount()
						);
	pTLB->SendChunkResponse(peerinfo,len);

	for(UINT i=0;i<pCore->GetListenerCount();i++)
	{
		int len = sprintf(peerinfo,"                       %d over [ %s:%d ] (max. %d)\r\n",
									pCore->GetConnectionCount(i),						
									pCore->GetListenerAddress(i).GetDottedDecimalAddress(),
									pCore->GetListenerAddress(i).GetPort(),
									
									pCore->GetConnectionNumberLimit());
		pTLB->SendChunkResponse(peerinfo,len);
	}

	DefaultRequest_SectionEnd(pTLB);
}


void HttpServCore::DefaultRequest_DumpOSInfo(inet::HttpThreadLocalBlock* pTLB, CPerformanceCounters* pPrefCounters)
{
	DefaultRequest_SectionBegin(pTLB,"Windows Information");
	OSVERSIONINFOEXA vinfo;
	vinfo.dwOSVersionInfoSize = sizeof(vinfo);
	GetVersionExA((LPOSVERSIONINFOA)&vinfo);

	int ver = vinfo.dwMajorVersion*100 + vinfo.dwMinorVersion;
	LPCSTR name = NULL;
	if(ver >= 700)
		name = "7";
	else if(ver >= 601)
		name = (vinfo.wProductType==VER_NT_WORKSTATION)?"7":"Server 2008 R2";
	else if(ver >= 600)
		name = (vinfo.wProductType==VER_NT_WORKSTATION)?"Vista":"Server 2008";
	else if(ver >= 501)
		name = (vinfo.wProductType==VER_NT_WORKSTATION)?"XP":"Server 2003";
	else if(ver >=500)
		name = (vinfo.wProductType==VER_NT_WORKSTATION)?"2000":"Server 2000";
	else
		name = (vinfo.wProductType==VER_NT_WORKSTATION)?"NT":"Server 2000";

	MEMORYSTATUS memstat;
	GlobalMemoryStatus(&memstat);

	char peerinfo[1024];
	int len = sprintf(	peerinfo,	"OS Version:            Windows %s (%d.%d.%04d)\r\n"
									"Serivce Pack:          %s (%d.%d)\r\n"
									"Windows Suite:         ",
									name,vinfo.dwMajorVersion,vinfo.dwMinorVersion,vinfo.dwBuildNumber,
									vinfo.szCSDVersion,vinfo.wServicePackMajor,vinfo.wServicePackMinor);
	pTLB->SendChunkResponse(peerinfo,len);
	if(vinfo.wSuiteMask & VER_SUITE_BACKOFFICE)		pTLB->SendChunkResponse("[BackOffice] ",13);
	if(vinfo.wSuiteMask & VER_SUITE_BLADE)			pTLB->SendChunkResponse("[Web Edition] ",14);
	if(vinfo.wSuiteMask & VER_SUITE_COMPUTE_SERVER)	pTLB->SendChunkResponse("[Cluster Edition] ",18);
	if(vinfo.wSuiteMask & VER_SUITE_DATACENTER)		pTLB->SendChunkResponse("[DataCenter] ",13);
	if(vinfo.wSuiteMask & VER_SUITE_ENTERPRISE)		pTLB->SendChunkResponse("[Enterprise] ",13);
	if(vinfo.wSuiteMask & VER_SUITE_EMBEDDEDNT)		pTLB->SendChunkResponse("[Embedded Edition] ",19);
	if(vinfo.wSuiteMask & VER_SUITE_PERSONAL)		pTLB->SendChunkResponse("[Home Edition] ",15);
	if(vinfo.wSuiteMask & VER_SUITE_SINGLEUSERTS)	pTLB->SendChunkResponse("[RemoteDesktop Single-User] ",28);
	if(vinfo.wSuiteMask & VER_SUITE_SMALLBUSINESS)	pTLB->SendChunkResponse("[Small Business Server] ",24);
	if(vinfo.wSuiteMask & VER_SUITE_STORAGE_SERVER)	pTLB->SendChunkResponse("[Storage Server] ",17);
	if(vinfo.wSuiteMask & VER_SUITE_TERMINAL)		pTLB->SendChunkResponse("[Terminal Server] ",18);
	if(vinfo.wSuiteMask & 0x00008000)				pTLB->SendChunkResponse("[Home Server] ",13); // VER_SUITE_WH_SERVER

	len = sprintf(	peerinfo,	"\r\n"
								"Memory Load :          %d%% (%0.2fGB/%0.2fGB)\r\n"
								"Page File:             %0.2fGB/%0.2fGB\r\n"
								"Virtual Memory:        %0.2fGB/%0.2fGB\r\n",
								memstat.dwMemoryLoad,(memstat.dwTotalPhys-memstat.dwAvailPhys)/1024/1024/1024.0f,memstat.dwTotalPhys/1024/1024/1024.0f,
								(memstat.dwTotalPageFile-memstat.dwAvailPageFile)/1024/1024/1024.0f,memstat.dwTotalPageFile/1024/1024/1024.0f,
								(memstat.dwTotalVirtual-memstat.dwAvailVirtual)/1024/1024/1024.0f,memstat.dwTotalVirtual/1024/1024/1024.0f
					);
	pTLB->SendChunkResponse(peerinfo,len);

	static const char wh[] = "                       ";

	{	DWORD disks = GetLogicalDrives();
		TCHAR path[4];
		path[1] = _T(':');		path[2] = _T('\\');		path[3] = _T('\0');
		ULONGLONG available,free,total;

		UINT disk_co = 0;

		for(TCHAR d = 'A' ; d<='Z'; d++)
		{
			path[0] = d;
			if(	(disks & (1<<(d-'A'))) &&
				GetDiskFreeSpaceEx(path,NULL,(PULARGE_INTEGER)&total,NULL)
			)disk_co++;
		}

		len = sprintf(	peerinfo,	"Logical Disks:         %d disks       Avail. /     Free /    Total\r\n", disk_co);
		pTLB->SendChunkResponse(peerinfo,len);

		for(TCHAR d = 'A' ; d<='Z'; d++)
		{
			path[0] = d;
			if(	(disks & (1<<(d-'A'))) &&
				GetDiskFreeSpaceEx(path,(PULARGE_INTEGER)&available,(PULARGE_INTEGER)&total,(PULARGE_INTEGER)&free)
			)
			{
				pTLB->SendChunkResponse(wh,sizeof(wh)-1);

				len = sprintf(peerinfo,	"Drive %c:   %7.2fGB /%7.2fGB /%7.2fGB\r\n",
										d,
										(UINT)(available/(1024*1024))/1024.0f,
										(UINT)(free/(1024*1024))/1024.0f,
										(UINT)(total/(1024*1024))/1024.0f
							 );
				pTLB->SendChunkResponse(peerinfo,len);
			}
		}
	}

	if(pPrefCounters)
	{
		len = sprintf(peerinfo,	"Preformance Counters   %d\r\n", pPrefCounters->GetSize());
		pTLB->SendChunkResponse(peerinfo,len);

		pPrefCounters->UpdateAll();
		for(UINT i=0;i<pPrefCounters->GetSize();i++)
		{
			const rt::String_Ref& name = pPrefCounters->GetFriendName(i);
			ATL::CT2A na(name);
			int nlen = (int)min(strlen(na),sizeof(wh)-2);
			pTLB->SendChunkResponse(na,nlen);
			pTLB->SendChunkResponse(':');
			if(nlen < sizeof(wh)-2)
				pTLB->SendChunkResponse(wh,sizeof(wh)-2-nlen);
			
			rt::String_Ref tt = pPrefCounters->GetValue_Text(i);
			ATL::CT2A ta(tt);
			pTLB->SendChunkResponse(ta,(UINT)strlen(ta));
			pTLB->SendChunkResponse("\r\n",2);
		}
	}

	DefaultRequest_SectionEnd(pTLB);
}

void HttpServCore::DefaultRequest_Begin(inet::HttpThreadLocalBlock* pTLB, LPCSTR title)
{
	pTLB->SendChunkResponse_Begin();

	int title_len = (int)strlen(title);

	static const char html1[] = 
	"<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.01 Transitional//EN\" \"http://www.w3.org/TR/html4/loose.dtd\">"
	"<html><head><meta http-equiv=\"Content-Type\" content=\"text/html; charset=utf-8\"><title>";

	pTLB->SendChunkResponse(html1,sizeof(html1)-1);
	pTLB->SendChunkResponse(title,title_len);
	
	static const char html2[] = 
	"</title><style type=\"text/css\">"
	"<!-- body,td,th { font-family: Calibri, Lucida Console;"
	"	font-size: 14px; color: #000000; } "
	"--></style></head>"
	"<body style=\"background-color: #FFFFFF;\"><br />";

	pTLB->SendChunkResponse(html2,sizeof(html2)-1);
}

void HttpServCore::DefaultRequest_End(inet::HttpThreadLocalBlock* pTLB)
{
	static const char html[] = 
	"<br /><br /><br /><br /></body></html>";
	pTLB->SendChunkResponse(html,sizeof(html)-1);

	pTLB->SendChunkResponse_End();
}

void HttpServCore::DefaultRequest_SectionBegin(inet::HttpThreadLocalBlock* pTLB, LPCSTR title)
{
	static const char html1[] = "<strong style=\"font-size:14px\">";
	pTLB->SendChunkResponse(html1,sizeof(html1)-1);

	pTLB->SendChunkResponse(title,(UINT)strlen(title));

	static const char html2[] = 
	"</strong><br />"
	"<table width=\"550px\" border=\"0\" cellspacing=\"0\" cellpadding=\"4\">"
	"<tr><td style=\"background-color:#DDDDDD\"><pre style=\"font-size:12px\">";
	pTLB->SendChunkResponse(html2,sizeof(html2)-1);
}

void HttpServCore::DefaultRequest_SectionEnd(inet::HttpThreadLocalBlock* pTLB)
{
	static const char html[] = "</pre></td></tr></table><br />";
	pTLB->SendChunkResponse(html,sizeof(html));
}

BOOL HttpServCore::LoadHttpServConfig(LPCTSTR fn_xml, rt::BufferEx<CInetAddr>& addr, UINT& thread_count, UINT& concurrent_session_max)
{
	w32::CFileBuffer<char>	file;
	w32::CXMLParser	xml;

	if(	file.SetSizeAs(fn_xml) &&
		xml.Load(file,TRUE,file.GetSize()) &&
		xml.EnterXPath("/config/httpserv")
	)
	{	thread_count = xml.GetAttribute_Int("thread_count",0);
		concurrent_session_max = xml.GetAttribute_Int("concurrency",0);
		rt::StringA text;

		xml.EscapeXPath();
		if(xml.EnterFirstChildNode())
		{
			do
			{
				if(xml.GetInnerText(text))
				{
					if(xml.GetNodeName() == "port")
					{
						CInetAddr& a = addr.push_back();
						a.SetAsLocal();
						a.SetPort(atoi(text));
					}
					else if(xml.GetNodeName() == "address")
					{	CInetAddr a;
						if(a.SetAddress(ATL::CA2T(text)))
							addr.push_back(a);
					}
				}
			}while(xml.EnterNextSiblingNode());
		}

		return TRUE;
	}

	return FALSE;
}

HRESULT HttpServCore::ParseDataReceived(HttpThreadLocalBlock* pTLB, UINT newly_received_size)
{
	inet::HttpRequest&			req = pTLB->Request;
	inet::HttpConnection*		pConn = (inet::HttpConnection*)(pTLB->pConn);

	rt::BufferEx<BYTE>& buf = pConn->m_RecvBuffer;

	if(pConn->m_RequestPacketSize) // continue partial request content body
	{
		if(pConn->m_RecvBufferUsed >= pConn->m_RequestPacketSize)
		{
			goto REQUEST_DATA_IS_READY;
		}
		else
			return S_FALSE;
	}
	else	// start from the beginning
	{
		pTLB->Clear();

		// check HTTP method
		if(pConn->m_RecvBufferUsed > 5)
		{
			LPSTR hdr = (LPSTR)&buf[0];
			if(HashHttpVerb(hdr) <= REGQUEST_MAX){}
			else return E_FAIL; // HTTP method illegal
		}
		else return S_FALSE; // too short, nothing to do with
		
		UINT i = 0;
		if(newly_received_size + 3 < pTLB->pConn->m_RecvBufferUsed)
			i = pConn->m_RecvBufferUsed - (newly_received_size + 3);

		static const DWORD end_tag = MAKEFOURCC('\r','\n','\r','\n');

		// scan for end-tag
		for(;(i+3)<pConn->m_RecvBufferUsed;i++)
		{
			if(*((DWORD*)&buf[i]) == end_tag)
			{
				buf[i + 2] = '\0';

				i += 4;
				pConn->m_RequestHeaderSize = i;

				LPSTR http,tail;
				// check content length
				int content_length = 0;
				if((http = strstr((LPSTR)buf.Begin(),"Content-Length: ")) &&
				   (tail = strchr(http,'\r'))
				)
				{	*tail = '\0';
					content_length = atoi(http + 16);
					*tail = '\r';
					if(content_length > _postdata_size_max) // post data too large
						return E_OUTOFMEMORY;

					ASSERT(pConn->m_RequestPacketSize == 0);
					pConn->m_RequestPacketSize = content_length + pConn->m_RequestHeaderSize;

					// preparing data post, don't response
					//HRESULT ret = ((CHttpServ*)pTLB->pConn->m_pServ)->
					//					m_pHandler_Request(pTLB);

					// nothing should be sent
					//ASSERT(pTLB->m_ResponseHeaderSize == 0);
					//ASSERT(pTLB->m_ResponseBufferUsed == 0);

					//if(FAILED(ret))
					//	return ret; // preparing data post failed

					if(pConn->m_RecvBufferUsed >= pConn->m_RequestPacketSize)
						goto REQUEST_DATA_IS_READY;
					else
						return S_FALSE;
				}
				else
				{	pConn->m_RequestPacketSize = pConn->m_RequestHeaderSize;
					goto REQUEST_DATA_IS_READY;
				}
			}
		}

		if(pTLB->pConn->m_RecvBufferUsed<_http_header_size_max)
			return S_FALSE; // receive more data
		else
			return E_OUTOFMEMORY; // too much data received, yet HTTP header is still not ended
	}

	ASSERT(0);

REQUEST_DATA_IS_READY:

	ASSERT(pConn->m_RequestHeaderSize);
	ASSERT(pConn->m_RequestPacketSize >= pConn->m_RequestHeaderSize);
	ASSERT(pConn->m_RecvBufferUsed >= pConn->m_RequestPacketSize);

	UINT hlen = pConn->m_RequestHeaderSize;
	// Parse request header
	LPSTR hdr = (LPSTR)&buf[0];
	LPSTR tail;
	LPSTR http;
	if(	(hlen > 4+1+4+3+4) &&
		(tail = strchr(hdr+5,'\r')) &&
		(*tail = '\0',true) &&
		(http = strstr(hdr+5,"HTTP/1.")) &&
		(http[-1] == ' ')
	){}
	else return E_INVALIDARG; // illegal header

	static const UINT URI_offset[] = {6,8,4,0,0,0,0,0,0,0,7,4,5,5};
	http[-1] = '\0';
	
	req.Method = HashHttpVerb(hdr);
	if(REGQUEST_MAX < req.Method)
		return E_INVALIDARG;

	req.pURI = hdr + URI_offset[req.Method];	//UTF-8
	req.URI_len = inet::HttpServCore::ConvertToUTF8((LPSTR)req.pURI,(UINT)(http - req.pURI - 1));
	{	// parse '?'
		LPSTR pQ = (LPSTR)strchr(req.pURI,'?');
		if(pQ)
		{
			*pQ = '\0';
			int uri_len = (int)(pQ - req.pURI);
			req.Query_len = req.URI_len - uri_len - 1;
			req.pQuery = pQ + 1;
			req.URI_len = uri_len;
		}
		else
		{
			req.pQuery = NULL;
			req.Query_len = 0;
		}
	}
	req.pRemainHeader = tail+2;
	req.RemainHeader_len = hlen - (int)((LPSTR)req.pRemainHeader-hdr) - 2;

	// check range
	{	LPSTR end;
		LPSTR tail;
		if((http = strstr((LPSTR)req.pRemainHeader,"Range: bytes=")) &&
		   (end = strchr(http,'-')) &&
		   (tail = strchr(end,'\r'))
		)
		{	http += 13;
			end++;
			tail[0] = '\0';
			end[-1] = '\0';
			req.Range[0] = _atoi64(http);
			req.Range[1] = _atoi64(end);
			tail[0] = '\r';
			end[-1] = '-';
			if(req.Range[1]<req.Range[0])
				req.Range[1] = LLONG_MAX;
		}
		else
		{	req.Range[0] = 1;
			req.Range[1] = 0;
		}
	}

	// check content-length
	req.pContent = NULL;
	req.Content_len = 0;
	if(pConn->m_RequestPacketSize > pConn->m_RequestHeaderSize)
	{	req.pContent = &buf[pConn->m_RequestHeaderSize];
		req.Content_len = pConn->m_RequestPacketSize - pConn->m_RequestHeaderSize;
	}
	else
	{	req.pContent = NULL;
		req.Content_len = 0;
	}

	return S_OK;
}




} // namespace inet
} // namespace w32
