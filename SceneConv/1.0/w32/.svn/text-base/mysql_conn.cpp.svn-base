#include "stdafx.h"
#include "mysql_conn.h"

#include <mysql.h>

namespace w32
{

void MySQL_Connection::_QRet::Clear()
{
	if(m_pResultSet)
	{
		mysql_free_result(m_pResultSet);
		m_pResultSet = NULL;
	}
}

MySQL_Connection::MySQL_Connection()
{
	m_pConn = NULL;
	m_bTranscation = FALSE;
	m_bConnected = FALSE;
}

MySQL_Connection::~MySQL_Connection()
{
	Destroy();
}

BOOL MySQL_Connection::ConnectTo(LPCSTR user, LPCSTR password, LPCSTR server_url_in) // server_url = [schema@]hostname[:port]
{
	rt::StringA url(server_url_in);
	LPSTR server_url = url.GetBuffer();

	// parse server_url
	UINT port = 0;
	LPSTR p;
	if(p = strchr(server_url,':'))
	{	port = atoi(p+1);
		*((LPSTR)p) = '\0';
	}
	LPSTR sch_p = NULL;
	LPSTR sch = NULL;
	LPSTR hostname = server_url;
	if(sch_p = strchr(server_url,'@'))
	{	*(sch_p) = '\0';
		sch = server_url;
		hostname = sch_p+1;
	}

	BOOL ret = ConnectTo(user,password,hostname,sch,port);
	if(p)*p = ':';
	if(sch_p)*sch_p = '@';

	return ret;
}

UINT MySQL_Connection::GetLastWarningCount() const
{
	return mysql_warning_count(m_pConn);
}


BOOL MySQL_Connection::DuplicateConnection(const MySQL_Connection& x)
{
	Destroy();
	return ConnectTo(x._Backup_User,x._Backup_Password,x._Backup_Server,x._Backup_Databse.Begin(),x._Backup_Port);
}

BOOL MySQL_Connection::ConnectTo(LPCSTR user, LPCSTR password, LPCSTR server_addr, LPCSTR databse, UINT port)
{
	ASSERT(!IsConnected());
	
	if(	(m_pConn = mysql_init(m_pConn)) &&
		(mysql_real_connect(m_pConn,server_addr,user,password,databse,port,NULL,CLIENT_MULTI_RESULTS|CLIENT_MULTI_STATEMENTS|CLIENT_FOUND_ROWS) == m_pConn)
	)
	{	m_bConnected = TRUE;
		my_bool reconnect = true;
		VERIFY(0==mysql_options(m_pConn, MYSQL_OPT_RECONNECT, &reconnect));
		VERIFY(0==mysql_set_character_set(m_pConn, "utf8"));
		mysql_autocommit(m_pConn,true);

		_Backup_User = user;
		_Backup_Password = password;
		_Backup_Server = server_addr;
		_Backup_Databse = databse;
		_Backup_Port = port;

	}
	else
	{	m_bConnected = FALSE;
	}

	return IsConnected();
}

void MySQL_Connection::Transcation_Begin()
{
	m_TranscationCSS.Lock();

	ASSERT(m_bTranscation == FALSE);
	mysql_autocommit(m_pConn,FALSE);
	m_bTranscation = TRUE;
}

void MySQL_Connection::Transcation_Commit()
{
	ASSERT(m_bTranscation == TRUE);
	mysql_commit(m_pConn);
	mysql_autocommit(m_pConn,TRUE);
	m_bTranscation = FALSE;

	m_TranscationCSS.Unlock();
}

void MySQL_Connection::Transcation_Rollback()
{
	ASSERT(m_bTranscation == TRUE);
	mysql_rollback(m_pConn);
	mysql_autocommit(m_pConn,TRUE);
	m_bTranscation = FALSE;

	m_TranscationCSS.Unlock();
}

LPCSTR MySQL_Connection::GetLastErrorMessage() const
{
	return mysql_error(m_pConn);
}

UINT MySQL_Connection::GetLastError() const
{
	return mysql_errno(m_pConn);
}


void MySQL_Connection::Destroy()
{
	if(m_pConn)
	{
		if(m_bTranscation)
			Transcation_Rollback();
		mysql_close(m_pConn);
		m_pConn = NULL;
	}
}

BOOL MySQL_Connection::Execute(LPCSTR pSQL, ULONGLONG* pMatchedCount)
{
	ASSERT(m_pConn);
	if(mysql_query(m_pConn,pSQL) == 0)
	{	if(pMatchedCount)*pMatchedCount = mysql_affected_rows(m_pConn);
		return TRUE;
	}
	else
	{	if(pMatchedCount)*pMatchedCount = 0;
		return FALSE;
	}
}

MySQL_Connection::_QRet MySQL_Connection::Query(LPCSTR pSQL, BOOL allow_seek)
{
	if(Execute(pSQL))
	{	
		return _QRet(allow_seek?mysql_store_result(m_pConn):mysql_use_result(m_pConn),mysql_field_count(m_pConn));
	}

	return _QRet();
}


MySQL_UpdateValues::MySQL_UpdateValues(MySQL_Connection& sql_conn, LPCSTR table, LPCSTR fields, BOOL no_overwirte)
	:_Conn(sql_conn)
{
	_Statement.SetSize(128*1024);
	int len;
	if(no_overwirte)
		len = sprintf(_Statement,"INSERT INTO %s (%s) VALUES ",table,fields);
	else
		len = sprintf(_Statement,"REPLACE INTO %s (%s) VALUES ",table,fields);

	_pValues = _pValuesBase = &_Statement[len];
	_Claimed = FALSE;
	_HasError = FALSE;
}

LPSTR MySQL_UpdateValues::ClaimRow(UINT length)
{
	ASSERT(!_Claimed);
	if((int)(length+3) >= _Statement.End() - _pValues)
		if(!Flush())return NULL;
	_Claimed = TRUE;
	_pValues[0] = '(';
	_pValues++;
	return _pValues;
}

void MySQL_UpdateValues::SubmitRow(UINT finalized_length)
{
	ASSERT(_Claimed);
	_pValues += finalized_length;
	_pValues[0] = ')';
	_pValues[1] = ',';
	_pValues+=2;
	_Claimed = FALSE;
}

BOOL MySQL_UpdateValues::Flush()
{	
	ASSERT(!_Claimed);
	if(_pValues > _pValuesBase)
	{
		_pValues[0] = '\0';
		_pValues[-1] = ';';
		if(!_Conn.Execute(_Statement))
		{	_HasError = TRUE;
			return FALSE;
		}
		_pValues = _pValuesBase;
	}
	return TRUE;
}

MySQL_RecordSet::MySQL_RecordSet(const MySQL_Connection::_QRet& x)
{
	m_pResultSet = NULL;
	*this = x;
}

void MySQL_RecordSet::operator = (const MySQL_Connection::_QRet& x)
{
	Clear();

	m_pResultSet = x.m_pResultSet;
	m_FieldCount = x.m_FieldCount;

	if(m_pResultSet)
		Next();
	else
		m_CurrentRow = NULL;

	rt::_CastToNonconst(&x)->m_pResultSet = NULL;
}

BOOL MySQL_RecordSet::Next()
{
	ASSERT(m_pResultSet);
	return (m_CurrentRow = mysql_fetch_row(m_pResultSet))!=NULL;
}

LPCSTR MySQL_RecordSet::GetFieldName(UINT i) const
{
	ASSERT(m_pResultSet);
	return mysql_fetch_fields(m_pResultSet)[i].name;
}

DWORD MySQL_RecordSet::GetFieldType(UINT i) const
{
	ASSERT(m_pResultSet);
	int t = mysql_fetch_fields(m_pResultSet)[i].type;
	if( (t>=MYSQL_TYPE_DECIMAL && t<=MYSQL_TYPE_LONG) || t==MYSQL_TYPE_LONGLONG || t==MYSQL_TYPE_INT24 || t==MYSQL_TYPE_YEAR || t==MYSQL_TYPE_NEWDECIMAL)
		return SQL_FIELD_TYPE_INTEGER;
	else
	if(t==MYSQL_TYPE_FLOAT || t==MYSQL_TYPE_DOUBLE)
		return SQL_FIELD_TYPE_FLOAT;
	else
	if(	(t>=MYSQL_TYPE_TIMESTAMP && t<=MYSQL_TYPE_DATETIME) || t==MYSQL_TYPE_NEWDATE )
		return SQL_FIELD_TYPE_TIMEDATE;
	else
	if( t>=MYSQL_TYPE_TINY_BLOB && t<=MYSQL_TYPE_BLOB )
		return SQL_FIELD_TYPE_BLOB;
	else
	if( t == MYSQL_TYPE_ENUM )
		return SQL_FIELD_TYPE_ENUM;
	else
	if( t == MYSQL_TYPE_SET )
		return SQL_FIELD_TYPE_SET;
	else
	if( t == MYSQL_TYPE_VARCHAR || t == MYSQL_TYPE_VAR_STRING || t == MYSQL_TYPE_STRING)
		return SQL_FIELD_TYPE_STRING;
	else
		return SQL_FIELD_TYPE_UNKNOWN;
}

BOOL MySQL_RecordSet::SeekTo(UINT offset)
{
	ASSERT(m_pResultSet);
	mysql_data_seek(m_pResultSet,offset);

	return Next();
}

LPCSTR MySQL_RecordSet::GetField(UINT field_id)
{
	ASSERT(m_CurrentRow);
	if(m_FieldCount>field_id)
		return m_CurrentRow[field_id];
	else
		return NULL;
}

UINT MySQL_RecordSet::GetSize_Seekable() const
{
	ASSERT(m_pResultSet);
	return (UINT)mysql_num_rows(m_pResultSet);
}


} // namespace w32



