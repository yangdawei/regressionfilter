#include "stdafx.h"
#include "prog_idct.h"
#include <float.h>
#include <math.h>


namespace w32
{
	DWORD	g_TMD_Mode = 0;
	HWND	g_TMD_HostWnd = NULL;
	RECT	g_TMD_HostArea = {0,0,0,0};
};


HBRUSH w32::_meta_::_Prograss_Indicator::m_uiProgressbar = NULL;
HBRUSH w32::_meta_::_Prograss_Indicator::m_uiBg = NULL;
HPEN w32::_meta_::_Prograss_Indicator::m_uiBorder = NULL;
HPEN w32::_meta_::_Prograss_Indicator::m_uiBorder_lt = NULL;


void w32::SetTimeMeasureDisplayMode(DWORD mode, HWND hWnd, const LPRECT pArea)
{
	g_TMD_Mode = mode;
	g_TMD_HostWnd = hWnd;

	if(pArea)
	{
		g_TMD_HostArea = *pArea;
		g_TMD_HostArea.right --;
		g_TMD_HostArea.bottom --;
	}
}

#pragma warning(disable:4267)

#ifndef AFX_IDW_STATUS_BAR
#define AFX_IDW_STATUS_BAR              0xE801  // Status bar window
#endif

#ifndef SB_GETPARTS
#define SB_GETPARTS             (WM_USER+6)
#endif

#ifndef SB_GETRECT
#define SB_GETRECT              (WM_USER+10)
#endif



void w32::_meta_::_Prograss_Indicator::_SetupStatusBar()
{
	int parts;
	if( parts = (int)::SendMessage(m_hHostWnd,SB_GETPARTS,(WPARAM)1,(LPARAM)&parts))
	{
		int max_width=0;
		int PaneIndex = 0;
		for(int i=0;i<parts;i++)
		{
			CRect rc;
			::SendMessage(m_hHostWnd,SB_GETRECT,(WPARAM)SB_GETRECT,(LPARAM)&rc);
			if( rc.Width() > max_width )
			{
				PaneIndex = i;
				max_width = rc.Width();
			}
		}

		::SendMessage(m_hHostWnd,SB_GETRECT,PaneIndex,(LPARAM)&m_HostArea);

		m_HostArea.right--;
		m_HostArea.bottom--;

		static const int PROGRESSBAR_MAX = 250;

		int ttw = m_HostArea.right - m_HostArea.left - 4;
		if( ttw+4 > PROGRESSBAR_MAX )
			m_HostArea.left += ttw + 4 - PROGRESSBAR_MAX;
	}
}


w32::_meta_::_Prograss_Indicator::_Prograss_Indicator(DWORD work_thread_id, LPCTSTR title)
{
	ZeroMemory(m_szTitle,sizeof(m_szTitle));
	_tcsncpy(m_szTitle,title?title:_T("In Progress"),sizeofArray(m_szTitle)-1);
	m_szTitleLen = _tcslen(m_szTitle);

	m_hHostWnd = NULL;

	m_Mode = g_TMD_Mode;
	switch(m_Mode)
	{
	case TMDM_STATUSBAR_FRAMEWND:
		{	
			HWND hMainWnd = g_TMD_HostWnd;
			if(hMainWnd && ::GetWindowThreadProcessId(hMainWnd,NULL)!=work_thread_id)
			{
				if(m_hHostWnd = ::GetDlgItem(hMainWnd,AFX_IDW_STATUS_BAR))
					_SetupStatusBar();
			}
			//else
			//{	//so sad, the UI thread is the working thread. If _Prograss_Indicator runs
			//	//in the main UI thread, deadlock occurs. Because the message pipeline is blocked
			//}
		}
		break;
	case TMDM_STATUSBAR:
		m_hHostWnd = g_TMD_HostWnd;
		_SetupStatusBar();
		break;
	case TMDM_WND_RECT:
		m_hHostWnd = g_TMD_HostWnd;
		m_HostArea = g_TMD_HostArea;
		break;
	case TMDM_CONSOLE: break;
	default:
		ASSERT(0);
	}

	if(m_hHostWnd)
	{
		if(!m_uiProgressbar)m_uiProgressbar = ::CreateSolidBrush(RGB(90,100,166));
		if(!m_uiBg)m_uiBg = ::CreateSolidBrush(::GetSysColor(COLOR_3DFACE));

		if(!m_uiBorder)m_uiBorder = ::CreatePen(PS_SOLID,1,::GetSysColor(COLOR_3DSHADOW));
		if(!m_uiBorder_lt)m_uiBorder_lt = ::CreatePen(PS_SOLID,1,::GetSysColor(COLOR_3DSHADOW));

		m_BackDC.Create(m_HostArea.right - m_HostArea.left,
						m_HostArea.bottom - m_HostArea.top,
						24);

		m_HostPosition.x = m_HostArea.left;
		m_HostPosition.y = m_HostArea.top;
		
		_bar_offset = -(int)(m_BackDC.GetWidth()-4)/2;
	}

	m_TimeMeasure.Start();
}

w32::_meta_::_Prograss_Indicator::~_Prograss_Indicator()
{
	if( m_hHostWnd )
	{
		::RedrawWindow(m_hHostWnd,NULL,NULL,RDW_INVALIDATE | RDW_UPDATENOW | RDW_ERASE);
	}
	else if(TMDM_CONSOLE == m_Mode)
	{	
		w32::Logging::DumpCleanLine();
	}
}


void w32::_meta_::_Prograss_Indicator::UpdatePrograss(float prog)
{
	TCHAR ts[100];
    UINT ts_lev = 0;
	{
		double time_disp;
		if(prog > FLT_EPSILON)
		{
			time_disp = (1-prog)*m_TimeMeasure.Snapshot()/(prog*1000.0);
		}
		else
		{
			time_disp = m_TimeMeasure.Snapshot()/1000;
		}

		_stprintf(ts,_T("%02dh:"),((int)(time_disp/3600.0)) );
        if ( time_disp - 3600.0 > EPSILON ) ts_lev = 2;
		time_disp = fmod(time_disp, 3600.0);

		_stprintf(&ts[4],_T("%02dm:"),((int)(time_disp/60.0)));
        if ( time_disp - 60.0 > EPSILON && ts_lev == 0) ts_lev = 1;
		time_disp = fmod(time_disp, 60.0);

		_stprintf(&ts[8],_T("%02ds"),((int)time_disp));

	}

	if(m_Mode == TMDM_CONSOLE && ::w32::Console::IsAvailable())
	{
		HANDLE hConsole = (HANDLE)GetStdHandle(STD_OUTPUT_HANDLE);
		_tprintf(_T("%s: "),m_szTitle);

		if(prog > FLT_EPSILON)
		{
			SetConsoleTextAttribute(hConsole, FOREGROUND_INTENSITY|FOREGROUND_GREEN);
			_tprintf(_T("%5.2f%%"),prog*100.0f);
			SetConsoleTextAttribute(hConsole, FOREGROUND_RED|FOREGROUND_GREEN|FOREGROUND_BLUE);

			if(m_szTitleLen<26)
			{
				_tprintf(_T(" ["));
				for(int i=0;i<20;i++)
				{
					float t = i - prog*20;
					if(t<0)
					{
						_tprintf(_T(">"));
					}
					else if(t<0.5)
					{
						_tprintf(_T("="));
					}
					else
					{
						_tprintf(_T("-"));
					}
				}
				_tprintf(_T("]"));
			}
			else
			{	static int label_id = 0;
				static const LPCTSTR symbol = _T("-\\|/");
				_tprintf(_T(" %c"),symbol[(++label_id)%4]);
			}
	 
			if(m_szTitleLen<30)
				_tprintf(_T(" Remain:["));
			else
				_tprintf(_T(" ["));

			switch ( ts_lev )
			{
			case 2:
				SetConsoleTextAttribute(hConsole, FOREGROUND_INTENSITY|FOREGROUND_RED); break;
			case 1:
				SetConsoleTextAttribute(hConsole, FOREGROUND_INTENSITY|FOREGROUND_RED|FOREGROUND_GREEN); break;
			case 0:
				SetConsoleTextAttribute(hConsole, FOREGROUND_INTENSITY|FOREGROUND_GREEN); break;
			default:
				ASSERT(0);
			}
			_tprintf(_T("%s"),ts);
			SetConsoleTextAttribute(hConsole, FOREGROUND_RED|FOREGROUND_GREEN|FOREGROUND_BLUE);
		}
		else
		{	static int sym_co = 0;
			TCHAR sym[5] = _T("-\\|/");

			SetConsoleTextAttribute(hConsole, FOREGROUND_INTENSITY|FOREGROUND_GREEN);
			_tprintf(_T(" %c "),sym[sym_co++]);
			SetConsoleTextAttribute(hConsole, FOREGROUND_RED|FOREGROUND_GREEN|FOREGROUND_BLUE);
			sym_co = sym_co%4;

			_tprintf(_T(" Passed:[ "));

			SetConsoleTextAttribute(hConsole, FOREGROUND_INTENSITY|FOREGROUND_GREEN);
			_tprintf(_T("%s "),ts);
			SetConsoleTextAttribute(hConsole, FOREGROUND_RED|FOREGROUND_GREEN|FOREGROUND_BLUE);
		}

		_tprintf(_T("]\r"));
	}
	else if(m_hHostWnd && m_Mode != TMDM_CONSOLE)
	{	
		TCHAR text[200];
		if(prog > FLT_EPSILON)
			_stprintf(text,_T("%2d.%02d%%  (%s)"),(int)(prog*100),((int)(prog*10000))%100,ts);
		else
			_stprintf(text,_T("%s passed"),ts);

		BOOL no_text = FALSE;
		HDC dc;
		dc = m_BackDC.GetDC(); 

		RECT hostarea = {0,0,m_BackDC.GetWidth()-1,m_BackDC.GetHeight()-1};

		SIZE sz;
		::GetTextExtentPoint32(dc,text,(int)_tcslen(text),&sz);

		int ttw = hostarea.right - hostarea.left - 4;
		if(sz.cx > ttw)
		{
			*_tcschr(text,_T(' ')) = _T('\0');
			::GetTextExtentPoint32(dc,text,(int)_tcslen(text),&sz);
			if(sz.cx > ttw)no_text = TRUE;
		}

		::FillRect(dc,&hostarea,m_uiBg);

		{HPEN org_pen = (HPEN)::SelectObject(dc,m_uiBorder);
			::MoveToEx(dc,hostarea.left,hostarea.bottom,NULL);
			::LineTo(dc,hostarea.left,hostarea.top);
			::LineTo(dc,hostarea.right,hostarea.top);
			::SelectObject(dc,m_uiBorder_lt);
			::LineTo(dc,hostarea.right,hostarea.bottom);
			::LineTo(dc,hostarea.left,hostarea.bottom);
			::SelectObject(dc,org_pen);
		}

		RECT progrc;
		progrc = hostarea;

		progrc.left+=2;
		progrc.right-=2;
		progrc.top+=2;
		progrc.bottom--;

		if(prog > FLT_EPSILON)
		{
			progrc.right = (LONG)(progrc.left + (progrc.right - progrc.left)*prog + 0.5f);
			::FillRect(dc,&progrc,m_uiProgressbar);
		}
		else
		{	// draw something else
			int BLOCK_S = (progrc.right-progrc.left)*3/2;
			_bar_offset = (_bar_offset+1)%(BLOCK_S);

			RECT rc = progrc;
			rc.top++;
			rc.bottom--;
			for(int i = rc.left - (BLOCK_S - _bar_offset - 1);i<progrc.right;i+=BLOCK_S)
			{
				rc.left = max(i, progrc.left);
				rc.right= max(min(i + BLOCK_S/3,progrc.right),progrc.left);
				::FillRect(dc,&rc,m_uiProgressbar);
			}
		}

		if( !no_text )
		{
			HFONT font = (HFONT)::SendMessage(m_hHostWnd, WM_GETFONT, 0, 0);
			HFONT org_font = (HFONT)::SelectObject(dc,font);
			::SetTextColor(dc,RGB(0,0,0));
			::SetBkMode(dc,TRANSPARENT);
			::DrawText(dc,text,(int)_tcslen(text),&hostarea,DT_CENTER|DT_VCENTER|DT_SINGLELINE);
			::SelectObject(dc,org_font);
		}

		{	m_BackDC.ReleaseDC();
			HDC dc = ::GetDC(m_hHostWnd);
			m_BackDC.Draw(dc,m_HostPosition.x,m_HostPosition.y);
			::ReleaseDC(m_hHostWnd,dc);
		}
	}
}

