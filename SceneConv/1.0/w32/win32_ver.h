#pragma once

//////////////////////////////////////////////////////////////////////
// Visual Computing Foundation Classes (VCFC)
//					Jiaping Wang  2006.2
//					e_boris2002@hotmail.com
//
// (C) Copyright Jiaping Wang 2004.
//
// Permission to copy, use, modify, sell and distribute this software
// is granted provided this copyright notice appears in all copies.
// This software is provided "as is" without express or implied
// warranty, and with no claim as to its suitability for any purpose.
//
// Absolutely no warranty!!
//////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////
//  Brief Description
//
//  win32_ver.h
//
//  windows version selection
//
//////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////
// Revise History
//
// Inital version	2006.6.9		Jiaping
// Add <windowsx.h>	2006.9.10		Jiaping
//
//////////////////////////////////////////////////////////////////////

#ifdef _INC_WINDOWS
#pragma message(">> <windows.h> has been included somewhere, windows version selection is disabled")
#pragma message(">>    if some symbol can not be found, check w32\\win32_ver.h for version selection marcos.")
#endif

#ifndef _INC_WINDOWS

//////////////////////////////////////////////////////////////////////
// Enable win32 APIs beyond Win9x/WinNT4/IE4.0 
#ifdef WINVER
	#undef WINVER
#endif
#define WINVER 0x0501 

#ifdef _WIN32_WINNT
	#undef _WIN32_WINNT
#endif
#define _WIN32_WINNT 0x0501 

#ifdef _WIN32_WINDOWS
	#undef _WIN32_WINDOWS
#endif
#define _WIN32_WINDOWS 0x0510 

#ifdef _WIN32_IE
	#undef _WIN32_IE
#endif
#define _WIN32_IE 0x0600 
//////////////////////////////////////////////////////////////////////

#define WIN32_LEAN_AND_MEAN

#include <Windows.h>

#endif // #ifndef _INC_WINDOWS

#include <windowsx.h>

