#include "stdafx.h"
#include "pe_coff.h"
#include <stdio.h>


//////////////////////////////////////////////////
// Dump Info
LPCSTR w32::DumpInfo_PECoff_MachineName(UINT machine_tag)
{
	switch(machine_tag)
	{	case IMAGE_FILE_MACHINE_UNKNOWN	 : return "x000";			
		case IMAGE_FILE_MACHINE_I386     : return "i386";			
		case IMAGE_FILE_MACHINE_R3000    : return "MIPS R3000";		
		case IMAGE_FILE_MACHINE_R4000    : return "MIPS R4000";		
		case IMAGE_FILE_MACHINE_R10000   : return "MIPS R10000";		
		case IMAGE_FILE_MACHINE_WCEMIPSV2: return "MIPS WCE v2";		
		case IMAGE_FILE_MACHINE_ALPHA    : return "Alpha/AXP";			
		case IMAGE_FILE_MACHINE_SH3      : return "Hitachi SH3";		
		case IMAGE_FILE_MACHINE_SH3DSP   : return "Hitachi SH3(DSP)";	
		case IMAGE_FILE_MACHINE_SH3E     : return "Hitachi SH3E";		
		case IMAGE_FILE_MACHINE_SH4      : return "Hitachi SH4";		
		case IMAGE_FILE_MACHINE_SH5      : return "Hitachi SH5";		
		case IMAGE_FILE_MACHINE_ARM      : return "ARM";				
		case IMAGE_FILE_MACHINE_THUMB    : return "Thumb";				
		case IMAGE_FILE_MACHINE_AM33     : return "Matsushita AM33";	
		case IMAGE_FILE_MACHINE_POWERPC  : return "IBM PowerPC";		
		case IMAGE_FILE_MACHINE_POWERPCFP: return "IBM PowerPC (FPU)";	
		case IMAGE_FILE_MACHINE_IA64     : return "Intel Itanium 64";	
		case IMAGE_FILE_MACHINE_MIPS16   : return "MIPS16";			
		case IMAGE_FILE_MACHINE_ALPHA64  : return "ALPHA64";			
		case IMAGE_FILE_MACHINE_MIPSFPU  : return "MIPS (FPU)";		
		case IMAGE_FILE_MACHINE_MIPSFPU16: return "MIPS16 (FPU)";		
		case IMAGE_FILE_MACHINE_TRICORE  : return "Infineon";			
		case IMAGE_FILE_MACHINE_CEF      : return "xCEF";				
		case IMAGE_FILE_MACHINE_EBC      : return "EFI Byte Code";		
		case IMAGE_FILE_MACHINE_AMD64    : return "AMD64 (K8)";		
		case IMAGE_FILE_MACHINE_M32R     : return "Mitsubishi M32R";	
		case IMAGE_FILE_MACHINE_CEE      : return "xC0EE";
		default							 : return "Undefined";
	};
}

void w32::DumpInfo_PECoff_Basic(const CPECoffModule& pefile)
{
	printf("\nImage Type :  ");
	if(pefile.IsCOFF())
		printf("Common Object File (OBJ)");
	else
	{	if(pefile.m_BasicInfo.Characteristics & IMAGE_FILE_EXECUTABLE_IMAGE)
		{	if(pefile.m_BasicInfo.Characteristics & IMAGE_FILE_DLL)
				printf("Dynamic-Link Library (DLL)");
			else
				printf("Executable File (EXE)");
		}else{	printf("Link Incomplete"); }
	}

	printf("\nPlatform   :  %s",DumpInfo_PECoff_MachineName(pefile.m_BasicInfo.TargetMachine));

	printf("\nBuild Time :  ");
	{	struct tm * pLT = localtime(&pefile.m_BasicInfo.TimeDateStamp);
		printf("%02d/%02d/%04d  %02d:%02d %cM",
				pLT->tm_mon + 1,
				pLT->tm_mday,
				pLT->tm_year + 1900,
				pLT->tm_hour%12,
				pLT->tm_min,
				pLT->tm_hour>12?'P':'A');
	}

	printf("\nSection Num:  %d\nSymbol  Num:  %d",pefile.GetRawSectionCount(),pefile.GetSymbolCount());
	printf("\nOpt. Header:  ");
	switch(pefile.m_OptionalHeader.opt_header_type)
	{	
	case IMAGE_NT_OPTIONAL_HDR32_MAGIC:	printf("Win32"); break;
	case IMAGE_NT_OPTIONAL_HDR64_MAGIC:	printf("Win64"); break;
	case IMAGE_ROM_OPTIONAL_HDR_MAGIC:	printf("ROM");	 break;
	case 0:								printf("N/A");	 break;
	default: 
		printf("Unknown (%dB)",pefile.m_BasicInfo.SizeOfOptionalHeader);
		break;
	}

	if( pefile.m_OptionalHeader.opt_header_type )
	{	printf("\nLinker  Ver:  %d.%d",pefile.m_OptionalHeader.opt_header_rom.MajorLinkerVersion,
										pefile.m_OptionalHeader.opt_header_rom.MinorLinkerVersion);
		//printf("\nCode Entry :  [%08X:%08X]",pefile.m_OptionalHeader.opt_header_rom.AddressOfEntryPoint,
		//						pefile.RVA2OFB(pefile.m_OptionalHeader.opt_header_rom.AddressOfEntryPoint));
		//printf("\nCode Base  :  [%08X:%08X]",pefile.m_OptionalHeader.opt_header_rom.BaseOfCode,
		//						pefile.RVA2OFB(pefile.m_OptionalHeader.opt_header_rom.BaseOfCode));
		printf("\nCode Size  :  %dB",pefile.m_OptionalHeader.opt_header_rom.SizeOfCode);
		printf("\nData Size  :  %dB/%dB (Init/BSS)",pefile.m_OptionalHeader.opt_header_rom.SizeOfInitializedData,
													pefile.m_OptionalHeader.opt_header_rom.SizeOfUninitializedData);
	}
}

void w32::DumpInfo_PECoff_RawSection(const CPECoffModule& pefile)
{
	for(UINT i=0;i<pefile.m_RawSections.GetSize();i++)
	{
		printf("\n%8s  ",pefile.m_RawSections[i].Name);
		putchar((pefile.m_RawSections[i].Characteristics & IMAGE_SCN_MEM_READ)?'R':'_');
		putchar((pefile.m_RawSections[i].Characteristics & IMAGE_SCN_MEM_WRITE)?'W':'_');
		putchar((pefile.m_RawSections[i].Characteristics & IMAGE_SCN_MEM_EXECUTE)?'E':'_');
		putchar(' ');
		putchar(' ');

		if(pefile.IsCOFF())
		{
			int AL = ((0x00F00000 & pefile.m_RawSections[i].Characteristics)>>20)-1;
			printf("OFB[%08X]  %4dAL  SIZE(%dB) ",
					pefile.m_RawSections[i].PointerToRawData,
					1<<AL,
					pefile.m_RawSections[i].SizeOfRawData);
		}
		else
		{
			printf("ADDR[%08X:%08X]  SIZE(%dB/%dB) ",
					pefile.m_RawSections[i].VirtualAddress,
					pefile.m_RawSections[i].PointerToRawData,
					pefile.m_RawSections[i].Misc.VirtualSize,
					pefile.m_RawSections[i].SizeOfRawData);
		}
	}
}

void w32::DumpInfo_LIB_ArchiveMember(const CLibrarianModule& libfile)
{
	UINT memco = libfile.GetArchiveMemberCount();
	for(UINT i=0;i<memco;i++)
	{	
		printf("\n%03d) [",i);
		DWORD flag = libfile.GetArchiveMemberFlag(i);
		if(flag != 0xffffffff)
		{	
			printf("%s] ",w32::DumpInfo_PECoff_MachineName(flag&w32::CLibrarianModule::ArchMem_MaskMachine));
			switch(flag & w32::CLibrarianModule::ArchMem_MaskImportType)
			{	
			case w32::CLibrarianModule::ArchMem_Object: 
				printf("COFF"); break;
			case w32::CLibrarianModule::ArchMem_ImportCode: 
				printf("iCOD"); break;
			case w32::CLibrarianModule::ArchMem_ImportData: 
				printf("iDAT"); break;
			case w32::CLibrarianModule::ArchMem_ImportConst: 
				printf("iCNST"); break;
			default: ASSERT(0);
			};
		}else{ printf("#Unknown Archive Content"); }
		printf(":%dB\t%s",libfile.GetArchiveMemberSize(i),libfile.GetArchiveMemberFullName(i));
	}
}

/////////////////////////////////////////////////
// CPECoffModule
void w32::CPECoffModule::Clear()
{
	m_BasicInfo.PE_HeaderOffset = LONG_MAX;
	m_OptionalHeader.opt_header_type = 0;
	//clear all buffered info
	m_RawSections.SetSize();
	m_SymbolTable.SetSize();
}

#define __Check(x)			if(!(x))goto PARSEPECOFF_ERROR;
#define __CheckFileRead		__Check(!f.ErrorOccured());
BOOL w32::CPECoffModule::ParsePECoff(CFile64& f, BOOL IsParsingPECOFF, UINT start)
{	ASSERT(f.IsOpen());
	Clear();
	if(IsParsingPECOFF)
	{
		//check old dos header
		IMAGE_DOS_HEADER dos;
		f.Read(&dos,sizeof(dos));
		__CheckFileRead;
		__Check(dos.e_magic==0x5a4d);
		m_BasicInfo.PE_HeaderOffset = dos.e_lfanew;

		//check PE sign
		f.Seek(m_BasicInfo.PE_HeaderOffset + start);
		
		DWORD sign=0;
		f.Read(&sign,4);
		__CheckFileRead;
		__Check(sign == IMAGE_NT_SIGNATURE);

		m_BasicInfo.PE_HeaderOffset+=4;
	}else{ m_BasicInfo.PE_HeaderOffset = 0; }

	//Load PECOFF header from current file position
	IMAGE_FILE_HEADER pe;
	{
		f.Read(&pe,sizeof(pe));
		__CheckFileRead;

		m_BasicInfo.TargetMachine = pe.Machine;
		m_BasicInfo.TimeDateStamp = pe.TimeDateStamp;
		m_BasicInfo.Characteristics = pe.Characteristics;
		m_BasicInfo.SizeOfOptionalHeader = pe.SizeOfOptionalHeader;
	}

	//Load optional header
	if( pe.SizeOfOptionalHeader )
	{	WORD opt_type;
		f.Read(&opt_type,sizeof(WORD));
		f.Seek(-(int)(sizeof(WORD)),w32::CFile64::Seek_Current);
		__CheckFileRead;
		switch(opt_type)
		{
		case IMAGE_NT_OPTIONAL_HDR32_MAGIC:
			f.Read(&m_OptionalHeader,sizeof(m_OptionalHeader.opt_header32));
			break;
		case IMAGE_NT_OPTIONAL_HDR64_MAGIC:
			f.Read(&m_OptionalHeader,sizeof(m_OptionalHeader.opt_header64));
			break;
		case IMAGE_ROM_OPTIONAL_HDR_MAGIC:
			f.Read(&m_OptionalHeader,sizeof(m_OptionalHeader.opt_header_rom));
			break;
		default:
			ZeroMemory(&m_OptionalHeader,sizeof(m_OptionalHeader));
			f.Read(&m_OptionalHeader,min(sizeof(m_OptionalHeader),pe.SizeOfOptionalHeader));
			break;
		}
		__CheckFileRead;
	}else{ m_OptionalHeader.opt_header_type = 0; }

	//Load image sections
	{	DWORD NumberOfSections = pe.NumberOfSections;
		if( NumberOfSections && m_RawSections.SetSize(NumberOfSections) )
		{	
			f.Read(&m_RawSections[0],sizeof(IMAGE_SECTION_HEADER)*NumberOfSections);
			__CheckFileRead;
		}
	}
	
	//Load Symbol Table and symbol string table
	if( pe.NumberOfSymbols && m_SymbolTable.SetSize(pe.NumberOfSymbols) )
	{	
		//Symbol Table
		f.Seek(pe.PointerToSymbolTable + start);
		f.Read(&m_SymbolTable[0],sizeof(IMAGE_SYMBOL)*pe.NumberOfSymbols);
		__CheckFileRead;
		

		//string table
		DWORD string_table_len = 0;
		f.Read(&string_table_len,sizeof(DWORD));
		string_table_len-=4;

		__Check(m_SymbolStringTable.SetSize(string_table_len));
		f.Read(&m_SymbolStringTable[0],string_table_len);
		__CheckFileRead;
	}
	return TRUE;

PARSEPECOFF_ERROR:
	Clear();
	return FALSE;
}
#undef __Check
#undef __CheckFileRead

/////////////////////////////////////////////////
// CExecutableModule
BOOL w32::CExecutableModule::Open(LPCTSTR fn)
{
	Close();

	if(_pefile.Open(fn))
	{
		BOOL IsParsingCOFF = FALSE; // sign for paring common object file format
		{// is extend name ".obj" 
			LPCTSTR pext = _tcsrchr(fn,_T('.'));
			if(pext && _tcslen(pext)==4)
			{	IsParsingCOFF = ( pext[1] == _T('O') || pext[1] == _T('o') ) &&
								( pext[2] == _T('B') || pext[2] == _T('b') ) &&
								( pext[3] == _T('J') || pext[3] == _T('j') ) ;
			}
		}

		if(ParsePECoff(_pefile,!IsParsingCOFF))
		{	if(!IsParsingCOFF)
				return _SectMap.BuildSectionMap(m_RawSections,m_RawSections.GetSize(),_pefile);
			return TRUE;
		}
	}

	Close();
	return FALSE;
}


void w32::CExecutableModule::Close()
{	
	CPECoffModule::Clear();
	_SectMap.Clear();

	if(_pefile.IsOpen())
		_pefile.Close();
}

IMAGE_DATA_DIRECTORY* w32::CExecutableModule::GetDataDirectoryEntry(UINT entry)
{	ASSERT(entry<IMAGE_NUMBEROF_DIRECTORY_ENTRIES);
	IMAGE_DATA_DIRECTORY* pRet=NULL;
	if(m_OptionalHeader.opt_header_type == IMAGE_NT_OPTIONAL_HDR32_MAGIC)
		pRet = &m_OptionalHeader.opt_header32.DataDirectory[entry];
	else if(m_OptionalHeader.opt_header_type == IMAGE_NT_OPTIONAL_HDR64_MAGIC)
		pRet = &m_OptionalHeader.opt_header64.DataDirectory[entry];

	if(pRet && pRet->Size ==0)
		return NULL;
	else
		return pRet;
}

void w32::CExecutableModule::_AddrSpcConv::Clear()
{
	_SectionMap.SetSize(0);
	_LastSection = 0;
}

BOOL w32::CExecutableModule::_AddrSpcConv::BuildSectionMap(IMAGE_SECTION_HEADER* pSections,UINT len,w32::CFile64& file)
{
	if(_SectionMap.SetSize(len))
	{
		for(UINT i=0;i<len;i++)
		{
			_SectionMap[i].RVA_Start = pSections[i].VirtualAddress;
			_SectionMap[i].RVA_End = pSections[i].VirtualAddress + pSections[i].Misc.VirtualSize;
			_SectionMap[i].RawDataAddress = pSections[i].PointerToRawData;

			if(!_SectionMap[i].RawData.SetSize(pSections[i].SizeOfRawData) )
				return FALSE;

			if(_SectionMap[i].RawData.GetSize())
			{	file.Seek(_SectionMap[i].RawDataAddress);
				file.Read(_SectionMap[i].RawData,_SectionMap[i].RawData.GetSize());
				if(file.ErrorOccured())
				{	_SectionMap[i].RawData.SetSize();
					file.ClearError();
				}
			}
		}

		//Sorting, bubble
		BOOL exchanged;
		do
		{	exchanged = FALSE;
			for(UINT i=1;i<len;i++)
			{
				if(_SectionMap[i].RVA_Start > _SectionMap[i-1].RVA_Start){}
				else
				{//swap
					rt::Swap(_SectionMap[i],_SectionMap[i-1]);
					exchanged = TRUE;
				}
			}
		}while(exchanged);

		_LastSection = 0;

		return TRUE;
	}
	return FALSE;
}


DWORD w32::CExecutableModule::_AddrSpcConv::RVA2OFB(DWORD rva)
{
	ASSERT(_LastSection>=0 && _LastSection<_SectionMap.GetSize());

	if(	rva >= _SectionMap[_LastSection].RVA_Start && 
		rva <  _SectionMap[_LastSection].RVA_End )
	{	
		return _SectionMap[_LastSection].RawDataAddress + (rva - _SectionMap[_LastSection].RVA_Start);
	}

	for(UINT i=0;i<_SectionMap.GetSize();i++)
	{
		if(_SectionMap[i].RVA_Start>rva || _SectionMap[i].RVA_End<=rva){}
		else
		{	_LastSection = i;
			return _SectionMap[i].RawDataAddress + (rva - _SectionMap[i].RVA_Start);
		}
	}

	return 0;
}

LPBYTE w32::CExecutableModule::_AddrSpcConv::RVA2PTR(DWORD rva)
{
	ASSERT(_LastSection>=0 && _LastSection<_SectionMap.GetSize());

	if(	rva >= _SectionMap[_LastSection].RVA_Start && 
		rva <  _SectionMap[_LastSection].RVA_End )
	{	
		UINT offset = rva - _SectionMap[_LastSection].RVA_Start;
		if( _SectionMap[_LastSection].RawData.GetSize()>offset )
			return &_SectionMap[_LastSection].RawData[offset];
		else
			return NULL;
	}

	for(UINT i=0;i<_SectionMap.GetSize();i++)
	{
		if(_SectionMap[i].RVA_Start>rva || _SectionMap[i].RVA_End<=rva){}
		else
		{	
			_LastSection = i;
			UINT offset = rva - _SectionMap[_LastSection].RVA_Start;
			if( _SectionMap[_LastSection].RawData.GetSize()>offset )
				return &_SectionMap[_LastSection].RawData[offset];
		else
			return NULL;
		}
	}

	return NULL;
}


//#define IMAGE_DIRECTORY_ENTRY_EXPORT        
//#define IMAGE_DIRECTORY_ENTRY_IMPORT        
//#define IMAGE_DIRECTORY_ENTRY_RESOURCE      
//#define IMAGE_DIRECTORY_ENTRY_EXCEPTION     
//#define IMAGE_DIRECTORY_ENTRY_SECURITY      
//#define IMAGE_DIRECTORY_ENTRY_BASERELOC     
//#define IMAGE_DIRECTORY_ENTRY_DEBUG         
////      IMAGE_DIRECTORY_ENTRY_COPYRIGHT     
//#define IMAGE_DIRECTORY_ENTRY_ARCHITECTURE  
//#define IMAGE_DIRECTORY_ENTRY_GLOBALPTR     
//#define IMAGE_DIRECTORY_ENTRY_TLS           
//#define IMAGE_DIRECTORY_ENTRY_LOAD_CONFIG   
//#define IMAGE_DIRECTORY_ENTRY_BOUND_IMPORT  
//#define IMAGE_DIRECTORY_ENTRY_IAT           
//#define IMAGE_DIRECTORY_ENTRY_DELAY_IMPORT  
//#define IMAGE_DIRECTORY_ENTRY_COM_DESCRIPTOR

BOOL w32::CExecutableModule::Parse_Directory_IMPORT()
{
	IMAGE_DATA_DIRECTORY* pdd = GetDataDirectoryEntry(IMAGE_DIRECTORY_ENTRY_IMPORT);
	if(!pdd)return FALSE;
	
	//go parse
	return TRUE;
}

BOOL w32::CExecutableModule::Parse_Directory_EXPORT()
{
	IMAGE_DATA_DIRECTORY* pdd = GetDataDirectoryEntry(IMAGE_DIRECTORY_ENTRY_EXPORT);
	if(!pdd)return FALSE;
	
	//parse directoty header
	IMAGE_EXPORT_DIRECTORY *ed = (IMAGE_EXPORT_DIRECTORY*)_SectMap.RVA2PTR(pdd->VirtualAddress);
	{
		m_Directory_Export.Header = ed;
		if(!m_Directory_Export.Header)return FALSE;
	}

	//Load all functions entrypoint
	if(ed->NumberOfFunctions)
	{	LPDWORD FnEntryPoint = (LPDWORD)_SectMap.RVA2PTR(ed->AddressOfFunctions);
		LPWORD  Ordinal2Name = (LPWORD)_SectMap.RVA2PTR(ed->AddressOfNameOrdinals);
		LPDWORD Names = (LPDWORD)_SectMap.RVA2PTR(ed->AddressOfNames);

		if( FnEntryPoint )
		{	if(m_Directory_Export.Functions.SetSize(ed->NumberOfFunctions))
			{
				for(UINT i=0;i<ed->NumberOfFunctions;i++)
				{
					m_Directory_Export.Functions[i].Entrypoint = FnEntryPoint[i];
					m_Directory_Export.Functions[i].Ordinal = i+ed->Base;
					//search associated name
					if(Ordinal2Name && Names)
					{
						for (UINT j=0; j < ed->NumberOfNames; j++ )
							if ( Ordinal2Name[j] == i )
							{
								m_Directory_Export.Functions[i].Name = (LPSTR)_SectMap.RVA2PTR(Names[j]);
								goto GO_NEXT_FUNCTION;
							}
					}
					//search failed, the function has no name
					m_Directory_Export.Functions[i].Name = NULL;
GO_NEXT_FUNCTION:
					continue;
				}
			}else{ return FALSE; }
		}
	}

	return TRUE;
}

BOOL w32::CExecutableModule::Parse_Directory_RESOURCE()
{
	IMAGE_DATA_DIRECTORY* pdd = GetDataDirectoryEntry(IMAGE_DIRECTORY_ENTRY_RESOURCE);
	if(!pdd)return FALSE;
	
	//go parse
	return TRUE;
}



/////////////////////////////////////////////////
// CLibrarianModule
#define __Check(x)			if(!(x))goto LIBRARIAN_ERROR;
#define __CheckFileRead		__Check(!_pefile.ErrorOccured());
BOOL w32::CLibrarianModule::Open(LPCTSTR fn)
{	
	Close();
	if(_pefile.Open(fn))
	{	// Signature "!<arch>\n" 0x0a3e686372613c21
		ULONGLONG sign;
		_pefile.Read(&sign,sizeof(ULONGLONG));
		__Check(sign == 0x0a3e686372613c21);
		__CheckFileRead;

		// skip first linker member
		_ArchiveMemberHDR memhdr;
		_pefile.Read(&memhdr,sizeof(_ArchiveMemberHDR));
		__CheckFileRead;
		__Check(memhdr.Name[0] == '/');
		__Check(memhdr.FixupEndOfHdr());

		UINT sz = atoi(memhdr.Size);
		__Check(sz>0);
		if(sz&1)sz++;
		_pefile.Seek(sz,w32::CFile64::Seek_Current);

		// Load Second linker member
		rt::Buffer<DWORD>		ArchiveMemberOffset;
		rt::Buffer<WORD>		SymbolOffset;

		_pefile.Read(&memhdr,sizeof(_ArchiveMemberHDR));
		__CheckFileRead;
		__Check(memhdr.Name[0] == '/');
		__Check(memhdr.FixupEndOfHdr());
		{
			// member list
			_pefile.Read(&sz,4);
			__CheckFileRead;
			__Check(ArchiveMemberOffset.SetSize(sz));
			_pefile.Read(&ArchiveMemberOffset[0],sizeof(DWORD)*sz);
			__CheckFileRead;

			// symbol list
			_pefile.Read(&sz,4);
			__CheckFileRead;
			__Check(SymbolOffset.SetSize(sz));
			_pefile.Read(&SymbolOffset[0],sizeof(WORD)*sz);
			__CheckFileRead;
			
			// string table
			sz = atoi(memhdr.Size);
			UINT sz_past =	ArchiveMemberOffset.GetSize()*sizeof(DWORD) + 4 +
							SymbolOffset.GetSize()*sizeof(WORD) + 4;
			__Check(sz > sz_past);
			sz = sz - sz_past;
			if(sz&1)sz++;
			__Check(m_SymbolStringTable.SetSize(sz));
			_pefile.Read(&m_SymbolStringTable[0],m_SymbolStringTable.GetSize());
			__CheckFileRead;
		}

		// Longnames Member list
		_pefile.Read(&memhdr,sizeof(_ArchiveMemberHDR));
		__CheckFileRead;
		if(memhdr.Name[0] == '/' && memhdr.Name[1] == '/')
		{
			__Check(memhdr.FixupEndOfHdr());
			sz = atoi(memhdr.Size);
			if(sz&1)sz++;
			__Check(m_LongNameTable.SetSize(sz));
			_pefile.Read(&m_LongNameTable[0],sz);
			__CheckFileRead;
		}// Longnames Member list doesn't exists
		else{ m_LongNameTable.SetSize(); }

		// Build member list
		__Check(m_ArchiveMembers.SetSize(ArchiveMemberOffset.GetSize()));
		WORD	ContentHeader[10];
		for(UINT i=0;i<ArchiveMemberOffset.GetSize();i++)
		{	_pefile.Seek(ArchiveMemberOffset[i]);
			_pefile.Read(&memhdr,sizeof(_ArchiveMemberHDR));
			__CheckFileRead;
			__Check(memhdr.FixupEndOfHdr());

			UINT content_len = atoi(memhdr.Size);
			m_ArchiveMembers[i].Size = content_len;
			m_ArchiveMembers[i].Offset = (DWORD)_pefile.GetCurrentPosition();

			LPCSTR pSN;
			pSN = m_ArchiveMembers[i].Name;
			if( memhdr.Name[0] == '/' )
			{	
				if(memhdr.Name[1] == ' ')
					strcpy(m_ArchiveMembers[i].Name,"[#LNK]");
				else
				{//refer to Longnames Member list
                    m_ArchiveMembers[i].Name[0] = '/';
					UINT offset = atoi(&memhdr.Name[1]);
					if(offset < m_LongNameTable.GetSize())
					{	m_ArchiveMembers[i].NoRef = FALSE;
						m_ArchiveMembers[i].NameRef = &m_LongNameTable[offset];
						pSN = &m_LongNameTable[offset];
					}
					else
						strcpy(m_ArchiveMembers[i].Name,"[#ERR]");
				}
			}
			else
			{	LPSTR p = strchr(memhdr.Name,'/');
				if(p)*p = 0;
				strcpy(m_ArchiveMembers[i].Name,memhdr.Name);
			}

			LPCSTR p1 = strrchr(pSN,'/');
			if(!p1){p1 = pSN;}else{p1++;}
			LPCSTR p2 = strrchr(pSN,'\\');
			if(!p2){p2 = pSN;}else{p2++;}

			m_ArchiveMembers[i].ShortName = max(p1,p2);

			//Update Flag
			if(content_len > 20)
			{	_pefile.Read(ContentHeader,20);
				if(	ContentHeader[0] == IMAGE_FILE_MACHINE_UNKNOWN &&
					ContentHeader[1] == 0xffff )
				{	// import library entry
					m_ArchiveMembers[i].Flag = ContentHeader[3] | 
											  ((ContentHeader[9]<<16) & ArchMem_MaskImportType);
				}
				else
				{	// common library entry
					m_ArchiveMembers[i].Flag = ContentHeader[0] | ArchMem_Object;
				}
			}else{ m_ArchiveMembers[i].Flag = 0xffffffff; }
		}

		// Build symbol list
		__Check(m_Symbols.SetSize(SymbolOffset.GetSize()));
		LPCSTR pName = m_SymbolStringTable.Begin();
		LPCSTR pNameEnd = m_SymbolStringTable.End();
		for(UINT i=0;i<SymbolOffset.GetSize();i++)
		{	m_Symbols[i].Name = pName;
			m_Symbols[i].ArchiveMemberId = SymbolOffset[i]-1;

			pName = &pName[strlen(pName)+1];
			if(pName < pNameEnd){}
			else{ pName = "[#OVERFLOW]"; }
		}

		return TRUE;
	}
LIBRARIAN_ERROR:
	Close();
	return FALSE;
}
#undef __Check
#undef __CheckFileRead

void w32::CLibrarianModule::Close()
{
	m_SymbolStringTable.SetSize();
	m_LongNameTable.SetSize();
	m_ArchiveMembers.SetSize();

	if(_pefile.IsOpen())
		_pefile.Close();
}


LPCSTR w32::CLibrarianModule::GetArchiveMemberFullName(UINT idx) const
{	ASSERT(idx<m_ArchiveMembers.GetSize());
	if(m_ArchiveMembers[idx].NoRef)
		return m_ArchiveMembers[idx].Name;
	else
		return m_ArchiveMembers[idx].NameRef;
}


BOOL w32::CLibrarianModule::GetArchiveMemberRawData(UINT idx, LPBYTE pData, UINT len)
{	
	len = min(GetArchiveMemberSize(idx),len);
	ASSERT_ARRAY(pData,len);

	_pefile.ClearError();
	_pefile.Seek(m_ArchiveMembers[idx].Offset);
	if(_pefile.ErrorOccured())return FALSE;
	_pefile.Read(pData,len);
	if(_pefile.ErrorOccured())return FALSE;

	return TRUE;
}




















