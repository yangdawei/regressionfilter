#pragma once

//////////////////////////////////////////////////////////////////////
// Visual Computing Foundation Classes (VCFC)
//					Jiaping Wang  2006.2
//					e_boris2002@hotmail.com
//
// (C) Copyright Jiaping Wang 2004.
//
// Permission to copy, use, modify, sell and distribute this software
// is granted provided this copyright notice appears in all copies.
// This software is provided "as is" without express or implied
// warranty, and with no claim as to its suitability for any purpose.
//
// Absolutely no warranty!!
//////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////
//  Brief Description
//
//  inet_navi.h
//
//  A simple text based web browser, targating on crawler
//
//  Library Dependence: inet.lib
//
//////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////
// Revise History
//
// Inital version	2010.7.4		Jiaping
//
//////////////////////////////////////////////////////////////////////

#include "inet.h"
#include "xml_soap.h"



namespace w32
{

//////////////////////////////////////////////////////////
// What has been handled:
// 1. HTTP 3xx redirection with Location
// 2. HTML http-equiv="refresh" redirection
//
//

class CHttpNavigator	// a simple web navigator with redirection and cookie
{
	rt::StringA			_AdditionalHeader;
	rt::StringA			_Refer;
	void				_UpdateAdditionalHeader();
	rt::_reflection*	_pCallback;

protected:
	rt::StringA		m_NavigatedDestination;
	CHttpSession	m_HttpSession;

protected:
	void	SetRedirected(const rt::StringA_Ref& url);

public:
	DWORD	GetIrresponsiveTime() const { return m_HttpSession.GetIrresponsiveTime(); }	// msec
	void	CancelResponseWaiting(){ m_HttpSession.CancelResponseWaiting(); }
	BOOL	NavigateTo(LPCSTR pURL, int max_redirection_times = 8);	// automatic redirection (3xx) handling, cookie storage
	const rt::StringA& 
			GetNavigateDestination() const { return m_NavigatedDestination; };	// final redirected URL

public:
	LPBYTE				GetResponse(){ return m_HttpSession.GetResponse(); }
	UINT				GetResponseLength(){ return m_HttpSession.GetResponseLength(); }
	rt::StringA_Ref		GetDocument(){ return rt::StringA_Ref((LPCSTR)GetResponse(),GetResponseLength()); }

	CHttpNavigator();

	void	SetCommonHeaders(LPCSTR agent, BOOL keepalive = FALSE){ m_HttpSession.SetCommonHeaders(agent,keepalive); }
	void	SetReferer(LPCSTR referer);
	void	SetEventCallback(rt::_reflection* p){ m_HttpSession.SetEventSink(_pCallback = p); }
	
	LPCSTR	GetResponseHeader(){ return m_HttpSession.GetResponseHeader(); }
	UINT	GetResponseHeaderLength(){ return m_HttpSession.GetResponseHeaderLength(); }
	UINT	GetResponseStatusCode(){ return m_HttpSession.GetResponseParsedHeader().m_StateCode; }
	BOOL	IsResponseSuccess() const { return m_HttpSession.GetResponseParsedHeader().m_StateCode/100 == 2; }
};


enum	// values provided in msg of OnReflect
{	
	HTTPCLIENT_EVENT_REDIRECT = 100				// (LPCSTR)param is the target url
};



} // namespace w32

