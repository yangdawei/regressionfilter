#pragma once

//////////////////////////////////////////////////////////////////////
// Visual Computing Foundation Classes (VCFC)
//					Jiaping Wang  2006.2
//					e_boris2002@hotmail.com
//
// (C) Copyright Jiaping Wang 2004.
//
// Permission to copy, use, modify, sell and distribute this software
// is granted provided this copyright notice appears in all copies.
// This software is provided "as is" without express or implied
// warranty, and with no claim as to its suitability for any purpose.
//
// Absolutely no warranty!!
//////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////
//  Brief Description
//
//  debug_log.h
//
//  debug helpers and logging
//
//	Optional Macros
//	W32_DISABLE_LOGGING		enable log output to console
//
//////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////
// Revise History
//
// Inital version	2006.6.8		Jiaping
//					2009.4.10		Jiaping Remove	W32_RUNTIME_LOGGING_FILE
//											Remove	W32_RUNTIME_LOGGING_CONSOLE
//											Add		W32_DISABLE_LOGGING
//
//////////////////////////////////////////////////////////////////////

#include "win32_ver.h"
#include "w32_basic.h"


namespace w32
{

//////////////////////////////////////////////////////////
// Logging output
#ifndef W32_DISABLE_LOGGING
	#define _CheckDump(x)	(_STD_OUT<<x)
#else
	#define _CheckDump(x)	while(0)
#endif

#ifndef W32_DISABLE_LOGGING
	#define	_CheckDumpSrcLoc	{::w32::Logging::DumpSourceLocation(__LINE__,_T(__FILE__),_T(__FUNCTION__));}
	#define _CheckErrorW32		{if(::w32::Logging::w32CheckError()){ _CheckDumpSrcLoc; }}
	#define _CheckDumpCleanLine	{::w32::Logging::DumpCleanLine();}
	#define _CheckPointUI		{::w32::Logging::PopupSourceLocation(__LINE__,_T(__FILE__),_T(__FUNCTION__));}
#else
	#define _CheckErrorW32		while(0)
	#define	_CheckDumpSrcLoc	while(0)
	#define _CheckDumpCleanLine	while(0)
	#define _CheckPointUI		while(0)
#endif

//Common helper Macros

#define ERRMSG_HRESULT(re)	((LPCTSTR)::w32::HResultReport(re))
#define ERRMSG(scode)		ERRMSG_HRESULT( HRESULT_FROM_WIN32(scode) )
#define ERRMSG_LASTERR()	((LPCTSTR)::w32::HResultReport(w32::HresultFromLastError()))
#define REPORT_LAST_ERROR	{ _CheckDump(ERRMSG_LASTERR()<<_T('\n')); }

#define _CheckPoint			_CheckDumpSrcLoc

//////////////////////////////////////////////////////////
// HRESULT to Text
class HResultReport
{
protected:
	TCHAR m_ErrorMsg[MAX_PATH];
public:
	operator LPCTSTR()const { return m_ErrorMsg; }
	HResultReport(HRESULT hr);
};

//////////////////////////////////////////////////////////
// Enable Logging 
struct Logging
{	
	static BOOL	w32CheckError();
	static void	DumpSourceLocation(int line,LPCTSTR file,LPCTSTR func);
	static void	PopupSourceLocation(int line,LPCTSTR file,LPCTSTR func);
	static void DumpCleanLine();

	static void DumpCPUInfo();
	static void DumpOSInfo();
	static void DumpDisplayInfo();

protected:
#ifndef _WIN64
private:
	typedef	void (*DELAY_FUNC)(unsigned int uiMS);
	static __int64 __cdecl _GetCyclesDifference (DELAY_FUNC, unsigned int);
	static void _Delay (unsigned int);
	static void _DelayOverhead (unsigned int);
#endif // #ifndef _WIN64
};


//////////////////////////////////////////////////////////
// This function dynamically creates a "Console" window and points stdout and stderr to it.
// It also hooks stdin to the window
struct Console
{
	static BOOL IsCreated;
	static void Create(LPCTSTR Title = NULL,BOOL DisableClose = TRUE);
	static void Destroy();
	static void Active();
	static void Write(LPCTSTR string);
	static void SetTitle(LPCTSTR str);
	static BOOL IsAvailable();
};

extern void EnableCrashDump(LPCTSTR dump_filename = NULL, BOOL full_memory = FALSE, rt::_reflection* callback = NULL);
extern void DisableCrashDump();
extern BOOL CreateCrashDump(LPCTSTR dump_fn, BOOL full_memory = FALSE);

}


