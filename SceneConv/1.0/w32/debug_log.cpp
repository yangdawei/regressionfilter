#include "stdafx.h"
#include "debug_log.h"

#include <errno.h>
#include <fcntl.h>
#include <Wincon.h>
#include <io.h>
#include <Dbghelp.h>
#pragma comment(lib,"Dbghelp.lib")

BOOL w32::Console::IsCreated = FALSE;

void w32::Console::Create(LPCTSTR Title,BOOL DisableClose)
{
	IsCreated = TRUE;

#pragma warning(disable:4311)
	if(IsAvailable())return;

	HANDLE hConsole = NULL;

    int  hConHandle;
    long lStdHandle;
    CONSOLE_SCREEN_BUFFER_INFO coninfo;
    FILE   *fp;
    // allocate a console for this app
	AllocConsole();
	// set the screen buffer to be big enough to let us scroll text
    GetConsoleScreenBufferInfo(GetStdHandle(STD_OUTPUT_HANDLE),	&coninfo);
    coninfo.dwSize.Y = 2000;
	// How many lines do you want to have in the console buffer
    SetConsoleScreenBufferSize(GetStdHandle(STD_OUTPUT_HANDLE),	coninfo.dwSize);
	// redirect unbuffered STDOUT to the console
    hConsole = GetStdHandle(STD_OUTPUT_HANDLE);
	lStdHandle = reinterpret_cast<long>( GetStdHandle(STD_OUTPUT_HANDLE) );
    hConHandle = _open_osfhandle(lStdHandle, _O_TEXT);
    fp = _fdopen( hConHandle, "w" );
    *stdout = *fp;
    setvbuf( stdout, NULL, _IONBF, 0 ); 
	// redirect unbuffered STDIN to the console
    lStdHandle = reinterpret_cast<long>(GetStdHandle(STD_INPUT_HANDLE)); 
	hConHandle = _open_osfhandle(lStdHandle, _O_TEXT);
    fp = _fdopen( hConHandle, "r" ); 
	*stdin = *fp;
    setvbuf( stdin, NULL, _IONBF, 0 );
    // redirect unbuffered STDERR to the console
    lStdHandle = reinterpret_cast<long>(GetStdHandle(STD_ERROR_HANDLE)); 
	hConHandle = _open_osfhandle(lStdHandle, _O_TEXT);
    fp = _fdopen( hConHandle, "w" );
    *stderr = *fp;
    setvbuf( stderr, NULL, _IONBF, 0 );

	// make cout, wcout, cin, wcin, wcerr, cerr, wclog and clog point to console as well
	std::ios::sync_with_stdio();

	HWND hConsoleWnd = ::GetConsoleWindow();
	ASSERT(hConsoleWnd);

#ifndef W32_RUNTIME_LOGGING_WITH_CLOSE
	if(DisableClose)
	{	HMENU Menu= ::GetSystemMenu(hConsoleWnd,FALSE);
		if(Menu)
			::ModifyMenu(Menu,SC_CLOSE,MF_BYCOMMAND|MF_GRAYED|MF_DISABLED,NULL,NULL);
	}
#endif

	if(Title)
		::SetWindowText(hConsoleWnd,Title);
	else
		::SetWindowText(hConsoleWnd,_T("Debug Logging Output"));
#pragma warning(default:4311)
}

void w32::Console::Destroy()
{
	::FreeConsole();
}

void w32::Console::Write(LPCTSTR string)
{
	::WriteConsole(GetStdHandle(STD_OUTPUT_HANDLE),string,(DWORD)_tcslen(string),NULL,NULL);
}

void w32::Console::Active()
{	
	HWND hConsole = ::GetConsoleWindow();
	if(hConsole)
		::BringWindowToTop(hConsole);
}

void w32::Console::SetTitle(LPCTSTR str)
{	
	HWND hConsole = ::GetConsoleWindow();
	if(hConsole)
		::SetWindowText(hConsole,str);
}

BOOL w32::Console::IsAvailable()
{
	return ::GetConsoleWindow()!=NULL;
}

namespace w32
{
	static TCHAR g_DumpFilename[MAX_PATH] = {_T('\0')};
	static rt::_reflection* g_Callback = NULL;
	static DWORD g_DumpFlag = 0;
	BOOL _WriteCrashDump(LPCTSTR fn, PEXCEPTION_POINTERS ExceptionInfo, DWORD dump_flag)
	{
		HANDLE hFile;
		hFile = ::CreateFile(fn,FILE_ALL_ACCESS,FILE_SHARE_DELETE|FILE_SHARE_READ|FILE_SHARE_WRITE,NULL,OPEN_ALWAYS,0,NULL);
		if(hFile != INVALID_HANDLE_VALUE)
		{
			BOOL ret;
			if(ExceptionInfo)
			{
				MINIDUMP_EXCEPTION_INFORMATION ei;
				ei.ThreadId = ::GetCurrentThreadId();
				ei.ExceptionPointers = ExceptionInfo;
				ei.ClientPointers = TRUE;

				ret = MiniDumpWriteDump(::GetCurrentProcess(),::GetCurrentProcessId(),
										hFile,	(MINIDUMP_TYPE)(dump_flag),
										&ei,	NULL,	NULL);
			}
			else
			{
				ret = MiniDumpWriteDump(::GetCurrentProcess(),::GetCurrentProcessId(),
										hFile,	(MINIDUMP_TYPE)(dump_flag),
										NULL,	NULL,	NULL);
			}

			::CloseHandle(hFile);
			return ret;
		}
		return FALSE;
	}
}

void w32::EnableCrashDump(LPCTSTR dump_filename, BOOL full_memory, rt::_reflection* callback)
{
	_tcscpy_s(g_DumpFilename,sizeofArray(g_DumpFilename),dump_filename);

	struct _expception_handler
	{	static LONG WINAPI handler(struct _EXCEPTION_POINTERS *ExceptionInfo)
		{	
			static BOOL bInHandler = FALSE;

			if(!bInHandler)
			{	
				bInHandler = TRUE;

				if(g_DumpFilename)
					_WriteCrashDump(g_DumpFilename, ExceptionInfo, g_DumpFlag);

				if(g_Callback)
					g_Callback->OnReflect(ExceptionInfo);

				bInHandler = FALSE;
			}
			return EXCEPTION_EXECUTE_HANDLER;
		}
	};

	g_Callback = callback;
	g_DumpFlag = MiniDumpNormal|MiniDumpWithHandleData|MiniDumpWithProcessThreadData;
	if(full_memory)g_DumpFlag |= MiniDumpWithDataSegs|MiniDumpWithFullMemory;

	SetUnhandledExceptionFilter(_expception_handler::handler);
}

BOOL w32::CreateCrashDump(LPCTSTR dump_fn, BOOL full_memory)
{
	DWORD _DumpFlag = MiniDumpNormal|MiniDumpWithHandleData|MiniDumpWithProcessThreadData;
	if(full_memory)_DumpFlag |= MiniDumpWithDataSegs|MiniDumpWithFullMemory;

	return _WriteCrashDump(dump_fn, NULL, _DumpFlag);
}


void w32::DisableCrashDump()
{
	SetUnhandledExceptionFilter(NULL);
}


w32::HResultReport::HResultReport(HRESULT hr)
{	LPCTSTR ge_str = _T("[GENERIC]: ");
	switch(hr)
	{
	case E_UNEXPECTED:	_stprintf(m_ErrorMsg,_T("%sUnexpected."),ge_str); return;
	case E_NOTIMPL:		_stprintf(m_ErrorMsg,_T("%sNot implemented."),ge_str); return;
	case E_INVALIDARG:	_stprintf(m_ErrorMsg,_T("%sOne or more arguments are invalid."),ge_str); return;
	case E_NOINTERFACE:	_stprintf(m_ErrorMsg,_T("%sNo such interface supported."),ge_str); return;
	case E_POINTER:		_stprintf(m_ErrorMsg,_T("%sInvalid pointer."),ge_str); return;
	case E_HANDLE:		_stprintf(m_ErrorMsg,_T("%sInvalid handle."),ge_str); return;
	case E_ABORT:		_stprintf(m_ErrorMsg,_T("%sOperation aborted."),ge_str); return;
	case E_FAIL:		_stprintf(m_ErrorMsg,_T("%sUnspecified error."),ge_str); return;
	case E_ACCESSDENIED:_stprintf(m_ErrorMsg,_T("%sGeneral access denied."),ge_str); return;
	case E_PENDING:		_stprintf(m_ErrorMsg,_T("%sData is yet available, operation pending."),ge_str); return;
	}
		
	LPCTSTR FaciName;
	switch(HRESULT_FACILITY(hr))
	{
	case FACILITY_WINDOWS_CE           : FaciName = _T("WINDOWS_CE"); break;
	case FACILITY_WINDOWS              : FaciName = _T("WINDOWS"); break;
	case FACILITY_URT                  : FaciName = _T("URT"); break;
	case FACILITY_UMI                  : FaciName = _T("UMI"); break;
	case FACILITY_SXS                  : FaciName = _T("SXS"); break;
	case FACILITY_STORAGE              : FaciName = _T("STORAGE"); break;
	case FACILITY_STATE_MANAGEMENT     : FaciName = _T("STATE_MANAGEMENT"); break;
	case FACILITY_SCARD                : FaciName = _T("SCARD"); break;
	case FACILITY_SETUPAPI             : FaciName = _T("SETUPAPI"); break;
	case FACILITY_SECURITY             : FaciName = _T("SECURITY"); break;
	case FACILITY_RPC                  : FaciName = _T("RPC"); break;
	case FACILITY_WIN32                : FaciName = _T("WIN32"); break;
	case FACILITY_CONTROL              : FaciName = _T("CONTROL"); break;
	case FACILITY_NULL                 : FaciName = _T("GENERIC"); break;
	case FACILITY_METADIRECTORY        : FaciName = _T("METADIRECTORY"); break;
	case FACILITY_MSMQ                 : FaciName = _T("MSMQ"); break;
	case FACILITY_MEDIASERVER          : FaciName = _T("MEDIASERVER"); break;
	case FACILITY_INTERNET             : FaciName = _T("INTERNET"); break;
	case FACILITY_ITF                  : FaciName = _T("COM"); break;
	case FACILITY_HTTP                 : FaciName = _T("HTTP"); break;
	case FACILITY_DPLAY                : FaciName = _T("DPLAY"); break;
	case FACILITY_DISPATCH             : FaciName = _T("DISPATCH"); break;
	case FACILITY_CONFIGURATION        : FaciName = _T("CONFIGURATION"); break;
	case FACILITY_COMPLUS              : FaciName = _T("COMPLUS"); break;
	case FACILITY_CERT                 : FaciName = _T("CERT"); break;
	case FACILITY_BACKGROUNDCOPY       : FaciName = _T("BACKGROUNDCOPY"); break;
	case FACILITY_ACS                  : FaciName = _T("ACS"); break;
	case FACILITY_AAF                  : FaciName = _T("AAF"); break;
	default: FaciName = _T("UNKNOWN");
	}

	UINT len_pass = _stprintf(m_ErrorMsg,_T("[%s (0x%08X)]: "),FaciName,hr);
	LPTSTR pMsg = &m_ErrorMsg[len_pass];

	if( FACILITY_WIN32 == HRESULT_FACILITY(hr) &&
		::FormatMessage(FORMAT_MESSAGE_FROM_SYSTEM,NULL,SCODE_CODE(hr),0,pMsg,MAX_PATH-len_pass,NULL) )
	{	//remove return char
		while(*pMsg)
		{
			if( *pMsg == _T('\r') || *pMsg == _T('\n') )*pMsg = _T(' ');
			pMsg++;
		}
	}
}


////////////////////////////////////////////////////////////////////////////////
// Logging: Dump system settings and configurations
// Also handles text storage for logging
static const LPCTSTR TextBar= _T(" Information\n==========================\n");

void w32::Logging::DumpOSInfo()
{
	_CheckDump(_T("OS")<<TextBar<<_T("Platform       : "));
	{
		OSVERSIONINFO osi;
		osi.dwOSVersionInfoSize = sizeof(OSVERSIONINFO);
		if(GetVersionEx(&osi))
		{
			_CheckDump(_T("Windows "));
			switch(osi.dwMajorVersion*100+osi.dwMinorVersion)
			{
			case 400:
				if(osi.dwPlatformId == VER_PLATFORM_WIN32_NT)
				{
					_CheckDump(_T("NT 4.0 "));
				}
				else
				{
					_CheckDump(_T("95 "));
				}
				break;
			case 410:
				_CheckDump(_T("98 "));
				break;
			case 490:
				_CheckDump(_T("Me "));
				break;
			case 351:
				_CheckDump(_T("NT 3.51 "));
				break;
			case 500:
				_CheckDump(_T("2000 "));
				break;
			case 501:
				_CheckDump(_T("XP "));
				break;
			default:
				if(osi.dwMajorVersion >= 5)
				{
					_CheckDump(_T("2000 Next-Generation Ver=")<<osi.dwMajorVersion);
				}
				else
				{
					_CheckDump(_T("Unknown Ver=")<<osi.dwMajorVersion);
				}
			}
		}
		else
		{
			_CheckDump(_T("Failed for ")<<ERRMSG(GetLastError()));
		}

		_CheckDump(osi.szCSDVersion);
		_CheckDump(_T("\nVersion        : "));
		_CheckDump(osi.dwMajorVersion<<_T('.')<<osi.dwMinorVersion<<_T('.')<<osi.dwBuildNumber<<_T('\n'));
	}

	////Memory
	{
		MEMORYSTATUS mi;
        
		GlobalMemoryStatus(&mi);
		_CheckDump(_T("Physical memory: ")<<(DWORD)((mi.dwAvailPhys>>20)+1)<<_T("MB Free of total ")<<(DWORD)((mi.dwTotalPhys>>20)+1)<<_T("MB\nVirtual memory : "));
		_CheckDump((DWORD)((mi.dwAvailVirtual>>20)+1)<<_T("MB Free of total ")<<(DWORD)((mi.dwTotalVirtual>>20)+1)<<_T("MB\n"));
	}

	//Desktop
	{
		RECT rc;
		VERIFY(SystemParametersInfo(SPI_GETWORKAREA,NULL,&rc,NULL));
		_CheckDump(_T("Desktop Size   : ")<<rc.right<<_T(" X ")<<rc.bottom);
	}

	_CheckDump(_T("\n\n\n"));
}

void w32::Logging::DumpDisplayInfo()
{
	_CheckDump(_T("Display")<<TextBar);

	DEVMODE dm;
	if(EnumDisplaySettings(NULL,ENUM_CURRENT_SETTINGS,&dm))
	{
		_CheckDump(_T("Device Name  : ")<<dm.dmDeviceName);
		_CheckDump(_T("\nColor Depth  : ")<<(int)dm.dmBitsPerPel);
		_CheckDump(_T("Bits\nResolution   : ")<<(int)dm.dmPelsWidth<<_T(" X ")<<(int)dm.dmPelsHeight);
		_CheckDump(_T("\nRefresh Freq.: ")<<(int)dm.dmDisplayFrequency<<_T("Hz\n\n\n"));
	}
	else
	{
		_CheckDump(_T("Failed for ")<<ERRMSG(GetLastError())<<_T('\n'));
	}
}

BOOL w32::Logging::w32CheckError()
{	
	DWORD ec;
	if((ec = GetLastError()) != ERROR_SUCCESS)
	{
		_CheckDump(_T("\n  Win32 error occurred: ")<<ERRMSG(ec)<<_T('\n'));
		SetLastError(ERROR_SUCCESS);
		return TRUE;
	}
	
	return FALSE;
}

void w32::Logging::DumpSourceLocation(int line,LPCTSTR file,LPCTSTR func)
{	
	LPCTSTR file_name = _tcsrchr(file,_T('\\'));
	if(file_name==NULL)
		file_name=file;
	else
		file_name++;

	_CheckDump(_T("  [")<<func<<_T("] L")<<line<<_T(":")<<file_name<<_T("\n")); 
}

void w32::Logging::PopupSourceLocation(int line,LPCTSTR file,LPCTSTR func)
{	
	LPCTSTR file_name = _tcsrchr(file,_T('\\'));
	if(file_name==NULL)
		file_name=file;
	else
		file_name++;

	TCHAR msg[MAX_PATH + 100];
	_stprintf(msg,_T("Paused At:\15\12\15\12Function: %s \15\12Location: L%d:%s"),func,line,file_name);

	::MessageBox(NULL,msg,_T("Check Point"),MB_OK|MB_ICONINFORMATION);
}

void w32::Logging::DumpCleanLine()
{
	_CheckDump(_T('\r')); 
	for(int i=0;i<79;i++)
		_CheckDump(_T(' '));
	_CheckDump(_T('\r')); 
}


#ifndef _WIN64
    #define	I32TO64(x, y)				((((__int64) x) << 32) + y)
    #define RDTSC_INSTRUCTION			_asm _emit 0x0f _asm _emit 0x31
#endif

void w32::Logging::DumpCPUInfo()
{
#ifndef _WIN64
	//OEM String
	_CheckDump(_T("CPU")<<TextBar);

	try
	{
		char Maker;   //G = Inter A = AMD  or other
		{
			char OEMString[13]; 

			_asm
			{ 
				mov eax,0 
				cpuid 
				mov DWORD PTR OEMString,ebx 
				mov DWORD PTR OEMString+4,edx 
				mov DWORD PTR OEMString+8,ecx 
				mov BYTE PTR OEMString+12,0 
			}

			Maker = OEMString[0];
			
			_CheckDump(_T("Manufacturer      : "));

			switch(Maker)
			{
			case 'G':
				_CheckDump(_T("Intel"));
				break;
			case 'A':
				_CheckDump(_T("AMD"));
				break;
			case 'U':
				_CheckDump(_T("UMC"));
				break;
			case 'C':
				_CheckDump(_T("Cyrix/IDT"));
				break;
			default:
				_CheckDump(_T("Other"));
			}

			_CheckDump(_T(" with OEM String [ ")<<OEMString<<_T(" ]\n"));
		}


		//CPU Model
		BOOL HasTSC = FALSE;
		{
			DWORD sEAX,sEBX,sEDX;
			_asm
			{
				mov eax,1
				cpuid
				mov DWORD PTR sEAX,eax
				mov DWORD PTR sEBX,ebx
				mov DWORD PTR sEDX,edx
			}

			DWORD ExtendedFamily =		((sEAX & 0x0FF00000) >> 20);	// Bits 27..20 Used
			//DWORD ExtendedModel =		((sEAX & 0x000F0000) >> 16);	// Bits 19..16 Used
			DWORD Type =				((sEAX & 0x0000F000) >> 12);	// Bits 15..12 Used
			DWORD Family =				((sEAX & 0x00000F00) >> 8);		// Bits 11..8 Used
			DWORD Model =				((sEAX & 0x000000F0) >> 4);		// Bits 7..4 Used
			DWORD Revision =			((sEAX & 0x0000000F) >> 0);		// Bits 3..0 Used

			_CheckDump(_T("Brand ID          : "));
			switch (Maker)
			{
				case 'G':  //Intel
					// Check the family / model / revision to determine the CPU ID.
					switch (Family) {
						case 3:
							_CheckDump(_T("i80386")); 
							break;
						case 4:
							switch (Model) 
							{
								case 0: _CheckDump(_T("i80486DX-25/33")); break;
								case 1: _CheckDump(_T("i80486DX-50")); break;
								case 2: _CheckDump(_T("i80486SX")); break;
								case 3: _CheckDump(_T("i80486DX2")); break;
								case 4: _CheckDump(_T("i80486SL")); break;
								case 5: _CheckDump(_T("i80486SX2")); break;
								case 7: _CheckDump(_T("i80486DX2 WriteBack")); break;
								case 8: _CheckDump(_T("i80486DX4")); break;
								case 9: _CheckDump(_T("i80486DX4 WriteBack")); break;
								default: _CheckDump(_T("Unknown 80486 family")); 
							}
							break;
						case 5:
							switch (Model) {
								case 0: _CheckDump(_T("P5 A-Step")); break;
								case 1: _CheckDump(_T("P5")); break;
								case 2: _CheckDump(_T("P54C")); break;
								case 3: _CheckDump(_T("P24T OverDrive")); break;
								case 4: _CheckDump(_T("P55C")); break;
								case 7: _CheckDump(_T("P54C")); break;
								case 8: _CheckDump(_T("P55C (0.25um)")); break;
								default: _CheckDump(_T("Unknown Pentium family")); 
							}
							break;
						case 6:
							switch (Model) {
								case 0: _CheckDump(_T("P6 A-Step")); break;
								case 1: _CheckDump(_T("P6")); break;
								case 3: _CheckDump(_T("PentiumII (0.28 um)")); break;
								case 5: _CheckDump(_T("PentiumII (0.25 um)")); break;
								case 6: _CheckDump(_T("PentiumII With On-Die L2 Cache")); break;
								case 7: _CheckDump(_T("PentiumIII (0.25 um)")); break;
								case 8: _CheckDump(_T("PentiumIII (0.18 um) With 256 KB On-Die L2 Cache ")); break;
								case 9: _CheckDump(_T("Centrino (Pentium-M  0.13 um)")); break;
								case 0xa: _CheckDump(_T("PentiumIII (0.18 um) With 1 Or 2 MB On-Die L2 Cache ")); break;
								case 0xb: _CheckDump(_T("PentiumIII (0.13 um) With 256 Or 512 KB On-Die L2 Cache ")); break;
								default: _CheckDump(_T("Unknown P6 family"));
							}
							break;
						case 7:
							_CheckDump(_T("Intel Merced (IA-64)"));
							break;
						case 0xf:
							// Check the extended family bits...
							switch (ExtendedFamily) 
							{
								case 0:
									switch (Model) 
									{
										case 0: _CheckDump(_T("PentiumIV (0.18 um)")); break;
										case 1: _CheckDump(_T("PentiumIV (0.18 um)")); break;
										case 2: _CheckDump(_T("PentiumIV (0.13 um) with Hyper Thread")); break;
										case 3: _CheckDump(_T("PentiumIV (0.13 um) with Hyper Thread II")); break;
										default: _CheckDump(_T("Unknown Pentium 4 family")); 
									}
									break;
								case 1:
									_CheckDump(_T("Intel McKinley (IA-64)"));
									break;
							}
							break;
						default:
							_CheckDump(_T("Unknown Intel family"));
					}
					break;

				case 'A':  //AMD
					// Check the family / model / revision to determine the CPU ID.
					switch (Family) {
						case 4:
							switch (Model) {
								case 3: _CheckDump(_T("80486DX2")); break;
								case 7: _CheckDump(_T("80486DX2 WriteBack")); break;
								case 8: _CheckDump(_T("80486DX4")); break;
								case 9: _CheckDump(_T("80486DX4 WriteBack")); break;
								case 0xe: _CheckDump(_T("5x86")); break;
								case 0xf: _CheckDump(_T("5x86WB")); break;
								default: _CheckDump(_T("Unknown 80486 family")); 
							}
							break;
						case 5:
							switch (Model) {
								case 0: _CheckDump(_T("SSA5 (PR75, PR90, PR100)")); break;
								case 1: _CheckDump(_T("5k86 (PR120, PR133)")); break;
								case 2: _CheckDump(_T("5k86 (PR166)")); break;
								case 3: _CheckDump(_T("5k86 (PR200)")); break;
								case 6: _CheckDump(_T("K6 (0.30 um)")); break;
								case 7: _CheckDump(_T("K6 (0.25 um)")); break;
								case 8: _CheckDump(_T("K6-2")); break;
								case 9: _CheckDump(_T("K6-III")); break;
								case 0xd: _CheckDump(_T("K6-2+ or K6-III+ (0.18 um)")); break;
								default: _CheckDump(_T("Unknown 80586 family")); 
							}
							break;
						case 6:
							switch (Model) {
								case 1: _CheckDump(_T("Athlon(0.25 um)")); break;
								case 2: _CheckDump(_T("Athlon(0.18 um)")); break;
								case 3: _CheckDump(_T("Duron(SF core)")); break;
								case 4: _CheckDump(_T("Athlon(Thunderbird core)")); break;
								case 6: _CheckDump(_T("Athlon(Palomino core)")); break;
								case 7: _CheckDump(_T("Duron(Morgan core)")); break;
								case 8: _CheckDump(_T("AthlonXP (Thoroughbred core)")); break;
								default: _CheckDump(_T("Unknown K7 family")); 
							}
							break;
						default:
							_CheckDump(_T("Unknown AMD family"));
					}
					break;
				default:
					_CheckDump(_T("Unknown family"));
			}

			_CheckDump(_T("\nType ID           : ")<<Type<<_T("   Family: ")<<Family<<_T("   Model: ")<<Model<<_T("   Stepping: ")<<Revision);

			_CheckDump(_T("\nExtend Features   :"));

			if((sEDX & 0x00000001))_CheckDump(_T(" FPU"));		// FPU Present --> Bit 0
			HasTSC = (BOOL)(sEDX & 0x00000010);
			if((sEDX & 0x00000200))_CheckDump(_T(" APIC"));		// APIC Present --> Bit 9
			if((sEDX & 0x00001000))_CheckDump(_T(" MTRR"));		// MTRR Present --> Bit 12
			if((sEDX & 0x00008000))_CheckDump(_T(" CMOV"));		// CMOV Present --> Bit 15
			if((sEDX & 0x00040000))_CheckDump(_T(" Serial"));	// Serial Present --> Bit 18
			if((sEDX & 0x00400000))_CheckDump(_T(" ACPI"));		// ACPI Capable --> Bit 22
			if((sEDX & 0x00800000))_CheckDump(_T(" MMX"));		// MMX Present --> Bit 23
			if((sEDX & 0x02000000))_CheckDump(_T(" SSE"));		// SSE Present --> Bit 25
			if((sEDX & 0x04000000))_CheckDump(_T(" SSE2"));		// SSE2 Present --> Bit 26
			if((sEDX & 0x20000000))_CheckDump(_T(" Thermal"));	// Thermal Monitor Present --> Bit 29
			if((sEDX & 0x40000000))_CheckDump(_T(" IA64"));		// IA64 Present --> Bit 30

			if(Maker == 'A')
			{DWORD Ef;
				_asm
				{
					mov eax,0x80000001
					cpuid
					mov DWORD PTR Ef, edx
				}

				if(Ef & 0x80000000)_CheckDump(_T(" 3DNow"));	// 3DNow Present --> Bit 31.
				if(Ef & 0x40000000)_CheckDump(_T(" 3DNow+"));	// 3DNow Present --> Bit 31.
			}

		}

		_CheckDump(_T("\nClock Frequence   : "));
		//Speed
		{
			if(HasTSC)
			{
				unsigned int uiRepetitions = 1;
				unsigned int uiMSecPerRepetition = 50;
				__int64	i64Total = 0, i64Overhead = 0;

				for (unsigned int nCounter = 0; nCounter < uiRepetitions; nCounter ++) 
				{
					i64Total += _GetCyclesDifference (_Delay, uiMSecPerRepetition);
					i64Overhead += _GetCyclesDifference (_DelayOverhead, uiMSecPerRepetition);
				}

				// Calculate the MHz speed.
				i64Total -= i64Overhead;
				i64Total /= uiRepetitions;
				i64Total /= uiMSecPerRepetition;

				// Save the CPU speed.
				_CheckDump(((double)i64Total / 1000.0)<<_T("Mhz"));
			}
			else
				_CheckDump(_T("Failed for no RDTSC instruction support"));
		}
	}
	catch(...)
	{
		_CheckDump(_T("Failed for no CPUID instruction support"));
	}

	_CheckDump(_T("\n\n\n"));
#endif //#ifndef _WIN64
}

#ifndef _WIN64

__int64	__cdecl w32::Logging::_GetCyclesDifference (DELAY_FUNC DelayFunction, unsigned int uiParameter)
{
	unsigned int edx1, eax1;
	unsigned int edx2, eax2;
		
	// Calculate the frequency of the CPU instructions.
	__try {
		_asm {
			push uiParameter		; push parameter param
			mov ebx, DelayFunction	; store func in ebx

			RDTSC_INSTRUCTION

			mov esi, eax			; esi = eax
			mov edi, edx			; edi = edx

			call ebx				; call the delay functions

			RDTSC_INSTRUCTION

			pop ebx

			mov edx2, edx			; edx2 = edx
			mov eax2, eax			; eax2 = eax

			mov edx1, edi			; edx2 = edi
			mov eax1, esi			; eax2 = esi
		}
	}

	// A generic catch-all just to be sure...
	__except (1) {
		return -1;
	}

	return (I32TO64 (edx2, eax2) - I32TO64 (edx1, eax1));
}

void w32::Logging::_Delay (unsigned int uiMS)
{
	LARGE_INTEGER Frequency, StartCounter, EndCounter;
	__int64 x;

	// Get the frequency of the high performance counter.
	if (!QueryPerformanceFrequency (&Frequency)) return;
	x = Frequency.QuadPart / 1000 * uiMS;

	// Get the starting position of the counter.
	QueryPerformanceCounter (&StartCounter);

	do {
		// Get the ending position of the counter.	
		QueryPerformanceCounter (&EndCounter);
	} while (EndCounter.QuadPart - StartCounter.QuadPart < x);
}

void w32::Logging::_DelayOverhead (unsigned int uiMS)
{
	LARGE_INTEGER Frequency, StartCounter, EndCounter;
	__int64 x;

	// Get the frequency of the high performance counter.
	if (!QueryPerformanceFrequency (&Frequency)) return;
	x = Frequency.QuadPart / 1000 * uiMS;

	// Get the starting position of the counter.
	QueryPerformanceCounter (&StartCounter);
	
	do {
		// Get the ending position of the counter.	
		QueryPerformanceCounter (&EndCounter);
	} while (EndCounter.QuadPart - StartCounter.QuadPart == x);
}

#endif //#ifndef _WIN64



