#include "stdafx.h"
#include "mysql_conn.h"

#include <assert.h>
#include <mysql.h>

namespace w32
{

void MySQL_Connection::_QRet::Clear()
{
	if(m_pResultSet)
	{
		mysql_free_result(m_pResultSet);
		m_pResultSet = NULL;
	}

}

MySQL_Connection::MySQL_Connection()
{
	m_pConn = NULL;
	m_bTranscation = FALSE;
	m_bConnected = FALSE;
}

MySQL_Connection::~MySQL_Connection()
{
	Destroy();
}

BOOL MySQL_Connection::ConnectTo(LPCSTR user, LPCSTR password, LPCSTR server_url_in) // server_url = [schema@]hostname[:port]
{
	rt::StringA url(server_url_in);
	LPSTR server_url = url.GetBuffer();

	// parse server_url
	UINT port = 0;
	LPSTR p;
	if(p = strchr(server_url,':'))
	{	port = atoi(p+1);
		*((LPSTR)p) = '\0';
	}
	LPSTR sch_p = NULL;
	LPSTR sch = NULL;
	LPSTR hostname = server_url;
	if(sch_p = strchr(server_url,'@'))
	{	*(sch_p) = '\0';
		sch = server_url;
		hostname = sch_p+1;
	}

	BOOL ret = ConnectTo(user,password,hostname,sch,port);
	if(p)*p = ':';
	if(sch_p)*sch_p = '@';

	return ret;
}

UINT MySQL_Connection::GetLastWarningCount() const
{
	return mysql_warning_count(m_pConn);
}


BOOL MySQL_Connection::DuplicateConnection(const MySQL_Connection& x)
{
	Destroy();
	return ConnectTo(x._Backup_User,x._Backup_Password,x._Backup_Server,x._Backup_Databse.Begin(),x._Backup_Port);
}

BOOL MySQL_Connection::ConnectTo(LPCSTR user, LPCSTR password, LPCSTR server_addr, LPCSTR databse, UINT port)
{
	ASSERT(!IsConnected());
	
	if(	(m_pConn = mysql_init(m_pConn)) &&
		(mysql_real_connect(m_pConn,server_addr,user,password,databse,port,NULL,CLIENT_MULTI_RESULTS|CLIENT_MULTI_STATEMENTS|CLIENT_FOUND_ROWS) == m_pConn)
	)
	{	m_bConnected = TRUE;
		my_bool reconnect = true;
		VERIFY(0==mysql_options(m_pConn, MYSQL_OPT_RECONNECT, &reconnect));
		VERIFY(0==mysql_set_character_set(m_pConn, "utf8"));
		mysql_autocommit(m_pConn,true);

		_Backup_User = user;
		_Backup_Password = password;
		_Backup_Server = server_addr;
		_Backup_Databse = databse;
		_Backup_Port = port;

	}
	else
	{	m_bConnected = FALSE;
	}

	return IsConnected();
}

void MySQL_Connection::Transcation_Begin()
{
	m_TranscationCSS.Lock();

	ASSERT(m_bTranscation == FALSE);
	mysql_autocommit(m_pConn,FALSE);
	m_bTranscation = TRUE;
}

void MySQL_Connection::Transcation_Commit()
{
	ASSERT(m_bTranscation == TRUE);
	mysql_commit(m_pConn);
	mysql_autocommit(m_pConn,TRUE);
	m_bTranscation = FALSE;

	m_TranscationCSS.Unlock();
}

void MySQL_Connection::Transcation_Rollback()
{
	ASSERT(m_bTranscation == TRUE);
	mysql_rollback(m_pConn);
	mysql_autocommit(m_pConn,TRUE);
	m_bTranscation = FALSE;

	m_TranscationCSS.Unlock();
}

LPCSTR MySQL_Connection::GetLastErrorMessage() const
{
	return mysql_error(m_pConn);
}

UINT MySQL_Connection::GetLastError() const
{
	return mysql_errno(m_pConn);
}


void MySQL_Connection::Destroy()
{
	if(m_pConn)
	{
		if(m_bTranscation)
			Transcation_Rollback();
		mysql_close(m_pConn);
		m_pConn = NULL;
	}
}

BOOL MySQL_Connection::Execute(LPCSTR pSQL, ULONGLONG* pMatchedCount)
{
	ASSERT(m_pConn);
	if(mysql_query(m_pConn,pSQL) == 0)
	{	if(pMatchedCount)*pMatchedCount = mysql_affected_rows(m_pConn);
		return TRUE;
	}
	else
	{	if(pMatchedCount)*pMatchedCount = 0;
		return FALSE;
	}
}

MySQL_Connection::_QRet MySQL_Connection::Query(LPCSTR pSQL, BOOL allow_seek)
{
	if(Execute(pSQL))
	{	
		return _QRet(allow_seek?mysql_store_result(m_pConn):mysql_use_result(m_pConn),mysql_field_count(m_pConn));
	}

	return _QRet();
}



MySQL_UpdateValues::MySQL_UpdateValues(MySQL_Connection& sql_conn, LPCSTR table, LPCSTR fields, BOOL no_overwirte)
	:_Conn(sql_conn)
{
	_Statement.SetSize(128*1024);
	int len;
	if(no_overwirte)
		len = sprintf(_Statement,"INSERT INTO %s (%s) VALUES ",table,fields);
	else
		len = sprintf(_Statement,"REPLACE INTO %s (%s) VALUES ",table,fields);

	_pValues = _pValuesBase = &_Statement[len];
	_Claimed = FALSE;
	_HasError = FALSE;
}

LPSTR MySQL_UpdateValues::ClaimRow(UINT length)
{
	ASSERT(!_Claimed);
	if((int)(length+3) >= _Statement.End() - _pValues)
		if(!Flush())return NULL;
	_Claimed = TRUE;
	_pValues[0] = '(';
	_pValues++;
	return _pValues;
}

void MySQL_UpdateValues::SubmitRow(UINT finalized_length)
{
	ASSERT(_Claimed);
	_pValues += finalized_length;
	_pValues[0] = ')';
	_pValues[1] = ',';
	_pValues+=2;
	_Claimed = FALSE;
}

BOOL MySQL_UpdateValues::Flush()
{	
	ASSERT(!_Claimed);
	if(_pValues > _pValuesBase)
	{
		_pValues[0] = '\0';
		_pValues[-1] = ';';
		if(!_Conn.Execute(_Statement))
		{	_HasError = TRUE;
			return FALSE;
		}
		_pValues = _pValuesBase;
	}
	return TRUE;
}

MySQL_RecordSet::MySQL_RecordSet(const MySQL_Connection::_QRet& x)
{
	m_pResultSet = NULL;
	*this = x;
}

void MySQL_RecordSet::operator = (const MySQL_Connection::_QRet& x)
{
	Clear();

	m_pResultSet = x.m_pResultSet;
	m_FieldCount = x.m_FieldCount;

	if(m_pResultSet)
		Next();
	else
		m_CurrentRow = NULL;

	rt::_CastToNonconst(&x)->m_pResultSet = NULL;
}

BOOL MySQL_RecordSet::Next()
{
	ASSERT(m_pResultSet);
	return (m_CurrentRow = mysql_fetch_row(m_pResultSet))!=NULL;
}

LPCSTR MySQL_RecordSet::GetFieldName(UINT i) const
{
	ASSERT(m_pResultSet);
	return mysql_fetch_fields(m_pResultSet)[i].name;
}

DWORD MySQL_RecordSet::GetFieldType(UINT i) const
{
	ASSERT(m_pResultSet);
	int t = mysql_fetch_fields(m_pResultSet)[i].type;
	if( (t>=MYSQL_TYPE_DECIMAL && t<=MYSQL_TYPE_LONG) || t==MYSQL_TYPE_LONGLONG || t==MYSQL_TYPE_INT24 || t==MYSQL_TYPE_YEAR || t==MYSQL_TYPE_NEWDECIMAL)
		return SQL_FIELD_TYPE_INTEGER;
	else
	if(t==MYSQL_TYPE_FLOAT || t==MYSQL_TYPE_DOUBLE)
		return SQL_FIELD_TYPE_FLOAT;
	else
	if(	(t>=MYSQL_TYPE_TIMESTAMP && t<=MYSQL_TYPE_DATETIME) || t==MYSQL_TYPE_NEWDATE )
		return SQL_FIELD_TYPE_TIMEDATE;
	else
	if( t>=MYSQL_TYPE_TINY_BLOB && t<=MYSQL_TYPE_BLOB )
		return SQL_FIELD_TYPE_BLOB;
	else
	if( t == MYSQL_TYPE_ENUM )
		return SQL_FIELD_TYPE_ENUM;
	else
	if( t == MYSQL_TYPE_SET )
		return SQL_FIELD_TYPE_SET;
	else
	if( t == MYSQL_TYPE_VARCHAR || t == MYSQL_TYPE_VAR_STRING || t == MYSQL_TYPE_STRING)
		return SQL_FIELD_TYPE_STRING;
	else
		return SQL_FIELD_TYPE_UNKNOWN;
}

BOOL MySQL_RecordSet::SeekTo(UINT offset)
{
	ASSERT(m_pResultSet);
	mysql_data_seek(m_pResultSet,offset);

	return Next();
}

LPCSTR MySQL_RecordSet::GetField(UINT field_id)
{
	ASSERT(m_CurrentRow);
	if(m_FieldCount>field_id)
		return m_CurrentRow[field_id];
	else
		return NULL;
}

UINT MySQL_RecordSet::GetSize_Seekable() const
{
	ASSERT(m_pResultSet);
	return (UINT)mysql_num_rows(m_pResultSet);
}


MySQL_BindValues::MySQL_BindValues(unsigned int size){
	__binds = NULL;
	Create(size);
}

MySQL_BindValues::~MySQL_BindValues(){
	Destroy();
}

void MySQL_BindValues::Create(unsigned int size){
	Destroy();	

	if(size>0) {
		__binds = new st_mysql_bind[size];
		memset(__binds ,0,sizeof(st_mysql_bind)*size);
	
		_lens.SetSize(size);
		_vals.SetSize(size);

		for(unsigned int i=0;i<size;i++){
			__binds[i].length = &_lens[i];
			__binds[i].buffer = NULL;
			__binds[i].buffer_length= 0;
		}

		_bindFlag.SetSize(size);
		_bindFlag.Set(0);
	}

}

void MySQL_BindValues::Destroy(){
	if(__binds){
		delete []__binds;
		__binds = NULL;
		_lens.SetSize(0);
		_vals.SetSize(0);
		_bindFlag.SetSize(0);
	}
}

#define DEFINE_BIND_SET_NUM_VALUE_IMPL( _TYPE, _BUFFER_TYPE, _IS_UNSIGNED)    \
void MySQL_BindValues::setValue(unsigned int idx, _TYPE val){ \
	assert(idx<size()); \
	_vals[idx].SetSize(sizeof(_TYPE)); \
	*(reinterpret_cast<_TYPE*>(_vals[idx].Begin())) = val; \
	__binds[idx].buffer_type=  _BUFFER_TYPE; \
	__binds[idx].buffer= _vals[idx].Begin(); \
	__binds[idx].is_null= 0; \
	__binds[idx].is_unsigned = _IS_UNSIGNED; \
	_bindFlag[idx] = 1; \
}
//} \
//bool MySQL_BindValues::getValue(unsigned int idx,_TYPE& val){ \
//	assert(idx<size()); \
//	/*if(_BUFFER_TYPE != __binds[idx].buffer_type) return false; */ \
//    val = *(reinterpret_cast<_TYPE*>(_vals[idx].Begin())); \
//	return true;\
//}

DEFINE_BIND_SET_NUM_VALUE_IMPL(float,MYSQL_TYPE_FLOAT,false) 
DEFINE_BIND_SET_NUM_VALUE_IMPL(double,MYSQL_TYPE_DOUBLE,false) 
DEFINE_BIND_SET_NUM_VALUE_IMPL(short,MYSQL_TYPE_SHORT,false) 
DEFINE_BIND_SET_NUM_VALUE_IMPL(unsigned short,MYSQL_TYPE_SHORT,true) 
DEFINE_BIND_SET_NUM_VALUE_IMPL(long,MYSQL_TYPE_LONG,false) 
DEFINE_BIND_SET_NUM_VALUE_IMPL(unsigned long,MYSQL_TYPE_LONG,true) 
DEFINE_BIND_SET_NUM_VALUE_IMPL(long long,MYSQL_TYPE_LONGLONG,false) 
DEFINE_BIND_SET_NUM_VALUE_IMPL(unsigned long long,MYSQL_TYPE_LONGLONG,true) 

//bool MySQL_BindValues::getValue(unsigned int idx,unsigned int & val){
//	unsigned long l;
//	if(!getValue(idx,l)) return false;
//	val = l;
//	return true;
//}
//
//bool MySQL_BindValues::getValue(unsigned int idx,int & val){
//	long l;
//	if(!getValue(idx,l)) return false;
//	val = l;
//	return true;
//}

void MySQL_BindValues::setValue(unsigned int idx,unsigned int val){
	setValue(idx,(unsigned long)val);
}

void MySQL_BindValues::setValue(unsigned int idx,int val){
	setValue(idx,(long)val);
}

void MySQL_BindValues::setValue(unsigned int idx, const std::string& val){
	assert(idx<size()); 
	_vals[idx].SetSize( __max(val.length()+1,DEFAULT_BUF_SIZE) );
	memcpy(_vals[idx].Begin(),val.c_str(),val.length()+1);
	__binds[idx].buffer_type=  MYSQL_TYPE_STRING; 
	__binds[idx].buffer= _vals[idx].Begin();
	__binds[idx].is_null= 0; 
	__binds[idx].buffer_length = _vals[idx].GetSize();
	_lens[idx] = val.length();
	__binds[idx].length= &_lens[idx]; 
	_bindFlag[idx] = 1;
}
void MySQL_BindValues::setValue(unsigned int idx, const rt::Buffer<BYTE>& val){
	assert(idx<size()); 
	_vals[idx].SetSize( __max(val.GetSize(),DEFAULT_BUF_SIZE));
	memcpy(_vals[idx].Begin(),val.Begin(),val.GetSize());
	__binds[idx].buffer_type=  MYSQL_TYPE_BLOB; 
	__binds[idx].buffer= _vals[idx].Begin();
	__binds[idx].is_null= 0;
	__binds[idx].buffer_length = _vals[idx].GetSize();
	_lens[idx] = val.GetSize();
	__binds[idx].length = &_lens[idx]; 
	_bindFlag[idx] = 1; 
}

//bool MySQL_BindValues::getValue(unsigned int idx, std::string& val){
//	
//	assert(idx<size()); 
//	//if(__binds[idx].buffer_type !=  MYSQL_TYPE_STRING) return false;
//	val = std::string( (const char*) _vals[idx].Begin(), __min(_lens[idx],_vals[idx].GetSize()));
//	
//	return true;
//}
//bool MySQL_BindValues::getValue(unsigned int idx, rt::Buffer<BYTE>& val){
//
//	assert(idx<size()); 
//	//if(__binds[idx].buffer_type !=  MYSQL_TYPE_BLOB) return false;
//	val.SetSize(__min(_lens[idx],_vals[idx].GetSize()));
//	memcpy(val.Begin(),_vals[idx].Begin(),val.GetSize());
//
//	return true;
//}
//
//void MySQL_BindValues::ReallocateEnoughBuffer(){
//	for(unsigned int i=0;i<size();i++){
//		//if( __binds[i].buffer_type == MYSQL_TYPE_BLOB || __binds[i].buffer_type == MYSQL_TYPE_STRING ){
//		if( __binds[i].length != NULL && *__binds[i].length != 0){
//			_vals[i].SetSize(*(__binds[i].length));
//			__binds[i].buffer= _vals[i].Begin();
//			__binds[i].buffer_length = _vals[i].GetSize();
//		}
//	}
//}

//bool MySQL_BindValues::bind2Stmt(st_mysql_stmt* stmt){
//
//	assert(stmt);
//	if(!stmt || __p == NULL) return false;
//	if(_bindFlag.Sum() != size()) return false;
//
//	st_mysql_bind* binds = static_cast<st_mysql_bind*> (__p);
//
//
//	if ( size()>0 && mysql_stmt_bind_param(stmt, binds))
//	{
//		return false;
//	}
//
//	return true;
//}
//
//bool MySQL_BindValues::bindRes2Stmt(st_mysql_stmt* stmt){
//
//	assert(stmt && size()> 0 && __p != NULL);
//	if(!stmt || __p == NULL || size()==0) return false;
//
//	st_mysql_bind* binds = static_cast<st_mysql_bind*> (__p);
//
//	if(mysql_stmt_bind_result(stmt, binds))
//	{
//		return false;
//	}
//
//
//
//	return true;
//}


int MySQL_ConnectionEx::_ExecutePrepare(LPCSTR pSQL, MySQL_BindValues & bindVals)
{
	ASSERT(m_pConn);

	if(!bindVals.isReady()) return -1;

	st_mysql_stmt * stmt = mysql_stmt_init(m_pConn);
	if(mysql_stmt_prepare(stmt,pSQL,strlen(pSQL))
		|| (bindVals.getMySqlBind()!=NULL && mysql_stmt_bind_param(stmt, bindVals.getMySqlBind()))
		||  mysql_stmt_execute(stmt) ) {		
		mysql_stmt_close(stmt);
		return -1;
	}

	int affectedRows = 	mysql_stmt_affected_rows(stmt);

	if(mysql_stmt_close(stmt)) {
		return -1;
	}

	return affectedRows;
}


bool MySQL_ConnectionEx::_QueryPrepare(LPCSTR pSQL,MySQL_BindValues & bindVals,rt::Buffer< rt::Buffer<rt::Buffer<BYTE> >  > & outVals){

	ASSERT(m_pConn);

	if(!bindVals.isReady()) return false;

	st_mysql_stmt * stmt = mysql_stmt_init(m_pConn);
	if(mysql_stmt_prepare(stmt,pSQL,strlen(pSQL))
		|| (bindVals.getMySqlBind()!=NULL && mysql_stmt_bind_param(stmt, bindVals.getMySqlBind()))
		||  mysql_stmt_execute(stmt) ) {
		mysql_stmt_close(stmt);
		return false;
	}
	
	my_bool mb = true;
	mysql_stmt_attr_set(stmt,STMT_ATTR_UPDATE_MAX_LENGTH,&mb);

	st_mysql_res* prepare_meta_result = mysql_stmt_result_metadata(stmt);	
	unsigned int nColumns= mysql_num_fields(prepare_meta_result);
	mysql_free_result(prepare_meta_result);

	mysql_stmt_store_result(stmt);	
	unsigned int nRows = mysql_stmt_num_rows(stmt);
	outVals.SetSize(nRows);
	for(unsigned int i=0;i<nRows;i++){ 
		outVals[i].SetSize(nColumns);
	}


	rt::Buffer<st_mysql_bind> binds;
	rt::Buffer<unsigned long> lengths;
	binds.SetSize(nColumns);
	lengths.SetSize(nColumns);
	memset(binds.Begin(),0,sizeof(st_mysql_bind)*nColumns);


	for(unsigned int i=0;i<nRows;i++){ 

		for(unsigned int j=0; j< nColumns; j++) {
			binds[j].buffer_type = MYSQL_TYPE_BLOB;
			binds[j].buffer_length = 0;
			binds[j].length = &lengths[j];
		}
		mysql_stmt_data_seek(stmt,i);
		mysql_stmt_bind_result(stmt, binds.Begin());
		mysql_stmt_fetch(stmt);

		for(unsigned int j=0; j< nColumns; j++) {			
			binds[j].buffer_type = MYSQL_TYPE_BLOB;
			outVals[i][j].SetSize( *binds[j].length ) ;
			binds[j].buffer = outVals[i][j].Begin();
			binds[j].buffer_length = *binds[j].length;
		}
		mysql_stmt_bind_result(stmt, binds.Begin());
		mysql_stmt_data_seek(stmt,i);
		mysql_stmt_fetch(stmt);
	}
	
	mysql_stmt_close(stmt);

	return true;
}

} // namespace w32



