#include "stdafx.h"
#include "wnd_app.h"
#include "debug_log.h"

#ifndef WM_KICKIDLE
#define WM_KICKIDLE         0x036A
#endif

namespace w32
{

namespace _meta_
{	

enum _tagWindowClassType
{
	WCT_NORMAL = 0,
	WCT_LAYERED,

	WCT_MAX
};

class _WndBase_Class
{	friend class CWndBase;
protected:
	static LRESULT CALLBACK _CommonWndProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam)
	{	
#pragma warning(disable:4312)
		CWndBase * P_WND = (CWndBase *)::GetWindowLongPtr(hWnd,0);
#pragma warning(default:4312)
		if(P_WND)
		{
			return P_WND->WndProc(message,wParam,lParam);
		}
		else
			return DefWindowProc(hWnd,message,wParam,lParam);
	}

	static INT_PTR CALLBACK _CommonDlgProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam)
	{	
#pragma warning(disable:4312)
		CWndBase * P_WND = (CWndBase *)::GetWindowLongPtr(hWnd,DWLP_USER);
#pragma warning(default:4312)
		if(P_WND)
		{
			return P_WND->WndProc(message,wParam,lParam);
		}
		else
			return 0;
	}
	static void  _RegisterClass(UINT class_style, LPCTSTR prefix, _tagWindowClassType type)
	{
		ASSERT(type < WCT_MAX);
		HMODULE hInstance = ::GetModuleHandle(NULL);
		int len = _stprintf(Name[type],_T("%0*X@"),sizeof(LPVOID)*2,(size_t)hInstance);
		memcpy(&Name[type][len],prefix,min(_tcslen(prefix),16));

		WNDCLASSEX wcex;
		wcex.cbSize = sizeof(WNDCLASSEX);

		wcex.style			= class_style;
		wcex.hbrBackground	= NULL;

		wcex.lpfnWndProc	= (WNDPROC)_CommonWndProc;
		wcex.cbClsExtra		= 4; // Hold this of CWndBase
		wcex.cbWndExtra		= sizeof(LPVOID);  //Holds a pointer to CwglWnd Object
		wcex.hInstance		= hInstance;
		wcex.hIcon			= NULL;
		wcex.hCursor		= LoadCursor(NULL, IDC_ARROW);
		wcex.lpszMenuName	= NULL;
		wcex.lpszClassName	= Name[type];
		wcex.hIconSm		= NULL;
		VERIFY(RegisterClassEx(&wcex));
	}
	static TCHAR Name[WCT_MAX][16+1+sizeof(LPVOID)*2];
	
public:
	_WndBase_Class(){ ZeroMemory(Name,sizeof(Name)); }
	static LPCTSTR GetWindowClassName(_tagWindowClassType type = w32::_meta_::WCT_NORMAL)
	{	
		ASSERT(type < WCT_MAX);
		if(Name[type][0]){}
		else
		{	switch(type)
			{
				case WCT_NORMAL: _RegisterClass(0, _T("w32::CWndBase"),type); break;
				case WCT_LAYERED: _RegisterClass(CS_HREDRAW|CS_VREDRAW, _T("w32::CWndLayered"),type); break;
				default: ASSERT(0);
			}
		}
		return Name[type];
	}
	~_WndBase_Class()
	{	for(UINT i=0;i<WCT_MAX;i++)
			if(Name[0])::UnregisterClass(Name[0],::GetModuleHandle(NULL));
	}
};
_WndBase_Class _TheWndClass;
TCHAR _WndBase_Class::Name[WCT_MAX][16+1+sizeof(LPVOID)*2];

} // namespace _meta_

} // namespace w32

w32::CWndBase* w32::CWndBase::_pMainWnd = NULL;
HMODULE		   w32::CWndBase::_hModuleHandle = ::GetModuleHandle(NULL);
w32::CWndBase* w32::CWndBase::_pAutoDeleteWndList = NULL;
w32::CCriticalSection w32::CWndBase::_AutoDeleteWndList_CCS;

w32::CWndBase::CWndBase()
{
	hWnd = NULL;
	Hosted_Child = NULL;
	hDC = NULL;
	_IsInFallScreen = FALSE;
	_bAutoDeleteThis = FALSE;
	_Next_AutoDelWnd = NULL;
	_bMoveWindowByClientRegion = FALSE;
	_bAsDialog = FALSE;

#ifdef __AFXWIN_H__
	_pMfcWnd = new CWnd;
	ASSERT(_pMfcWnd);
#else
	_Reserved = NULL;
#endif
}

void w32::CWndBase::EnableWindowDragFromClientRegion(BOOL yes)
{
	_bMoveWindowByClientRegion = yes;
}

void w32::CWndBase::EnableDeleteThisOnDestroy()
{
	ASSERT(hWnd);

	_bAutoDeleteThis = TRUE;

	EnterCCSBlock(_AutoDeleteWndList_CCS);
	_Next_AutoDelWnd = _pAutoDeleteWndList;
	_pAutoDeleteWndList = this;
}

BOOL w32::CWndBase::IsChildWindow()
{   
    return GetStyle()&WS_CHILD;
}

BOOL w32::CWndBase::Attach(HWND hwnd)
{
	hWnd = hwnd;
	if(SetupAfterCreation())
		return TRUE;
	else
	{	hWnd = NULL;
		return FALSE;
	}
}

HWND w32::CWndBase::Detach()
{	ASSERT(hWnd);
	ASSERT(0);     // not implemeted !!!! 
	return NULL;
}


BOOL w32::CWndBase::SetupAfterCreation()
{
	if(hWnd)
	{	
		if(!_bAsDialog)
		{
		#ifndef _WIN64
			::SetWindowLongPtr(hWnd,0,(LONG)this);
		#else
			::SetWindowLongPtr(hWnd,0,(LONG_PTR)this);
		#endif
			WndProc(WM_CREATE,0,0);
		}
		else
		{
		#ifndef _WIN64
			::SetWindowLongPtr(hWnd,DWLP_USER,(LONG)this);
		#else
			::SetWindowLongPtr(hWnd,DWLP_USER,(LONG_PTR)this);
		#endif
			WndProc(WM_CREATE,0,0);
			WndProc(WM_INITDIALOG,0,0);
		}

		if(!OnInitWnd())
		{	_CheckErrorW32;
			DestroyWindow();
			return FALSE;
		}

	#ifdef __AFXWIN_H__
		ASSERT(_pMfcWnd);
		_pMfcWnd->Attach(hWnd);
	#endif
		return TRUE;
	}
	else
	{	_CheckErrorW32;
		return FALSE;
	}
}


BOOL w32::CWndBase::Create(DWORD WinStyle,HWND Parent)
{
	ASSERT(hWnd==NULL);
	hWnd = CreateWindow(_meta_::_WndBase_Class::GetWindowClassName(w32::_meta_::WCT_NORMAL)
						,_T(""), WinStyle,
						CW_USEDEFAULT, CW_USEDEFAULT, CW_USEDEFAULT, CW_USEDEFAULT, 
						Parent, NULL, ::GetModuleHandle(NULL), NULL);
	_bAsDialog = FALSE;
	if( SetupAfterCreation() )
		return TRUE;
	else
	{	hWnd = NULL;
		return FALSE;
	}
}

BOOL w32::CWndBase::Create_LayeredWindow(INT x, INT y, INT w, INT h, HWND Parent)
{
	ASSERT(hWnd==NULL);
	hWnd = CreateWindowEx(	WS_EX_LAYERED | WS_EX_TRANSPARENT | WS_EX_NOACTIVATE | WS_EX_LEFT,
							_meta_::_WndBase_Class::GetWindowClassName(w32::_meta_::WCT_LAYERED),
							_T(""), 0,
							x,y,w,h,
							Parent, NULL, ::GetModuleHandle(NULL), NULL);
						
	_bAsDialog = FALSE;
	if( SetupAfterCreation() )
		return TRUE;
	else
	{	hWnd = NULL;
		return FALSE;
	}
}


BOOL w32::CWndBase::Create_Dialog(LPCTSTR lpDlgTemplate, HWND Parent)
{
	ASSERT(hWnd==NULL);

	hWnd = CreateDialog(w32::GetResourceModule(),lpDlgTemplate,Parent,_meta_::_WndBase_Class::_CommonDlgProc);
	_bAsDialog = TRUE;
	if(SetupAfterCreation())
		return TRUE;
	else
	{	hWnd = NULL;
		return FALSE;
	}
}


BOOL w32::CWndBase::Create_ToolWnd(HWND Parent)
{	
	BOOL ret = Create(WS_CAPTION|WS_SYSMENU|WS_THICKFRAME,Parent);
	if(ret)
	{
		SetStyleEx(WS_EX_TOOLWINDOW | GetStyleEx());
		AlignTo(NULL,ALIGN_X);
	}
	return ret;
}

void w32::CWndBase::EnableResizing(BOOL is)
{ 
	if(is)
	{
		SetStyle(GetStyle()|WS_THICKFRAME);
		HMENU Menu= ::GetSystemMenu(*this,FALSE);
		if(Menu)
		{
			::InsertMenu(Menu,4,MF_BYPOSITION|MF_STRING|MF_ENABLED,SC_MAXIMIZE,_T("Ma&ximize"));
		}
	}
	else
	{
		SetStyle((GetStyle()&(~WS_THICKFRAME)));
		HMENU Menu= ::GetSystemMenu(*this,FALSE);
		if(Menu)
		{
			::ModifyMenu(Menu,SC_MAXIMIZE,MF_BYCOMMAND|MF_DISABLED,NULL,NULL);
		}
	}

	::RedrawWindow(*this,NULL,NULL,RDW_FRAME|RDW_INVALIDATE);
}

void w32::CWndBase::HostChildWindow(HWND hChild)
{
	if(Hosted_Child)
	{
		::ShowWindow(Hosted_Child,SW_HIDE);
		Hosted_Child = NULL;
	}

	Hosted_Child = hChild;

	if(Hosted_Child)
	{
		::SetParent(Hosted_Child,*this);
		RECT rc;
		::GetClientRect(*this,&rc);
		::SetWindowPos(Hosted_Child,NULL,rc.left,rc.top,(rc.right-rc.left),(rc.bottom-rc.top),SWP_NOZORDER);
		::ShowWindow(Hosted_Child,SW_SHOW);
	}
}

void w32::CWndBase::AlignTo(HWND Reference,DWORD AlignFlag)
{
	HWND MyParent = ::GetParent(*this);
	RECT rc,org_rc;
	::GetWindowRect( *this, &org_rc );

	if(Reference)
	{
		::GetWindowRect( Reference, &rc );
	}
	else
	{
		rc.left = 0;
		rc.top = 0;
		rc.right = ::GetSystemMetrics(SM_CXFULLSCREEN);
		rc.bottom = ::GetSystemMetrics(SM_CYFULLSCREEN);
	}

	ASSERT(rc.right >= rc.left);
	ASSERT(rc.bottom >= rc.top);
	ASSERT(org_rc.right >= org_rc.left);
	ASSERT(org_rc.bottom >= org_rc.top);

	POINT	p;
	int w,h;
	p.x = AlignFlag&ALIGN_X?rc.left:org_rc.left;
	p.y = AlignFlag&ALIGN_Y?rc.top:org_rc.top;
	w = AlignFlag&ALIGN_WIDTH?(rc.right - rc.left):(org_rc.right - org_rc.left);
	h = AlignFlag&ALIGN_HEIGHT?(rc.bottom - rc.top):(org_rc.bottom - org_rc.top);

	if( MyParent )::ScreenToClient(MyParent,&p);
	
	::SetWindowPos(*this,NULL,p.x,p.y,w,h,SWP_NOZORDER);
}

w32::CWndBase::~CWndBase(void)
{
	DestroyWindow();
	hWnd = NULL;
#ifdef __AFXWIN_H__
	ASSERT(_pMfcWnd);
	_pMfcWnd->Detach();
	_SafeDel(_pMfcWnd);
#endif
}

void w32::CWndBase::DestroyWindow(void)
{
	if(hWnd)
	{	ReleaseDC();
		if(::DestroyWindow(hWnd)){}
		else
		{
#pragma warning(disable: 4244 )
		::SetWindowLongPtr(hWnd,GWLP_WNDPROC,(LONG_PTR)DefWindowProc);  // just in case window is not completely destroyed, maybe in multithreading env. 
#pragma warning(default: 4244 )
		}
		hWnd = NULL;
	}
}


void	w32::CWndBase::ResizeWindowByClientArea(int Width,int Height)
{	RECT rc_win,rc_client;
	::GetWindowRect(*this,&rc_win);
	::GetClientRect(*this,&rc_client);

	::SetWindowPos(*this,NULL,0,0,
					Width +( rc_win.right - rc_win.left - rc_client.right),
					Height+( rc_win.bottom - rc_win.top - rc_client.bottom),
					SWP_NOZORDER|SWP_NOMOVE);
	::PostMessage(*this,WM_EXITSIZEMOVE,0,0);
}

void w32::CWndBase::SwitchFullScreen()
{
	static const DWORD style_to_remove = WS_CHILD|WS_CAPTION|WS_BORDER|WS_THICKFRAME|WS_MAXIMIZEBOX|WS_MINIMIZEBOX|WS_DLGFRAME|WS_OVERLAPPED|WS_SYSMENU;
	static const DWORD styleEx_to_remove = WS_EX_WINDOWEDGE|WS_EX_APPWINDOW|WS_EX_CLIENTEDGE|WS_EX_DLGMODALFRAME|WS_EX_MDICHILD|WS_EX_STATICEDGE|WS_EX_TOOLWINDOW;

	if( !IsInFullScreen() )
	{	// go full srceen
		_IsInFallScreen = TRUE;
		_OrgParentWnd = ::GetParent(*this);
		::GetWindowRect(*this,&_OrgLayout);
		::SetParent(*this,NULL);

		_OrgStyle = GetStyle();
		_OrgStyleEx = GetStyleEx();

		SetStyle( (_OrgStyle& (~style_to_remove)) | WS_POPUP );
		SetStyleEx( _OrgStyleEx& (~styleEx_to_remove) );

		if(_OrgParentWnd)
		{	POINT p = {0,0};
			::ScreenToClient(_OrgParentWnd,&p);
			_OrgLayout.left += p.x;
			_OrgLayout.right += p.x;
			_OrgLayout.bottom += p.y;
			_OrgLayout.top += p.y;
		}

		// just in case that we are using multiple monitor in DualView mode
		int scrw = GetSystemMetrics(SM_CXSCREEN);
		int scrh = GetSystemMetrics(SM_CYSCREEN);
		int x,y;
		{	POINT p = {_OrgLayout.left,_OrgLayout.top};
			::ClientToScreen(_OrgParentWnd,&p);
			
			x = ((int)(p.x/(float)scrw + 0.5f))*scrw;
			y = ((int)(p.y/(float)scrh + 0.5f))*scrh;
			if(p.x<0)x-=scrw;
			if(p.y<0)y-=scrh;

			int vw = GetSystemMetrics(SM_CXVIRTUALSCREEN);
			int vh = GetSystemMetrics(SM_CYVIRTUALSCREEN);
			int vx = GetSystemMetrics(SM_XVIRTUALSCREEN);
			int vy = GetSystemMetrics(SM_YVIRTUALSCREEN);

			x = min(max(x,vx),vw+vx-scrw);
			y = min(max(y,vy),vh+vy-scrh);
		}
		
		::SetWindowPos(*this,HWND_TOPMOST,x,y,scrw,scrh,SWP_NOZORDER);
		::PostMessage(*this,WM_EXITSIZEMOVE,0,0);
	}
	else
	{	_IsInFallScreen = FALSE;
		::SetParent(*this,_OrgParentWnd);

		SetStyle(_OrgStyle);
		SetStyleEx(_OrgStyleEx);

		::SetWindowPos(*this,HWND_NOTOPMOST,_OrgLayout.left,_OrgLayout.top,
											_OrgLayout.right-_OrgLayout.left,
											_OrgLayout.bottom-_OrgLayout.top,SWP_NOZORDER);
		::PostMessage(*this,WM_EXITSIZEMOVE,0,0);
		HWND ancestor = ::GetAncestor(*this,GA_ROOT);
		if(ancestor!=hWnd)::BringWindowToTop(ancestor);
	}
}

void w32::CWndBase::MainMenu_SetCheck(UINT Id,BOOL Check)
{
	HMENU m = GetMenu(*this);
	if(m)CheckMenuItem(m,Id,Check?MF_CHECKED:MF_UNCHECKED);
}

void w32::CWndBase::MainMenu_Enable(UINT Id,BOOL Enable)
{
	HMENU m = GetMenu(*this);
	if(m)EnableMenuItem(m,Id,Enable?MF_ENABLED:MF_GRAYED);
}

void w32::CWndBase::MainMenu_SetText(UINT Id,LPCTSTR text)
{	ASSERT(text);
	HMENU m = GetMenu(*this);
	if(m)ModifyMenu(m,Id,MF_BYCOMMAND|MF_STRING,Id,text);
}

int w32::CWndBase::MessageBox(LPCTSTR lpText,LPCTSTR lpCaption,UINT uType)
{
	return ::MessageBox((*this),lpText,lpCaption,uType);
}

LRESULT w32::CWndBase::WndProc(UINT message, WPARAM wParam, LPARAM lParam)
{
	switch (message) 
	{
	case WM_SIZE:
		if(Hosted_Child)
		{
			int width = max(1,LOWORD(lParam));
			int height = max(1,HIWORD(lParam));
			::SetWindowPos(Hosted_Child,NULL,0,0,width,height,SWP_NOZORDER|SWP_NOMOVE);
			return 0;
		}
	case WM_CLOSE:
		if(GetStyleEx()&WS_EX_TOOLWINDOW)
		{
			::ShowWindow(*this,SW_HIDE);
			return 0;
		}
		break;
	case WM_LBUTTONDOWN:
		SetFoucs();
		if(_bMoveWindowByClientRegion)
			::SendMessage(hWnd,WM_NCLBUTTONDOWN,HTCAPTION,lParam);
		break;
	case WM_RBUTTONDOWN:
	case WM_MBUTTONDOWN:
	case WM_ACTIVATE:
		SetFoucs();
		break;
	case WM_NCDESTROY:
		#ifdef __AFXWIN_H__
			ASSERT(_pMfcWnd);
			_pMfcWnd->Detach();
		#endif
		hWnd = NULL;
		if(this == _pMainWnd)
		{
			EnterCCSBlock(_AutoDeleteWndList_CCS);
			// Close all auto-delete windows
			CWndBase* p = _pAutoDeleteWndList;
			while(p)
			{
				CWndBase* next = p->_Next_AutoDelWnd;
				if(p!=this)
				{
					p->_bAutoDeleteThis = FALSE;
					p->DestroyWindow();
					delete p;
				}
				p = next;
			}

			_CheckHeap;
			PostQuitMessage(0);
		}

		if(_bAutoDeleteThis)
		{	
			EnterCCSBlock(_AutoDeleteWndList_CCS);
			CWndBase* p = _pAutoDeleteWndList;
			ASSERT(p);
			if(p==this)
			{ _pAutoDeleteWndList = _Next_AutoDelWnd; }
			else
			{	
				while(p->_Next_AutoDelWnd)
					if(p->_Next_AutoDelWnd == this)break;

				ASSERT(p->_Next_AutoDelWnd == this);
				p->_Next_AutoDelWnd = _Next_AutoDelWnd;
			}
		
			delete this;
		}

		return 0;
		break;
	}

	if(!_bAsDialog)
		return DefWindowProc(hWnd, message, wParam, lParam);
	else
		return 0;
}

void w32::CWndBase::CenterWindow()
{
	RECT rc;
	::GetWindowRect(hWnd,&rc);

	RECT rcw;
	::SystemParametersInfo(SPI_GETWORKAREA,0,&rcw,0);

	::SetWindowPos( *this,NULL,
					(rcw.left+((rcw.right - rcw.left) - (rc.right - rc.left))/2),
					(rcw.top+((rcw.bottom - rcw.top) - (rc.bottom - rc.top))/2),
					0,0,SWP_NOZORDER|SWP_NOSIZE|SWP_NOACTIVATE);
}

void w32::CWndBase::SetMenu(LPCTSTR ResId)
{	HMENU	hMenu = LoadMenu(_hModuleHandle,ResId);
	ASSERT(hMenu);
	SetMenu(hMenu);
}

void w32::CWndBase::SetIcon(LPCTSTR ResId, BOOL bBigIcon)
{	HICON	hIcon = LoadIcon(_hModuleHandle,ResId);
	ASSERT(hIcon);
	SetIcon(hIcon, bBigIcon);
}

void w32::SetResourceModule(HANDLE DLL)
{
	w32::CWndBase::_hModuleHandle = (HMODULE)DLL;
}

HINSTANCE w32::GetResourceModule()
{
	return (HINSTANCE)w32::CWndBase::_hModuleHandle;
}


/////////////////////////////////////////////////////////
// Layered Window
w32::CLayeredWnd::CLayeredWnd()
{
	_hMemoryDC = NULL;
	_hOrgBmpOnMemoryDC = NULL;
	m_Alpha = 200;
	m_RLExt_Position.x = m_RLExt_Position.y = 0;
	m_RLExt_Padding.cx = m_RLExt_Padding.cy = 0;
}

HBITMAP w32::CLayeredWnd::_CreateMemoryDC_BMP()
{
	BITMAPINFOHEADER stBmpInfoHeader = { 0 };   
	int nBytesPerLine = ((m_RLSize.cx * 32 + 31) & (~31)) >> 3;
	stBmpInfoHeader.biSize = sizeof(BITMAPINFOHEADER);   
	stBmpInfoHeader.biWidth = m_RLSize.cx;   
	stBmpInfoHeader.biHeight = m_RLSize.cy;   
	stBmpInfoHeader.biPlanes = 1;   
	stBmpInfoHeader.biBitCount = 32;   
	stBmpInfoHeader.biCompression = BI_RGB;   
	stBmpInfoHeader.biClrUsed = 0;   
	stBmpInfoHeader.biSizeImage = nBytesPerLine * m_RLSize.cy;

	PVOID pvBits = NULL;   
	HBITMAP hbmpMem = ::CreateDIBSection(NULL, (PBITMAPINFO)&stBmpInfoHeader, DIB_RGB_COLORS, &pvBits, NULL, 0);
	ASSERT(hbmpMem != NULL);
	ZeroMemory( pvBits, m_RLSize.cx * 4 * m_RLSize.cy);

	return hbmpMem;
}

void w32::CLayeredWnd::Refresh()
{
	if(_hMemoryDC)
	{	
		//{	HDC dc = ::GetWindowDC(*this);
		//	OnRender(dc);
		//	::ReleaseDC(*this,dc);
		//}

		OnRender(_hMemoryDC);

		static POINT Src = {0, 0};
		BLENDFUNCTION stBlend = { AC_SRC_OVER, 0, m_Alpha, AC_SRC_ALPHA };

		HDC layDC = _wRenderLayer.GetDC();
		::UpdateLayeredWindow(_wRenderLayer, layDC, &m_RLPosition, &m_RLSize, _hMemoryDC, &Src, 0, &stBlend, ULW_ALPHA);
		_wRenderLayer.ReleaseDC();
	}
}

BOOL w32::CLayeredWnd::Create(UINT Style, HWND Parent)
{
	if(_CreateBaseWnd(Style, Parent))
	{	_CreateRenderLayer(*this);
		return TRUE;
	}
	return FALSE;
}

BOOL w32::CLayeredWnd::_CreateRenderLayer(HWND parent)
{
	m_RLSize.cx = m_Size.cx + m_RLExt_Padding.cx;
	m_RLSize.cy = m_Size.cy + m_RLExt_Padding.cy;

	HDC hDC = ::GetDC(*this);
	_hMemoryDC = ::CreateCompatibleDC(hDC);
	::SetViewportOrgEx(_hMemoryDC,m_RLExt_Position.x,m_RLExt_Position.y,NULL);
	_hOrgBmpOnMemoryDC = ::SelectObject(_hMemoryDC,_CreateMemoryDC_BMP());
	::DeleteDC(hDC);

	BOOL ret = _wRenderLayer.Create_LayeredWindow(m_Position.x-m_RLExt_Position.x, m_Position.y-m_RLExt_Position.y,
												  m_RLSize.cx, m_RLSize.cy, parent);
	if(ret)
	{
		SetStyleEx( GetStyleEx() | WS_EX_LAYERED & (~WS_EX_WINDOWEDGE) );
		::SetLayeredWindowAttributes( *this, 0, 1, LWA_ALPHA);
		_LastWM_SIZEWPARAM = INFINITE;
		return TRUE;
	}
	else return FALSE;
}

void w32::CLayeredWnd::SetRenderLayerExtension(int left, int top, int right, int bottom)	// should call before Creation
{
	m_RLExt_Position.x = left;
	m_RLExt_Position.y = top;

	m_RLExt_Padding.cx = left + right;
	m_RLExt_Padding.cy = top + bottom;

	if(_wRenderLayer.GetSafeHwnd())
	{
		_UpdateRenderLayerSize();
		if(_hMemoryDC)
			::SetViewportOrgEx(_hMemoryDC,m_RLExt_Position.x,m_RLExt_Position.y,NULL);
		Refresh();
	}

}

void w32::CLayeredWnd::_UpdateRenderLayerSize()
{
	m_RLSize.cx = m_Size.cx + m_RLExt_Padding.cx;
	m_RLSize.cy = m_Size.cy + m_RLExt_Padding.cy;

	static SIZE BMP_SIZE = { 0, 0 };
	if(BMP_SIZE.cx < m_RLSize.cx || BMP_SIZE.cy < m_RLSize.cy)
	{
		::DeleteObject(::SelectObject(_hMemoryDC, _CreateMemoryDC_BMP()));
		BMP_SIZE = m_RLSize;
	}

	_wRenderLayer.SetWindowPos(NULL,m_Position.x-m_RLExt_Position.x, m_Position.y-m_RLExt_Position.y,m_RLSize.cx,m_RLSize.cy,SWP_NOZORDER);
}

LRESULT w32::CLayeredWnd::WndProc(UINT message, WPARAM wParam, LPARAM lParam)
{
	switch (message)
	{
	case WM_ERASEBKGND:
		return 0;
	case WM_SIZE:
		{	
			RECT rc;
			::GetWindowRect(*this,&rc);
			m_Size.cx = max(1,rc.right - rc.left);
			m_Size.cy = max(1,rc.bottom - rc.top);

			if(_wRenderLayer.GetSafeHwnd())
			{
				if(_LastWM_SIZEWPARAM != wParam)
				{
					if(SIZE_MINIMIZED == wParam)
						_wRenderLayer.SetStyle( _wRenderLayer.GetStyle() & (~WS_VISIBLE) );
					else
						_wRenderLayer.SetStyle( _wRenderLayer.GetStyle() | WS_VISIBLE );

					_LastWM_SIZEWPARAM = wParam;
				}

				_UpdateRenderLayerSize();	
			}
		}
		return 0;
	case WM_DESTROY:
		_wRenderLayer.DestroyWindow();
		::DeleteObject(::SelectObject(_hMemoryDC, _hOrgBmpOnMemoryDC));
		::DeleteDC(_hMemoryDC);
		_hOrgBmpOnMemoryDC = NULL;
		_hMemoryDC = NULL;
		break;
	case WM_MOVE:
	case WM_MOVING:
		{	
			RECT rc;
			::GetWindowRect(*this,&rc);
			m_Position.x = rc.left;
			m_Position.y = rc.top;
			m_RLPosition.x = m_Position.x-m_RLExt_Position.x;
			m_RLPosition.y = m_Position.y-m_RLExt_Position.y;
			if(_wRenderLayer.GetSafeHwnd())
			{	
				_wRenderLayer.SetWindowPos(	NULL,m_RLPosition.x,m_RLPosition.y,
											0,0,SWP_NOZORDER|SWP_NOSIZE|SWP_NOREDRAW);
			}
		}
		break;
	//case WM_SHOWWINDOW:
	//	::SendMessage(_wRenderLayer,WM_SHOWWINDOW,wParam,lParam);
		break;
	};

	return __super::WndProc(message, wParam, lParam);
}


BOOL w32::CLayeredWnd::_CreateBaseWnd(UINT Style, HWND Parent)
{
	if(__super::Create(Style, Parent))
	{
		RECT rc;
		::GetWindowRect(*this,&rc);
		m_Size.cx = max(1,rc.right - rc.left);
		m_Size.cy = max(1,rc.bottom - rc.top);
		m_Position.x = rc.left;
		m_Position.y = rc.top;
		return TRUE;
	}
	return FALSE;
}

BOOL w32::CLayeredWnd::ShowWindow(int nCmdShow)
{
	return __super::ShowWindow(nCmdShow);
}

void w32::CLayeredWnd::SetOpacity(int alpha)
{
	m_Alpha = min(255,max(0,alpha));
	Refresh();
}
/////////////////////////////////////////////////////////
// UI Event

#ifndef WM_MENUCOMMAND
	#define WM_MENUCOMMAND 0x0126
#endif

DWORD	w32::CUIEvent::g_UI_Thread = UINT_MAX;

void w32::CUIEvent::Fire(int id,UINT cookie,BOOL ThreadId)
{	
	if( g_UI_Thread != UINT_MAX )
	{	::PostThreadMessage( g_UI_Thread,WM_MENUCOMMAND,id,NULL);	}
	else
	{	ASSERT(ThreadId!=UINT_MAX); 
		::PostThreadMessage( ThreadId,WM_MENUCOMMAND,(WPARAM)id,(LPARAM)cookie);	
	}
}


UINT w32::CUIEvent::Awaiting(UINT id,UINT* pCookie,int TimeLimit_ms,BOOL IgnoreGlobalThreadId)
{
	BOOL WM_Quit_Received=FALSE;
	WPARAM QuitCode;
	int MsgId_Count=0;
	int MsgId_BufferSize=20;
	UINT* pMsgId_Seq=new UINT[20];

	if(IgnoreGlobalThreadId){ g_UI_Thread = UINT_MAX; }
	else
	{
		if( g_UI_Thread == UINT_MAX ){ g_UI_Thread = GetCurrentThreadId(); }
		else{ ASSERT( g_UI_Thread==GetCurrentThreadId() ); }
	}
		
	//Message bump here until the desired message or timeover
	{
		MSG msg;

		double deadline = TimeLimit_ms;
		w32::CTimeMeasure tm;
		tm.Start();
		for(;!WM_Quit_Received;)
		{
			BOOL HasMessage = ::PeekMessage(&msg, NULL, NULL, NULL,TRUE);
			if(TimeLimit_ms==INFINITE || tm.Snapshot() < deadline){}
			else
			{
				id = ANY_ID;
				goto EndOfWaiting; 
			}

			if(HasMessage)
			{	if(msg.message == WM_QUIT)
				{	WM_Quit_Received = TRUE;
					QuitCode = msg.wParam;
			}	}
			else
			{	Sleep(100);
				continue;
			}

			if(msg.message == WM_MENUCOMMAND && msg.hwnd == NULL)
			{
				if( id == (UINT)msg.wParam || ANY_ID == id)
				{
					id = (UINT)msg.wParam;
					if(pCookie)*pCookie=(UINT)msg.lParam;
					goto EndOfWaiting; 
				}
				else
				{
					if( MsgId_Count == MsgId_BufferSize )
					{//enlarge msg id buffer
						MsgId_BufferSize*=2;
						UINT* new_buffer = new UINT[MsgId_BufferSize];
						memcpy(new_buffer,pMsgId_Seq,sizeof(UINT)*MsgId_Count);
						rt::Swap(new_buffer,pMsgId_Seq);
						_SafeDelArray( new_buffer );
					}

					pMsgId_Seq[MsgId_Count] = (UINT)msg.wParam;
					MsgId_Count++;
				}
				continue;
			}

#ifdef WM_KICKIDLE
			if(msg.message != WM_KICKIDLE)
#endif
			{
				::TranslateMessage(&msg);
				::DispatchMessage(&msg);
			}
		}
	}

EndOfWaiting:
	int thread_id = GetCurrentThreadId();
	for(int i=0;i<MsgId_Count;i++)
	{
		::PostThreadMessage( thread_id,WM_MENUCOMMAND,pMsgId_Seq[i],NULL);
	}

	if( WM_Quit_Received )
		::PostThreadMessage( thread_id,WM_QUIT,QuitCode,NULL);

	_SafeDelArray( pMsgId_Seq );

	return id;
}

BOOL w32::CUIEvent::PumpMessageOnce()
{
	MSG msg;
	BOOL SomeMessageProcessed = FALSE;
	while(::PeekMessage(&msg,NULL,NULL,NULL,PM_REMOVE))
	{
#ifdef WM_KICKIDLE
		if(msg.message != WM_KICKIDLE)
#endif
		{
			SomeMessageProcessed = TRUE;
			::TranslateMessage(&msg);
			::DispatchMessage(&msg);
		}
	}

	return SomeMessageProcessed;
}

BOOL w32::WaitForEnteringCriticalSection_UI(w32::CCriticalSection& ccs, UINT timeout)
{
	double deadline = timeout==INFINITE?DBL_MAX:timeout;
	w32::CTimeMeasure tm;
	tm.Start();
	while(tm.Snapshot()<deadline)
	{
#ifdef __AFXWIN_H__
		_AFX_THREAD_STATE *pState = AfxGetThreadState();

		if(::PeekMessage(&(pState->m_msgCur), NULL, NULL, NULL,TRUE))
		{	if(pState->m_msgCur.message == WM_QUIT)
			{	::PostThreadMessage(GetCurrentThreadId(),WM_QUIT,0,0);
				return FALSE;
		}	}
		else
		{	if(ccs.TryLock())return TRUE;
			Sleep(100);
			continue;
		}

		if (pState->m_msgCur.message != WM_KICKIDLE && !AfxPreTranslateMessage(&(pState->m_msgCur)))
		{
			::TranslateMessage(&(pState->m_msgCur));
			::DispatchMessage(&(pState->m_msgCur));
		}
#else
		MSG	msgCur;
		if(::PeekMessage(&msgCur, NULL, NULL, NULL,TRUE))
		{	if(msgCur.message == WM_QUIT)
			{	::PostThreadMessage(GetCurrentThreadId(),WM_QUIT,0,0);
				return FALSE;
		}	}
		else
		{	if(ccs.TryLock())return TRUE;
			Sleep(100);
			continue;
		}

		::TranslateMessage(&msgCur);
		::DispatchMessage(&msgCur);
#endif
	}

	return FALSE;
}


BOOL w32::WaitForThreadEnding_UI(HANDLE hThread, UINT time_wait_ms)
{
	double deadline = (time_wait_ms==INFINITE)?DBL_MAX:time_wait_ms;
	w32::CTimeMeasure tm;
	tm.Start();
	while(tm.Snapshot()<deadline)
	{
#ifdef __AFXWIN_H__
		_AFX_THREAD_STATE *pState = AfxGetThreadState();

		if(::PeekMessage(&(pState->m_msgCur), NULL, NULL, NULL,TRUE))
		{	if(pState->m_msgCur.message == WM_QUIT)
			{	::PostThreadMessage(GetCurrentThreadId(),WM_QUIT,0,0);
				return FALSE;
		}	}
		else
		{	DWORD exitcode;
			if(!::GetExitCodeThread(hThread,&exitcode) || exitcode!=STILL_ACTIVE)
				return TRUE;
			Sleep(100);
			continue;
		}

		if (pState->m_msgCur.message != WM_KICKIDLE && !AfxPreTranslateMessage(&(pState->m_msgCur)))
		{
			::TranslateMessage(&(pState->m_msgCur));
			::DispatchMessage(&(pState->m_msgCur));
		}
#else
		MSG	msgCur;
		if(::PeekMessage(&msgCur, NULL, NULL, NULL,TRUE))
		{	if(msgCur.message == WM_QUIT)
			{	::PostThreadMessage(GetCurrentThreadId(),WM_QUIT,0,0);
				return FALSE;
		}	}
		else
		{	DWORD exitcode;
			if(::GetExitCodeThread(hThread,&exitcode) && exitcode!=STILL_ACTIVE)
				return TRUE;
			Sleep(100);
			continue;
		}

		::TranslateMessage(&msgCur);
		::DispatchMessage(&msgCur);
#endif
	}

	return FALSE;
}

#ifdef __AFXWIN_H__

UINT w32::CUIEvent::Awaiting_MFC(UINT id,UINT* pCookie,int TimeLimit_ms,BOOL IgnoreGlobalThreadId) //return waited event id
{
	BOOL WM_Quit_Received=FALSE;
	WPARAM QuitCode;
	int MsgId_Count=0;
	int MsgId_BufferSize=20;
	rt::BufferEx<UINT>	pMsgId_Seq;
	pMsgId_Seq.SetSize(20);

	if(IgnoreGlobalThreadId){ g_UI_Thread = UINT_MAX; }
	else
	{
		if( g_UI_Thread == UINT_MAX ){ g_UI_Thread = GetCurrentThreadId(); }
		else{ ASSERT( g_UI_Thread==GetCurrentThreadId() ); }
	}
	//Message bump here until the desired message or time over
	{	double deadline = TimeLimit_ms;
		w32::CTimeMeasure tm;
		tm.Start();
		for(;;)
		{	_AFX_THREAD_STATE *pState = AfxGetThreadState();
			if(::PeekMessage(&(pState->m_msgCur), NULL, NULL, NULL,TRUE))
			{	if(pState->m_msgCur.message == WM_QUIT)
				{	WM_Quit_Received = TRUE;
					QuitCode = pState->m_msgCur.wParam;
			}	}
			else
			{	Sleep(100);
				continue;
			}

			if(TimeLimit_ms==INFINITE || tm.Snapshot() < deadline){}
			else
			{	id = ANY_ID;
				goto EndOfWaiting; 
			}

			if(pState->m_msgCur.message == WM_MENUCOMMAND && pState->m_msgCur.lParam == NULL)
			{
				if( id == (UINT)pState->m_msgCur.wParam || ANY_ID == id)
				{
					id = (UINT)pState->m_msgCur.wParam;
					goto EndOfWaiting; 
				}
				else
				{	if( MsgId_Count == MsgId_BufferSize )
					{//enlarge msg id buffer
						MsgId_BufferSize*=2;
						pMsgId_Seq.ChangeSize(MsgId_BufferSize);
					}
					pMsgId_Seq[MsgId_Count] = (UINT)pState->m_msgCur.wParam;
					MsgId_Count++;
				}
				continue;
			}

			if (pState->m_msgCur.message != WM_KICKIDLE && !AfxPreTranslateMessage(&(pState->m_msgCur)))
			{
				::TranslateMessage(&(pState->m_msgCur));
				::DispatchMessage(&(pState->m_msgCur));
			}
		}
	}
EndOfWaiting:
	int thread_id = GetCurrentThreadId();
	for(int i=0;i<MsgId_Count;i++)
	{
		::PostThreadMessage( thread_id,WM_MENUCOMMAND,pMsgId_Seq[i],NULL);
	}
	if( WM_Quit_Received )
		::PostThreadMessage( thread_id,WM_QUIT,QuitCode,NULL);
	return id;
}


BOOL w32::CUIEvent::PumpMessageOnce_MFC()
{
	_AFX_THREAD_STATE *pState = AfxGetThreadState();

	BOOL SomeMessageProcessed = FALSE;
	while(::PeekMessage(&(pState->m_msgCur), NULL, NULL, NULL,PM_REMOVE))
	{
		if (pState->m_msgCur.message != WM_KICKIDLE && !AfxPreTranslateMessage(&(pState->m_msgCur)))
		{
			SomeMessageProcessed = TRUE;
			::TranslateMessage(&(pState->m_msgCur));
			::DispatchMessage(&(pState->m_msgCur));
		}
	}
	return SomeMessageProcessed;
}

#endif // __AFXWIN_H__

void w32::_meta_::_IdleKick_MFC::OnIdle()
{
#ifdef __AFXWIN_H__
	if(AfxGetApp())
		AfxGetApp()->OnIdle(IdleCount++);  // process idle for MFC threading model
#endif
}




