//{{NO_DEPENDENCIES}}
// Microsoft Visual C++ generated include file.
// Used by SceneConv.rc
//
#define IDD_SCENECONV_DIALOG            102
#define IDS_APP_TITLE                   103
#define IDM_ABOUT                       104
#define IDM_EXIT                        105
#define IDI_SCENECONV                   107
#define IDI_SMALL                       108
#define IDC_SCENECONV                   109
#define IDR_MAINFRAME                   128
#define IDR_UVWARP                      129
#define ID_FILE_LOAD                    32771
#define ID_FILE_LOADSCENEFILE           32772
#define ID_FILE_SAVESCENEFILE           32773
#define IDM_LOAD_TEXTURE                32774
#define ID_VIEW_SHOWCOLOR               32775
#define ID_VIEW_SHOWNORMAL              32776
#define ID_VIEW_SHOWUV                  32777
#define ID_VIEW_SHOWTEXTURE             32778
#define ID_VIEW_SHOWNONE                32779
#define ID_EDIT_SPLITSHAREDVERTEX       32780
#define ID_EDIT_MERGEEQUALVERTEX        32781
#define ID_EDIT_SUBDIVIDISION           32782
#define ID_EDIT_FLIPTRIANGLEFACET       32783
#define ID_EDIT_FIXTRIANGLEFACETORIENTATION 32784
#define ID_EDIT_RECALCULATEVERTEXNORMAL 32785
#define ID_EDIT_SMOOTHOUTLIER           32786
#define ID_REPAIR_GEG                   32787
#define ID_EDIT_MESHSMOOTH              32788
#define ID_FILE_EXPORT                  32789
#define ID_VIEW_UVMULTIPLY              32790
#define ID_EDIT_SAMPLEUVFROM            32791
#define ID_FILE_LOADSCENEINORIGINALSIZE 32792
#define ID_EDIT_ZPlan_UV                32793
#define ID_EDIT_ZPolar_UV               32794
#define ID_VIEW_SHOWTANGENT             32795
#define ID_EDIT_FAKETANGENT             32796
#define ID_EDIT_COMFORMNORMAL           32797
#define ID_EDIT_ORTHOGONALIZETANGENT    32798
#define ID_SIGNALEDITING_RECALCULATETANGENT 32799
#define ID_EDIT_COMFORMNORMALONLY       32800
#define ID_UVUNWARP                     32801
#define ID_TTT                          32802
#define ID_WHEEL_ZOOM                   32803
#define ID_WHEEL_XY                     32804
#define ID_WHEEL_X                      32805
#define ID_WHEEL_Y                      32806
#define ID_TOOLS_ARRANGE                32807
#define ID_TOOLS_FITRESOLUTION          32808
#define ID_TOOLS_FITSCALE               32809
#define IDC_STATIC                      -1

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NO_MFC                     1
#define _APS_NEXT_RESOURCE_VALUE        130
#define _APS_NEXT_COMMAND_VALUE         32810
#define _APS_NEXT_CONTROL_VALUE         1000
#define _APS_NEXT_SYMED_VALUE           110
#endif
#endif
