#include <iostream>
#include "XMLDoc.h"
#include <sstream>

using namespace std;
using namespace tixml;

void stringToArray(string &str, float *array, int count)
{
	stringstream ss(str);
	char temp;
	for (int i = 0; i < count; i++)
	{
		ss >> temp;
		ss >> array[i];
	}
}

string arrayToString(float *array, int count)
{
	stringstream ss;
	ss << '(' << array[0];
	for (int i = 1; i < count; i++)
	{
		ss << "," << array[i];
		if (i == count - 1)
			ss << ')';
	}
	return ss.str();
}

void testReadXML()
{
	tixml::XMLDoc doc;
	doc.Load("SceneConfig.xml");

	string centerStr = doc.get<string>("LightList.SRBF.@Center", 0);
	float center[3];
	stringToArray(centerStr, center, 2);
	cout << "center = (" << center[0] << "," << center[1] << ")" << endl;

	string rotationStr = doc.get<string>("ObjectList.Object.Transformation.@Rotation", 1);
	float rotation[16];
	stringToArray(rotationStr, rotation, 16);
	for (int i = 0; i < 4; i++)
	{
		for (int j = 0; j < 4; j++)
			cout << rotation[i * 4 + j] << " ";
		cout << endl;
	}

	cout << arrayToString(center, 2) << endl;
}

void testWriteXML()
{
	tixml::XMLDoc doc;
	doc.Create("SceneConfig");

	tixml::XMLNode * pNode1 = new tixml::XMLNode("GlobalSetting","");
	tixml::XMLNode * pNode1Child1 = new tixml::XMLNode("ViewTransformation","");
	tixml::XMLNode * pNode1Child2 = new tixml::XMLNode("LightTransformation","");
	tixml::XMLNode * pNode1Child3 = new tixml::XMLNode("CubeMap","");
	pNode1Child1->setAttrib<float>("Scale", 1.0);
	pNode1Child1->setAttrib<string>("Translation", "(xxx)");
	pNode1Child1->setAttrib<string>("Rotation", "(XXXX)");
	pNode1->children().push_back(pNode1Child1);
	pNode1->children().push_back(pNode1Child2);
	pNode1->children().push_back(pNode1Child3);

	tixml::XMLNode * pNode2 = new tixml::XMLNode("ObjectList","");


	tixml::XMLNode * pNode3 = new tixml::XMLNode("LightList","");

	doc.getRoot()->children().push_back(pNode1);
	doc.getRoot()->children().push_back(pNode2);
	doc.getRoot()->children().push_back(pNode3);

	doc.Save("savedSceneConfig.xml");
}
// 
// int main(int argc, char* argv[])
// {
// 	testReadXML();
// 	testWriteXML();
// 	system("pause");
// 	return 0;
// }
// 
