// stdafx.h : include file for standard system include files,
// or project specific include files that are used frequently, but
// are changed infrequently
//

#pragma once

//#define WGLH_ENABLE_GPU

#define EPSLON_RELAXATION 0.00001


#include "1.0\w32\w32_basic.h"
#include "1.0\rt\runtime_base.h"
#include "1.0\rt\type_traits.h"
#include "1.0\rt\compact_vector.h"
#include "1.0\rt\fixvec_type.h"
#include "1.0\w32\debug_log.h"
#include "1.0\w32\prog_idct.h"
#include "1.0\wglh\wglarcball.h"
#include "1.0\w32\file_64.h"
#include "1.0\wglh\wglMesh.h"
#include "1.0\wglh\wglImage.h"

#include "resource.h"
#include <atlbase.h>
#include <atlstr.h>

// C RunTime Header Files
#include <stdlib.h>
#include <malloc.h>
#include <memory.h>
#include <tchar.h>

#ifdef _DEBUG
#define FOR_DBG
#else
#define FOR_DBG //
#endif

#define USE_SPOT_LIGHT

// TODO: reference additional headers your program requires here
