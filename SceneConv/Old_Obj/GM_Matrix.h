#pragma once
#include "gm_vector.h"

namespace GraphMath
{
	class mat4x4f
	{
	public :
		float m[4][4];	// col first in memory

		// Constructors
		//
		mat4x4f() { LoadIdentity(); }
		mat4x4f( const float * p  ) { SetMat(p); }
		mat4x4f( const mat4x4f& mat )
		{
			::memcpy( (void *)m, (void*)mat.m, sizeof(float)*16 );
		}

		inline float & operator () (int x, int y) { return m[y][x]; }
		inline const float & operator () ( int x, int y ) const { return m[y][x]; }		

		inline mat4x4f&	operator = ( const mat4x4f& mat );

		// Transformation
		//
		inline void LoadIdentity();
		inline bool	IsIdentity(float fDelta = Delta);

		inline void SetMat( const float *p );	
		inline void SetMat( const double *p );

		inline void GetMat( float *p );			
		inline void GetMat( double *p );	

		const float * GetMat(void) const
		{
			return (float *)m;
		}

		float * GetMatPtr(void) 
		{
			return (float *)m;
		}

		inline bool	GetInvMat(mat4x4f& matInv) const;


		inline void transpose ();
		inline void rotterm();		

		inline void print() const;

		inline BOOL SaveToFile(FILE * pfile) const;
		inline BOOL LoadFromFile(FILE * pfile);


	
		// Operations defined on matrix and vector
		//
		friend inline mat4x4f operator + (const mat4x4f & m1, const mat4x4f & m2);
		friend inline mat4x4f operator - (const mat4x4f & m1, const mat4x4f & m2);
		friend inline mat4x4f operator * (const mat4x4f & m1, const mat4x4f & m2);		
		
		friend inline vec4f operator * (const vec4f & v, const mat4x4f & m);	
		friend inline vec3f operator * (const vec3f & v, const mat4x4f & m);		
	};

	typedef std::vector < mat4x4f >	Mat4x4fArray;

	// Copy
	//	
	mat4x4f& mat4x4f::operator = (const mat4x4f& mat)
	{
		if (&mat != this) {
			::memcpy(this->m, mat.m, sizeof(float) * 16);
		}
		return *this;
	}



	// Transformation
	//
	void mat4x4f::LoadIdentity()
	{
		//for ( int y=0; y<4; y++ ) 
		//	for ( int x=0; x<4; x++ ) 
		//	{
		//		if ( x==y ) 
		//			m[y][x] = 1;
		//		else
		//			m[y][x] = 0;
		//	}

		memset(m, 0, sizeof (float)*16);
		m[0][0] = m[1][1] = m[2][2] = m[3][3] = 1.0f;
	}
		
	//	check identity
	bool mat4x4f::IsIdentity(float fDelta)
	{
		for ( int y=0; y<4; y++ ) {
			for ( int x=0; x<4; x++ ) 
			{
				if ( x==y ) 
				{
					if ( fabs(m[y][x] - 1) > fDelta) return false;
				}
				else
				{
					if ( fabs(m[y][x]) > fDelta) return false;
				}
			}
		}
		return true;
	}


	//	compute the get inverse matrix
	bool	mat4x4f::GetInvMat(mat4x4f& matInv) const
	{
		//	cite the destination matrix
		matInv.LoadIdentity ();
		float* inverse = (float*)(matInv.m);
		
		///////////////////////////////////////
		//	copy from Yanyun's code
		int i, j, k, swap;
		double t;
		float temp[4][4];
		float *src;

		src = (float*)this->m;
		
		for (i = 0; i < 4; i++)
		{
			for (j = 0; j < 4; j++)
			{
				temp[i][j] = src[i * 4 + j];
			}
		}		
		
		for (i = 0; i < 4; i++)
		{
			/* 
			** Look for largest element in column */
			swap = i;
			for (j = i + 1; j < 4; j++)
			{
				if (fabs(temp[j][i]) > fabs(temp[i][i]))
				{
					swap = j;
				}
			}
			
			if (swap != i)
			{
				/* 
				** Swap rows. */
				for (k = 0; k < 4; k++)
				{
					t = temp[i][k];
					temp[i][k] = temp[swap][k];
					temp[swap][k] = (float)t;
					
					t = inverse[i * 4 + k];
					inverse[i * 4 + k] = inverse[swap * 4 + k];
					inverse[swap * 4 + k] = (float)t;
				}
			}
			if (temp[i][i] == 0)
			{
			/* 
			** No non-zero pivot.  The matrix is singular, which
			shouldn't ** happen.  This means the user gave us a
				bad matrix. */
				return false;
			}
			t = temp[i][i];
			for (k = 0; k < 4; k++)
			{
				temp[i][k] /= (float)t;
				inverse[i * 4 + k] /= (float)t;
			}
			for (j = 0; j < 4; j++)
			{
				if (j != i)
				{
					t = temp[j][i];
					for (k = 0; k < 4; k++)
					{
						temp[j][k] -= temp[i][k] * (float)t;
						inverse[j * 4 + k] -= inverse[i * 4 + k] * (float)t;
					}
				}
			}
		}

		return true;
	}

	//	Set the value of matrix
	void mat4x4f::SetMat(const float *p)
	{
		for ( int i=0; i<16; i++ )
			((float *)m)[i] = p[i];
	}

	//	Set the value of matrix
	void mat4x4f::SetMat(const double *p)
	{
		for ( int i=0; i<16; i++ )
			( (float *)m )[i] = (float)p[i];
	}

	//	Get the value of matrix
	void mat4x4f::GetMat(float *p)
	{
		for ( int i=0; i<16; i++ )
			p[i] = ( (float *)m )[i];
	}

	//	Get the value of matrix
	void mat4x4f::GetMat(double *p)
	{
		for ( int i=0; i<16; i++ )
			p[i] = (double) ( (float *)m )[i];
	}



	void mat4x4f::transpose ()
	{
		for (int x=0; x<3; x++)
			for (int y=x; y<4; y++)
				MathTool::DataSwap( m[y][x] ,m[x][y] );

	}

	void mat4x4f::rotterm()
	{
		for (int i=0; i<3; i++)
		{
			m[i][3] = 0.0f;
			m[3][i] = 0.0f;
		}
		m[3][3] = 1.0f;
	}

	void mat4x4f::print() const
	{
		for (int y=0; y<4; y++)
		{
			for (int x=0; x<4; x++)			
				printf("%9.4f ", m[y][x]);

			printf("\n");
		}
	}

	BOOL mat4x4f::SaveToFile(FILE * pfile) const
	{
		if ( NULL==pfile )
			return FALSE;

		for (int y=0; y<4; y++)
		{
			for (int x=0; x<4; x++)			
				fprintf(pfile, "%9.4f ", m[y][x]);

			printf("\n");
		}
		return TRUE;
	}

	BOOL mat4x4f::LoadFromFile(FILE * pfile)
	{ 
		if ( NULL==pfile )
			return FALSE;

		for (int y=0; y<4; y++)
		{
			for (int x=0; x<4; x++)			
				fscanf(pfile, "%f ", &m[y][x]);

			printf("\n");
		}
		return TRUE;
	}


	// Operations defined on matrix and vector
	//
	inline mat4x4f operator + (const mat4x4f & m1, const mat4x4f & m2)
	{
		mat4x4f r;
		for ( int y=0; y<4; y++ ) {
			for ( int x=0; x<4; x++ ) 
				r(x,y) = m1(x,y) + m2(x,y);
		}
		return r;
	}

	inline mat4x4f operator - (const mat4x4f & m1, const mat4x4f & m2)
	{
		mat4x4f r;
		for ( int y=0; y<4; y++ ) {
			for ( int x=0; x<4; x++ )
				r(x,y) = m1(x,y) - m2(x,y);
		}
		return r;
	}

	inline mat4x4f operator * (const mat4x4f & m1, const mat4x4f & m2)
	{
		mat4x4f r;
		for ( int y=0; y<4; y++ ) {
			for ( int x=0; x<4; x++ ) {

				float f[4],t[4];
				f[0] = m1(0,y);
				f[1] = m1(1,y);
				f[2] = m1(2,y);
				f[3] = m1(3,y);

				t[0] = m2(x,0);
				t[1] = m2(x,1);
				t[2] = m2(x,2);
				t[3] = m2(x,3);

                float dot=0;
				for ( int k=0; k<4; k++ )
					dot += m1(k, y) * m2(x , k);
				r(x,y) = dot;
			}
		}
		return r;
	}
	
	
	inline vec4f operator * (const vec4f & v, const mat4x4f & m)
	{
		vec4f r;
		r.x = v.x*m(0,0) + v.y*m(0,1) + v.z*m(0,2) + v.w * m(0,3);
		r.y = v.x*m(1,0) + v.y*m(1,1) + v.z*m(1,2) + v.w * m(1,3);
		r.z = v.x*m(2,0) + v.y*m(2,1) + v.z*m(2,2) + v.w * m(2,3);
		r.w = v.x*m(3,0) + v.y*m(3,1) + v.z*m(3,2) + v.w * m(3,3);

		return r;
	}			

	inline vec3f operator * (const vec3f & v, const mat4x4f & m)
	{
		vec3f r;
		r.x = v.x*m(0,0) + v.y*m(0,1) + v.z*m(0,2) ;
		r.y = v.x*m(1,0) + v.y*m(1,1) + v.z*m(1,2) ;
		r.z = v.x*m(2,0) + v.y*m(2,1) + v.z*m(2,2) ;		

		return r;
	}			
	
	
} // end of namespace




