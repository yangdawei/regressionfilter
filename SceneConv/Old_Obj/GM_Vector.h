#pragma once

#include <vector>
#include "math.h"
#include "mathtool.h"

namespace GraphMath
{
	/*/////////////////////////////////////////////////////////////////////
	Module Name:	vec3f

	Description:	
		3D vector definition of graphic math

	Create Date:	2003-10-13
/////////////////////////////////////////////////////////////////////*/
	class vec3f
	{
	public:
		float x, y, z, w;

	//-------------------------------------------------------------
	//	Alias access functions	
	//-------------------------------------------------------------
	public:
		float& r( void )	{ return x; }
		float& g( void )	{ return y; }
		float& b( void )	{ return z; }				

		const float& r( void ) const { return x; }
		const float& g( void ) const { return y; }
		const float& b( void ) const { return z; }
		

	//-------------------------------------------------------------
	//	Construction/Deconstruction	
	//-------------------------------------------------------------
	public:
		vec3f() : x(0),y(0),z(0), w(1) {}
		vec3f(float xx, float yy, float zz) : x(xx),y(yy),z(zz) {};	
		template < class T > vec3f(const T* p) { SetVec(p); }

		//------------------------------------------------------------
		//	Equal
		//------------------------------------------------------------
		vec3f(const vec3f & v) { x = v.x;	y = v.y;	z = v.z; }

        ~vec3f() {}

		void Reset(void) { x=y=z=0; w=1; }

	public:
		template<class T>
		inline void	SetVec(const T* p)
		{
			ASSERT(p);
			x = float(p[0]);
			y = float(p[1]);
			z = float(p[2]);
		}

		template < class T > void
		GetVec(T * p) 
		{
			ASSERT(p);
			p[0] = (T) x;
			p[1] = (T) y;
			p[2] = (T) z;
			p[3] = (T) w;
		}

	//-------------------------------------------------------------
	//	Basic math operations	
	//-------------------------------------------------------------
	public:
		//	Binary calculation
		inline vec3f operator + ( const float   k ) const;
		inline vec3f operator + ( const vec3f & v ) const;		

		inline vec3f operator - ( const float   k ) const;
		inline vec3f operator - ( const vec3f & v ) const;		
		
		inline vec3f operator * ( const float   k ) const;
		inline vec3f operator * ( const vec3f & v ) const; 

		inline vec3f operator / ( const float   k ) const;
		inline vec3f operator / ( const vec3f & v ) const;

		//	Inverse
		inline vec3f operator - () const;

		//	Self calculation
		inline void operator += ( const float   k );
		inline void operator += ( const vec3f & v )
		{
			x += v.x;
			y += v.y;
			z += v.z;		
		}

		inline void operator -= ( const float   k );
		inline void operator -= ( const vec3f & v ); 	

		inline void operator *= ( const float   k );
		inline void operator *= ( const vec3f & v ); 	

		inline void operator /= ( const float   k );
		inline void operator /= ( const vec3f & v ); 	

		inline float& operator[] ( const UINT & nD );
		inline const float& operator[] ( const UINT & nD ) const;
		

	//-------------------------------------------------------------
	//	unify function	
	//-------------------------------------------------------------
	public:
		inline float length( void ) const;
		inline bool  unify ( void );
		inline void  clamp ( float fL = 0.0f, float fM = 1.0f );

	//-------------------------------------------------------------
	//	binary computation functions declaration	
	//-------------------------------------------------------------
	public:
		friend bool operator == ( const vec3f & v1, const vec3f & v2 );
		friend bool operator != ( const vec3f & v1, const vec3f & v2 );

		friend float VecDot  ( const vec3f & v1, const vec3f & v2 ); // doc_product		
		friend vec3f VecCross( const vec3f & v1, const vec3f & v2 ); // cross_product	
	};


	///////////////////////////////////////////////////////////////
	//	Basic math operations	
	///////////////////////////////////////////////////////////////

	//------------------------------------------------------------
	//	Add 
	//------------------------------------------------------------
	vec3f vec3f::operator + ( const vec3f & v ) const
	{
		vec3f r;
		r.x = x + v.x;
		r.y = y + v.y;
		r.z = z + v.z;		
		return r;
	}		
	vec3f vec3f::operator + ( const float k ) const
	{
		vec3f r;
		r.x = x + k;
		r.y = y + k;
		r.z = z + k;		
		return r;
	}

	//------------------------------------------------------------
	//	Sub 
	//------------------------------------------------------------
	vec3f vec3f::operator - ( const vec3f & v ) const
	{
		vec3f r;
		r.x = x - v.x;
		r.y = y - v.y;
		r.z = z - v.z;		
		return r;
	}
	vec3f vec3f::operator - ( const float k ) const
	{
		vec3f r;
		r.x = x - k;
		r.y = y - k;
		r.z = z - k;		
		return r;
	}


	//------------------------------------------------------------
	//	Inverse
	//------------------------------------------------------------
	vec3f vec3f::operator - ( void ) const
	{
		vec3f r;
		r.x = -x;
		r.y = -y;
		r.z = -z;

		return r;
	}

	//------------------------------------------------------------
	//	Mul
	//------------------------------------------------------------
	vec3f vec3f::operator * ( const vec3f & v ) const
	{
		vec3f r;
		r.x = x * v.x;
		r.y = y * v.y;
		r.z = z * v.z;		
		return r;
	}	


	vec3f vec3f::operator * ( const float k ) const
	{
		vec3f r;
		r.x = x * k;
		r.y = y * k;
		r.z = z * k;		
		return r;
	}

	//------------------------------------------------------------
	//	Div
	//------------------------------------------------------------
	vec3f vec3f::operator / ( const vec3f & v ) const
	{
		vec3f r;
		r.x = x / v.x;
		r.y = y / v.y;
		r.z = z / v.z;		
		return r;
	}	
	vec3f vec3f::operator / ( const float k ) const
	{
		vec3f r;
		r.x = x / k;
		r.y = y / k;
		r.z = z / k;		
		return r;
	}

	//------------------------------------------------------------
	//	Add to Self 
	//------------------------------------------------------------

	void vec3f::operator += ( const float k ) 
	{		
		x += k;
		y += k;
		z += k;				
	}

	//------------------------------------------------------------
	//	Sub to Self 
	//------------------------------------------------------------
	void vec3f::operator -= ( const vec3f & v ) 
	{		
		x -= v.x;
		y -= v.y;
		z -= v.z;				
	}
	void vec3f::operator -= ( const float k ) 
	{		
		x -= k;
		y -= k;
		z -= k;				
	}


	//------------------------------------------------------------
	//	Mul to Self 
	//------------------------------------------------------------
	void vec3f::operator *= ( const vec3f & v ) 
	{		
		x *= v.x;
		y *= v.y;
		z *= v.z;			
	}	


	void vec3f::operator *= ( const float k ) 
	{		
		x *= k;
		y *= k;
		z *= k;				
	}

	//------------------------------------------------------------
	//	Div to Self 
	//------------------------------------------------------------
	void vec3f::operator /= ( const vec3f & v ) 
	{		
		x /= v.x;
		y /= v.y;
		z /= v.z;			
	}	
	void vec3f::operator /= ( const float k ) 
	{		
		x /= k;
		y /= k;
		z /= k;			
	}


	//------------------------------------------------------------
	//	Cite Array
	//------------------------------------------------------------
	float&	vec3f::operator[] ( const UINT & nD )
	{
		float* pData = (float*)this;
		if (nD < 4)
			return pData[nD];
		else
			return pData[0];			
	}

	const float& vec3f::operator[] ( const UINT & nD ) const
	{
		float* pData = (float*)this;
		if (nD < 4)
			return pData[nD];
		else
			return pData[0];			
	}
	

	///////////////////////////////////////////////////////////////
	//	Unify functions
	///////////////////////////////////////////////////////////////

	//	Get length
	float vec3f::length( void ) const 
	{
		return (float)sqrt( x*x + y*y + z*z );
	}

	//	unify 
	bool vec3f::unify( void )
	{
		float len = length();

		if (len == 0.0f)
			return false;

		x /= len;
		y /= len;
		z /= len;

		return true;
	}

	//	clamp
	void vec3f::clamp( float fL, float fM )
	{
		x = MathTool::DataClip ( x, fL, fM );
		y = MathTool::DataClip ( y, fL, fM );
		z = MathTool::DataClip ( z, fL, fM );	
	}	



	///////////////////////////////////////////////////////////////
	//	Binary operation functions
	///////////////////////////////////////////////////////////////
	
	//	Equal
	inline bool operator == ( const vec3f & v1, const vec3f & v2 )
	{
		return ( v1.x==v2.x && v1.y==v2.y && v1.z==v2.z );
	}


	inline bool operator != ( const vec3f & v1, const vec3f & v2 )
	{
		return ( v1.x!=v2.x || v1.y!=v2.y || v1.z!=v2.z );
	}

	inline vec3f VecCross(const vec3f & v1, const vec3f & v2)
	{
		vec3f r;		
		r.x = v1.y * v2.z - v2.y * v1.z;
		r.y = v2.x * v1.z - v1.x * v2.z;
		r.z = v1.x * v2.y - v2.x * v1.y;
		return r;
	}	

	inline float VecDot(const vec3f & v1, const vec3f & v2)
	{
		return ( v1.x * v2.x + v1.y * v2.y + v1.z * v2.z );
	}





/*/////////////////////////////////////////////////////////////////////
	Module Name:	vec4f

	Description:	
		4D vector definition of graphic math
		Inherit from vec3f data

	Create Date:	2003-09-19
/////////////////////////////////////////////////////////////////////*/

	class	vec4f: public vec3f
	{			

	//-------------------------------------------------------------
	//	Construction/Deconstruction	
	//-------------------------------------------------------------
	public:
		vec4f() { w = 1.0f; }
		vec4f( float xx, float yy, float zz, float ww ): vec3f( xx, yy, zz )
				{ w = ww; }

		vec4f( const vec4f & v ) 	{ x = v.x;	y = v.y;	z = v.z;	w = v.w;  }	
		vec4f( const vec3f & v ) 	{ x = v.x;	y = v.y;	z = v.z;	w = 1.0f; }	

		vec4f( const float & f )	{ x = f; y = f; z = f; w = f; }
		

		~vec4f() {}

	public:
		float& a(void)	{ return w; }
		const float& a(void) const { return w; }

	public:
		template<class T>
			inline void	SetVec( const T* p )
		{
			vec3f::SetVec<T>(p);			
			w = float(p[3]);
		}

	//-------------------------------------------------------------
	//	Basic math operations	
	//-------------------------------------------------------------
	public:
		//	Binary calculation
		inline vec4f operator + ( const float   k ) const;
		inline vec4f operator + ( const vec4f & v ) const;		

		inline vec4f operator - ( const float   k ) const;
		inline vec4f operator - ( const vec4f & v ) const;		
		
		inline vec4f operator * ( const float   k ) const;
		inline vec4f operator * ( const vec4f & v ) const; 

		inline vec4f operator / ( const float   k ) const;
		inline vec4f operator / ( const vec4f & v ) const;

		//	Inverse
		inline vec4f operator - ( void ) const;

		//	Self calculation
		inline void operator += ( const float   k );
		inline void operator += ( const vec4f & v ); 	

		inline void operator -= ( const float   k );
		inline void operator -= ( const vec4f & v ); 	

		inline void operator *= ( const float   k );
		inline void operator *= ( const vec4f & v ); 	

		inline void operator /= ( const float   k );
		inline void operator /= ( const vec4f & v ); 	
		
	//-------------------------------------------------------------
	//	unify function	
	//-------------------------------------------------------------
	public:		
		inline void  clamp( float fL = 0.0f, float fM = 1.0f );

	//-------------------------------------------------------------
	//	binary computation functions declaration	
	//-------------------------------------------------------------
	public:
		friend bool operator == ( const vec4f & v1, const vec4f & v2 );
		friend bool operator != ( const vec4f & v1, const vec4f & v2 );

		friend float VecDot3  ( const vec4f & v1, const vec4f & v2 );  // doc_product
		friend float VecDot4  ( const vec4f & v1, const vec4f & v2 );  // doc_product
		friend vec4f VecCross3( const vec4f & v1, const vec4f & v2 ); // cross_product	
		
	};

	typedef std::vector < vec4f >	Vec4fArray;
	typedef std::vector < vec3f >	Vec3fArray;
	
	

	///////////////////////////////////////////////////////////////
	//	Basic math operations	
	///////////////////////////////////////////////////////////////

	//------------------------------------------------------------
	//	Add 
	//------------------------------------------------------------
	vec4f vec4f::operator + ( const vec4f & v ) const
	{
		vec4f r;
		r.x = x + v.x;
		r.y = y + v.y;
		r.z = z + v.z;
		r.w = w + v.w;

		return r;
	}		
	vec4f vec4f::operator + ( const float k ) const
	{
		vec4f r;
		r.x = x + k;
		r.y = y + k;
		r.z = z + k;
		r.w = w + k;
		return r;
	}

	//------------------------------------------------------------
	//	Sub 
	//------------------------------------------------------------
	vec4f vec4f::operator - ( const vec4f & v ) const
	{
		vec4f r;
		r.x = x - v.x;
		r.y = y - v.y;
		r.z = z - v.z;		
		r.w = w - v.w;
		return r;
	}
	vec4f vec4f::operator - ( const float k ) const
	{
		vec4f r;
		r.x = x - k;
		r.y = y - k;
		r.z = z - k;	
		r.w = w - k;
		return r;
	}


	//------------------------------------------------------------
	//	Inverse
	//------------------------------------------------------------
	vec4f vec4f::operator - ( void ) const
	{
		vec4f r;
		r.x = -x;
		r.y = -y;
		r.z = -z;
		r.w = -w;

		return r;
	}

	//------------------------------------------------------------
	//	Mul
	//------------------------------------------------------------
	vec4f vec4f::operator * ( const vec4f & v ) const
	{
		vec4f r;
		r.x = x * v.x;
		r.y = y * v.y;
		r.z = z * v.z;
		r.w = w * v.w;
		return r;
	}	


	vec4f vec4f::operator * ( const float k ) const
	{
		vec4f r;
		r.x = x * k;
		r.y = y * k;
		r.z = z * k;		
		r.w = w * k;		
		return r;
	}

	//------------------------------------------------------------
	//	Div
	//------------------------------------------------------------
	vec4f vec4f::operator / ( const vec4f & v ) const
	{
		vec4f r;
		r.x = x / v.x;
		r.y = y / v.y;
		r.z = z / v.z;		
		r.w = w / v.w;		
		return r;
	}	
	vec4f vec4f::operator / ( const float k ) const
	{
		vec4f r;
		r.x = x / k;
		r.y = y / k;
		r.z = z / k;
		r.w = w / k;
		return r;
	}

	//------------------------------------------------------------
	//	Add to Self 
	//------------------------------------------------------------
	void vec4f::operator += ( const vec4f & v ) 
	{		
		x += v.x;
		y += v.y;
		z += v.z;	
		w += v.w;	
		
		
	}		
	void vec4f::operator += ( const float k ) 
	{		
		x += k;
		y += k;
		z += k;				
		w += k;				
	}

	//------------------------------------------------------------
	//	Sub to Self 
	//------------------------------------------------------------
	void vec4f::operator -= ( const vec4f & v ) 
	{		
		x -= v.x;
		y -= v.y;
		z -= v.z;
		w -= v.w;
	}
	void vec4f::operator -= ( const float k ) 
	{		
		x -= k;
		y -= k;
		z -= k;				
		w -= k;				
	}


	//------------------------------------------------------------
	//	Mul to Self 
	//------------------------------------------------------------
	void vec4f::operator *= ( const vec4f & v ) 
	{		
		x *= v.x;
		y *= v.y;
		z *= v.z;			
		w *= v.w;			
	}	


	void vec4f::operator *= ( const float k ) 
	{		
		x *= k;
		y *= k;
		z *= k;				
		w *= k;				
	}

	//------------------------------------------------------------
	//	Div to Self 
	//------------------------------------------------------------
	void vec4f::operator /= ( const vec4f & v ) 
	{		
		x /= v.x;
		y /= v.y;
		z /= v.z;			
		w /= v.w;			
	}	
	void vec4f::operator /= ( const float k ) 
	{		
		x /= k;
		y /= k;
		z /= k;
		w /= k;
	}	

	//	clamp
	void vec4f::clamp( float fL, float fM )
	{
		vec3f::clamp( fL, fM );
		w = MathTool::DataClip ( w, fL, fM );		
	}	



	///////////////////////////////////////////////////////////////
	//	Binary operation functions
	///////////////////////////////////////////////////////////////
	
	//	Equal
	inline bool operator == ( const vec4f & v1, const vec4f & v2 )
	{
		return ( v1.x==v2.x && v1.y==v2.y && v1.z==v2.z && v1.w == v2.w );
	}


	inline bool operator != ( const vec4f & v1, const vec4f & v2 )
	{
		return ( v1.x!=v2.x || v1.y!=v2.y || v1.z!=v2.z || v1.z !=v2.w );
	}

	inline vec4f VecCross3( const vec4f & v1, const vec4f & v2 )
	{
		vec4f r;		
		r = VecCross( (vec3f) v1, (vec3f) v2 );		
		return r;
	}	

	inline float VecDot3( const vec4f & v1, const vec4f & v2 )
	{
		return VecDot( (vec3f) v1, (vec3f) v2 );
	}


	inline float VecDot4( const vec4f & v1, const vec4f & v2)
	{
		return ( VecDot( (vec3f) v1, (vec3f) v2) + v1.w*v2.w );
	}

}// end of namespace



