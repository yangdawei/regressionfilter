#pragma once

#include "GM_vector.h"

namespace GraphMath
{
/*/////////////////////////////////////////////////////////////////////
	Module Name:	spheref

	Description:	
		sphere coordinate in the polar angle representation os sphere

	Create Date:	2003-10-13
/////////////////////////////////////////////////////////////////////*/

	class spheref
	{
	public:
		float p;	//Polar angle
		float a;	//Azimuthal angle	

	//-------------------------------------------------------------
	//	Construction/Deconstruction	
	//-------------------------------------------------------------
	public:
		spheref( void ): p(0),a(0) {}
		spheref( const float pp,const float aa ) : p(pp),a(aa)	{}	
		spheref( const spheref& sph ) : p(sph.p), a(sph.a) {}			

		~spheref( void ) {}

	//-------------------------------------------------------------
	//	Basic math operations	
	//-------------------------------------------------------------
	public:		
		inline spheref operator + ( const spheref & sph );
		inline spheref operator - ( const spheref & sph );

		inline spheref operator * ( const float k );

	//-------------------------------------------------------------
	//	binary computation functions declaration	
	//-------------------------------------------------------------
	public:
		friend bool operator == ( const spheref & sph1, const spheref & sph2 );
		friend bool operator != ( const spheref & sph1, const spheref & sph2 );

		friend vec3f		Sph2Vec( const spheref & sph );
		friend spheref		Vec2Sph( const vec3f & vec );

	};

	///////////////////////////////////////////////////////////////
	//	Basic math operations	
	///////////////////////////////////////////////////////////////

	//------------------------------------------------------------
	//	Add 
	//------------------------------------------------------------
	spheref spheref::operator + ( const spheref& sph )
	{
		return spheref( p + sph.p, a + sph.a );
	}

	//------------------------------------------------------------
	//	Sub 
	//------------------------------------------------------------
	spheref spheref::operator - ( const spheref& sph )
	{
		return spheref( p - sph.p, a - sph.a);
	}

	//------------------------------------------------------------
	//	Mul 
	//------------------------------------------------------------
	spheref spheref::operator * ( const float k )
	{
		return spheref( p * k, a * k );
	}

	///////////////////////////////////////////////////////////////
	//	Binary operation functions
	///////////////////////////////////////////////////////////////
	
	//	Equal
	inline bool operator == ( const spheref & sph1, const spheref & sph2 )
	{
		return ( sph1.p == sph2.p ) && ( sph1.a == sph2.a );
	}

	//	Unequal
	inline bool operator != ( const spheref & sph1, const spheref & sph2 )
	{
		return ( sph1.p != sph2.p ) || ( sph1.a != sph2.a );
	}

	// vector to sphere
	inline  vec3f		Sph2Vec( const spheref& sph )
	{
		vec3f	vec;

		vec.z = (float)cos (sph.p);

		float sinp = (float)sin( sph.p );
		vec.x = sinp * (float)cos( sph.a );
		vec.y = sinp * (float)sin( sph.a );		

		return vec;
	}

	//	sphere to vector
	inline  spheref		Vec2Sph( const vec3f& vec )
	{
		spheref sph;
		
		sph.p = (float) atan2( sqrt ( vec.x*vec.x + vec.y*vec.y ) , vec.z);
		
		sph.a = atan2( vec.y, vec.x );

		if (sph.a < 0 ) 
			sph.a += 2 * float(Pi);

		return sph;
	}
	

	typedef std::vector < spheref >	SpherefArray;
} // end of namespace

