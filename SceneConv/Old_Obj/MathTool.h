/********************************************************************
*
*		Subset of Inline Globle Fucntions 
*	
*	Description	:	Math Tool functions
*	From		:	Define the math functions by template
*
*	Author		:	Xi Wang
*	Date		:	12/02/2002
*
*********************************************************************/
#ifndef __MATH_TOOL__
#define __MATH_TOOL__

#include <math.h>

const double	Pi		= 3.1415926535;
const float		Delta	= 0.0001f;
const double	Eps		= 0.000000001;

namespace MathTool 
{

	////////////////////////////////////////////////////////////////////
	//Name:DataSwap
	//Func:Swap the two values 
	//Date:2002/12/02
	////////////////////////////////////////////////////////////////////
	template <class T>	
		inline void DataSwap(T& a, T& b)
	{		
		T tmp = a;		
		a = b;
		b = tmp;		
	}

	////////////////////////////////////////////////////////////////////
	//Name:DataClip
	//Func:clip the given val if it is beyond the given domain
	//Date:2002/10/25
	////////////////////////////////////////////////////////////////////
	template <class T>  
		inline T DataClip(const T& val, const T& Min, const T& Max)
	{			
		if (val < Min) return Min;
		if (val > Max) return Max;
		return val;
	}


	////////////////////////////////////////////////////////////////////
	//Name:DataInterpolate
	//Func:Interpolate the value
	//Date:2002/10/25
	////////////////////////////////////////////////////////////////////
	template <class T> 
		inline T DataInterpolate(const T& val1, const T& val2, const float f2val1)
	{	
		return ( val1 + (val2 - val1) * f2val1 );
	}

	////////////////////////////////////////////////////////////////////
	//Name:IsDataInDomain
	//Func:check whether the given value is in the domain or not
	//Date:2002/07/17
	////////////////////////////////////////////////////////////////////
	template <class T> 
		inline bool IsDataInDomain(const T& val, const T& Min, const T& Max)
	{		
		return ( (val >= Min) && (val <= Max) );
	}

	////////////////////////////////////////////////////////////////////
	//Name:Delta Equal
	//Func:Test the equal of value
	//Date:2002/08/08
	////////////////////////////////////////////////////////////////////
	template <class T> 
	inline bool DeltaEqual(const T& val1, const T& val2)
	{
		return ( fabs(val1 - val2) < Delta );
	}
	
	////////////////////////////////////////////////////////////////////
	//Name:GetMax
	//Func:get the larger one in the given 2 values
	//Date:2002/08/08
	////////////////////////////////////////////////////////////////////
	template <class T> 
		inline T GetMax(const T& val1, const T& val2)
	{
		return (val1 > val2) ?  val1 : val2;
	}

	////////////////////////////////////////////////////////////////////
	//Name:GetMin
	//Func:get the smaller one in the given 2 values
	//Date:2002/08/08
	////////////////////////////////////////////////////////////////////
	template <class T> 
		inline T GetMin(const T& val1, const T& val2)
	{
		return (val1 < val2) ?  val1 : val2;
	}

	////////////////////////////////////////////////////////////////////
	//Name:GetLowerINT
	//Func:get the lower bound integer value of the given value
	//Date:2002/07/17
	////////////////////////////////////////////////////////////////////
	template <class T> 
	 inline int GetLowerINT(const T& fVal)
	{
		int nVal = int (fVal);

		if (nVal > fVal)
			nVal --;

		ASSERT	(	( nVal <= fVal ) && 
					( fVal <= (nVal+1) ) 
				);

		return nVal;

	}

	////////////////////////////////////////////////////////////////////
	//Name:GetUpperINT
	//Func:get the upper bound integer value of the given value
	//Date:2002/07/17
	////////////////////////////////////////////////////////////////////
	template <class T> 
	 inline int GetUpperINT(const T& fVal)
	{
		int nVal = GetLowerINT(fVal);
		return nVal + 1;
	}	


	////////////////////////////////////////////////////////////////////
	//Name:DomainRepeat
	//Func:domain repeat in the regin 
	//Date:2002/07/17
	////////////////////////////////////////////////////////////////////	
	template <class T> 
	inline T DomainRepeat(const int& nVal, const T& nDomain)
	{		
		ASSERT( nDomain > 0 );

		T nPos;
		nPos = (T)(nVal % nDomain);

		if (nPos < 0) 
			nPos += nDomain;

		ASSERT ( (nPos >= 0) &&  ( nPos < nDomain ) );
		
		return nPos;
	}	


	////////////////////////////////////////////////////////////////////
	//Name:Round
	//Func:Round a float value to integer , find the nearest int
	//	   2.49 -> 2; 2.51-> 3
	//	   -2.49 -> -2 -2.51 -> -3
	//Date:2002/07/17
	////////////////////////////////////////////////////////////////////	
	template <class T> 
	inline int RoundINT(const T& fVal)
	{		
		int nLow = GetLowerINT(fVal);

		if ( double(fVal - nLow) >= 0.5)
			return nLow + 1;
		else
			return nLow;
	}



	/////////////////////////////////////////////////////////////////
	//	Name: FloatToByte
	//	Func: Convert signed Float value to unsigned byte value	
	//	Date: 2003-10-02
	/////////////////////////////////////////////////////////////////
	inline BYTE FloatToByte(const float& fVal, 
							const float& fMin = 0.0f, const float& fMax = 1.0f, 
							bool* pbOverflow = NULL)
	{
		int nVal = RoundINT( 255.0f * (fVal-fMin) / (fMax-fMin) );

		if (nVal<0)
		{		
			if (pbOverflow)		
				*pbOverflow	= true; 

			return 0;		
		}
		else if (nVal>255) 
		{	
			if (pbOverflow)		
				*pbOverflow	= true; 

			return 255;
		}
		else
		{
			if (pbOverflow)		
				*pbOverflow	= false; 

			return BYTE(nVal);
		}
	}

	/////////////////////////////////////////////////////////////////
	//	Name: ByteToFloat
	//	Func: Convert unsigned byte value to signed float value
	//	Date: 2003-10-02
	/////////////////////////////////////////////////////////////////	
	inline float ByteToFloat(const BYTE& byteVal,
							const float& fMin = 0.0f, const float& fMax = 1.0f)
	{
		float fVal=float(byteVal)/255.0f ;
		fVal = fVal *( fMax- fMin) + fMin;
		return fVal;
	}

	/////////////////////////////////////////////////////////////////
	//	Name: RandomFloat
	//	Func: Get random float value
	//	Date: 2003-10-02
	/////////////////////////////////////////////////////////////////	
	inline float RandomFloat(const float& fMin = 0.0f, const float& fMax = 1.0f, const UINT nPrecision = 10000)
	{
		float fVal= (rand() % nPrecision) / ((float) nPrecision);
		fVal = fVal * (fMax - fMin) + fMin;
		return fVal;
	}

}



#endif // __MATH_TOOL__
