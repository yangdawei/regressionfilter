// GeoObject.cpp: implementation of the CGeoObject class.
//
//////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "GeoObject.h"

#include  <io.h>
#include  <stdio.h>
#include  <direct.h>

namespace GeoObj
{

#define VERTEX_EXTRAVALUE	10.0f

	//////////////////////////////////////////////////////////////////////
	// Construction/Destruction
	//////////////////////////////////////////////////////////////////////

	CGeoObject::CGeoObject()
	{
		//	Step.1	Set Initial value
		m_nVertices  = 0;
		m_nEdges     = 0;
		m_nTriangles = 0;
		m_nGroups    = 0;
		m_nMaterials = 0;

		m_pVertices  = NULL;
		m_pEdges     = NULL;
		m_pTriangles = NULL;
		m_pGroups    = NULL;
		m_pMaterials = NULL;

		m_bTextureLoaded = false;
		m_bBindTexture   = false;

		//	Step.2	Clear Data
		ClearData();
	}

	CGeoObject::~CGeoObject()
	{
		ClearData();
	}


	//////////////////////////////////////////////////////////////////////
	//	Free Geometry
	void CGeoObject::ClearData()
	{
		//	Clear Geometry
		if ( NULL != m_pVertices )		delete [] m_pVertices;
		if ( NULL != m_pEdges )			delete [] m_pEdges;
		if ( NULL != m_pTriangles )		delete [] m_pTriangles;
		if ( NULL != m_pGroups )		delete [] m_pGroups;
		if ( NULL != m_pMaterials )		delete [] m_pMaterials;

		m_nVertices  = 0;
		m_nEdges     = 0;
		m_nTriangles = 0;
		m_nGroups    = 0;
		m_nMaterials = 0;

		m_pVertices  = NULL;
		m_pEdges     = NULL;
		m_pTriangles = NULL;
		m_pGroups    = NULL;
		m_pMaterials = NULL;

		//	Clear Bounding
		m_vMax = m_vMin = m_vCenter = vec3f(0,0,0);

		//	Clear file info
		m_sFileName.Empty ();

		//////////////////////////////////////////////////////////////////
		m_bIsInitial = false;

		memset( m_sMtlFile, 0, sizeof(char)*256 );
	}



	//////////////////////////////////////////////////////////////////////
	//////////////////////////////////////////////////////////////////////
	//
	//	Build Topology Functions
	//
	//////////////////////////////////////////////////////////////////////
	//////////////////////////////////////////////////////////////////////


	////////////////////////////////////////////////////////////////////////
	//	Name:	SearchVerticesNeighbor
	//	Func:	Search vertices neighbors and calculate the edge lengths
	//	Date:	2002/12/20
	////////////////////////////////////////////////////////////////////////
	void CGeoObject::SearchVerticesNeighbor()
	{
		int		i, j, k, l, m,
				nVIdx, nNeighbor, *npNeighbor, nNeighborIndex;
		float*	fpEdgeLengthes;
		CVertex*pVrt;
		bool	bFound;

		for ( i = 0; i < m_nTriangles; i ++ )	// For each triangle
		{
			for ( j = 0; j < 3; j ++ )			// For each vertices of this triangle
			{
				nVIdx = m_pTriangles[i].m_nVertexIndices[j];
				pVrt  = &m_pVertices[nVIdx];

				// If all the neighbor if this vertex has already been found
				if ( pVrt->m_nNeighbor > 0 )
					continue;

				// Preparing the maximum possible buffer
				npNeighbor     = new int [pVrt->m_nTriangle*2];
				fpEdgeLengthes = new float [pVrt->m_nTriangle*2];
				nNeighbor      = 0;

				// For each sounding triangle
				for ( k = 0; k < pVrt->m_nTriangle; k ++ )
				{
					// found the two connected vertices
					for ( m = 0; m < 3; m ++ )
					{
						if (m_pTriangles[ pVrt->m_pTriangleIndices[k] ] .m_nVertexIndices[m] == nVIdx)
							continue;

						bFound = false;
						nNeighborIndex = m_pTriangles[ pVrt->m_pTriangleIndices[k] ] .m_nVertexIndices[m];
						for ( l = 0; l < nNeighbor; l ++ )
						{
							if (npNeighbor[l] == nNeighborIndex) // has already been added
							{
								bFound = true;
								break;
							}
						}

						if ( bFound ) continue;

						// Add a new point to the neighbor list and compute the related edge length
						npNeighbor[ nNeighbor ] = nNeighborIndex;
						fpEdgeLengthes[ nNeighbor ] = ( m_pVertices[ nVIdx ] .m_vPosition -
							m_pVertices[ nNeighborIndex ] .m_vPosition ) .length();
						nNeighbor ++;
					}
				}// End of for each sounding triangle --

				pVrt->m_nNeighbor = nNeighbor;
				pVrt->m_npVertexIndices = new int [nNeighbor];
				pVrt->m_fpEdgeLengthes  = new float [nNeighbor];
				memcpy( pVrt->m_npVertexIndices, npNeighbor,     sizeof(int)*nNeighbor );
				memcpy( pVrt->m_fpEdgeLengthes,  fpEdgeLengthes, sizeof(float)*nNeighbor );
				delete [] npNeighbor;		npNeighbor     = NULL;
				delete [] fpEdgeLengthes;	fpEdgeLengthes = NULL;
			}// -- End of for each vertices of this triangle --
		}// -- End of for each triangle --
	}


	////////////////////////////////////////////////////////////////////////
	//	Name:	BuildEdgeLink
	//	Func:	build the edges
	//	Date:	2002/12/20
	////////////////////////////////////////////////////////////////////////
	void CGeoObject::BuildEdgeLink()					 
	{
		int	i, j, k,
			nVIdx[2], nTIdx,
			nEdgePtr = 0;
		CEdge	*pEdges;
		bool	bFound;

		pEdges = new CEdge [ m_nTriangles*3 ];
		for (i = 0; i < m_nTriangles; i ++)
		{
			for (j = 0; j < 3; j ++)
			{
				// If this edge has already been added to the edge link, continue
				if (m_pTriangles[i] .m_nEdgeIndices[j] != -1)
					continue;

				// Add the edge to edge link
				m_pTriangles[i] .m_nEdgeIndices[j] = nEdgePtr;

				nVIdx[0] = m_pTriangles[i] .m_nVertexIndices[ j ];
				nVIdx[1] = m_pTriangles[i] .m_nVertexIndices[ (j+1)%3 ];
				pEdges[ nEdgePtr ] .m_nTriangleIndices[0] = i;
				pEdges[ nEdgePtr ] .m_nVertexIndices[0]   = nVIdx[0];
				pEdges[ nEdgePtr ] .m_nVertexIndices[1]   = nVIdx[1];

				//------------------------------------------------------------------------
				// change to contain dual half edge structure
				// xiaohua, 2004.04.19
				//------------------------------------------------------------------------
				
				// Search the other triangle that share this edge, change the status
				bFound = false;
				for (k = 0; k < m_pVertices[nVIdx[0]].m_nTriangle; k ++)
				{
					nTIdx = m_pVertices[nVIdx[0]].m_pTriangleIndices[k];
					if (nTIdx == i)	continue;

					/*
					0 ----(2)---- 2      0, 1, 2 vertex indices
					\         /        (0), (1) (2) edge indices
					\       /
					(0)   (1)
					\   /
					\ /
					1
					*/
						
					int nFoundEdgePtr = -1;
					if ( m_pTriangles[nTIdx].m_nVertexIndices[0] == nVIdx[1] )
					{
						if (m_pTriangles[nTIdx].m_nVertexIndices[1] == nVIdx[0])
							nFoundEdgePtr = m_pTriangles[nTIdx].m_nEdgeIndices[0];
						else if (m_pTriangles[nTIdx].m_nVertexIndices[2] == nVIdx[0])
							nFoundEdgePtr = m_pTriangles[nTIdx].m_nEdgeIndices[2];
						
						bFound = true;
					}
					else if (m_pTriangles[nTIdx].m_nVertexIndices[1] == nVIdx[1])
					{
						if (m_pTriangles[nTIdx].m_nVertexIndices[0] == nVIdx[0])
							nFoundEdgePtr = m_pTriangles[nTIdx].m_nEdgeIndices[0];
						else if (m_pTriangles[nTIdx].m_nVertexIndices[2] == nVIdx[0])
							nFoundEdgePtr = m_pTriangles[nTIdx].m_nEdgeIndices[1];

						bFound = true;
					}
					else if (m_pTriangles[nTIdx].m_nVertexIndices[2] == nVIdx[1])
					{
						if (m_pTriangles[nTIdx].m_nVertexIndices[1] == nVIdx[0])
							nFoundEdgePtr = m_pTriangles[nTIdx].m_nEdgeIndices[1];
						else if (m_pTriangles[nTIdx].m_nVertexIndices[0] == nVIdx[0])
							nFoundEdgePtr = m_pTriangles[nTIdx].m_nEdgeIndices[2];

						bFound = true;
					}

					if (bFound)
					{
						pEdges[nEdgePtr].m_nTriangleIndices[1] = nTIdx;
						if ( -1 != nFoundEdgePtr)	// has been assigned
						{
							pEdges[nEdgePtr].m_nTwinEdge = nFoundEdgePtr;
							pEdges[nFoundEdgePtr].m_nTwinEdge = nEdgePtr;
							//ASSERT( pEdges[nEdgePtr].m_nTriangleIndices[0]==pEdges[nFoundEdgePtr].m_nTriangleIndices[1] );
							//ASSERT( pEdges[nEdgePtr].m_nTriangleIndices[1]==pEdges[nFoundEdgePtr].m_nTriangleIndices[0] );
							//ASSERT( pEdges[nEdgePtr].m_nVertexIndices[0]==pEdges[nFoundEdgePtr].m_nVertexIndices[1] );
							//ASSERT( pEdges[nEdgePtr].m_nVertexIndices[1]==pEdges[nFoundEdgePtr].m_nVertexIndices[0] );
						}
						break;
					} 
				}				

				nEdgePtr ++;

				//------------------------------------------------------------------------
				// end change
				// xiaohua, 2004.04.19
				//------------------------------------------------------------------------


				//////// Search the other triangle that share this edge, change the status
				//////bFound = false;
				//////for (k = 0; k < m_pVertices[nVIdx[0]].m_nTriangle; k ++)
				//////{
				//////	nTIdx = m_pVertices[nVIdx[0]].m_pTriangleIndices[k];
				//////	if (nTIdx == i)	continue;

				//////	/*
				//////	0 ----(2)---- 2      0, 1, 2 vertex indices
				//////	\         /        (0), (1) (2) edge indices
				//////	\       /
				//////	(0)   (1)
				//////	\   /
				//////	\ /
				//////	1
				//////	*/

				//////	if ( m_pTriangles[nTIdx].m_nVertexIndices[0] == nVIdx[1] )
				//////	{
				//////		if (m_pTriangles[nTIdx].m_nVertexIndices[1] == nVIdx[0])
				//////			m_pTriangles[nTIdx].m_nEdgeIndices[0] = nEdgePtr;
				//////		else if (m_pTriangles[nTIdx].m_nVertexIndices[2] == nVIdx[0])
				//////			m_pTriangles[nTIdx].m_nEdgeIndices[2] = nEdgePtr;

				//////		bFound = true;
				//////	}
				//////	else if (m_pTriangles[nTIdx].m_nVertexIndices[1] == nVIdx[1])
				//////	{
				//////		if (m_pTriangles[nTIdx].m_nVertexIndices[0] == nVIdx[0])
				//////			m_pTriangles[nTIdx].m_nEdgeIndices[0] = nEdgePtr;
				//////		else if (m_pTriangles[nTIdx].m_nVertexIndices[2] == nVIdx[0])
				//////			m_pTriangles[nTIdx].m_nEdgeIndices[1] = nEdgePtr;

				//////		bFound = true;
				//////	}
				//////	else if (m_pTriangles[nTIdx].m_nVertexIndices[2] == nVIdx[1])
				//////	{
				//////		if (m_pTriangles[nTIdx].m_nVertexIndices[1] == nVIdx[0])
				//////			m_pTriangles[nTIdx].m_nEdgeIndices[1] = nEdgePtr;
				//////		else if (m_pTriangles[nTIdx].m_nVertexIndices[0] == nVIdx[0])
				//////			m_pTriangles[nTIdx].m_nEdgeIndices[2] = nEdgePtr;

				//////		bFound = true;
				//////	}

				//////	if (bFound)
				//////	{
				//////		pEdges[nEdgePtr].m_nTriangleIndices[1] = nTIdx;
				//////		break;
				//////	}
				//////}

				//////nEdgePtr ++;
			}// -- End of for each vertex of this polygon
		}// -- End of for each triangle

		// Save the result link and free the temporal link
		m_nEdges = nEdgePtr;
		m_pEdges = new CEdge [m_nEdges];
		memcpy( m_pEdges, pEdges, sizeof(CEdge)*m_nEdges );
		if (m_nTriangles != 0) {
			delete [] pEdges;
			pEdges = NULL;
		}
	}


	////////////////////////////////////////////////////////////////////////
	//	Name:	BuildNeighborsEdgeLink
	//	Func:	build m_npEdgeIndices
	//	Date:	2002/12/20
	////////////////////////////////////////////////////////////////////////
	void CGeoObject::BuildNeighborsEdgeLink()			 
	{
		int i;

		for(i = 0; i < this->m_nVertices; i++)
			this->m_pVertices[i].m_npEdgeIndices = new int[this->m_pVertices[i].m_nNeighbor];

		for(i = 0; i < this->m_nEdges; i++)
		{
			int iVerIdx1, iVerIdx2;
			int iNeighbor;
			iVerIdx1 = this->m_pEdges[i].m_nVertexIndices[0];
			iVerIdx2 = this->m_pEdges[i].m_nVertexIndices[1];
			
			if ( iVerIdx1 == iVerIdx2 )
				continue;


			for(int j = 0; j < this->m_pVertices[iVerIdx1].m_nNeighbor; j++)
			{
				if(this->m_pVertices[iVerIdx1].m_npVertexIndices[j] == iVerIdx2)
				{
					iNeighbor = j;
					break;
				}
			}
			this->m_pVertices[iVerIdx1].m_npEdgeIndices[iNeighbor] = i;

			//------------------------------------------------------------------------
			// change to contain dual half edge structure
			// xiaohua, 2004.04.19
			//------------------------------------------------------------------------

			//////for(j = 0; j < this->m_pVertices[iVerIdx2].m_nNeighbor; j++)
			//////{
			//////	if(this->m_pVertices[iVerIdx2].m_npVertexIndices[j] == iVerIdx1)
			//////	{
			//////		iNeighbor = j;
			//////		break;
			//////	}
			//////}
			//////this->m_pVertices[iVerIdx2].m_npEdgeIndices[iNeighbor] = i;
		}

	}


	////////////////////////////////////////////////////////////////////////
	//	Name:	ComputeNormal
	//	Func:	compute the normals of vertices and triangles
	//	Date:	2002/12/20
	////////////////////////////////////////////////////////////////////////
	void CGeoObject::ComputeNormal()					
	{
		////////////////////////////////////////////////////////////////////
		//	Step.1	Check
		
		if (m_bBindNormal)
			return;			

		////////////////////////////////////////////////////////////////////
		// Step.2 for each triangle, compute its normal
		for (int i = 0; i < m_nTriangles; i++)
		{
			CTriangle *p_tri = &m_pTriangles[i];
			const vec3f& v0 = m_pVertices[p_tri->m_nVertexIndices[0]].m_vPosition;
			const vec3f& v1 = m_pVertices[p_tri->m_nVertexIndices[1]].m_vPosition;
			const vec3f& v2 = m_pVertices[p_tri->m_nVertexIndices[2]].m_vPosition;	

			vec3f n = VecCross( v1-v0 , v2-v0 );

			p_tri->m_vN = n ;
			p_tri->m_vN.unify();		

		}

		////////////////////////////////////////////////////////////////////
		// Step.3 for each vertex, compute its normal which is the average of its neighbor faces
		for (int i = 0; i < m_nVertices; i++)
		{
			CVertex *p_vert = &m_pVertices[i];
			p_vert->m_vN = vec3f(0, 0, 0);
			for ( int j=0; j<p_vert->m_nTriangle; j++)
			{
				CTriangle *p_tri = &m_pTriangles[p_vert->m_pTriangleIndices[j]];
				p_vert->m_vN = p_vert->m_vN + p_tri->m_vN;
			}
			p_vert->m_vN.unify();
		}
	}


	////////////////////////////////////////////////////////////////////////
	//	Name:	ComputeBoundingbox
	//	Func:	compute the bounding box
	//	Date:	2002/12/20
	////////////////////////////////////////////////////////////////////////
	void CGeoObject::ComputeBoundingbox(bool bSkipExtraVtx /* = true */)		 
	{
		//	Step.1	Compute bounding
		m_vMax = m_vMin = m_pVertices[0].m_vPosition;
		for( int i=1; i<m_nVertices; i++)
		{
			CVertex *pVertex = m_pVertices + i;

			if ( bSkipExtraVtx ) {
				if ( fabs(pVertex->m_vPosition.x) > VERTEX_EXTRAVALUE ||
					fabs(pVertex->m_vPosition.y) > VERTEX_EXTRAVALUE ||
					fabs(pVertex->m_vPosition.z) > VERTEX_EXTRAVALUE )
					continue;	// skip this vertex
			}

			if (m_vMax.x < pVertex->m_vPosition.x)
				m_vMax.x = pVertex->m_vPosition.x;
			if (m_vMin.x > pVertex->m_vPosition.x)
				m_vMin.x = pVertex->m_vPosition.x;

			if (m_vMax.y < pVertex->m_vPosition.y)
				m_vMax.y = pVertex->m_vPosition.y;
			if (m_vMin.y > pVertex->m_vPosition.y)
				m_vMin.y = pVertex->m_vPosition.y;

			if (m_vMax.z < pVertex->m_vPosition.z)
				m_vMax.z = pVertex->m_vPosition.z;
			if (m_vMin.z > pVertex->m_vPosition.z)
				m_vMin.z = pVertex->m_vPosition.z;
		}

		//	Step.2	Get center
		m_vCenter =( m_vMax + m_vMin ) * 0.5f;	

		//	Step.3	Compute each Triangles Center
		{
			for (int nTri = 0; nTri < m_nTriangles; nTri++)
			{
				CTriangle *p_tri = &m_pTriangles[nTri];
				const vec3f & v0 = m_pVertices[p_tri->m_nVertexIndices[0]].m_vPosition;
				const vec3f & v1 = m_pVertices[p_tri->m_nVertexIndices[1]].m_vPosition;
				const vec3f & v2 = m_pVertices[p_tri->m_nVertexIndices[2]].m_vPosition;

				p_tri->m_vCenter = (v0 + v1+ v2) * 0.33333f;	//Computer the centerm_vCenter

			}
		}

		//	Step.4	Output Info
		printf("   BoundingBox <%8.3f,%8.3f,%8.3f> <%8.3f,%8.3f,%8.3f>\n",
			m_vMax.x, m_vMax.y, m_vMax.z,
			m_vMin.x, m_vMin.y, m_vMin.z);
	}


	void CGeoObject::ComputeBoundingSphere()
	{
		vec3f minXYZ[3],maxXYZ[3];

		minXYZ[0] = minXYZ[1] = minXYZ[2] = m_pVertices[0].m_vPosition;
		maxXYZ[0] = maxXYZ[1] = maxXYZ[2] = m_pVertices[0].m_vPosition;

		for( int i=1; i<m_nVertices; i++)
		{
			for(int j=0;j<3;j++)
			{
				if(m_pVertices[i].m_vPosition[j]<minXYZ[j][j])
				{
					minXYZ[j] = m_pVertices[i].m_vPosition;
				}
				if(m_pVertices[i].m_vPosition[j]>maxXYZ[j][j])
				{
					maxXYZ[j] = m_pVertices[i].m_vPosition;
				}
			}
		}
		
		//Initialize the radius and center of the bounding sphere
		float radius = 0;
		vec3f vCenter;
		int index = -1;
		for(int i=0;i<3;i++)
		{
			if((maxXYZ[i]-minXYZ[i]).length()/2>radius)
			{
				radius = (maxXYZ[i]-minXYZ[i]).length()/2;
				index = i;
			}
		}
		vCenter = (minXYZ[index] + maxXYZ[index])/2;

		// second pass, set the final bounding sphere
		for(int i=0;i<m_nVertices;i++)
		{
			vec3f vDis = m_pVertices[i].m_vPosition - vCenter;
			float distance = vDis.length();
			if(distance>radius)
			{
				vCenter += vDis*( ((distance - radius)/2)/distance);
				radius = (distance + radius)/2;
			}
		}

		m_sphereCenter = vCenter;
		m_sphereRadius = radius;

		m_vCenter = m_sphereCenter;
		m_vMin = m_sphereCenter - vec3f(m_sphereRadius,m_sphereRadius,m_sphereRadius);
		m_vMax = m_sphereCenter + vec3f(m_sphereRadius,m_sphereRadius,m_sphereRadius);
	}

	void CGeoObject::ScaleObjectToUnify()
	{
		//Scale all vertexs
		for(int i=0;i<m_nVertices;i++)
		{
			m_pVertices[i].m_vPosition = (m_pVertices[i].m_vPosition - m_sphereCenter)/m_sphereRadius;
		}
		
		m_sphereCenter = vec3f(0,0,0);
		m_sphereRadius = 1.0f;
		
		m_vCenter = m_sphereCenter;
		m_vMin = m_sphereCenter - vec3f(m_sphereRadius,m_sphereRadius,m_sphereRadius);
		m_vMax = m_sphereCenter + vec3f(m_sphereRadius,m_sphereRadius,m_sphereRadius);
		
		printf("   BoundingSphere <%8.3f,%8.3f,%8.3f> r = %8.3f\n",
			m_sphereCenter.x,m_sphereCenter.y,m_sphereCenter.z,
			m_sphereRadius);
		
		printf("   BoundingBox <%8.3f,%8.3f,%8.3f> <%8.3f,%8.3f,%8.3f>\n",
			m_vMax.x, m_vMax.y, m_vMax.z,
			m_vMin.x, m_vMin.y, m_vMin.z);
	}
	////////////////////////////////////////////////////////////////////////
	//	Name:	ComputeTextureScale
	//	Func:	Calculate the texture scale for triangle, and transfer to vertex
	//	Date:	2002/12/20
	////////////////////////////////////////////////////////////////////////
	void CGeoObject::ComputeTexScale()
	{	

		m_fScale=0;

		////////////////////////////////////////////////////////////////////
		//	Step.0	Check
		if ( m_bBindTexture )
			printf("     Compute the texture scale for OBJ\n");
		else
		{
			printf("     Can't compute the texture scale, NO texture!\n");
			return;
		}

		float fGeomArea,fTexArea;

		////////////////////////////////////////////////////////
		//	Step.1	Calculate scale for each triangle
		for ( int i=0;i<m_nTriangles ;i++ )
		{
			CTriangle *pTriangle=&m_pTriangles[i];
			//calculate Triangle area
			{
				const vec3f & v0 = m_pVertices[ pTriangle->m_nVertexIndices[0] ].m_vPosition;
				const vec3f & v1 = m_pVertices[ pTriangle->m_nVertexIndices[1] ].m_vPosition;
				const vec3f & v2 = m_pVertices[ pTriangle->m_nVertexIndices[2] ].m_vPosition;

				vec3f n = VecCross( v1 - v0 ,  v2 - v0 );
				fGeomArea =(float) sqrt( VecDot(n , n) ) ;
			}
			pTriangle->m_fGeoArea=fGeomArea;//Store those value on triangle

			//Calculate the first layer Texture Triangle Area
			{
				const vec3f & v0 = pTriangle->m_vTexture [0];
				const vec3f & v1 = pTriangle->m_vTexture [1];
				const vec3f & v2 = pTriangle->m_vTexture [2];

				vec3f n = VecCross(( v1 - v0 ) , ( v2 - v0 ));
				fTexArea = (float)sqrt( VecDot(n , n) ) ;	

			}		

			//	Calculate the average texture scale
			pTriangle->m_fScale = (float)sqrt( fGeomArea / fTexArea );

			//	Accumulate the average texture scale of whole model
			m_fScale += pTriangle->m_fScale / m_nTriangles;	//Calculate the average triangle scale
		}


		////////////////////////////////////////////////////////
		//	Step.2	Transfer the fScale value to vertex
		for ( int nVrt=0; nVrt < m_nVertices; nVrt++ )
		{
			CVertex* pVrt=m_pVertices+nVrt;
			pVrt->m_fScale=0;

			int nTri;
			for (nTri=0; nTri < pVrt->m_nTriangle; nTri++ )
			{
				CTriangle* pTri = m_pTriangles + pVrt->m_pTriangleIndices [nTri];
				pVrt->m_fScale += pTri->m_fScale;
			}

			pVrt->m_fScale /= nTri;

		}
	}


	////////////////////////////////////////////////////////////////////////
	//	Name:	TransferNormalTexture
	//	Func:	Transfer the normal and texture information to new representation of geometry
	//	Date:	2002/12/20
	////////////////////////////////////////////////////////////////////////
	void CGeoObject::TransferNormalTexture(COBJmodel* pObjModel)
	{

		////////////////////////////////////////////////////////////////////
		//	Step.1 Transfer texture
		if ( m_bBindTexture )
		{
			for ( UINT nTri=0; nTri < pObjModel->nTriangles; nTri++ )
			{
				for ( UINT nVrt=0; nVrt < 3; nVrt++ )
				{				
					int tindex = pObjModel->pTriangles [nTri] .tindices [nVrt];

					if ( tindex )	//if tindex==0 means a invalid texture coordinate				
						m_pTriangles[nTri].m_vTexture[nVrt]=pObjModel->vpTexCoords [tindex];	//Copy texture coordinate to triangle			
				}
			}
		}

		////////////////////////////////////////////////////////////////////
		//	Step.1 Transfer Normal
		if ( m_bBindNormal )
		{
			int nTri, nVrt;
			////////////////////////////////////////////////////////////////
			//	1.1	Clean the Tri and Vertex normal
			for ( nTri=0; nTri < m_nTriangles; nTri++ )
				m_pTriangles [ nTri ] .m_vN = vec3f(0,0,0);

			for ( nVrt=0; nVrt<m_nVertices; nVrt++ )
				m_pVertices [ nVrt ] .m_vN = vec3f(0,0,0);


			////////////////////////////////////////////////////////////////
			//	1.2	Copy the value from obj model
			for ( nTri=0; nTri < (int)(pObjModel->nTriangles); nTri++ )
			{
				for ( nVrt=0; nVrt < 3; nVrt++ )
				{						
					int nindex = pObjModel->pTriangles [nTri].nindices [nVrt];
					int vindex = pObjModel->pTriangles [nTri].vindices [nVrt];		

					if (nindex)		
					{
						m_pVertices[vindex-1].m_vN	= m_pVertices[vindex-1].m_vN + pObjModel->vpNormals[nindex];	//	To vertex				
						m_pTriangles[nTri].m_vN		= m_pTriangles[nTri].m_vN + pObjModel->vpNormals[nindex];		//	To triangle
					}			
				}	
			}


			////////////////////////////////////////////////////////////////
			//	1.3	Normalize all
			for ( nTri=0; nTri < m_nTriangles; nTri++ )
				m_pTriangles[nTri].m_vN.unify ();

			for ( nVrt=0; nVrt < m_nVertices; nVrt++ )
				m_pVertices[nVrt].m_vN.unify ();
		}

	}


	////////////////////////////////////////////////////////////////////////
	//	Name:	ComputeTextureOrientation
	//	Func:	If texture coordinate available, compute texture orientation
	//	Date:	2002/12/20
	////////////////////////////////////////////////////////////////////////
	void CGeoObject::ComputeTexOrientation()
	{
		////////////////////////////////////////////////////////////////////
		//	Step.0	Check
		if (m_bBindTexture)
			printf("     Compute the texture orientation for OBJ\n");
		else
		{
			printf("     Can't compute the texture orientation, NO texture!\n");
			return;
		}


		////////////////////////////////////////////////////////////////////
		//	Step.1	Calculate S and T for each triangle
		for (int i=0;i<m_nTriangles;i++)
		{
			vec3f vS,vT;
			vec3f* vTex=m_pTriangles[i].m_vTexture;		//Texture Coordinate


			int	vIdx[3];	//Index of 3 vertices
			vIdx[0] = m_pTriangles[i].m_nVertexIndices[0];
			vIdx[1] = m_pTriangles[i].m_nVertexIndices[1];
			vIdx[2] = m_pTriangles[i].m_nVertexIndices[2];

			vec3f vP[3];		//Position of 3 vertices
			vP[0] = m_pVertices [vIdx[0]].m_vPosition ;
			vP[1] = m_pVertices [vIdx[1]].m_vPosition ;
			vP[2] = m_pVertices [vIdx[2]].m_vPosition ;

			if (fabs(vTex[0].y-vTex[1].y)<Delta)		//P1-P0 parallel to S coordination
			{
				if (vTex[0].x>vTex[1].x)
					vS=vP[0]-vP[1];
				else
					vS=vP[1]-vP[0];
				vS.unify ();					
			}
			else
			{
				vec3f vU;	//vU.x+vU.y+vU.z==0 and let vU.z==1
				vU.x=(vTex[2].y-vTex[1].y)/(vTex[1].y-vTex[0].y);
				vU.y=(vTex[0].y-vTex[2].y)/(vTex[1].y-vTex[0].y);
				vU.z=1;

				//Check the S coordination direction 
				{
					vec3f vTestS=vTex[0]*vU.x+vTex[1]*vU.y+vTex[2]*vU.z;
					//		ASSERT(fabs(vTestS.y)<Delta);
					if (vTestS.x<0)				
						vU=vU*(-1);	//Change the direction	

				}

				vS=vP[0]*vU.x+vP[1]*vU.y+vP[2]*vU.z;
				vS.unify();
			}

			vT=VecCross(m_pTriangles[i].m_vN , vS);
			vT.unify();

			//Store the value to triangles
			m_pTriangles[i].m_vS=vS;
			m_pTriangles[i].m_vT=vT;



		}

		////////////////////////////////////////////////////////////////////
		//	Step.2	Transfer S,T for each Vertex
		for (int i=0;i<m_nVertices;i++)
		{
			CVertex* pVrt=m_pVertices+i;

			vec3f& vS = pVrt->m_vS;
			vec3f& vT = pVrt->m_vT;

			vT = vS = vec3f(0,0,0);			// Clear old value		


			for (int j=0;j<pVrt->m_nTriangle ;j++)
			{
				int ntIdx = pVrt->m_pTriangleIndices [j];
				vS = vS+m_pTriangles[ntIdx].m_vS;
				vT = vT+m_pTriangles[ntIdx].m_vT;
			}

			vS.unify ();
			vT.unify ();
		}
	}


	////////////////////////////////////////////////////////////////////////
	//	Name:	ConvertData
	//	Func:	Convert from a ObjModel
	//	Date:	2002/12/20
	////////////////////////////////////////////////////////////////////////
	void CGeoObject::ConvertData(COBJmodel* pObjModel)
	{
		////////////////////////////////////////////////////////////////////
		// A special data structure defined for convertion
		// this is the list of all vertices, used for building their face neighbors
		typedef struct _tmp_pnt_list
		{
			int	nPID,		        // the vertex index in the list
				nNID,               // the flag that whether this vertex has been dealt with the neighbor faces
				nTriNum,	        // number of neighbor triangles
				*npTriLnk;	        // list of the neighbor triangles index
			_tmp_pnt_list *next;    // the next vertex node in the list
		} tmp_pnt_list;
		////////////////////////////////////////////////////////////////////

		////////////////////////////////////////////////////////////////////
		// Step.2 build the face neighbors of each vertex
		printf("\n\n========================================================\n");
		printf("   begin building face neighbors ...  \n");

		// 2.1 get the vertices
		int				nVertices	= pObjModel->nVertices;
		vec3f		*pvVertices	= pObjModel->vpVertices;

		tmp_pnt_list	*pNewPointLnk = new tmp_pnt_list[nVertices+1];
		for (int i = 0; i <= nVertices; i ++)
		{
			pNewPointLnk[i].nPID = i;
			pNewPointLnk[i].nTriNum = 0;
			pNewPointLnk[i].npTriLnk = NULL;
			pNewPointLnk[i].nNID = -1;
		}

		// 2.2. triangle list memory
		m_nTriangles = pObjModel->nTriangles;
		m_pTriangles = new CTriangle [m_nTriangles];

		// for each triangle, add it to its vertices' face neighbors
		for (int i = 0; i < m_nTriangles; i ++)  // for each triangle
		{

			for( int j = 0; j < 3; j ++ )  // for each vertex of this triangle
			{
				int nPID = pObjModel->pTriangles[i].vindices[j];  // the index of the vertex

				// If it is the first triangle that share this vertex
				if (pNewPointLnk[nPID].nNID == -1)
				{
					pNewPointLnk[nPID].nNID = 1;
					pNewPointLnk[nPID].nTriNum = 1;
					pNewPointLnk[nPID].npTriLnk = new int[1];
					pNewPointLnk[nPID].npTriLnk[0] = i; 

					m_pTriangles[i].m_nVertexIndices[j] = nPID-1;
				}
				else    // If it is not the first triangle
				{
					int nNeiTriNum = pNewPointLnk[nPID].nTriNum ;
					int *pTriLnk = pNewPointLnk[nPID].npTriLnk;
					pNewPointLnk[nPID].npTriLnk = new int[ nNeiTriNum + 1 ];
					memcpy(pNewPointLnk[nPID].npTriLnk, pTriLnk, nNeiTriNum*sizeof(int));
					pNewPointLnk[nPID].npTriLnk[ nNeiTriNum ] = i;
					pNewPointLnk[nPID].nTriNum ++;
					delete [] pTriLnk;

					m_pTriangles[i].m_nVertexIndices[j] = nPID-1;
				}
			}
		}
		////////////////////////////////////////////////////////////////////


		////////////////////////////////////////////////////////////////////    
		// Step.3  copy back to the vertices data structure
		m_nVertices = nVertices;
		m_pVertices = new CVertex[m_nVertices];

		for (int i = 0; i < nVertices; i ++)
		{
			int ii = i + 1;
			m_pVertices[i].m_vPosition = pvVertices[pNewPointLnk[ii].nPID];
			m_pVertices[i].m_nTriangle = pNewPointLnk[ii].nTriNum;


			if(pNewPointLnk[ii].nTriNum != 0)
			{
				m_pVertices[i].m_pTriangleIndices = new int[pNewPointLnk[ii].nTriNum];
				memcpy(m_pVertices[i].m_pTriangleIndices,
					pNewPointLnk[ii].npTriLnk, sizeof(int)*pNewPointLnk[ii].nTriNum);
			}

			if ( pNewPointLnk[ii].npTriLnk != NULL )
				delete [] pNewPointLnk[ii].npTriLnk;
		}
		delete [] pNewPointLnk;
		////////////////////////////////////////////////////////////////////



		////////////////////////////////////////////////////////////////////
		// Step.4 build the group

		// 4.2. group memory
		//m_nGroups = 1;
		//m_pGroups = new CGroup[1];
		//m_pGroups[0].m_nMaterialIndex = 0;
		//m_pGroups[0].m_nTriangles = m_nTriangles;
		//m_pGroups[0].m_pTirangleIndices = new int [m_nTriangles];
		//for (i = 0; i < m_nTriangles; i ++)
		//	m_pGroups[0].m_pTirangleIndices[i] = i;
		
		m_nGroups = pObjModel->nGroups;
		m_pGroups = new CGroup[m_nGroups];
		AccessObj::COBJgroup * pObjGroup = pObjModel->pGroups;
		for ( int igroup=0; igroup<m_nGroups; ++igroup ) 
		{
			CGroup * pGeoGroup = &m_pGroups[igroup];
			ASSERT( pGeoGroup && pObjGroup );
			sprintf( pGeoGroup->m_sName, "%s", pObjGroup->name );
			pGeoGroup->m_nMaterialIndex = pObjGroup->material;
			if ( pObjGroup->nTriangles > 0 )
			{
				pGeoGroup->m_nTriangles = pObjGroup->nTriangles;
				pGeoGroup->m_pTirangleIndices = new int [pGeoGroup->m_nTriangles];
				for (int i = 0; i < pGeoGroup->m_nTriangles; i ++)
					pGeoGroup->m_pTirangleIndices[i] = pObjGroup->pTriangles[i];
			}
			if ( pObjGroup->m_nMatNum >= 0 )
			{
				pGeoGroup->m_nMatNum = pObjGroup->m_nMatNum;
				pGeoGroup->m_pnMatIdx = new int[ pGeoGroup->m_nMatNum+1 ];
				pGeoGroup->m_pnTriIdx = new int[ pGeoGroup->m_nMatNum+1 ];
				for (int i=0; i <= pGeoGroup->m_nMatNum; i++ )
				{
					pGeoGroup->m_pnMatIdx[i] = pObjGroup->m_pnMatIdx[i];
					pGeoGroup->m_pnTriIdx[i] = pObjGroup->m_pnTriIdx[i];
				}
			}
			pObjGroup = pObjGroup->next;			
		}

		sprintf( m_sMtlFile, "%s", pObjModel->mtllibname );
		m_nMaterials = pObjModel->nMaterials;
		if ( m_nMaterials > 0 )
		{
			m_pMaterials = new CMaterial[m_nMaterials];
			for ( int imat=0; imat<m_nMaterials; ++imat )
			{
				CMaterial * pGeoMat = &m_pMaterials[imat];
				AccessObj::COBJmaterial * pObjMat = &pObjModel->pMaterials[imat];
				ASSERT( pGeoMat && pObjMat );
				sprintf( pGeoMat->m_sName, "%s", pObjMat->name );
				memcpy( pGeoMat->m_fDiffuse, pObjMat->diffuse, sizeof(float)*4 );
				memcpy( pGeoMat->m_fAmbient, pObjMat->ambient, sizeof(float)*4 );
				memcpy( pGeoMat->m_fSpecular, pObjMat->specular, sizeof(float)*4 );
				memcpy( pGeoMat->m_fEmissive, pObjMat->emissive, sizeof(float)*4 );
				memcpy( pGeoMat->m_fShininess, pObjMat->shininess, sizeof(float)*1 );
				if ( pObjMat->bTextured )
				{
					pGeoMat->m_bTextured = pObjMat->bTextured;
					sprintf( pGeoMat->m_sTexture, "%s", pObjMat->sTexture );
					pGeoMat->m_nWidth = pObjMat->nWidth;
					pGeoMat->m_nHeight = pObjMat->nHeight;
					int size = pGeoMat->m_nWidth * pGeoMat->m_nHeight * 3;
					pGeoMat->m_pTexImage = new unsigned char[size];
					memcpy( pGeoMat->m_pTexImage, pObjMat->pTexImage, size );
				}
			}
		}

		// 5. build the vertex neighbor of vertices
		// Search vertices neighbors and calc the edge lengths
		printf("   Searching neighbor of each vertex ... \n");
		printf("   build the vertex neighbors of the vertices... \n");
		SearchVerticesNeighbor();


		// 6. build the edges	
		BuildEdgeLink();


		// 7. build m_npEdgeIndices. by sqyu@msra
		BuildNeighborsEdgeLink();


		//	8.	Get the normal of texture information

		if (pObjModel->vpNormals) 
		{
			m_bBindNormal = true;
			printf("     WITH Vertex Normal Information available in OBJ file!\n");			
		}
		else
		{
			m_bBindNormal = false;
			printf("     NO Vertex Normal Information available in OBJ file!\n");			
		}


		if (pObjModel->vpTexCoords ) 
		{
			m_bBindTexture = true;
			printf("     WITH Vertex Texture Information available in OBJ file!\n");			
		}
		else
		{
			m_bBindTexture = false;
			printf("     NO Vertex Texture Information available in OBJ file!\n");			
		}	

		//If there are normal information available in OBJ model, vertex normal information will be overwritten.
		TransferNormalTexture(pObjModel);


		//	9.	Output Information	
		printf("     Finished. %d vertices, %d triangles\n", m_nVertices, m_nTriangles);

	}



	/////////////////////////////////////////////////////////////////////
	/////////////////////////////////////////////////////////////////////
	//
	//	Draw Model in different style Functions
	//	Date	:	2002/12/20	
	//
	/////////////////////////////////////////////////////////////////////
	/////////////////////////////////////////////////////////////////////	

	/////////////////////////////////////////////////////////////////////	
	void CGeoObject::DrawSceneByPoints()
	{
		glDisable(GL_TEXTURE_2D);	

		glPointSize(3);
		glBegin(GL_POINTS);
		for (int nVrt = 0; nVrt < m_nVertices; nVrt ++)
		{
			CVertex* pVrt=m_pVertices+nVrt;
			glNormal3f(pVrt->m_vN .x,pVrt->m_vN .y,pVrt->m_vN .z);
			glVertex3f(pVrt->m_vPosition .x,pVrt->m_vPosition .y,pVrt->m_vPosition .z);
		}
		glEnd();
		glPointSize(1);

	}

	/////////////////////////////////////////////////////////////////////	
	void CGeoObject::DrawSceneByLineWire()
	{
		int	i;
		vec3f *v[2];
		vec3f *n[2];

		glDisable(GL_TEXTURE_2D);

		{
			glBegin(GL_LINES);
			for (i = 0; i < m_nEdges; i ++)
			{		


				v[0] = &m_pVertices[m_pEdges[i].m_nVertexIndices[0]].m_vPosition;
				v[1] = &m_pVertices[m_pEdges[i].m_nVertexIndices[1]].m_vPosition;

				n[0] = &m_pVertices[m_pEdges[i].m_nVertexIndices[0]].m_vN;
				n[1] = &m_pVertices[m_pEdges[i].m_nVertexIndices[1]].m_vN;

				glNormal3f(n[0]->x,n[0]->y,n[0]->z);
				glVertex3f(v[0]->x, v[0]->y, v[0]->z);

				glNormal3f(n[0]->x,n[0]->y,n[0]->z);
				glVertex3f(v[1]->x, v[1]->y, v[1]->z);
			}

			glEnd();
		}

	}

	/////////////////////////////////////////////////////////////////////	
	void CGeoObject::DrawSceneByUntexturedTriangles()
	{
		int	i, j, ntIdx;
		vec3f *v[3], *n[3];
		glDisable(GL_TEXTURE_2D);

		glBegin(GL_TRIANGLES);
		{
			for (i = 0; i < m_nGroups; i ++)
			{
				for (j = 0; j < m_pGroups[i].m_nTriangles; j ++)
				{		

					ntIdx = m_pGroups[i].m_pTirangleIndices[j];

					v[0] = &m_pVertices[m_pTriangles[ntIdx].m_nVertexIndices[0]].m_vPosition;
					v[1] = &m_pVertices[m_pTriangles[ntIdx].m_nVertexIndices[1]].m_vPosition;
					v[2] = &m_pVertices[m_pTriangles[ntIdx].m_nVertexIndices[2]].m_vPosition;

					n[0] = &m_pVertices[m_pTriangles[ntIdx].m_nVertexIndices[0]].m_vN;
					n[1] = &m_pVertices[m_pTriangles[ntIdx].m_nVertexIndices[1]].m_vN;
					n[2] = &m_pVertices[m_pTriangles[ntIdx].m_nVertexIndices[2]].m_vN;

					{						
						glNormal3f(n[0]->x, n[0]->y, n[0]->z);
						glVertex3f(v[0]->x, v[0]->y, v[0]->z);				

						glNormal3f(n[1]->x, n[1]->y, n[1]->z);
						glVertex3f(v[1]->x, v[1]->y, v[1]->z);					

						glNormal3f(n[2]->x, n[2]->y, n[2]->z);
						glVertex3f(v[2]->x, v[2]->y, v[2]->z);					
					}
				}
			}
		}
		glEnd();
	}

	/////////////////////////////////////////////////////////////////////	
	void CGeoObject::DrawSceneByTexturedTriangles()
	{
		int	i, j, ntIdx;
		vec3f *v[3], *n[3], *t[3];

		glColor3f(1.f, 1.f, 1.f);
		SetMaterial (1.0f,1.0f,1.0f);		

		glBegin(GL_TRIANGLES);
		{
			for (i = 0; i < m_nGroups; i ++)
			{
				for (j = 0; j < m_pGroups[i].m_nTriangles; j ++)
				{	

					ntIdx = m_pGroups[i].m_pTirangleIndices[j];


					v[0] = &m_pVertices[m_pTriangles[ntIdx].m_nVertexIndices[0]].m_vPosition;
					v[1] = &m_pVertices[m_pTriangles[ntIdx].m_nVertexIndices[1]].m_vPosition;
					v[2] = &m_pVertices[m_pTriangles[ntIdx].m_nVertexIndices[2]].m_vPosition;

					n[0] = &m_pVertices[m_pTriangles[ntIdx].m_nVertexIndices[0]].m_vN;
					n[1] = &m_pVertices[m_pTriangles[ntIdx].m_nVertexIndices[1]].m_vN;
					n[2] = &m_pVertices[m_pTriangles[ntIdx].m_nVertexIndices[2]].m_vN;

					t[0] = &m_pTriangles[ntIdx].m_vTexture[0];
					t[1] = &m_pTriangles[ntIdx].m_vTexture[1];
					t[2] = &m_pTriangles[ntIdx].m_vTexture[2];			
					{	

						glTexCoord2f(t[0]->x, t[0]->y);
						glNormal3f(n[0]->x, n[0]->y, n[0]->z);
						glVertex3f(v[0]->x, v[0]->y, v[0]->z);

						glTexCoord2f(t[1]->x, t[1]->y);
						glNormal3f(n[1]->x, n[1]->y, n[1]->z);
						glVertex3f(v[1]->x, v[1]->y, v[1]->z);

						glTexCoord2f(t[2]->x, t[2]->y);
						glNormal3f(n[2]->x, n[2]->y, n[2]->z);
						glVertex3f(v[2]->x, v[2]->y, v[2]->z);

					}
				}
			}
		}
		glEnd();
	}


	/////////////////////////////////////////////////////////////////////	
	//	Show the wireframe
	void CGeoObject::DrawWireFrame()
	{
#define LineOffsetDelta	0.001f

		glDisable(GL_TEXTURE_2D);
		glDisable(GL_LIGHTING);

		vec3f vP;
		vec3f vN;

		// Drawing wireframe

		glColor3f (0.5f, 0.35f, 0.35f);
		glBegin(GL_LINES);
		{
			for (int i = 0; i < m_nEdges; i ++)
			{
				for (int nVrt=0;nVrt<2;nVrt++)
				{
					int nVrtIdx=m_pEdges[i].m_nVertexIndices[nVrt];

					vP = m_pVertices[nVrtIdx].m_vPosition;
					vN = m_pVertices[nVrtIdx].m_vN ;

					vP=vP + vN* LineOffsetDelta;					
					glVertex3f(vP.x, vP.y, vP.z);				
				}			
			}
		}
		glEnd();
	}


	////////////////////////////////////////////////////////////////////////
	//	Name:	SetMaterial
	//	Func:	Set current material
	//	Date:	2002/12/20
	////////////////////////////////////////////////////////////////////////
	void CGeoObject::SetMaterial(float fR,float fG,float fB)
	{	
		float fAmbient[4]  = { 0.0f, 0.0f, 0.0f, 1.0f };
		float fDiffuse[4]  = { 1.0f, 1.0f, 1.0f, 1.0f };
		float fSpecular[4] = { 1.0f, 1.0f, 1.0f, 1.0f };		


		fDiffuse[0]=fR;	fDiffuse[1]=fG;	fDiffuse[2]=fB;


		//	Set material
		glMaterialfv(GL_FRONT_AND_BACK, GL_AMBIENT, fAmbient);
		glMaterialfv(GL_FRONT_AND_BACK, GL_DIFFUSE, fDiffuse);
		glMaterialfv(GL_FRONT_AND_BACK, GL_SPECULAR,fSpecular);
		glMaterialf(GL_FRONT_AND_BACK,	GL_SHININESS, 20);



		//	Set color
		glColor3f(fDiffuse[0], fDiffuse[1], fDiffuse[2]);


	}



	////////////////////////////////////////////////////////////////////////
	//	Name:	DrawScene
	//	Func:	Draw the model with specified rendering mode
	//	Date:	2002/12/20
	////////////////////////////////////////////////////////////////////////
	void CGeoObject::DrawScene(int nRenderingMode,  vec3f vColor )
	{

		{
			//	Set the model material
			SetMaterial(vColor.x,vColor.y,vColor.z);

			// Drawing element
			if (nRenderingMode & DRAW_POINTS_SCENE)
			{
				DrawSceneByPoints();			
			}
			else if (nRenderingMode & DRAW_WIREFRAME_TRIANGLES)
			{
				DrawSceneByLineWire();			
			}
			else if (nRenderingMode & DRAW_UNTEXTURED_TRIANGLES)
			{
				DrawSceneByUntexturedTriangles();			
			}
			else if (nRenderingMode & DRAW_TEXTURED_TRIANGLES)
			{
				glEnable(GL_TEXTURE_2D);		

				DrawSceneByTexturedTriangles();

				glDisable(GL_TEXTURE_2D);			
			}

		}	

		//draw wire frame
		if (nRenderingMode & SHOW_WIRE_FRAME)
			DrawWireFrame();

	}

	////////////////////////////////////////////////////////////////////////
	//	Name:	LoadOBJModel
	//	Func:	Load the obj model from specified file
	//	Date:	2002/12/20
	////////////////////////////////////////////////////////////////////////
	BOOL CGeoObject::LoadOBJModel(CString sFileName)
	{
		////////////////////////////////////////////////////////////////////
		//		Step.1		Clear old Data in CGeoScene
		ClearData();			

		printf(" -------------------------------------------------------\n");


		////////////////////////////////////////////////////////////////////
		//		Step.2		Load model for OBJ file
		{		
			CAccessObj oaModel;	

			//		2.1		Load OBJ Model to CAccessObj object	
			if ( _access(sFileName,0)!=-1 )
				oaModel.LoadOBJ(sFileName.GetBuffer(200));
			else
			{
				printf("ERROR<CGeoObject::LoadOBJModel> %s File Not Exist!\n", sFileName);
				return FALSE;
			}

			//		2.2		Convert CAccessObj info to CGeoScene, and build Geometry link
			ConvertData(oaModel.m_pModel);		

			//		2.3		Delete the OBJ model
			oaModel.Destory();		


			//		2.4		Compute the useful informations

			ComputeNormal();					//  Compute the normal of faces and vertices	
			ComputeTexScale();					//	Compute the texture scale
			ComputeTexOrientation();			//	Compute the texture orientation
			ComputeBoundingSphere();
			//ScaleObjectToUnify();
			//ComputeBoundingbox ();				//	Compute the boundingbox
			
			//		2.5		Output the successful information
			m_sFileName.Format("%s",sFileName);
			printf("   Successfully load OBJ model <%s>\n",sFileName.GetBuffer(200));

		}

		printf(" -------------------------------------------------------\n");



		//	DEBUG CODE
#ifdef _DEBUG
		FILE* pFile  = fopen("Geom_Xfer.txt","w");
		for (int nVrt=0; nVrt<m_nVertices; nVrt++)
		{
			CVertex* pVrt = m_pVertices + nVrt;
			vec3f& vX = pVrt->m_vS;
			vec3f& vY = pVrt->m_vT;
			vec3f& vZ = pVrt->m_vN;
			float& fScale = pVrt->m_fScale ;

			fprintf(pFile, "<%6.3f, %6.3f, %6.3f>",vX.x, vX.y, vX.z);
			fprintf(pFile, "<%6.3f, %6.3f, %6.3f>",vY.x, vY.y, vY.z);
			fprintf(pFile, "<%6.3f, %6.3f, %6.3f>",vZ.x, vZ.y, vZ.z); 
			fprintf(pFile, "<%8.3f>",fScale); 

			fprintf(pFile,"\n");
		}

		fclose(pFile);
#endif

		////////////////////////////////////////////////////////////////////
		m_bIsInitial = true;		
		return TRUE;
	}	


	////////////////////////////////////////////////////////////////////////
	//	Name:	SaveOBJModel
	//	Func:	Save the obj model from specified file
	//	Date:	2002/12/20
	////////////////////////////////////////////////////////////////////////
	BOOL CGeoObject::SaveOBJModel(CString sFileName)
	{
		////////////////////////////////////////////////////////////////////
		//	Step.1	Build Temp File Access
		CAccessObj m_AccObj;

		if (!m_AccObj.m_pModel)
			m_AccObj.m_pModel=new COBJmodel;
		if (!m_AccObj.m_pModel)
		{
			printf("ERROR<CGeoObject::SaveOBJModel>: Fail to allocate model\n");
			return FALSE;
		}

		COBJmodel* pModel=m_AccObj.m_pModel;

		////////////////////////////////////////////////////////////////////
		//  Step.2	Build Vertex List	
		{
			//	Allocate
			pModel->nVertices =m_nVertices;
			pModel->vpVertices =new vec3f[pModel->nVertices+1];  //start from ONE
			if (!pModel->vpVertices )	//check allocate operation
			{		
				printf("ERROR<CGeoObject::SaveOBJModel>: Fail to allocate vertices\n");
				return FALSE;
			}

			//	Copy position
			for (int nVrt=0; nVrt<m_nVertices; nVrt++)
			{
				pModel->vpVertices[nVrt+1] = m_pVertices[nVrt].m_vPosition ;

			}
		}

		////////////////////////////////////////////////////////////////////
		//  Step.2B	Build Normal List
		if ( m_bBindNormal ) // added by xiaohua
		{
			//	Allocate
			pModel->nNormals = m_nTriangles;
			pModel->vpNormals =new vec3f[pModel->nNormals+1];  //start from ONE
			if (!pModel->vpNormals )	//check allocate operation
			{		
				printf("ERROR<CGeoObject::SaveOBJModel>: Fail to allocate normals\n");
				return FALSE;
			}

			//	Copy position
			for (int nNml=0; nNml<m_nTriangles; nNml++)
			{
				pModel->vpNormals[nNml+1] = m_pTriangles[nNml].m_vN;
			}
		}	

		////////////////////////////////////////////////////////////////////
		//  Step.3	Build Texture List
		{	
			//	Allocate
			pModel->nTexCoords  = m_nTriangles * 3 + 1;	
			pModel->vpTexCoords =new vec3f[pModel->nTexCoords+1]; //start from ONE
			if (!pModel->vpTexCoords )	//check allocate operation
			{
				printf("ERROR<CGeoObject::SaveOBJModel>: Fail to allocate textures\n");
				return FALSE;
			}

			//	Copy textures
			for (int nTri=0; nTri<m_nTriangles; nTri++)
			{
				CTriangle* pTri = m_pTriangles + nTri;
				for (int nT=0; nT<3; nT++)
				{
					int nTexIdx = 3*nTri + nT;
					pModel->vpTexCoords[nTexIdx + 1 ] = pTri->m_vTexture [nT];
				}
			}
		}

		////////////////////////////////////////////////////////////////////
		//  Step.4	Build Face List
		{
			//	Allocate
			pModel->nTriangles  =	m_nTriangles;
			pModel->pTriangles =new COBJtriangle[pModel->nTriangles];	//start from ZERO
			if (!pModel->pTriangles )//check allocate operation
			{
				printf("ERROR<CGeoObject::SaveOBJModel>: Fail to allocate triangles\n");
				return FALSE;
			}

			//	Copy triangles
			for (int nTri=0; nTri<m_nTriangles; nTri++)
			{
				CTriangle* pTri = m_pTriangles + nTri;
				COBJtriangle* pOBJTri = pModel->pTriangles + nTri ;

				for (int nK=0; nK<3; nK++)
				{
					pOBJTri->vindices	[nK] = pTri->m_nVertexIndices [nK] + 1;
					pOBJTri->nindices   [nK] = nTri + 1; // added by xiaohua
					pOBJTri->tindices	[nK] = nTri*3 + nK + 1;
				}
			}
		}		

		////////////////////////////////////////////////////////////
		//	Step.5	Add Triangles to default group
		{
			//pModel->nGroups =1;
			//pModel->pGroups =new COBJgroup;
			//if (!pModel->pGroups)
			//{
			//	printf("ERROR<CGeoObject::SaveOBJModel>: Fail to allocate pGroups\n");
			//	return FALSE;
			//}

			//sprintf(pModel->pGroups->name, "Default");		//Set the group name

			//pModel->pGroups->nTriangles=pModel->nTriangles ;
			//pModel->pGroups->pTriangles =new unsigned int[pModel->pGroups->nTriangles];
			//if (!pModel->pGroups->pTriangles)
			//{
			//	printf("ERROR<CGeoObject::SaveOBJModel>: Fail to allocate Triangle List for pGroups\n");
			//	return FALSE;
			//}

			////	Fill group
			//for (UINT nTri=0; nTri<pModel->pGroups->nTriangles; nTri++)
			//	pModel->pGroups->pTriangles[nTri]=nTri;

			pModel->nGroups = m_nGroups;
			pModel->pGroups = NULL;
			for ( int igroup=m_nGroups-1; igroup>=0; --igroup ) 
			{
				CGroup * pGeoGroup = &m_pGroups[igroup];
				AccessObj::COBJgroup * pObjGroup = new AccessObj::COBJgroup;
				ASSERT( pGeoGroup && pObjGroup );
				pObjGroup->next = pModel->pGroups;
				pModel->pGroups = pObjGroup;

				sprintf( pObjGroup->name, "%s", pGeoGroup->m_sName );
				pObjGroup->material = pGeoGroup->m_nMaterialIndex;
	
				if ( pGeoGroup->m_nTriangles > 0 )
				{
					pObjGroup->nTriangles = pGeoGroup->m_nTriangles;
					pObjGroup->pTriangles = new unsigned int[pObjGroup->nTriangles];
					for (unsigned int i = 0; i < pObjGroup->nTriangles; i ++)
						pObjGroup->pTriangles[i] = pGeoGroup->m_pTirangleIndices[i];
				}
				if ( pGeoGroup->m_nMatNum >= 0 )
				{
					pObjGroup->m_nMatNum = pGeoGroup->m_nMatNum;
					pObjGroup->m_pnMatIdx = new int[ pObjGroup->m_nMatNum+1 ];
					pObjGroup->m_pnTriIdx = new int[ pObjGroup->m_nMatNum+1 ];
					for ( int i=0; i <= pObjGroup->m_nMatNum; i++ )
					{
						pObjGroup->m_pnMatIdx[i] = pGeoGroup->m_pnMatIdx[i];
						pObjGroup->m_pnTriIdx[i] = pGeoGroup->m_pnTriIdx[i];
					}
				}
		
			}

			sprintf( pModel->mtllibname, "%s", m_sMtlFile );
			pModel->nMaterials = m_nMaterials;
			if ( m_nMaterials > 0 )
			{
				pModel->pMaterials = new AccessObj::COBJmaterial[pModel->nMaterials];
				for ( unsigned int imat=0; imat<pModel->nMaterials; ++imat )
				{
					CMaterial * pGeoMat = &m_pMaterials[imat];
					AccessObj::COBJmaterial * pObjMat = &pModel->pMaterials[imat];
					ASSERT( pGeoMat && pObjMat );
					sprintf( pObjMat->name, "%s", pGeoMat->m_sName );
					memcpy( pObjMat->diffuse, pGeoMat->m_fDiffuse, sizeof(float)*4 );
					memcpy( pObjMat->ambient, pGeoMat->m_fAmbient, sizeof(float)*4 );
					memcpy( pObjMat->specular, pGeoMat->m_fSpecular, sizeof(float)*4 );
					memcpy( pObjMat->emissive, pGeoMat->m_fEmissive, sizeof(float)*4 );
					memcpy( pObjMat->shininess, pGeoMat->m_fShininess, sizeof(float)*1 );
					if ( pGeoMat->m_bTextured )
					{
						pObjMat->bTextured = pGeoMat->m_bTextured;
						sprintf( pObjMat->sTexture, "%s", pGeoMat->m_sTexture );
					}
				}
			}

		}

		//////////////////////////////////////////////////////////////
		//	Step.6	Output OBJ File
		int mode = OBJ_TEXTURE;
		if ( m_bBindNormal ) mode |= OBJ_SMOOTH;
		if ( m_nMaterials > 0 ) mode |= OBJ_MATERIAL;

		m_AccObj.WriteOBJ (sFileName.GetBuffer(200),mode);			

		////////////////////////////////////////////////////////////////////
		return TRUE;
	}

}
