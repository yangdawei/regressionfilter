// GeoObject.h: interface for the CGeoObject class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_GEOOBJECT_H__4B65F9C8_306F_44FF_8579_A97E296955BF__INCLUDED_)
#define AFX_GEOOBJECT_H__4B65F9C8_306F_44FF_8579_A97E296955BF__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000


#include "AccessObj.h"
#include "GraphMath.h"
//#include "../glExtension.h"



namespace GeoObj
{
	using namespace AccessObj;
	using namespace GraphMath;

	// Drawing mode 
#define DRAW_POINTS_SCENE			0x0001
#define DRAW_WIREFRAME_TRIANGLES	0x0002
#define DRAW_UNTEXTURED_TRIANGLES	0x0004
#define DRAW_TEXTURED_TRIANGLES		0x0008

	// Rendering modified styles
#define SHOW_WIRE_FRAME				0x0100
#define SHOW_COORDINATE				0x0200
#define SHOW_LIGHTPOSITION			0x0400 
#define LIGHT_ON_WHEN_RENDERING		0x0800

	class CGeoObject  
	{
		/////////////////////////////////////////////////////////////////////
		//	Geometry Element Definition
		/////////////////////////////////////////////////////////////////////
	public:

		// --------------------------------------------------------------------
		// Material
		class CMaterial
		{
		public:
			char  m_sName[256];			// name of material 
			float m_fDiffuse[4];		// diffuse component 
			float m_fAmbient[4];		// ambient component 
			float m_fSpecular[4];		// specular component 
			float m_fEmissive[4];		// emissive component 
			float m_fShininess[1];		// specular exponent 

			// Texture Data
			GLuint	m_nTexId;
			bool m_bTextured;
			char m_sTexture[256];	// name of texture file
			int m_nWidth, m_nHeight;
			unsigned char *m_pTexImage;


			char  * MaterialName()       { return (m_sName);      }
			float * DiffuseComponent()   { return (m_fDiffuse);   }
			float * AmbientComponent()   { return (m_fAmbient);   }
			float * SpecularComponent()  { return (m_fSpecular);  }
			float * EmissiveComponent()  { return (m_fEmissive);  }
			float * ShininessComponent() { return (m_fShininess); }

			CMaterial()
			{
				sprintf (m_sName, "default");
				m_fDiffuse[0]   = m_fDiffuse[1]  = m_fDiffuse[2]  = m_fDiffuse[3]  = 1.0f;
				m_fAmbient[0]   = m_fAmbient[1]  = m_fAmbient[2]  = m_fAmbient[3]  = 0.1f;
				m_fSpecular[0]  = m_fSpecular[1] = m_fSpecular[2] = m_fSpecular[3] = 0.0f;
				m_fEmissive[0]  = m_fEmissive[1] = m_fEmissive[2] = m_fEmissive[3] = 0.0f;
				m_fShininess[0] = 10;

				m_nTexId = 0;
				m_bTextured = false;
				memset( m_sTexture, 0, sizeof(char)*256 );
				m_nWidth = m_nHeight = 0;
				m_pTexImage = 0;
			}

			virtual ~CMaterial()
			{
				if ( m_pTexImage )	delete [] m_pTexImage;
				m_pTexImage = 0;
			}

			void SetMaterial( void )
			{
				glMaterialfv( GL_FRONT, GL_DIFFUSE, m_fDiffuse );
				glMaterialfv( GL_FRONT, GL_AMBIENT, m_fAmbient );
				glMaterialfv( GL_FRONT, GL_SPECULAR, m_fSpecular );
				glMaterialfv( GL_FRONT, GL_EMISSION, m_fEmissive );
				glMaterialfv( GL_FRONT, GL_SHININESS, m_fShininess );
			}

			bool LoadTexture( void ) 
			{
				if ( !m_bTextured )	return false;
				if ( glIsTexture(m_nTexId) )
					glDeleteTextures(1, &m_nTexId);
				glGenTextures(1,&m_nTexId);

				glEnable( GL_TEXTURE_2D );
				glBindTexture(GL_TEXTURE_2D,m_nTexId);
				glPixelStorei(GL_UNPACK_ALIGNMENT, 1);
				glTexEnvf(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_MODULATE);
				glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
				glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
				glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
				glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
				glTexImage2D(GL_TEXTURE_2D, 0, 3, m_nWidth, m_nHeight, 0, 
					GL_RGB, GL_UNSIGNED_BYTE, m_pTexImage);
				//if ( glGetError() != GL_NO_ERROR )
				//	AfxMessageBox("CGeoObject::CMaterial::LoadTexture Error");
			}

			bool BindTexture( void )
			{
				if ( !m_bTextured )	return false;
				if ( !glIsTexture(m_nTexId) )	return false;
//				glActiveTextureARB(GL_TEXTURE0_ARB);				
				glEnable( GL_TEXTURE_2D );
				glBindTexture(GL_TEXTURE_2D,m_nTexId);				
			}

			bool UnBindTexture( void )
			{
				if ( !m_bTextured )	return false;
				if ( !glIsTexture(m_nTexId) )	return false;
//				glActiveTextureARB(GL_TEXTURE0_ARB);								
				glDisable( GL_TEXTURE_2D );
			}

		};// ------------------------------------------------------------------

		// --------------------------------------------------------------------
		// vertex information class
		
		class CVertex
		{
		public:
			vec3f	m_vPosition;			// world coordinate of the vertex
			vec3f	m_vTexture;

			vec3f	m_vS,	m_vT,	m_vN;	//	Local framework
			
			float	m_fScale;				// Texture scale

			int		m_nTriangle;			// number of triangles that share this vertex
			int *	m_pTriangleIndices;		// link of triangles that share this vertex


			int		m_nNeighbor;			// neighbor vertices/Edge number
			int *	m_npVertexIndices;		// indices of neighbor vertices
			int *	m_npEdgeIndices;		// indices of neighbor edges
			float *	m_fpEdgeLengthes;		// Lengthes of edge connected to this vertex		

			BYTE *	m_pCustomAttrib;		// customize byte array
											//	m_pCustomAttrib = new BYTE[ sizeof (CVertexCustom) ]


		public:
			/////////////////////////////////////////////////////////////////
			// construction and destruction
			/////////////////////////////////////////////////////////////////
			CVertex()
			{
				m_vPosition =	vec3f(0, 0, 0);
				m_vTexture	=	vec3f(0, 0, 0);			

				m_vS		=	vec3f(0, 0, 0);
				m_vT		=	vec3f(0, 0, 0);
				m_vN		=	vec3f(0, 0, 0);



				m_fScale	       = 0;

				m_nTriangle        = 0;
				m_pTriangleIndices = NULL;

				m_nNeighbor        = 0;
				m_npVertexIndices  = NULL;
				m_npEdgeIndices	   = NULL;
				m_fpEdgeLengthes   = NULL;		

				m_pCustomAttrib	   = NULL;

			}
			virtual ~CVertex()
			{
				if ( 0 != m_nTriangle )
				{
					ASSERT( m_pTriangleIndices );
					delete [] m_pTriangleIndices;
					m_pTriangleIndices = NULL;
				}

				if ( 0 != m_nNeighbor )
				{
					ASSERT( m_npVertexIndices 
						&&  m_npEdgeIndices
						&&  m_fpEdgeLengthes );
					delete [] m_npVertexIndices;
					delete [] m_npEdgeIndices;
					delete [] m_fpEdgeLengthes;
					m_npVertexIndices  = NULL;
					m_npEdgeIndices    = NULL;
					m_fpEdgeLengthes   = NULL;
				}

				if ( NULL != m_pCustomAttrib ) 
				{
					delete m_pCustomAttrib;
					m_pCustomAttrib    = NULL;
				}

			}	
		};// ------------------------------------------------------------------

		// --------------------------------------------------------------------
		// Edge information class
		class CEdge
		{
		public:
			int	m_nTwinEdge;				// the twin edge
			int	m_nVertexIndices[2];		// the two vertices that define this edge
			int	m_nTriangleIndices[2];		// the two triangles that share this edge
			int	m_nFlag;					// flag

			//
			// construction and destruction
			//
			CEdge()	
			{
				m_nTwinEdge = -1;
				m_nVertexIndices[0]   = m_nVertexIndices[1]   = -1; 
				m_nTriangleIndices[0] = m_nTriangleIndices[1] = -1;
			}
			virtual ~CEdge() {}
		};// ------------------------------------------------------------------

		// --------------------------------------------------------------------
		// Triangle information class
		class CTriangle
		{
		public:
			int		m_nVertexIndices[3];	// indices of the three vertices in vertex link
			vec3f	m_vTexture[3];			// texture coordinates of the three vertices
			vec3f	m_vS, m_vT, m_vN;		// local frame
			vec3f	m_vCenter;				// center of this triangle

			float	m_fScale;				// texture scale	

			int		m_nEdgeIndices[3];		// indices of the three edges of this triangle
			int		m_nGroupIndex;			// group indices

			void *	m_pCustomAttrib;
			

		public:		//Data for N-patch calculation
			float	m_fGeoArea, m_fTexArea;	//Triangle represent area in geometry space and texture space


		public:

			//
			// construction and destruction
			//
			CTriangle()
			{
				m_nVertexIndices[0] = m_nVertexIndices[1] = m_nVertexIndices[2] = -1;
				m_nEdgeIndices[0]   = m_nEdgeIndices[1]   = m_nEdgeIndices[2]   = -1;
				m_vTexture[0]       = m_vTexture[1]       = m_vTexture[2]       = vec3f(0, 0, 0);
				m_vS = m_vT = m_vN  = m_vCenter = vec3f(0, 0, 0);
				m_fScale            =  1.f;
				m_nGroupIndex       = -1;

				m_pCustomAttrib     = NULL;
			
			}
			virtual ~CTriangle() 
			{
				if ( NULL != m_pCustomAttrib )
					delete m_pCustomAttrib;
				m_pCustomAttrib = NULL;
			}
		};// ------------------------------------------------------------------



		// --------------------------------------------------------------------
		// Group information class
		class CGroup
		{
		public:
			char	m_sName[256];			// name of this group 
			int		m_nMaterialIndex;			// material index
			int		m_nTriangles;				// triangle number of this group
			int *	m_pTirangleIndices;			// indices link of triangles that belong to this group
			class CGroup *m_pLeft, *m_pRight;	// pointer to the two sub-trees

			int		m_nMatNum;
			int	*	m_pnMatIdx;
			int	*	m_pnTriIdx;

			//
			// construction and destruction
			//
			CGroup()
			{
				memset( m_sName, 0, sizeof(char)*256 );
				m_nTriangles       =  0;
				m_nMaterialIndex   = -1;
				m_pTirangleIndices = NULL;
				m_pLeft = m_pRight = NULL;

				m_nMatNum = 0;
				m_pnMatIdx = NULL;
				m_pnTriIdx = NULL;
			}

			virtual ~CGroup()
			{
				if ( m_pTirangleIndices ) 	delete [] m_pTirangleIndices;
				if ( m_pnMatIdx )	delete [] m_pnMatIdx;
				if ( m_pnTriIdx )	delete [] m_pnTriIdx;

				m_pTirangleIndices = NULL;
				m_nTriangles = 0;
				m_pnMatIdx = NULL;
				m_pnTriIdx = NULL;
				m_nMatNum = 0;
			}
		};// ------------------------------------------------------------------

	public:
		CGeoObject();
		virtual ~CGeoObject();


		/////////////////////////////////////////////////////////////////////
		//	Geometry Variables
		/////////////////////////////////////////////////////////////////////
	public:
		int			m_nMaterials;		//	number materials
		CMaterial *	m_pMaterials;		//	materials link

		int			m_nVertices;		//	number of vertices
		CVertex *	m_pVertices;		//	vertices link

		int			m_nEdges;			//	number of edges
		CEdge *		m_pEdges;			//	edges link

		int			m_nTriangles;		//	number of triangles
		CTriangle *	m_pTriangles;		//	triangles link


		int			m_nGroups;			//	group number
		CGroup *	m_pGroups;			//	root of the group tree	

		//	Bounding value
		vec3f		m_vMax, m_vMin;		//	bounding box of the scene
		vec3f		m_vCenter;			//	Center of model
		//	Bounding sphere
		vec3f		m_sphereCenter;		//sphere center
		float		m_sphereRadius;		//sphere radius

		float		m_fScale;			//	Average Texture Scale


		//	File Attributes
		bool		m_bBindNormal;		//	Mark of whether the Obj is bind with normal information
		bool		m_bBindTexture;		//	Mark of whether the Obj is bind with texture information
		CString		m_sFileName;		//	Model Filename
		char		m_sMtlFile[256];	// 
		


	public:
		void	BoundingBox( vec3f &vMax, vec3f &vMin ) const
				{
					vMax = m_vMax;
					vMin = m_vMin;
				}

		vec3f	Center(void) const { return m_vCenter; }

		// access function
		int		GetMaterialNum(void) const { return m_nMaterials; }
		int		GetVertexNum(void)   const { return m_nVertices; }
		int		GetEdgeNum(void)     const { return m_nEdges; }
		int		GetTriangleNum(void) const { return m_nTriangles; }
		int		GetGroupNum(void)	 const { return m_nGroups; }

		CMaterial * GetMateriaPtr(void)  const { return m_pMaterials; }
		inline CVertex *   GetVertexPtr(void)   const { return m_pVertices; }
		CEdge *     GetEdgePrt(void)     const { return m_pEdges; }
		inline CTriangle * GetTrianglePtr(void) const { return m_pTriangles; }
		CGroup *    GetGroupPtr(void)    const { return m_pGroups; }

	protected:
		bool	m_bIsInitial;
		bool	m_bTextureLoaded;

		bool	IsTextureLoaded( void ) const { return m_bTextureLoaded; }
		void	LoadTexture( void ) 
				{
					if ( m_nMaterials!=0 )
					{
						ASSERT( m_pMaterials );
						for ( int i=0; i<m_nMaterials; ++i )
							m_pMaterials[i].LoadTexture();
					}
					m_bTextureLoaded = true;
				}

	public:
		bool	IsInit() {return m_bIsInitial;}
		void 	ClearData();


		/////////////////////////////////////////////////////////////////////
		//	Geometry Building Functions
		/////////////////////////////////////////////////////////////////////
	protected:

		//	Build Topology
		void	SearchVerticesNeighbor();						//	Search vertices neighbors and calc the edge lengths
		void	BuildEdgeLink();								//	Build the edges
		void	BuildNeighborsEdgeLink();						//	Build m_npEdgeIndices

		//	Convert a ObjModel 
		void	ConvertData(COBJmodel* pObjModel);
		void	TransferNormalTexture(COBJmodel* pObjModel);	//	Transfer the Normal and texture coordinate value in model


		void	ComputeNormal();								//	Compute the normals of vertices and triangles	
		void	ComputeBoundingbox(bool bSkipExtraVtx = true);	//	Compute the bounding box
		void	ComputeBoundingSphere();						//	Compute the bounding sphere
		void	ComputeTexScale();								//	Compute the texture scale
		void	ComputeTexOrientation();						//	Compute the texture orientation
    public:
		void	ScaleObjectToUnify();



		/////////////////////////////////////////////////////////////////////
		//	Draw Model in different style
		/////////////////////////////////////////////////////////////////////	
	protected:	
		void	DrawSceneByPoints();
		void	DrawSceneByLineWire();
		void	DrawSceneByUntexturedTriangles();	
		void	DrawSceneByTexturedTriangles();

		void	DrawWireFrame();	

		void	SetMaterial( float fR, float fG, float fB );

		/////////////////////////////////////////////////////////////////////
		//	Interface Functions
		/////////////////////////////////////////////////////////////////////	
	public:
		void	DrawScene(int nRenderingMode, vec3f vColor = vec3f(0.8f,0.8f,0.8f));

		BOOL	LoadOBJModel(CString	sFileName);
		BOOL	SaveOBJModel(CString	sFileName);	
	};

}
#endif // !defined(AFX_GEOOBJECT_H__4B65F9C8_306F_44FF_8579_A97E296955BF__INCLUDED_)

