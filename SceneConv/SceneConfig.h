#pragma once

#include <iostream>
#include <string>
using namespace std;

class CSceneConfig
{
public:
	void loadConfigFile(const char *fileName);
	void saveConfigFile(const char *fileName);
private:
	void stringToArray(string &str, float *array, int count);
	string arrayToString(float *array, int count);
};
