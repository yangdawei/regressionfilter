#include "stdafx.h"
#include "SceneWnd.h"
#include "mesh_proc.h"
#include "Old_Obj\GeoObject.h"
#include <boost/function.hpp>
#include <boost/bind.hpp>
#include <GL/glut.h>

#include "XMLDoc.h"

#include <sstream>
#include "GL\glext.h"
#include "1.0\wglh\wglfbo.h"

using namespace std;
using namespace tixml;

#define WINDOW_SIZE     (512)
#define WINDOW_NEAR     (1.0)
#define WINDOW_FAR      (100.0)

#define SHADOW_SIZE     (256)
#define SHADOW_NEAR     (1.0)
#define SHADOW_FAR      (100.0)

CSceneWnd::CSceneWnd() : CArcballWndEx(), m_fs_shader(".\\shader\\shadow.cxx"), m_vs_shader(".\\shader\\shadow.hxx") //, m_shader_project(NULL)
{
	m_kd = 0.0;
	m_ks = 0.0;
	m_hx = 0.0;
	m_hy = 0.0;
	m_hz = 0.0;
	m_lambda = 0.0;

	this->_DepthMin = 0.01;
	//this->_DepthMax = 10.0f;
}

string CSceneWnd::ShowFileDialog(LPSTR filter)
{
	char fileName[100];
	OPENFILENAME  ofn;
	ZeroMemory(&ofn, sizeof(ofn));
	ofn.lStructSize = sizeof(ofn);
	ofn.hwndOwner = NULL;
	ofn.lpstrFile = fileName;
	ofn.lpstrFile[0] = '\0';
	ofn.nMaxFile = sizeof(fileName);
	ofn.lpstrFilter = filter;
	ofn.nFilterIndex = 0;
	ofn.lpstrFileTitle = NULL;
	ofn.nMaxFileTitle = 0;
	ofn.lpstrInitialDir = NULL;
	ofn.Flags = OFN_PATHMUSTEXIST | OFN_FILEMUSTEXIST;
	if (GetOpenFileName(&ofn))
		return string(fileName);
	return "";
}

void CSceneWnd::AddObject(string fileName)
{
	object.push_back(new wglh::CwglMesh);
	ImportWavefrontObjFile(fileName.c_str(), *object.back());
	/*add*/
	vector<num::Vec3f> vec;
	triangle_list.push_back( vec ); 
	getTriangleList( fileName.c_str(), triangle_list[triangle_list.size()-1] );
	
	object_name.push_back("OBJECT");
	float scale = 1.0;
	object_scale.push_back(scale);
	num::Vec3f translation(0.0, 0.0, 0.0);
	object_translation.push_back(translation);
	num::Mat4x4f rotation;
	rotation.SetIdentity();
	object_rotation.push_back(rotation);
	object_brdf.push_back(BRDF(1.0, 1.0, num::Vec3f(0.0, 0.0, 0.0), 1.0));
	num::Vec3f color(1.0, 0.0, 0.0);
	object_color.push_back(color);
	m_combo->setData(object_name);
}

void CSceneWnd::AddLight(string fileName)
{
	FILE *file = fopen(fileName.c_str(), "r");
	wglh::CwglSpotLight newlight;
	spotlight.clear();
	while (fscanf(file, "%f%f%f%f%f%f", &newlight.translation.x, &newlight.translation.y, &newlight.translation.z,
		&newlight.color.x, &newlight.color.y, &newlight.color.z) == 6)
	{
		spotlightColor[spotlight.size()] = newlight.color;
		spotlightPos[spotlight.size()] = newlight.translation;
		spotlight.push_back(newlight);
		if (spotlight.size() == 10)
			break;
	}
	if (!spotlight.empty())
		OnLightChange(0);
	fclose(file);
}

void CSceneWnd::OnAddObject()
{
	string fileName = ShowFileDialog("WaveFront Object File(*.obj)\0*.obj\0");
	if (fileName.length() != 0)
		AddObject(fileName);
}

void CSceneWnd::OnAddLight()
{
	//!TODO: add light
	string fileName = ShowFileDialog("Text file(*.txt)\0*.txt\0");
	if (fileName.length() != 0)
		AddLight(fileName);
}

void CSceneWnd::OnRemoveObject()
{
	object.erase(object.begin() + current_object_id);
	object_name.erase(object_name.begin() + current_object_id);
	object_scale.erase(object_scale.begin() + current_object_id);
	object_translation.erase(object_translation.begin() + current_object_id);
	object_rotation.erase(object_rotation.begin() + current_object_id);
	object_brdf.erase(object_brdf.begin() + current_object_id);
	object_color.erase(object_color.begin() + current_object_id);
	m_combo->setData(object_name);
	OnSelectionChange(0);
}

void CSceneWnd::OnSaveConfigFile()
{
	string fileName = ShowFileDialog("Scene Config File\0*.xml\0");
	if (fileName.length() != 0)
	{
		printf("Save: %s\n", fileName.c_str());
	}
}

void CSceneWnd::OnLoadConfigFile()
{
	string fileName = ShowFileDialog("Scene Config File\0*.xml\0");
	if (fileName.length() != 0)
	{
		printf("Loading Scene Config File: %s\n", fileName.c_str());
		loadConfigFile(fileName.c_str());
	}
}

void CSceneWnd::OnSelectionChange(int selection)
{
	current_object_id = selection;
	m_transformCtrl.SetScale(object_scale[current_object_id]);
	m_transformCtrl.GetTranslation_Ref() = object_translation[current_object_id];
	m_transformCtrl.SetRotationMat(GetRotationToScene(), object_rotation[current_object_id]);
	m_kd = object_brdf[current_object_id].kd;
	m_ks = object_brdf[current_object_id].ks;
	m_hx = object_brdf[current_object_id].h[0];
	m_hy = object_brdf[current_object_id].h[1];
	m_hz = object_brdf[current_object_id].h[2];
	m_lambda = object_brdf[current_object_id].lambda;
#ifndef USE_SPOT_LIGHT
	m_seek_brdf_kd->setValue(m_kd);
	m_seek_brdf_ks->setValue(m_ks);
	m_seek_brdf_hx->setValue(m_hx);
	m_seek_brdf_hy->setValue(m_hy);
	m_seek_brdf_hz->setValue(m_hz);
	m_seek_brdf_lambda->setValue(m_lambda);
#endif
}

num::Vec3f CSceneWnd::getLightDirection(int lightIndex)
{
	return GetRotationToEnviroment().Product(light_srbf[lightIndex].center);
}

num::Vec3f CSceneWnd::getViewPosition()
{
	num::Mat4x4f ViewRotationMatrix;
	GetRotationToScene().Inverse(ViewRotationMatrix);
	return ViewRotationMatrix.Product(ViewTransformation.translation);
}

void CSceneWnd::OnRender()
{
	preRender();

#ifndef USE_SPOT_LIGHT
	glClearColor(0.2f, 0.4f, 0.6f, 1.0f);
#else
	glClearColor(0.0f, 0.0f, 0.0f, 0.0f);
#endif
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	num::Mat4x4f lightTransformMatrix, viewTransformMatrix;
#ifndef USE_SPOT_LIGHT
	OnRenderEnvironment();
#endif // !USE_SPOT_LIGHT

	glEnable(GL_DEPTH_TEST);

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glTranslatef(0,0,-m_SceneZoom);
	m_SceneArcBall.Apply();

	FOR_DBG
	renderWithShadow();
	//renderWithoutShadow();

#ifdef USE_SPOT_LIGHT
	OnRenderLight();
#endif

	glFinish();

	//draw sprites
	if(m_sprites){
		m_sprites->draw();
	}

	SwapBuffers(wglGetCurrentDC());
}
float lightLambda = 500.0f;
float s = 170.0f;
void CSceneWnd::OnRenderObject()
{
	/*add here*/
	FOR_DBG
	m_shTex.Enable();
	_CheckErrorGL
	wglh::ext::CwglMultiTextureEnv::ActiveTextureUnit(0);
	m_shTex.Apply();

	m_shader_program.Apply();
#ifndef	USE_SPOT_LIGHT
	m_shader_program.SetUniformArray3f("lightDirection", &getLightDirection(0)[0], 1);
	m_shader_program.SetUniformArray3f("lightColor", &light_srbf[0].color[0], 1);
#endif
	m_shader_program.SetUniformArray3f("viewPosition", &getViewPosition()[0], 1);

	for (int i = 0; i < object.size(); i++)
	{
		glPushMatrix();
		if (current_object_id == i)
		{
			m_transformCtrl.prepareDrag();
			m_transformCtrl.ApplyFull(GetRotationToScene());
			object_scale[i] = m_transformCtrl.GetScale();
			object_translation[i] = m_transformCtrl.GetTranslation_Ref();
			object_rotation[i] = m_transformCtrl.GetRotationMatrix(GetRotationToScene());
		} 
		else
		{
			glTranslatef(object_translation[i][0], object_translation[i][1], object_translation[i][2]);
			glScalef(object_scale[i], object_scale[i], object_scale[i]);
			glMultMatrixf(object_rotation[i]);
		}

		int other, start;
		if( i == 0 ) {
			other = 1;
			start = triangle_list[0].size() / 3;
		}
		else {
			other = 0;
			start = 0;
		}
		
		//////////////////////////////////////////////////////////////////////////
		//If There is only one object, 'other' will be 1, and the size of 'object'
		//will be 1, and exception raised
		//////////////////////////////////////////////////////////////////////////

		m_shader_program.SetUniformArray3f("pointColor", &object_color[i], 1);	
		m_shader_program.SetUniformMatrix_4x4("objectRotation", object_rotation[i]);
		m_shader_program.SetUniformArray3f("objectTranslation", &object_translation[i][0], 1);
		m_shader_program.SetUniform("objectScale", object_scale[i]);
		/*add*/
		m_shader_program.SetUniformArray3f("texColor", &object_color[other], 1);	
		m_shader_program.SetUniformMatrix_4x4("texRotation", object_rotation[other]);
		m_shader_program.SetUniformArray3f("texTranslation", &object_translation[other][0], 1);
		m_shader_program.SetUniform("texScale", object_scale[other]);
		
		object[i]->Render(0);
		glPopMatrix();
	}

	m_shader_program.DisableShaders();
	/*add*/
	wglh::ext::CwglMultiTextureEnv::ActiveTextureUnit(0);
	m_shTex.Disable();
	if(m_InteractiveMode == 0){
		glPushMatrix();
		m_transformCtrl.ApplyTranslate();
		m_transformCtrl.DrawCtrl();
		glPopMatrix();
	}
}

#ifdef USE_SPOT_LIGHT
void CSceneWnd::OnRenderLight()
{
	for (int i = 0; i < spotlight.size(); i++)
	{
		glPushMatrix();
		if (current_light_id == i)
		{
			m_lightCtrl.prepareDrag();
			spotlightPos[i] = spotlight[i].translation = m_lightCtrl.GetTranslation_Ref();
		} 
		glTranslatef(spotlight[i].translation.x, spotlight[i].translation.y, spotlight[i].translation.z);
		spotlight[i].render();
		glPopMatrix();
	}
	if (m_InteractiveMode == IMC_ENVIROMENT)
	{
		glPushMatrix();
		const num::Vec3f &trans = m_lightCtrl.GetTranslation_Ref();
		glTranslatef(trans.x, trans.y, trans.z);
		m_lightCtrl.DrawCtrl();
		glPopMatrix();
	}
}
#endif;

void CSceneWnd::OnUpdateBRDF()
{
#ifndef USE_SPOT_LIGHT
	object_brdf[current_object_id].kd = m_seek_brdf_kd->getValue();
	object_brdf[current_object_id].ks = m_seek_brdf_ks->getValue();
	object_brdf[current_object_id].h[0] = m_seek_brdf_hx->getValue();
	object_brdf[current_object_id].h[1] = m_seek_brdf_hy->getValue();
	object_brdf[current_object_id].h[2] = m_seek_brdf_hz->getValue();
	object_brdf[current_object_id].lambda = m_seek_brdf_lambda->getValue();
#endif
}

void CSceneWnd::OnInitScene()
{
	m_SceneZoom = 5.0f;
	m_cubeMapEnv.Load_PFM("grace.pfm");
	m_cubeMapEnv.InitForRendering();
	_SafeDel(m_sprites);
	using namespace jgl;

#ifndef USE_SPOT_LIGHT
	JSpriteContainer::setResourcePath(".\\raw\\");
	m_sprites = new JSpriteContainer(10,10,250,220,true);

	m_seek_brdf_kd = new JGLSeekBar(&m_kd, 0, 1, 90, 140, "Kd:");
	m_seek_brdf_kd->addListener(boost::bind(&CSceneWnd::OnUpdateBRDF,this));
	m_sprites->addSprite(m_seek_brdf_kd);

	m_seek_brdf_ks = new JGLSeekBar(&m_ks, 0, 1, 90, 120, "Ks:");
	m_seek_brdf_ks->addListener(boost::bind(&CSceneWnd::OnUpdateBRDF,this));
	m_sprites->addSprite(m_seek_brdf_ks);

	m_seek_brdf_hx = new JGLSeekBar(&m_hx, 0, 100, 90, 100, "Hx:");
	m_seek_brdf_hx->addListener(boost::bind(&CSceneWnd::OnUpdateBRDF,this));
	m_sprites->addSprite(m_seek_brdf_hx);

	m_seek_brdf_hy = new JGLSeekBar(&m_hy, 0, 100, 90, 80, "Hy:");
	m_seek_brdf_hy->addListener(boost::bind(&CSceneWnd::OnUpdateBRDF,this));
	m_sprites->addSprite(m_seek_brdf_hy);

	m_seek_brdf_hz = new JGLSeekBar(&m_hz, 0, 100, 90, 60, "Hz:");
	m_seek_brdf_hz->addListener(boost::bind(&CSceneWnd::OnUpdateBRDF,this));
	m_sprites->addSprite(m_seek_brdf_hz);

	m_seek_brdf_lambda = new JGLSeekBar(&m_lambda, 0, 500, 90, 40, "Lambda:");
	m_seek_brdf_lambda->addListener(boost::bind(&CSceneWnd::OnUpdateBRDF,this));
	m_sprites->addSprite(m_seek_brdf_lambda);

	m_button_save_scene = new JGLButton(10, 10, 50, 20, "SAVE");
	m_button_save_scene->addClickListener(boost::bind(&CSceneWnd::OnSaveConfigFile, this));
	m_sprites->addSprite(m_button_save_scene);
	m_button_load_scene = new JGLButton(80, 10, 50, 20, "LOAD");
	m_button_load_scene->addClickListener(boost::bind(&CSceneWnd::OnLoadConfigFile, this));
	m_sprites->addSprite(m_button_load_scene);

	m_button_add_object = new JGLButton(130, 190, 20, 20, "+");
	m_button_add_object->addClickListener(boost::bind(&CSceneWnd::OnAddObject, this));
	m_sprites->addSprite(m_button_add_object);
	m_button_remove_object = new JGLButton(170, 190, 20, 20, "-");
	m_button_remove_object->addClickListener(boost::bind(&CSceneWnd::OnRemoveObject, this));
	m_sprites->addSprite(m_button_remove_object);

	m_sprites->addSprite(new JGLLabel(10, 170, "BRDF Parameters:"));
	m_combo = new JGLCombo(10, 190, 100, 20, object_name);
	m_combo->addSelectionChangedListener(boost::bind(&CSceneWnd::OnSelectionChange, this, _1));
	m_sprites->addSprite(m_combo);
#endif	

	m_shader_program = m_fs_shader + m_vs_shader;
	m_shader_project = m_shader_program;
	m_shader_project.UpdateShaderList();
	

	loadConfigFile("DefaultSceneConfig.xml");
	CreateTexture();

#ifdef USE_SPOT_LIGHT
	AddLight("DefaultLight.txt");
	initShadowMap();
#endif
}

void CSceneWnd::stringToArray(string &str, float *array, int count)
{
	stringstream ss(str);
	char temp;
	for (int i = 0; i < count; i++)
	{
		ss >> temp;
		ss >> array[i];
	}
}

void CSceneWnd::stringToVec3f(string &str, num::Vec3f &vec)
{
	stringstream ss(str);
	char temp;
	for (int i = 0; i < 3; i++)
	{
		ss >> temp;
		ss >> vec[i];
	}
}

void CSceneWnd::stringToMat4x4f(string &str, num::Mat4x4f &mat)
{
	stringToArray(str, &mat.m[0][0], 16);
}

string CSceneWnd::arrayToString(float *array, int count)
{
	stringstream ss;
	ss << '(' << array[0];
	for (int i = 1; i < count; i++)
	{
		ss << "," << array[i];
		if (i == count - 1)
			ss << ')';
	}
	return ss.str();
}

void CSceneWnd::loadConfigFile(const char *fileName)
{
	object.clear();
	object_name.clear();
	object_brdf.clear();
	object_color.clear();
	object_rotation.clear();
	object_scale.clear();
	object_translation.clear();
	light_srbf.clear();
	tixml::XMLDoc doc;
	doc.Load(fileName);
	// Read Global Setting
	ViewTransformation.scale = doc.get<float>("GlobalSetting.ViewTransformation.@Scale", 0);
	stringToVec3f(doc.get<string>("GlobalSetting.ViewTransformation.@Translation", 0), ViewTransformation.translation);
	stringToMat4x4f(doc.get<string>("GlobalSetting.ViewTransformation.@Rotation", 0), ViewTransformation.rotation);
	LightTransformation.scale = doc.get<float>("GlobalSetting.LightTransformation.@Scale", 0);
	stringToVec3f(doc.get<string>("GlobalSetting.LightTransformation.@Translation", 0), LightTransformation.translation);
	stringToMat4x4f(doc.get<string>("GlobalSetting.LightTransformation.@Rotation", 0), LightTransformation.rotation);

	// Read Objects
	int obj_num = doc.get<int>("ObjectList.@Count", 0);
	for (int i = 0; i < obj_num; i++)
	{
		string obj_name, obj_filename;
		float obj_scale;
		num::Vec3f obj_translation, obj_color, srbf_center, brdf_h;
		num::Mat4x4f obj_rotation;
		// Object Name
		obj_name = doc.get<string>("ObjectList.Object.@Name", i);
		object_name.push_back(obj_name.c_str());
		// File Name
		obj_filename = doc.get<string>("ObjectList.Object.SourceFile.@Name", i);
		object.push_back(new wglh::CwglMesh);
		ImportWavefrontObjFile(obj_filename.c_str(), *object.back());
		/*add*/
		vector<num::Vec3f> vec;
		triangle_list.push_back( vec ); 
		getTriangleList( obj_filename.c_str(), triangle_list[triangle_list.size()-1] );

		// Scale
		obj_scale = doc.get<float>("ObjectList.Object.Transformation.@Scale", i);
		object_scale.push_back(obj_scale);
		// Translation
		stringToVec3f(doc.get<string>("ObjectList.Object.Transformation.@Translation", i), obj_translation);
		object_translation.push_back(obj_translation);
		// Rotation
		stringToMat4x4f(doc.get<string>("ObjectList.Object.Transformation.@Rotation", i), obj_rotation);
		object_rotation.push_back(obj_rotation);
		// Color
		stringToVec3f(doc.get<string>("ObjectList.Object.Color.@Value", i), obj_color);
		object_color.push_back(obj_color);
		// BRDF
		m_kd = doc.get<float>("ObjectList.Object.BRDF.@Kd", i);
		m_ks = doc.get<float>("ObjectList.Object.BRDF.@Ks", i);
		stringToVec3f(doc.get<string>("ObjectList.Object.BRDF.@H", i), brdf_h);
		m_hx = brdf_h[0];
		m_hy = brdf_h[1];
		m_hz = brdf_h[2];
		m_lambda = doc.get<float>("ObjectList.Object.BRDF.@Lambda", i);
		object_brdf.push_back(BRDF(m_kd, m_ks, brdf_h, m_lambda));
	}

	// Read Lights
	int light_num = doc.get<int>("LightList.@Count", 0);
	for (int i = 0; i < light_num; i++)
	{
		SRBF srbf;
		stringToVec3f(doc.get<string>("LightList.SRBF.@Center", i), srbf.center);
		srbf.radius = doc.get<float>("LightList.SRBF.@Radius", i);
		srbf.intensity = doc.get<float>("LightList.SRBF.@Intensity", i);
		stringToVec3f(doc.get<string>("LightList.SRBF.@Color", i), srbf.color);
		light_srbf.push_back(srbf);
	}
#ifndef USE_SPOT_LIGHT
	m_combo->setData(object_name);
#endif

	OnSelectionChange(0);
}

void CSceneWnd::saveConfigFile(const char *fileName)
{
	tixml::XMLDoc doc;
	doc.Create("SceneConfig");

	tixml::XMLNode * pNode1 = new tixml::XMLNode("GlobalSetting","");
	tixml::XMLNode * pNode1Child1 = new tixml::XMLNode("ViewTransformation","");
	tixml::XMLNode * pNode1Child2 = new tixml::XMLNode("LightTransformation","");
	tixml::XMLNode * pNode1Child3 = new tixml::XMLNode("CubeMap","");
	pNode1Child1->setAttrib<float>("Scale", 1.0);
	pNode1Child1->setAttrib<string>("Translation", "(xxx)");
	pNode1Child1->setAttrib<string>("Rotation", "(XXXX)");
	pNode1->children().push_back(pNode1Child1);
	pNode1->children().push_back(pNode1Child2);
	pNode1->children().push_back(pNode1Child3);

	tixml::XMLNode * pNode2 = new tixml::XMLNode("ObjectList","");

	tixml::XMLNode * pNode3 = new tixml::XMLNode("LightList","");

	doc.getRoot()->children().push_back(pNode1);
	doc.getRoot()->children().push_back(pNode2);
	doc.getRoot()->children().push_back(pNode3);

	doc.Save(fileName);
}

bool CSceneWnd::handleKeyboard(unsigned char c){
	switch (c)
	{
	case 'R':
		m_shader_project.LoadChangedShaders();
		break;
	}
#ifdef USE_SPOT_LIGHT
	// Because no sprites to use, you can only select objects(lights),
	// Load/save scene config file, add lights/objects on keyboard.
	int id;
	//Select object/light
	if (c >= '0' && c <= '9')
	{
		id = c - '0';
		if (id == 0)
			id = 10;
		switch (m_InteractiveMode)
		{
		case IMC_ENVIROMENT:
			if (!spotlight.empty())
			{
				id %= spotlight.size();
				OnLightChange(id);
				_tprintf("Select light id: %d\n", id);
			}
			else
				_tprintf("No lights.\n");
			break;
		case IMC_NONE:
			if (!object.empty())
			{
				id %= object.size();
				OnSelectionChange(id);
				_tprintf("Select object id: %d\n", id);
			}
			else
				_tprintf("No objects.\n");
		}
	}
	//Load scene config file
	else if (c == 'L')
	{
		OnLoadConfigFile();
	}
	else if (c == 'I')
	{
		OnAddLight();
	}
#endif
	return false;
}
/*add*/
void CSceneWnd::getTriangleList( const char* filename, vector<num::Vec3f>& triangle_list ) {
	GeoObj::CGeoObject obj;
	if(obj.LoadOBJModel(CString(filename))) {
		for( int j = 0; j < obj.m_nTriangles; j++ ) {
			//cout << obj.m_pTriangles[j].m_nVertexIndices[0] << endl;
			num::Vec3f v1;
			v1.CopyFrom((float*)&obj.m_pVertices[(obj.m_pTriangles[j].m_nVertexIndices[0])].m_vPosition );
			num::Vec3f v2;
			v2.CopyFrom((float*)&obj.m_pVertices[(obj.m_pTriangles[j].m_nVertexIndices[1])].m_vPosition );
			num::Vec3f v3;
			v3.CopyFrom((float*)&obj.m_pVertices[(obj.m_pTriangles[j].m_nVertexIndices[2])].m_vPosition );
			triangle_list.push_back( v1 );
			triangle_list.push_back( v2 );
			triangle_list.push_back( v3 );
		}
	}
}
void CSceneWnd::CreateTexture() {
	int m_texResX = 2048;
	int m_texResY = 3072;
	int m_texResVertexY = 3072/3;
	int m_texResVertexX = 2048;

	//SMM::Buffer<num::Vec4f> pData;
	rt::Buffer<num::Vec4f> pData;
	pData.SetSize( m_texResX*m_texResY );
	//num::Vec4f v = num::Vec4f( 0, 0, 0, 0 );
	//pData.SetItem( v );
	
	//triangle_list
	/*for( int i = 0; i < m_texResVertexY; i++ ) {
		for( int j = 0; j < m_texResVertexX; j++ ) {
			int iVertex = i * m_texResVertexX + j;
			for( int s = 0; s < object.size(); s++ ) {
				for( int k = 0; k < triangle_list[s].size(); k++ ) {
					pData.At(i) = num::Vec4f( triangle_list[s][k].x, triangle_list[s][k].y, triangle_list[s][k].z, 0 );
				}
			}
		}
	}*/
	int i = 0;
	for( int s = 0; s < object.size(); s++ ) {
		for( int k = 0; k < triangle_list[s].size(); k+=3 ) {
			pData.At(i) = num::Vec4f( triangle_list[s][k].x, triangle_list[s][k].y, triangle_list[s][k].z, 0 );
			pData.At( i + m_texResX) = num::Vec4f( triangle_list[s][k+1].x, triangle_list[s][k+1].y, triangle_list[s][k+1].z, 0 );
			pData.At( i + 2*m_texResX) = num::Vec4f( triangle_list[s][k+2].x, triangle_list[s][k+2].y, triangle_list[s][k+2].z, 0 );
			i++;
			if( i % ( m_texResX ) == 0 )
				i += ( 2 * m_texResX );
		}
	}
	m_shTex.TextureFilterSet( wglh::tx::FilterNearest, wglh::tx::FilterNearest );
	m_shTex.DefineTexture( m_texResX, m_texResY, pData.Begin() );

	m_shTex.Enable();
	m_shTex.Apply();
	m_shTex.Disable();
}

void CSceneWnd::OnLightChange(int selection)
{
	current_light_id = selection;
	m_lightCtrl.GetTranslation_Ref() = spotlight[current_light_id].translation;
	spotlightColor[current_light_id] = spotlight[current_light_id].color;
	spotlightPos[current_light_id] = spotlight[current_light_id].translation;
}

num::Vec4f white(1.f, 1.f, 1.f, 1.f);
num::Vec4f black(.1f, .1f, .1f, .1f);
num::Vec4f dim(.2f, .2f, .2f, .2f);

num::Vec3f glb_cameraPosition(-2.5f, 3.5f,-2.5f);
num::Vec3f glb_lightPosition(2.0f, 3.0f,-2.0f);

const int texW = 1024;
const int texH = 1024;
void CSceneWnd::initShadowMap()
{
	//Creating the Depth Cubemap Texture

	//depth cubemap texture
	glGenTextures(1, &m_texID);
	glBindTexture(GL_TEXTURE_CUBE_MAP, m_texID);

	//fixes seam-artifacts due to numerical precision limitations
	glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
	glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
	glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_R, GL_CLAMP_TO_EDGE);

	glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_MIN_FILTER, GL_LINEAR);

	glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_DEPTH_TEXTURE_MODE, GL_LUMINANCE);
	glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_COMPARE_MODE, GL_COMPARE_R_TO_TEXTURE);
	glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_COMPARE_FUNC, GL_LEQUAL);

	// traditional 24 bit unsigned int z-buffer
	//GLint internal_format = GL_DEPTH_COMPONENT24;
	//GLenum data_type = GL_UNSIGNED_INT;

	//float z-buffer (if more precision is needed)
	GLint internal_format = GL_DEPTH_COMPONENT24;
	GLenum data_type = GL_FLOAT;

	GLenum format = GL_DEPTH_COMPONENT;
	
	for (GLint face = 0; face < 6; face++)
	{
		glTexImage2D(GL_TEXTURE_CUBE_MAP_POSITIVE_X + face,
			0,
			internal_format,
			texW, texH, 0,
			format,
			data_type,
			NULL);
	}

	//Attaching Cubemap to FBO
	wglh::ext::CFrameBufferObject::LoadExtensionEntryPoint();
	//Create FBO
	m_FBO.Create();

	//attach depth cubemap texture to FBO's depth attachment point

	wglh::ext::CFrameBufferObject::CheckFramebufferStatus();
	_CheckErrorGL

	// create camera matrix
	//TODO: 

	// create light face matrices
	glLoadIdentity(); gluLookAt(0.0, 0.0, 0.0,  1.0, 0.0, 0.0,  0.0,-1.0, 0.0); // +X
	glGetFloatv(GL_MODELVIEW_MATRIX, lightFaceMatrix[0]);
	glLoadIdentity(); gluLookAt(0.0, 0.0, 0.0, -1.0, 0.0, 0.0,  0.0,-1.0, 0.0); // -X
	glGetFloatv(GL_MODELVIEW_MATRIX, lightFaceMatrix[1]);
	glLoadIdentity(); gluLookAt(0.0, 0.0, 0.0,  0.0, 1.0, 0.0,  0.0, 0.0, 1.0); // +Y
	glGetFloatv(GL_MODELVIEW_MATRIX, lightFaceMatrix[2]);
	glLoadIdentity(); gluLookAt(0.0, 0.0, 0.0,  0.0,-1.0, 0.0,  0.0, 0.0,-1.0); // -Y
	glGetFloatv(GL_MODELVIEW_MATRIX, lightFaceMatrix[3]);
	glLoadIdentity(); gluLookAt(0.0, 0.0, 0.0,  0.0, 0.0, 1.0,  0.0,-1.0, 0.0); // +Z
	glGetFloatv(GL_MODELVIEW_MATRIX, lightFaceMatrix[4]);
	glLoadIdentity(); gluLookAt(0.0, 0.0, 0.0,  0.0, 0.0,-1.0,  0.0,-1.0, 0.0); // -Z
	glGetFloatv(GL_MODELVIEW_MATRIX, lightFaceMatrix[5]);

	// create light projection matrix
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	gluPerspective(90.0, 1.0, SHADOW_NEAR, SHADOW_FAR);
	glGetFloatv(GL_PROJECTION_MATRIX, lightProjectionMatrix);
	glMatrixMode(GL_MODELVIEW);
	_CheckErrorGL
}

void CSceneWnd::renderWithShadow()
{
	m_shader_program.Apply();
	glClear(GL_ACCUM_BUFFER_BIT);
	for (size_t i = 0; i < spotlight.size(); i++)
	{
		setupLight(i);
		setupMatrices(i);
		renderDepthCubemap();
		RenderScene();
		glAccum(GL_ACCUM, 1.0);
	}
	glAccum(GL_RETURN, 1.0);
	m_shader_program.DisableShaders();
}

void CSceneWnd::renderDepthCubemap()
{
	//Save status
	glMatrixMode(GL_PROJECTION);
	glPushMatrix();
	glMatrixMode(GL_MODELVIEW);
	glPushMatrix();

	m_FBO.Apply();
	glDrawBuffer(GL_NONE);
	glReadBuffer(GL_NONE);
	glViewport(0, 0, texW, texH);

	glCullFace(GL_FRONT);

	glMatrixMode(GL_PROJECTION);
	glLoadMatrixf(lightProjectionMatrix);
	glMatrixMode(GL_MODELVIEW);
	m_shader_program.SetUniform("shadow_pass", 1);
	for (size_t i = 0; i < 6; i++)
	{
		m_FBO.AttachTexture2D(GL_DEPTH_ATTACHMENT_EXT, GL_TEXTURE_CUBE_MAP_POSITIVE_X + i, m_texID, 0);
		glClear(GL_DEPTH_BUFFER_BIT);
		glLoadMatrixf(lightFaceMatrix[i]);
		glMultMatrixf(lightViewMatrix);
		renderObjectPrimitive();
	}
	//Restore
	glMatrixMode(GL_PROJECTION);
	glPopMatrix();
	glMatrixMode(GL_MODELVIEW);
	glPopMatrix();
}

void CSceneWnd::RenderScene()
{
	m_FBO.ApplyNull();
	glDrawBuffer(GL_BACK);
	glReadBuffer(GL_BACK);
	_CheckErrorGL
	glViewport(0, 0, width, height);

	glCullFace(GL_BACK);
	glMatrixMode(GL_PROJECTION);
	glLoadMatrixf(cameraProjectionMatrix);
	glMatrixMode(GL_MODELVIEW);

	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT | GL_STENCIL_BUFFER_BIT);
	glLoadMatrixf(cameraViewMatrix);

	m_shader_program.SetUniform("shadow_pass", 0);
	m_shader_program.SetUniformMatrix_4x4("light_view_matrix", lightViewMatrix);
	m_shader_program.SetUniformMatrix_4x4("light_projection_matrix", lightProjectionMatrix);


	glEnable(GL_TEXTURE_CUBE_MAP);
	glBindTexture(GL_TEXTURE_CUBE_MAP, m_texID);

	OnRenderObject();
	m_shader_program.Apply();

	glBindTexture(GL_TEXTURE_CUBE_MAP, 0);
	glDisable(GL_TEXTURE_CUBE_MAP);
}

void CSceneWnd::setupMatrices(unsigned index)
{
	glGetFloatv(GL_MODELVIEW_MATRIX, cameraViewMatrix);
	glGetFloatv(GL_PROJECTION_MATRIX, cameraProjectionMatrix);
	glMatrixMode(GL_MODELVIEW);
	glPushMatrix();
	{
		glLoadIdentity();
		glTranslatef(-spotlightPos[index].x, -spotlightPos[index].y, -spotlightPos[index].z);
		glGetFloatv(GL_MODELVIEW_MATRIX, lightViewMatrix);
	}
	glPopMatrix();
}

void CSceneWnd::setupLight(unsigned index)
{
#ifdef USE_SPOT_LIGHT
	int size = min<int>(10, spotlight.size());
	m_shader_program.Apply();
	m_shader_program.SetUniform("spotlightColor", spotlightColor[index]);
	m_shader_program.SetUniform("spotlightPos", spotlightPos[index]);
#endif
}

void CSceneWnd::renderWithoutShadow()
{
	OnRenderObject();
}

void CSceneWnd::renderObjectPrimitive()
{
	for (int i = 0; i < object.size(); i++)
	{
		glPushMatrix();
		if (current_object_id == i)
		{
			m_transformCtrl.prepareDrag();
			m_transformCtrl.ApplyFull(GetRotationToScene());
			object_scale[i] = m_transformCtrl.GetScale();
			object_translation[i] = m_transformCtrl.GetTranslation_Ref();
			object_rotation[i] = m_transformCtrl.GetRotationMatrix(GetRotationToScene());
		} 
		else
		{
			glTranslatef(object_translation[i][0], object_translation[i][1], object_translation[i][2]);
			glScalef(object_scale[i], object_scale[i], object_scale[i]);
			glMultMatrixf(object_rotation[i]);
		}

		object[i]->Render(0);
		glPopMatrix();
	}
}
