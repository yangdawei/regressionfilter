in vec3 pointLocation;
in vec3 pointNormal;

uniform vec3 viewPosition;
uniform vec3 lightDirection;
uniform vec3 pointColor;
uniform vec3 lightColor;

void main()
{
	vec3 i = normalize(lightDirection);
	vec3 o = normalize(viewPosition - pointLocation);
	vec3 h = normalize(i + o);

	float diffuse = max(dot(i, pointNormal), 0.0);
	float specular = pow(max(dot(h, pointNormal), 0.0), 50.0);
	
	vec3 finalColor = pointColor * diffuse + lightColor * specular;
	
	gl_FragColor = vec4(finalColor, 1.0);
}