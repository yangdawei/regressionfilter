#version 130
//#extension GL_ARB_gpu_shader5 : enable

uniform float objectScale;
uniform vec3 objectTranslation;
uniform mat4 objectRotation;

uniform vec3 spotlightPos[10];
uniform int spotlightNum;

out vec3 pointLocation;
out vec3 pointNormal;
out vec3 spotlightDir[10];

void main()
{
	gl_Position = gl_ModelViewProjectionMatrix * gl_Vertex;
	pointLocation = vec3(objectRotation * gl_Vertex);
	pointLocation = pointLocation * objectScale;
	pointLocation = pointLocation + objectTranslation;
	for (int i = 0; i < spotlightNum; i++)
	{
		spotlightDir[i] = spotlightPos[i] - pointLocation;
	}

	pointNormal = normalize(vec3(objectRotation * vec4(gl_Normal, 1.0)));
}
