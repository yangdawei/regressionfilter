in vec3 pointLocation;
in vec3 pointNormal;
in vec3 spotlightDir[10];

uniform vec3 viewPosition;
uniform vec3 pointColor;
uniform int spotlightNum;

uniform vec3 spotlightColor[10];

void main()
{
	vec3 finalColor = vec3(0.0, 0.0, 0.0);
	vec3 o = normalize(viewPosition - pointLocation);
	for (int cnt = 0; cnt < spotlightNum; cnt++)
	{
		vec3 i = normalize(spotlightDir[cnt]);
		vec3 h = normalize(i + o);
		float diffuse = max(dot(i, pointNormal), 0.0);
		float specular = pow(max(dot(h, pointNormal), 0.0), 2.0);

		finalColor += spotlightColor[cnt] * diffuse;// + spotlightColor[cnt] * specular;
	}

	gl_FragColor = vec4(finalColor, 1.0);
}