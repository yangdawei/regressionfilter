in vec3 pointLocation;
in vec3 pointNormal;
in vec3 viewPosition;
in vec3 lightDirection;

//vec3 pointColor = vec3(1.0, 0.0, 0.0);
uniform vec3 pointColor;
vec3 lightColor = vec3(1.0, 1.0, 1.0);

void main()
{
	vec3 i = normalize(lightDirection);
	vec3 o = normalize(viewPosition - pointLocation);
	vec3 h = normalize(i + o);

	float diffuse = max(dot(i, pointNormal), 0.0);
	float specular = pow(max(dot(h, pointNormal), 0.0), 50.0);
	
	vec3 finalColor = pointColor * diffuse + lightColor * specular;
	
	gl_FragColor = vec4(finalColor, 1.0);
	
	if (pointLocation == vec3(0.0, 0.0, 0.0))
		gl_FragColor = vec4(1.0, 0.0, 0.0, 0.0);
}