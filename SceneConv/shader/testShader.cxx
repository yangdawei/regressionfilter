uniform float lambda;
uniform vec3 viewPos;
uniform vec3 i_c;
//uniform vec3 pos_a;
//uniform vec3 pos_b;
//uniform vec3 pos_c;
uniform vec3 triangles[36*4*3];
uniform vec3 brdf1_c;
uniform vec3 brdf1_h;
uniform vec3 brdf2_c;
uniform vec3 brdf2_h;
//uniform vec3 nn2;
uniform float s;

varying vec3 N;
varying vec3 v;

float PI = 3.1415927;
float exp = 2.7182818;
vec3 color;
/*float sinh( float x ) {
	return ( pow( exp, x ) - pow( exp, -x ) ) / 2.0;
}

float f( float lambda, float m, float phi ) {
	return lambda * ( sin(phi) / sqrt(pow(m,2.0) + pow(sin(phi), 2.0)) - 1.0 );
}


float int_phi( float lambda, float m, float phi_1, float phi_2 ) {
	float res = 0.0;
	
    int n = 5;
    for( int i = 0; i < n; i++ ) {
        float s1 = (phi_2 - phi_1) / float(n) * (float(i)) + phi_1;
        float s2 = (phi_2 - phi_1) / float(n) * (float(i)+1.0) + phi_1;
        float f1 = f( lambda, m, s1 );
        float f2 = f( lambda, m, s2 );
            
        float a = (f2-f1) / (s2-s1);
        float b = f1 - a*s1;
        res += 1.0 / lambda * ((s2-s1) - 1.0/a * ( pow(exp, f2)-pow( exp, f1)));
	}
	return res;
}
float int_tri_phi( float lambda, float m, float phi_1, float phi_2 ) {
	float res = 0.0;
	if( phi_1 > phi_2 ) {
		res = -1.0 * int_phi( lambda, m, phi_2, phi_1 );
	}
	else if ( phi_1 < PI/2.0 && phi_2 > PI/2.0 ) {
		res =  int_phi( lambda, m, phi_1, PI/2.0 ) + int_phi( lambda, m, PI/2.0, phi_2 );
    }
	else
		res = int_phi( lambda, m, phi_1, phi_2 );
	return res;
}
*/
void product_srbf( vec3 i_1, float lambda_1, vec3 i_2, float lambda_2, 
	inout vec3 i_r, inout float c_r, inout float lambda_r ) {
	vec3 i_m = (lambda_1 * i_1  + i_2 * lambda_2) / (lambda_1 + lambda_2);
	float len = length( i_m );
	i_r = i_m / len;
    //i_r = normalize( i_m );
	lambda_r = ( lambda_1 + lambda_2 ) * len;
    c_r = pow( exp, (lambda_1 + lambda_2) * ( len - 1.0  ) );
}
/*
vec3 vec_on_plane( vec3 i_c, vec3 pos_p, vec3 pos_a ) {
	vec3 res;
    vec3 v_a;
	v_a = pos_p - pos_a;
	float l = dot( v_a, i_c );
    vec3 remVec;
	remVec = v_a - l * i_c;
    res = remVec / l;
    return res;
}
void tri_param( vec3 i_c, vec3 pos_p, vec3 pos_a, vec3 pos_b, 
	inout float m, inout float phi_min, inout float phi_max ) {
	
	vec3 v_a = vec_on_plane(i_c,pos_p,pos_a);
    vec3 v_b = vec_on_plane(i_c,pos_p,pos_b);      
    vec3 v_a2b;
	v_a2b = v_a - v_b;
	vec3 tmp;
	tmp = cross(v_a, v_b);
	if( dot( tmp, i_c ) < 1e-6 )
		v_a2b = -v_a2b;
	float len_a = length( v_a );
    float len_b = length( v_b );
    float len_ab = length( v_a2b );
    //float m, phi_min, phi_max;
    if (len_a < 1e-6 || len_b < 1e-6 || len_ab < 1e-6 ) {
        m = 0.0;
        phi_min = 0.0;
        phi_max = 0.0;
    }
    else {
		float cos_phi_a;
		cos_phi_a = dot( v_a2b, v_a );
		//color = v_a2b;
		cos_phi_a = cos_phi_a / (len_a*len_ab);
		float cos_phi_b;
		cos_phi_b = dot( v_a2b, v_b );
		cos_phi_b = cos_phi_b / (len_b*len_ab);
        vec3 v_h;
		v_h = v_a - cos_phi_a  * len_a * v_a2b / len_ab ;
        m = length( v_h );
		//cout << cos_phi_a << endl;
        phi_min = acos(cos_phi_a);
		//color = phi_min;
        phi_max = acos(cos_phi_b);
    }
}
float int_arbi_triangle( float lambda, vec3 i_c, vec3 pos_p, 
	vec3 pos_a, vec3 pos_b, vec3 pos_c ) {
	
	float res = 0.0;
    float m1, m2, m3, phi1_min, phi2_min, phi3_min, phi1_max, phi2_max, phi3_max;
    tri_param(i_c,pos_p,pos_a,pos_b,m1, phi1_min, phi1_max);
    tri_param(i_c,pos_p,pos_b,pos_c,m2, phi2_min, phi2_max);
    tri_param(i_c,pos_p,pos_c,pos_a,m3, phi3_min, phi3_max);
    
	res = int_tri_phi(lambda,m1,phi1_min,phi1_max) + int_tri_phi(lambda,m2,phi2_min,phi2_max) + int_tri_phi(lambda,m3,phi3_min,phi3_max);
	return res;
}
*/
void getSTDir( vec3 vN, inout vec3 vS, inout vec3 vT ) {

	if( abs(vN.z) > 1.0 - 1e-6) {
		vS = vec3( 1, 0, 0 );
        vT = cross(vN, vS);
		vT = normalize( vT );
    }
    else {
		vec3 tmp = vec3( 0, 0, 1 );
		vS = cross( tmp, vN );
		vS = normalize(vS);
		vT = cross( vN, vS );
		vT = normalize(vT);
    }
}

float int_gauss_whole( vec3 i1, float lambda ) {
	return 2.0 * PI / lambda * ( 1.0 - pow( exp, -2.0 * lambda ) );
}
/*
float int_gauss_tri( vec3 pos_m, vec3 pos_p, vec3 pos_a, vec3 pos_b, vec3 pos_c, vec3 n1, float S,
	float lambda, vec3 i_c ) {
	vec3 i_m = pos_p - pos_m;
	float len = length( pos_p - pos_m );
	vec3 i_v = i_m / len;
	float U = S * dot( i_m, n1 ) / pow( len, 3.0 );
	float sum = pow( distance( pos_a, pos_b ), 2.0 ) + pow( distance( pos_b, pos_c ), 2.0 ) + pow( distance( pos_a, pos_c ), 2.0 );
	float W = U * ( 1.0 - pow( dot( i_v, n1 ), 2 ) * sum / ( 72.0 * pow( len, 2.0 ) ) );
	float lambda_v = U / ( U - W );
	float c_v = U * U / ( 2 * PI * ( U - W ) );
	vec3 i_r;
	float c_r, lambda_r;
	product_srbf( i_c, lambda, i_v, lambda_v, i_r, c_r, lambda_r );
	c_r = c_r * c_v;
	return c_r * int_gauss_whole( i_r, lambda_r );
}
float g( float x, float m ) {
	float res = 0.0;
	res = x - atan((2.0*pow(sin(0.5*x), 2.0) - 1.0)/sqrt( pow(m, 2.0) + pow( sin(x), 2.0) ));
	return res;
}
float int_solid_angle( vec3 i_c, vec3 pos_a, vec3 pos_b, vec3 pos_c, vec3 pos_p ) {
	float m, phi_min, phi_max;
	float res = 0.0;
	tri_param(i_c,pos_p,pos_b,pos_c,m, phi_min, phi_max);
    res = g( phi_max, m ) - g( phi_min, m );
	if( phi_max < phi_min ) {
		res = -1.0 * res;
	}
	return res;
}
*/
float int_solid_angle2( vec3 pos_a, vec3 pos_b, vec3 pos_c, vec3 pos_p ) {
	float det;
	det = abs( dot( pos_a, cross( pos_b, pos_c ) ) );
	float len_a = length( pos_a );
	float len_b = length( pos_b );
	float len_c = length( pos_c );
	float div;
	div = len_a*len_b*len_c + dot( pos_a, pos_b )*len_c + dot( pos_a, pos_c )*len_b + dot( pos_b, pos_c )*len_a;
	float at;
	at = atan( det, div );
	if( at < 0.0 ) 
		at += PI;
	float sr;
	sr = 2.0 * at;
	return sr;
}
float int_gauss_tri2 ( vec3 pos_m, vec3 pos_p, vec3 pos_a, vec3 pos_b, vec3 pos_c, vec3 n1, float S,
	float lambda, vec3 i_c ) {
	
	vec3 i_m = pos_p - pos_m;
	//vec3 i_a = pos_p - pos_a;
	//i_a = normalize( i_a );
	float len = length( pos_p - pos_m );
	vec3 i_v = i_m / len;
	//float U1 = int_solid_angle( i_a, pos_a, pos_b, pos_c, pos_p );
	float U1 = int_solid_angle2( pos_a-pos_p, pos_b-pos_p, pos_c-pos_p, pos_p );
	//float W = 2.0 * PI * sqrt( 1.0 - pow( 1.0 - U1 / ( 2.0 * PI )  , 2.0 ) );
	//float W1 = PI * (1.0 - pow( 1.0 - U1 / ( 2.0 * PI )  , 2.0 ) );
	
	//float W = sqrt( 1.0 - pow( 1.0 - U / ( 2.0 * PI )  , 2.0 ) );
	//float U = S * dot( i_m, n1 ) / pow( len, 3.0 );
	//float sum = pow( distance( pos_a, pos_b ), 2.0 ) + pow( distance( pos_b, pos_c ), 2.0 ) + pow( distance( pos_a, pos_c ), 2.0 );
	//float W = U * ( 1.0 - pow( dot( i_v, n1 ), 2 ) * sum / ( 72.0 * pow( len, 2.0 ) ) );
	//float W1 = U1 * ( 1.0 - pow( dot( i_v, n1 ), 2 ) * sum / ( 72.0 * pow( len, 2.0 ) ) );
	
	//float c = W1 / U1;
	//float lambda_v1 = ( 3.0*c - pow( c, 3.0 ) ) / ( 1 - pow( c, 2.0 ) );
	//float lambda_v1 = U1 / ( U1 - W1 );
	float lambda_v1 = 4.0*PI / U1;
	//float c_v1 = U1 * U1 / ( 2 * PI * ( U1 - W1 ) );
	float c_v1 = 2.0;
	//float lambda_v = U / ( U - W );
	//float c_v = U * U / ( 2 * PI * ( U - W ) );
	vec3 i_r;
	float c_r, lambda_r;
	product_srbf( i_c, lambda, i_v, lambda_v1, i_r, c_r, lambda_r );
	c_r = c_r * c_v1;
	//color = c_r * int_gauss_whole( i_r, lambda_r );
	//color = lambda_v1;
	return c_r * int_gauss_whole( i_r, lambda_r );
	//color = W1 - W;
	//return lambda_v1 - lambda_v;
}
void main() {
	float res = 0.0;
	float res1, res2, res3_1, res3_2, res4_1, res4_2 = 0.0;
	vec3 p1;// = pos_a;
	vec3 p2;// = pos_b;
	vec3 p3;// = pos_c;
	vec3 pos_p = v;
	vec3 n2 = N;
	//vec3 n2 = nn2;
	//vec3 vDir = normalize( viewPos - v );
	//vec3 vDir = viewPos;
	vDir = normalize( viewPos );
	i_c = normalize( i_c );
	for( int i = 0; i < 36*4; i++ ) {
		
		//p2 = p3; 
		//p3 = p4;
		p1 = triangles[3*i];
		p2 = triangles[3*i+1];
		p3 = triangles[3*i+2];
		vec3 center_tri = (p1+p2+p3)/3;
		vec3 aver_r;
		aver_r = pos_p - center_tri;
		aver_r = normalize(aver_r);
		vec3 n1;
		vec3 tmp1, tmp2;
		tmp1 = p2-p1;
		tmp2 = p3-p1;
		n1 = cross( tmp1, tmp2 );
		n1 = normalize(n1);
		vec3 s1, t1;
		getSTDir(n1, s1, t1);
	
		float S = length( cross( p2 - p1, p3 - p1 ) ) / 2.0;
		float brdf_kd_1 = brdf1_c.x;
		float brdf_ks_1 = brdf1_c.y;
    
		vec3 brdf_h_1;
		//brdf_h_1 = vec3( brdf1[2], brdf1[3], brdf1[4]);
		brdf_h_1 = brdf1_h;
		float brdf_lambda_1 = brdf1_c.z;
		vec3 h_1;
		h_1 = brdf_h_1.x * s1 + brdf_h_1.y * t1 + brdf_h_1.z* n1;

		vec3 i_c_r;
		i_c_r = 2.0 * dot(i_c, h_1) * h_1 - i_c;        

		float lambda_m = (lambda+(brdf_lambda_1/4.0) );
		float i_m_sqrt = 1.0 - lambda*(brdf_lambda_1/4.0)/pow(lambda_m,2.0) * ( 1.0 - dot(i_c_r, aver_r) );
    
		float f1_n1_r = 2.0*PI*brdf_ks_1*dot(i_c, n1)/(lambda_m * i_m_sqrt);
		float g1_ic_dot_n1 = dot( i_c, n1 ) * (2.0*PI*brdf_kd_1/lambda) * ( 1.0 - pow(exp, -2.0 * lambda));

		float lambda_1_pie = lambda*(brdf_lambda_1/4.0) / (lambda+(brdf_lambda_1/4.0) );
    
		float kd_2 = brdf2_c.x;
		float ks_2 = brdf2_c.y;
		vec3 brdf_h_2;
		//brdf_h_2 = vec3( brdf2[2], brdf2[3], brdf2[4]);
		brdf_h_2 = brdf2_h;
		float brdf_lambda_2 = brdf2_c.z;
		vec3 s2, t2;
		getSTDir(n2, s2, t2);
		vec3 h_2;
		h_2 = brdf_h_2.x* s2 + brdf_h_2.y * t2 + brdf_h_2.z* n2;
    
		float coeff_for_cos = 1.170;
		float lambda_for_cos = 2.133;
    
		
		
		/*res1 = g1_ic_dot_n1 * kd_2 * coeff_for_cos *
		int_arbi_triangle(lambda_for_cos,-n2,pos_p,p1,p2,p3);

		res2 = g1_ic_dot_n1 * ks_2 * dot( aver_r, -n2 ) *
		int_arbi_triangle(brdf_lambda_2/4.0,vDir - 2.0*dot( vDir, h_2)*h_2,pos_p,p1,p2,p3);    

		res3_1= kd_2 * f1_n1_r * dot( aver_r, -n2) * 
		int_arbi_triangle(lambda_1_pie,i_c_r,pos_p,p1,p2,p3);      

		res3_2 = kd_2 * f1_n1_r * dot( aver_r, -n2) * pow( exp, -2.0*lambda_m) * 
		int_arbi_triangle(-lambda_1_pie,i_c_r,pos_p,p1,p2,p3);

		vec3 i_comb_1, i_comb_2;
		float lambda_comb_1, lambda_comb_2, c_comb_1, c_comb_2;
		product_srbf(i_c_r,lambda_1_pie,vDir - 2.0 * dot(vDir, h_2)*h_2,brdf_lambda_2/4.0, 
			i_comb_1, c_comb_1, lambda_comb_1 );
		product_srbf(i_c_r,-lambda_1_pie,vDir - 2.0 * dot(vDir, h_2)*h_2,brdf_lambda_2/4.0, 
			i_comb_2, c_comb_2, lambda_comb_2 );    
	
		res4_1 = f1_n1_r * ks_2 * dot(aver_r,-n2) * c_comb_1 * int_arbi_triangle(lambda_comb_1,i_comb_1,pos_p,p1,p2,p3);
		res4_2 = f1_n1_r * ks_2 * dot(aver_r, -n2) * pow(exp, -2.0 * lambda_m) * c_comb_2 *
			int_arbi_triangle(lambda_comb_2,i_comb_2,pos_p,p1,p2,p3);    
		*/
		
		/*
		res1 = g1_ic_dot_n1 * kd_2 * coeff_for_cos * int_gauss_tri( center_tri, pos_p, p1, p2, p3, n1, S, lambda_for_cos, -n2 );
		res2 = g1_ic_dot_n1 * ks_2 *  dot( aver_r, -n2 ) * 
			int_gauss_tri( center_tri, pos_p, p1, p2, p3, n1, S, brdf_lambda_2/4.0,vDir - 2.0*dot( vDir, h_2)*h_2 );
		res3_1 = kd_2 * f1_n1_r * dot( aver_r, -n2) * 
			int_gauss_tri( center_tri, pos_p, p1, p2, p3, n1, S, lambda_1_pie,i_c_r );
		res3_2 = kd_2 * f1_n1_r * dot( aver_r, -n2) * pow( exp, -2.0*lambda_m) * 
			int_gauss_tri( center_tri, pos_p, p1, p2, p3, n1, S, -lambda_1_pie,i_c_r );
		vec3 i_comb_1, i_comb_2;
		float lambda_comb_1, lambda_comb_2, c_comb_1, c_comb_2;
		product_srbf(i_c_r,lambda_1_pie,vDir - 2.0 * dot(vDir, h_2)*h_2,brdf_lambda_2/4.0, 
			i_comb_1, c_comb_1, lambda_comb_1 );
		product_srbf(i_c_r,-lambda_1_pie,vDir - 2.0 * dot(vDir, h_2)*h_2,brdf_lambda_2/4.0, 
			i_comb_2, c_comb_2, lambda_comb_2 );
		res4_1 = f1_n1_r * ks_2 * dot(aver_r,-n2) * c_comb_1 * 
			int_gauss_tri( center_tri, pos_p, p1, p2, p3, n1, S, lambda_comb_1, i_comb_1 );
		res4_2 = f1_n1_r * ks_2 * dot(aver_r, -n2) * pow(exp, -2.0 * lambda_m) * c_comb_2 *
			int_gauss_tri( center_tri, pos_p, p1, p2, p3, n1, S, lambda_comb_2, i_comb_2 ); 
		*/
		
		res1 = g1_ic_dot_n1 * kd_2 * coeff_for_cos * int_gauss_tri2( center_tri, pos_p, p1, p2, p3, n1, S, lambda_for_cos, -n2 );
		res2 = g1_ic_dot_n1 * ks_2 *  dot( aver_r, -n2 ) * 
			int_gauss_tri2( center_tri, pos_p, p1, p2, p3, n1, S, brdf_lambda_2/4.0,vDir - 2.0*dot( vDir, h_2)*h_2 );
		res3_1 = kd_2 * f1_n1_r * dot( aver_r, -n2) * 
			int_gauss_tri2( center_tri, pos_p, p1, p2, p3, n1, S, lambda_1_pie,i_c_r );
		res3_2 = kd_2 * f1_n1_r * dot( aver_r, -n2) * pow( exp, -2.0*lambda_m) * 
			int_gauss_tri2( center_tri, pos_p, p1, p2, p3, n1, S, -lambda_1_pie,i_c_r );
		vec3 i_comb_1, i_comb_2;
		float lambda_comb_1, lambda_comb_2, c_comb_1, c_comb_2;
		product_srbf(i_c_r,lambda_1_pie,vDir - 2.0 * dot(vDir, h_2)*h_2,brdf_lambda_2/4.0, 
			i_comb_1, c_comb_1, lambda_comb_1 );
		product_srbf(i_c_r,-lambda_1_pie,vDir - 2.0 * dot(vDir, h_2)*h_2,brdf_lambda_2/4.0, 
			i_comb_2, c_comb_2, lambda_comb_2 );
		res4_1 = f1_n1_r * ks_2 * dot(aver_r,-n2) * c_comb_1 * 
			int_gauss_tri2( center_tri, pos_p, p1, p2, p3, n1, S, lambda_comb_1, i_comb_1 );
		res4_2 = f1_n1_r * ks_2 * dot(aver_r, -n2) * pow(exp, -2.0 * lambda_m) * c_comb_2 *
			int_gauss_tri2( center_tri, pos_p, p1, p2, p3, n1, S, lambda_comb_2, i_comb_2 ); 
		
    
		res = res + res1 + res2 + (res3_1 - res3_2) + (res4_1 - res4_2);
	}
	vec3 color = vec3((res)* s);
	//gl_FragColor = lambda / 256;
	gl_FragColor = vec4( color, 1.0 );
}