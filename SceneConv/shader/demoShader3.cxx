in vec3 pointLocation;
in vec3 pointNormal;
uniform sampler2D shTexture;

uniform vec3 viewPosition;
uniform vec3 lightDirection;
uniform vec3 pointColor;
uniform vec3 lightColor;

uniform float lightLambda;
uniform int tri_num;
//uniform vec3 triangles[500];
uniform vec3 brdfs[2];
uniform vec3 self_brdf[2];
uniform float s;

uniform vec3 texColor;
uniform float texScale;
uniform vec3 texTranslation;
uniform mat4 texRotation;

uniform int startIndex;
uniform int resX;
uniform int resY;

float PI = 3.1415927;
float exp = 2.7182818;

struct BRDF_NDF {
	vec3 h;
	float lambda;
	float kd, ks;
};
struct SRBF {
	vec3 i_c;
	float lambda;
};

vec3 color;

void product_srbf( vec3 i_1, float lambda_1, vec3 i_2, float lambda_2, 
	inout vec3 i_r, inout float c_r, inout float lambda_r ) {
	
	vec3 i_m = (lambda_1 * i_1  + i_2 * lambda_2);
	float len = length( i_m );
	i_r = i_m / len;
    lambda_r = len;
    c_r = pow( exp, ( lambda_r - (lambda_1 + lambda_2) ) );
	
}
void getSTDir( vec3 vN, inout vec3 vS, inout vec3 vT ) {

	if( abs(vN.z) > 1.0 - 1e-6) {
		vS = vec3( 1, 0, 0 );
        vT = cross(vN, vS);
		vT = normalize( vT );
    }
    else {
		vec3 tmp = vec3( 0, 0, 1 );
		vS = cross( tmp, vN );
		vS = normalize(vS);
		vT = cross( vN, vS );
		vT = normalize(vT);
    }
}

float int_gauss_whole( vec3 i1, float lambda ) {
	return 2.0 * PI / lambda * ( 1.0 - pow( exp, -2.0 * lambda ) );
}

float int_solid_angle2( vec3 pos_a, vec3 pos_b, vec3 pos_c ) {
	float det;
	det = abs( dot( pos_a, cross( pos_b, pos_c ) ) );
	float len_a = length( pos_a );
	float len_b = length( pos_b );
	float len_c = length( pos_c );
	float div;
	div = len_a*len_b*len_c + dot( pos_a, pos_b )*len_c + dot( pos_a, pos_c )*len_b + dot( pos_b, pos_c )*len_a;
	float at;
	at = atan( det, div );
	if( at < 0.0 ) 
		at += PI;
	float sr;
	sr = 2.0 * at;
	return sr;
}

float int_gauss_tri2 ( vec3 pos_m, vec3 pos_p, vec3 pos_a, vec3 pos_b, vec3 pos_c,
	float lambda, vec3 i_c ) {
	float res = 0.0;
	vec3 i_m = pos_p - pos_m;
	float len = length( pos_p - pos_m );
	vec3 i_v = i_m / len;
	float U1 = int_solid_angle2( pos_a-pos_p, pos_b-pos_p, pos_c-pos_p );
	if( U1 < 1e-6 )
		return res;
	float lambda_v1 = 4.0*PI / U1;
	float c_v1 = 2.0;
	vec3 i_r;
	float c_r, lambda_r;
	product_srbf( i_c, lambda, i_v, lambda_v1, i_r, c_r, lambda_r );
	c_r = c_r * c_v1;
	return c_r * int_gauss_whole( i_r, lambda_r );
}

void main()
{
	// //vec3 i_c = normalize(lightDirection);
	// vec3 o = normalize(viewPosition - pointLocation);
	
	// vec3 res, res1, res2, res3_1, res3_2, res4_1, res4_2;
	// vec3 p1, p2, p3, pos_p, n2, vDir;
	// pos_p = pointLocation;
	// n2 = pointNormal;
	// //vDir = normalize(viewPosition);
	// vDir = o;
	
	// float fCoordTexX, fCoordTexY;
	
	// float stepTexX, stepTexY;
	// stepTexX = 1.0/resX;
	// stepTexY = 1.0/resY;
	// int indexX, indexY;
	
// //	lambda = 400.0;
	
	// BRDF_NDF brdf1, brdf2;
	
	// brdf1.kd = brdfs[0].x;
	// brdf1.ks = brdfs[0].y;
	// brdf1.lambda = brdfs[0].z;
	// brdf1.h = brdfs[1];
	
	// brdf2.kd = self_brdf[0].x;
	// brdf2.ks = self_brdf[0].y;
	// brdf2.lambda = self_brdf[0].z;
	// brdf2.h = self_brdf[1];
	
	// SRBF srbf;
	// srbf.i_c = normalize(lightDirection);
	// srbf.lambda = lightLambda;
	
	
	// for( int i = 0; i < tri_num; i++ ) {
	
		// indexX = mod( (startIndex+i), resX );
		// indexY = floor( (startIndex+i) / resX ); 
		// fCoordTexX = indexX * stepTexX + stepTexX / 2.0;
		// fCoordTexY = indexY * stepTexY * 3  + stepTexY / 2.0;
		// p1 = texture2D( shTexture, vec2( fCoordTexX, fCoordTexY ) ).xyz;
		// p2 = texture2D( shTexture, vec2( fCoordTexX, fCoordTexY + stepTexY ) ).xyz;
		// p3 = texture2D( shTexture, vec2( fCoordTexX, fCoordTexY + stepTexY*2) ).xyz;
		
		// p1 = vec3(texRotation * vec4(p1,0.0));
		// p1 = p1 * texScale;
		// p1 = p1 + texTranslation;
		
		// p2 = vec3(texRotation * vec4(p2,0.0));
		// p2 = p2 * texScale;
		// p2 = p2 + texTranslation;
		
		// p3 = vec3(texRotation * vec4(p3,0.0));
		// p3 = p3 * texScale;
		// p3 = p3 + texTranslation;
		
		// vec3 center_tri = ( p1 + p2 + p3 ) / 3;
		// vec3 aver_r;
		// aver_r = pos_p - center_tri;
		// aver_r = normalize(aver_r);
		// vec3 n1;
		// vec3 tmp1, tmp2;
		// tmp1 = p2-p1;
		// tmp2 = p3-p1;
		// n1 = cross( tmp1, tmp2 );
		// n1 = normalize(n1);
		// vec3 s1, t1;
		// getSTDir(n1, s1, t1);
		
		// // float brdf_kd_1 = brdfs[0].x;
		// // float brdf_ks_1 = brdfs[0].y;
    
		// // vec3 brdf_h_1;
		// // brdf_h_1 = brdfs[1];
		// // float brdf1.lambda = brdfs[0].z;
		
		// vec3 h_1;
		// h_1 = brdf1.h.x * s1 + brdf1.h.y * t1 + brdf1.h.z* n1;

		// vec3 i_c_r;
		// i_c_r = 2.0 * dot(srbf.i_c, h_1) * h_1 - srbf.i_c;        

		// float lambda_m = ( srbf.lambda + ( brdf1.lambda / 4.0 ) );
		// float i_m_sqrt = 1.0 - srbf.lambda * ( brdf1.lambda / 4.0 ) / pow( lambda_m, 2.0 ) * ( 1.0 - dot( i_c_r, aver_r ) );
    
		// float f1_n1_r = 2.0 * PI * brdf1.ks * max( dot(srbf.i_c, n1), 0.0 ) / ( lambda_m * i_m_sqrt );
		// float g1_ic_dot_n1 = max( dot( srbf.i_c, n1 ), 0.0 ) * (2.0 * PI * brdf1.kd / srbf.lambda ) * ( 1.0 - pow(exp, -2.0 * srbf.lambda));

		// float lambda_1_pie = srbf.lambda * ( brdf1.lambda / 4.0 ) / ( srbf.lambda + ( brdf1.lambda / 4.0 ) );
    
		// float kd_2 = self_brdf[0].x;
		// float ks_2 = self_brdf[0].y;
		// vec3 brdf_h_2;
		// brdf_h_2 = self_brdf[1];
		// float brdf_lambda_2 = self_brdf[0].z;
		// vec3 s2, t2;
		// getSTDir(n2, s2, t2);
		// vec3 h_2;
		// h_2 = brdf2.h.x* s2 + brdf2.h.y * t2 + brdf2.h.z* n2;
    
		// float coeff_for_cos = 1.170;
		// float lambda_for_cos = 2.133;
		
		// res1 = //0.0;
		// g1_ic_dot_n1 * brdf2.kd * coeff_for_cos * int_gauss_tri2( center_tri, pos_p, p1, p2, p3, lambda_for_cos, -n2 )* vec3( 1.0, 1.0, 1.0 );
		// res2 = //0.0;
			 // g1_ic_dot_n1 * brdf2.ks *  max( dot( aver_r, -n2 ), 0.0 ) * 
			   // int_gauss_tri2( center_tri, pos_p, p1, p2, p3, brdf2.lambda / 4.0,vDir - 2.0* dot( vDir, h_2)*h_2 )* vec3( 1.0, 1.0, 1.0 );
		// res3_1 = //0.0;
			 // brdf2.kd * f1_n1_r * max( dot( aver_r, -n2), 0.0 ) * 
			 // int_gauss_tri2( center_tri, pos_p, p1, p2, p3, lambda_1_pie,i_c_r ) * vec3( 1.0, 1.0, 1.0 );
		// //color = dot( aver_r, -pointNormal );
		// //color = pointNormal;
		// res3_2 = brdf2.kd * f1_n1_r * max( dot( aver_r, -n2), 0.0 ) * pow( exp, -2.0 * lambda_m ) * 
			  // int_gauss_tri2( center_tri, pos_p, p1, p2, p3, -lambda_1_pie,i_c_r ) * vec3( 1.0, 1.0, 1.0 );
		// // res3_2 = kd_2 * f1_n1_r * dot( aver_r, -n2) * pow( exp, -2.0 * lambda_m ) * 
			 // // int_gauss_tri2( center_tri, pos_p, p1, p2, p3, -lambda_1_pie,i_c_r ) * vec3( 1.0, 1.0, 1.0 );
		// vec3 i_comb_1, i_comb_2;
		// float lambda_comb_1, lambda_comb_2, c_comb_1, c_comb_2;
		// product_srbf(i_c_r, lambda_1_pie, vDir - 2.0 * dot( vDir, h_2)*h_2, brdf2.lambda / 4.0, 
			// i_comb_1, c_comb_1, lambda_comb_1 );
		// product_srbf(i_c_r, -lambda_1_pie, vDir - 2.0 * dot( vDir, h_2)*h_2, brdf2.lambda / 4.0, 
			// i_comb_2, c_comb_2, lambda_comb_2 );
		// res4_1 = //0.0;
			 // f1_n1_r * brdf2.ks * max( dot(aver_r,-n2), 0.0 ) * c_comb_1 * 
			 // int_gauss_tri2( center_tri, pos_p, p1, p2, p3, lambda_comb_1, i_comb_1 ) * vec3( 1.0, 1.0, 1.0 );
		// res4_2 = 
		 // f1_n1_r * brdf2.ks * max( dot(aver_r, -n2), 0.0 ) * pow( exp, -2.0 * lambda_m ) * c_comb_2 *
			  // int_gauss_tri2( center_tri, pos_p, p1, p2, p3, lambda_comb_2, i_comb_2 ) * vec3( 1.0, 1.0, 1.0 ); 
		// // res4_2 = 
		 // // f1_n1_r * ks_2 * dot(aver_r, -n2) * pow( exp, -2.0 * lambda_m ) * c_comb_2 *
			 // // int_gauss_tri2( center_tri, pos_p, p1, p2, p3, lambda_comb_2, i_comb_2 ) * vec3( 1.0, 1.0, 1.0 ); 
		
		// //res3_2 = 0.0;
		// //res4_2 = 0.0;
		// //res = res + res1;
		// //res = res + pow( exp, -2.0 * lambda_m );
		// //res = res + res1;
		// res = res + res1 + res2 + (res3_1 - res3_2) + (res4_1 - res4_2);
		// //color = vec3( p1 );
	// }
	// //s = 3000.2;
	// //res = clamp(res);
	// // if( res.x > 1.0 || res.y > 1.0 || res.z > 1.0 )
		// // res = vec3( 1.0 );
	// //res = vec3( 1.0 );
	// //vec3 color = vec3((res)*s);
	// //vec3 color = vec3( 1.0, 0.0, 0.0 );
	// //vec3 color = pointColor;
	gl_FragColor.xyz = (pointNormal + vec3 (1)) / 2.0;
	
	//gl_FragColor = vec4(finalColor, 1.0);
}