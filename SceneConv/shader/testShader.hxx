varying vec3 N;
varying vec3 v;

void main() {
	v = gl_Vertex;//vec3(gl_ModelViewMatrix * gl_Vertex);    
    N = gl_Normal;
	gl_Position = ftransform();
	gl_Position.xyz /= gl_Position.w;
	gl_Position.w = 1.0;
}