#version 130
//#extension GL_ARB_gpu_shader5 : enable

uniform float objectScale;
uniform vec3 objectTranslation;
uniform mat4 objectRotation;

uniform vec3 spotlightPos;
uniform int shadow_pass;

out vec3 pointLocation;
out vec3 pointNormal;
out vec3 spotlightDir;

void main()
{
	gl_Position = gl_ModelViewProjectionMatrix * gl_Vertex;

	if (shadow_pass == 0)
	{
		pointLocation = vec3(objectRotation * gl_Vertex);
		pointLocation = pointLocation * objectScale;
		pointLocation = pointLocation + objectTranslation;
		spotlightDir = spotlightPos - pointLocation;

		pointNormal = normalize(vec3(objectRotation * vec4(gl_Normal, 1.0)));
	}
}
