in vec3 pointLocation;
in vec3 pointNormal;
in vec3 viewPosition;
in vec3 lightDirection;

//vec3 pointColor = vec3(1.0, 0.0, 0.0);
uniform vec3 pointColor;

uniform float lambda;
uniform int tri_num;
//uniform vec3 triangles[500];
uniform vec3 brdfs[2];
uniform vec3 self_brdf[2];
uniform float s;
uniform sampler2D shTexture;
uniform int startIndex;
uniform int resX;
uniform int resY;


vec3 lightColor = vec3(1.0, 1.0, 1.0);
float PI = 3.1415927;
float exp = 2.7182818;



void product_srbf( vec3 i_1, float lambda_1, vec3 i_2, float lambda_2, 
	inout vec3 i_r, inout float c_r, inout float lambda_r ) {
	vec3 i_m = (lambda_1 * i_1  + i_2 * lambda_2) / (lambda_1 + lambda_2);
	float len = length( i_m );
	i_r = i_m / len;
    lambda_r = ( lambda_1 + lambda_2 ) * len;
    c_r = pow( exp, (lambda_1 + lambda_2) * ( len - 1.0  ) );
}
void getSTDir( vec3 vN, inout vec3 vS, inout vec3 vT ) {

	if( abs(vN.z) > 1.0 - 1e-6) {
		vS = vec3( 1, 0, 0 );
        vT = cross(vN, vS);
		vT = normalize( vT );
    }
    else {
		vec3 tmp = vec3( 0, 0, 1 );
		vS = cross( tmp, vN );
		vS = normalize(vS);
		vT = cross( vN, vS );
		vT = normalize(vT);
    }
}

float int_gauss_whole( vec3 i1, float lambda ) {
	return 2.0 * PI / lambda * ( 1.0 - pow( exp, -2.0 * lambda ) );
}

float int_solid_angle2( vec3 pos_a, vec3 pos_b, vec3 pos_c ) {
	float det;
	det = abs( dot( pos_a, cross( pos_b, pos_c ) ) );
	float len_a = length( pos_a );
	float len_b = length( pos_b );
	float len_c = length( pos_c );
	float div;
	div = len_a*len_b*len_c + dot( pos_a, pos_b )*len_c + dot( pos_a, pos_c )*len_b + dot( pos_b, pos_c )*len_a;
	float at;
	at = atan( det, div );
	if( at < 0.0 ) 
		at += PI;
	float sr;
	sr = 2.0 * at;
	return sr;
}

float int_gauss_tri2 ( vec3 pos_m, vec3 pos_p, vec3 pos_a, vec3 pos_b, vec3 pos_c,
	float lambda, vec3 i_c ) {
	
	vec3 i_m = pos_p - pos_m;
	float len = length( pos_p - pos_m );
	vec3 i_v = i_m / len;
	float U1 = int_solid_angle2( pos_a-pos_p, pos_b-pos_p, pos_c-pos_p );
	float lambda_v1 = 4.0*PI / U1;
	float c_v1 = 2.0;
	vec3 i_r;
	float c_r, lambda_r;
	product_srbf( i_c, lambda, i_v, lambda_v1, i_r, c_r, lambda_r );
	c_r = c_r * c_v1;
	return c_r * int_gauss_whole( i_r, lambda_r );
}



void main()
{
	//vec3 color = pointColor;
	vec3 i_c = normalize(lightDirection);
	vec3 o = normalize(viewPosition - pointLocation);
//	vec3 h = normalize(i + o);

//	float diffuse = max(dot(i, pointNormal), 0.0);
//	float specular = pow(max(dot(h, pointNormal), 0.0), 50.0);
	
//	vec3 finalColor = pointColor * diffuse + lightColor * specular;

	vec3 res, res1, res2, res3_1, res3_2, res4_1, res4_2;
	vec3 p1, p2, p3, pos_p, n2, vDir;
	pos_p = pointLocation;
	n2 = pointNormal;
	//vDir = normalize(viewPosition);
	vDir = o;
	//vec3 triangles[2000*3];
	vec2 txcoord;
	txcoord = vec2(gl_TexCoord[0]);
	//float iVertexX, iVertexY;
	//float stepVertexX, stepVertexY;
	//stepVertexX = 1.0/(float)resVertexX;
	//stepVertexY = 1.0/(float)resVertexY;
	//float iVertexX, iVertexY;
	//iVertexX = (txcoord.x-stepVerTexX/2.0)/stepVerTexX;
	//iVertexY = (txcoord.y-stepVerTexY/2.0)/stepVerTexY;
	float fCoordTexX, fCoordTexY;
	//fCoordTexX = iVertexX * stepTexX * tri_num + stepTexX / 2.0;
	//fCoordTexY = iVertexY * stepTexY + stepTexY / 2.0;
	
	float stepTexX, stepTexY;
	stepTexX = 1.0/resX;
	stepTexY = 1.0/resY;
	int indexX, indexY;
	//int startIndexX, startIndexY;
	//fCoordTexX = startIndexX * stepTexX + stepTexX / 2.0;
	//fCoordTexY = startIndexY * stepTexY * 3  + stepTexY / 2.0;
	
	
	for( int i = 0; i < tri_num; i++ ) {
		//p1 = triangles[3*i];
		//p2 = triangles[3*i+1];
		//p3 = triangles[3*i+2];
		indexX = mod( (startIndex+i), resX );
		indexY = floor( (startIndex+i) / resX ); 
		fCoordTexX = indexX * stepTexX + stepTexX / 2.0;
		fCoordTexY = indexY * stepTexY * 3  + stepTexY / 2.0;
		p1 = texture2D( shTexture, vec2( fCoordTexX, fCoordTexY ) ).xyz;
		p2 = texture2D( shTexture, vec2( fCoordTexX, fCoordTexY + stepTexY ) ).xyz;
		p3 = texture2D( shTexture, vec2( fCoordTexX, fCoordTexY + stepTexY*2) ).xyz;
		
		
		vec3 center_tri = (p1+p2+p3)/3;
		vec3 aver_r;
		aver_r = pos_p - center_tri;
		aver_r = normalize(aver_r);
		vec3 n1;
		vec3 tmp1, tmp2;
		tmp1 = p2-p1;
		tmp2 = p3-p1;
		n1 = cross( tmp1, tmp2 );
		n1 = normalize(n1);
		vec3 s1, t1;
		getSTDir(n1, s1, t1);
		
		float brdf_kd_1 = brdfs[0].x;//brdfs[0].x;
		float brdf_ks_1 = brdfs[0].y;
    
		vec3 brdf_h_1;
		brdf_h_1 = brdfs[1];
		float brdf_lambda_1 = brdfs[0].z;
		
		vec3 h_1;
		h_1 = brdf_h_1.x * s1 + brdf_h_1.y * t1 + brdf_h_1.z* n1;

		vec3 i_c_r;
		i_c_r = 2.0 * dot(i_c, h_1) * h_1 - i_c;        

		float lambda_m = (lambda+(brdf_lambda_1/4.0) );
		float i_m_sqrt = 1.0 - lambda*(brdf_lambda_1/4.0)/pow(lambda_m,2.0) * ( 1.0 - dot(i_c_r, aver_r) );
    
		float f1_n1_r = 2.0*PI*brdf_ks_1*dot(i_c, n1)/(lambda_m * i_m_sqrt);
		float g1_ic_dot_n1 = dot( i_c, n1 ) * (2.0*PI*brdf_kd_1/lambda) * ( 1.0 - pow(exp, -2.0 * lambda));

		float lambda_1_pie = lambda*(brdf_lambda_1/4.0) / (lambda+(brdf_lambda_1/4.0) );
    
		float kd_2 = self_brdf[0].x;//self_brdf[0].x
		float ks_2 = self_brdf[0].y;
		vec3 brdf_h_2;
		brdf_h_2 = self_brdf[1];
		float brdf_lambda_2 = self_brdf[0].z;
		vec3 s2, t2;
		getSTDir(n2, s2, t2);
		vec3 h_2;
		h_2 = brdf_h_2.x* s2 + brdf_h_2.y * t2 + brdf_h_2.z* n2;
    
		float coeff_for_cos = 1.170;
		float lambda_for_cos = 2.133;
		
		res1 = g1_ic_dot_n1 * kd_2 * coeff_for_cos * int_gauss_tri2( center_tri, pos_p, p1, p2, p3, lambda_for_cos, -n2 ) * pointColor;
		res2 = g1_ic_dot_n1 * ks_2 *  dot( aver_r, -n2 ) * 
			int_gauss_tri2( center_tri, pos_p, p1, p2, p3, brdf_lambda_2/4.0,vDir - 2.0* max( dot( vDir, h_2), 0.0 )*h_2 );
		res3_1 = kd_2 * f1_n1_r * dot( aver_r, -n2) * 
			int_gauss_tri2( center_tri, pos_p, p1, p2, p3, lambda_1_pie,i_c_r ) * pointColor;
		res3_2 = kd_2 * f1_n1_r * dot( aver_r, -n2) * pow( exp, -2.0*lambda_m) * 
			int_gauss_tri2( center_tri, pos_p, p1, p2, p3, -lambda_1_pie,i_c_r ) * pointColor;
		vec3 i_comb_1, i_comb_2;
		float lambda_comb_1, lambda_comb_2, c_comb_1, c_comb_2;
		product_srbf(i_c_r,lambda_1_pie,vDir - 2.0 * max( dot( vDir, h_2), 0.0 )*h_2,brdf_lambda_2/4.0, 
			i_comb_1, c_comb_1, lambda_comb_1 );
		product_srbf(i_c_r,-lambda_1_pie,vDir - 2.0 * max( dot( vDir, h_2), 0.0 )*h_2,brdf_lambda_2/4.0, 
			i_comb_2, c_comb_2, lambda_comb_2 );
		res4_1 = f1_n1_r * ks_2 * dot(aver_r,-n2) * c_comb_1 * 
			int_gauss_tri2( center_tri, pos_p, p1, p2, p3, lambda_comb_1, i_comb_1 );
		res4_2 = f1_n1_r * ks_2 * dot(aver_r, -n2) * pow(exp, -2.0 * lambda_m) * c_comb_2 *
			int_gauss_tri2( center_tri, pos_p, p1, p2, p3, lambda_comb_2, i_comb_2 ); 
		
    
		res = res + res1 + res2 + (res3_1 - res3_2) + (res4_1 - res4_2);
		//color = vec3( p1 );
	}
	vec3 color = vec3((res)*s);
	//vec3 color = vec3( 1.0, 0.0, 0.0 );
	gl_FragColor = vec4(color, 1.0);
	
//	if (pointLocation == vec3(0.0, 0.0, 0.0))
//		gl_FragColor = vec4(1.0, 0.0, 0.0, 0.0);
}