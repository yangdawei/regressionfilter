#version 130
#extension GL_ARB_gpu_shader5 : enable

uniform float objectScale;
uniform vec3 objectTranslation;
uniform mat4 objectRotation;

out vec3 pointLocation;
out vec3 pointNormal;

void main()
{
	gl_Position = gl_ModelViewProjectionMatrix * gl_Vertex;
	pointLocation = vec3(objectRotation * gl_Vertex);
	pointLocation = pointLocation * objectScale;
	pointLocation = pointLocation + objectTranslation;
	
    pointNormal = normalize(vec3(objectRotation * vec4(gl_Normal, 1.0)));
	
	gl_TexCoord[0] = gl_MultiTexCoord0;
}
