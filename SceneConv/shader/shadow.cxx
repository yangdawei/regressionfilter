#version 130
#extension GL_EXT_gpu_shader4 : require

in vec3 pointLocation;
in vec3 pointNormal;
in vec3 spotlightDir;

uniform vec3 viewPosition;
uniform vec3 pointColor;

//Shadows
uniform samplerCubeShadow shadow;
uniform mat4x4 light_view_matrix, light_projection_matrix;

uniform vec3 spotlightColor;

void main()
{
	vec3 finalColor = vec3(0.0, 0.0, 0.0);
	vec3 o = normalize(viewPosition - pointLocation);
	vec3 i = normalize(spotlightDir);
	vec3 h = normalize(i + o);
	float diffuse = max(dot(i, pointNormal), 0.0);
	float specular = pow(max(dot(h, pointNormal), 0.0), 2.0);

	finalColor += spotlightColor * diffuse;// + spotlightColor * specular;

	//Shadow test
	vec4 position_cs = vec4(pointLocation, 1.0);
	vec4 position_ls = light_view_matrix * position_cs;
	vec4 abs_position = abs(position_ls);
	float fs_z = -max(abs_position.x, max(abs_position.y, abs_position.z));
	vec4 clip = light_projection_matrix * vec4(0.0, 0.0, fs_z, 1.0);
	float depth = (clip.z / clip.w) * 0.5 + 0.5;
	vec4 result = shadowCube(shadow, vec4(position_ls.xyz, depth-0.005/**tan(acos(dot(pointNormal, spotlightDir[0])))*/));

	gl_FragColor = vec4(finalColor * result.xyz, 1.0);
}	