#version 130
#extension GL_ARB_gpu_shader5 : enable

uniform vec3 lightOriginalDirection;
uniform vec3 viewOriginalPosition;
uniform mat4 lightTransformMatrix;
uniform mat4 viewTransformMatrix;
uniform mat4 objectRotationMatrix;

out vec3 pointLocation;
out vec3 pointNormal;
out vec3 viewPosition;
out vec3 lightDirection;

void main()
{
	gl_Position = gl_ModelViewProjectionMatrix * gl_Vertex;
	pointLocation = vec3(objectRotationMatrix * gl_Vertex);
    pointNormal = normalize(vec3(objectRotationMatrix * vec4(gl_Normal, 1.0)));
	//viewPosition = vec3(inverse(viewTransformMatrix) * vec4(viewOriginalPosition, 1.0));
	viewPosition = viewOriginalPosition;
	lightDirection = vec3(lightTransformMatrix * vec4(lightOriginalDirection, 1.0));
	gl_TexCoord[0] = gl_MultiTexCoord0;
//	pointLocation = vec3(0.0, 0.0, 0.0);
//	gl_Position = gl_ModelViewProjectionMatrix * gl_Vertex;
//	pointLocation = vec3(gl_Vertex);
}
