#include "jbu.h"

float gaussian(float x, float sigma)
{
	return exp(-0.5 * x * x / (sigma * sigma));
}

template <class T>
T length2(vec2<T> v)
{
	return sqrtf(v.x * v.x + v.y * v.y);
}

template <class T>
T length3(vec4<T> v)
{
	return sqrtf(v.x * v.x + v.y * v.y + v.z * v.z);
}



vec4f bilinear_sample(const ImageVec4f &solution_small, vec2f q_)
{
	float intpart, fracpart;
	fracpart = modff(q_.s, &intpart);
	unsigned i = intpart;
	float a = fracpart;

	fracpart = modff(q_.t, &intpart);
	unsigned j = intpart;
	float b = fracpart;

	unsigned i_1 = min(solution_small.height() - 1, i + 1);
	unsigned j_1 = min(solution_small.width() - 1, j + 1);
	vec4f f00 = solution_small.get(j, i);
	vec4f f01 = solution_small.get(j_1, i);
	vec4f f10 = solution_small.get(j, i_1);
	vec4f f11 = solution_small.get(j_1, i_1);

	return f00 * (1 - a) * (1 - b)
		+ f01 * (1 - a) * b
		+ f10 * a * (1 - b)
		+ f11 * a * b;
}


void joint_bilateral_upsampling(const ImageVec4f &solution_small,
								const ImageVec4f &guide_large, ImageVec4f &solution_large, int win_size)
{
	const double sigma_d = 0.5;
	const double sigma_r = 0.1;

	unsigned sw = solution_small.width();
	unsigned sh = solution_small.height();
	solution_large.resize(guide_large.width(), guide_large.height());
	float scale = solution_small.width() / (float) guide_large.width();

	int half_win = win_size / 2.0 / scale;
	printf("half_win = %d\n", half_win);

	for (size_t i = 0; i < solution_large.height(); i++)
	{
		printf("%lu/%lu\n", i, solution_large.height());
		for (size_t j = 0; j < solution_large.width(); j++)
		{
			float kp;
			vec2i p(i, j);
			vec2f p_(i * scale, j * scale);

			// Calculate kp
			kp = 0.0;
			for (int qi = -half_win; qi <= half_win; qi++)
				for (int qj = -half_win; qj <= half_win; qj++)
				{
					vec2i q(i + qi, j + qj);
					vec2f q_ = vec2f(q.s * scale, q.t * scale);
					if (q_.s >= 0 && q_.s < sh && q_.t >= 0 && q_.t < sw)
					{
						kp += gaussian(length2(p_ - q_), sigma_d)
							* gaussian(length3(guide_large.get(j, i) - guide_large.get(j+qj, i+qi)), sigma_r);
					}
				}

				// Calculate Sp
				vec4f &sp = solution_large.get(j, i);
				sp = vec4f(0, 0, 0, 0);
				for (int qi = -half_win; qi <= half_win; qi++)
					for (int qj = -half_win; qj <= half_win; qj++)
					{
						vec2i q(i + qi, j + qj);
						vec2f q_ = vec2f(q.s * scale, q.t * scale);
						if (q_.s >= 0 && q_.s < sh && q_.t >= 0 && q_.t < sw)
						{
							sp += bilinear_sample(solution_small, q_)
								* gaussian(length2(p_ - q_), sigma_d)
								* gaussian(length3(guide_large.get(j, i) - guide_large.get(j+qj, i+qi)), sigma_r);
						}
					}
					sp /= kp;
		}
	}
}