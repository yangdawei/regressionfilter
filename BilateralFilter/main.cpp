#define _CRT_SECURE_NO_WARNINGS

#include <cstdlib>
#include <cmath>
#include <cstdio>

#include "ImageVec4f.h"
#include "helper.h"
#include "tests.h"

using namespace std;


/*
void bilinear_resize(const ImageVec4f &input, ImageVec4f &output)
{
	float rscale = input.height() / (float) output.height();
	float cscale = input.width() / (float) output.width();
	printf("%f %f\n", rscale, cscale);

	for (size_t i = 0; i < output.height(); i++)
	{
		for (size_t j = 0; j < output.width(); j++)
		{
			output.get(j, i) = bilinear_sample(input, vec2f(i * rscale, j * cscale));
		}
	}
}
*/

template <typename T> T dot3(vec4<T> left, vec4<T> right)
{
	return left.x * right.x + left.y * right.y + left.z * right.z;
}

void calc_error()
{
	puts("===========MSE=========");
	puts("NN\t\tJBU\t\tBicubic");
	for (int i = 1; i <= 3; i++)
	{
		ImageVec4f origin, nn_result, jbu_result, bic_result;
		char buffer[1024];
		sprintf(buffer, "../Trainer/data/scene%d_large/screenshot.raw", i);
		read_image_raw(buffer, origin);
		sprintf(buffer, "../Outputs/jbu_result_%d_large.raw", i);
		read_image_raw(buffer, jbu_result);
		sprintf(buffer, "../Outputs/result%d_large.raw", i);
		read_image_raw(buffer, nn_result);
		sprintf(buffer, "../Outputs/bicubic_%d.raw", i);
		read_image_raw(buffer, bic_result);

		float sum_nn = 0, sum_jbu = 0, sum_bic;
		const int offset = 10;
		for (size_t i = offset; i <= origin.height()-offset; i++)
		{
			for (size_t j = offset; j <= origin.width()-offset; j++)
			{
				vec4f error_nn = origin.get(j, i) - nn_result.get(j-offset, i-offset);
				vec4f error_jbu = origin.get(j, i) - jbu_result.get(j, i);
				vec4f error_bic = origin.get(j, i) - bic_result.get(j, i);
				sum_nn += dot3(error_nn, error_nn);
				sum_jbu += dot3(error_jbu, error_jbu);
				sum_bic += dot3(error_bic, error_bic);
			}
		}
		size_t total = origin.height() * origin.width() * 3;
		sum_nn = sqrt(sum_nn / total);
		sum_jbu = sqrt(sum_jbu / total);
		sum_bic = sqrt(sum_bic / total);
		printf("%.8f\t%.8f\t%.8f\n", sum_nn, sum_jbu, sum_bic);
	}
}


int main(int argc, char *argv[])
{
	// test_antialising();
	// test_tonemapping();
	calc_error();
	return 0;
}