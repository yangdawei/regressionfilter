#define _CRT_SECURE_NO_WARNINGS

#include "tests.h"
#include "ImageVec4f.h"
#include "helper.h"
#include "jbu.h"

void test_tonemapping()
{
	ImageVec4f origin_small, origin_large, tm_small, tm_large, solt_small, solt_large;
	read_image_raw("../Images/env2.raw", origin_small);
	read_image_raw("../Images/env.raw", origin_large);
	read_image_raw("../Images/env_tm2.raw", tm_small);
	// read_image_raw("../Images/env_tm.raw", tm_large);
	tm_large.resize(origin_large.width(), origin_large.height());

	solt_small.resize(origin_small.width(), origin_small.height());
	solt_large.resize(origin_large.width(), origin_large.height());

	for (size_t i = 0; i < solt_small.height(); i++)
		for (size_t j = 0; j < solt_small.width(); j++)
		{
			solt_small.get(j, i) = tm_small.get(j, i) / origin_small.get(j, i);
			// solt_large.get(j, i) = tm_large.get(j, i) / origin_large.get(j, i);
		}
	joint_bilateral_upsampling(solt_small, origin_large, solt_large, 5);
	for (size_t i = 0; i < solt_large.height(); i++)
		for (size_t j = 0; j < solt_large.width(); j++)
			tm_large.get(j, i) = origin_large.get(j, i) * solt_large.get(j, i);

	tm_large.savePFM("../Outputs/env_tm_jbu.pfm");
}


void test_antialising()
{
	ImageVec4f image_small, image_large, normal_large;

	char buf_small[4096];
	char buf_large[4096];
	char buf_shrink[4096];
	for (int i = 1; i < 4; i++)
	{
		sprintf(buf_small, "../RendererCUDA/pfm/scene%d/screenshot.raw", i);
		sprintf(buf_large, "../RendererCUDA/pfm/scene%d_large/normal.raw", i);
		sprintf(buf_shrink, "../Outputs/jbu_result_%d_large.raw", i);
		read_image_raw(buf_small, image_small);
		read_image_raw(buf_large, normal_large);
		joint_bilateral_upsampling(image_small, normal_large, image_large, 5);

		/*
		ImageVec4f image_shrink(image_small.width(), image_small.height());
		for (size_t i = 0; i < image_shrink.height(); i++)
		{
			for (size_t j = 0; j < image_shrink.width(); j++)
			{
				vec4f sum = vec4f(0, 0, 0, 0);
				for (size_t ii = 0; ii < 4; ii++)
					for (size_t jj = 0; jj < 4; jj++)
						sum += image_large.get(4 * j + jj, 4 * i + ii);
				image_shrink.get(j, i) = sum / 16.0;
			}
		} */
		save_image_raw(buf_shrink, image_large);
	}
}


void test_bilinear()
{
	ImageVec4f image_small;
	image_small.loadPFM("../RendererCUDA/pfm/screenshot1.pfm");
	ImageVec4f image_large;
	image_large.resize(image_small.width() * 4, image_small.height() * 4);
	for (size_t i = 0; i < image_large.height(); i++)
		for (size_t j = 0; j < image_large.width(); j++)
			image_large.get(j, i) = bilinear_sample(image_small, vec2f(i / 4.0, j / 4.0));

	image_large.savePFM("Large.pfm");
}
