load net
image = pfm_read('data/scene/screenshot.pfm');
normal = pfm_read('data/scene_large/normal.pfm');
image1 = pfm_read('data/scene1/screenshot.pfm');
normal1 = pfm_read('data/scene1_large/normal.pfm');
image2 = pfm_read('data/scene2/screenshot.pfm');
normal2 = pfm_read('data/scene2_large/normal.pfm');
image3 = pfm_read('data/scene3/screenshot.pfm');
normal3 = pfm_read('data/scene3_large/normal.pfm');

result = nn_eval(net, image, normal);
result1 = nn_eval(net, image1, normal1);
result2 = nn_eval(net, image2, normal2);
result3 = nn_eval(net, image3, normal3);

% compare1 = imresize(pfm_read('data/scene1_large/screenshot.pfm'), 0.25);
% compare2 = imresize(pfm_read('data/scene2_large/screenshot.pfm'), 0.25);
% compare3 = imresize(pfm_read('data/scene3_large/screenshot.pfm'), 0.25);
compare1 = pfm_read('data/scene1_large/screenshot.pfm');
compare2 = pfm_read('data/scene2_large/screenshot.pfm');
compare3 = pfm_read('data/scene3_large/screenshot.pfm');

imwrite(result1, 'result1_large.png');
imwrite(result2, 'result2_large.png');
imwrite(result3, 'result3_large.png');
imwrite(compare1, 'groundtruth1_large.png');
imwrite(compare2, 'groundtruth2_large.png');
imwrite(compare3, 'groundtruth3_large.png');