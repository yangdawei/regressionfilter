function [input_data, output_data] = gen_data(source_small_name, source_large_name, addition_large_name)
    source_small = pfm_read(source_small_name);
    source_large = pfm_read(source_large_name);
    addition_large = pfm_read(addition_large_name);

    [h_large, w_large, ~] = size(source_large);

    source_small = imresize(source_small, [h_large, w_large]);
    % Now the sizes of source_small and source_large are the same.

    input_data = source_small;
    output_data = source_large;

    % Show antialised
    % imshow(input_data - output_data);
    % figure;
    % imshow(input_data);
    % figure;
    % imshow(output_data);

    win_size = 5;

    % generate train data
    sample_size = (h_large - win_size) * (w_large - win_size);
    patch_size = win_size * win_size * 3;
    half_win = floor(win_size / 2);

    input_data = zeros(patch_size * 2, sample_size);
    output_data = zeros(3, sample_size);
    count = 1;

    for i = 1:h_large - win_size + 1
        for j = 1:w_large - win_size + 1
            input_data(1:patch_size, count) = reshape(source_small(i:i+win_size-1, j:j+win_size-1, :), patch_size, 1);
            input_data(patch_size+1:end, count) = reshape(addition_large(i:i+win_size-1, j:j+win_size-1, :), patch_size, 1);
            output_data(:, count) = source_large(i + half_win, j+half_win, :);
            count = count + 1;
        end
    end
end
