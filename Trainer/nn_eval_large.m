function [result] = nn_eval_large(net, image, normal, scale)
    % 'scale' must be an integer.
    [h, w, ~] = size(image);

    % Scale the image
    image = imresize(image, scale);
    normal = imresize(normal, scale);
    
    win_size = 5;
    % generate train data
    newh = scale * (h-win_size) + 1;
    neww = scale * (w-win_size) + 1;
    sample_size = newh * neww;
    patch_size = win_size * win_size * 3;
    % half_win = floor(win_size / 2);

    input_data = zeros(patch_size * 2, sample_size);
    count = 1;

    for i = 1:newh
        for j = 1:neww
            input_data(1:patch_size, count) = reshape(image(i:scale:i+scale*win_size-1, j:scale:j+scale*win_size-1, :), patch_size, 1);
            input_data(patch_size+1:end, count) = reshape(normal(i:scale:i+scale*win_size-1, j:scale:j+scale*win_size-1, :), patch_size, 1);
            count = count + 1;
        end
    end
    output_data = net(input_data);
    data = reshape(output_data, 3, neww, newh);
    result = permute(data, [3, 2, 1]);
end