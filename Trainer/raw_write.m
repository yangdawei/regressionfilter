function [] = raw_write(filename, mat)
%RAW_WRITE Summary of this function goes here
%   Detailed explanation goes here

	fp = fopen(filename, 'wb');
	[h, w, ~] = size(mat);
	fwrite(fp, w, 'uint32');
	fwrite(fp, h, 'uint32');
	mat = cat(3, mat, zeros(h, w));
	fwrite(fp, permute(mat, [3, 2, 1]), 'float32');
	fclose(fp);
end
