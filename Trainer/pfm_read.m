function [data] = pfm_read(file_name)
    %% Warning: only support color images.
    %%

    % Read header
    fid = fopen(file_name, 'r');
    fscanf(fid, 'PF');
    result = fscanf(fid, '%d %d\n-1\n');
    w = result(1);
    h = result(2);
    pos = ftell(fid);
    fclose(fid);

    % Read raw data
    fid = fopen(file_name, 'rb');
    fseek(fid, pos, 'bof');
    data = fread(fid, h * w * 3, 'float32');
    data = reshape(data, 3, w, h);
    data = permute(data, [3, 2, 1]);

    fclose(fid);
end