function [] = pfm_write(filename, mat)
%PFM_WRITE Summary of this function goes here
%   Detailed explanation goes here

fp = fopen(filename, 'w');
[h, w, ~] = size(mat);
fprintf(fp, 'PF\n%d %d\n-1\n', w, h);
fclose(fp);
fp = fopen(filename, 'ab');
fwrite(fp, permute(mat, [3, 2, 1]), 'float32');
fclose(fp);
end

