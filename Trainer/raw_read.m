function [data] = raw_read(file_name)
    %% Warning: only support color images.
    %%
	fid = fopen(file_name, 'rb');
	w = fread(fid, 1, 'uint32');
	h = fread(fid, 1, 'uint32');
	data = fread(fid, h * w * 4, 'float32');
	data = reshape(data, 4, w, h);
	data = permute(data, [3, 2, 1]);
	data = data(:, :, 1:3);
end
