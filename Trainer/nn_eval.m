function [result] = nn_eval(net, image, normal)
    image = imresize(image, size(normal));
    [h, w, ~] = size(image);
    
    win_size = 5;
    % generate train data
    sample_size = (h - win_size + 1) * (w - win_size + 1);
    patch_size = win_size * win_size * 3;
    % half_win = floor(win_size / 2);

    input_data = zeros(patch_size * 2, sample_size);
    count = 1;

    for i = 1:h - win_size + 1
        for j = 1:w - win_size + 1
            input_data(1:patch_size, count) = reshape(image(i:i+win_size-1, j:j+win_size-1, :), patch_size, 1);
            input_data(patch_size+1:end, count) = reshape(normal(i:i+win_size-1, j:j+win_size-1, :), patch_size, 1);
            count = count + 1;
        end
    end
    output_data = net(input_data);
    data = reshape(output_data, 3, w-win_size+1, h-win_size+1);
    result = permute(data, [3, 2, 1]);
end