all_original_names = {'big', 'env', 'reno'};

for i=1:length(all_original_names)
    hdr = hdrread(['../Images/' all_original_names{i} '.hdr']);

    % Write original PFM
    pfm_write(['../Images/' all_original_names{i} '.pfm'], hdr);
    raw_write(['../Images/' all_original_names{i} '.raw'], hdr);

    % Write shrinked PFM
    shrinked = imresize(hdr, 0.25);
    pfm_write(['../Images/' all_original_names{i} '2.pfm'], shrinked);
    raw_write(['../Images/' all_original_names{i} '2.raw'], shrinked);

    rgb = tonemap(hdr, 'AdjustLightness', [0.1, 1], 'AdjustSaturation', 1.5);
    rgb = double(rgb) / 255;

    % Write tonemapped PFM
    pfm_write(['../Images/' all_original_names{i} '_tm.pfm'], rgb);
    raw_write(['../Images/' all_original_names{i} '_tm.raw'], rgb);

    % Write tonemapped shrinked PFM
    pfm_write(['../Images/' all_original_names{i} '_tm2.pfm'], imresize(rgb, 0.25));
    raw_write(['../Images/' all_original_names{i} '_tm2.raw'], imresize(rgb, 0.25));
end
