#pragma once

#include "headers.hh"

SharedArray< float > ScaleLuminance(
	SharedArray< float > const image,
	SharedArray< float > const luminance,
	unsigned int const rows,
	unsigned int const columns,
	double const saturation
	);

SharedArray< float > FindLuminance(
	SharedArray< float > const image,
	unsigned int const rows,
	unsigned int const columns
	);