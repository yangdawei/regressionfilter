#pragma once

#include "ImageVec4f.h"

void save_image_raw(const char *filename, const ImageVec4f &image);

void read_image_raw(const char *filename, ImageVec4f &image);
