#include <iostream>
#include <vector>

#include "squish_luminance.hh"
#include "hdr_squish.hh"
#include "helper.h"
#include "ImageVec4f.h"

using namespace std;

SharedArray<float> read_raw(const char *filename, unsigned &rows, unsigned &cols)
{
	ImageVec4f image;
	read_image_raw(filename, image);
	rows = image.height();
	cols = image.width();
	SharedArray<float> ret(3 * rows * cols);
	size_t index = 0;
	for (size_t i = 0; i < rows; i++)
	{
		for (size_t j = 0; j < cols; j++)
		{
			vec4f elem = image.get(j, i);
			ret[index+0] = elem.x;
			ret[index+1] = elem.y;
			ret[index+2] = elem.z;
			index += 3;
		}
	}
	return ret;
}

void save_pfm(const char *filename, SharedArray<float> data, unsigned rows, unsigned cols)
{
	ImageVec4f image(cols, rows);
	size_t index = 0;
	for (size_t i = 0; i < rows; i++)
	{
		for (size_t j = 0; j < cols; j++)
		{
			image.get(j, i) = vec4f(data[index], data[index+1], data[index+2]);
			index += 3;
		}
	}
	image.savePFM(filename);
}

void save_gray_pfm(const char *filename, SharedArray<float> data, unsigned rows, unsigned cols)
{
	ImageVec4f image(cols, rows);
	size_t index = 0;
	for (size_t i = 0; i < rows; i++)
	{
		for (size_t j = 0; j < cols; j++)
		{
			image.get(j, i) = vec4f(data[index], data[index], data[index]);
			index++;
		}
	}
	image.savePFM(filename);
}

int main(int argc, char *argv[])
{
	double alpha, beta, delta, theta, epsilon, saturation;
	//if ( std::isnan( alpha ) )
		alpha = 0.1;
	//if ( std::isnan( beta ) )
		beta = 0.1;
	//if ( std::isnan( delta ) )
		delta = 1.1;
	//if ( std::isnan( theta ) )
		theta = 0;
	//if ( std::isnan( epsilon ) )
		epsilon = 0.0001;
	//if ( std::isnan( saturation ) )
		saturation = 1;

	const char *input = "../Images/reno.raw";
	const char *output = "../Outputs/reno.pfm";

	std::cout << "Loading HDR image" << std::endl;
	unsigned int rows, columns;
	SharedArray< float > image = read_raw(input, rows, columns);
	save_pfm("image.pfm", image, rows, columns);

	std::cout << "Finding luminance" << std::endl;
	SharedArray< float > const luminance = FindLuminance(image, rows, columns);
	save_gray_pfm("luminance.pfm", luminance, rows, columns);

	std::cout << "Performing HDR luminance squishing" << std::endl;
	SquishLuminance(luminance, rows, columns, epsilon, alpha, beta, delta, theta);
	save_gray_pfm("new_luminance.pfm", luminance, rows, columns);

	std::cout << "Scaling luminance" << std::endl;
	SharedArray< float > const result = ScaleLuminance(image, luminance, rows, columns, saturation);

	std::cout << "Saving HDR image to " << output << std::endl;
	// save_raw(output, result, rows, columns);
	save_pfm(output, result, rows, columns);

	return 0;
}