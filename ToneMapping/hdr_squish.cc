/*
	Copyright (C) 2010  Andrew Cotter

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


/**
	\file hdr_squish.cc
*/




#include "headers.hh"




//============================================================================
//    FindLuminance function
//============================================================================


SharedArray< float > FindLuminance(
	SharedArray< float > const image,
	unsigned int const rows,
	unsigned int const columns
)
{
	SharedArray< float > result( rows * columns );

	#pragma omp parallel for schedule( static )
	for ( int ii = 0; ii < static_cast< int >( rows * columns ); ++ii ) {

		float const red   = image[ ii * 3 + 0 ];
		float const green = image[ ii * 3 + 1 ];
		float const blue  = image[ ii * 3 + 2 ];

		double const intensity = std::max( red + green + blue, 3.0f / 65536 ) / 3.0;

		result[ ii ] = std::log( intensity );
	}

	return result;
}




//============================================================================
//    ScaleLuminance function
//============================================================================


SharedArray< float > ScaleLuminance(
	SharedArray< float > const image,
	SharedArray< float > const luminance,
	unsigned int const rows,
	unsigned int const columns,
	double const saturation
)
{
	if ( saturation <= 0 )
		throw std::runtime_error( "saturation must be positive" );

	SharedArray< float > result( rows * columns * 3 );

	float maximum = -std::numeric_limits< float >::infinity();
	#pragma omp parallel
	{	float localMaximum = -std::numeric_limits< float >::infinity();

		#pragma omp for schedule( static )
		for ( int ii = 0; ii < static_cast< int >( rows * columns ); ++ii ) {

			float const red   = image[ ii * 3 + 0 ];
			float const green = image[ ii * 3 + 1 ];
			float const blue  = image[ ii * 3 + 2 ];

			double const intensity = std::max( red + green + blue, 3.0f / 65536 ) / 3.0;

			result[ ii * 3 + 0 ] = saturation * std::log( std::max( red,   1.0f / 65536 ) / intensity ) + luminance[ ii ];
			result[ ii * 3 + 1 ] = saturation * std::log( std::max( green, 1.0f / 65536 ) / intensity ) + luminance[ ii ];
			result[ ii * 3 + 2 ] = saturation * std::log( std::max( blue,  1.0f / 65536 ) / intensity ) + luminance[ ii ];

			localMaximum = std::max( localMaximum, result[ ii * 3 + 0 ] );
			localMaximum = std::max( localMaximum, result[ ii * 3 + 1 ] );
			localMaximum = std::max( localMaximum, result[ ii * 3 + 2 ] );
		}

		#pragma omp critical
		maximum = std::max( maximum, localMaximum );
	}

	#pragma omp parallel for schedule( static )
	for ( int ii = 0; ii < static_cast< int >( rows * columns * 3 ); ++ii )
		result[ ii ] = std::exp( result[ ii ] - maximum );

	return result;
}
