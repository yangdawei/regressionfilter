#pragma once
#include <vector>
#include <cassert>
#include "nvVector.h"

using namespace std;
using namespace nv;

class ImageVec4f
{
protected:
	vector<vec4f> imageData;
	unsigned w,h;
public:
	const void* getData() const { return imageData.data(); }
	void *getData() { return imageData.data(); }
	ImageVec4f(){}
	ImageVec4f(unsigned w, unsigned h);
	void resize(unsigned w, unsigned h);
	unsigned width() const { return w; }
	unsigned height() const { return h; }
	vec4f &ImageVec4f::get(unsigned x, unsigned y)
	{
		if(!(x<w && y<h))
		{
			assert(false);
			return imageData[(y*w+x) % (w * h)];
		}
		return imageData[y*w+x];
	}
	vec4f get(unsigned x, unsigned y) const
	{
		if(!(x<w && y<h))
		{
			assert(false);
			return imageData[(y*w+x) % (w * h)];
		}
		return imageData[y*w+x];
	}
	void set(unsigned x, unsigned y, vec4f val);
	void savePFM(const std::string &fileName);
	void loadPFM(const std::string &fileName);
};

