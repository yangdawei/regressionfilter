#include "StdAfx.h"
#include "Renderer.h"

#include <cuda_runtime.h>
#include <device_launch_parameters.h>

void append(vector<vector<float>> &v1, const vector<vector<float>> &v2)
{
	for(unsigned i=0; i<v2.size(); i++)
		v1.push_back(v2[i]);
}

#ifdef INTP_DEBUG

void SimpleShapeEXT::transform(const vector2f &_mat, TexArray &_tex)
{
	unsigned height = _mat.size();
	unsigned maxLen = _mat[0].size();
	unsigned len = maxLen;
	unsigned width = maxLen%4==0 ? maxLen/4 : maxLen/4+1;

	float root = sqrtf(float(width * height));
	int const_K = int(root / width);
	int transWidth, transHeight;
	if (const_K > 0)
	{
		transWidth = width * const_K;
		transHeight = int(ceilf(height / float(const_K)));
	}
	else
	{
		transWidth = width;
		transHeight = height;
	}

	printf("Alloc: %dx%dx4=%d floats\n", transWidth, transHeight, transWidth * transHeight * 4);
	// pixels.resize(transWidth * transHeight*4);
	GLfloat *pixels = new GLfloat[transWidth * transHeight * 4];
	//printf("Alloc success!\n");
	for(unsigned i=0; i<height; i++)
	{
		pixels[i*width*4] = float(_mat[i].size());

		for(unsigned j=0; j<_mat[i].size(); j++)
		{
			pixels[i*width*4+j] = _mat[i][j];
		}
	}
	_tex.id.resize(transHeight);
	for (int i = 0; i < transHeight; i++)
		_tex.id[i].resize(transWidth);

	memcpy(_tex.id.data(), pixels, sizeof(float) * transWidth * transHeight * 4);

	delete[] pixels;
}
#endif

struct TexArray
{
	int w;
	int h;
	int k;
	GLfloat *id;
	vec4f getVec4(int ix, int iy)
	{
		int idx = iy * w / k + ix;
		int x = idx % w;
		int y = idx / w;
		return vec4f(id + 4 * (y * w + x));
	}
	int getId(int ix, int iy)
	{
		int idx = iy * w / k + ix;
		int x = idx % w;
		int y = idx / w;
		return (y*w + x);
	}
	float getFloat(int x, int y) { return getVec4(x / 4, y)[x % 4]; }
	int getInt(int x, int y)
	{
		return int(getFloat(x, y));
	}
};
inline void validate(GLfloat *data, int w, int h, int tw, int th, int const_K)
{
	TexArray test;
	test.id = data;
	test.w = tw;
	test.h = th;
	test.k = const_K;
	for (int i = 0; i < h; i++)
		for (int j = 0; j < w; j++)
			if (test.getFloat(j, i) != data[i * w * 4 + j])
			{
				printf("%d, %d\n", test.getId(j, i), i * w * 4 + j);
				system("pause");
			}
}

void Renderer::setTexArray(const string& progName, const string& varName, const vector<vector<float>>& tex, bool equalLen)
{
	assert(tex.size() > 0);
	GLuint texID = 0;
 	_CheckErrorGL
	glActiveTexture(GL_TEXTURE0+maxTexUnit);
	glGenTextures(1, &texID);
	texIDs.push_back(texID);
	glBindTexture(GL_TEXTURE_2D, texID);
	_CheckErrorGL
	
	printf("%u %u\n", tex.size(), tex[0].size());
	unsigned height = tex.size();
	unsigned maxLen = tex[0].size();
	unsigned len = maxLen;
	if(!equalLen)
	{
		for(unsigned i=0; i<tex.size(); i++)
		{
			maxLen = tex[i].size()>maxLen ? tex[i].size() : maxLen;
		}
	}
	maxLen = equalLen ? maxLen : maxLen+1;
	unsigned width = maxLen%4==0 ? maxLen/4 : maxLen/4+1;

	float root = sqrtf(float(width * height));
	//float root = 4096;
	int const_K = int(root / width);
	//const_K = -1;
	int transWidth, transHeight;
	if (const_K > 0)
	{
		transWidth = width * const_K;
		transHeight = int(ceilf(width * height /  (float) transWidth));
	}
	else
	{
		transWidth = width;
		transHeight = height;
		const_K = 1;
	}
	if (transWidth * transHeight  < width * height)
	{
		printf("Transformed matrix is smaller than origin matrix!\n");
		assert(false);
		system("pause");
	}
	printf("Origin:%dx%dx4=%d floats\n", width, height, width * height * 4);
	printf("Alloc: %dx%dx4=%d floats\n", transWidth, transHeight, transWidth * transHeight * 4);

	GLfloat *pixels = (GLfloat *)malloc(sizeof(GLfloat) * 4 * transWidth * transHeight);
	if (pixels == NULL)
	{
		puts("Alloc failed!");
		system("pause");
	}

	puts("Alloc successfully!\n");

	if (!equalLen)
	{
		for(unsigned i=0; i<height; i++)
		{
			pixels[i*width*4] = float(tex[i].size());
		
			for(unsigned j=1; j<tex[i].size()+1; j++)
			{
				pixels[i*width*4+j] = tex[i][j-1];
			}
		}
	}
	else
	{
		for (unsigned i = 0; i < tex.size(); i++)
		{
			for (unsigned j = 0; j < tex[i].size(); j++)
			{
				pixels[i*width*4+j] = tex[i][j];
			}
			//printf("%d/%d\n", cnt, 4 * height * width);
		}
	}
	validate(pixels, width, height, transWidth, transHeight, const_K);

	puts("Load to memory successfully!");

	_CheckErrorGL
		glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA32F, transWidth, transHeight, 0, GL_RGBA, GL_FLOAT, pixels);
	_CheckErrorGL
	//delete[] pixels;
	free(pixels);

	ostringstream oss;
	oss << varName << ".id";
	_CheckErrorGL
	shader.setUniform(progName, oss.str(), int(maxTexUnit));
	oss.str("");
	oss << varName << ".width";
	_CheckErrorGL
	shader.setUniform(progName, oss.str(), int(transWidth));
	_CheckErrorGL
	oss.str("");
	oss << varName << ".height";
	shader.setUniform(progName, oss.str(), int(transHeight));
	oss.str("");
	oss << varName << ".const_K";
	shader.setUniform(progName, oss.str(), int(const_K));
	maxTexUnit++;
	_CheckErrorGL
	
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAX_LEVEL, 0);
	_CheckErrorGL
	glActiveTexture(GL_TEXTURE0+maxTexUnit);
	_CheckErrorGL
}

void Renderer::releaseTextures()
{
	maxTexUnit = 0;
	glDeleteTextures(texIDs.size(), texIDs.data());
	texIDs.clear();
}

void Renderer::setShaderParams()
{
	releaseTextures();
	shader.setUniform("numLights", int(pointLightList.size()));
	for(size_t i=0; i<pointLightList.size(); i++)
	{
		ostringstream oss;
		oss << "lightList[" << i << "]" << ".position";
		shader.setUniform(oss.str(), pointLightList[i].position);
		oss.str("");
		oss << "lightList[" << i << "]" << ".color";
		shader.setUniform(oss.str(), pointLightList[i].color);
	}
	shader.setUniform("numObjects", int(shapeList.size()));
	for(size_t i=0; i<shapeList.size(); i++)
	{
		ostringstream oss;
		oss << "objTransMatList[" << i << "]";
		shader.setUniform(oss.str(), shapeList[i].getTransMat());
		oss.str("");

		oss << "objMaterialList[" << i << "]";
		string baseStr = oss.str();
		oss << ".diffColor";
		shader.setUniform(oss.str(), shapeList[i].getMaterial().diffColor);
		oss.str("");

		oss << baseStr << ".specColor";
		shader.setUniform(oss.str(), shapeList[i].getMaterial().specColor);
		oss.str("");
		oss << baseStr << ".kd";
		shader.setUniform(oss.str(), shapeList[i].getMaterial().kd);
		oss.str("");
		oss << baseStr << ".ks";
		shader.setUniform(oss.str(), shapeList[i].getMaterial().ks);
		oss.str("");
		oss << baseStr << ".alpha";
		shader.setUniform(oss.str(), shapeList[i].getMaterial().alpha);
	}
}