#include "StdAfx.h"
#include "SceneController.h"

void SceneController::Arrow::draw()
{
	if(Renderer::selectedShapeIndices.size()==0 && Renderer::selectedLightIndices.size()==0)
		return;
	glPushMatrix();
	vec3f dir = endPoint - startPoint;
	float len = dir.length();
	dir.normalize();
	float angle = acos(dir.dot(vec3f(0, 0, 1)));
	vec3f axis = vec3f(0, 0, 1).cross(dir);
	axis.normalize();
	matrix4<float> rm = rotMat(axis, angle);
	glPushMatrix();
	glTranslatef(startPoint.x, startPoint.y, startPoint.z);
	glMultMatrixf(transpose(rm).get_value());
	glutSolidCylinder(ARROW_BODY_RADIUS, ARROW_BODY_RATIO*len, 32, 1);
	glPopMatrix();
	
	glTranslatef(startPoint.x, startPoint.y, startPoint.z);
	glTranslatef(dir.x*ARROW_BODY_RATIO*len, dir.y*ARROW_BODY_RATIO*len, dir.z*ARROW_BODY_RATIO*len);
	glMultMatrixf(transpose(rm).get_value());
	glutSolidCone(ARROW_HEAD_RADIUS, (1-ARROW_BODY_RATIO)*len, 32, 1);
	glPopMatrix();
}

void SceneController::Circle::draw()
{
	if(Renderer::selectedShapeIndices.size()==0 && Renderer::selectedLightIndices.size()==0)
		return;
	glPushMatrix();
	float angle = acos(norm.dot(vec3f(0, 0, 1)));
	vec3f axis = vec3f(0, 0, 1).cross(norm);
	axis.normalize();
	matrix4<float> rm = rotMat(axis, angle);
	glTranslatef(center.x, center.y, center.z);
	glMultMatrixf(transpose(rm).get_value());
	glutSolidTorus(ROT_INNER_RADIUS, ROT_OUTER_RADIUS, 32, 32);
	glPopMatrix();
	Arrow arr;
	glColor4f(0.5, 0.5, 0.5, ROT_ALPHA);
	arr.startPoint = center;
	arr.endPoint = center + dragDir*ROT_OUTER_RADIUS;
	if(isDragging)
		arr.draw();
}

SceneController::Arrow* SceneController::getArrow(int id)
{
	switch(id)
	{
	case 0:
		return &arrow_x;
	case 1:
		return &arrow_y;
	case 2:
		return &arrow_z;
	}
	return NULL;
}

SceneController::Circle* SceneController::getCircle(int id)
{
	switch(id)
	{
	case 0:
		return &circle_xy;
	case 1:
		return &circle_yz;
	case 2:
		return &circle_xz;
	}
	return NULL;
}

void SceneController::draw()
{
	if(!centerReady)
		return;

	for(int i=0; i<3; i++)
	{
		if (Renderer::shader.getCurrentProgID() != 0)
			Renderer::shader.setUniform("controlID", i);
		glColor4f(getArrow(i)->color.x, getArrow(i)->color.y, getArrow(i)->color.z, getArrow(i)->color.w);
		getArrow(i)->startPoint = center;
		getArrow(i)->endPoint = center + vec3f(i==0, i==1, i==2)*AXIS_LEN;
		getArrow(i)->draw();
	}

	for(int i=0; i<3; i++)
	{
		if (Renderer::shader.getCurrentProgID() != 0)
			Renderer::shader.setUniform("controlID", i+3);
		glColor4f(getCircle(i)->color.x, getCircle(i)->color.y, getCircle(i)->color.z, getCircle(i)->color.w);
		getCircle(i)->center = center;
		getCircle(i)->norm = vec3f(i==1, i==2, i==0);
		if(dragControlID != i+3)
			getCircle(i)->isDragging = false;
		getCircle(i)->draw();
	}
}

void SceneController::handleMouse(int button, int state, int x, int y, int controlID)
{
	dragControlID = -1;
	if(state == GLUT_DOWN)
	{
		dragButton = button;
		dragStartPoint = vec2i(x, y);
		dragControlID = controlID;
	}
}

void SceneController::handlePassiveMotion(int x, int y, int controlID)
{
	arrow_x.color = vec4f(AXIS_X_COLOR, AXIS_ALPHA);
	arrow_y.color = vec4f(AXIS_Y_COLOR, AXIS_ALPHA);
	arrow_z.color = vec4f(AXIS_Z_COLOR, AXIS_ALPHA);
	circle_xy.color = vec4f(ROT_XY_COLOR, ROT_ALPHA);
	circle_yz.color = vec4f(ROT_YZ_COLOR, ROT_ALPHA);
	circle_xz.color = vec4f(ROT_XZ_COLOR, ROT_ALPHA);
	if(controlID<0)
		return;
	if(controlID<3)
	{
		getArrow(controlID)->color = vec4f(1, 1, 1, AXIS_ALPHA);
	}
	else
	{
		getCircle(controlID-3)->color = vec4f(1, 1, 1, ROT_ALPHA);
	}
	Renderer::render();
}

vec3f SceneController::getViewDir(int x, int y)
{
	vec3<double> dir;
	vec4<int> vp;
	glGetIntegerv(GL_VIEWPORT, vp);
	gluUnProject(x, vp[3]-1-y, 1, Renderer::viewMatd.get_value(), Renderer::projMatd.get_value(), vp, &dir.x, &dir.y, &dir.z);
	vec3f dirf = vec3f(dir.x, dir.y, dir.z);
	dirf.normalize();
	return dirf;
}

float SceneController::getDistAloneAxis(const vec3f& center, const vec3f& axis, const vec3f& eyePos, const vec3f& viewDir)
{
	float d = axis.dot(viewDir);
	d = d>0.999 ? 0.999 : d < -0.999 ? -0.999 : d;
	return (eyePos-center).dot((axis+viewDir)/(1+d)+(axis-viewDir)/(1-d))*0.5;
}

void SceneController::updateCenter()
{
	centerReady = false;
	vec4f center4(0, 0, 0, 0);
	for(set<unsigned>::iterator it=Renderer::selectedShapeIndices.begin(); it!=Renderer::selectedShapeIndices.end(); it++)
	{
		int idx = *it;
		vec4f sc = vec4f(Renderer::shapeList[idx].getCenter(), 1);
		center4 += (Renderer::shapeList[idx].getTransMat()*sc);
	}
	for(set<unsigned>::iterator it=Renderer::selectedLightIndices.begin(); it!=Renderer::selectedLightIndices.end(); it++)
	{
		center4 += vec4f(Renderer::pointLightList[*it].position, 1);
	}
	center = vec3f(center4.x, center4.y, center4.z)/(Renderer::selectedShapeIndices.size()+Renderer::selectedLightIndices.size());
	centerReady = true;
}

void SceneController::handleActiveMotion(int x, int y, int controlID)
{
	if(dragControlID<0)
		return;
	vec3f startViewDir = getViewDir(dragStartPoint.x, dragStartPoint.y);
	vec3f viewDir = getViewDir(x, y);
	
	updateCenter();

	float dragDist[3];
	vec3f endDir[3];
	vec3f startDir[3];
	vec3f axis[3];
	float angle[3];
	for(int i=0; i<3; i++)
	{
		float startDist = getDistAloneAxis(center, vec3f(i==0, i==1, i==2), Renderer::eyePos, startViewDir);
		float endDist = getDistAloneAxis(center, vec3f(i==0, i==1, i==2), Renderer::eyePos, viewDir);
		dragDist[i] = endDist - startDist;
		float dist = center[(i+2)%3] - Renderer::eyePos[(i+2)%3];
		vec3f startPosOnPlane = Renderer::eyePos + (dist/startViewDir[(i+2)%3])*startViewDir;
		vec3f endPosOnPlane = Renderer::eyePos + (dist/viewDir[(i+2)%3])*viewDir;
		startDir[i] = startPosOnPlane - center;
		startDir[i].normalize();
		endDir[i] = endPosOnPlane - center;
		endDir[i].normalize();
		angle[i] = acos(startDir[i].dot(endDir[i]));
		axis[i] = startDir[i].cross(endDir[i]);
		axis[i].normalize();
	}

	if(dragControlID>=3)
	{
		getCircle(dragControlID-3)->isDragging = true;
		getCircle(dragControlID-3)->dragDir = endDir[dragControlID-3];
	}

	for(set<unsigned>::iterator it=Renderer::selectedLightIndices.begin(); it!=Renderer::selectedLightIndices.end(); it++)
	{
		Renderer::PointLight &pl = Renderer::pointLightList[*it];
		if(dragControlID < 3)
		{
			pl.position += vec3f(dragControlID==0, dragControlID==1, dragControlID==2)*dragDist[dragControlID];
		}
		else
		{
			pl.position -= center;
			pl.position = *((vec3f*)&(transpose(rotMat(axis[dragControlID-3], angle[dragControlID-3]))*vec4f(pl.position, 1)));
			pl.position += center;
		}
		Renderer::render();
	}

	for(set<unsigned>::iterator it=Renderer::selectedShapeIndices.begin(); it!=Renderer::selectedShapeIndices.end(); it++)
	{
		SimpleShapeEXT &shape = Renderer::shapeList[*it];
		if(dragControlID < 3)
		{
			shape.translate(vec3f(dragControlID==0, dragControlID==1, dragControlID==2)*dragDist[dragControlID]);
		}
		else
		{
			shape.rotate(axis[dragControlID-3], angle[dragControlID-3], center);
		}
		Renderer::render();
	}
	dragStartPoint = vec2i(x, y);
}