#include "StdAfx.h"
#include "Renderer.h"

inline vec3f getVecFromValue(const char* str)
{
	vec3f vec;
	sscanf_s(str, "%f,%f,%f", &vec.x, &vec.y, &vec.z);
	return vec;
}

inline matrix4<float> getMatFromValue(const char* str)
{
	matrix4<float> mat;
	float m[16];
	sscanf_s(str, "%f,%f,%f,%f,%f,%f,%f,%f,%f,%f,%f,%f,%f,%f,%f,%f",
		&m[0], &m[1], &m[2], &m[3],
		&m[4], &m[5], &m[6], &m[7],
		&m[8], &m[9], &m[10], &m[11],
		&m[12], &m[13], &m[14], &m[15]);
	mat.set_value(m);
	return mat;
}

void Renderer::loadSceneFromXML(const string& fileName)
{
	xml_document<> doc;
	char* text = textFileRead(fileName.c_str());
	doc.parse<0>(text);

	xml_node<> *root_node = doc.first_node("Scene");
	for(xml_node<>* n = root_node->first_node("Object"); n; n = n->next_sibling("Object"))
	{
		string objFilePath(n->first_node("objFilePath")->value());
		// string nnFilePath(n->first_node("nnFilePath")->value());
		float kd = atof(n->first_node("kd")->value());
		float ks = atof(n->first_node("ks")->value());
		float alpha = atof(n->first_node("alpha")->value());
		matrix4<float> transMat = getMatFromValue(n->first_node("transformMat")->value());
		vec3f diffColor = getVecFromValue(n->first_node("diffColor")->value());
		vec3f specColor = getVecFromValue(n->first_node("specColor")->value());
		shapeList.push_back(SimpleShapeEXT());
		SimpleShapeEXT &_shape = shapeList[shapeList.size()-1];
		_shape.initSelf(objFilePath, kd, ks, alpha, diffColor, specColor, (transMat), true);
	}
	for(xml_node<>* n = root_node->first_node("PointLight"); n; n = n->next_sibling("PointLight"))
	{
		PointLight pl;
		pl.position = getVecFromValue(n->first_attribute("position")->value());
		pl.color = getVecFromValue(n->first_attribute("color")->value());
		pointLightList.push_back(pl);
	}
	free(text);
}