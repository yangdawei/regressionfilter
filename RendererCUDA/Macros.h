#define EYE_DIST_DECAY 0.9f
#define EYE_THETA_ROT_SPEED 0.01f;
#define EYE_PHI_ROT_SPEED 0.01f;
#define NEAR_Z 1e-2f
#define FAR_Z 1e2f
#define M_PI 3.14159265358979f
#define ARROW_BODY_RATIO 0.7
#define ARROW_BODY_RADIUS 0.02
#define ARROW_HEAD_RADIUS 0.04
#define AXIS_LEN 0.8
#define AXIS_ALPHA 0.5
#define AXIS_X_COLOR vec3f(1, 0, 0)
#define AXIS_Y_COLOR vec3f(0, 1, 0)
#define AXIS_Z_COLOR vec3f(0, 0, 1)
#define ROT_INNER_RADIUS 0.02
#define ROT_OUTER_RADIUS 0.4
#define ROT_XY_COLOR vec3f(1, 1, 0)
#define ROT_YZ_COLOR vec3f(0, 1, 1)
#define ROT_XZ_COLOR vec3f(1, 0, 1)
#define ROT_ALPHA 0.5