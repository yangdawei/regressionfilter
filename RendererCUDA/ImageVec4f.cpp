#define _CRT_SECURE_NO_WARNINGS

#include <cassert>
#include "ImageVec4f.h"

ImageVec4f::ImageVec4f(unsigned w, unsigned h)
{
	resize(w, h);
}

void ImageVec4f::resize(unsigned w, unsigned h)
{
	this->w = w;
	this->h = h;
	imageData.resize(w*h);
}

void ImageVec4f::savePFM(const std::string &fileName)
{
	FILE* file;
	errno_t error = fopen_s(&file, fileName.c_str(), "w");
	if (error != 0)
	{
		printf("%s\n", strerror(error));
		system("pause");
		return;
	}

	fprintf_s(file, "PF\n%d %d\n-1\n", w, h);
	fclose(file);
	FILE* file_b;
	fopen_s(&file_b, fileName.c_str(), "ab");
	for(unsigned i=0; i<h; i++)
	{
		for(unsigned j=0; j<w; j++)
			fwrite(get(j, i), sizeof(float), 3, file_b);
	}
	fclose(file_b);
}
void ImageVec4f::loadPFM(const std::string &fileName)
{
	FILE *file;
	errno_t error = fopen_s(&file, fileName.c_str(), "r");
	if (error != 0)
	{
		printf("%s\n", strerror(error));
		system("pause");
		return;
	}
	unsigned w, h;
	char buffer[4096];
	fgets(buffer, 4096, file);
	fgets(buffer, 4096, file);
	sscanf(buffer, "%d %d", &w, &h);
	fgets(buffer, 4096, file);

	resize(w, h);

	long pos = ftell(file);
	fclose(file);

	FILE *file_b;
	fopen_s(&file_b, fileName.c_str(), "rb");
	fseek(file_b, pos, SEEK_SET);
	for (unsigned i = 0; i < h; i++)
		for (unsigned j = 0; j < w; j++)
		{
			fread(get(j, i), sizeof(float), 3, file_b);
		}
		fclose(file_b);
}

void ImageVec4f::set(unsigned x, unsigned y, vec4f val)
{
	assert(x < w && y < h);
	imageData[y*w+x] = val;
}
