#include "StdAfx.h"
#include "Renderer.h"


GLuint Renderer::fboID;
GLuint Renderer::color_rboID;
GLuint Renderer::depth_rboID;
vec2i Renderer::wndSize(1024/1, 768/1);
Shader Renderer::shader;
vector<SimpleShapeEXT> Renderer::shapeList;
ImageVec4f Renderer::image;
vec3f Renderer::eyePos(0,0,2);
vec3f Renderer::eyeFront(0,0,-1);
vec3f Renderer::eyeUp(0,1,0);
int Renderer::lastMouseButton = -1;
vec2i Renderer::lastMousePos(0,0);
float Renderer::theta = -M_PI/4;
float Renderer::phi = M_PI/4;
vec3f Renderer::eyeFocus(0,0,0);
float Renderer::eyeDist = 5;
vector<unsigned> Renderer::texIDs;
unsigned Renderer::maxTexUnit = 0;
bool Renderer::renderToFBO = true;
vector<Renderer::PointLight> Renderer::pointLightList;
set<unsigned> Renderer::selectedShapeIndices;
set<unsigned> Renderer::selectedLightIndices;
SceneController Renderer::sceneController;
matrix4<float> Renderer::viewMat;
matrix4<float> Renderer::projMat;
matrix4<double> Renderer::viewMatd;
matrix4<double> Renderer::projMatd;
//int Renderer::shaderDebugCode = -1;
//vec2i Renderer::debugCoord(0, 0);