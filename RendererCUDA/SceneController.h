#pragma once

#include "Renderer.h"

class SceneController
{
protected:
	class Arrow
	{
	public:
		vec4f color;
		vec3f startPoint;
		vec3f endPoint;
		void draw();
	};
	class Circle
	{
	public:
		vec4f color;
		bool isDragging;
		vec3f dragDir;
		vec3f center;
		vec3f norm;
		void draw();
	};
	Arrow arrow_x, arrow_y, arrow_z;
	Circle circle_xy, circle_yz, circle_xz;
	vec2i dragStartPoint;
	int dragControlID;
	int dragButton;
	vec3f getViewDir(int x, int y);
	float getDistAloneAxis(const vec3f& center, const vec3f& axis, const vec3f& eyePos, const vec3f& viewDir);
	vec3f center;
	bool centerReady;
	Arrow *getArrow(int id);
	Circle *getCircle(int id);
public:
	void updateCenter();
	void draw();
	void handleMouse(int button, int state, int x, int y, int controlID);
	void handlePassiveMotion(int x, int y, int controlID);
	void handleActiveMotion(int x, int y, int controlID);
};
