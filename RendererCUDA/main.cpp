// Sampler.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include "Renderer.h"
#include "Knn.h"

#ifdef INTP_DEBUG
#include <WinSock2.h>
#pragma comment(lib, "Wsock32.lib")
SOCKET sHost;

//ANNkd_tree *rdTree, *rsTree, *pdTree, *psTree;
//ANNpoint queryPoint;

#endif


int main(int argc, char **argv)
{
	Renderer::init();

	Renderer::shader.createProgram("cuda", vector<string>(1, "shader/cuda.vert"), vector<string>(1, "shader/cuda.frag"));
	Renderer::shader.createProgram("control", vector<string>(1, "shader/control.vert"), vector<string>(1, "shader/control.frag"));
	Renderer::shader.createProgram("light", vector<string>(1, "shader/light.vert"), vector<string>(1, "shader/light.frag"));
	Renderer::shader.createProgram("debug", vector<string>(1, "shader/debug.vert"), vector<string>(1, "shader/debug.frag"));

	Renderer::loadSceneFromXML("scene/box.xml");
	Renderer::shader.useProgram("nnshader");
	Renderer::initCUDA();

	//Renderer::setShaderParams();

	glutMainLoop();

	Renderer::releaseCudaResources();
}
