#include "StdAfx.h"
#include "SimpleShape.h"

vec3f SimpleShape::getCenter() const
{
	return (minCoord+maxCoord)/2;
}

float SimpleShape::getDiagLen() const
{
	return (maxCoord - minCoord).length();
}

void SimpleShape::loadShape(const string &fileName, bool normalize)
{
	int ret;
	vertexList.clear();
	faceVertexIndexList.clear();
	string suffix = fileName.substr(fileName.length()-4,4);
	if(suffix == ".shp")
	{
		FILE* file;
		fopen_s(&file, fileName.c_str(),"rb");
		int size;
		fread(&size,sizeof(int),1,file);
		vertexList.resize(size);
		fread(vertexList.data(),sizeof(float),size,file);
		fread(&size,sizeof(int),1,file);
		faceVertexIndexList.resize(size);
		fread(faceVertexIndexList.data(),sizeof(unsigned int),size,file);
		fclose(file);
	}
	else if(suffix == ".obj")
	{
		char line[BUFFERSIZE];
		char attrib[BUFFERSIZE];
		char parms[3][BUFFERSIZE];
		FILE* file;
		fopen_s(&file, fileName.c_str(),"r");
		
		while(fgets(line,BUFFERSIZE,file))
		{
			if(line[0] == '#')
				continue;
			int num = sscanf_s(line,"%s %s %s %s",attrib,BUFFERSIZE,parms[0],BUFFERSIZE,parms[1],BUFFERSIZE,parms[2],BUFFERSIZE);
			if(num!=4)
				continue;
			if(strcmp("v",attrib)==0)
			{
				vec3f vert;
				sscanf_s(parms[0],"%f",&vert.x,sizeof(float));
				sscanf_s(parms[1],"%f",&vert.y,sizeof(float));
				sscanf_s(parms[2],"%f",&vert.z,sizeof(float));
				vertexList.push_back(vert);
				
			}
			if(strcmp("f",attrib)==0)
			{
				vec3ui tri, vnTri, tTri;

				ret = sscanf_s(parms[0],"%d/%d/%d",&tri.x,&tTri.x,&vnTri.x,sizeof(unsigned));
				ret = sscanf_s(parms[1],"%d/%d/%d",&tri.y,&tTri.y,&vnTri.y,sizeof(unsigned));
				ret = sscanf_s(parms[2],"%d/%d/%d",&tri.z,&tTri.z,&vnTri.z,sizeof(unsigned));
				if(ret==1)
				{
					ret = sscanf_s(parms[0],"%d//%d",&tri.x,&vnTri.x,sizeof(unsigned));
					ret = sscanf_s(parms[1],"%d//%d",&tri.y,&vnTri.y,sizeof(unsigned));
					ret = sscanf_s(parms[2],"%d//%d",&tri.z,&vnTri.z,sizeof(unsigned));
				}
				tri -= vec3ui(1,1,1);
				vnTri -= vec3ui(1,1,1);
				faceVertexIndexList.push_back(tri);
				if(ret >= 2)
					faceVertexNormalIndexList.push_back(vnTri);
			}
			if(strcmp("vn",attrib)==0)
			{
				vec3f vn;
				sscanf_s(parms[0],"%f",&vn.x,sizeof(float));
				sscanf_s(parms[1],"%f",&vn.y,sizeof(float));
				sscanf_s(parms[2],"%f",&vn.z,sizeof(float));
				vertexNormalList.push_back(vn);
			}
		}
		fclose(file);
		getBoundingBox(minCoord, maxCoord);
	}
	if(normalize)
	{
		this->unitize();
	}
	getBoundingBox(minCoord, maxCoord);
	setupVBO();
#ifdef INTP_DEBUG
	colorList.resize(vertexList.size());
	calcNormalPerVertex();
#endif
}

void SimpleShape::setupVBO()
{
	unsigned int bufferSize = 9 * faceVertexIndexList.size();

	GLfloat *positions = new GLfloat[bufferSize];
	GLfloat *normals = new GLfloat[bufferSize];

	//Copy data
	for (size_t faceIdx = 0; faceIdx < faceVertexIndexList.size(); faceIdx++)
	{
		for (size_t i = 0; i < 3; i++)
		{
			//vec3f &normal = vertexNormalList[faceVertexIndexList[faceIdx][i]];
			//normal.normalize();
			for (size_t j = 0; j < 3; j++)
			{
				positions[3 * (3 * faceIdx + i) + j] = vertexList[faceVertexIndexList[faceIdx][i]][j];
				normals[3 * (3 * faceIdx + i) + j] = vertexNormalList[faceVertexNormalIndexList[faceIdx][i]][j];
			}
		}
	}

	//Create buffer
	//Positions
	glGenBuffers(1, &posBuffer);
	glBindBuffer(GL_ARRAY_BUFFER, posBuffer);
	glBufferData(GL_ARRAY_BUFFER, bufferSize * sizeof(GLfloat), positions, GL_STATIC_DRAW);

	//Normals
	glGenBuffers(1, &normBuffer);
	glBindBuffer(GL_ARRAY_BUFFER, normBuffer);
	glBufferData(GL_ARRAY_BUFFER, bufferSize * sizeof(GLfloat), normals, GL_STATIC_DRAW);

	//switch on VBO
	glEnableClientState(GL_VERTEX_ARRAY);
	glBindBuffer(GL_ARRAY_BUFFER, posBuffer);
	glVertexPointer(3, GL_FLOAT, 0, 0);

	glEnableClientState(GL_NORMAL_ARRAY);
	glBindBuffer(GL_ARRAY_BUFFER, normBuffer);
	glNormalPointer(GL_FLOAT, 0, 0);

	//Clear the memory
	delete[] positions;
	delete[] normals;
}

void SimpleShape::getBoundingBox(vec3f &minCoord, vec3f &maxCoord)
{
	if(vertexList.size()<1)
		return;
	minCoord.x=maxCoord.x=vertexList[0].x;
	minCoord.y=maxCoord.y=vertexList[0].y;
	minCoord.z=maxCoord.z=vertexList[0].z;
	for(unsigned i=1;i<getVertexNum();i++)
	{
		float &x = vertexList[i].x;
		float &y = vertexList[i].y;
		float &z = vertexList[i].z;
		minCoord.x = min(x,minCoord.x);
		maxCoord.x = max(x,maxCoord.x);
		minCoord.y = min(y,minCoord.y);
		maxCoord.y = max(y,maxCoord.y);
		minCoord.z = min(z,minCoord.z);
		maxCoord.z = max(z,maxCoord.z);
	}

}

void SimpleShape::unitize()
{
	/*
	vec3f p0 = vertexList[0];
	vec3f p1 = vertexList[1];
	double radius = (p1 - p0).length() / 2;
	vec3f center = (p1 + p0) / 2;
	for (size_t i = 0; i < vertexList.size(); i++)
	{
		vec3f q = vertexList[i];
		vec3f d = q - center;
		if (d.length() <= radius)
			continue;
		//else
		d = -d;
		d.normalize();
		vec3f q2 = center + d * radius;
		center = (q2 + q) / 2;
		radius = (q2 - q).length() / 2;
	}
	for (unsigned i = 0; i < getVertexNum(); i++)
	{
		vertexList[i] -= center;
		vertexList[i] /= radius;
	}
	*/
	float minx = FLT_MAX, miny = FLT_MAX, minz = FLT_MAX;
	float maxx = FLT_MIN, maxy = FLT_MIN, maxz = FLT_MIN;
	for (size_t i = 0; i < vertexList.size(); i++)
	{
		minx = min<float>(minx, vertexList[i].x);
		miny = min<float>(miny, vertexList[i].y);
		minz = min<float>(minz, vertexList[i].z);
		maxx = max<float>(maxx, vertexList[i].x);
		maxy = max<float>(maxy, vertexList[i].y);
		maxz = max<float>(maxz, vertexList[i].z);
	}
	vec3f center = vec3f((minx+maxx)/2, (miny+maxy)/2, (minz+maxz)/2);
	float radius = powf(maxx-minx, 2) + powf(maxy-miny, 2) + powf(maxz-minz, 2);
	radius = sqrtf(radius);
	for (size_t i = 0; i < getVertexNum(); i++)
	{
		vertexList[i] -= center;
		vertexList[i] /= radius;
	}
}

void SimpleShape::saveShape(const string &fileName)
{
	FILE* file;
	fopen_s(&file, fileName.c_str(),"wb");
	int size;

	size = vertexList.size()*3;
	fwrite(&size,sizeof(int),1,file);
	fwrite(vertexList.data(),sizeof(float),size,file);

	size = faceVertexIndexList.size()*3;
	fwrite(&size,sizeof(int),1,file);
	fwrite(faceVertexIndexList.data(),sizeof(unsigned int),size,file);

	fclose(file);
}

#ifdef INTP_DEBUG
#define COLOR_DEBUG(x) glColor3fv(x.get_value());
#else
#define COLOR_DEBUG(x)
#endif

void SimpleShape::drawShape()
{
	glEnableClientState(GL_VERTEX_ARRAY);
	glBindBuffer(GL_ARRAY_BUFFER, posBuffer);
	glVertexPointer(3, GL_FLOAT, 0, 0);

	glEnableClientState(GL_NORMAL_ARRAY);
	glBindBuffer(GL_ARRAY_BUFFER, normBuffer);
	glNormalPointer(GL_FLOAT, 0, 0);

	glBegin(GL_TRIANGLES);
	for(unsigned i=0; i<faceVertexIndexList.size(); i++)
	{
		vec3f &p1 = vertexList[faceVertexIndexList[i].x];
		vec3f &p2 = vertexList[faceVertexIndexList[i].y];
		vec3f &p3 = vertexList[faceVertexIndexList[i].z];
		
		vec3f normal = (p2-p1).cross(p3-p2);
		normal.normalize();
		glNormal3fv(normal);
		//if(faceVertexIndexList[i].x<vertexNormalList.size())
			//glNormal3fv(vertexNormalList[faceVertexIndexList[i].x]);
		COLOR_DEBUG(colorList[faceVertexIndexList[i].x]);
		glVertex3fv(p1);
		//if(faceVertexIndexList[i].y<vertexNormalList.size())
			//glNormal3fv(vertexNormalList[faceVertexIndexList[i].y]);
		COLOR_DEBUG(colorList[faceVertexIndexList[i].y]);
		glVertex3fv(p2);
		//if(faceVertexIndexList[i].z<vertexNormalList.size())
			//glNormal3fv(vertexNormalList[faceVertexIndexList[i].z]);
		COLOR_DEBUG(colorList[faceVertexIndexList[i].z]);
		glVertex3fv(p3);
	}
	glEnd();

	//glDrawArrays(GL_TRIANGLES, 0, 9 * faceVertexIndexList.size());
}
