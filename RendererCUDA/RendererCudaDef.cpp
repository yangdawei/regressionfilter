#include <cuda.h>
#include <cuda_runtime.h>

#include "Renderer.h"
#include "render_kernel.cuh"
#include "helper_floatvecmat.h"

GLuint Renderer::cuVertexTexID, Renderer::cuNormalTexID, Renderer::cuFboID, Renderer::cuDepthRbo, Renderer::pboID;
CudaObjPack_t Renderer::cuObjs;
CudaLightPack_t Renderer::cuLights;
cudaGraphicsResource_t Renderer::vertexRes, Renderer::normalRes, Renderer::displayRes;
GLuint Renderer::texID;

void transferCUDA(float **ptr, const vector<float> &_vec)
{
	assert(_vec.size() > 0);
	size_t len = _vec.size();

	float *data = new float[len];
	memcpy(data, _vec.data(), sizeof(float) * len);
	cudaMalloc(ptr, len * sizeof(float));
	cudaMemcpy(*ptr, data, len * sizeof(float), cudaMemcpyHostToDevice);
}

void transferCUDA(float **ptr, const vector<vector<float>> &_mat)
{
	assert(_mat.size() > 0);
	assert(_mat[0].size() > 0);
	size_t h = _mat.size();
	size_t w = _mat[0].size();

	float *data = new float[h * w];
	for (size_t i = 0; i < h; i++)
		memcpy(data + i * w, _mat[i].data(), sizeof(float) * w);

	cudaMalloc(ptr, w * h * sizeof(float));
	cudaMemcpy(*ptr, data, w * h * sizeof(float), cudaMemcpyHostToDevice);
}

void Renderer::releaseCudaResources()
{
	cudaGraphicsUnregisterResource(displayRes);
	cudaGraphicsUnregisterResource(vertexRes);
	cudaGraphicsUnregisterResource(normalRes);

	glDeleteFramebuffers(1, &cuFboID);
	glDeleteRenderbuffers(1, &cuDepthRbo);
	glDeleteBuffers(1, &pboID);
	glDeleteTextures(1, &cuVertexTexID);
	glDeleteTextures(1, &cuNormalTexID);
	glDeleteTextures(1, &texID);

	_CheckErrorCUDA(cudaFree(cuObjs));
	_CheckErrorCUDA(cudaFree(cuLights));
}

void Renderer::initCudaFBO()
{
	// Set the texture for rendering result.
	glGenTextures(1, &texID);
	glBindTexture(GL_TEXTURE_2D, texID);

	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);

	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA32F, wndSize.x, wndSize.y, 0, GL_RGBA, GL_FLOAT, NULL);

	// Set FBOs, textures for generating vertex and normal texture.
	glGenFramebuffers(1, &cuFboID);
	glBindFramebuffer(GL_FRAMEBUFFER, cuFboID);

	glGenRenderbuffers(1, &cuDepthRbo);
	glBindRenderbuffer(GL_RENDERBUFFER, cuDepthRbo);
	glRenderbufferStorage(GL_RENDERBUFFER, GL_DEPTH_COMPONENT24, wndSize.x, wndSize.y);

	glGenTextures(1, &cuVertexTexID);
	glBindTexture(GL_TEXTURE_2D, cuVertexTexID);
	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA32F, wndSize.x, wndSize.y, 0, GL_RGBA, GL_FLOAT, 0);
	glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
	glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
	glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
	glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);

	glGenTextures(1, &cuNormalTexID);
	glBindTexture(GL_TEXTURE_2D, cuNormalTexID);
	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA32F, wndSize.x, wndSize.y, 0, GL_RGBA, GL_FLOAT, 0);
	glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
	glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
	glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
	glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);

	glFramebufferRenderbuffer(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_RENDERBUFFER, cuDepthRbo);
	glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_2D, cuVertexTexID, 0);

	glBindFramebuffer(GL_FRAMEBUFFER, 0);
}

void Renderer::initCUDA()
{
	// Init OpenGL FBOs for CUDA rendering
	initCudaFBO();
	_CheckErrorCUDA(cudaGraphicsGLRegisterImage(&vertexRes, cuVertexTexID, GL_TEXTURE_2D, cudaGraphicsMapFlagsReadOnly));
	_CheckErrorCUDA(cudaGraphicsGLRegisterImage(&normalRes, cuNormalTexID, GL_TEXTURE_2D, cudaGraphicsMapFlagsReadOnly));

	// Pixel framebuffer object for display the rendering result
	glGenBuffers(1, &pboID);
	glBindBuffer(GL_PIXEL_UNPACK_BUFFER, pboID);
	glBufferData(GL_PIXEL_UNPACK_BUFFER, wndSize.x * wndSize.y * sizeof(float) * 4, NULL, GL_DYNAMIC_DRAW);

	_CheckErrorCUDA(cudaGraphicsGLRegisterBuffer(&displayRes, pboID, cudaGraphicsMapFlagsWriteDiscard));

	// Init data.
	size_t objNum = shapeList.size();

	// Make objects parameters.
	CudaObjPack_t cuObjsCPU = new CudaObjPack[objNum];
	_CheckErrorCUDA(cudaMalloc(&cuObjs, sizeof(CudaObjPack) * objNum));
	{
		for (size_t i = 0; i < objNum; i++)
		{
			cuObjsCPU[i].diffColor = fromVec3(shapeList[i].getMaterial().diffColor);
			cuObjsCPU[i].specColor = fromVec3(shapeList[i].getMaterial().specColor);
			cuObjsCPU[i].alpha = shapeList[i].getMaterial().alpha;
			cuObjsCPU[i].kd = shapeList[i].getMaterial().kd;
			cuObjsCPU[i].ks = shapeList[i].getMaterial().ks;
			cuObjsCPU[i].transMat = fromMat4(shapeList[i].getTransMat());

			/*
			float3 color = cuObjsCPU[i].diffColor;
			printf("Diffcolor of %d: %f %f %f\n", i, color.x, color.y, color.z);
			color = cuObjsCPU[i].specColor;
			printf("Speccolor of %d: %f %f %f\n", i, color.x, color.y, color.z);
			*/
		}
		_CheckErrorCUDA(cudaMemcpy(cuObjs, cuObjsCPU, sizeof(CudaObjPack) * objNum, cudaMemcpyHostToDevice));
	}
	delete[] cuObjsCPU;

	// Make light parameters.
	size_t lightNum = pointLightList.size();
	CudaLightPack_t cuLightsCPU = new CudaLightPack[lightNum];
	_CheckErrorCUDA(cudaMalloc(&cuLights, sizeof(CudaLightPack) * lightNum));
	{
		for (size_t i = 0; i < lightNum; i++)
		{
			cuLightsCPU[i].color = fromVec3(pointLightList[i].color);
			cuLightsCPU[i].position = fromVec3(pointLightList[i].position);
		}
		_CheckErrorCUDA(cudaMemcpy(cuLights, cuLightsCPU, sizeof(CudaLightPack) * lightNum, cudaMemcpyHostToDevice));
	}
	delete[] cuLightsCPU;

	// Make NN paramters.
}


void Renderer::renderVNPrepare()
{
	matrix4<float> mvMat;
	glEnable(GL_DEPTH_TEST);
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	gluLookAt(eyePos.x, eyePos.y, eyePos.z,
		eyeFocus.x, eyeFocus.y, eyeFocus.z,
		eyeUp.x, eyeUp.y, eyeUp.z);
	shader.useProgram("cuda");
	shader.setUniform("pMat", projMat);
	shader.setUniform("objID", -1);
}

void Renderer::renderVertexNormal()
{
	renderVNPrepare();

	// 0 means vertex, 1 means normal&objID.
	renderDebugInfo(0, cuFboID, cuVertexTexID, false);
	renderDebugInfo(1, cuFboID, cuNormalTexID, false);

	//static ImageVec4f tmp(wndSize.x, wndSize.y);
	//glBindTexture(GL_TEXTURE_2D, cuNormalTexID);
	//glGetTexImage(GL_TEXTURE_2D, 0, GL_RGBA, GL_FLOAT, tmp.getData());
	//tmp.savePFM("pfm/norm.pfm");
}

void Renderer::mapResources(float4 **pdispPtr, cudaArray_t *pvertPtr, cudaArray_t *pnormPtr)
{
	_CheckErrorCUDA(cudaGraphicsMapResources(1, &vertexRes));
	_CheckErrorCUDA(cudaGraphicsMapResources(1, &normalRes));
	_CheckErrorCUDA(cudaGraphicsMapResources(1, &displayRes));

	size_t size;
	_CheckErrorCUDA(cudaGraphicsResourceGetMappedPointer((void **)pdispPtr, &size, displayRes));
	_CheckErrorCUDA(cudaGraphicsSubResourceGetMappedArray(pvertPtr, vertexRes, 0, 0));
	_CheckErrorCUDA(cudaGraphicsSubResourceGetMappedArray(pnormPtr, normalRes, 0, 0));
}

void Renderer::unmapResources()
{
	_CheckErrorCUDA(cudaGraphicsUnmapResources(1, &vertexRes));
	_CheckErrorCUDA(cudaGraphicsUnmapResources(1, &normalRes));
	_CheckErrorCUDA(cudaGraphicsUnmapResources(1, &displayRes));
}

void Renderer::updateKernelParams()
{
	size_t objNum = shapeList.size();
	CudaObjPack_t cuObjsCPU = new CudaObjPack[objNum];
	{
		for (size_t i = 0; i < objNum; i++)
		{
			cuObjsCPU[i].diffColor = fromVec3(shapeList[i].getMaterial().diffColor);
			cuObjsCPU[i].specColor = fromVec3(shapeList[i].getMaterial().specColor);
			cuObjsCPU[i].alpha = shapeList[i].getMaterial().alpha;
			cuObjsCPU[i].kd = shapeList[i].getMaterial().kd;
			cuObjsCPU[i].ks = shapeList[i].getMaterial().ks;
			cuObjsCPU[i].transMat = fromMat4(shapeList[i].getTransMat());

			// print(cuObjsCPU[i].transMat);
			// puts("");
			/*
			float3 color = cuObjsCPU[i].diffColor;
			printf("Diffcolor of %d: %f %f %f\n", i, color.x, color.y, color.z);
			color = cuObjsCPU[i].specColor;
			printf("Speccolor of %d: %f %f %f\n", i, color.x, color.y, color.z);
			*/
		}
		_CheckErrorCUDA(cudaMemcpy(cuObjs, cuObjsCPU, sizeof(CudaObjPack) * objNum, cudaMemcpyHostToDevice));
	}
	delete[] cuObjsCPU;

	// Make light parameters.
	size_t lightNum = pointLightList.size();
	CudaLightPack_t cuLightsCPU = new CudaLightPack[lightNum];
	{
		for (size_t i = 0; i < lightNum; i++)
		{
			cuLightsCPU[i].color = fromVec3(pointLightList[i].color);
			cuLightsCPU[i].position = fromVec3(pointLightList[i].position);
		}
		_CheckErrorCUDA(cudaMemcpy(cuLights, cuLightsCPU, sizeof(CudaLightPack) * lightNum, cudaMemcpyHostToDevice));
	}
	delete[] cuLightsCPU;
}

void Renderer::renderCUDA()
{
	_CheckErrorGL;

	// Render objID, vertex and normal information of each pixel
	// And store to texture handle - cuVertexTexID & cuNormalTexID.
	renderVertexNormal();
#if 0
	static bool save = true;
	if (true)
	{
		glBindTexture(GL_TEXTURE_2D, cuVertexTexID);
		glGetTexImage(GL_TEXTURE_2D, 0, GL_RGBA, GL_FLOAT, tmp.getData());
		tmp.savePFM("pfm/vertex.pfm");

		glBindTexture(GL_TEXTURE_2D, cuNormalTexID);
		glGetTexImage(GL_TEXTURE_2D, 0, GL_RGBA, GL_FLOAT, tmp.getData());
		tmp.savePFM("pfm/normal.pfm");
		save = false;
	}
#endif

	float4 *dispPtr;
	cudaArray_t vertPtr, normPtr;

	// Map the texture to CUDA texture. 
	// Note: dispPtr, vertPtr and normPtr may vary each execution of this function.
	mapResources(&dispPtr, &vertPtr, &normPtr);
	{
		// Rendered to dispPtr, or pixel framebuffer object pboID in OpenGL.
		// framework_test(dispPtr, vertPtr, normPtr, wndSize.x, wndSize.y);
		updateKernelParams();
		launch_kernel(dispPtr, wndSize.x, wndSize.y, vertPtr, normPtr, cuObjs, cuLights, shapeList.size(), pointLightList.size(), fromVec3(eyePos));
	}
	unmapResources();

	// Copy the rendered pbo data to texture using DMA(very fast).
	glBindBuffer(GL_PIXEL_UNPACK_BUFFER, pboID);

	glBindTexture(GL_TEXTURE_2D, texID);
	glTexSubImage2D(GL_TEXTURE_2D, 0, 0, 0, wndSize.x, wndSize.y, GL_RGBA, GL_FLOAT, 0);
	glBindBuffer(GL_PIXEL_UNPACK_BUFFER, 0);

#if 0
	bool save2 = true;
	if (save2)
	{
		glGetTexImage(GL_TEXTURE_2D, 0, GL_RGBA, GL_FLOAT, tmp.getData());
		tmp.savePFM("pfm/result.pfm");
		save2 = false;
	}
#endif

	//BindBuffer(GL_PIXEL_UNPACK_BUFFER, 0);
	//BindFramebuffer(GL_FRAMEBUFFER, 0);

	// Draw the rendered result.

#if 0
	static ImageVec4f tmp(wndSize.x, wndSize.y);
	glBindTexture(GL_TEXTURE_2D, texID);
	glGetTexImage(GL_TEXTURE_2D, 0, GL_RGBA, GL_FLOAT, tmp.getData());
	tmp.savePFM("pfm/test2.pfm");
#endif


	_CheckErrorGL;
}
