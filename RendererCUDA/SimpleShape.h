#pragma once
#include "stdafx.h"

#include <string>
#include <vector>
#include "GL/freeglut.h"
#include "nvVector.h"

using namespace std;
using namespace nv;

#define BUFFERSIZE 1024

/*
#define min(a,b) a<b?a:b
#define max(a,b) a>b?a:b
*/

class SimpleShape
{
protected:
	vec3f minCoord, maxCoord;

	vector<vec3f> colorList;



	// VBO
	GLuint posBuffer;
	GLuint normBuffer;

public:
	vector<vec3ui> faceVertexIndexList;
	vector<vec3f> vertexNormalList;
	vector<vec3f> myNormalList;
	vector<vec3ui> faceVertexNormalIndexList;
	vector<vec3f> vertexList;

	SimpleShape(){};
	SimpleShape(const string &fileName, bool normalize = false){ loadShape(fileName, normalize); }
	vec3f getVertexPosition(int vi) const { return vertexList[vi]; }
	vec3ui getVertexIndices(int ti) const { return faceVertexIndexList[ti]; }
	unsigned getVertexNum() const { return vertexList.size(); }
	unsigned getTriangleNum() const { return faceVertexIndexList.size(); }
	vec3f getCenter() const;
	float getDiagLen() const;
	void getBoundingBox(vec3f &minCoord, vec3f &maxCoord);
	void loadShape(const string &fileName, bool normalize = false);
	void unitize();
	void saveShape(const string &fileName);
	void drawShape();
	void setupVBO();

public:
	union diff_vec
	{
		struct 
		{
			nv::vec3f l;
			nv::vec3f b;
			nv::vec3f n;
			nv::vec2f ds;
		};
		float _arr[11];
	};

	union spec_vec
	{
		struct
		{
			nv::vec3f l;
			nv::vec3f b;
			nv::vec3f h;
			float a;
			nv::vec2f ds;
		};
		float _arr[12];
	};

	std::vector<diff_vec> diffSamples;
	std::vector<spec_vec> specSamples;

	inline void setColor(size_t vID, vec3f color)
	{
		colorList[vID] = color;
		//colorList[vID] = vec3f(0.5, 0.0, 0.8);
	}
	void loadDebugSamples(const char *diffFile, const char *specFile)
	{
		const unsigned diff_sample_dim = 11;
		const unsigned spec_sample_dim = 12;

		diffSamples.clear();
		specSamples.clear();

		//Load diffSamples
		FILE *fd;
		fopen_s(&fd, diffFile, "rb");
		while (!feof(fd))
		{
			diff_vec tmpd;
			fread(tmpd._arr, sizeof(float), diff_sample_dim, fd);
			diffSamples.push_back(tmpd);
		}
		fclose(fd);

		//Load specSamples
		FILE *fs;
		fopen_s(&fs, specFile, "rb");
		while (!feof(fd))
		{
			spec_vec tmps;
			fread(tmps._arr, sizeof(float), spec_sample_dim, fs);
			specSamples.push_back(tmps);
		}
		fclose(fs);
	}
	void calcNormalPerVertex()
	{
		myNormalList.resize(vertexList.size());
		vector<vector<vec3f>> tmpVN(vertexList.size());
		for (size_t i = 0; i < faceVertexIndexList.size(); i++)
			for (int j = 0; j < 3; j++)
				tmpVN[faceVertexIndexList[i][j]].push_back(vertexNormalList[faceVertexNormalIndexList[i][j]]);

		for (size_t i = 0; i < tmpVN.size(); i++)
		{
			vec3f average(0, 0, 0);
			for (size_t j = 0; j < tmpVN[i].size(); j++)
				average += tmpVN[i][j];
			average.normalize();
			myNormalList[i] = average;
		}
	}
};