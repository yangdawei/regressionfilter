#pragma once
#include "stdafx.h"

#include "Macros.h"
#include "Shader.h"
#include "SimpleShapeEXT.h"
#include "ImageVec4f.h"
#include "SceneController.h"
#include "ext/rapidxml/rapidxml.hpp"

#include <set>
#include <sstream>
#include <GL/freeglut_ext.h>

#include <cuda_gl_interop.h>
#include <cuda_runtime.h>
#include <helper_cuda.h>

#include "render_kernel.cuh"

#define M_PI 3.14159265358979f

//#define DEBUG_SHADER 1

#ifdef INTP_DEBUG
#include <WinSock2.h>
extern SOCKET sHost;
#endif

using namespace rapidxml;

class Renderer
{
	friend class SceneController;
protected:
	static SceneController sceneController;
	struct PointLight
	{
		vec3f position;
		vec3f color;
	};
	static bool renderToFBO;
	static GLuint fboID, color_rboID, depth_rboID;
	static GLuint cuNormalTexID, cuVertexTexID;
	static vec2i wndSize;
	static int lastMouseButton;
	static vec3f eyeFocus;
	static float eyeDist;
	static vec2i lastMousePos;
	static float theta, phi;
	static vec3f eyePos, eyeFront, eyeUp;
	static ImageVec4f image;
	static vector<GLuint> texIDs;
	static unsigned maxTexUnit;
	static vector<PointLight> pointLightList;
	static cudaGraphicsResource_t displayRes, vertexRes, normalRes;
	static GLuint pboID, texID;

	static CudaObjPack_t cuObjs;
	static CudaLightPack_t cuLights;

public:
	static vector<SimpleShapeEXT> shapeList;

	static set<unsigned> selectedShapeIndices;
	static set<unsigned> selectedLightIndices;
	static matrix4<float> viewMat;
	static matrix4<float> projMat;
	static matrix4<double> viewMatd;
	static matrix4<double> projMatd;
#if DEBUG_SHADER
	static int shaderDebugCode;
	static vec2i debugCoord;
#endif
	static void usePrespective();
	static void releaseTextures();
	static void reshape(int w, int h);
	static void initFBO();
	static void initCUDA();
	static void initCudaFBO();
	static void cleanUpFBO();
	static void mouseFunc(int button, int state, int x, int y);
	static void mouseWheelFunc(int button, int dir, int x, int y);
	static void mouseActiveMotionFunc(int x, int y);
	static void keyFunc(unsigned char key, int x, int y);
	static void render();
	static void releaseCudaResources();

	// static void renderCPU();
	//static void interpolate(const vector<SimpleShape::diff_vec> &_diffSamples);
	//static void interpolate(const vector<SimpleShape::spec_vec> &_specSamples);

	static void updateEyeBySphericalCoord();
	static void renderControlInfo();
	static void renderDebugInfo(int infoIdx, GLuint fbo=0, GLuint tex=0, bool readImage=true);
	static void renderDebugPrepare();
	static void renderDebugPost();
	static void mousePassiveMotionFunc(int x, int y);
public:
	static Shader shader;
	static GLuint cuFboID;
	static GLuint cuDepthRbo;
	static void lookAt(const vec3f& eyePos, const vec3f& eyeFront, const vec3f&eyeUp);
	static vec2i getWndSize(){ return wndSize; }
	static ImageVec4f getImage() { return image; }
	static void renderImage();
	static void setWndSize(const vec2i& size){ reshape(size.x, size.y); }
	static void init();
	static void setTexArray(const string& progName, const string& varName, const vector<vector<float>>& tex, bool equalLen = false);
	static void setTexArray(const string& progName, const string& varName, const vector<vec4f>& tex, bool equalLen = false)
	{
		vector<float> fvTex(tex.size()*4);
		memcpy(fvTex.data(), tex.data(), sizeof(float)*4*tex.size());
		setTexArray(progName, varName, vector<vector<float>>(1, fvTex), equalLen);
	}
	static void loadSceneFromXML(const string& fileName);
	static void setShaderParams();
	static void renderCUDA();
	static void renderVertexNormal();
	static void renderVNPrepare();
	static void renderCUDAInput();
	static void mapResources(float4 **pdispPtr, cudaArray_t *pvertPtr, cudaArray_t *pnormPtr);
	static void unmapResources();
	static void renderLight();
	static void renderWidgets();
	static void updateKernelParams();

#if defined USE_SAMPLER && !defined FILE_OUTPUT && defined INTP_DEBUG
	static void Renderer::transfer();
#endif
};

