#define _CRT_SECURE_NO_WARNINGS

#include "helper.h"

void save_image_raw(const char *filename, const ImageVec4f &image)
{
	FILE *fp = fopen(filename, "wb");
	unsigned w = image.width();
	unsigned h = image.height();
	fwrite(&w, sizeof(unsigned), 1, fp);
	fwrite(&h, sizeof(unsigned), 1, fp);
	fwrite(image.getData(), sizeof(float), image.width() * image.height() * 4, fp);
	fclose(fp);
}

void read_image_raw(const char *filename, ImageVec4f &image)
{
	FILE *fp = fopen(filename, "rb");
	unsigned w, h;
	fread(&w, sizeof(unsigned), 1, fp);
	fread(&h, sizeof(unsigned), 1, fp);
	image.resize(w, h);
	fread(image.getData(), sizeof(float), image.width() * image.height() * 4, fp);
	fclose(fp);
}
