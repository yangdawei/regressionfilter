#pragma once

#include "SimpleShape.h"
#include "nvMatrix.h"

inline matrix4<float> rotMat(const vec3f& axis, const float angle)
{
	float c = cos(angle);
	float s = sin(angle);
	float _c = 1 - c;
	float _s = 1 - s;
	float x = axis.x;
	float y = axis.y;
	float z = axis.z;
	return matrix4<float>(	c+_c*x*x, _c*x*y-s*z, _c*x*z+s*y, 0,
		_c*x*y+s*z, c+_c*y*y, _c*y*z-s*x, 0,
		_c*x*z-s*y, _c*y*z+s*x, c+_c*z*z, 0,
		0, 0, 0, 1);
}

typedef vector<vector<float>> vector2f;
typedef vector<vector<vec4f>> vector2vec4f;

class SimpleShapeEXT : public SimpleShape
{
protected:
	struct PhongMat
	{
		float kd, ks, alpha;
		vec3f diffColor, specColor;
	} material;

public:
	/*
	vector<vector<float>> treeNodeList;
	vector<vector<float>> diffNNList, specNNList;
	vector<float> kdIndices;
	*/
	matrix4<float> transformMat;

	struct MatSquare
	{
		int width;
		int height;
		int const_K;
		float *_data;
	};

	union
	{
		struct
		{
			MatSquare diffNNSq, specNNSq, treeNodeSq, kdIndicesSq;
		};
		MatSquare _squares[4];
	};

	void loadnnTree(const string& fileDir);
	void initSelf(const string &objFileName, const float kd, const float ks,
		const float alpha, const vec3f& diffColor, const vec3f& specColor,
		const matrix4<float> &transformMat, bool normalize = false)
	{
		__super::loadShape(objFileName, normalize);
		material.kd = kd;
		material.ks = ks;
		material.specColor = specColor;
		material.diffColor = diffColor;
		material.alpha = alpha;
		this->transformMat = transformMat;
		// loadnnTree(nnFileName);
	}
	void translate(const vec3f& shift);
	void rotate(const vec3f& axis, const float angle, const vec3f& center);
	matrix4<float> getTransMat() const { return transformMat; }
	PhongMat getMaterial() const { return material; }

	//const vector2f &getDiffNNList() const { return diffNNList; }
	//const vector2f &getSpecNNList() const { return specNNList; }
	//const vector<float> &getKdIndices() const { return kdIndices; }
	//const vector2f &getTreeNodeList() const { return treeNodeList; }
	void packMatrix(MatSquare &_sq, vector<vector<float>> &_mat);
};

