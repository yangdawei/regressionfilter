#include "StdAfx.h"
#include "SimpleShapeEXT.h"

/*
void SimpleShapeEXT::NNTree::load(FILE *file)
{
	vec2f size;
	fread_s(&size, sizeof(size), sizeof(float), 2, file);
	treeNodeList.resize(size.y);
	fread_s(treeNodeList.data(), sizeof(vec4f)*size.y, sizeof(vec4f), size.y, file);
	fread_s(&size, sizeof(size), sizeof(float), 2, file);
	nnList.resize(size.y);
	for(unsigned i=0; i<nnList.size(); i++)
	{
		nnList[i].resize(size.x);
		fread_s(nnList[i].data(), sizeof(float)*size.x, sizeof(float), size.x, file);
	}
}
*/

void SimpleShapeEXT::packMatrix(MatSquare &_sq, vector<vector<float>> &_mat)
{
	unsigned height = _mat.size();
	unsigned len = _mat[0].size();

	unsigned width = len%4==0 ? len/4 : len/4+1;

	float root = sqrtf(float(width * height));
	int const_K = int(root / width);

	int transWidth, transHeight;
	if (const_K > 0)
	{
		transWidth = width * const_K;
		transHeight = int(ceilf(width * height /  (float) transWidth));
	}
	else
	{
		transWidth = width;
		transHeight = height;
		const_K = 1;
	}
	if (transWidth * transHeight  < width * height)
	{
		printf("Transformed matrix is smaller than origin matrix!\n");
		assert(false);
		system("pause");
	}
	printf("Origin:%dx%dx4=%d floats\n", width, height, width * height * 4);
	printf("Alloc: %dx%dx4=%d floats\n", transWidth, transHeight, transWidth * transHeight * 4);
	_sq._data = (GLfloat *)malloc(sizeof(GLfloat) * 4 * transWidth * transHeight);
	if (_sq._data == NULL)
	{
		puts("Alloc failed!");
		system("pause");
	}

	puts("Alloc successfully!\n");

	for (unsigned i = 0; i < _mat.size(); i++)
	{
		for (unsigned j = 0; j < _mat[i].size(); j++)
		{
			_sq._data[i*width*4+j] = _mat[i][j];
		}
		//printf("%d/%d\n", cnt, 4 * height * width);
	}
	//validate(pixels, width, height, transWidth, transHeight, const_K);

	puts("Load to memory successfully!");

	_sq.width = transWidth;
	_sq.height = transHeight;
	_sq.const_K = const_K;
}

void SimpleShapeEXT::loadnnTree(const string& fileDir)
{
	_CheckErrorGL;
	vector<vector<float>> diffNNList, specNNList, treeNodeList;
	vector<float> kdIndices;

	FILE *file, *alphaFile;
	string nnFilePath = fileDir + "\\render";
	string alphaFilePath = fileDir + "\\alpha";
	//Read alpha file
	fopen_s(&alphaFile, alphaFilePath.c_str(), "rb");
	fread_s(&material.alpha, sizeof(float), sizeof(float), 1, alphaFile);
	fclose(alphaFile);

	vec2f size;
	vec2i isize;
	//Read vector

	fopen_s(&file, nnFilePath.c_str(), "rb");
	fread(&size, sizeof(float), 2, file);
	isize.x = size.x;
	isize.y = size.y;
	assert(isize.y == 1);
	kdIndices.resize(isize.x);
	fread(kdIndices.data(), sizeof(float), isize.x, file);

	// Read k-d tree
	fread(&size, sizeof(float), 2, file);
	isize.x = size.x;
	isize.y = size.y;

	printf("K-d tree size: %d\n", isize.x);

	// K-d tree: 24-dimensions
	treeNodeList.resize(size.x);
	assert(isize.y == 24);
	float buffer[24];
	int nnCount = 0;
	for (size_t i = 0; i < isize.x; i++)
	{
		fread(buffer, sizeof(float), 24, file);
		if (int(buffer[21]) == -1)
		{
			buffer[20] = float(nnCount);
			nnCount++;
		}

		///<Debug>
		/*
		for (int i = 0; i < 24; i++)
			printf("%f ", buffer[i]);
		puts("");
		system("pause");
		*/
		///</Debug>

		treeNodeList[i].resize(isize.y);
		memcpy(treeNodeList[i].data(), buffer, 24 * sizeof(float));
	}

	// NN tree:
	fread_s(&size, sizeof(size), sizeof(float), 2, file);
	isize.x = size.x;
	isize.y = size.y;
	diffNNList.resize(isize.x);
	for(unsigned i = 0; i < isize.x; i++)
	{
		diffNNList[i].resize(isize.y);
		fread(diffNNList[i].data(), sizeof(float), isize.y, file);
	}
	fread_s(&size, sizeof(size), sizeof(float), 2, file);
	isize.x = size.x;
	isize.y = size.y;
	specNNList.resize(isize.x);
	for (unsigned i = 0; i < isize.x; i++)
	{
		specNNList[i].resize(isize.y);
		fread(specNNList[i].data(), sizeof(float), isize.y, file);
	}

	fclose(file);

	packMatrix(diffNNSq, diffNNList);
	diffNNList.clear();
	diffNNList.shrink_to_fit();
	packMatrix(specNNSq, specNNList);
	specNNList.clear();
	specNNList.shrink_to_fit();
	packMatrix(treeNodeSq, treeNodeList);
	treeNodeList.clear();
	treeNodeList.shrink_to_fit();
	packMatrix(kdIndicesSq, vector<vector<float>>(1, kdIndices));
	kdIndices.clear();
	kdIndices.shrink_to_fit();
}

void SimpleShapeEXT::translate(const vec3f& shift)
{
	transformMat._41 += shift.x/transformMat._44;
	transformMat._42 += shift.y/transformMat._44;
	transformMat._43 += shift.z/transformMat._44;
}

void SimpleShapeEXT::rotate(const vec3f& axis, const float angle, const vec3f& center)
{ 
	translate(-center);
	transformMat = transpose(rotMat(axis, angle)) * transformMat; 
	translate(center);
}
