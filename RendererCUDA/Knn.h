#pragma once

#include <string>
#include <vector>
#include <cstdio>

#include "stdafx.h"
#include "nvVector.h"
#include "Shader.h"
#include "Renderer.h"
#include "SimpleShape.h"
//#include "ANN/ANN.h"

#define DDIM 9
#define SDIM 10

//extern ANNkd_tree *rdTree, *rsTree, *pdTree, *psTree;
//extern ANNpoint queryPoint;

#ifdef INTP_DEUBG


/*
union diff_vec
{
	struct 
	{
		nv::vec3f l;
		nv::vec3f b;
		nv::vec3f n;
		nv::vec2f ds;
	};
	float _arr[11];
};

union spec_vec
{
	struct
	{
		nv::vec3f l;
		nv::vec3f b;
		nv::vec3f h;
		float a;
		nv::vec2f ds;
	};
	float _arr[12];
};
*/

/*
class Knn
{
private:
	std::vector<SimpleShape::diff_vec> diffSamples;
	std::vector<SimpleShape::spec_vec> specSamples;
	 unsigned diff_dim, spec_dim;

public:
	Knn() { diff_dim = 11, spec_dim = 12; }

	void loadDiffSpec(const std::string &_diffFile, const std::string &_specFile)
	{
		diffSamples.clear();
		specSamples.clear();

		//Load diffSamples
		FILE *fd;
		fopen_s(&fd, _diffFile.c_str(), "rb");
		while (!feof(fd))
		{
			SimpleShape::diff_vec tmpd;
			fread(tmpd._arr, sizeof(float), diff_dim, fd);
			diffSamples.push_back(tmpd);
		}
		fclose(fd);

		//Load specSamples
		FILE *fs;
		fopen_s(&fs, _specFile.c_str(), "rb");
		while (!feof(fd))
		{
			SimpleShape::spec_vec tmps;
			fread(tmps._arr, sizeof(float), spec_dim, fs);
			specSamples.push_back(tmps);
		}
		fclose(fs);
	}

	void loadToShader(int idx)
	{
		vector<vector<float>> diffSampleList(diffSamples.size(), vector<float>(diff_dim));
		vector<vector<float>> specSampleList(specSamples.size(), vector<float>(spec_dim));
		for (size_t i = 0; i < diffSamples.size(); i++)
			for (size_t j = 0; j < diff_dim; j++)
				diffSampleList[i][j] = diffSamples[i]._arr[j];

		for (size_t i = 0; i < specSamples.size(); i++)
			for (size_t j = 0; j < spec_dim; j++)
				specSampleList[i][j] = specSamples[i]._arr[j];

		ostringstream oss;
		oss.str("");
		oss << "diffSamples[" << idx << "]";
		_CheckErrorGL
		Renderer::setTexArray("nnshader", oss.str(), diffSampleList);
		_CheckErrorGL

		oss.str("");
		oss << "specSamples[" << idx << "]";
		Renderer::setTexArray("nnshader", oss.str(), specSampleList);
		_CheckErrorGL
	}

private:
};
*/

#endif
