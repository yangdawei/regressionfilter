#include "StdAfx.h"
#include "Renderer.h"
#include "Knn.h"
#include "helper.h"

#ifdef INTP_DEBUG
#include "Knn.h"
#ifdef USE_SAMPLER
#include <WinSock2.h>
#endif
#endif
int g_objID = -1;

#ifdef FILE_OUTPUT
#include <cstdio>
#endif

bool usePhongShader = false;
bool showIndirectOnly = false;
bool interpolation = false;
bool flatModel = true;

#define DEBUG_SHADER 1

void checkIn(float lb, float x, float rb)
{
	if (lb > rb || x < lb - 5e-2 || x > rb + 5e-2) {
		printf("warning: checkIn %f %f %f\n", lb, x, rb);
		// system("pause");
	}
}
void Renderer::usePrespective()
{
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	gluPerspective(90, float(wndSize.x)/wndSize.y, NEAR_Z, FAR_Z);
}


inline void displayTexture(GLuint texture)
{
	glBindFramebuffer(GL_FRAMEBUFFER, 0);
	glBindBuffer(GL_PIXEL_UNPACK_BUFFER, 0);
	glBindTexture(GL_TEXTURE_2D, texture);
	glUseProgram(0);

	glMatrixMode(GL_PROJECTION);
	glPushMatrix();
	glLoadIdentity();

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();

	glColor3f(1.f, 1.f, 1.f);

	glDisable(GL_DEPTH_TEST);

	glColor3f(1, 1, 1);

	glBegin(GL_QUADS);
	glTexCoord2f(0.0, 0.0); glVertex3f(-1.0, -1.0, 0.5);
	glTexCoord2f(1.0, 0.0); glVertex3f(1.0, -1.0, 0.5);
	glTexCoord2f(1.0, 1.0); glVertex3f(1.0, 1.0, 0.5);
	glTexCoord2f(0.0, 1.0); glVertex3f(-1.0, 1.0, 0.5);
	glEnd();

	glMatrixMode(GL_PROJECTION);
	glPopMatrix();
}

void Renderer::renderWidgets()
{
	sceneController.draw();
}

void Renderer::renderLight()
{
	_CheckErrorGL;
	glBindFramebuffer(GL_FRAMEBUFFER, cuFboID);
	glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_2D, texID, 0);

	shader.useProgram("light");
	shader.setUniform("eye_position", eyePos);
	for(unsigned i=0; i<pointLightList.size(); i++)
	{
		PointLight &pl = pointLightList[i];
		shader.setUniform("light_position", pl.position);
		glColor3f(pl.color.x, pl.color.y, pl.color.z);
		shader.setUniform("light_color", pl.color);
		glPushMatrix();
		glTranslatef(pl.position.x, pl.position.y, pl.position.z);
		glutSolidSphere(0.1, 20, 20);
		glPopMatrix();
	}
	_CheckErrorGL;
}

void Renderer::render()
{
	_CheckErrorGL

	printf("eyePos\t\t:(%.5f, %.5f, %.5f\n", eyePos.x, eyePos.y, eyePos.z);
	printf("eyeUp\t\t:(%.5f, %.5f, %.5f\n", eyeUp.x, eyeUp.y, eyeUp.z);
	printf("eyeFront\t:(%.5f, %.5f, %.5f)\n", eyeFront.x, eyeFront.y, eyeFront.z);
	printf("eyeFocus\t:(%.5f, %.5f, %.5f)\n", eyeFocus.x, eyeFocus.y, eyeFocus.z);

	if (flatModel)
		glShadeModel(GL_FLAT);
	else
		glShadeModel(GL_SMOOTH);

	glLightModeli(GL_LIGHT_MODEL_TWO_SIDE, GL_TRUE);

	renderControlInfo();

	matrix4<float> mvMat;
	glClearColor(0, 0, 0, 1);
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	glEnable(GL_DEPTH_TEST);
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	gluLookAt(eyePos.x, eyePos.y, eyePos.z,
		eyeFocus.x, eyeFocus.y, eyeFocus.z,
		eyeUp.x, eyeUp.y, eyeUp.z);
	glGetFloatv(GL_MODELVIEW_MATRIX, (GLfloat*)viewMat.get_value());
	glGetFloatv(GL_PROJECTION_MATRIX, (GLfloat*)projMat.get_value());
	glGetDoublev(GL_MODELVIEW_MATRIX, (GLdouble*)viewMatd.get_value());
	glGetDoublev(GL_PROJECTION_MATRIX, (GLdouble*)projMatd.get_value());

	glEnable(GL_TEXTURE_2D);

	renderCUDA();
	renderLight();
	renderWidgets();

	glBindFramebuffer(GL_FRAMEBUFFER, cuFboID);
	glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_2D, texID, 0);

	glEnable(GL_TEXTURE_2D);
	displayTexture(texID);

	shader.useProgram("");
	glEnable(GL_BLEND);
	glDisable(GL_DEPTH_TEST);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE);
	sceneController.draw();
	glDisable(GL_BLEND);
	glEnable(GL_DEPTH_TEST);

	glFlush();

#ifdef INTP_DEBUG

	ImageVec4f pixels;
	FILE * shot;
	// we get the width/height of the screen into this array
	int screenStats[4];

	// get the width/height of the window
	glGetIntegerv(GL_VIEWPORT, screenStats);

	// generate an array large enough to hold the pixel data 
	// (width*height*bytesPerPixel)
	pixels.resize(screenStats[2], screenStats[3]);
	// read in the pixel data, TGA's pixels are BGR aligned
	glReadPixels(0, 0, pixels.width(), pixels.height(), GL_RGBA, GL_FLOAT, pixels.getData());
	pixels.savePFM("pfm/render.pfm");
#endif
	
	glutSwapBuffers();
}

//infoIdx: {0 : LighPos, 1 : BouncingPoint, 2 : Normal, 3 : ReflDir}

void Renderer::renderDebugPrepare()
{
	matrix4<float> mvMat;
	initFBO();
	glEnable(GL_DEPTH_TEST);
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	gluLookAt(eyePos.x, eyePos.y, eyePos.z,
		eyeFocus.x, eyeFocus.y, eyeFocus.z,
		eyeUp.x, eyeUp.y, eyeUp.z);
	shader.useProgram("debug");
	shader.setUniform("pMat", projMat);
	shader.setUniform("objID", -1);
	setShaderParams();

	// Lights
	for(size_t i=0; i<pointLightList.size(); i++)
	{
		ostringstream oss;
		oss << "lightList[" << i << "]" << ".position";
		shader.setUniform(oss.str(), pointLightList[i].position);
		oss.str("");
		oss << "lightList[" << i << "]" << ".color";
		shader.setUniform(oss.str(), pointLightList[i].color);
	}
	shader.setUniform("objID", -1);
}

void Renderer::renderDebugPost()
{
	cleanUpFBO();
}

void Renderer::renderDebugInfo(int infoIdx, GLuint fbo /*=0*/, GLuint tex /*=0*/, bool readImage/*=true*/)
{
	if (fbo == 0)
		fbo = fboID;
	if (tex == 0)
		tex = color_rboID;

	glBindFramebuffer(GL_FRAMEBUFFER, fbo);
	glBindTexture(GL_TEXTURE_2D, tex);
	glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_2D, tex, 0);

	glClearColor(-1, -1, -1, -1);
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	// Attach color buffer to FBO

	matrix4<float> mvMat;
	shader.setUniform("infoIdx", infoIdx);

	for(unsigned i=0; i<shapeList.size(); i++)
	{
		glPushMatrix();
		glMultMatrixf(shapeList[i].getTransMat().get_value());
		glGetFloatv(GL_MODELVIEW_MATRIX, (GLfloat*)mvMat.get_value());
		shader.setUniform("mvMat", mvMat);
		shader.setUniform("pMat", projMat);
		shader.setUniform("objID", int(i));
		shapeList[i].drawShape();
		glPopMatrix();
	}

	if (readImage)
	{
		glReadBuffer(GL_COLOR_ATTACHMENT0);
		glReadPixels(0, 0, wndSize.x, wndSize.y, GL_RGBA, GL_FLOAT, image.getData());
	}

	glFlush();
}

void Renderer::renderControlInfo()
{
	matrix4<float> mvMat;
	initFBO();
	glClearColor(-1, -1, -1, -1);
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	glEnable(GL_DEPTH_TEST);
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	gluLookAt(eyePos.x, eyePos.y, eyePos.z,
		eyeFocus.x, eyeFocus.y, eyeFocus.z,
		eyeUp.x, eyeUp.y, eyeUp.z);
	shader.useProgram("control");
	shader.setUniform("pMat", projMat);
	shader.setUniform("objID", -1);
	shader.setUniform("lightID", -1);
	shader.setUniform("controlID", -1);

	for(unsigned i=0; i<pointLightList.size(); i++)
	{
		PointLight &pl = pointLightList[i];
		shader.setUniform("lightID", int(i));
		glPushMatrix();
		glTranslatef(pl.position.x, pl.position.y, pl.position.z);
		glutSolidSphere(0.1, 20, 20);
		glPopMatrix();
	}

	shader.setUniform("lightID", -1);

	for(unsigned i=0; i<shapeList.size(); i++)
	{
		glPushMatrix();
		glMultMatrixf(shapeList[i].getTransMat().get_value());
		glGetFloatv(GL_MODELVIEW_MATRIX, (GLfloat*)mvMat.get_value());
		shader.setUniform("mvMat", mvMat);
		shader.setUniform("pMat", projMat);
		shader.setUniform("objID", int(i));
		shapeList[i].drawShape();
		glPopMatrix();
	}

	shader.setUniform("objID", -1);

	glDisable(GL_DEPTH_TEST);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE);
	sceneController.draw();
	glEnable(GL_DEPTH_TEST);
	glFlush();
	glReadBuffer(GL_COLOR_ATTACHMENT0);
	glReadPixels(0, 0, wndSize.x, wndSize.y, GL_RGBA, GL_FLOAT, image.getData());

	cleanUpFBO();
}

static float nnInput[50], nnResult[50];

void Renderer::mouseFunc(int button, int state, int x, int y)
{
	render();
	vec4f controlInfo = image.get(x, wndSize.y - 1 - y);
	sceneController.handleMouse(button, state, x, y, int(controlInfo.y));
	

	if(state==GLUT_DOWN)
	{
		lastMousePos = vec2i(x, y);
		if(button==GLUT_RIGHT_BUTTON)
		{
			//debugCoord = vec2i(x, y);
			selectedShapeIndices.clear();
			selectedLightIndices.clear();
		}
		if(button==GLUT_LEFT_BUTTON)
		{
			int mod = glutGetModifiers();
			if(mod != GLUT_ACTIVE_SHIFT && mod != GLUT_ACTIVE_CTRL && controlInfo.y<0)
			{
				selectedLightIndices.clear();
				selectedShapeIndices.clear();
			}
			if(controlInfo.x>=0)
			{
				selectedShapeIndices.insert(unsigned(controlInfo.x));
			}
			if(controlInfo.z>=0)
			{
				selectedLightIndices.insert(unsigned(controlInfo.z));
			}
			// Ctrl + leftbutton, show the information of the current pos.
			if (mod == GLUT_ACTIVE_CTRL && controlInfo.x >= 0)
			{
				renderDebugPrepare();
				renderDebugInfo(0);
				image.savePFM("pfm/0.pfm");
				vec4f lightPos = image.get(x, wndSize.y - 1 - y);
				int objID = int(lightPos.w);
				renderDebugInfo(1);
				image.savePFM("pfm/1.pfm");
				vec4f bouncPos = image.get(x, wndSize.y - 1 - y);
				renderDebugInfo(2);
				image.savePFM("pfm/2.pfm");
				vec4f normal = image.get(x, wndSize.y - 1 - y);
				renderDebugInfo(3);
				image.savePFM("pfm/3.pfm");
				vec4f reflDir = image.get(x, wndSize.y - 1 - y);
				renderDebugPost();

				printf("ID: %d\n", objID);
				printf("L: (%f, %f, %f)\n", lightPos.x, lightPos.y, lightPos.z);
				printf("P: (%f, %f, %f)\n", bouncPos.x, bouncPos.y, bouncPos.z);
				printf("N: (%f, %f, %f)\n", normal.x, normal.y, normal.z);
				printf("R: (%f, %f, %f)\n", reflDir.x, reflDir.y, reflDir.z);
				float alpha = reflDir.w;
				printf("A: %f\n", alpha);

				/*
				nnInput[0] = lightPos.x;
				nnInput[1] = lightPos.y;
				nnInput[2] = lightPos.z;
				nnInput[3] = bouncPos.x;
				nnInput[4] = bouncPos.y;
				nnInput[5] = bouncPos.z;
				nnInput[6] = normal.x;
				nnInput[7] = normal.y;
				nnInput[8] = normal.z;

				evalNNTree(shapeList[1-objID].getKdIndices(), shapeList[objID].getTreeNodeList(), shapeList[objID].getDiffNNList());
				printf("Diffuse (%f, %f)\n", nnResult[0], nnResult[1]);

				nnInput[0] = lightPos.x;
				nnInput[1] = lightPos.y;
				nnInput[2] = lightPos.z;
				nnInput[3] = bouncPos.x;
				nnInput[4] = bouncPos.y;
				nnInput[5] = bouncPos.z;
				nnInput[6] = reflDir.x;
				nnInput[7] = reflDir.y;
				nnInput[8] = reflDir.z;
				nnInput[9] = alpha;

				evalNNTree(shapeList[1-objID].getKdIndices(), shapeList[objID].getTreeNodeList(), shapeList[objID].getSpecNNList());
				printf("Specular(%f, %f)\n", nnResult[0], nnResult[1]);
				*/

				puts("");
			}
		}
	}

	sceneController.updateCenter();

	if(controlInfo.y<0)
		lastMouseButton = button;
	else
		lastMouseButton = -1;
}

void Renderer::mouseWheelFunc(int button, int dir, int x, int y)
{
	eyeDist *= dir<0?EYE_DIST_DECAY:1/EYE_DIST_DECAY;
	eyePos = eyeFocus - eyeFront*eyeDist;
	render();
}



void Renderer::updateEyeBySphericalCoord()
{
	vec3f up(0,1,0);
	eyeFront = -vec3f(sin(phi)*cos(theta), cos(phi), sin(phi)*sin(theta));
	float angle = acos(up.dot(eyeUp));
	vec3f axis = up.cross(eyeUp);
	axis.normalize();
	matrix4<float> rotMatrix = rotMat(axis, angle);
	eyeFront = *(vec3f*)&(rotMatrix*vec4f(eyeFront,1));
	eyePos = eyeFocus - eyeFront*eyeDist;
}

void Renderer::mouseActiveMotionFunc(int x, int y)
{
	render();
	vec4f controlInfo = image.get(x, wndSize.y - 1 - y);
	sceneController.handleActiveMotion(x, y, int(controlInfo.y));
	vec2i nextMousePos(x, y);
	vec2i delta = nextMousePos - lastMousePos;
	switch(lastMouseButton)
	{
	case GLUT_LEFT_BUTTON:
		theta += delta.x*EYE_THETA_ROT_SPEED;
		phi -= delta.y*EYE_PHI_ROT_SPEED;
		theta = theta > 2*M_PI?theta-2*M_PI : (theta<0 ? theta+2*M_PI : theta);
		phi = phi >= M_PI-M_PI/18 ? M_PI-M_PI/18 : (phi<=M_PI/18 ? M_PI/18 : phi);
		updateEyeBySphericalCoord();
		break;
	}
	render();
	lastMousePos = nextMousePos;
}

void Renderer::reshape(int w, int h)
{
	cleanUpFBO();
	wndSize.x = w;
	wndSize.y = h;
	image.resize(w, h);
	usePrespective();
	if(renderToFBO)
		initFBO();
}

void Renderer::lookAt(const vec3f& eyePos, const vec3f& eyeFront, const vec3f&eyeUp)
{
	Renderer::eyePos = eyePos;
	Renderer::eyeUp = eyeUp;
	Renderer::eyeFront = eyeFront;
}

void Renderer::renderImage()
{
	initFBO();
	render();
	glReadBuffer(GL_COLOR_ATTACHMENT0);
	glReadPixels(0, 0, wndSize.x, wndSize.y, GL_RGBA, GL_FLOAT, image.getData());
	cleanUpFBO();
}

void Renderer::initFBO()
{
	// Create
	// frame buffer object
	glGenFramebuffers(1, &fboID);
	glBindFramebuffer(GL_FRAMEBUFFER, fboID);

	// depth buffer with render buffer object
	glGenRenderbuffers(1, &depth_rboID);
	glBindRenderbuffer(GL_RENDERBUFFER, depth_rboID);
	glRenderbufferStorage(GL_RENDERBUFFER, GL_DEPTH_COMPONENT24, wndSize.x, wndSize.y);

	// color buffer with texture object
	glGenTextures(1, &color_rboID);
	glBindTexture(GL_TEXTURE_2D, color_rboID);
	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA32F, wndSize.x, wndSize.y, 0, GL_RGBA, GL_FLOAT, 0);
	glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
	glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
	glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);

	//Attach depth buffer to FBO
	glFramebufferRenderbuffer(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_RENDERBUFFER, depth_rboID);

	// Attach color buffer to FBO
	glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_2D, color_rboID, 0);
}

void Renderer::cleanUpFBO()
{
	if (fboID)
	{
		glDeleteFramebuffers(1, &fboID);
		fboID = 0;
	}

	if (depth_rboID)
	{
		glDeleteRenderbuffers(1, &depth_rboID);
		depth_rboID = 0;
	}

	if (color_rboID)
	{
		glDeleteTextures(1, &color_rboID);
		color_rboID = 0;
	}
}

void Renderer::mousePassiveMotionFunc(int x, int y)
{
	render();
	vec4f controlInfo = image.get(x, wndSize.y - 1 - y);
	sceneController.handlePassiveMotion(x, y, int(controlInfo.y));
}

void Renderer::init()
{
	int ac = 0;
	char *av;
	glutInit(&ac, &av);
	glutInitDisplayMode(GLUT_DEPTH | GLUT_DOUBLE | GLUT_RGBA);
	glutInitWindowPosition(100,100);
	glutInitWindowSize(wndSize.x, wndSize.y);
	glutCreateWindow("FBO");
	glutDisplayFunc(render);
	glutMouseFunc(mouseFunc);
	glutMotionFunc(mouseActiveMotionFunc);
	glutMouseWheelFunc(mouseWheelFunc);
	glutKeyboardFunc(keyFunc);
	glutPassiveMotionFunc(mousePassiveMotionFunc);
	shader.init();
	updateEyeBySphericalCoord();
	setWndSize(wndSize);
	glLightModeli(GL_LIGHT_MODEL_TWO_SIDE, GL_TRUE);

	glEnable(GL_AUTO_NORMAL);
	glEnable(GL_NORMALIZE);

	if(renderToFBO)
		initFBO();
}

void Renderer::keyFunc(unsigned char key, int x, int y)
{
	if(key == 'r')
	{
		printf("Reloading shader...\n");
		shader.deleteProgram("");
		shader.createProgram("nnshader", vector<string>(1, "shader/nnshader.vert"), vector<string>(1, "shader/nnshader.frag"));
		shader.useProgram("nnshader");
		setShaderParams();
		printf("Done.\n");
		render();
	}
#ifdef DEBUG_SHADER

	if (key == 'p')	//Exchange shader between phong and nnshader
	{
		usePhongShader = !usePhongShader;
		// renderCPU();
		glutPostRedisplay();
		return;
	}
	if (key == 'o') // show indirect light only;
	{
		showIndirectOnly = !showIndirectOnly;
		// renderCPU();
		glutPostRedisplay();
		return;
	}
	if (key == 'g')
	{
		interpolation = !interpolation;
		printf("Toggle interpolation\n");
		glutPostRedisplay();
		return;
	}
	if (key == 'h')
	{
		printf("Refresh\n");
		// renderCPU();
		glutPostRedisplay();
		return;
	}
	if (key == 'j')
	{
		printf("Toggle flat/smooth model\n");
		flatModel = !flatModel;
		glutPostRedisplay();
		return;
	}
	if (key == ' ')
	{
		puts("Save scene and normal.");
		glReadBuffer(GL_COLOR_ATTACHMENT0);
		glReadPixels(0, 0, wndSize.x, wndSize.y, GL_RGBA, GL_FLOAT, image.getData());
		image.savePFM("pfm/screenshot.pfm");
		save_image_raw("pfm/screenshot.raw", image);

		renderDebugPrepare();
		renderDebugInfo(5);
		image.savePFM("pfm/normal.pfm");
		save_image_raw("pfm/normal.raw", image);
		renderDebugPost();

		return;
	}
	if (key == 's')
	{
		puts("Save camera.");
		FILE *fcam = fopen("camera.txt", "w");
		fprintf(fcam, "%.10f %.10f %.10f\n", eyePos.x, eyePos.y, eyePos.z);
		fprintf(fcam, "%.10f %.10f %.10f\n", eyeUp.x, eyeUp.y, eyeUp.z);
		fprintf(fcam, "%.10f %.10f %.10f\n", eyeFront.x, eyeFront.y, eyeFront.z);
		fprintf(fcam, "%.10f %.10f %.10f\n", eyeFocus.x, eyeFocus.y, eyeFocus.z);
		fclose(fcam);
	}
	if (key == 'l')
	{
		puts("Load camera.");
		FILE *fcam = fopen("camera.txt", "r");
		if (fcam == NULL)
		{
			puts("No camera.txt!");
			return;
		}
		fscanf(fcam, "%f %f %f", &eyePos.x, &eyePos.y, &eyePos.z);
		fscanf(fcam, "%f %f %f", &eyeUp.x, &eyeUp.y, &eyeUp.z);
		fscanf(fcam, "%f %f %f", &eyeFront.x, &eyeFront.y, &eyeFront.z);
		fscanf(fcam, "%f %f %f", &eyeFocus.x, &eyeFocus.y, &eyeFocus.z);
		fclose(fcam);
		glutPostRedisplay();
	}

	//shaderDebugCode = -1;
#endif
}
