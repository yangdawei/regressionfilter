#version 150
#define NN_MAX_NEURON_NUM 25
#define MAX_OBJ_NUM 5
#define MAX_LIGHT_NUM 5
#define DEBUG 1

#define TYPE_IDX 20
#define L_CHD 21
#define R_CHD 22
#define NN_IDX 23
#define R_OFFSET(x) (11+x)
#define BIGGER(x) (3+x)

#if DEBUG
/*8uniform*/ int debugCode = -1;
#endif

in vec3 normal;
in vec4 vertex_position;
out vec4 fragColor;

struct PointLight;
struct TexArray;
struct PhongMaterial;

// bounce related
uniform int numBounces;
uniform int interpolation;

// light related
uniform int numLights;
uniform PointLight lightList[MAX_LIGHT_NUM];

// obj related
uniform int objID;
uniform int numObjects;
uniform mat4 objTransMatList[MAX_OBJ_NUM];
uniform PhongMaterial objMaterialList[MAX_OBJ_NUM];

// eye related
uniform vec3 eye_position;

// nn related
uniform TexArray diff_treeNodeTex[MAX_OBJ_NUM], diff_nnListTex[MAX_OBJ_NUM];
uniform TexArray spec_treeNodeTex[MAX_OBJ_NUM], spec_nnListTex[MAX_OBJ_NUM];
uniform TexArray diffSamples[MAX_OBJ_NUM], specSamples[MAX_OBJ_NUM];

float nnInput[NN_MAX_NEURON_NUM];
float nnResult[NN_MAX_NEURON_NUM];

float map(float val, float minVal, float maxVal);
float unmap(float val, float minVal, float maxVal);
void mapDiffInput(int oi);
void unmapDiffOutput(int oi);
void mapSpecInput(int oi);
void unmapSpecOutput(int oi);
void interpolate(int dim, TexArray samples);
float tansig(float x);
float sigmoid(float x);
void evalNNTree(TexArray treeNodeList, TexArray nnList, vec3 vl, vec3 vp, vec3 vr);
void evalNN(TexArray nnList, int nnID);
int getFaceIdx(vec3 aVec);
int getPosSpaceIdx(vec3 aVec);
int getInitIndex(vec3 vl, vec3 vp, vec3 vr);

struct PointLight
{
	vec3 position;
	vec3 color;
};

struct TexArray
{
	int width;
	int height;
	sampler2D id;
	vec4 getVec4(int x, int y)
	{
		float u = (x+0.5)/width;
		float v = (y+0.5)/height;
		return texture2D(id, vec2(u, v));
	}
	float getFloat(int x, int y)
	{
		return getVec4(x/4, y)[x%4];
	}
	int getInt(int x, int y)
	{
		return int(round(getVec4(x/4, y)[x%4]));
	}
};

struct PhongMaterial
{
	vec3 diffColor;
	vec3 specColor;
	float kd, ks, alpha;
};

void main()
{
	vec3 normal = normalize(normal);
	vec4 transPos = (objTransMatList[objID])*vertex_position;
	vec4 transNormal = (objTransMatList[objID]*vec4(normal, 0));
	vec3 out_dir = normalize(eye_position - transPos.xyz);
	vec3 reflDir = dot(out_dir, normal)*normal*2-out_dir;
	vec4 transReflDir = objTransMatList[objID]*vec4(reflDir, 0);
	vec3 diffColor_0b = vec3(0,0,0);
	vec3 specColor_0b = vec3(0,0,0);
	vec3 diffColor_1b = vec3(0,0,0);
	vec3 specColor_1b = vec3(0,0,0);
	vec3 color_0b;
	vec3 color_1b;
	vec3 ds_color;
	
	for(int li=0; li<numLights; li++)
	{
		vec3 in_dir = normalize(transPos.xyz - lightList[li].position);
		vec3 half_dir = normalize(out_dir - in_dir);
		float spec_intensity = pow(max(dot(normal, half_dir),0), objMaterialList[objID].alpha);
		diffColor_0b += lightList[li].color*objMaterialList[objID].kd*objMaterialList[objID].diffColor;
		specColor_0b += lightList[li].color*objMaterialList[objID].ks*objMaterialList[objID].specColor*spec_intensity;
		diffColor_0b *= max(dot(normal, -in_dir), 0);
		specColor_0b *= max(dot(normal, -in_dir), 0);
		for(int oi=0; oi<numObjects; oi++)
		{
			if(oi == objID)
				continue;
			mat4 invMat = inverse(objTransMatList[oi]);
			vec3 relPos = (invMat*transPos).xyz;
			vec3 relNormal = normalize((invMat*transNormal).xyz);
			vec3 relReflDir = normalize((invMat*transReflDir).xyz);
			vec3 relLightPos = (invMat*vec4(lightList[li].position,1)).xyz;
			
			
			nnInput[0] = relLightPos.x;
			nnInput[1] = relLightPos.y;
			nnInput[2] = relLightPos.z;
			nnInput[3] = relPos.x;
			nnInput[4] = relPos.y;
			nnInput[5] = relPos.z;
			nnInput[6] = relNormal.x;
			nnInput[7] = relNormal.y;
			nnInput[8] = relNormal.z;
#if DEBUG
			switch(debugCode)
			{
			case 0:
				fragColor = vec4(nnInput[0], nnInput[1], nnInput[2], nnInput[3]);
				return;
			case 1:
				fragColor = vec4(nnInput[4], nnInput[5], nnInput[6], nnInput[7]);
				return;
			case 2:
				fragColor.x = nnInput[8];
			}
#endif
			evalNNTree(diff_treeNodeTex[oi], diff_nnListTex[oi], relLightPos,
				relPos, relNormal);
			//if (interpolation == 0)
				// evalNNTree(diff_treeNodeTex[oi], diff_nnListTex[oi]);
			//else
				//interpolate(9, diffSamples[oi]);

#if DEBUG
			if(debugCode == 2)
			{
				fragColor.yzw = vec3(nnResult[0] / 10.0+1, nnResult[1] / 10.0+1, 0);
				return;
			}
#endif

			//fragColor = vec4(nnResult[0], nnResult[1], 0.0, 1.0);
			//return;
			
			ds_color = objMaterialList[oi].diffColor*objMaterialList[oi].kd*nnResult[0] +
				objMaterialList[oi].specColor*objMaterialList[oi].ks*nnResult[1];
			diffColor_1b += lightList[li].color*objMaterialList[objID].diffColor*objMaterialList[objID].kd*ds_color;
			
			nnInput[0] = relLightPos.x;
			nnInput[1] = relLightPos.y;
			nnInput[2] = relLightPos.z;
			nnInput[3] = relPos.x;
			nnInput[4] = relPos.y;
			nnInput[5] = relPos.z;
			nnInput[6] = relReflDir.x;
			nnInput[7] = relReflDir.y;
			nnInput[8] = relReflDir.z;
			nnInput[9] = objMaterialList[objID].alpha;

			evalNNTree(spec_treeNodeTex[oi], spec_nnListTex[oi], relLightPos, relPos, relReflDir);
			//if (interpolation == 0)
				// evalNNTree(spec_treeNodeTex[oi], spec_nnListTex[oi]);
			//else
				//interpolate(10, specSamples[oi]);

//Debug
			//fragColor = vec4(nnResult[0], nnResult[1], 0.0, 1.0);
			//return;
			
			ds_color = objMaterialList[oi].diffColor*objMaterialList[oi].kd*nnResult[0] +
				objMaterialList[oi].specColor*objMaterialList[oi].ks*nnResult[1];
			specColor_1b += lightList[li].color*objMaterialList[objID].specColor*objMaterialList[objID].ks*ds_color;
		}
	}
	color_0b = diffColor_0b + specColor_0b;
	color_1b = diffColor_1b + specColor_1b;
	if (numBounces == -1)
		fragColor = vec4(color_1b, 1);
	else if (numBounces != 0)
		fragColor = vec4(color_0b+color_1b, 1);
	else
		fragColor = vec4(color_0b, 1);
}

float map(float val, float minVal, float maxVal)
{
	/*
	if (val < minVal-0.000001 || val > maxVal+0.000001)
		g_mapCorrect = false;
		*/
	if (minVal != maxVal)
		return (val-minVal)/(maxVal-minVal)*2-1;
	else
		return minVal;
}

float unmap(float val, float minVal, float maxVal)
{
	/*
	if (val < -1 || val > 2)
		g_unmapCorrect = false;
		*/
	// return (1+val)/2.0*(maxVal-minVal)+minVal;
	return val * (maxVal - minVal) + minVal;
}

int getFaceIdx(vec3 aVec)
{
	float lx = abs(aVec.x);
	float ly = abs(aVec.y);
	float lz = abs(aVec.z);
	int lIdx;
	if (lx > ly && lx > lz)
		lIdx = lx > 0? 0 : 1;
	else if (ly > lx && ly > lz)
		lIdx = ly > 0? 2 : 3;
	else
		lIdx = lz > 0? 4 : 5;
	return lIdx;
}

int getPosSpaceIdx(vec3 aVec)
{
	ivec3 vIdx = ivec3(4.5 + 1.5 * aVec);
	return (9*vIdx.x + vIdx.y) * 9 + vIdx.z;
}

int getInitIndex(vec3 vl, vec3 vp, vec3 vr)
{
	int lIdx, pIdx, rIdx;
	float lLen = length(vl.xyz);
	// L out of range
	if (lLen > 10 || lLen < 3)
		return -1;
	// P out of range
	if (abs(vp.x) > 3 || abs(vp.y) > 3 || abs(vp.z) > 3)
		return -1;

	lIdx = getFaceIdx(vl);
	rIdx = getFaceIdx(vr);
	pIdx = getPosSpaceIdx(vp);

	return (lIdx + pIdx * 6) * 6 + rIdx;
}


void evalNNTree(TexArray treeNodeList, TexArray nnList, vec3 vl, vec3 vp, vec3 vr)
{
	int idx = getInitIndex(vl, vp, vr);
	// If out of the region, then ignore
	if (idx == -1)
	{
		nnResult[0] = nnResult[1] = 0.0;
		return;
	}
	int splitDim;
	while ((splitDim = int(treeNodeList.getFloat(TYPE_IDX, idx))) != -1)
	{
		if (splitDim  > 3)
			splitDim = R_OFFSET(splitDim);
		float bigger = treeNodeList.getFloat(BIGGER(splitDim), idx);
		float smaller = treeNodeList.getFloat(splitDim, idx);
		float mid = (bigger + smaller) / 2.0;
		if (nnInput[splitDim] <= mid)
			splitDim = int(treeNodeList.getFloat(L_CHD, idx));
		else
			splitDim = int(treeNodeList.getFloat(R_CHD, idx));
	}
	evalNN(nnList, int(treeNodeList.getFloat(NN_IDX, idx)));
}

/*
void evalNNTree(TexArray treeNodeList, TexArray nnList)
{
	int i = getInitIndex();
	int nodeID = 0;
	vec4 nodeInfo = treeNodeList.getVec4(nodeID, 0);
// [splitDim; splitValue; rightChildIndices; dataIndices] 
	while(!(int(nodeInfo.z)==0))
	{
		if(nnInput[ int(nodeInfo.x)-1 ]<=nodeInfo.y)
		{
			nodeID ++;
		}
		else
		{
			nodeID = int(nodeInfo.z) - 1;
		}
		nodeInfo = treeNodeList.getVec4(nodeID, 0);
	}
	if(debugCode==4)
		fragColor = vec4(nodeID);
// now at leaf
	evalNN(nnList, int(nodeInfo.w)-1);
} */

void evalNN(TexArray nnList, int nnID)
{
	int idx;
	int inputDims = nnList.getInt(0, nnID);
	
// map input--start
	idx = 2; // idx = input range mat
	for(int i=0; i<inputDims; i++)
	{
		nnInput[i] = map(nnInput[i], nnList.getFloat(idx+2*i, nnID), nnList.getFloat(idx+2*i+1, nnID));
	}
// map input--end

// evaluate--start
	idx += 2*inputDims; // idx = numLayers
	int numLayers = nnList.getInt(idx, nnID);
	idx ++; // idx = w1 mat size
	
	for(int i=0; i<numLayers; i++)
	{
		int rows = nnList.getInt(idx, nnID);
		int cols = nnList.getInt(idx+1, nnID);
		
		idx += 2; // idx = w mat
		for(int next=0; next<rows; next++)
		{
			nnResult[next] = 0;
			for(int prev=0; prev<cols; prev++)
			{
				nnResult[next] += nnInput[prev] * nnList.getFloat(idx+next*cols+prev, nnID);
			}
		}
		
		idx += cols*rows;
		
		idx += 2;
		for(int bi=0; bi<rows; bi++)
		{
			nnResult[bi] += nnList.getFloat(idx + bi, nnID);
			if(i != numLayers - 1)
				nnResult[bi] = tansig(nnResult[bi]);
			nnInput[bi] = nnResult[bi];
		}
		idx += rows;
	}
// evaluate--end
// unmap output--start
	int outputDims = nnList.getInt(idx, nnID);
	idx += 2;
	
	for(int i=0; i<outputDims; i++)
	{
		nnResult[i] = unmap(nnResult[i], nnList.getFloat(idx+2*i, nnID), nnList.getFloat(idx+2*i+1, nnID)); 
	}
// unmap output--end
}

/*
void interpolate(int dim, TexArray samples)
{
	int idx = 0;
	float min_sum = 1e32;

	for (int i = 0; i < samples.height; i++)
	{
		float sum = 0;
		for (int j = 0; j < dim; j++)
		{
			float error = samples.getFloat(j, i) - nnInput[j];
			sum += error * error;
		}
		if (sum < min_sum)
		{
			idx = i;
			min_sum = sum;
		}
	}
	nnResult[0] = samples.getFloat(dim+1, idx);
	nnResult[1] = samples.getFloat(dim+2, idx);
}
*/

float sigmoid(float x)
{
	return 1/(1+exp(-x));
}

float tansig(float x)
{
	return 2/(1+exp(-2*x))-1;
}
