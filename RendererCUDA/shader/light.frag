#version 150

uniform vec3 eye_position;
uniform vec3 light_color;
uniform vec3 light_position;

in vec3 normal;
out vec4 fragColor;

void main()
{
	vec3 viewDir = normalize(eye_position - light_position);
	vec3 normal = normalize(normal);
	fragColor = vec4(light_color*(dot(normal, viewDir)*0.5+0.5), 1);
}