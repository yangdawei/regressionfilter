#version 150

uniform int objID;
uniform int controlID;
uniform int lightID;

out vec4 color;

void main()
{
	color = vec4(float(objID), float(controlID), float(lightID), 0);
}