varying vec3 normal;

attribute vec4 gl_Vertex, gl_Normal;
uniform mat4 pMat, mvMat;

void main()
{
	normal = gl_Normal;
    gl_Position =  gl_ModelViewProjectionMatrix * gl_Vertex;
}