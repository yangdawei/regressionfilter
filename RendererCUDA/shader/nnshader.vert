//#version 150

uniform mat4 pMat, mvMat;

varying vec3 normal;
varying vec4 vertex_position;

attribute vec4 gl_Vertex;
attribute vec3 gl_Normal;

void main()
{
	vertex_position = gl_Vertex;
    gl_Position =  pMat * (mvMat * gl_Vertex);
	normal = gl_Normal;
}