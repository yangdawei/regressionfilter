#version 150

in vec3 normal;
in vec4 vertex_position;
out vec4 fragColor;

struct PointLight;
struct PhongMaterial;

// light related
uniform int numLights;
uniform PointLight lightList[10];

// obj related
uniform int objID;
uniform mat4 objTransMatList[10];
uniform PhongMaterial objMaterialList[2];

// eye related
uniform vec3 eye_position;

// Debug related
uniform int infoIdx;

struct PointLight
{
	vec3 position;
	vec3 color;
};
struct PhongMaterial
{
	vec3 diffColor;
	vec3 specColor;
	float kd, ks, alpha;
};

void main()
{
	if (infoIdx < 4)
	{
		vec3 normal = normalize(normal);
		vec4 transPos = (objTransMatList[objID])*vertex_position;
		vec4 transNormal = (objTransMatList[objID]*vec4(normal, 0));
		normal = transNormal.xyz;
		vec3 out_dir = normalize(eye_position - transPos.xyz);
		vec3 reflDir = dot(out_dir, normal)*normal*2-out_dir;
		vec4 transReflDir = objTransMatList[objID]*vec4(reflDir, 0);
		
		int li = 0;
		vec3 in_dir = normalize(transPos.xyz - lightList[li].position);
		vec3 half_dir = normalize(out_dir - in_dir);
		int oi = 1 - objID;
		mat4 invMat = inverse(objTransMatList[oi]);
		vec3 relPos = (invMat*transPos).xyz;
		vec3 relNormal = normalize((invMat*transNormal).xyz);
		vec3 relReflDir = normalize((invMat*transReflDir).xyz);
		vec3 relLightPos = (invMat*vec4(lightList[li].position,1)).xyz;
		if (infoIdx == 0)	// Lighting point
			fragColor = vec4(relLightPos, objID);
		else if (infoIdx == 1) // Bouncing point
			fragColor = vec4(relPos, 0.0);
		else if (infoIdx == 2) // Normal
			fragColor = vec4(relNormal, 0.0);
		else 					// Refldir
			fragColor = vec4(relReflDir, objMaterialList[objID].alpha);
	}
	else
	{
		if (infoIdx == 4)
			fragColor = normalize(objTransMatList[objID]*vertex_position);
		else if (infoIdx == 5)
			fragColor = normalize(objTransMatList[objID]*vec4(normal, 0));
	}
}
