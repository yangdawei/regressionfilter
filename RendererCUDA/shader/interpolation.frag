#version 150
#define NN_MAX_NEURON_NUM 25
#define MAX_OBJ_NUM 5
#define MAX_LIGHT_NUM 5
#define DEBUG 1

#if DEBUG
/*8uniform*/ int debugCode = -1;
#endif

in vec3 normal;
in vec4 vertex_position;
out vec4 fragColor;

struct PointLight;
struct TexArray;
struct PhongMaterial;

// bounce related
uniform int numBounces;

// light related
uniform int numLights;
uniform PointLight lightList[MAX_LIGHT_NUM];

// obj related
uniform int objID;
uniform int numObjects;
uniform mat4 objTransMatList[MAX_OBJ_NUM];
uniform PhongMaterial objMaterialList[MAX_OBJ_NUM];

// eye related
uniform vec3 eye_position;

// nn related
uniform TexArray diff_treeNodeTex[MAX_OBJ_NUM], diff_nnListTex[MAX_OBJ_NUM];
uniform TexArray spec_treeNodeTex[MAX_OBJ_NUM], spec_nnListTex[MAX_OBJ_NUM];

float nnInput[NN_MAX_NEURON_NUM];
float nnResult[NN_MAX_NEURON_NUM];

float map(float val, float minVal, float maxVal);
float unmap(float val, float minVal, float maxVal);
void mapDiffInput(int oi);
void unmapDiffOutput(int oi);
void mapSpecInput(int oi);
void unmapSpecOutput(int oi);
float tansig(float x);
float sigmoid(float x);
void evalNNTree(TexArray treeNodeList, TexArray nnList);
void evalNN(TexArray nnList, int nnID);

bool g_mapCorrect = true;
bool g_unmapCorrect = true;

struct PointLight
{
	vec3 position;
	vec3 color;
};

struct TexArray
{
	int width;
	int height;
	sampler2D id;
	vec4 getVec4(int x, int y)
	{
		float u = (x+0.5)/width;
		float v = (y+0.5)/height;
		return texture2D(id, vec2(u, v));
	}
	float getFloat(int x, int y)
	{
		return getVec4(x/4, y)[x%4];
	}
	int getInt(int x, int y)
	{
		return int(round(getVec4(x/4, y)[x%4]));
	}
};

struct PhongMaterial
{
	vec3 diffColor;
	vec3 specColor;
	float kd, ks, alpha;
};

void main()
{

	vec3 normal = normalize(normal);
	vec4 transPos = (objTransMatList[objID])*vertex_position;
	vec4 transNormal = (objTransMatList[objID]*vec4(normal, 0));
	vec3 out_dir = normalize(eye_position - transPos.xyz);
	vec3 reflDir = dot(out_dir, normal)*normal*2-out_dir;
	vec4 transReflDir = objTransMatList[objID]*vec4(reflDir, 0);
	vec3 diffColor_0b = vec3(0,0,0);
	vec3 specColor_0b = vec3(0,0,0);
	vec3 diffColor_1b = vec3(0,0,0);
	vec3 specColor_1b = vec3(0,0,0);
	vec3 color_0b;
	vec3 color_1b;
	vec3 ds_color;
	
	for(int li=0; li<numLights; li++)
	{
		vec3 in_dir = normalize(transPos.xyz - lightList[li].position);
		vec3 half_dir = normalize(out_dir - in_dir);
		float spec_intensity = pow(max(dot(normal, half_dir),0), objMaterialList[objID].alpha);
		diffColor_0b += lightList[li].color*objMaterialList[objID].kd*objMaterialList[objID].diffColor;
		specColor_0b += lightList[li].color*objMaterialList[objID].ks*objMaterialList[objID].specColor*spec_intensity;
		diffColor_0b *= max(dot(normal, -in_dir), 0);
		specColor_0b *= max(dot(normal, -in_dir), 0);
		for(int oi=0; oi<numObjects; oi++)
		{
			if(oi == objID)
				continue;
			mat4 invMat = inverse(objTransMatList[oi]);
			vec3 relPos = (invMat*transPos).xyz;
			vec3 relNormal = normalize((invMat*transNormal).xyz);
			vec3 relReflDir = normalize((invMat*transReflDir).xyz);
			vec3 relLightPos = (invMat*vec4(lightList[li].position,1)).xyz;
			
			
			nnInput[0] = relLightPos.x;
			nnInput[1] = relLightPos.y;
			nnInput[2] = relLightPos.z;
			nnInput[3] = relPos.x;
			nnInput[4] = relPos.y;
			nnInput[5] = relPos.z;
			nnInput[6] = relNormal.x;
			nnInput[7] = relNormal.y;
			nnInput[8] = relNormal.z;
#if DEBUG
			switch(debugCode)
			{
			case 0:
				fragColor = vec4(nnInput[0], nnInput[1], nnInput[2], nnInput[3]);
				return;
			case 1:
				fragColor = vec4(nnInput[4], nnInput[5], nnInput[6], nnInput[7]);
				return;
			case 2:
				fragColor.x = nnInput[8];
			}
#endif
			evalNNTree(diff_treeNodeTex[oi], diff_nnListTex[oi]);
			
#if DEBUG
			if(debugCode == 2)
			{
				fragColor.yzw = vec3(nnResult[0] / 10.0+1, nnResult[1] / 10.0+1, 0);
				return;
			}
#endif
			
			//fragColor = vec4(nnResult[0]);
			//return;
			
			ds_color = objMaterialList[oi].diffColor*objMaterialList[oi].kd*nnResult[0] +
				objMaterialList[oi].specColor*objMaterialList[oi].ks*nnResult[1];
			diffColor_1b += lightList[li].color*objMaterialList[objID].diffColor*objMaterialList[objID].kd*ds_color;
			
			nnInput[0] = relLightPos.x;
			nnInput[1] = relLightPos.y;
			nnInput[2] = relLightPos.z;
			nnInput[3] = relPos.x;
			nnInput[4] = relPos.y;
			nnInput[5] = relPos.z;
			nnInput[6] = relReflDir.x;
			nnInput[7] = relReflDir.y;
			nnInput[8] = relReflDir.z;
			nnInput[9] = objMaterialList[objID].alpha;

			evalNNTree(spec_treeNodeTex[oi], spec_nnListTex[oi]);
			
			ds_color = objMaterialList[oi].diffColor*objMaterialList[oi].kd*nnResult[0] +
				objMaterialList[oi].specColor*objMaterialList[oi].ks*nnResult[1];
			specColor_1b += lightList[li].color*objMaterialList[objID].specColor*objMaterialList[objID].ks*ds_color;
		}
	}
	color_0b = diffColor_0b + specColor_0b;
	color_1b = diffColor_1b + specColor_1b;
	if (numBounces == -1)
		fragColor = vec4(color_1b, 1);
	else if (numBounces != 0)
		fragColor = vec4(color_0b+color_1b, 1);
	else
		fragColor = vec4(color_0b, 1);

	//if (g_mapCorrect)
		//fragColor.x = 1.0f;
	//else
		//fragColor.x = 0.f;
	//if (g_unmapCorrect)
//		fragColor.z = 1.0f;
//	else
//		fragColor.z = 0.f;

//	fragColor.y = 0.1f;
}

float map(float val, float minVal, float maxVal)
{
	/*
	if (val < minVal-0.000001 || val > maxVal+0.000001)
		g_mapCorrect = false;
		*/

	return (val-minVal)/(maxVal-minVal)*2-1;
}

float unmap(float val, float minVal, float maxVal)
{
	/*
	if (val < -1 || val > 2)
		g_unmapCorrect = false;
		*/
	return val*(maxVal-minVal)+minVal;
}

void evalNNTree(TexArray treeNodeList, TexArray nnList)
{
	int nodeID = 0;
	vec4 nodeInfo = treeNodeList.getVec4(nodeID, 0);
// [splitDim; splitValue; rightChildIndices; dataIndices] 
	while(!(int(nodeInfo.z)==0))
	{
		if(nnInput[ int(nodeInfo.x)-1 ]<=nodeInfo.y)
		{
			nodeID ++;
		}
		else
		{
			nodeID = int(nodeInfo.z) - 1;
		}
		nodeInfo = treeNodeList.getVec4(nodeID, 0);
	}
	if(debugCode==4)
		fragColor = vec4(nodeID);
// now at leaf
	evalNN(nnList, int(nodeInfo.w)-1);
}

void evalNN(TexArray nnList, int nnID)
{
	int idx;
	int inputDims = nnList.getInt(0, nnID);
	
// map input--start
	idx = 2; // idx = input range mat
	for(int i=0; i<inputDims; i++)
	{
		nnInput[i] = map(nnInput[i], nnList.getFloat(idx+2*i, nnID), nnList.getFloat(idx+2*i+1, nnID));
	}
// map input--end

// evaluate--start
	idx += 2*inputDims; // idx = numLayers
	int numLayers = nnList.getInt(idx, nnID);
	idx ++; // idx = w1 mat size
	
	for(int i=0; i<numLayers; i++)
	{
		int rows = nnList.getInt(idx, nnID);
		int cols = nnList.getInt(idx+1, nnID);
		
		idx += 2; // idx = w mat
		for(int next=0; next<rows; next++)
		{
			nnResult[next] = 0;
			for(int prev=0; prev<cols; prev++)
			{
				nnResult[next] += nnInput[prev] * nnList.getFloat(idx+next*cols+prev, nnID);
			}
		}
		
		idx += cols*rows;
		
		idx += 2;
		for(int bi=0; bi<rows; bi++)
		{
			nnResult[bi] += nnList.getFloat(idx + bi, nnID);
			//if(i != numLayers - 1)
			nnResult[bi] = sigmoid(nnResult[bi]);
			nnInput[bi] = nnResult[bi];
		}
		idx += rows;
	}
// evaluate--end
	//fragColor = vec4(nnResult[0]*0.5+0.5);
	//return;
// unmap output--start
	int outputDims = nnList.getInt(idx, nnID);
	idx += 2;
	
	for(int i=0; i<outputDims; i++)
	{
		nnResult[i] = unmap(nnResult[i], nnList.getFloat(idx+2*i, nnID), nnList.getFloat(idx+2*i+1, nnID)); 
	}
// unmap output--end
}

/*
float tansig(float x)
{
	return 2/(1+exp(-2*x))-1;
	//return 1/(1+exp(-x))-1;
}
*/

float sigmoid(float x)
{
	return 1/(1+exp(-x));
}
