attribute vec4 gl_Vertex;
uniform mat4 pMat, mvMat;

void main()
{
    gl_Position =  gl_ModelViewProjectionMatrix * gl_Vertex;
}