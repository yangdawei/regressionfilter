#version 150

in vec3 normal;
in vec4 vertex_position;
out vec4 fragColor;

uniform int objID;
uniform int infoIdx;

void main()
{
	if (infoIdx == 0)
		fragColor = vertex_position;
	else if (infoIdx == 1)
		fragColor = vec4(normalize(normal), objID);
	else
		fragColor = vec4(1, 1, 1, 1);
}
