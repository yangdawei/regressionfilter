#include <cuda.h>
#include <cuda_runtime.h>
#include <device_launch_parameters.h>
#include <cuda_texture_types.h>

#include "render_kernel.cuh"

#include <stdio.h>

#define MAX_OBJ_NUM 5

using namespace std;

texture<float4, cudaTextureType2D, cudaReadModeElementType> g_vertTex, g_normTex;
texture<float4, cudaTextureType2D, cudaReadModeElementType> g_diffNNTex[MAX_OBJ_NUM], g_specNNTex[MAX_OBJ_NUM];
texture<float4, cudaTextureType2D, cudaReadModeElementType> g_kdIndices[MAX_OBJ_NUM], g_treeNodeTex[MAX_OBJ_NUM];

float3 g_eyePos;

CudaObjPack_t g_objs;
unsigned g_objNum;

CudaLightPack_t g_lights;
unsigned g_lightNum;

//#define ASSIGN4(a, b) {a.x = b.x, a.y = b.y, a.z = b.z, a.w = b.w;}

__global__ void kernel_test(float4 *displayPtr, int width, int height)
{
	int x = threadIdx.x + blockIdx.x * blockDim.x;
	int y = threadIdx.y + blockIdx.y * blockDim.y;
	int offset = x + y * blockDim.x * gridDim.x;

	float4 result1 = tex2D(g_vertTex, x, y);
	float4 result2 = tex2D(g_normTex, x, y);
	float4 result = (result1+result2)/2;
	/*
	result.x = (result1.x + result2.x) / 1.f;
	result.y = (result1.y + result2.y) / 1.f;
	result.z = (result1.z + result2.z) / 1.f;
	*/

	displayPtr[offset] = result;
}

void framework_test(float4 *displayPtr, cudaArray_t vertArr, cudaArray_t normArr, int width, int height)
{
	struct cudaChannelFormatDesc channelDesc = 
	{
		32, 32, 32, 32, 
		cudaChannelFormatKindFloat
	};
	cudaBindTextureToArray(&g_vertTex, vertArr, &channelDesc);
	cudaBindTextureToArray(&g_normTex, normArr, &channelDesc);

	dim3 grids(width/16, height/16);
	dim3 threads(16, 16);
	kernel_test<<<grids, threads>>>(displayPtr, width, height);
}

__global__ void kernel(float4 *displayPtr, int width, int height, CudaObjPack_t objs, CudaLightPack_t lights, unsigned lightNum, float3 eyePos)
{
	int x = threadIdx.x + blockIdx.x * blockDim.x;
	int y = threadIdx.y + blockIdx.y * blockDim.y;
	int offset = x + y * blockDim.x * gridDim.x;

	float4 vertexPosition = tex2D(g_vertTex, x, y);
	float4 normal_4 = tex2D(g_normTex, x, y);
	float3 normal = make_float3(normal_4.x, normal_4.y, normal_4.z);
	int objID = normal_4.w;
	normal_4.w = 0.f;

	if (objID < 0)
	{
		displayPtr[offset] = make_float4(0, 0, 0, 0);
		return;
	}

	float3 out_dir, 
		color_0b, color_1b, ds_color, reflDir, diffColor_0b,
		specColor_0b, diffColor_1b, specColor_1b, in_dir, half_dir, relPos, relNormal,
		relReflDir, relLightPos, fragColor;

	float4 transPos, transNormal, transReflDir;
	float4x4 invMat;

	transPos = objs[objID].transMat * vertexPosition;
	transNormal = objs[objID].transMat * normal_4;
	normal = make_float3(transNormal.x, transNormal.y, transNormal.z);
	normal = normalize(normal);
	const float3 &eye_position = eyePos;
	out_dir = normalize(eye_position - make_float3(transPos.x, transPos.y, transPos.z));
	reflDir = dot(out_dir, normal) * normal * 2 - out_dir;
	transReflDir = objs[objID].transMat * make_float4(reflDir.x, reflDir.y, reflDir.z, 0.f);

	diffColor_0b = make_float3(0, 0, 0);
	specColor_0b = make_float3(0, 0, 0);
	diffColor_1b = make_float3(0, 0, 0);
	specColor_1b = make_float3(0, 0, 0);

	for (int li = 0; li < lightNum; li++)
	{
		float3 diff = make_float3(transPos.x, transPos.y, transPos.z) - lights[li].position;
		in_dir = normalize(diff);
		float light_dist = length(diff);
		half_dir = out_dir - in_dir;
		half_dir = normalize(half_dir);
		float spec_intensity = pow(max(dot(normal, half_dir), 0.f), objs[objID].alpha);
		float d_t = max(dot(normal, -in_dir), 0.f);
		diffColor_0b += d_t * lights[li].color * objs[objID].kd * objs[objID].diffColor / light_dist;
		specColor_0b += d_t * lights[li].color * objs[objID].ks * objs[objID].specColor * spec_intensity / light_dist;
	}
	fragColor = diffColor_0b + specColor_0b;

	displayPtr[offset] = make_float4(fragColor, 0.f);
	// displayPtr[offset] = transNormal;
}

void launch_kernel(float4 *displayPtr, unsigned width, unsigned height, cudaArray_t vertArr, cudaArray_t normArr, CudaObjPack_t objPack, CudaLightPack_t lightPack, unsigned objNum, unsigned lightNum, float3 eyePosition)
{
	// Prepare vert and norm tex
	struct cudaChannelFormatDesc channelDesc = 
	{
		32, 32, 32, 32, 
		cudaChannelFormatKindFloat
	};
	cudaBindTextureToArray(&g_vertTex, vertArr, &channelDesc);
	cudaBindTextureToArray(&g_normTex, normArr, &channelDesc);

	// Prepare parameters

	dim3 grids(width/16, height/16);
	dim3 threads(16, 16);
	kernel<<<grids, threads>>>(displayPtr, width, height, objPack, lightPack, lightNum, eyePosition);;
	//kernel_test<<<grids, threads>>>(displayPtr, width, height);
}
