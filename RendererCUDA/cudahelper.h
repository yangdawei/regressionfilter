#ifndef CUDAHELPER_H
#define CUDAHELPER_H

#include <cuda_runtime.h>

#define checkCudaErrors(val)           check ( (val), #val, __FILE__, __LINE__ )
#ifndef MAX
#define MAX(a,b) (a > b ? a : b)
#endif

int gpuGetMaxGflopsDeviceId();


#endif