#pragma once

#include "nvMatrix.h"
#include "nvVector.h"
#include "render_kernel.cuh"

inline float4 fromVec4(const nv::vec4f &v)
{
	return make_float4(v.x, v.y, v.z, v.y);
}

inline float3 fromVec3(const nv::vec3f &v)
{
	return make_float3(v.x, v.y, v.z);
}

inline float4x4 fromMat4(const nv::matrix4<float> &m)
{
	float4x4 ret;
	for (int i = 0; i < 4; i++)
		for (int j = 0; j < 4; j++)
			ret.m_elements[i+4*j] = m(i, j);
	return ret;
}