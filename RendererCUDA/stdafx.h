// stdafx.h : include file for standard system include files,
// or project specific include files that are used frequently, but
// are changed infrequently
//

#pragma once

//#define INTP_DEBUG
//#define USE_SAMPLER
//#define FILE_OUTPUT
//#define FILE_OUTPUT_TRAIN_DEBUG
#define _CheckErrorGL {\
	GLenum code;\
	if ((code = glGetError()) != GL_NO_ERROR)\
	{\
		printf("Error %d: %s\n", int(code), gluErrorString(code));\
		printf("At %s:%d\n", __FILE__, __LINE__); \
	}\
}

#include "targetver.h"

#include <stdio.h>
#include <tchar.h>
#include <sstream>
#include <GL/glew.h>
#include <cassert>
#include <cstdlib>



// TODO: reference additional headers your program requires here
